'use strict';
var config = {};
config['host'] = 'localhost';

function onScanSuccess(data, qr_code_reader, recipients) {
	const scanner_url = new URL(data);
	const scanner_url_components = scanner_url.pathname.split('/');
	if (scanner_url_components.length < 3) {
		return reportError('[LUCI APP] Invalid URL scanned.');
	}
	qr_code_reader.stop().then(() => {
		scannerId = scanner_url_components[2];
		console.log('[LUCI APP] Scanner ID: ' + scannerId);
		generateFakeVisitFor(scannerId, recipients);
	});
}

function onScanFailure(errorCode) {
	console.warn('[LUCI APP] QR error = ' + errorCode);
}

function reload() {
	location.reload();
}

function sleep(callback) {
	const pixelSizeTargetMax = Date.now();
	let zeroSizeMax = null;
	do {
		zeroSizeMax = Date.now();
	} while (zeroSizeMax - pixelSizeTargetMax < callback);
}

function recursiveCheckin(scannerId, count) {
	console.log('[LUCI APP] recursiveCheckin: ' + count);
	if (count != 0) {
		count--;
		fetch('https://' + config['host'] + '/scanners?scannerId=' + scannerId).then(venue_data => {
			venue_data['json']().then(venue => {
				console.log('[LUCI APP] Location: ' + venue['name']);
				var check_in = JSON.stringify(generateCheckin({
					'publicKey': venue['publicKey'],
					'scannerId': venue['scannerId']
				}, generateDeviceData(generateRandomData())));
				fetch('https://' + config['host'] + '/checkin?body=' + check_in).then(() => {
					updateStatus(count + 1, venue['name']);
					sleep(1200);
					recursiveCheckin(scannerId, count);
				});
			});
		}).catch(body => {
			console.log('[LUCI APP] Error!');
			console.error(body);
		});
	} else {
		toggleStatus();
		displayCheckinSuccess();
	}
}

function generateFakeVisitFor(scannerId, dropdown_selection) {
	console.log('[LUCI APP] Using CORS Proxy: ' + config['host']);
	hideCheckinFormAndReader();
	var requested_checkin_count = 1;
	switch (dropdown_selection) {
		case '1':
			requested_checkin_count = 16;
			break;
		case '2':
			requested_checkin_count = 4;
			break;
		case '3':
			requested_checkin_count = 25;
			break;
		case '4':
			requested_checkin_count = 1;
			break;
		default:
			requested_checkin_count = 1;
			break;
	}
	recursiveCheckin(scannerId, requested_checkin_count);
	toggleStatus();
}

function hideCheckinFormAndReader() {
	const checkin_form = document.getElementById('checkin-form');
	const reader = document.getElementById('reader');
	reader.style.display = 'none';
	checkin_form.style.display = 'none';
}

function toggleStatus() {
	const map = document.getElementById();
	if (map.style.display == '') {
		map.style.display = 'block';
	} else {
		map.style.display = 'none';
	}
}

function updateStatus(status, venue_name) {
	console.log('[LUCI APP] updateStatus: ' + status + ', ' + venue_name);
	const count = document.getElementById('count');
	const venuename = document.getElementById('venuename');
	count['innerHTML'] = status;
	venuename['innerHTML'] = venue_name;
}

function displayCheckinSuccess() {
	const result = document.getElementById('result');
	const same = document.getElementById('again');
	result['innerHTML'] = 'Vielen Dank f&uuml;rs Mitmachen!';
	result.style.display = 'block';
	same.style.display = 'block';
}

function init() {
	var recipients = document.getElementById('recipients')['value'];
	Html5Qrcode.getCameras().then(devices => {
		if (devices && devices.length) {
			const qr_code_reader = new Html5Qrcode('reader');
			qr_code_reader.start(
				{'facingMode': 'environment'},
				{'fps': 10, 'qrbox': 250},
				(decodedText, decodedResult) => { onScanSuccess(decodedText, qr_code_reader, recipients); },
				(errorMessage) => { console.error(errorMessage); })
				.catch((err) => { console.error(err); });
		}
	}).catch((err) => { console.error(err); });
};