'use strict';
!function(Collection, require) {
  if ("object" == typeof exports && "object" == typeof module) {
    module.exports = require();
  } else {
    if ("function" == typeof define && define.amd) {
      define([], require);
    } else {
      if ("object" == typeof exports) {
        exports.forge = require();
      } else {
        Collection.forge = require();
      }
    }
  }
}(this, function() {
  return function(e) {
    /**
     * @param {string} i
     * @return {?}
     */
    function t(i) {
      if (n[i]) {
        return n[i].exports;
      }
      var module = n[i] = {
        i : i,
        l : false,
        exports : {}
      };
      return e[i].call(module.exports, module, module.exports, t), module.l = true, module.exports;
    }
    var n = {};
    return t.m = e, t.c = n, t.i = function(value) {
      return value;
    }, t.d = function(d, name, n) {
      if (!t.o(d, name)) {
        Object.defineProperty(d, name, {
          configurable : false,
          enumerable : true,
          get : n
        });
      }
    }, t.n = function(module) {
      /** @type {function(): ?} */
      var n = module && module.__esModule ? function() {
        return module.default;
      } : function() {
        return module;
      };
      return t.d(n, "a", n), n;
    }, t.o = function(t, object) {
      return Object.prototype.hasOwnProperty.call(t, object);
    }, t.p = "", t(t.s = 42);
  }([function(module, canCreateDiscussions) {
    module.exports = {
      options : {
        usePureJavaScript : false
      }
    };
  }, function(mixin, canCreateDiscussions, baseTemp) {
    /**
     * @param {number} o
     * @return {undefined}
     */
    function push(o) {
      if (8 !== o && 16 !== o && 24 !== o && 32 !== o) {
        throw new Error("Only 8, 16, 24, or 32 bits supported: " + o);
      }
    }
    /**
     * @param {!Object} b
     * @return {undefined}
     */
    function ByteStringBuffer(b) {
      if (this.data = "", this.read = 0, "string" == typeof b) {
        /** @type {!Object} */
        this.data = b;
      } else {
        if (util.isArrayBuffer(b) || util.isArrayBufferView(b)) {
          /** @type {!Uint8Array} */
          var arr = new Uint8Array(b);
          try {
            /** @type {string} */
            this.data = String.fromCharCode.apply(null, arr);
          } catch (e) {
            /** @type {number} */
            var i = 0;
            for (; i < arr.length; ++i) {
              this.putByte(arr[i]);
            }
          }
        } else {
          if (b instanceof ByteStringBuffer || "object" == typeof b && "string" == typeof b.data && "number" == typeof b.read) {
            this.data = b.data;
            this.read = b.read;
          }
        }
      }
      /** @type {number} */
      this._constructedStringLength = 0;
    }
    /**
     * @param {!Object} b
     * @param {number} options
     * @return {?}
     */
    function DataBuffer(b, options) {
      options = options || {};
      this.read = options.readOffset || 0;
      this.growSize = options.growSize || 1024;
      var elements = util.isArrayBuffer(b);
      var a = util.isArrayBufferView(b);
      return elements || a ? (elements ? this.data = new DataView(b) : this.data = new DataView(b.buffer, b.byteOffset, b.byteLength), void(this.write = "writeOffset" in options ? options.writeOffset : this.data.byteLength)) : (this.data = new DataView(new ArrayBuffer(0)), this.write = 0, null !== b && void 0 !== b && this.putBytes(b), void("writeOffset" in options && (this.write = options.writeOffset)));
    }
    var base = baseTemp(0);
    var util = mixin.exports = base.util = base.util || {};
    !function() {
      /**
       * @param {!Object} event
       * @return {undefined}
       */
      function draw(event) {
        if (event.source === window && event.data === zeroTimeoutMessageName) {
          event.stopPropagation();
          var pipelets = r.slice();
          /** @type {number} */
          r.length = 0;
          pipelets.forEach(function(unDraw) {
            unDraw();
          });
        }
      }
      if ("undefined" != typeof process && process.nextTick) {
        return util.nextTick = process.nextTick, void("function" == typeof setImmediate ? util.setImmediate = setImmediate : util.setImmediate = util.nextTick);
      }
      if ("function" == typeof setImmediate) {
        return util.setImmediate = function() {
          return setImmediate.apply(void 0, arguments);
        }, void(util.nextTick = function(callback) {
          return setImmediate(callback);
        });
      }
      if (util.setImmediate = function(callback) {
        setTimeout(callback, 0);
      }, "undefined" != typeof window && "function" == typeof window.postMessage) {
        /** @type {string} */
        var zeroTimeoutMessageName = "forge.setImmediate";
        /** @type {!Array} */
        var r = [];
        /**
         * @param {!Function} callback
         * @return {undefined}
         */
        util.setImmediate = function(callback) {
          r.push(callback);
          if (1 === r.length) {
            window.postMessage(zeroTimeoutMessageName, "*");
          }
        };
        window.addEventListener("message", draw, true);
      }
      if ("undefined" != typeof MutationObserver) {
        /** @type {number} */
        var a = Date.now();
        /** @type {boolean} */
        var attr = true;
        /** @type {!Element} */
        var div = document.createElement("div");
        /** @type {!Array} */
        r = [];
        (new MutationObserver(function() {
          var pipelets = r.slice();
          /** @type {number} */
          r.length = 0;
          pipelets.forEach(function(saveNotifs) {
            saveNotifs();
          });
        })).observe(div, {
          attributes : true
        });
        /** @type {function(!Function): undefined} */
        var oldSetImmediate = util.setImmediate;
        /**
         * @param {!Function} callback
         * @return {undefined}
         */
        util.setImmediate = function(callback) {
          if (Date.now() - a > 15) {
            /** @type {number} */
            a = Date.now();
            oldSetImmediate(callback);
          } else {
            r.push(callback);
            if (1 === r.length) {
              div.setAttribute("a", attr = !attr);
            }
          }
        };
      }
      /** @type {function(!Function): undefined} */
      util.nextTick = util.setImmediate;
    }();
    util.isNodejs = "undefined" != typeof process && process.versions && process.versions.node;
    /** @type {function(*): boolean} */
    util.isArray = Array.isArray || function(name) {
      return "[object Array]" === Object.prototype.toString.call(name);
    };
    /**
     * @param {!Object} obj
     * @return {?}
     */
    util.isArrayBuffer = function(obj) {
      return "undefined" != typeof ArrayBuffer && obj instanceof ArrayBuffer;
    };
    /**
     * @param {!Object} x
     * @return {?}
     */
    util.isArrayBufferView = function(x) {
      return x && util.isArrayBuffer(x.buffer) && void 0 !== x.byteLength;
    };
    /** @type {function(!Object): undefined} */
    util.ByteBuffer = ByteStringBuffer;
    /** @type {function(!Object): undefined} */
    util.ByteStringBuffer = ByteStringBuffer;
    /** @type {number} */
    var _MAX_CONSTRUCTED_STRING_LENGTH = 4096;
    /**
     * @param {number} x
     * @return {undefined}
     */
    util.ByteStringBuffer.prototype._optimizeConstructedString = function(x) {
      this._constructedStringLength += x;
      if (this._constructedStringLength > _MAX_CONSTRUCTED_STRING_LENGTH) {
        this.data.substr(0, 1);
        /** @type {number} */
        this._constructedStringLength = 0;
      }
    };
    /**
     * @return {?}
     */
    util.ByteStringBuffer.prototype.length = function() {
      return this.data.length - this.read;
    };
    /**
     * @return {?}
     */
    util.ByteStringBuffer.prototype.isEmpty = function() {
      return this.length() <= 0;
    };
    /**
     * @param {number} b
     * @return {?}
     */
    util.ByteStringBuffer.prototype.putByte = function(b) {
      return this.putBytes(String.fromCharCode(b));
    };
    /**
     * @param {number} b
     * @param {number} n
     * @return {?}
     */
    util.ByteStringBuffer.prototype.fillWithByte = function(b, n) {
      /** @type {string} */
      b = String.fromCharCode(b);
      var d = this.data;
      for (; n > 0;) {
        if (1 & n) {
          /** @type {string} */
          d = d + b;
        }
        /** @type {number} */
        n = n >>> 1;
        if (n > 0) {
          /** @type {string} */
          b = b + b;
        }
      }
      return this.data = d, this._optimizeConstructedString(n), this;
    };
    /**
     * @param {!Object} data
     * @return {?}
     */
    util.ByteStringBuffer.prototype.putBytes = function(data) {
      return this.data += data, this._optimizeConstructedString(data.length), this;
    };
    /**
     * @param {!Object} str
     * @return {?}
     */
    util.ByteStringBuffer.prototype.putString = function(str) {
      return this.putBytes(util.encodeUtf8(str));
    };
    /**
     * @param {number} i
     * @return {?}
     */
    util.ByteStringBuffer.prototype.putInt16 = function(i) {
      return this.putBytes(String.fromCharCode(i >> 8 & 255) + String.fromCharCode(255 & i));
    };
    /**
     * @param {number} i
     * @return {?}
     */
    util.ByteStringBuffer.prototype.putInt24 = function(i) {
      return this.putBytes(String.fromCharCode(i >> 16 & 255) + String.fromCharCode(i >> 8 & 255) + String.fromCharCode(255 & i));
    };
    /**
     * @param {number} i
     * @return {?}
     */
    util.ByteStringBuffer.prototype.putInt32 = function(i) {
      return this.putBytes(String.fromCharCode(i >> 24 & 255) + String.fromCharCode(i >> 16 & 255) + String.fromCharCode(i >> 8 & 255) + String.fromCharCode(255 & i));
    };
    /**
     * @param {number} i
     * @return {?}
     */
    util.ByteStringBuffer.prototype.putInt16Le = function(i) {
      return this.putBytes(String.fromCharCode(255 & i) + String.fromCharCode(i >> 8 & 255));
    };
    /**
     * @param {number} i
     * @return {?}
     */
    util.ByteStringBuffer.prototype.putInt24Le = function(i) {
      return this.putBytes(String.fromCharCode(255 & i) + String.fromCharCode(i >> 8 & 255) + String.fromCharCode(i >> 16 & 255));
    };
    /**
     * @param {number} i
     * @return {?}
     */
    util.ByteStringBuffer.prototype.putInt32Le = function(i) {
      return this.putBytes(String.fromCharCode(255 & i) + String.fromCharCode(i >> 8 & 255) + String.fromCharCode(i >> 16 & 255) + String.fromCharCode(i >> 24 & 255));
    };
    /**
     * @param {number} i
     * @param {number} n
     * @return {?}
     */
    util.ByteStringBuffer.prototype.putInt = function(i, n) {
      push(n);
      /** @type {string} */
      var bytes = "";
      do {
        /** @type {number} */
        n = n - 8;
        /** @type {string} */
        bytes = bytes + String.fromCharCode(i >> n & 255);
      } while (n > 0);
      return this.putBytes(bytes);
    };
    /**
     * @param {number} i
     * @param {number} n
     * @return {?}
     */
    util.ByteStringBuffer.prototype.putSignedInt = function(i, n) {
      return i < 0 && (i = i + (2 << n - 1)), this.putInt(i, n);
    };
    /**
     * @param {string} buffer
     * @return {?}
     */
    util.ByteStringBuffer.prototype.putBuffer = function(buffer) {
      return this.putBytes(buffer.getBytes());
    };
    /**
     * @return {?}
     */
    util.ByteStringBuffer.prototype.getByte = function() {
      return this.data.charCodeAt(this.read++);
    };
    /**
     * @return {?}
     */
    util.ByteStringBuffer.prototype.getInt16 = function() {
      /** @type {number} */
      var e = this.data.charCodeAt(this.read) << 8 ^ this.data.charCodeAt(this.read + 1);
      return this.read += 2, e;
    };
    /**
     * @return {?}
     */
    util.ByteStringBuffer.prototype.getInt24 = function() {
      /** @type {number} */
      var e = this.data.charCodeAt(this.read) << 16 ^ this.data.charCodeAt(this.read + 1) << 8 ^ this.data.charCodeAt(this.read + 2);
      return this.read += 3, e;
    };
    /**
     * @return {?}
     */
    util.ByteStringBuffer.prototype.getInt32 = function() {
      /** @type {number} */
      var e = this.data.charCodeAt(this.read) << 24 ^ this.data.charCodeAt(this.read + 1) << 16 ^ this.data.charCodeAt(this.read + 2) << 8 ^ this.data.charCodeAt(this.read + 3);
      return this.read += 4, e;
    };
    /**
     * @return {?}
     */
    util.ByteStringBuffer.prototype.getInt16Le = function() {
      /** @type {number} */
      var e = this.data.charCodeAt(this.read) ^ this.data.charCodeAt(this.read + 1) << 8;
      return this.read += 2, e;
    };
    /**
     * @return {?}
     */
    util.ByteStringBuffer.prototype.getInt24Le = function() {
      /** @type {number} */
      var e = this.data.charCodeAt(this.read) ^ this.data.charCodeAt(this.read + 1) << 8 ^ this.data.charCodeAt(this.read + 2) << 16;
      return this.read += 3, e;
    };
    /**
     * @return {?}
     */
    util.ByteStringBuffer.prototype.getInt32Le = function() {
      /** @type {number} */
      var e = this.data.charCodeAt(this.read) ^ this.data.charCodeAt(this.read + 1) << 8 ^ this.data.charCodeAt(this.read + 2) << 16 ^ this.data.charCodeAt(this.read + 3) << 24;
      return this.read += 4, e;
    };
    /**
     * @param {number} n
     * @return {?}
     */
    util.ByteStringBuffer.prototype.getInt = function(n) {
      push(n);
      /** @type {number} */
      var value = 0;
      do {
        value = (value << 8) + this.data.charCodeAt(this.read++);
        /** @type {number} */
        n = n - 8;
      } while (n > 0);
      return value;
    };
    /**
     * @param {number} n
     * @return {?}
     */
    util.ByteStringBuffer.prototype.getSignedInt = function(n) {
      var x = this.getInt(n);
      /** @type {number} */
      var max = 2 << n - 2;
      return x >= max && (x = x - (max << 1)), x;
    };
    /**
     * @param {number} count
     * @return {?}
     */
    util.ByteStringBuffer.prototype.getBytes = function(count) {
      var rval;
      return count ? (count = Math.min(this.length(), count), rval = this.data.slice(this.read, this.read + count), this.read += count) : 0 === count ? rval = "" : (rval = 0 === this.read ? this.data : this.data.slice(this.read), this.clear()), rval;
    };
    /**
     * @param {number} count
     * @return {?}
     */
    util.ByteStringBuffer.prototype.bytes = function(count) {
      return "undefined" == typeof count ? this.data.slice(this.read) : this.data.slice(this.read, this.read + count);
    };
    /**
     * @param {number} i
     * @return {?}
     */
    util.ByteStringBuffer.prototype.at = function(i) {
      return this.data.charCodeAt(this.read + i);
    };
    /**
     * @param {number} i
     * @param {number} val
     * @return {?}
     */
    util.ByteStringBuffer.prototype.setAt = function(i, val) {
      return this.data = this.data.substr(0, this.read + i) + String.fromCharCode(val) + this.data.substr(this.read + i + 1), this;
    };
    /**
     * @return {?}
     */
    util.ByteStringBuffer.prototype.last = function() {
      return this.data.charCodeAt(this.data.length - 1);
    };
    /**
     * @return {?}
     */
    util.ByteStringBuffer.prototype.copy = function() {
      var c = util.createBuffer(this.data);
      return c.read = this.read, c;
    };
    /**
     * @return {?}
     */
    util.ByteStringBuffer.prototype.compact = function() {
      return this.read > 0 && (this.data = this.data.slice(this.read), this.read = 0), this;
    };
    /**
     * @return {?}
     */
    util.ByteStringBuffer.prototype.clear = function() {
      return this.data = "", this.read = 0, this;
    };
    /**
     * @param {number} count
     * @return {?}
     */
    util.ByteStringBuffer.prototype.truncate = function(count) {
      /** @type {number} */
      var len = Math.max(0, this.length() - count);
      return this.data = this.data.substr(this.read, len), this.read = 0, this;
    };
    /**
     * @return {?}
     */
    util.ByteStringBuffer.prototype.toHex = function() {
      /** @type {string} */
      var ret = "";
      var i = this.read;
      for (; i < this.data.length; ++i) {
        var r = this.data.charCodeAt(i);
        if (r < 16) {
          /** @type {string} */
          ret = ret + "0";
        }
        ret = ret + r.toString(16);
      }
      return ret;
    };
    /**
     * @return {?}
     */
    util.ByteStringBuffer.prototype.toString = function() {
      return util.decodeUtf8(this.bytes());
    };
    /** @type {function(!Object, number): ?} */
    util.DataBuffer = DataBuffer;
    /**
     * @return {?}
     */
    util.DataBuffer.prototype.length = function() {
      return this.write - this.read;
    };
    /**
     * @return {?}
     */
    util.DataBuffer.prototype.isEmpty = function() {
      return this.length() <= 0;
    };
    /**
     * @param {number} amount
     * @param {number} growSize
     * @return {?}
     */
    util.DataBuffer.prototype.accommodate = function(amount, growSize) {
      if (this.length() >= amount) {
        return this;
      }
      /** @type {number} */
      growSize = Math.max(growSize || this.growSize, amount);
      /** @type {!Uint8Array} */
      var r = new Uint8Array(this.data.buffer, this.data.byteOffset, this.data.byteLength);
      /** @type {!Uint8Array} */
      var buf = new Uint8Array(this.length() + growSize);
      return buf.set(r), this.data = new DataView(buf.buffer), this;
    };
    /**
     * @param {number} b
     * @return {?}
     */
    util.DataBuffer.prototype.putByte = function(b) {
      return this.accommodate(1), this.data.setUint8(this.write++, b), this;
    };
    /**
     * @param {number} b
     * @param {number} n
     * @return {?}
     */
    util.DataBuffer.prototype.fillWithByte = function(b, n) {
      this.accommodate(n);
      /** @type {number} */
      var hits = 0;
      for (; hits < n; ++hits) {
        this.data.setUint8(b);
      }
      return this;
    };
    /**
     * @param {!Object} data
     * @param {string} encoding
     * @return {?}
     */
    util.DataBuffer.prototype.putBytes = function(data, encoding) {
      if (util.isArrayBufferView(data)) {
        /** @type {!Uint8Array} */
        var src = new Uint8Array(data.buffer, data.byteOffset, data.byteLength);
        /** @type {number} */
        var len = src.byteLength - src.byteOffset;
        this.accommodate(len);
        /** @type {!Uint8Array} */
        var dst = new Uint8Array(this.data.buffer, this.write);
        return dst.set(src), this.write += len, this;
      }
      if (util.isArrayBuffer(data)) {
        /** @type {!Uint8Array} */
        src = new Uint8Array(data);
        this.accommodate(src.byteLength);
        /** @type {!Uint8Array} */
        dst = new Uint8Array(this.data.buffer);
        return dst.set(src, this.write), this.write += src.byteLength, this;
      }
      if (data instanceof util.DataBuffer || "object" == typeof data && "number" == typeof data.read && "number" == typeof data.write && util.isArrayBufferView(data.data)) {
        /** @type {!Uint8Array} */
        src = new Uint8Array(data.data.byteLength, data.read, data.length());
        this.accommodate(src.byteLength);
        /** @type {!Uint8Array} */
        dst = new Uint8Array(data.data.byteLength, this.write);
        return dst.set(src), this.write += src.byteLength, this;
      }
      if (data instanceof util.ByteStringBuffer && (data = data.data, encoding = "binary"), encoding = encoding || "binary", "string" == typeof data) {
        var view;
        if ("hex" === encoding) {
          return this.accommodate(Math.ceil(data.length / 2)), view = new Uint8Array(this.data.buffer, this.write), this.write += util.binary.hex.decode(data, view, this.write), this;
        }
        if ("base64" === encoding) {
          return this.accommodate(3 * Math.ceil(data.length / 4)), view = new Uint8Array(this.data.buffer, this.write), this.write += util.binary.base64.decode(data, view, this.write), this;
        }
        if ("utf8" === encoding && (data = util.encodeUtf8(data), encoding = "binary"), "binary" === encoding || "raw" === encoding) {
          return this.accommodate(data.length), view = new Uint8Array(this.data.buffer, this.write), this.write += util.binary.raw.decode(view), this;
        }
        if ("utf16" === encoding) {
          return this.accommodate(2 * data.length), view = new Uint16Array(this.data.buffer, this.write), this.write += util.text.utf16.encode(view), this;
        }
        throw new Error("Invalid encoding: " + encoding);
      }
      throw Error("Invalid parameter: " + data);
    };
    /**
     * @param {!Object} buffer
     * @return {?}
     */
    util.DataBuffer.prototype.putBuffer = function(buffer) {
      return this.putBytes(buffer), buffer.clear(), this;
    };
    /**
     * @param {!Object} str
     * @return {?}
     */
    util.DataBuffer.prototype.putString = function(str) {
      return this.putBytes(str, "utf16");
    };
    /**
     * @param {number} i
     * @return {?}
     */
    util.DataBuffer.prototype.putInt16 = function(i) {
      return this.accommodate(2), this.data.setInt16(this.write, i), this.write += 2, this;
    };
    /**
     * @param {number} i
     * @return {?}
     */
    util.DataBuffer.prototype.putInt24 = function(i) {
      return this.accommodate(3), this.data.setInt16(this.write, i >> 8 & 65535), this.data.setInt8(this.write, i >> 16 & 255), this.write += 3, this;
    };
    /**
     * @param {number} val
     * @return {?}
     */
    util.DataBuffer.prototype.putInt32 = function(val) {
      return this.accommodate(4), this.data.setInt32(this.write, val), this.write += 4, this;
    };
    /**
     * @param {?} i
     * @return {?}
     */
    util.DataBuffer.prototype.putInt16Le = function(i) {
      return this.accommodate(2), this.data.setInt16(this.write, i, true), this.write += 2, this;
    };
    /**
     * @param {number} i
     * @return {?}
     */
    util.DataBuffer.prototype.putInt24Le = function(i) {
      return this.accommodate(3), this.data.setInt8(this.write, i >> 16 & 255), this.data.setInt16(this.write, i >> 8 & 65535, true), this.write += 3, this;
    };
    /**
     * @param {number} i
     * @return {?}
     */
    util.DataBuffer.prototype.putInt32Le = function(i) {
      return this.accommodate(4), this.data.setInt32(this.write, i, true), this.write += 4, this;
    };
    /**
     * @param {number} i
     * @param {number} n
     * @return {?}
     */
    util.DataBuffer.prototype.putInt = function(i, n) {
      push(n);
      this.accommodate(n / 8);
      do {
        /** @type {number} */
        n = n - 8;
        this.data.setInt8(this.write++, i >> n & 255);
      } while (n > 0);
      return this;
    };
    /**
     * @param {number} i
     * @param {number} n
     * @return {?}
     */
    util.DataBuffer.prototype.putSignedInt = function(i, n) {
      return push(n), this.accommodate(n / 8), i < 0 && (i = i + (2 << n - 1)), this.putInt(i, n);
    };
    /**
     * @return {?}
     */
    util.DataBuffer.prototype.getByte = function() {
      return this.data.getInt8(this.read++);
    };
    /**
     * @return {?}
     */
    util.DataBuffer.prototype.getInt16 = function() {
      var rval = this.data.getInt16(this.read);
      return this.read += 2, rval;
    };
    /**
     * @return {?}
     */
    util.DataBuffer.prototype.getInt24 = function() {
      /** @type {number} */
      var e = this.data.getInt16(this.read) << 8 ^ this.data.getInt8(this.read + 2);
      return this.read += 3, e;
    };
    /**
     * @return {?}
     */
    util.DataBuffer.prototype.getInt32 = function() {
      var rval = this.data.getInt32(this.read);
      return this.read += 4, rval;
    };
    /**
     * @return {?}
     */
    util.DataBuffer.prototype.getInt16Le = function() {
      var rval = this.data.getInt16(this.read, true);
      return this.read += 2, rval;
    };
    /**
     * @return {?}
     */
    util.DataBuffer.prototype.getInt24Le = function() {
      /** @type {number} */
      var e = this.data.getInt8(this.read) ^ this.data.getInt16(this.read + 1, true) << 8;
      return this.read += 3, e;
    };
    /**
     * @return {?}
     */
    util.DataBuffer.prototype.getInt32Le = function() {
      var rval = this.data.getInt32(this.read, true);
      return this.read += 4, rval;
    };
    /**
     * @param {number} n
     * @return {?}
     */
    util.DataBuffer.prototype.getInt = function(n) {
      push(n);
      /** @type {number} */
      var value = 0;
      do {
        value = (value << 8) + this.data.getInt8(this.read++);
        /** @type {number} */
        n = n - 8;
      } while (n > 0);
      return value;
    };
    /**
     * @param {number} n
     * @return {?}
     */
    util.DataBuffer.prototype.getSignedInt = function(n) {
      var x = this.getInt(n);
      /** @type {number} */
      var max = 2 << n - 2;
      return x >= max && (x = x - (max << 1)), x;
    };
    /**
     * @param {number} count
     * @return {?}
     */
    util.DataBuffer.prototype.getBytes = function(count) {
      var rval;
      return count ? (count = Math.min(this.length(), count), rval = this.data.slice(this.read, this.read + count), this.read += count) : 0 === count ? rval = "" : (rval = 0 === this.read ? this.data : this.data.slice(this.read), this.clear()), rval;
    };
    /**
     * @param {number} count
     * @return {?}
     */
    util.DataBuffer.prototype.bytes = function(count) {
      return "undefined" == typeof count ? this.data.slice(this.read) : this.data.slice(this.read, this.read + count);
    };
    /**
     * @param {number} i
     * @return {?}
     */
    util.DataBuffer.prototype.at = function(i) {
      return this.data.getUint8(this.read + i);
    };
    /**
     * @param {number} i
     * @param {number} b
     * @return {?}
     */
    util.DataBuffer.prototype.setAt = function(i, b) {
      return this.data.setUint8(i, b), this;
    };
    /**
     * @return {?}
     */
    util.DataBuffer.prototype.last = function() {
      return this.data.getUint8(this.write - 1);
    };
    /**
     * @return {?}
     */
    util.DataBuffer.prototype.copy = function() {
      return new util.DataBuffer(this);
    };
    /**
     * @return {?}
     */
    util.DataBuffer.prototype.compact = function() {
      if (this.read > 0) {
        /** @type {!Uint8Array} */
        var src = new Uint8Array(this.data.buffer, this.read);
        /** @type {!Uint8Array} */
        var dst = new Uint8Array(src.byteLength);
        dst.set(src);
        /** @type {!DataView} */
        this.data = new DataView(dst);
        this.write -= this.read;
        /** @type {number} */
        this.read = 0;
      }
      return this;
    };
    /**
     * @return {?}
     */
    util.DataBuffer.prototype.clear = function() {
      return this.data = new DataView(new ArrayBuffer(0)), this.read = this.write = 0, this;
    };
    /**
     * @param {number} count
     * @return {?}
     */
    util.DataBuffer.prototype.truncate = function(count) {
      return this.write = Math.max(0, this.length() - count), this.read = Math.min(this.read, this.write), this;
    };
    /**
     * @return {?}
     */
    util.DataBuffer.prototype.toHex = function() {
      /** @type {string} */
      var ret = "";
      var i = this.read;
      for (; i < this.data.byteLength; ++i) {
        var r = this.data.getUint8(i);
        if (r < 16) {
          /** @type {string} */
          ret = ret + "0";
        }
        ret = ret + r.toString(16);
      }
      return ret;
    };
    /**
     * @param {string} encoding
     * @return {?}
     */
    util.DataBuffer.prototype.toString = function(encoding) {
      /** @type {!Uint8Array} */
      var result = new Uint8Array(this.data, this.read, this.length());
      if (encoding = encoding || "utf8", "binary" === encoding || "raw" === encoding) {
        return util.binary.raw.encode(result);
      }
      if ("hex" === encoding) {
        return util.binary.hex.encode(result);
      }
      if ("base64" === encoding) {
        return util.binary.base64.encode(result);
      }
      if ("utf8" === encoding) {
        return util.text.utf8.decode(result);
      }
      if ("utf16" === encoding) {
        return util.text.utf16.decode(result);
      }
      throw new Error("Invalid encoding: " + encoding);
    };
    /**
     * @param {?} data
     * @param {string} encoding
     * @return {?}
     */
    util.createBuffer = function(data, encoding) {
      return encoding = encoding || "raw", void 0 !== data && "utf8" === encoding && (data = util.encodeUtf8(data)), new util.ByteBuffer(data);
    };
    /**
     * @param {string} n
     * @param {number} c
     * @return {?}
     */
    util.fillString = function(n, c) {
      /** @type {string} */
      var s = "";
      for (; c > 0;) {
        if (1 & c) {
          /** @type {string} */
          s = s + n;
        }
        /** @type {number} */
        c = c >>> 1;
        if (c > 0) {
          n = n + n;
        }
      }
      return s;
    };
    /**
     * @param {string} name
     * @param {string} array
     * @param {number} n
     * @return {?}
     */
    util.xorBytes = function(name, array, n) {
      /** @type {string} */
      var srcFormatted = "";
      /** @type {string} */
      var code = "";
      /** @type {string} */
      var line = "";
      /** @type {number} */
      var i = 0;
      /** @type {number} */
      var o = 0;
      for (; n > 0; --n, ++i) {
        /** @type {number} */
        code = name.charCodeAt(i) ^ array.charCodeAt(i);
        if (o >= 10) {
          /** @type {string} */
          srcFormatted = srcFormatted + line;
          /** @type {string} */
          line = "";
          /** @type {number} */
          o = 0;
        }
        /** @type {string} */
        line = line + String.fromCharCode(code);
        ++o;
      }
      return srcFormatted = srcFormatted + line;
    };
    /**
     * @param {string} str
     * @return {?}
     */
    util.hexToBytes = function(str) {
      /** @type {string} */
      var ret = "";
      /** @type {number} */
      var i = 0;
      if (str.length & true) {
        /** @type {number} */
        i = 1;
        /** @type {string} */
        ret = ret + String.fromCharCode(parseInt(str[0], 16));
      }
      for (; i < str.length; i = i + 2) {
        /** @type {string} */
        ret = ret + String.fromCharCode(parseInt(str.substr(i, 2), 16));
      }
      return ret;
    };
    /**
     * @param {?} key
     * @return {?}
     */
    util.bytesToHex = function(key) {
      return util.createBuffer(key).toHex();
    };
    /**
     * @param {number} i
     * @return {?}
     */
    util.int32ToBytes = function(i) {
      return String.fromCharCode(i >> 24 & 255) + String.fromCharCode(i >> 16 & 255) + String.fromCharCode(i >> 8 & 255) + String.fromCharCode(255 & i);
    };
    /** @type {string} */
    var hexDigits = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    /** @type {!Array} */
    var ref2 = [62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, 64, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51];
    /**
     * @param {string} data
     * @param {number} index
     * @return {?}
     */
    util.encode64 = function(data, index) {
      var dec;
      var aStatedRank;
      var a;
      /** @type {string} */
      var line = "";
      /** @type {string} */
      var srcFormatted = "";
      /** @type {number} */
      var i = 0;
      for (; i < data.length;) {
        dec = data.charCodeAt(i++);
        aStatedRank = data.charCodeAt(i++);
        a = data.charCodeAt(i++);
        /** @type {string} */
        line = line + hexDigits.charAt(dec >> 2);
        /** @type {string} */
        line = line + hexDigits.charAt((3 & dec) << 4 | aStatedRank >> 4);
        if (isNaN(aStatedRank)) {
          /** @type {string} */
          line = line + "==";
        } else {
          /** @type {string} */
          line = line + hexDigits.charAt((15 & aStatedRank) << 2 | a >> 6);
          /** @type {string} */
          line = line + (isNaN(a) ? "=" : hexDigits.charAt(63 & a));
        }
        if (index && line.length > index) {
          /** @type {string} */
          srcFormatted = srcFormatted + (line.substr(0, index) + "\r\n");
          /** @type {string} */
          line = line.substr(index);
        }
      }
      return srcFormatted = srcFormatted + line;
    };
    /**
     * @param {string} input
     * @return {?}
     */
    util.decode64 = function(input) {
      input = input.replace(/[^A-Za-z0-9\+\/=]/g, "");
      var v1;
      var v2;
      var proj_xend;
      var b1;
      /** @type {string} */
      var output = "";
      /** @type {number} */
      var i = 0;
      for (; i < input.length;) {
        v1 = ref2[input.charCodeAt(i++) - 43];
        v2 = ref2[input.charCodeAt(i++) - 43];
        proj_xend = ref2[input.charCodeAt(i++) - 43];
        b1 = ref2[input.charCodeAt(i++) - 43];
        /** @type {string} */
        output = output + String.fromCharCode(v1 << 2 | v2 >> 4);
        if (64 !== proj_xend) {
          /** @type {string} */
          output = output + String.fromCharCode((15 & v2) << 4 | proj_xend >> 2);
          if (64 !== b1) {
            /** @type {string} */
            output = output + String.fromCharCode((3 & proj_xend) << 6 | b1);
          }
        }
      }
      return output;
    };
    /**
     * @param {!Object} str
     * @return {?}
     */
    util.encodeUtf8 = function(str) {
      return unescape(encodeURIComponent(str));
    };
    /**
     * @param {string} bytes
     * @return {?}
     */
    util.decodeUtf8 = function(bytes) {
      return decodeURIComponent(escape(bytes));
    };
    util.binary = {
      raw : {},
      hex : {},
      base64 : {}
    };
    /**
     * @param {?} s
     * @return {?}
     */
    util.binary.raw.encode = function(s) {
      return String.fromCharCode.apply(null, s);
    };
    /**
     * @param {?} obj
     * @param {string} a
     * @param {string} p
     * @return {?}
     */
    util.binary.raw.decode = function(obj, a, p) {
      /** @type {string} */
      var b = a;
      if (!b) {
        /** @type {!Uint8Array} */
        b = new Uint8Array(obj.length);
      }
      p = p || 0;
      /** @type {string} */
      var j = p;
      /** @type {number} */
      var i = 0;
      for (; i < obj.length; ++i) {
        b[j++] = obj.charCodeAt(i);
      }
      return a ? j - p : b;
    };
    /** @type {function(?): ?} */
    util.binary.hex.encode = util.bytesToHex;
    /**
     * @param {?} obj
     * @param {string} a
     * @param {string} p
     * @return {?}
     */
    util.binary.hex.decode = function(obj, a, p) {
      /** @type {string} */
      var args = a;
      if (!args) {
        /** @type {!Uint8Array} */
        args = new Uint8Array(Math.ceil(obj.length / 2));
      }
      p = p || 0;
      /** @type {number} */
      var index = 0;
      /** @type {string} */
      var i = p;
      if (1 & obj.length) {
        /** @type {number} */
        index = 1;
        /** @type {number} */
        args[i++] = parseInt(obj[0], 16);
      }
      for (; index < obj.length; index = index + 2) {
        /** @type {number} */
        args[i++] = parseInt(obj.substr(index, 2), 16);
      }
      return a ? i - p : args;
    };
    /**
     * @param {?} s
     * @param {number} n
     * @return {?}
     */
    util.binary.base64.encode = function(s, n) {
      var dec;
      var firstChar;
      var a;
      /** @type {string} */
      var i = "";
      /** @type {string} */
      var sampleCopyPasteLine = "";
      /** @type {number} */
      var k = 0;
      for (; k < s.byteLength;) {
        dec = s[k++];
        firstChar = s[k++];
        a = s[k++];
        /** @type {string} */
        i = i + hexDigits.charAt(dec >> 2);
        /** @type {string} */
        i = i + hexDigits.charAt((3 & dec) << 4 | firstChar >> 4);
        if (isNaN(firstChar)) {
          /** @type {string} */
          i = i + "==";
        } else {
          /** @type {string} */
          i = i + hexDigits.charAt((15 & firstChar) << 2 | a >> 6);
          /** @type {string} */
          i = i + (isNaN(a) ? "=" : hexDigits.charAt(63 & a));
        }
        if (n && i.length > n) {
          /** @type {string} */
          sampleCopyPasteLine = sampleCopyPasteLine + (i.substr(0, n) + "\r\n");
          /** @type {string} */
          i = i.substr(n);
        }
      }
      return sampleCopyPasteLine = sampleCopyPasteLine + i;
    };
    /**
     * @param {?} str
     * @param {!Array} output
     * @param {string} offset
     * @return {?}
     */
    util.binary.base64.decode = function(str, output, offset) {
      /** @type {!Array} */
      var data = output;
      if (!data) {
        /** @type {!Uint8Array} */
        data = new Uint8Array(3 * Math.ceil(str.length / 4));
      }
      str = str.replace(/[^A-Za-z0-9\+\/=]/g, "");
      offset = offset || 0;
      var v1;
      var v2;
      var proj_xend;
      var m;
      /** @type {number} */
      var i = 0;
      /** @type {string} */
      var j = offset;
      for (; i < str.length;) {
        v1 = ref2[str.charCodeAt(i++) - 43];
        v2 = ref2[str.charCodeAt(i++) - 43];
        proj_xend = ref2[str.charCodeAt(i++) - 43];
        m = ref2[str.charCodeAt(i++) - 43];
        /** @type {number} */
        data[j++] = v1 << 2 | v2 >> 4;
        if (64 !== proj_xend) {
          /** @type {number} */
          data[j++] = (15 & v2) << 4 | proj_xend >> 2;
          if (64 !== m) {
            /** @type {number} */
            data[j++] = (3 & proj_xend) << 6 | m;
          }
        }
      }
      return output ? j - offset : data.subarray(0, j);
    };
    util.text = {
      utf8 : {},
      utf16 : {}
    };
    /**
     * @param {?} data
     * @param {number} r
     * @param {number} o
     * @return {?}
     */
    util.text.utf8.encode = function(data, r, o) {
      data = util.encodeUtf8(data);
      /** @type {number} */
      var result = r;
      if (!result) {
        /** @type {!Uint8Array} */
        result = new Uint8Array(data.length);
      }
      o = o || 0;
      /** @type {number} */
      var i = o;
      /** @type {number} */
      var index = 0;
      for (; index < data.length; ++index) {
        result[i++] = data.charCodeAt(index);
      }
      return r ? i - o : result;
    };
    /**
     * @param {?} val
     * @return {?}
     */
    util.text.utf8.decode = function(val) {
      return util.decodeUtf8(String.fromCharCode.apply(null, val));
    };
    /**
     * @param {?} s
     * @param {string} b
     * @param {number} x
     * @return {?}
     */
    util.text.utf16.encode = function(s, b, x) {
      /** @type {string} */
      var a = b;
      if (!a) {
        /** @type {!Uint8Array} */
        a = new Uint8Array(2 * s.length);
      }
      /** @type {!Uint16Array} */
      var indexes = new Uint16Array(a.buffer);
      x = x || 0;
      /** @type {number} */
      var value = x;
      /** @type {number} */
      var j = x;
      /** @type {number} */
      var i = 0;
      for (; i < s.length; ++i) {
        indexes[j++] = s.charCodeAt(i);
        value = value + 2;
      }
      return b ? value - x : a;
    };
    /**
     * @param {!Object} a
     * @return {?}
     */
    util.text.utf16.decode = function(a) {
      return String.fromCharCode.apply(null, new Uint16Array(a.buffer));
    };
    /**
     * @param {!Object} api
     * @param {string} bytes
     * @param {?} options
     * @return {?}
     */
    util.deflate = function(api, bytes, options) {
      if (bytes = util.decode64(api.deflate(util.encode64(bytes)).rval), options) {
        /** @type {number} */
        var start = 2;
        var n = bytes.charCodeAt(1);
        if (32 & n) {
          /** @type {number} */
          start = 6;
        }
        bytes = bytes.substring(start, bytes.length - 4);
      }
      return bytes;
    };
    /**
     * @param {!Object} api
     * @param {string} bytes
     * @param {?} size
     * @return {?}
     */
    util.inflate = function(api, bytes, size) {
      var rval = api.inflate(util.encode64(bytes)).rval;
      return null === rval ? null : util.decode64(rval);
    };
    /**
     * @param {!Object} api
     * @param {?} id
     * @param {!Object} obj
     * @return {undefined}
     */
    var _setStorageObject = function(api, id, obj) {
      if (!api) {
        throw new Error("WebStorage not available.");
      }
      var data;
      if (null === obj ? data = api.removeItem(id) : (obj = util.encode64(JSON.stringify(obj)), data = api.setItem(id, obj)), "undefined" != typeof data && data.rval !== true) {
        /** @type {!Error} */
        var ansNode = new Error(data.error.message);
        throw ansNode.id = data.error.id, ansNode.name = data.error.name, ansNode;
      }
    };
    /**
     * @param {!Object} api
     * @param {?} id
     * @return {?}
     */
    var _getStorageObject = function(api, id) {
      if (!api) {
        throw new Error("WebStorage not available.");
      }
      var rval = api.getItem(id);
      if (api.init) {
        if (null === rval.rval) {
          if (rval.error) {
            /** @type {!Error} */
            var a = new Error(rval.error.message);
            throw a.id = rval.error.id, a.name = rval.error.name, a;
          }
          /** @type {null} */
          rval = null;
        } else {
          rval = rval.rval;
        }
      }
      return null !== rval && (rval = JSON.parse(util.decode64(rval))), rval;
    };
    /**
     * @param {!Object} api
     * @param {?} id
     * @param {?} e
     * @param {?} key
     * @return {undefined}
     */
    var _removeItem = function(api, id, e, key) {
      var obj = _getStorageObject(api, id);
      if (null === obj) {
        obj = {};
      }
      obj[e] = key;
      _setStorageObject(api, id, obj);
    };
    /**
     * @param {!Object} api
     * @param {?} id
     * @param {?} key
     * @return {?}
     */
    var _getItem = function(api, id, key) {
      var rval = _getStorageObject(api, id);
      return null !== rval && (rval = key in rval ? rval[key] : null), rval;
    };
    /**
     * @param {!Object} api
     * @param {?} id
     * @param {?} k
     * @return {undefined}
     */
    var _setItem = function(api, id, k) {
      var obj = _getStorageObject(api, id);
      if (null !== obj && k in obj) {
        delete obj[k];
        /** @type {boolean} */
        var n = true;
        var key;
        for (key in obj) {
          /** @type {boolean} */
          n = false;
          break;
        }
        if (n) {
          /** @type {null} */
          obj = null;
        }
        _setStorageObject(api, id, obj);
      }
    };
    /**
     * @param {!Object} api
     * @param {?} id
     * @return {undefined}
     */
    var _clearItems = function(api, id) {
      _setStorageObject(api, id, null);
    };
    /**
     * @param {!Function} func
     * @param {!Object} args
     * @param {!Object} location
     * @return {?}
     */
    var _callStorageFunction = function(func, args, location) {
      /** @type {null} */
      var rval = null;
      if ("undefined" == typeof location) {
        /** @type {!Array} */
        location = ["web", "flash"];
      }
      var view;
      /** @type {boolean} */
      var viewIsInCorrectPosition = false;
      /** @type {null} */
      var s = null;
      var idx;
      for (idx in location) {
        view = location[idx];
        try {
          if ("flash" === view || "both" === view) {
            if (null === args[0]) {
              throw new Error("Flash local storage not available.");
            }
            rval = func.apply(this, args);
            /** @type {boolean} */
            viewIsInCorrectPosition = "flash" === view;
          }
          if (!("web" !== view && "both" !== view)) {
            args[0] = localStorage;
            rval = func.apply(this, args);
            /** @type {boolean} */
            viewIsInCorrectPosition = true;
          }
        } catch (seocounter_meta) {
          s = seocounter_meta;
        }
        if (viewIsInCorrectPosition) {
          break;
        }
      }
      if (!viewIsInCorrectPosition) {
        throw s;
      }
      return rval;
    };
    /**
     * @param {?} value
     * @param {!Object} key
     * @param {?} force
     * @param {?} url
     * @param {!Object} location
     * @return {undefined}
     */
    util.setItem = function(value, key, force, url, location) {
      _callStorageFunction(_removeItem, arguments, location);
    };
    /**
     * @param {?} index
     * @param {?} e
     * @param {?} currentRow
     * @param {!Object} location
     * @return {?}
     */
    util.getItem = function(index, e, currentRow, location) {
      return _callStorageFunction(_getItem, arguments, location);
    };
    /**
     * @param {?} index
     * @param {?} callback
     * @param {?} url
     * @param {!Object} location
     * @return {undefined}
     */
    util.removeItem = function(index, callback, url, location) {
      _callStorageFunction(_setItem, arguments, location);
    };
    /**
     * @param {?} id
     * @param {?} callback
     * @param {!Object} location
     * @return {undefined}
     */
    util.clearItems = function(id, callback, location) {
      _callStorageFunction(_clearItems, arguments, location);
    };
    /**
     * @param {boolean} str
     * @return {?}
     */
    util.parseUrl = function(str) {
      /** @type {!RegExp} */
      var ansi_re = /^(https?):\/\/([^:&^\/]*):?(\d*)(.*)$/g;
      /** @type {number} */
      ansi_re.lastIndex = 0;
      /** @type {(Array<string>|null)} */
      var match = ansi_re.exec(str);
      /** @type {(null|{full: ?, fullHost: string, host: string, path: string, port: (number|string), scheme: string})} */
      var url = null === match ? null : {
        full : str,
        scheme : match[1],
        host : match[2],
        port : match[3],
        path : match[4]
      };
      return url && (url.fullHost = url.host, url.port ? 80 !== url.port && "http" === url.scheme ? url.fullHost += ":" + url.port : 443 !== url.port && "https" === url.scheme && (url.fullHost += ":" + url.port) : "http" === url.scheme ? url.port = 80 : "https" === url.scheme && (url.port = 443), url.full = url.scheme + "://" + url.fullHost), url;
    };
    /** @type {null} */
    var options = null;
    /**
     * @param {string} name
     * @return {?}
     */
    util.getQueryVariables = function(name) {
      var argv;
      /**
       * @param {string} source
       * @return {?}
       */
      var parse = function(source) {
        var t = {};
        var kvpairs = source.split("&");
        /** @type {number} */
        var i = 0;
        for (; i < kvpairs.length; i++) {
          var key;
          var last;
          var space = kvpairs[i].indexOf("=");
          if (space > 0) {
            key = kvpairs[i].substring(0, space);
            last = kvpairs[i].substring(space + 1);
          } else {
            key = kvpairs[i];
            /** @type {null} */
            last = null;
          }
          if (!(key in t)) {
            /** @type {!Array} */
            t[key] = [];
          }
          if (!(key in Object.prototype || null === last)) {
            t[key].push(unescape(last));
          }
        }
        return t;
      };
      return "undefined" == typeof name ? (null === options && (options = "undefined" != typeof window && window.location && window.location.search ? parse(window.location.search.substring(1)) : {}), argv = options) : argv = parse(name), argv;
    };
    /**
     * @param {string} fragment
     * @return {?}
     */
    util.parseFragment = function(fragment) {
      /** @type {string} */
      var fp = fragment;
      /** @type {string} */
      var name = "";
      var pos = fragment.indexOf("?");
      if (pos > 0) {
        fp = fragment.substring(0, pos);
        name = fragment.substring(pos + 1);
      }
      var path = fp.split("/");
      if (path.length > 0 && "" === path[0]) {
        path.shift();
      }
      var childQuery = "" === name ? {} : util.getQueryVariables(name);
      return {
        pathString : fp,
        queryString : name,
        path : path,
        query : childQuery
      };
    };
    /**
     * @param {string} reqString
     * @return {?}
     */
    util.makeRequest = function(reqString) {
      var frag = util.parseFragment(reqString);
      var req = {
        path : frag.pathString,
        query : frag.queryString,
        getPath : function(index) {
          return "undefined" == typeof index ? frag.path : frag.path[index];
        },
        getQuery : function(k, i) {
          var rval;
          return "undefined" == typeof k ? rval = frag.query : (rval = frag.query[k], rval && "undefined" != typeof i && (rval = rval[i])), rval;
        },
        getQueryLast : function(k, _default) {
          var currentFavicon;
          var tag = req.getQuery(k);
          return currentFavicon = tag ? tag[tag.length - 1] : _default;
        }
      };
      return req;
    };
    /**
     * @param {string} url
     * @param {number} query
     * @param {!Object} fragment
     * @return {?}
     */
    util.makeLink = function(url, query, fragment) {
      url = jQuery.isArray(url) ? url.join("/") : url;
      var expRecords = jQuery.param(query || {});
      return fragment = fragment || "", url + (expRecords.length > 0 ? "?" + expRecords : "") + (fragment.length > 0 ? "#" + fragment : "");
    };
    /**
     * @param {number} val
     * @param {!Object} data
     * @param {?} type
     * @return {undefined}
     */
    util.setPath = function(val, data, type) {
      if ("object" == typeof val && null !== val) {
        /** @type {number} */
        var lastRowIdx = 0;
        var rowCount = data.length;
        for (; lastRowIdx < rowCount;) {
          var code = data[lastRowIdx++];
          if (lastRowIdx == rowCount) {
            val[code] = type;
          } else {
            /** @type {boolean} */
            var obj = code in val;
            if (!obj || obj && "object" != typeof val[code] || obj && null === val[code]) {
              val[code] = {};
            }
            val = val[code];
          }
        }
      }
    };
    /**
     * @param {string} val
     * @param {!Object} map
     * @param {string} root
     * @return {?}
     */
    util.getPath = function(val, map, root) {
      /** @type {number} */
      var k = 0;
      var l = map.length;
      /** @type {boolean} */
      var i = true;
      for (; i && k < l && "object" == typeof val && null !== val;) {
        var op = map[k++];
        /** @type {boolean} */
        i = op in val;
        if (i) {
          val = val[op];
        }
      }
      return i ? val : root;
    };
    /**
     * @param {number} a
     * @param {!Object} item
     * @return {undefined}
     */
    util.deletePath = function(a, item) {
      if ("object" == typeof a && null !== a) {
        /** @type {number} */
        var w = 0;
        var h = item.length;
        for (; w < h;) {
          var i = item[w++];
          if (w == h) {
            delete a[i];
          } else {
            if (!(i in a) || "object" != typeof a[i] || null === a[i]) {
              break;
            }
            a = a[i];
          }
        }
      }
    };
    /**
     * @param {(Object|string)} val
     * @return {?}
     */
    util.isEmpty = function(val) {
      var i;
      for (i in val) {
        if (val.hasOwnProperty(i)) {
          return false;
        }
      }
      return true;
    };
    /**
     * @param {string} template
     * @return {?}
     */
    util.format = function(template) {
      var individualMD5;
      var r;
      /** @type {!RegExp} */
      var m = /%./g;
      /** @type {number} */
      var __$0 = 0;
      /** @type {!Array} */
      var s = [];
      /** @type {number} */
      var i = 0;
      for (; individualMD5 = m.exec(template);) {
        r = template.substring(i, m.lastIndex - 2);
        if (r.length > 0) {
          s.push(r);
        }
        /** @type {number} */
        i = m.lastIndex;
        var code = individualMD5[0][1];
        switch(code) {
          case "s":
          case "o":
            if (__$0 < arguments.length) {
              s.push(arguments[__$0++ + 1]);
            } else {
              s.push("<?>");
            }
            break;
          case "%":
            s.push("%");
            break;
          default:
            s.push("<%" + code + "?>");
        }
      }
      return s.push(template.substring(i)), s.join("");
    };
    /**
     * @param {number} count
     * @param {number} num
     * @param {?} prefix
     * @param {string} value
     * @return {?}
     */
    util.formatNumber = function(count, num, prefix, value) {
      /** @type {number} */
      var n = count;
      /** @type {number} */
      var length = isNaN(num = Math.abs(num)) ? 2 : num;
      var offset = void 0 === prefix ? "," : prefix;
      var t = void 0 === value ? "." : value;
      /** @type {string} */
      var sign = n < 0 ? "-" : "";
      /** @type {string} */
      var intPart = parseInt(n = Math.abs(+n || 0).toFixed(length), 10) + "";
      /** @type {number} */
      var j = intPart.length > 3 ? intPart.length % 3 : 0;
      return sign + (j ? intPart.substr(0, j) + t : "") + intPart.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (length ? offset + Math.abs(n - intPart).toFixed(length).slice(2) : "");
    };
    /**
     * @param {number} size
     * @return {?}
     */
    util.formatSize = function(size) {
      return size = size >= 1073741824 ? util.formatNumber(size / 1073741824, 2, ".", "") + " GiB" : size >= 1048576 ? util.formatNumber(size / 1048576, 2, ".", "") + " MiB" : size >= 1024 ? util.formatNumber(size / 1024, 0) + " KiB" : util.formatNumber(size, 0) + " bytes";
    };
    /**
     * @param {string} ip
     * @return {?}
     */
    util.bytesFromIP = function(ip) {
      return ip.indexOf(".") !== -1 ? util.bytesFromIPv4(ip) : ip.indexOf(":") !== -1 ? util.bytesFromIPv6(ip) : null;
    };
    /**
     * @param {string} ip
     * @return {?}
     */
    util.bytesFromIPv4 = function(ip) {
      if (ip = ip.split("."), 4 !== ip.length) {
        return null;
      }
      var b = util.createBuffer();
      /** @type {number} */
      var i = 0;
      for (; i < ip.length; ++i) {
        /** @type {number} */
        var num = parseInt(ip[i], 10);
        if (isNaN(num)) {
          return null;
        }
        b.putByte(num);
      }
      return b.getBytes();
    };
    /**
     * @param {string} ip
     * @return {?}
     */
    util.bytesFromIPv6 = function(ip) {
      /** @type {number} */
      var t = 0;
      ip = ip.split(":").filter(function(inRevIdx) {
        return 0 === inRevIdx.length && ++t, true;
      });
      /** @type {number} */
      var zeros = 2 * (8 - ip.length + t);
      var b = util.createBuffer();
      /** @type {number} */
      var i = 0;
      for (; i < 8; ++i) {
        if (ip[i] && 0 !== ip[i].length) {
          var salt = util.hexToBytes(ip[i]);
          if (salt.length < 2) {
            b.putByte(0);
          }
          b.putBytes(salt);
        } else {
          b.fillWithByte(0, zeros);
          /** @type {number} */
          zeros = 0;
        }
      }
      return b.getBytes();
    };
    /**
     * @param {string} bytes
     * @return {?}
     */
    util.bytesToIP = function(bytes) {
      return 4 === bytes.length ? util.bytesToIPv4(bytes) : 16 === bytes.length ? util.bytesToIPv6(bytes) : null;
    };
    /**
     * @param {string} bytes
     * @return {?}
     */
    util.bytesToIPv4 = function(bytes) {
      if (4 !== bytes.length) {
        return null;
      }
      /** @type {!Array} */
      var UNICODE_SPACES = [];
      /** @type {number} */
      var i = 0;
      for (; i < bytes.length; ++i) {
        UNICODE_SPACES.push(bytes.charCodeAt(i));
      }
      return UNICODE_SPACES.join(".");
    };
    /**
     * @param {string} bytes
     * @return {?}
     */
    util.bytesToIPv6 = function(bytes) {
      if (16 !== bytes.length) {
        return null;
      }
      /** @type {!Array} */
      var t = [];
      /** @type {!Array} */
      var results = [];
      /** @type {number} */
      var i = 0;
      /** @type {number} */
      var pos = 0;
      for (; pos < bytes.length; pos = pos + 2) {
        var i = util.bytesToHex(bytes[pos] + bytes[pos + 1]);
        for (; "0" === i[0] && "0" !== i;) {
          i = i.substr(1);
        }
        if ("0" === i) {
          var result = results[results.length - 1];
          /** @type {number} */
          var index = t.length;
          if (result && index === result.end + 1) {
            result.end = index;
            if (result.end - result.start > results[i].end - results[i].start) {
              /** @type {number} */
              i = results.length - 1;
            }
          } else {
            results.push({
              start : index,
              end : index
            });
          }
        }
        t.push(i);
      }
      if (results.length > 0) {
        var node = results[i];
        if (node.end - node.start > 0) {
          t.splice(node.start, node.end - node.start + 1, "");
          if (0 === node.start) {
            t.unshift("");
          }
          if (7 === node.end) {
            t.push("");
          }
        }
      }
      return t.join(":");
    };
    /**
     * @param {string} options
     * @param {string} callback
     * @return {?}
     */
    util.estimateCores = function(options, callback) {
      /**
       * @param {!Array} max
       * @param {number} samples
       * @param {number} numWorkers
       * @return {?}
       */
      function sample(max, samples, numWorkers) {
        if (0 === samples) {
          /** @type {number} */
          var avg = Math.floor(max.reduce(function(buckets, name) {
            return buckets + name;
          }, 0) / max.length);
          return util.cores = Math.max(1, avg), URL.revokeObjectURL(blob), callback(null, util.cores);
        }
        map(numWorkers, function(canCreateDiscussions, results) {
          max.push(reduce(numWorkers, results));
          sample(max, samples - 1, numWorkers);
        });
      }
      /**
       * @param {number} numWorkers
       * @param {!Function} callback
       * @return {undefined}
       */
      function map(numWorkers, callback) {
        /** @type {!Array} */
        var workers = [];
        /** @type {!Array} */
        var results = [];
        /** @type {number} */
        var i = 0;
        for (; i < numWorkers; ++i) {
          /** @type {!Worker} */
          var worker = new Worker(blob);
          worker.addEventListener("message", function(card) {
            if (results.push(card.data), results.length === numWorkers) {
              /** @type {number} */
              var i = 0;
              for (; i < numWorkers; ++i) {
                workers[i].terminate();
              }
              callback(null, results);
            }
          });
          workers.push(worker);
        }
        /** @type {number} */
        i = 0;
        for (; i < numWorkers; ++i) {
          workers[i].postMessage(i);
        }
      }
      /**
       * @param {number} numWorkers
       * @param {!NodeList} results
       * @return {?}
       */
      function reduce(numWorkers, results) {
        /** @type {!Array} */
        var collectionRels = [];
        /** @type {number} */
        var i = 0;
        for (; i < numWorkers; ++i) {
          var r2 = results[i];
          /** @type {!Array} */
          var colorDist = collectionRels[i] = [];
          /** @type {number} */
          var n = 0;
          for (; n < numWorkers; ++n) {
            if (i !== n) {
              var r1 = results[n];
              if (r2.st > r1.st && r2.st < r1.et || r1.st > r2.st && r1.st < r2.et) {
                colorDist.push(n);
              }
            }
          }
        }
        return collectionRels.reduce(function(e, comboActions) {
          return Math.max(e, comboActions.length);
        }, 0);
      }
      if ("function" == typeof options && (callback = options, options = {}), options = options || {}, "cores" in util && !options.update) {
        return callback(null, util.cores);
      }
      if ("undefined" != typeof navigator && "hardwareConcurrency" in navigator && navigator.hardwareConcurrency > 0) {
        return util.cores = navigator.hardwareConcurrency, callback(null, util.cores);
      }
      if ("undefined" == typeof Worker) {
        return util.cores = 1, callback(null, util.cores);
      }
      if ("undefined" == typeof Blob) {
        return util.cores = 2, callback(null, util.cores);
      }
      /** @type {string} */
      var blob = URL.createObjectURL(new Blob(["(", function() {
        self.addEventListener("message", function(canCreateDiscussions) {
          /** @type {number} */
          var st = Date.now();
          /** @type {number} */
          var et = st + 4;
          for (; Date.now() < et;) {
          }
          self.postMessage({
            st : st,
            et : et
          });
        });
      }.toString(), ")()"], {
        type : "application/javascript"
      }));
      sample([], 5, 16);
    };
  }, function(module, canCreateDiscussions, FORGE) {
    var forge = FORGE(0);
    FORGE(5);
    FORGE(30);
    FORGE(28);
    FORGE(1);
    (function() {
      return forge.random && forge.random.getBytes ? void(module.exports = forge.random) : void function($) {
        /**
         * @return {?}
         */
        function spawnPrng() {
          var stream = forge.prng.create(self);
          return stream.getBytes = function(n, callback) {
            return stream.generate(n, callback);
          }, stream.getBytesSync = function(count) {
            return stream.generate(count);
          }, stream;
        }
        var self = {};
        /** @type {!Array} */
        var output = new Array(4);
        var out = forge.util.createBuffer();
        /**
         * @param {!Array} key
         * @return {?}
         */
        self.formatKey = function(key) {
          var tmp = forge.util.createBuffer(key);
          return key = new Array(4), key[0] = tmp.getInt32(), key[1] = tmp.getInt32(), key[2] = tmp.getInt32(), key[3] = tmp.getInt32(), forge.aes._expandKey(key, false);
        };
        /**
         * @param {!Array} seed
         * @return {?}
         */
        self.formatSeed = function(seed) {
          var tmp = forge.util.createBuffer(seed);
          return seed = new Array(4), seed[0] = tmp.getInt32(), seed[1] = tmp.getInt32(), seed[2] = tmp.getInt32(), seed[3] = tmp.getInt32(), seed;
        };
        /**
         * @param {string} w
         * @param {!Array} data
         * @return {?}
         */
        self.cipher = function(w, data) {
          return forge.aes._updateBlock(w, data, output, false), out.putInt32(output[0]), out.putInt32(output[1]), out.putInt32(output[2]), out.putInt32(output[3]), out.getBytes();
        };
        /**
         * @param {!Array} a
         * @return {?}
         */
        self.increment = function(a) {
          return ++a[3], a;
        };
        self.md = forge.md.sha256;
        var _ctx = spawnPrng();
        /** @type {null} */
        var getRandomValues = null;
        if ("undefined" != typeof window) {
          var _crypto = window.crypto || window.msCrypto;
          if (_crypto && _crypto.getRandomValues) {
            /**
             * @param {?} bytes
             * @return {?}
             */
            getRandomValues = function(bytes) {
              return _crypto.getRandomValues(bytes);
            };
          }
        }
        if (forge.options.usePureJavaScript || !forge.util.isNodejs && !getRandomValues) {
          if ("undefined" == typeof window || void 0 === window.document, _ctx.collectInt(+new Date, 32), "undefined" != typeof navigator) {
            /** @type {string} */
            var _navBytes = "";
            var key;
            for (key in navigator) {
              try {
                if ("string" == typeof navigator[key]) {
                  _navBytes = _navBytes + navigator[key];
                }
              } catch (e) {
              }
            }
            _ctx.collect(_navBytes);
            /** @type {null} */
            _navBytes = null;
          }
          if ($) {
            $().mousemove(function(e) {
              _ctx.collectInt(e.clientX, 16);
              _ctx.collectInt(e.clientY, 16);
            });
            $().keypress(function(e) {
              _ctx.collectInt(e.charCode, 8);
            });
          }
        }
        if (forge.random) {
          for (key in _ctx) {
            forge.random[key] = _ctx[key];
          }
        } else {
          forge.random = _ctx;
        }
        /** @type {function(): ?} */
        forge.random.createInstance = spawnPrng;
        module.exports = forge.random;
      }("undefined" != typeof jQuery ? jQuery : null);
    })();
  }, function(mixin, canCreateDiscussions, FORGE) {
    /**
     * @param {!Object} bytes
     * @param {number} remaining
     * @param {number} n
     * @return {undefined}
     */
    function _checkBufferLength(bytes, remaining, n) {
      if (n > remaining) {
        /** @type {!Error} */
        var error = new Error("Too few bytes to parse DER.");
        throw error.available = bytes.length(), error.remaining = remaining, error.requested = n, error;
      }
    }
    /**
     * @param {!Object} bytes
     * @param {number} remaining
     * @param {number} depth
     * @param {!Object} options
     * @return {?}
     */
    function create(bytes, remaining, depth, options) {
      var start;
      _checkBufferLength(bytes, remaining, 2);
      var MSG_TYPE_MASK = bytes.getByte();
      remaining--;
      /** @type {number} */
      var params = 192 & MSG_TYPE_MASK;
      /** @type {number} */
      var type = 31 & MSG_TYPE_MASK;
      start = bytes.length();
      var length = _getValueLength(bytes, remaining);
      if (remaining = remaining - (start - bytes.length()), void 0 !== length && length > remaining) {
        if (options.strict) {
          /** @type {!Error} */
          var error = new Error("Too few bytes to read ASN.1 value.");
          throw error.available = bytes.length(), error.remaining = remaining, error.requested = length, error;
        }
        /** @type {number} */
        length = remaining;
      }
      var value;
      var bitStringContents;
      /** @type {boolean} */
      var constructed = 32 === (32 & MSG_TYPE_MASK);
      if (constructed) {
        if (value = [], void 0 === length) {
          for (;;) {
            if (_checkBufferLength(bytes, remaining, 2), bytes.bytes(2) === String.fromCharCode(0, 0)) {
              bytes.getBytes(2);
              /** @type {number} */
              remaining = remaining - 2;
              break;
            }
            start = bytes.length();
            value.push(create(bytes, remaining, depth + 1, options));
            /** @type {number} */
            remaining = remaining - (start - bytes.length());
          }
        } else {
          for (; length > 0;) {
            start = bytes.length();
            value.push(create(bytes, length, depth + 1, options));
            /** @type {number} */
            remaining = remaining - (start - bytes.length());
            /** @type {number} */
            length = length - (start - bytes.length());
          }
        }
      }
      if (void 0 === value && params === asn1.Class.UNIVERSAL && type === asn1.Type.BITSTRING && (bitStringContents = bytes.bytes(length)), void 0 === value && options.decodeBitStrings && params === asn1.Class.UNIVERSAL && type === asn1.Type.BITSTRING && length > 1) {
        var savedRead = bytes.read;
        /** @type {number} */
        var savedRemaining = remaining;
        /** @type {number} */
        var abort = 0;
        if (type === asn1.Type.BITSTRING && (_checkBufferLength(bytes, remaining, 1), abort = bytes.getByte(), remaining--), 0 === abort) {
          try {
            start = bytes.length();
            var subOptions = {
              verbose : options.verbose,
              strict : true,
              decodeBitStrings : true
            };
            var composed = create(bytes, remaining, depth + 1, subOptions);
            /** @type {number} */
            var used = start - bytes.length();
            /** @type {number} */
            remaining = remaining - used;
            if (type == asn1.Type.BITSTRING) {
              used++;
            }
            var tc = composed.tagClass;
            if (!(used !== length || tc !== asn1.Class.UNIVERSAL && tc !== asn1.Class.CONTEXT_SPECIFIC)) {
              /** @type {!Array} */
              value = [composed];
            }
          } catch (e) {
          }
        }
        if (void 0 === value) {
          bytes.read = savedRead;
          remaining = savedRemaining;
        }
      }
      if (void 0 === value) {
        if (void 0 === length) {
          if (options.strict) {
            throw new Error("Non-constructed ASN.1 object of indefinite length.");
          }
          /** @type {number} */
          length = remaining;
        }
        if (type === asn1.Type.BMPSTRING) {
          /** @type {string} */
          value = "";
          for (; length > 0; length = length - 2) {
            _checkBufferLength(bytes, remaining, 2);
            /** @type {string} */
            value = value + String.fromCharCode(bytes.getInt16());
            /** @type {number} */
            remaining = remaining - 2;
          }
        } else {
          value = bytes.getBytes(length);
        }
      }
      /** @type {(null|{bitStringContents: ?})} */
      var asn1Options = void 0 === bitStringContents ? null : {
        bitStringContents : bitStringContents
      };
      return asn1.create(params, type, constructed, value, asn1Options);
    }
    var forge = FORGE(0);
    FORGE(1);
    FORGE(6);
    var asn1 = mixin.exports = forge.asn1 = forge.asn1 || {};
    asn1.Class = {
      UNIVERSAL : 0,
      APPLICATION : 64,
      CONTEXT_SPECIFIC : 128,
      PRIVATE : 192
    };
    asn1.Type = {
      NONE : 0,
      BOOLEAN : 1,
      INTEGER : 2,
      BITSTRING : 3,
      OCTETSTRING : 4,
      NULL : 5,
      OID : 6,
      ODESC : 7,
      EXTERNAL : 8,
      REAL : 9,
      ENUMERATED : 10,
      EMBEDDED : 11,
      UTF8 : 12,
      ROID : 13,
      SEQUENCE : 16,
      SET : 17,
      PRINTABLESTRING : 19,
      IA5STRING : 22,
      UTCTIME : 23,
      GENERALIZEDTIME : 24,
      BMPSTRING : 30
    };
    /**
     * @param {!Object} algorithm
     * @param {string} name
     * @param {string} err
     * @param {string} data
     * @param {!Node} options
     * @return {?}
     */
    asn1.create = function(algorithm, name, err, data, options) {
      if (forge.util.isArray(data)) {
        /** @type {!Array} */
        var filteredData = [];
        /** @type {number} */
        var i = 0;
        for (; i < data.length; ++i) {
          if (void 0 !== data[i]) {
            filteredData.push(data[i]);
          }
        }
        /** @type {!Array} */
        data = filteredData;
      }
      var obj = {
        tagClass : algorithm,
        type : name,
        constructed : err,
        composed : err || forge.util.isArray(data),
        value : data
      };
      return options && "bitStringContents" in options && (obj.bitStringContents = options.bitStringContents, obj.original = asn1.copy(obj)), obj;
    };
    /**
     * @param {string} obj
     * @param {boolean} options
     * @return {?}
     */
    asn1.copy = function(obj, options) {
      var copy;
      if (forge.util.isArray(obj)) {
        /** @type {!Array} */
        copy = [];
        /** @type {number} */
        var i = 0;
        for (; i < obj.length; ++i) {
          copy.push(asn1.copy(obj[i], options));
        }
        return copy;
      }
      return "string" == typeof obj ? obj : (copy = {
        tagClass : obj.tagClass,
        type : obj.type,
        constructed : obj.constructed,
        composed : obj.composed,
        value : asn1.copy(obj.value, options)
      }, options && !options.excludeBitStringContents && (copy.bitStringContents = obj.bitStringContents), copy);
    };
    /**
     * @param {string} obj1
     * @param {string} obj2
     * @param {?} options
     * @return {?}
     */
    asn1.equals = function(obj1, obj2, options) {
      if (forge.util.isArray(obj1)) {
        if (!forge.util.isArray(obj2)) {
          return false;
        }
        if (obj1.length !== obj2.length) {
          return false;
        }
        /** @type {number} */
        var i = 0;
        for (; i < obj1.length; ++i) {
          return !!asn1.equals(obj1[i], obj2[i]);
        }
      }
      if (typeof obj1 != typeof obj2) {
        return false;
      }
      if ("string" == typeof obj1) {
        return obj1 === obj2;
      }
      var qs = obj1.tagClass === obj2.tagClass && obj1.type === obj2.type && obj1.constructed === obj2.constructed && obj1.composed === obj2.composed && asn1.equals(obj1.value, obj2.value);
      return options && options.includeBitStringContents && (qs = qs && obj1.bitStringContents === obj2.bitStringContents), qs;
    };
    /**
     * @param {?} b
     * @return {?}
     */
    asn1.getBerValueLength = function(b) {
      var block = b.getByte();
      if (128 !== block) {
        var bravePasswordManagerSetting;
        /** @type {number} */
        var useBuiltIn = 128 & block;
        return bravePasswordManagerSetting = useBuiltIn ? b.getInt((127 & block) << 3) : block;
      }
    };
    /**
     * @param {!Object} bytes
     * @param {number} remaining
     * @return {?}
     */
    var _getValueLength = function(bytes, remaining) {
      var val = bytes.getByte();
      if (remaining--, 128 !== val) {
        var length;
        /** @type {number} */
        var new_lo = 128 & val;
        if (new_lo) {
          /** @type {number} */
          var longFormBytes = 127 & val;
          _checkBufferLength(bytes, remaining, longFormBytes);
          length = bytes.getInt(longFormBytes << 3);
        } else {
          length = val;
        }
        if (length < 0) {
          throw new Error("Negative length: " + length);
        }
        return length;
      }
    };
    /**
     * @param {!Object} data
     * @param {!Object} options
     * @return {?}
     */
    asn1.fromDer = function(data, options) {
      return void 0 === options && (options = {
        strict : true,
        decodeBitStrings : true
      }), "boolean" == typeof options && (options = {
        strict : options,
        decodeBitStrings : true
      }), "strict" in options || (options.strict = true), "decodeBitStrings" in options || (options.decodeBitStrings = true), "string" == typeof data && (data = forge.util.createBuffer(data)), create(data, data.length(), 0, options);
    };
    /**
     * @param {string} obj
     * @return {?}
     */
    asn1.toDer = function(obj) {
      var bytes = forge.util.createBuffer();
      /** @type {number} */
      var b1 = obj.tagClass | obj.type;
      var value = forge.util.createBuffer();
      /** @type {boolean} */
      var tmpEvtType = false;
      if ("bitStringContents" in obj && (tmpEvtType = true, obj.original && (tmpEvtType = asn1.equals(obj, obj.original))), tmpEvtType) {
        value.putBytes(obj.bitStringContents);
      } else {
        if (obj.composed) {
          if (obj.constructed) {
            /** @type {number} */
            b1 = b1 | 32;
          } else {
            value.putByte(0);
          }
          /** @type {number} */
          var i = 0;
          for (; i < obj.value.length; ++i) {
            if (void 0 !== obj.value[i]) {
              value.putBuffer(asn1.toDer(obj.value[i]));
            }
          }
        } else {
          if (obj.type === asn1.Type.BMPSTRING) {
            /** @type {number} */
            i = 0;
            for (; i < obj.value.length; ++i) {
              value.putInt16(obj.value.charCodeAt(i));
            }
          } else {
            if (obj.type === asn1.Type.INTEGER && obj.value.length > 1 && (0 === obj.value.charCodeAt(0) && 0 === (128 & obj.value.charCodeAt(1)) || 255 === obj.value.charCodeAt(0) && 128 === (128 & obj.value.charCodeAt(1)))) {
              value.putBytes(obj.value.substr(1));
            } else {
              value.putBytes(obj.value);
            }
          }
        }
      }
      if (bytes.putByte(b1), value.length() <= 127) {
        bytes.putByte(127 & value.length());
      } else {
        var mask = value.length();
        /** @type {string} */
        var b = "";
        do {
          /** @type {string} */
          b = b + String.fromCharCode(255 & mask);
          /** @type {number} */
          mask = mask >>> 8;
        } while (mask > 0);
        bytes.putByte(128 | b.length);
        /** @type {number} */
        i = b.length - 1;
        for (; i >= 0; --i) {
          bytes.putByte(b.charCodeAt(i));
        }
      }
      return bytes.putBuffer(value), bytes;
    };
    /**
     * @param {string} replacement
     * @return {?}
     */
    asn1.oidToDer = function(replacement) {
      var t = replacement.split(".");
      var bytes = forge.util.createBuffer();
      bytes.putByte(40 * parseInt(t[0], 10) + parseInt(t[1], 10));
      var a;
      var valueBytes;
      var left;
      var i;
      /** @type {number} */
      var k = 2;
      for (; k < t.length; ++k) {
        /** @type {boolean} */
        a = true;
        /** @type {!Array} */
        valueBytes = [];
        /** @type {number} */
        left = parseInt(t[k], 10);
        do {
          /** @type {number} */
          i = 127 & left;
          /** @type {number} */
          left = left >>> 7;
          if (!a) {
            /** @type {number} */
            i = i | 128;
          }
          valueBytes.push(i);
          /** @type {boolean} */
          a = false;
        } while (left > 0);
        /** @type {number} */
        var n = valueBytes.length - 1;
        for (; n >= 0; --n) {
          bytes.putByte(valueBytes[n]);
        }
      }
      return bytes;
    };
    /**
     * @param {?} bytes
     * @return {?}
     */
    asn1.derToOid = function(bytes) {
      var oid;
      if ("string" == typeof bytes) {
        bytes = forge.util.createBuffer(bytes);
      }
      var index = bytes.getByte();
      /** @type {string} */
      oid = Math.floor(index / 40) + "." + index % 40;
      /** @type {number} */
      var controlsCount = 0;
      for (; bytes.length() > 0;) {
        index = bytes.getByte();
        /** @type {number} */
        controlsCount = controlsCount << 7;
        if (128 & index) {
          /** @type {number} */
          controlsCount = controlsCount + (127 & index);
        } else {
          /** @type {string} */
          oid = oid + ("." + (controlsCount + index));
          /** @type {number} */
          controlsCount = 0;
        }
      }
      return oid;
    };
    /**
     * @param {string} utc
     * @return {?}
     */
    asn1.utcTimeToDate = function(utc) {
      /** @type {!Date} */
      var date = new Date;
      /** @type {number} */
      var year = parseInt(utc.substr(0, 2), 10);
      /** @type {number} */
      year = year >= 50 ? 1900 + year : 2e3 + year;
      /** @type {number} */
      var MM = parseInt(utc.substr(2, 2), 10) - 1;
      /** @type {number} */
      var DD = parseInt(utc.substr(4, 2), 10);
      /** @type {number} */
      var hh = parseInt(utc.substr(6, 2), 10);
      /** @type {number} */
      var mm = parseInt(utc.substr(8, 2), 10);
      /** @type {number} */
      var ss = 0;
      if (utc.length > 11) {
        var ch = utc.charAt(10);
        /** @type {number} */
        var end = 10;
        if ("+" !== ch && "-" !== ch) {
          /** @type {number} */
          ss = parseInt(utc.substr(10, 2), 10);
          /** @type {number} */
          end = end + 2;
        }
      }
      if (date.setUTCFullYear(year, MM, DD), date.setUTCHours(hh, mm, ss, 0), end && (ch = utc.charAt(end), "+" === ch || "-" === ch)) {
        /** @type {number} */
        var N = parseInt(utc.substr(end + 1, 2), 10);
        /** @type {number} */
        var i = parseInt(utc.substr(end + 4, 2), 10);
        /** @type {number} */
        var offset = 60 * N + i;
        /** @type {number} */
        offset = offset * 6e4;
        if ("+" === ch) {
          date.setTime(+date - offset);
        } else {
          date.setTime(+date + offset);
        }
      }
      return date;
    };
    /**
     * @param {string} gentime
     * @return {?}
     */
    asn1.generalizedTimeToDate = function(gentime) {
      /** @type {!Date} */
      var date = new Date;
      /** @type {number} */
      var year = parseInt(gentime.substr(0, 4), 10);
      /** @type {number} */
      var MM = parseInt(gentime.substr(4, 2), 10) - 1;
      /** @type {number} */
      var DD = parseInt(gentime.substr(6, 2), 10);
      /** @type {number} */
      var hh = parseInt(gentime.substr(8, 2), 10);
      /** @type {number} */
      var mm = parseInt(gentime.substr(10, 2), 10);
      /** @type {number} */
      var ss = parseInt(gentime.substr(12, 2), 10);
      /** @type {number} */
      var fff = 0;
      /** @type {number} */
      var offset = 0;
      /** @type {boolean} */
      var l = false;
      if ("Z" === gentime.charAt(gentime.length - 1)) {
        /** @type {boolean} */
        l = true;
      }
      /** @type {number} */
      var end = gentime.length - 5;
      var initch = gentime.charAt(end);
      if ("+" === initch || "-" === initch) {
        /** @type {number} */
        var index = parseInt(gentime.substr(end + 1, 2), 10);
        /** @type {number} */
        var j = parseInt(gentime.substr(end + 4, 2), 10);
        /** @type {number} */
        offset = 60 * index + j;
        /** @type {number} */
        offset = offset * 6e4;
        if ("+" === initch) {
          /** @type {number} */
          offset = offset * -1;
        }
        /** @type {boolean} */
        l = true;
      }
      return "." === gentime.charAt(14) && (fff = 1e3 * parseFloat(gentime.substr(14), 10)), l ? (date.setUTCFullYear(year, MM, DD), date.setUTCHours(hh, mm, ss, fff), date.setTime(+date + offset)) : (date.setFullYear(year, MM, DD), date.setHours(hh, mm, ss, fff)), date;
    };
    /**
     * @param {!Date} date
     * @return {?}
     */
    asn1.dateToUtcTime = function(date) {
      if ("string" == typeof date) {
        return date;
      }
      /** @type {string} */
      var rval = "";
      /** @type {!Array} */
      var format = [];
      format.push(("" + date.getUTCFullYear()).substr(2));
      format.push("" + (date.getUTCMonth() + 1));
      format.push("" + date.getUTCDate());
      format.push("" + date.getUTCHours());
      format.push("" + date.getUTCMinutes());
      format.push("" + date.getUTCSeconds());
      /** @type {number} */
      var i = 0;
      for (; i < format.length; ++i) {
        if (format[i].length < 2) {
          /** @type {string} */
          rval = rval + "0";
        }
        /** @type {string} */
        rval = rval + format[i];
      }
      return rval = rval + "Z";
    };
    /**
     * @param {!Date} date
     * @return {?}
     */
    asn1.dateToGeneralizedTime = function(date) {
      if ("string" == typeof date) {
        return date;
      }
      /** @type {string} */
      var rval = "";
      /** @type {!Array} */
      var format = [];
      format.push("" + date.getUTCFullYear());
      format.push("" + (date.getUTCMonth() + 1));
      format.push("" + date.getUTCDate());
      format.push("" + date.getUTCHours());
      format.push("" + date.getUTCMinutes());
      format.push("" + date.getUTCSeconds());
      /** @type {number} */
      var i = 0;
      for (; i < format.length; ++i) {
        if (format[i].length < 2) {
          /** @type {string} */
          rval = rval + "0";
        }
        /** @type {string} */
        rval = rval + format[i];
      }
      return rval = rval + "Z";
    };
    /**
     * @param {number} x
     * @return {?}
     */
    asn1.integerToDer = function(x) {
      var rval = forge.util.createBuffer();
      if (x >= -128 && x < 128) {
        return rval.putSignedInt(x, 8);
      }
      if (x >= -32768 && x < 32768) {
        return rval.putSignedInt(x, 16);
      }
      if (x >= -8388608 && x < 8388608) {
        return rval.putSignedInt(x, 24);
      }
      if (x >= -2147483648 && x < 2147483648) {
        return rval.putSignedInt(x, 32);
      }
      /** @type {!Error} */
      var error = new Error("Integer too large; max is 32-bits.");
      throw error.integer = x, error;
    };
    /**
     * @param {!Array} bytes
     * @return {?}
     */
    asn1.derToInteger = function(bytes) {
      if ("string" == typeof bytes) {
        bytes = forge.util.createBuffer(bytes);
      }
      /** @type {number} */
      var n = 8 * bytes.length();
      if (n > 32) {
        throw new Error("Integer too large; max is 32-bits.");
      }
      return bytes.getSignedInt(n);
    };
    /**
     * @param {string} obj
     * @param {string} v
     * @param {string} capture
     * @param {!Array} errors
     * @return {?}
     */
    asn1.validate = function(obj, v, capture, errors) {
      /** @type {boolean} */
      var rval = false;
      if (obj.tagClass !== v.tagClass && "undefined" != typeof v.tagClass || obj.type !== v.type && "undefined" != typeof v.type) {
        if (errors) {
          if (obj.tagClass !== v.tagClass) {
            errors.push("[" + v.name + '] Expected tag class "' + v.tagClass + '", got "' + obj.tagClass + '"');
          }
          if (obj.type !== v.type) {
            errors.push("[" + v.name + '] Expected type "' + v.type + '", got "' + obj.type + '"');
          }
        }
      } else {
        if (obj.constructed === v.constructed || "undefined" == typeof v.constructed) {
          if (rval = true, v.value && forge.util.isArray(v.value)) {
            /** @type {number} */
            var j = 0;
            /** @type {number} */
            var i = 0;
            for (; rval && i < v.value.length; ++i) {
              rval = v.value[i].optional || false;
              if (obj.value[j]) {
                rval = asn1.validate(obj.value[j], v.value[i], capture, errors);
                if (rval) {
                  ++j;
                } else {
                  if (v.value[i].optional) {
                    /** @type {boolean} */
                    rval = true;
                  }
                }
              }
              if (!rval && errors) {
                errors.push("[" + v.name + '] Tag class "' + v.tagClass + '", type "' + v.type + '" expected value length "' + v.value.length + '", got "' + obj.value.length + '"');
              }
            }
          }
          if (rval && capture && (v.capture && (capture[v.capture] = obj.value), v.captureAsn1 && (capture[v.captureAsn1] = obj), v.captureBitStringContents && "bitStringContents" in obj && (capture[v.captureBitStringContents] = obj.bitStringContents), v.captureBitStringValue && "bitStringContents" in obj)) {
            if (obj.bitStringContents.length < 2) {
              /** @type {string} */
              capture[v.captureBitStringValue] = "";
            } else {
              var u = obj.bitStringContents.charCodeAt(0);
              if (0 !== u) {
                throw new Error("captureBitStringValue only supported for zero unused bits");
              }
              capture[v.captureBitStringValue] = obj.bitStringContents.slice(1);
            }
          }
        } else {
          if (errors) {
            errors.push("[" + v.name + '] Expected constructed "' + v.constructed + '", got "' + obj.constructed + '"');
          }
        }
      }
      return rval;
    };
    /** @type {!RegExp} */
    var c = /[^\\u0000-\\u00ff]/;
    /**
     * @param {string} obj
     * @param {number} level
     * @param {number} indentation
     * @return {?}
     */
    asn1.prettyPrint = function(obj, level, indentation) {
      /** @type {string} */
      var rval = "";
      level = level || 0;
      indentation = indentation || 2;
      if (level > 0) {
        /** @type {string} */
        rval = rval + "\n";
      }
      /** @type {string} */
      var indent = "";
      /** @type {number} */
      var i = 0;
      for (; i < level * indentation; ++i) {
        /** @type {string} */
        indent = indent + " ";
      }
      switch(rval = rval + (indent + "Tag: "), obj.tagClass) {
        case asn1.Class.UNIVERSAL:
          /** @type {string} */
          rval = rval + "Universal:";
          break;
        case asn1.Class.APPLICATION:
          /** @type {string} */
          rval = rval + "Application:";
          break;
        case asn1.Class.CONTEXT_SPECIFIC:
          /** @type {string} */
          rval = rval + "Context-Specific:";
          break;
        case asn1.Class.PRIVATE:
          /** @type {string} */
          rval = rval + "Private:";
      }
      if (obj.tagClass === asn1.Class.UNIVERSAL) {
        switch(rval = rval + obj.type, obj.type) {
          case asn1.Type.NONE:
            /** @type {string} */
            rval = rval + " (None)";
            break;
          case asn1.Type.BOOLEAN:
            /** @type {string} */
            rval = rval + " (Boolean)";
            break;
          case asn1.Type.INTEGER:
            /** @type {string} */
            rval = rval + " (Integer)";
            break;
          case asn1.Type.BITSTRING:
            /** @type {string} */
            rval = rval + " (Bit string)";
            break;
          case asn1.Type.OCTETSTRING:
            /** @type {string} */
            rval = rval + " (Octet string)";
            break;
          case asn1.Type.NULL:
            /** @type {string} */
            rval = rval + " (Null)";
            break;
          case asn1.Type.OID:
            /** @type {string} */
            rval = rval + " (Object Identifier)";
            break;
          case asn1.Type.ODESC:
            /** @type {string} */
            rval = rval + " (Object Descriptor)";
            break;
          case asn1.Type.EXTERNAL:
            /** @type {string} */
            rval = rval + " (External or Instance of)";
            break;
          case asn1.Type.REAL:
            /** @type {string} */
            rval = rval + " (Real)";
            break;
          case asn1.Type.ENUMERATED:
            /** @type {string} */
            rval = rval + " (Enumerated)";
            break;
          case asn1.Type.EMBEDDED:
            /** @type {string} */
            rval = rval + " (Embedded PDV)";
            break;
          case asn1.Type.UTF8:
            /** @type {string} */
            rval = rval + " (UTF8)";
            break;
          case asn1.Type.ROID:
            /** @type {string} */
            rval = rval + " (Relative Object Identifier)";
            break;
          case asn1.Type.SEQUENCE:
            /** @type {string} */
            rval = rval + " (Sequence)";
            break;
          case asn1.Type.SET:
            /** @type {string} */
            rval = rval + " (Set)";
            break;
          case asn1.Type.PRINTABLESTRING:
            /** @type {string} */
            rval = rval + " (Printable String)";
            break;
          case asn1.Type.IA5String:
            /** @type {string} */
            rval = rval + " (IA5String (ASCII))";
            break;
          case asn1.Type.UTCTIME:
            /** @type {string} */
            rval = rval + " (UTC time)";
            break;
          case asn1.Type.GENERALIZEDTIME:
            /** @type {string} */
            rval = rval + " (Generalized time)";
            break;
          case asn1.Type.BMPSTRING:
            /** @type {string} */
            rval = rval + " (BMP String)";
        }
      } else {
        /** @type {string} */
        rval = rval + obj.type;
      }
      if (rval = rval + "\n", rval = rval + (indent + "Constructed: " + obj.constructed + "\n"), obj.composed) {
        /** @type {number} */
        var subvalues = 0;
        /** @type {string} */
        var sub = "";
        /** @type {number} */
        i = 0;
        for (; i < obj.value.length; ++i) {
          if (void 0 !== obj.value[i]) {
            /** @type {number} */
            subvalues = subvalues + 1;
            /** @type {string} */
            sub = sub + asn1.prettyPrint(obj.value[i], level + 1, indentation);
            if (i + 1 < obj.value.length) {
              /** @type {string} */
              sub = sub + ",";
            }
          }
        }
        /** @type {string} */
        rval = rval + (indent + "Sub values: " + subvalues + sub);
      } else {
        if (rval = rval + (indent + "Value: "), obj.type === asn1.Type.OID) {
          var oid = asn1.derToOid(obj.value);
          /** @type {string} */
          rval = rval + oid;
          if (forge.pki && forge.pki.oids && oid in forge.pki.oids) {
            /** @type {string} */
            rval = rval + (" (" + forge.pki.oids[oid] + ") ");
          }
        }
        if (obj.type === asn1.Type.INTEGER) {
          try {
            /** @type {string} */
            rval = rval + asn1.derToInteger(obj.value);
          } catch (t) {
            /** @type {string} */
            rval = rval + ("0x" + forge.util.bytesToHex(obj.value));
          }
        } else {
          if (obj.type === asn1.Type.BITSTRING) {
            if (rval = rval + (obj.value.length > 1 ? "0x" + forge.util.bytesToHex(obj.value.slice(1)) : "(none)"), obj.value.length > 0) {
              var h = obj.value.charCodeAt(0);
              if (1 == h) {
                /** @type {string} */
                rval = rval + " (1 unused bit shown)";
              } else {
                if (h > 1) {
                  /** @type {string} */
                  rval = rval + (" (" + h + " unused bits shown)");
                }
              }
            }
          } else {
            if (obj.type === asn1.Type.OCTETSTRING) {
              if (!c.test(obj.value)) {
                /** @type {string} */
                rval = rval + ("(" + obj.value + ") ");
              }
              /** @type {string} */
              rval = rval + ("0x" + forge.util.bytesToHex(obj.value));
            } else {
              rval = rval + (obj.type === asn1.Type.UTF8 ? forge.util.decodeUtf8(obj.value) : obj.type === asn1.Type.PRINTABLESTRING || obj.type === asn1.Type.IA5String ? obj.value : c.test(obj.value) ? "0x" + forge.util.bytesToHex(obj.value) : 0 === obj.value.length ? "[null]" : obj.value);
            }
          }
        }
      }
      return rval;
    };
  }, function(mixin, canCreateDiscussions, FORGE) {
    var forge = FORGE(0);
    mixin.exports = forge.md = forge.md || {};
    forge.md.algorithms = forge.md.algorithms || {};
  }, function(mixin, canCreateDiscussions, FORGE) {
    /**
     * @param {string} name
     * @param {(!Function|RegExp|string)} mode
     * @return {undefined}
     */
    function registerAlgorithm(name, mode) {
      /**
       * @return {?}
       */
      var factory = function() {
        return new forge.aes.Algorithm(name, mode);
      };
      forge.cipher.registerAlgorithm(name, factory);
    }
    /**
     * @return {undefined}
     */
    function initialize() {
      /** @type {boolean} */
      initialized = true;
      /** @type {!Array} */
      p = [0, 1, 2, 4, 8, 16, 32, 64, 128, 27, 54];
      /** @type {!Array} */
      var d = new Array(256);
      /** @type {number} */
      var i = 0;
      for (; i < 128; ++i) {
        /** @type {number} */
        d[i] = i << 1;
        /** @type {number} */
        d[i + 128] = i + 128 << 1 ^ 283;
      }
      /** @type {!Array} */
      keys = new Array(256);
      /** @type {!Array} */
      a = new Array(256);
      /** @type {!Array} */
      arr = new Array(4);
      /** @type {!Array} */
      args = new Array(4);
      /** @type {number} */
      i = 0;
      for (; i < 4; ++i) {
        /** @type {!Array} */
        arr[i] = new Array(256);
        /** @type {!Array} */
        args[i] = new Array(256);
      }
      var n;
      var x;
      var x2;
      var j;
      var l;
      var v;
      var c;
      /** @type {number} */
      var k = 0;
      /** @type {number} */
      var xi = 0;
      /** @type {number} */
      i = 0;
      for (; i < 256; ++i) {
        /** @type {number} */
        j = xi ^ xi << 1 ^ xi << 2 ^ xi << 3 ^ xi << 4;
        /** @type {number} */
        j = j >> 8 ^ 255 & j ^ 99;
        /** @type {number} */
        keys[k] = j;
        /** @type {number} */
        a[j] = k;
        l = d[j];
        n = d[k];
        x = d[n];
        x2 = d[x];
        /** @type {number} */
        v = l << 24 ^ j << 16 ^ j << 8 ^ (j ^ l);
        /** @type {number} */
        c = (n ^ x ^ x2) << 24 ^ (k ^ x2) << 16 ^ (k ^ x ^ x2) << 8 ^ (k ^ n ^ x2);
        /** @type {number} */
        var i = 0;
        for (; i < 4; ++i) {
          /** @type {number} */
          arr[i][k] = v;
          /** @type {number} */
          args[i][j] = c;
          /** @type {number} */
          v = v << 24 | v >>> 8;
          /** @type {number} */
          c = c << 24 | c >>> 8;
        }
        if (0 === k) {
          /** @type {number} */
          k = xi = 1;
        } else {
          /** @type {number} */
          k = n ^ d[d[d[n ^ x2]]];
          /** @type {number} */
          xi = xi ^ d[d[xi]];
        }
      }
    }
    /**
     * @param {string} key
     * @param {boolean} decrypt
     * @return {?}
     */
    function _expandKey(key, decrypt) {
      var temp;
      var w = key.slice(0);
      /** @type {number} */
      var layerParamName = 1;
      var Nk = w.length;
      var Nr1 = Nk + 6 + 1;
      /** @type {number} */
      var end = Nb * Nr1;
      var i = Nk;
      for (; i < end; ++i) {
        temp = w[i - 1];
        if (i % Nk === 0) {
          /** @type {number} */
          temp = keys[temp >>> 16 & 255] << 24 ^ keys[temp >>> 8 & 255] << 16 ^ keys[255 & temp] << 8 ^ keys[temp >>> 24] ^ p[layerParamName] << 24;
          layerParamName++;
        } else {
          if (Nk > 6 && i % Nk === 4) {
            /** @type {number} */
            temp = keys[temp >>> 24] << 24 ^ keys[temp >>> 16 & 255] << 16 ^ keys[temp >>> 8 & 255] << 8 ^ keys[255 & temp];
          }
        }
        /** @type {number} */
        w[i] = w[i - Nk] ^ temp;
      }
      if (decrypt) {
        var name;
        var indexQuery = args[0];
        var state = args[1];
        var size = args[2];
        var data = args[3];
        var wnew = w.slice(0);
        end = w.length;
        /** @type {number} */
        i = 0;
        /** @type {number} */
        var wi = end - Nb;
        for (; i < end; i = i + Nb, wi = wi - Nb) {
          if (0 === i || i === end - Nb) {
            wnew[i] = w[wi];
            wnew[i + 1] = w[wi + 3];
            wnew[i + 2] = w[wi + 2];
            wnew[i + 3] = w[wi + 1];
          } else {
            /** @type {number} */
            var n = 0;
            for (; n < Nb; ++n) {
              name = w[wi + n];
              /** @type {number} */
              wnew[i + (3 & -n)] = indexQuery[keys[name >>> 24]] ^ state[keys[name >>> 16 & 255]] ^ size[keys[name >>> 8 & 255]] ^ data[keys[255 & name]];
            }
          }
        }
        w = wnew;
      }
      return w;
    }
    /**
     * @param {string} m
     * @param {!Array} t
     * @param {!Object} input
     * @param {boolean} decrypt
     * @return {undefined}
     */
    function callback(m, t, input, decrypt) {
      var e;
      var body;
      var n;
      var prev;
      var method;
      /** @type {number} */
      var clientHeight = m.length / 4 - 1;
      if (decrypt) {
        e = args[0];
        body = args[1];
        n = args[2];
        prev = args[3];
        method = a;
      } else {
        e = arr[0];
        body = arr[1];
        n = arr[2];
        prev = arr[3];
        method = keys;
      }
      var interestingPoint;
      var h;
      var lastTouchStretch;
      var w_mask;
      var viewportCenter;
      var auto;
      var touchStretch;
      /** @type {number} */
      interestingPoint = t[0] ^ m[0];
      /** @type {number} */
      h = t[decrypt ? 3 : 1] ^ m[1];
      /** @type {number} */
      lastTouchStretch = t[2] ^ m[2];
      /** @type {number} */
      w_mask = t[decrypt ? 1 : 3] ^ m[3];
      /** @type {number} */
      var pos = 3;
      /** @type {number} */
      var targetOffsetHeight = 1;
      for (; targetOffsetHeight < clientHeight; ++targetOffsetHeight) {
        /** @type {number} */
        viewportCenter = e[interestingPoint >>> 24] ^ body[h >>> 16 & 255] ^ n[lastTouchStretch >>> 8 & 255] ^ prev[255 & w_mask] ^ m[++pos];
        /** @type {number} */
        auto = e[h >>> 24] ^ body[lastTouchStretch >>> 16 & 255] ^ n[w_mask >>> 8 & 255] ^ prev[255 & interestingPoint] ^ m[++pos];
        /** @type {number} */
        touchStretch = e[lastTouchStretch >>> 24] ^ body[w_mask >>> 16 & 255] ^ n[interestingPoint >>> 8 & 255] ^ prev[255 & h] ^ m[++pos];
        /** @type {number} */
        w_mask = e[w_mask >>> 24] ^ body[interestingPoint >>> 16 & 255] ^ n[h >>> 8 & 255] ^ prev[255 & lastTouchStretch] ^ m[++pos];
        /** @type {number} */
        interestingPoint = viewportCenter;
        /** @type {number} */
        h = auto;
        /** @type {number} */
        lastTouchStretch = touchStretch;
      }
      /** @type {number} */
      input[0] = method[interestingPoint >>> 24] << 24 ^ method[h >>> 16 & 255] << 16 ^ method[lastTouchStretch >>> 8 & 255] << 8 ^ method[255 & w_mask] ^ m[++pos];
      /** @type {number} */
      input[decrypt ? 3 : 1] = method[h >>> 24] << 24 ^ method[lastTouchStretch >>> 16 & 255] << 16 ^ method[w_mask >>> 8 & 255] << 8 ^ method[255 & interestingPoint] ^ m[++pos];
      /** @type {number} */
      input[2] = method[lastTouchStretch >>> 24] << 24 ^ method[w_mask >>> 16 & 255] << 16 ^ method[interestingPoint >>> 8 & 255] << 8 ^ method[255 & h] ^ m[++pos];
      /** @type {number} */
      input[decrypt ? 1 : 3] = method[w_mask >>> 24] << 24 ^ method[interestingPoint >>> 16 & 255] << 16 ^ method[h >>> 8 & 255] << 8 ^ method[255 & lastTouchStretch] ^ m[++pos];
    }
    /**
     * @param {!Object} options
     * @return {?}
     */
    function _createCipher(options) {
      options = options || {};
      var t;
      var mode = (options.mode || "CBC").toUpperCase();
      /** @type {string} */
      var algorithm = "AES-" + mode;
      t = options.decrypt ? forge.cipher.createDecipher(algorithm, options.key) : forge.cipher.createCipher(algorithm, options.key);
      /** @type {function(string, !Object): undefined} */
      var n = t.start;
      return t.start = function(fn, options) {
        /** @type {null} */
        var dir = null;
        if (options instanceof forge.util.ByteBuffer) {
          /** @type {!Object} */
          dir = options;
          options = {};
        }
        options = options || {};
        options.output = dir;
        /** @type {string} */
        options.iv = fn;
        n.call(t, options);
      }, t;
    }
    var forge = FORGE(0);
    FORGE(12);
    FORGE(18);
    FORGE(1);
    mixin.exports = forge.aes = forge.aes || {};
    /**
     * @param {!Object} apmString
     * @param {undefined} key
     * @param {string} output
     * @param {number} mode
     * @return {?}
     */
    forge.aes.startEncrypting = function(apmString, key, output, mode) {
      var cipher = _createCipher({
        key : apmString,
        output : output,
        decrypt : false,
        mode : mode
      });
      return cipher.start(key), cipher;
    };
    /**
     * @param {!Object} key
     * @param {!Object} mode
     * @return {?}
     */
    forge.aes.createEncryptionCipher = function(key, mode) {
      return _createCipher({
        key : key,
        output : null,
        decrypt : false,
        mode : mode
      });
    };
    /**
     * @param {!Object} type
     * @param {undefined} key
     * @param {!Object} output
     * @param {string} mode
     * @return {?}
     */
    forge.aes.startDecrypting = function(type, key, output, mode) {
      var cipher = _createCipher({
        key : type,
        output : output,
        decrypt : true,
        mode : mode
      });
      return cipher.start(key), cipher;
    };
    /**
     * @param {string} key
     * @param {number} mode
     * @return {?}
     */
    forge.aes.createDecryptionCipher = function(key, mode) {
      return _createCipher({
        key : key,
        output : null,
        decrypt : true,
        mode : mode
      });
    };
    /**
     * @param {string} name
     * @param {!Object} mode
     * @return {undefined}
     */
    forge.aes.Algorithm = function(name, mode) {
      if (!initialized) {
        initialize();
      }
      var self = this;
      /** @type {string} */
      self.name = name;
      self.mode = new mode({
        blockSize : 16,
        cipher : {
          encrypt : function(data, val) {
            return callback(self._w, data, val, false);
          },
          decrypt : function(data, val) {
            return callback(self._w, data, val, true);
          }
        }
      });
      /** @type {boolean} */
      self._init = false;
    };
    /**
     * @param {!Object} args
     * @return {undefined}
     */
    forge.aes.Algorithm.prototype.initialize = function(args) {
      if (!this._init) {
        var tmp;
        var key = args.key;
        if ("string" != typeof key || 16 !== key.length && 24 !== key.length && 32 !== key.length) {
          if (forge.util.isArray(key) && (16 === key.length || 24 === key.length || 32 === key.length)) {
            tmp = key;
            key = forge.util.createBuffer();
            /** @type {number} */
            var i = 0;
            for (; i < tmp.length; ++i) {
              key.putByte(tmp[i]);
            }
          }
        } else {
          key = forge.util.createBuffer(key);
        }
        if (!forge.util.isArray(key)) {
          tmp = key;
          /** @type {!Array} */
          key = [];
          var n = tmp.length();
          if (16 === n || 24 === n || 32 === n) {
            /** @type {number} */
            n = n >>> 2;
            /** @type {number} */
            i = 0;
            for (; i < n; ++i) {
              key.push(tmp.getInt32());
            }
          }
        }
        if (!forge.util.isArray(key) || 4 !== key.length && 6 !== key.length && 8 !== key.length) {
          throw new Error("Invalid key parameter.");
        }
        var mode = this.mode.name;
        /** @type {boolean} */
        var logEntry = ["CFB", "OFB", "CTR", "GCM"].indexOf(mode) !== -1;
        this._w = _expandKey(key, args.decrypt && !logEntry);
        /** @type {boolean} */
        this._init = true;
      }
    };
    /**
     * @param {boolean} key
     * @param {boolean} decrypt
     * @return {?}
     */
    forge.aes._expandKey = function(key, decrypt) {
      return initialized || initialize(), _expandKey(key, decrypt);
    };
    /** @type {function(string, !Array, !Object, boolean): undefined} */
    forge.aes._updateBlock = callback;
    registerAlgorithm("AES-ECB", forge.cipher.modes.ecb);
    registerAlgorithm("AES-CBC", forge.cipher.modes.cbc);
    registerAlgorithm("AES-CFB", forge.cipher.modes.cfb);
    registerAlgorithm("AES-OFB", forge.cipher.modes.ofb);
    registerAlgorithm("AES-CTR", forge.cipher.modes.ctr);
    registerAlgorithm("AES-GCM", forge.cipher.modes.gcm);
    var keys;
    var a;
    var p;
    var arr;
    var args;
    /** @type {boolean} */
    var initialized = false;
    /** @type {number} */
    var Nb = 4;
  }, function(mixin, canCreateDiscussions, FORGE) {
    /**
     * @param {string} id
     * @param {string} name
     * @return {undefined}
     */
    function debug(id, name) {
      /** @type {string} */
      clsNameMap[id] = name;
      /** @type {string} */
      clsNameMap[name] = id;
    }
    /**
     * @param {string} key
     * @param {string} name
     * @return {undefined}
     */
    function addNewLinkName(key, name) {
      /** @type {string} */
      clsNameMap[key] = name;
    }
    var forge = FORGE(0);
    forge.pki = forge.pki || {};
    var clsNameMap = mixin.exports = forge.pki.oids = forge.oids = forge.oids || {};
    debug("1.2.840.113549.1.1.1", "rsaEncryption");
    debug("1.2.840.113549.1.1.4", "md5WithRSAEncryption");
    debug("1.2.840.113549.1.1.5", "sha1WithRSAEncryption");
    debug("1.2.840.113549.1.1.7", "RSAES-OAEP");
    debug("1.2.840.113549.1.1.8", "mgf1");
    debug("1.2.840.113549.1.1.9", "pSpecified");
    debug("1.2.840.113549.1.1.10", "RSASSA-PSS");
    debug("1.2.840.113549.1.1.11", "sha256WithRSAEncryption");
    debug("1.2.840.113549.1.1.12", "sha384WithRSAEncryption");
    debug("1.2.840.113549.1.1.13", "sha512WithRSAEncryption");
    debug("1.3.14.3.2.7", "desCBC");
    debug("1.3.14.3.2.26", "sha1");
    debug("2.16.840.1.101.3.4.2.1", "sha256");
    debug("2.16.840.1.101.3.4.2.2", "sha384");
    debug("2.16.840.1.101.3.4.2.3", "sha512");
    debug("1.2.840.113549.2.5", "md5");
    debug("1.2.840.113549.1.7.1", "data");
    debug("1.2.840.113549.1.7.2", "signedData");
    debug("1.2.840.113549.1.7.3", "envelopedData");
    debug("1.2.840.113549.1.7.4", "signedAndEnvelopedData");
    debug("1.2.840.113549.1.7.5", "digestedData");
    debug("1.2.840.113549.1.7.6", "encryptedData");
    debug("1.2.840.113549.1.9.1", "emailAddress");
    debug("1.2.840.113549.1.9.2", "unstructuredName");
    debug("1.2.840.113549.1.9.3", "contentType");
    debug("1.2.840.113549.1.9.4", "messageDigest");
    debug("1.2.840.113549.1.9.5", "signingTime");
    debug("1.2.840.113549.1.9.6", "counterSignature");
    debug("1.2.840.113549.1.9.7", "challengePassword");
    debug("1.2.840.113549.1.9.8", "unstructuredAddress");
    debug("1.2.840.113549.1.9.14", "extensionRequest");
    debug("1.2.840.113549.1.9.20", "friendlyName");
    debug("1.2.840.113549.1.9.21", "localKeyId");
    debug("1.2.840.113549.1.9.22.1", "x509Certificate");
    debug("1.2.840.113549.1.12.10.1.1", "keyBag");
    debug("1.2.840.113549.1.12.10.1.2", "pkcs8ShroudedKeyBag");
    debug("1.2.840.113549.1.12.10.1.3", "certBag");
    debug("1.2.840.113549.1.12.10.1.4", "crlBag");
    debug("1.2.840.113549.1.12.10.1.5", "secretBag");
    debug("1.2.840.113549.1.12.10.1.6", "safeContentsBag");
    debug("1.2.840.113549.1.5.13", "pkcs5PBES2");
    debug("1.2.840.113549.1.5.12", "pkcs5PBKDF2");
    debug("1.2.840.113549.1.12.1.1", "pbeWithSHAAnd128BitRC4");
    debug("1.2.840.113549.1.12.1.2", "pbeWithSHAAnd40BitRC4");
    debug("1.2.840.113549.1.12.1.3", "pbeWithSHAAnd3-KeyTripleDES-CBC");
    debug("1.2.840.113549.1.12.1.4", "pbeWithSHAAnd2-KeyTripleDES-CBC");
    debug("1.2.840.113549.1.12.1.5", "pbeWithSHAAnd128BitRC2-CBC");
    debug("1.2.840.113549.1.12.1.6", "pbewithSHAAnd40BitRC2-CBC");
    debug("1.2.840.113549.2.7", "hmacWithSHA1");
    debug("1.2.840.113549.2.8", "hmacWithSHA224");
    debug("1.2.840.113549.2.9", "hmacWithSHA256");
    debug("1.2.840.113549.2.10", "hmacWithSHA384");
    debug("1.2.840.113549.2.11", "hmacWithSHA512");
    debug("1.2.840.113549.3.7", "des-EDE3-CBC");
    debug("2.16.840.1.101.3.4.1.2", "aes128-CBC");
    debug("2.16.840.1.101.3.4.1.22", "aes192-CBC");
    debug("2.16.840.1.101.3.4.1.42", "aes256-CBC");
    debug("2.5.4.3", "commonName");
    debug("2.5.4.5", "serialName");
    debug("2.5.4.6", "countryName");
    debug("2.5.4.7", "localityName");
    debug("2.5.4.8", "stateOrProvinceName");
    debug("2.5.4.10", "organizationName");
    debug("2.5.4.11", "organizationalUnitName");
    debug("2.16.840.1.113730.1.1", "nsCertType");
    addNewLinkName("2.5.29.1", "authorityKeyIdentifier");
    addNewLinkName("2.5.29.2", "keyAttributes");
    addNewLinkName("2.5.29.3", "certificatePolicies");
    addNewLinkName("2.5.29.4", "keyUsageRestriction");
    addNewLinkName("2.5.29.5", "policyMapping");
    addNewLinkName("2.5.29.6", "subtreesConstraint");
    addNewLinkName("2.5.29.7", "subjectAltName");
    addNewLinkName("2.5.29.8", "issuerAltName");
    addNewLinkName("2.5.29.9", "subjectDirectoryAttributes");
    addNewLinkName("2.5.29.10", "basicConstraints");
    addNewLinkName("2.5.29.11", "nameConstraints");
    addNewLinkName("2.5.29.12", "policyConstraints");
    addNewLinkName("2.5.29.13", "basicConstraints");
    debug("2.5.29.14", "subjectKeyIdentifier");
    debug("2.5.29.15", "keyUsage");
    addNewLinkName("2.5.29.16", "privateKeyUsagePeriod");
    debug("2.5.29.17", "subjectAltName");
    debug("2.5.29.18", "issuerAltName");
    debug("2.5.29.19", "basicConstraints");
    addNewLinkName("2.5.29.20", "cRLNumber");
    addNewLinkName("2.5.29.21", "cRLReason");
    addNewLinkName("2.5.29.22", "expirationDate");
    addNewLinkName("2.5.29.23", "instructionCode");
    addNewLinkName("2.5.29.24", "invalidityDate");
    addNewLinkName("2.5.29.25", "cRLDistributionPoints");
    addNewLinkName("2.5.29.26", "issuingDistributionPoint");
    addNewLinkName("2.5.29.27", "deltaCRLIndicator");
    addNewLinkName("2.5.29.28", "issuingDistributionPoint");
    addNewLinkName("2.5.29.29", "certificateIssuer");
    addNewLinkName("2.5.29.30", "nameConstraints");
    debug("2.5.29.31", "cRLDistributionPoints");
    debug("2.5.29.32", "certificatePolicies");
    addNewLinkName("2.5.29.33", "policyMappings");
    addNewLinkName("2.5.29.34", "policyConstraints");
    debug("2.5.29.35", "authorityKeyIdentifier");
    addNewLinkName("2.5.29.36", "policyConstraints");
    debug("2.5.29.37", "extKeyUsage");
    addNewLinkName("2.5.29.46", "freshestCRL");
    addNewLinkName("2.5.29.54", "inhibitAnyPolicy");
    debug("1.3.6.1.4.1.11129.2.4.2", "timestampList");
    debug("1.3.6.1.5.5.7.1.1", "authorityInfoAccess");
    debug("1.3.6.1.5.5.7.3.1", "serverAuth");
    debug("1.3.6.1.5.5.7.3.2", "clientAuth");
    debug("1.3.6.1.5.5.7.3.3", "codeSigning");
    debug("1.3.6.1.5.5.7.3.4", "emailProtection");
    debug("1.3.6.1.5.5.7.3.8", "timeStamping");
  }, function(mixin, canCreateDiscussions, FORGE) {
    /**
     * @param {!Object} card
     * @return {?}
     */
    function next(card) {
      /** @type {string} */
      var str = card.name + ": ";
      /** @type {!Array} */
      var bindings = [];
      /**
       * @param {?} data
       * @param {string} tbi
       * @return {?}
       */
      var async = function(data, tbi) {
        return " " + tbi;
      };
      /** @type {number} */
      var i = 0;
      for (; i < card.values.length; ++i) {
        bindings.push(card.values[i].replace(/^(\S+\r\n)/, async));
      }
      /** @type {string} */
      str = str + (bindings.join(",") + "\r\n");
      /** @type {number} */
      var k = 0;
      /** @type {number} */
      var j = -1;
      /** @type {number} */
      i = 0;
      for (; i < str.length; ++i, ++k) {
        if (k > 65 && j !== -1) {
          var cha = str[j];
          if ("," === cha) {
            ++j;
            /** @type {string} */
            str = str.substr(0, j) + "\r\n " + str.substr(j);
          } else {
            /** @type {string} */
            str = str.substr(0, j) + "\r\n" + cha + str.substr(j + 1);
          }
          /** @type {number} */
          k = i - j - 1;
          /** @type {number} */
          j = -1;
          ++i;
        } else {
          if (!(" " !== str[i] && "\t" !== str[i] && "," !== str[i])) {
            /** @type {number} */
            j = i;
          }
        }
      }
      return str;
    }
    /**
     * @param {string} module
     * @return {?}
     */
    function n(module) {
      return module.replace(/^\s+/, "");
    }
    var forge = FORGE(0);
    FORGE(1);
    var _Uize_Util_Html_Encode = mixin.exports = forge.pem = forge.pem || {};
    /**
     * @param {!Object} msg
     * @param {number} options
     * @return {?}
     */
    _Uize_Util_Html_Encode.encode = function(msg, options) {
      options = options || {};
      var data;
      /** @type {string} */
      var str = "-----BEGIN " + msg.type + "-----\r\n";
      if (msg.procType && (data = {
        name : "Proc-Type",
        values : [String(msg.procType.version), msg.procType.type]
      }, str = str + next(data)), msg.contentDomain && (data = {
        name : "Content-Domain",
        values : [msg.contentDomain]
      }, str = str + next(data)), msg.dekInfo && (data = {
        name : "DEK-Info",
        values : [msg.dekInfo.algorithm]
      }, msg.dekInfo.parameters && data.values.push(msg.dekInfo.parameters), str = str + next(data)), msg.headers) {
        /** @type {number} */
        var i = 0;
        for (; i < msg.headers.length; ++i) {
          /** @type {string} */
          str = str + next(msg.headers[i]);
        }
      }
      return msg.procType && (str = str + "\r\n"), str = str + (forge.util.encode64(msg.body, options.maxline || 64) + "\r\n"), str = str + ("-----END " + msg.type + "-----\r\n");
    };
    /**
     * @param {?} obj
     * @return {?}
     */
    _Uize_Util_Html_Encode.decode = function(obj) {
      var m;
      /** @type {!Array} */
      var out = [];
      /** @type {!RegExp} */
      var a = /\s*-----BEGIN ([A-Z0-9- ]+)-----\r?\n?([\x21-\x7e\s]+?(?:\r?\n\r?\n))?([:A-Za-z0-9+\/=\s]+?)-----END \1-----/g;
      /** @type {!RegExp} */
      var s = /([\x21-\x7e]+):\s*([\x21-\x7e\s^:]+)/;
      /** @type {!RegExp} */
      var reDot = /\r?\n/;
      for (;;) {
        if (m = a.exec(obj), !m) {
          break;
        }
        var msg = {
          type : m[1],
          procType : null,
          contentDomain : null,
          dekInfo : null,
          headers : [],
          body : forge.util.decode64(m[3])
        };
        if (out.push(msg), m[2]) {
          /** @type {!Array<string>} */
          var pair = m[2].split(reDot);
          /** @type {number} */
          var i = 0;
          for (; m && i < pair.length;) {
            /** @type {string} */
            var str = pair[i].replace(/\s+$/, "");
            /** @type {number} */
            var j = i + 1;
            for (; j < pair.length; ++j) {
              /** @type {string} */
              var offset = pair[j];
              if (!/\s/.test(offset[0])) {
                break;
              }
              /** @type {string} */
              str = str + offset;
              /** @type {number} */
              i = j;
            }
            if (m = str.match(s)) {
              var group = {
                name : m[1],
                values : []
              };
              /** @type {!Array<string>} */
              var parts = m[2].split(",");
              /** @type {number} */
              var i = 0;
              for (; i < parts.length; ++i) {
                group.values.push(n(parts[i]));
              }
              if (msg.procType) {
                if (msg.contentDomain || "Content-Domain" !== group.name) {
                  if (msg.dekInfo || "DEK-Info" !== group.name) {
                    msg.headers.push(group);
                  } else {
                    if (0 === group.values.length) {
                      throw new Error('Invalid PEM formatted message. The "DEK-Info" header must have at least one subfield.');
                    }
                    msg.dekInfo = {
                      algorithm : parts[0],
                      parameters : parts[1] || null
                    };
                  }
                } else {
                  /** @type {string} */
                  msg.contentDomain = parts[0] || "";
                }
              } else {
                if ("Proc-Type" !== group.name) {
                  throw new Error('Invalid PEM formatted message. The first encapsulated header must be "Proc-Type".');
                }
                if (2 !== group.values.length) {
                  throw new Error('Invalid PEM formatted message. The "Proc-Type" header must have two subfields.');
                }
                msg.procType = {
                  version : parts[0],
                  type : parts[1]
                };
              }
            }
            ++i;
          }
          if ("ENCRYPTED" === msg.procType && !msg.dekInfo) {
            throw new Error('Invalid PEM formatted message. The "DEK-Info" header must be present if "Proc-Type" is "ENCRYPTED".');
          }
        }
      }
      if (0 === out.length) {
        throw new Error("Invalid PEM formatted message.");
      }
      return out;
    };
  }, function(mixin, canCreateDiscussions, FORGE) {
    var forge = FORGE(0);
    FORGE(4);
    FORGE(1);
    var directory_epub = mixin.exports = forge.hmac = forge.hmac || {};
    /**
     * @return {?}
     */
    directory_epub.create = function() {
      /** @type {null} */
      var unshifted_key = null;
      /** @type {null} */
      var _md = null;
      /** @type {null} */
      var bytes = null;
      /** @type {null} */
      var f = null;
      var ctx = {};
      return ctx.start = function(name, key) {
        if (null !== name) {
          if ("string" == typeof name) {
            if (name = name.toLowerCase(), !(name in forge.md.algorithms)) {
              throw new Error('Unknown hash algorithm "' + name + '"');
            }
            _md = forge.md.algorithms[name].create();
          } else {
            /** @type {string} */
            _md = name;
          }
        }
        if (null === key) {
          key = unshifted_key;
        } else {
          if ("string" == typeof key) {
            key = forge.util.createBuffer(key);
          } else {
            if (forge.util.isArray(key)) {
              /** @type {!Object} */
              var tmp = key;
              key = forge.util.createBuffer();
              /** @type {number} */
              var i = 0;
              for (; i < tmp.length; ++i) {
                key.putByte(tmp[i]);
              }
            }
          }
          var keylen = key.length();
          if (keylen > _md.blockLength) {
            _md.start();
            _md.update(key.bytes());
            key = _md.digest();
          }
          bytes = forge.util.createBuffer();
          f = forge.util.createBuffer();
          keylen = key.length();
          /** @type {number} */
          i = 0;
          for (; i < keylen; ++i) {
            tmp = key.at(i);
            bytes.putByte(54 ^ tmp);
            f.putByte(92 ^ tmp);
          }
          if (keylen < _md.blockLength) {
            /** @type {number} */
            tmp = _md.blockLength - keylen;
            /** @type {number} */
            i = 0;
            for (; i < tmp; ++i) {
              bytes.putByte(54);
              f.putByte(92);
            }
          }
          /** @type {!Object} */
          unshifted_key = key;
          bytes = bytes.bytes();
          f = f.bytes();
        }
        _md.start();
        _md.update(bytes);
      }, ctx.update = function(c) {
        _md.update(c);
      }, ctx.getMac = function() {
        var inner = _md.digest().bytes();
        return _md.start(), _md.update(f), _md.update(inner), _md.digest();
      }, ctx.digest = ctx.getMac, ctx;
    };
  }, function(mixin, canCreateDiscussions, FORGE) {
    /**
     * @return {undefined}
     */
    function _init() {
      /** @type {string} */
      _padding = String.fromCharCode(128);
      _padding = _padding + forge.util.fillString(String.fromCharCode(0), 64);
      /** @type {boolean} */
      c = true;
    }
    /**
     * @param {!Object} s
     * @param {!Object} buffer
     * @param {!NodeList} bytes
     * @return {undefined}
     */
    function _update(s, buffer, bytes) {
      var p;
      var a;
      var b;
      var c;
      var d;
      var e;
      var f;
      var i;
      var nCs = bytes.length();
      for (; nCs >= 64;) {
        a = s.h0;
        b = s.h1;
        c = s.h2;
        d = s.h3;
        e = s.h4;
        /** @type {number} */
        i = 0;
        for (; i < 16; ++i) {
          p = bytes.getInt32();
          buffer[i] = p;
          /** @type {number} */
          f = d ^ b & (c ^ d);
          p = (a << 5 | a >>> 27) + f + e + 1518500249 + p;
          e = d;
          d = c;
          /** @type {number} */
          c = (b << 30 | b >>> 2) >>> 0;
          b = a;
          a = p;
        }
        for (; i < 20; ++i) {
          /** @type {number} */
          p = buffer[i - 3] ^ buffer[i - 8] ^ buffer[i - 14] ^ buffer[i - 16];
          /** @type {number} */
          p = p << 1 | p >>> 31;
          /** @type {number} */
          buffer[i] = p;
          /** @type {number} */
          f = d ^ b & (c ^ d);
          p = (a << 5 | a >>> 27) + f + e + 1518500249 + p;
          e = d;
          d = c;
          /** @type {number} */
          c = (b << 30 | b >>> 2) >>> 0;
          b = a;
          a = p;
        }
        for (; i < 32; ++i) {
          /** @type {number} */
          p = buffer[i - 3] ^ buffer[i - 8] ^ buffer[i - 14] ^ buffer[i - 16];
          /** @type {number} */
          p = p << 1 | p >>> 31;
          /** @type {number} */
          buffer[i] = p;
          /** @type {number} */
          f = b ^ c ^ d;
          p = (a << 5 | a >>> 27) + f + e + 1859775393 + p;
          e = d;
          d = c;
          /** @type {number} */
          c = (b << 30 | b >>> 2) >>> 0;
          b = a;
          a = p;
        }
        for (; i < 40; ++i) {
          /** @type {number} */
          p = buffer[i - 6] ^ buffer[i - 16] ^ buffer[i - 28] ^ buffer[i - 32];
          /** @type {number} */
          p = p << 2 | p >>> 30;
          /** @type {number} */
          buffer[i] = p;
          /** @type {number} */
          f = b ^ c ^ d;
          p = (a << 5 | a >>> 27) + f + e + 1859775393 + p;
          e = d;
          d = c;
          /** @type {number} */
          c = (b << 30 | b >>> 2) >>> 0;
          b = a;
          a = p;
        }
        for (; i < 60; ++i) {
          /** @type {number} */
          p = buffer[i - 6] ^ buffer[i - 16] ^ buffer[i - 28] ^ buffer[i - 32];
          /** @type {number} */
          p = p << 2 | p >>> 30;
          /** @type {number} */
          buffer[i] = p;
          /** @type {number} */
          f = b & c | d & (b ^ c);
          p = (a << 5 | a >>> 27) + f + e + 2400959708 + p;
          e = d;
          d = c;
          /** @type {number} */
          c = (b << 30 | b >>> 2) >>> 0;
          b = a;
          a = p;
        }
        for (; i < 80; ++i) {
          /** @type {number} */
          p = buffer[i - 6] ^ buffer[i - 16] ^ buffer[i - 28] ^ buffer[i - 32];
          /** @type {number} */
          p = p << 2 | p >>> 30;
          /** @type {number} */
          buffer[i] = p;
          /** @type {number} */
          f = b ^ c ^ d;
          p = (a << 5 | a >>> 27) + f + e + 3395469782 + p;
          e = d;
          d = c;
          /** @type {number} */
          c = (b << 30 | b >>> 2) >>> 0;
          b = a;
          a = p;
        }
        /** @type {number} */
        s.h0 = s.h0 + a | 0;
        /** @type {number} */
        s.h1 = s.h1 + b | 0;
        /** @type {number} */
        s.h2 = s.h2 + c | 0;
        /** @type {number} */
        s.h3 = s.h3 + d | 0;
        /** @type {number} */
        s.h4 = s.h4 + e | 0;
        /** @type {number} */
        nCs = nCs - 64;
      }
    }
    var forge = FORGE(0);
    FORGE(4);
    FORGE(1);
    var sha1 = mixin.exports = forge.sha1 = forge.sha1 || {};
    forge.md.sha1 = forge.md.algorithms.sha1 = sha1;
    /**
     * @return {?}
     */
    sha1.create = function() {
      if (!c) {
        _init();
      }
      /** @type {null} */
      var _state = null;
      var _input = forge.util.createBuffer();
      /** @type {!Array} */
      var data = new Array(80);
      var md = {
        algorithm : "sha1",
        blockLength : 64,
        digestLength : 20,
        messageLength : 0,
        fullMessageLength : null,
        messageLengthSize : 8
      };
      return md.start = function() {
        /** @type {number} */
        md.messageLength = 0;
        /** @type {!Array} */
        md.fullMessageLength = md.messageLength64 = [];
        /** @type {number} */
        var eleSize = md.messageLengthSize / 4;
        /** @type {number} */
        var x = 0;
        for (; x < eleSize; ++x) {
          md.fullMessageLength.push(0);
        }
        return _input = forge.util.createBuffer(), _state = {
          h0 : 1732584193,
          h1 : 4023233417,
          h2 : 2562383102,
          h3 : 271733878,
          h4 : 3285377520
        }, md;
      }, md.start(), md.update = function(msg, encoding) {
        if ("utf8" === encoding) {
          msg = forge.util.encodeUtf8(msg);
        }
        var len = msg.length;
        md.messageLength += len;
        /** @type {!Array} */
        len = [len / 4294967296 >>> 0, len >>> 0];
        /** @type {number} */
        var i = md.fullMessageLength.length - 1;
        for (; i >= 0; --i) {
          md.fullMessageLength[i] += len[1];
          len[1] = len[0] + (md.fullMessageLength[i] / 4294967296 >>> 0);
          /** @type {number} */
          md.fullMessageLength[i] = md.fullMessageLength[i] >>> 0;
          /** @type {number} */
          len[0] = len[1] / 4294967296 >>> 0;
        }
        return _input.putBytes(msg), _update(_state, data, _input), (_input.read > 2048 || 0 === _input.length()) && _input.compact(), md;
      }, md.digest = function() {
        var finalBlock = forge.util.createBuffer();
        finalBlock.putBytes(_input.bytes());
        var remaining = md.fullMessageLength[md.fullMessageLength.length - 1] + md.messageLengthSize;
        /** @type {number} */
        var overflow = remaining & md.blockLength - 1;
        finalBlock.putBytes(_padding.substr(0, md.blockLength - overflow));
        var next;
        var carry;
        /** @type {number} */
        var bits = 8 * md.fullMessageLength[0];
        /** @type {number} */
        var i = 0;
        for (; i < md.fullMessageLength.length - 1; ++i) {
          /** @type {number} */
          next = 8 * md.fullMessageLength[i + 1];
          /** @type {number} */
          carry = next / 4294967296 >>> 0;
          /** @type {number} */
          bits = bits + carry;
          finalBlock.putInt32(bits >>> 0);
          /** @type {number} */
          bits = next >>> 0;
        }
        finalBlock.putInt32(bits);
        var s2 = {
          h0 : _state.h0,
          h1 : _state.h1,
          h2 : _state.h2,
          h3 : _state.h3,
          h4 : _state.h4
        };
        _update(s2, data, finalBlock);
        var rval = forge.util.createBuffer();
        return rval.putInt32(s2.h0), rval.putInt32(s2.h1), rval.putInt32(s2.h2), rval.putInt32(s2.h3), rval.putInt32(s2.h4), rval;
      }, md;
    };
    /** @type {null} */
    var _padding = null;
    /** @type {boolean} */
    var c = false;
  }, function(mixin, canCreateDiscussions, FORGE) {
    /**
     * @param {string} name
     * @param {(!Function|RegExp|string)} mode
     * @return {undefined}
     */
    function registerAlgorithm(name, mode) {
      /**
       * @return {?}
       */
      var factory = function() {
        return new forge.des.Algorithm(name, mode);
      };
      forge.cipher.registerAlgorithm(name, factory);
    }
    /**
     * @param {!NodeList} key
     * @return {?}
     */
    function _createKeys(key) {
      var temp;
      /** @type {!Array} */
      var pc2bytes0 = [0, 4, 536870912, 536870916, 65536, 65540, 536936448, 536936452, 512, 516, 536871424, 536871428, 66048, 66052, 536936960, 536936964];
      /** @type {!Array} */
      var pc2bytes1 = [0, 1, 1048576, 1048577, 67108864, 67108865, 68157440, 68157441, 256, 257, 1048832, 1048833, 67109120, 67109121, 68157696, 68157697];
      /** @type {!Array} */
      var n = [0, 8, 2048, 2056, 16777216, 16777224, 16779264, 16779272, 0, 8, 2048, 2056, 16777216, 16777224, 16779264, 16779272];
      /** @type {!Array} */
      var i = [0, 2097152, 134217728, 136314880, 8192, 2105344, 134225920, 136323072, 131072, 2228224, 134348800, 136445952, 139264, 2236416, 134356992, 136454144];
      /** @type {!Array} */
      var s = [0, 262144, 16, 262160, 0, 262144, 16, 262160, 4096, 266240, 4112, 266256, 4096, 266240, 4112, 266256];
      /** @type {!Array} */
      var o = [0, 1024, 32, 1056, 0, 1024, 32, 1056, 33554432, 33555456, 33554464, 33555488, 33554432, 33555456, 33554464, 33555488];
      /** @type {!Array} */
      var c = [0, 268435456, 524288, 268959744, 2, 268435458, 524290, 268959746, 0, 268435456, 524288, 268959744, 2, 268435458, 524290, 268959746];
      /** @type {!Array} */
      var pc2bytes7 = [0, 65536, 2048, 67584, 536870912, 536936448, 536872960, 536938496, 131072, 196608, 133120, 198656, 537001984, 537067520, 537004032, 537069568];
      /** @type {!Array} */
      var pc2bytes8 = [0, 262144, 0, 262144, 2, 262146, 2, 262146, 33554432, 33816576, 33554432, 33816576, 33554434, 33816578, 33554434, 33816578];
      /** @type {!Array} */
      var p = [0, 268435456, 8, 268435464, 0, 268435456, 8, 268435464, 1024, 268436480, 1032, 268436488, 1024, 268436480, 1032, 268436488];
      /** @type {!Array} */
      var h = [0, 32, 0, 32, 1048576, 1048608, 1048576, 1048608, 8192, 8224, 8192, 8224, 1056768, 1056800, 1056768, 1056800];
      /** @type {!Array} */
      var f = [0, 16777216, 512, 16777728, 2097152, 18874368, 2097664, 18874880, 67108864, 83886080, 67109376, 83886592, 69206016, 85983232, 69206528, 85983744];
      /** @type {!Array} */
      var d = [0, 4096, 134217728, 134221824, 524288, 528384, 134742016, 134746112, 16, 4112, 134217744, 134221840, 524304, 528400, 134742032, 134746128];
      /** @type {!Array} */
      var y = [0, 4, 256, 260, 0, 4, 256, 260, 1, 5, 257, 261, 1, 5, 257, 261];
      /** @type {number} */
      var clientHeight = key.length() > 8 ? 3 : 1;
      /** @type {!Array} */
      var keys = [];
      /** @type {!Array} */
      var crossfilterable_layers = [0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0];
      /** @type {number} */
      var j = 0;
      /** @type {number} */
      var targetOffsetHeight = 0;
      for (; targetOffsetHeight < clientHeight; targetOffsetHeight++) {
        var left = key.getInt32();
        var right = key.getInt32();
        /** @type {number} */
        temp = 252645135 & (left >>> 4 ^ right);
        /** @type {number} */
        right = right ^ temp;
        /** @type {number} */
        left = left ^ temp << 4;
        /** @type {number} */
        temp = 65535 & (right >>> -16 ^ left);
        /** @type {number} */
        left = left ^ temp;
        /** @type {number} */
        right = right ^ temp << -16;
        /** @type {number} */
        temp = 858993459 & (left >>> 2 ^ right);
        /** @type {number} */
        right = right ^ temp;
        /** @type {number} */
        left = left ^ temp << 2;
        /** @type {number} */
        temp = 65535 & (right >>> -16 ^ left);
        /** @type {number} */
        left = left ^ temp;
        /** @type {number} */
        right = right ^ temp << -16;
        /** @type {number} */
        temp = 1431655765 & (left >>> 1 ^ right);
        /** @type {number} */
        right = right ^ temp;
        /** @type {number} */
        left = left ^ temp << 1;
        /** @type {number} */
        temp = 16711935 & (right >>> 8 ^ left);
        /** @type {number} */
        left = left ^ temp;
        /** @type {number} */
        right = right ^ temp << 8;
        /** @type {number} */
        temp = 1431655765 & (left >>> 1 ^ right);
        /** @type {number} */
        right = right ^ temp;
        /** @type {number} */
        left = left ^ temp << 1;
        /** @type {number} */
        temp = left << 8 | right >>> 20 & 240;
        /** @type {number} */
        left = right << 24 | right << 8 & 16711680 | right >>> 8 & 65280 | right >>> 24 & 240;
        /** @type {number} */
        right = temp;
        /** @type {number} */
        var layer_i = 0;
        for (; layer_i < crossfilterable_layers.length; ++layer_i) {
          if (crossfilterable_layers[layer_i]) {
            /** @type {number} */
            left = left << 2 | left >>> 26;
            /** @type {number} */
            right = right << 2 | right >>> 26;
          } else {
            /** @type {number} */
            left = left << 1 | left >>> 27;
            /** @type {number} */
            right = right << 1 | right >>> 27;
          }
          /** @type {number} */
          left = left & -15;
          /** @type {number} */
          right = right & -15;
          /** @type {number} */
          var lefttemp = pc2bytes0[left >>> 28] | pc2bytes1[left >>> 24 & 15] | n[left >>> 20 & 15] | i[left >>> 16 & 15] | s[left >>> 12 & 15] | o[left >>> 8 & 15] | c[left >>> 4 & 15];
          /** @type {number} */
          var righttemp = pc2bytes7[right >>> 28] | pc2bytes8[right >>> 24 & 15] | p[right >>> 20 & 15] | h[right >>> 16 & 15] | f[right >>> 12 & 15] | d[right >>> 8 & 15] | y[right >>> 4 & 15];
          /** @type {number} */
          temp = 65535 & (righttemp >>> 16 ^ lefttemp);
          /** @type {number} */
          keys[j++] = lefttemp ^ temp;
          /** @type {number} */
          keys[j++] = righttemp ^ temp << 16;
        }
      }
      return keys;
    }
    /**
     * @param {!NodeList} result
     * @param {!Object} input
     * @param {!Object} size
     * @param {boolean} win
     * @return {undefined}
     */
    function callback(result, input, size, win) {
      var c;
      /** @type {number} */
      var vertical = 32 === result.length ? 3 : 9;
      /** @type {!Array} */
      c = 3 === vertical ? win ? [30, -2, -2] : [0, 32, 2] : win ? [94, 62, -2, 32, 64, 2, 30, -2, -2] : [0, 32, 2, 62, 30, -2, 64, 96, 2];
      var temp;
      var left = input[0];
      var right = input[1];
      /** @type {number} */
      temp = 252645135 & (left >>> 4 ^ right);
      /** @type {number} */
      right = right ^ temp;
      /** @type {number} */
      left = left ^ temp << 4;
      /** @type {number} */
      temp = 65535 & (left >>> 16 ^ right);
      /** @type {number} */
      right = right ^ temp;
      /** @type {number} */
      left = left ^ temp << 16;
      /** @type {number} */
      temp = 858993459 & (right >>> 2 ^ left);
      /** @type {number} */
      left = left ^ temp;
      /** @type {number} */
      right = right ^ temp << 2;
      /** @type {number} */
      temp = 16711935 & (right >>> 8 ^ left);
      /** @type {number} */
      left = left ^ temp;
      /** @type {number} */
      right = right ^ temp << 8;
      /** @type {number} */
      temp = 1431655765 & (left >>> 1 ^ right);
      /** @type {number} */
      right = right ^ temp;
      /** @type {number} */
      left = left ^ temp << 1;
      /** @type {number} */
      left = left << 1 | left >>> 31;
      /** @type {number} */
      right = right << 1 | right >>> 31;
      /** @type {number} */
      var i = 0;
      for (; i < vertical; i = i + 3) {
        var prop = c[i + 1];
        var op = c[i + 2];
        var type = c[i];
        for (; type != prop; type = type + op) {
          /** @type {number} */
          var kAlignMask = right ^ result[type];
          /** @type {number} */
          var TM = (right >>> 4 | right << 28) ^ result[type + 1];
          /** @type {number} */
          temp = left;
          /** @type {number} */
          left = right;
          /** @type {number} */
          right = temp ^ (to_matched_rules[kAlignMask >>> 24 & 63] | to_already_matched_rules[kAlignMask >>> 16 & 63] | f[kAlignMask >>> 8 & 63] | _alignPrices[63 & kAlignMask] | c[TM >>> 24 & 63] | l[TM >>> 16 & 63] | h[TM >>> 8 & 63] | piTable[63 & TM]);
        }
        /** @type {number} */
        temp = left;
        /** @type {number} */
        left = right;
        /** @type {number} */
        right = temp;
      }
      /** @type {number} */
      left = left >>> 1 | left << 31;
      /** @type {number} */
      right = right >>> 1 | right << 31;
      /** @type {number} */
      temp = 1431655765 & (left >>> 1 ^ right);
      /** @type {number} */
      right = right ^ temp;
      /** @type {number} */
      left = left ^ temp << 1;
      /** @type {number} */
      temp = 16711935 & (right >>> 8 ^ left);
      /** @type {number} */
      left = left ^ temp;
      /** @type {number} */
      right = right ^ temp << 8;
      /** @type {number} */
      temp = 858993459 & (right >>> 2 ^ left);
      /** @type {number} */
      left = left ^ temp;
      /** @type {number} */
      right = right ^ temp << 2;
      /** @type {number} */
      temp = 65535 & (left >>> 16 ^ right);
      /** @type {number} */
      right = right ^ temp;
      /** @type {number} */
      left = left ^ temp << 16;
      /** @type {number} */
      temp = 252645135 & (left >>> 4 ^ right);
      /** @type {number} */
      right = right ^ temp;
      /** @type {number} */
      left = left ^ temp << 4;
      /** @type {number} */
      size[0] = left;
      /** @type {number} */
      size[1] = right;
    }
    /**
     * @param {!Object} options
     * @return {?}
     */
    function _createCipher(options) {
      options = options || {};
      var t;
      var mode = (options.mode || "CBC").toUpperCase();
      /** @type {string} */
      var algorithm = "DES-" + mode;
      t = options.decrypt ? forge.cipher.createDecipher(algorithm, options.key) : forge.cipher.createCipher(algorithm, options.key);
      /** @type {function(string, !Object): undefined} */
      var n = t.start;
      return t.start = function(fn, options) {
        /** @type {null} */
        var dir = null;
        if (options instanceof forge.util.ByteBuffer) {
          /** @type {!Object} */
          dir = options;
          options = {};
        }
        options = options || {};
        options.output = dir;
        /** @type {string} */
        options.iv = fn;
        n.call(t, options);
      }, t;
    }
    var forge = FORGE(0);
    FORGE(12);
    FORGE(18);
    FORGE(1);
    mixin.exports = forge.des = forge.des || {};
    /**
     * @param {string} key
     * @param {string} val
     * @param {string} output
     * @param {string} mode
     * @return {?}
     */
    forge.des.startEncrypting = function(key, val, output, mode) {
      var acc = _createCipher({
        key : key,
        output : output,
        decrypt : false,
        mode : mode || (null === val ? "ECB" : "CBC")
      });
      return acc.start(val), acc;
    };
    /**
     * @param {number} key
     * @param {!Object} mode
     * @return {?}
     */
    forge.des.createEncryptionCipher = function(key, mode) {
      return _createCipher({
        key : key,
        output : null,
        decrypt : false,
        mode : mode
      });
    };
    /**
     * @param {string} e
     * @param {string} key
     * @param {string} output
     * @param {string} mode
     * @return {?}
     */
    forge.des.startDecrypting = function(e, key, output, mode) {
      var cipher = _createCipher({
        key : e,
        output : output,
        decrypt : true,
        mode : mode || (null === key ? "ECB" : "CBC")
      });
      return cipher.start(key), cipher;
    };
    /**
     * @param {string} key
     * @param {number} mode
     * @return {?}
     */
    forge.des.createDecryptionCipher = function(key, mode) {
      return _createCipher({
        key : key,
        output : null,
        decrypt : true,
        mode : mode
      });
    };
    /**
     * @param {string} name
     * @param {!Object} mode
     * @return {undefined}
     */
    forge.des.Algorithm = function(name, mode) {
      var self = this;
      /** @type {string} */
      self.name = name;
      self.mode = new mode({
        blockSize : 8,
        cipher : {
          encrypt : function(data, val) {
            return callback(self._keys, data, val, false);
          },
          decrypt : function(input, val) {
            return callback(self._keys, input, val, true);
          }
        }
      });
      /** @type {boolean} */
      self._init = false;
    };
    /**
     * @param {!Object} options
     * @return {undefined}
     */
    forge.des.Algorithm.prototype.initialize = function(options) {
      if (!this._init) {
        var key = forge.util.createBuffer(options.key);
        if (0 === this.name.indexOf("3DES") && 24 !== key.length()) {
          throw new Error("Invalid Triple-DES key size: " + 8 * key.length());
        }
        this._keys = _createKeys(key);
        /** @type {boolean} */
        this._init = true;
      }
    };
    registerAlgorithm("DES-ECB", forge.cipher.modes.ecb);
    registerAlgorithm("DES-CBC", forge.cipher.modes.cbc);
    registerAlgorithm("DES-CFB", forge.cipher.modes.cfb);
    registerAlgorithm("DES-OFB", forge.cipher.modes.ofb);
    registerAlgorithm("DES-CTR", forge.cipher.modes.ctr);
    registerAlgorithm("3DES-ECB", forge.cipher.modes.ecb);
    registerAlgorithm("3DES-CBC", forge.cipher.modes.cbc);
    registerAlgorithm("3DES-CFB", forge.cipher.modes.cfb);
    registerAlgorithm("3DES-OFB", forge.cipher.modes.ofb);
    registerAlgorithm("3DES-CTR", forge.cipher.modes.ctr);
    /** @type {!Array} */
    var c = [16843776, 0, 65536, 16843780, 16842756, 66564, 4, 65536, 1024, 16843776, 16843780, 1024, 16778244, 16842756, 16777216, 4, 1028, 16778240, 16778240, 66560, 66560, 16842752, 16842752, 16778244, 65540, 16777220, 16777220, 65540, 0, 1028, 66564, 16777216, 65536, 16843780, 4, 16842752, 16843776, 16777216, 16777216, 1024, 16842756, 65536, 66560, 16777220, 1024, 4, 16778244, 66564, 16843780, 65540, 16842752, 16778244, 16777220, 1028, 66564, 16843776, 1028, 16778240, 16778240, 0, 65540, 66560, 
    0, 16842756];
    /** @type {!Array} */
    var to_matched_rules = [-2146402272, -2147450880, 32768, 1081376, 1048576, 32, -2146435040, -2147450848, -2147483616, -2146402272, -2146402304, -2147483648, -2147450880, 1048576, 32, -2146435040, 1081344, 1048608, -2147450848, 0, -2147483648, 32768, 1081376, -2146435072, 1048608, -2147483616, 0, 1081344, 32800, -2146402304, -2146435072, 32800, 0, 1081376, -2146435040, 1048576, -2147450848, -2146435072, -2146402304, 32768, -2146435072, -2147450880, 32, -2146402272, 1081376, 32, 32768, -2147483648, 
    32800, -2146402304, 1048576, -2147483616, 1048608, -2147450848, -2147483616, 1048608, 1081344, 0, -2147450880, 32800, -2147483648, -2146435040, -2146402272, 1081344];
    /** @type {!Array} */
    var l = [520, 134349312, 0, 134348808, 134218240, 0, 131592, 134218240, 131080, 134217736, 134217736, 131072, 134349320, 131080, 134348800, 520, 134217728, 8, 134349312, 512, 131584, 134348800, 134348808, 131592, 134218248, 131584, 131072, 134218248, 8, 134349320, 512, 134217728, 134349312, 134217728, 131080, 520, 131072, 134349312, 134218240, 0, 512, 131080, 134349320, 134218240, 134217736, 512, 0, 134348808, 134218248, 131072, 134217728, 134349320, 8, 131592, 131584, 134217736, 134348800, 134218248, 
    520, 134348800, 131592, 8, 134348808, 131584];
    /** @type {!Array} */
    var to_already_matched_rules = [8396801, 8321, 8321, 128, 8396928, 8388737, 8388609, 8193, 0, 8396800, 8396800, 8396929, 129, 0, 8388736, 8388609, 1, 8192, 8388608, 8396801, 128, 8388608, 8193, 8320, 8388737, 1, 8320, 8388736, 8192, 8396928, 8396929, 129, 8388736, 8388609, 8396800, 8396929, 129, 0, 0, 8396800, 8320, 8388736, 8388737, 1, 8396801, 8321, 8321, 128, 8396929, 129, 1, 8192, 8388609, 8193, 8396928, 8388737, 8193, 8320, 8388608, 8396801, 128, 8388608, 8192, 8396928];
    /** @type {!Array} */
    var h = [256, 34078976, 34078720, 1107296512, 524288, 256, 1073741824, 34078720, 1074266368, 524288, 33554688, 1074266368, 1107296512, 1107820544, 524544, 1073741824, 33554432, 1074266112, 1074266112, 0, 1073742080, 1107820800, 1107820800, 33554688, 1107820544, 1073742080, 0, 1107296256, 34078976, 33554432, 1107296256, 524544, 524288, 1107296512, 256, 33554432, 1073741824, 34078720, 1107296512, 1074266368, 33554688, 1073741824, 1107820544, 34078976, 1074266368, 256, 33554432, 1107820544, 1107820800, 
    524544, 1107296256, 1107820800, 34078720, 0, 1074266112, 1107296256, 524544, 33554688, 1073742080, 524288, 0, 1074266112, 34078976, 1073742080];
    /** @type {!Array} */
    var f = [536870928, 541065216, 16384, 541081616, 541065216, 16, 541081616, 4194304, 536887296, 4210704, 4194304, 536870928, 4194320, 536887296, 536870912, 16400, 0, 4194320, 536887312, 16384, 4210688, 536887312, 16, 541065232, 541065232, 0, 4210704, 541081600, 16400, 4210688, 541081600, 536870912, 536887296, 16, 541065232, 4210688, 541081616, 4194304, 16400, 536870928, 4194304, 536887296, 536870912, 16400, 536870928, 541081616, 4210688, 541065216, 4210704, 541081600, 0, 541065232, 16, 16384, 
    541065216, 4210704, 16384, 4194320, 536887312, 0, 541081600, 536870912, 4194320, 536887312];
    /** @type {!Array} */
    var piTable = [2097152, 69206018, 67110914, 0, 2048, 67110914, 2099202, 69208064, 69208066, 2097152, 0, 67108866, 2, 67108864, 69206018, 2050, 67110912, 2099202, 2097154, 67110912, 67108866, 69206016, 69208064, 2097154, 69206016, 2048, 2050, 69208066, 2099200, 2, 67108864, 2099200, 67108864, 2099200, 2097152, 67110914, 67110914, 69206018, 69206018, 2, 2097154, 67108864, 67110912, 2097152, 69208064, 2050, 2099202, 69208064, 2050, 67108866, 69208066, 69206016, 2099200, 0, 2, 69208066, 0, 2099202, 
    69206016, 2048, 67108866, 67110912, 2048, 2097154];
    /** @type {!Array} */
    var _alignPrices = [268439616, 4096, 262144, 268701760, 268435456, 268439616, 64, 268435456, 262208, 268697600, 268701760, 266240, 268701696, 266304, 4096, 64, 268697600, 268435520, 268439552, 4160, 266240, 262208, 268697664, 268701696, 4160, 0, 0, 268697664, 268435520, 268439552, 266304, 262144, 266304, 262144, 268701696, 4096, 64, 268697664, 4096, 266304, 268439552, 64, 268435520, 268697600, 268697664, 268435456, 262144, 268439616, 0, 268701760, 262208, 268435520, 268697600, 268439552, 268439616, 
    0, 268701760, 266240, 266240, 4160, 4160, 262208, 268435456, 268701696];
  }, function(mixin, canCreateDiscussions, FORGE) {
    /**
     * @param {!Array} m
     * @param {!Object} key
     * @param {number} element
     * @return {?}
     */
    function _encodePkcs1_v1_5(m, key, element) {
      var value = forge.util.createBuffer();
      /** @type {number} */
      var k = Math.ceil(key.n.bitLength() / 8);
      if (m.length > k - 11) {
        /** @type {!Error} */
        var error = new Error("Message is too long for PKCS#1 v1.5 padding.");
        throw error.length = m.length, error.max = k - 11, error;
      }
      value.putByte(0);
      value.putByte(element);
      var num;
      /** @type {number} */
      var length = k - 3 - m.length;
      if (0 === element || 1 === element) {
        /** @type {number} */
        num = 0 === element ? 0 : 255;
        /** @type {number} */
        var i = 0;
        for (; i < length; ++i) {
          value.putByte(num);
        }
      } else {
        for (; length > 0;) {
          /** @type {number} */
          var remainingSpace = 0;
          var upper = forge.random.getBytes(length);
          /** @type {number} */
          i = 0;
          for (; i < length; ++i) {
            num = upper.charCodeAt(i);
            if (0 === num) {
              ++remainingSpace;
            } else {
              value.putByte(num);
            }
          }
          /** @type {number} */
          length = remainingSpace;
        }
      }
      return value.putByte(0), value.putBytes(m), value;
    }
    /**
     * @param {?} key
     * @param {!Object} state
     * @param {string} n
     * @param {number} b
     * @return {?}
     */
    function _decodePkcs1_v1_5(key, state, n, b) {
      /** @type {number} */
      var a = Math.ceil(state.n.bitLength() / 8);
      var bytes = forge.util.createBuffer(key);
      var s = bytes.getByte();
      var o = bytes.getByte();
      if (0 !== s || n && 0 !== o && 1 !== o || !n && 2 != o || n && 0 === o && "undefined" == typeof b) {
        throw new Error("Encryption block is invalid.");
      }
      /** @type {number} */
      var c = 0;
      if (0 === o) {
        /** @type {number} */
        c = a - 3 - b;
        /** @type {number} */
        var d = 0;
        for (; d < c; ++d) {
          if (0 !== bytes.getByte()) {
            throw new Error("Encryption block is invalid.");
          }
        }
      } else {
        if (1 === o) {
          /** @type {number} */
          c = 0;
          for (; bytes.length() > 1;) {
            if (255 !== bytes.getByte()) {
              --bytes.read;
              break;
            }
            ++c;
          }
        } else {
          if (2 === o) {
            /** @type {number} */
            c = 0;
            for (; bytes.length() > 1;) {
              if (0 === bytes.getByte()) {
                --bytes.read;
                break;
              }
              ++c;
            }
          }
        }
      }
      var row = bytes.getByte();
      if (0 !== row || c !== a - 3 - bytes.length()) {
        throw new Error("Encryption block is invalid.");
      }
      return bytes.getBytes();
    }
    /**
     * @param {!Object} state
     * @param {!Object} options
     * @param {!Object} callback
     * @return {undefined}
     */
    function _generateKeyPair(state, options, callback) {
      /**
       * @return {undefined}
       */
      function generate() {
        getPrime(state.pBits, function(value, tag) {
          return value ? callback(value) : (state.p = tag, null !== state.q ? finish(value, state.q) : void getPrime(state.qBits, finish));
        });
      }
      /**
       * @param {?} bits
       * @param {string} callback
       * @return {undefined}
       */
      function getPrime(bits, callback) {
        forge.prime.generateProbablePrime(bits, opts, callback);
      }
      /**
       * @param {?} uri
       * @param {string} c
       * @return {?}
       */
      function finish(uri, c) {
        if (uri) {
          return callback(uri);
        }
        if (state.q = c, state.p.compareTo(state.q) < 0) {
          var tmp = state.p;
          state.p = state.q;
          state.q = tmp;
        }
        if (0 !== state.p.subtract(BigInteger.ONE).gcd(state.e).compareTo(BigInteger.ONE)) {
          return state.p = null, void generate();
        }
        if (0 !== state.q.subtract(BigInteger.ONE).gcd(state.e).compareTo(BigInteger.ONE)) {
          return state.q = null, void getPrime(state.qBits, finish);
        }
        if (state.p1 = state.p.subtract(BigInteger.ONE), state.q1 = state.q.subtract(BigInteger.ONE), state.phi = state.p1.multiply(state.q1), 0 !== state.phi.gcd(state.e).compareTo(BigInteger.ONE)) {
          return state.p = state.q = null, void generate();
        }
        if (state.n = state.p.multiply(state.q), state.n.bitLength() !== state.bits) {
          return state.q = null, void getPrime(state.qBits, finish);
        }
        var d = state.e.modInverse(state.phi);
        state.keys = {
          privateKey : pki.rsa.setPrivateKey(state.n, state.e, d, state.p, state.q, d.mod(state.p1), d.mod(state.q1), state.q.modInverse(state.p)),
          publicKey : pki.rsa.setPublicKey(state.n, state.e)
        };
        callback(null, state.keys);
      }
      if ("function" == typeof options) {
        /** @type {!Object} */
        callback = options;
        options = {};
      }
      options = options || {};
      var opts = {
        algorithm : {
          name : options.algorithm || "PRIMEINC",
          options : {
            workers : options.workers || 2,
            workLoad : options.workLoad || 100,
            workerScript : options.workerScript
          }
        }
      };
      if ("prng" in options) {
        opts.prng = options.prng;
      }
      generate();
    }
    /**
     * @param {string} b
     * @return {?}
     */
    function _bnToBytes(b) {
      var hash = b.toString(16);
      if (hash[0] >= "8") {
        /** @type {string} */
        hash = "00" + hash;
      }
      var r = forge.util.hexToBytes(hash);
      return r.length > 1 && (0 === r.charCodeAt(0) && 0 === (128 & r.charCodeAt(1)) || 255 === r.charCodeAt(0) && 128 === (128 & r.charCodeAt(1))) ? r.substr(1) : r;
    }
    /**
     * @param {number} bits
     * @return {?}
     */
    function _getMillerRabinTests(bits) {
      return bits <= 100 ? 27 : bits <= 150 ? 18 : bits <= 200 ? 15 : bits <= 250 ? 12 : bits <= 300 ? 9 : bits <= 350 ? 8 : bits <= 400 ? 7 : bits <= 500 ? 6 : bits <= 600 ? 5 : bits <= 800 ? 4 : bits <= 1250 ? 3 : 2;
    }
    /**
     * @param {string} fn
     * @return {?}
     */
    function _detectSubtleCrypto(fn) {
      return "undefined" != typeof window && "object" == typeof window.crypto && "object" == typeof window.crypto.subtle && "function" == typeof window.crypto.subtle[fn];
    }
    /**
     * @param {string} fn
     * @return {?}
     */
    function _detectSubtleMsCrypto(fn) {
      return "undefined" != typeof window && "object" == typeof window.msCrypto && "object" == typeof window.msCrypto.subtle && "function" == typeof window.msCrypto.subtle[fn];
    }
    /**
     * @param {number} x
     * @return {?}
     */
    function _intToUint8Array(x) {
      var testText = forge.util.hexToBytes(x.toString(16));
      /** @type {!Uint8Array} */
      var buffer = new Uint8Array(testText.length);
      /** @type {number} */
      var i = 0;
      for (; i < testText.length; ++i) {
        buffer[i] = testText.charCodeAt(i);
      }
      return buffer;
    }
    var forge = FORGE(0);
    if (FORGE(3), FORGE(13), FORGE(6), FORGE(23), FORGE(27), FORGE(2), FORGE(1), "undefined" == typeof BigInteger) {
      var BigInteger = forge.jsbn.BigInteger;
    }
    var asn1 = forge.asn1;
    forge.pki = forge.pki || {};
    mixin.exports = forge.pki.rsa = forge.rsa = forge.rsa || {};
    var pki = forge.pki;
    /** @type {!Array} */
    var GCD_30_DELTA = [6, 4, 2, 4, 2, 4, 6, 2];
    var x509CertificateValidator = {
      name : "PrivateKeyInfo",
      tagClass : asn1.Class.UNIVERSAL,
      type : asn1.Type.SEQUENCE,
      constructed : true,
      value : [{
        name : "PrivateKeyInfo.version",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.INTEGER,
        constructed : false,
        capture : "privateKeyVersion"
      }, {
        name : "PrivateKeyInfo.privateKeyAlgorithm",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.SEQUENCE,
        constructed : true,
        value : [{
          name : "AlgorithmIdentifier.algorithm",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.OID,
          constructed : false,
          capture : "privateKeyOid"
        }]
      }, {
        name : "PrivateKeyInfo",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.OCTETSTRING,
        constructed : false,
        capture : "privateKey"
      }]
    };
    var rsaPrivateKeyValidator = {
      name : "RSAPrivateKey",
      tagClass : asn1.Class.UNIVERSAL,
      type : asn1.Type.SEQUENCE,
      constructed : true,
      value : [{
        name : "RSAPrivateKey.version",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.INTEGER,
        constructed : false,
        capture : "privateKeyVersion"
      }, {
        name : "RSAPrivateKey.modulus",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.INTEGER,
        constructed : false,
        capture : "privateKeyModulus"
      }, {
        name : "RSAPrivateKey.publicExponent",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.INTEGER,
        constructed : false,
        capture : "privateKeyPublicExponent"
      }, {
        name : "RSAPrivateKey.privateExponent",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.INTEGER,
        constructed : false,
        capture : "privateKeyPrivateExponent"
      }, {
        name : "RSAPrivateKey.prime1",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.INTEGER,
        constructed : false,
        capture : "privateKeyPrime1"
      }, {
        name : "RSAPrivateKey.prime2",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.INTEGER,
        constructed : false,
        capture : "privateKeyPrime2"
      }, {
        name : "RSAPrivateKey.exponent1",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.INTEGER,
        constructed : false,
        capture : "privateKeyExponent1"
      }, {
        name : "RSAPrivateKey.exponent2",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.INTEGER,
        constructed : false,
        capture : "privateKeyExponent2"
      }, {
        name : "RSAPrivateKey.coefficient",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.INTEGER,
        constructed : false,
        capture : "privateKeyCoefficient"
      }]
    };
    var rsaPublicKeyValidator = {
      name : "RSAPublicKey",
      tagClass : asn1.Class.UNIVERSAL,
      type : asn1.Type.SEQUENCE,
      constructed : true,
      value : [{
        name : "RSAPublicKey.modulus",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.INTEGER,
        constructed : false,
        capture : "publicKeyModulus"
      }, {
        name : "RSAPublicKey.exponent",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.INTEGER,
        constructed : false,
        capture : "publicKeyExponent"
      }]
    };
    var type = forge.pki.rsa.publicKeyValidator = {
      name : "SubjectPublicKeyInfo",
      tagClass : asn1.Class.UNIVERSAL,
      type : asn1.Type.SEQUENCE,
      constructed : true,
      captureAsn1 : "subjectPublicKeyInfo",
      value : [{
        name : "SubjectPublicKeyInfo.AlgorithmIdentifier",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.SEQUENCE,
        constructed : true,
        value : [{
          name : "AlgorithmIdentifier.algorithm",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.OID,
          constructed : false,
          capture : "publicKeyOid"
        }]
      }, {
        name : "SubjectPublicKeyInfo.subjectPublicKey",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.BITSTRING,
        constructed : false,
        value : [{
          name : "SubjectPublicKeyInfo.subjectPublicKey.RSAPublicKey",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.SEQUENCE,
          constructed : true,
          optional : true,
          captureAsn1 : "rsaPublicKey"
        }]
      }]
    };
    /**
     * @param {!Object} e
     * @return {?}
     */
    var emsaPkcs1v15encode = function(e) {
      var oid;
      if (!(e.algorithm in pki.oids)) {
        /** @type {!Error} */
        var error = new Error("Unknown message digest algorithm.");
        throw error.algorithm = e.algorithm, error;
      }
      oid = pki.oids[e.algorithm];
      var value = asn1.oidToDer(oid).getBytes();
      var digestInfo = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, []);
      var month = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, []);
      month.value.push(asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, value));
      month.value.push(asn1.create(asn1.Class.UNIVERSAL, asn1.Type.NULL, false, ""));
      var tmpDate = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OCTETSTRING, false, e.digest().getBytes());
      return digestInfo.value.push(month), digestInfo.value.push(tmpDate), asn1.toDer(digestInfo).getBytes();
    };
    /**
     * @param {number} x
     * @param {!Object} key
     * @param {string} pub
     * @return {?}
     */
    var _modPow = function(x, key, pub) {
      if (pub) {
        return x.modPow(key.e, key.n);
      }
      if (!key.p || !key.q) {
        return x.modPow(key.d, key.n);
      }
      if (!key.dP) {
        key.dP = key.d.mod(key.p.subtract(BigInteger.ONE));
      }
      if (!key.dQ) {
        key.dQ = key.d.mod(key.q.subtract(BigInteger.ONE));
      }
      if (!key.qInv) {
        key.qInv = key.q.modInverse(key.p);
      }
      var r;
      do {
        r = new BigInteger(forge.util.bytesToHex(forge.random.getBytes(key.n.bitLength() / 8)), 16);
      } while (r.compareTo(key.n) >= 0 || !r.gcd(key.n).equals(BigInteger.ONE));
      x = x.multiply(r.modPow(key.e, key.n)).mod(key.n);
      var t = x.mod(key.p).modPow(key.dP, key.p);
      var amount = x.mod(key.q).modPow(key.dQ, key.q);
      for (; t.compareTo(amount) < 0;) {
        t = t.add(key.p);
      }
      var C = t.subtract(amount).multiply(key.qInv).mod(key.p).multiply(key.q).add(amount);
      return C = C.multiply(r.modInverse(key.n)).mod(key.n);
    };
    /**
     * @param {!Array} m
     * @param {!Object} key
     * @param {number} bt
     * @return {?}
     */
    pki.rsa.encrypt = function(m, key, bt) {
      var eb;
      /** @type {number} */
      var pub = bt;
      /** @type {number} */
      var s = Math.ceil(key.n.bitLength() / 8);
      if (bt !== false && bt !== true) {
        /** @type {boolean} */
        pub = 2 === bt;
        eb = _encodePkcs1_v1_5(m, key, bt);
      } else {
        eb = forge.util.createBuffer();
        eb.putBytes(m);
      }
      var y = new BigInteger(eb.toHex(), 16);
      var x = _modPow(y, key, pub);
      var yhex = x.toString(16);
      var ed = forge.util.createBuffer();
      /** @type {number} */
      var sr = s - Math.ceil(yhex.length / 2);
      for (; sr > 0;) {
        ed.putByte(0);
        --sr;
      }
      return ed.putBytes(forge.util.hexToBytes(yhex)), ed.getBytes();
    };
    /**
     * @param {!Array} input
     * @param {!Object} key
     * @param {string} pub
     * @param {boolean} ml
     * @return {?}
     */
    pki.rsa.decrypt = function(input, key, pub, ml) {
      /** @type {number} */
      var value = Math.ceil(key.n.bitLength() / 8);
      if (input.length !== value) {
        /** @type {!Error} */
        var obj = new Error("Encrypted message length is invalid.");
        throw obj.length = input.length, obj.expected = value, obj;
      }
      var y = new BigInteger(forge.util.createBuffer(input).toHex(), 16);
      if (y.compareTo(key.n) >= 0) {
        throw new Error("Encrypted message is invalid.");
      }
      var x = _modPow(y, key, pub);
      var yhex = x.toString(16);
      var ed = forge.util.createBuffer();
      /** @type {number} */
      var da = value - Math.ceil(yhex.length / 2);
      for (; da > 0;) {
        ed.putByte(0);
        --da;
      }
      return ed.putBytes(forge.util.hexToBytes(yhex)), ml !== false ? _decodePkcs1_v1_5(ed.getBytes(), key, pub) : ed.getBytes();
    };
    /**
     * @param {number} bits
     * @param {number} e
     * @param {!Object} options
     * @return {?}
     */
    pki.rsa.createKeyPairGenerationState = function(bits, e, options) {
      if ("string" == typeof bits) {
        /** @type {number} */
        bits = parseInt(bits, 10);
      }
      bits = bits || 2048;
      options = options || {};
      var rval;
      var prng = options.prng || forge.random;
      var rng = {
        nextBytes : function(x) {
          var stripSlashes = prng.getBytesSync(x.length);
          /** @type {number} */
          var i = 0;
          for (; i < x.length; ++i) {
            x[i] = stripSlashes.charCodeAt(i);
          }
        }
      };
      var algorithm = options.algorithm || "PRIMEINC";
      if ("PRIMEINC" !== algorithm) {
        throw new Error("Invalid key generation algorithm: " + algorithm);
      }
      return rval = {
        algorithm : algorithm,
        state : 0,
        bits : bits,
        rng : rng,
        eInt : e || 65537,
        e : new BigInteger(null),
        p : null,
        q : null,
        qBits : bits >> 1,
        pBits : bits - (bits >> 1),
        pqState : 0,
        num : null,
        keys : null
      }, rval.e.fromInt(rval.eInt), rval;
    };
    /**
     * @param {!Object} state
     * @param {number} n
     * @return {?}
     */
    pki.rsa.stepKeyPairGenerationState = function(state, n) {
      if (!("algorithm" in state)) {
        /** @type {string} */
        state.algorithm = "PRIMEINC";
      }
      var THIRTY = new BigInteger(null);
      THIRTY.fromInt(30);
      var t2;
      /** @type {number} */
      var n = 0;
      /**
       * @param {number} y
       * @param {number} mask
       * @return {?}
       */
      var op_or = function(y, mask) {
        return y | mask;
      };
      /** @type {number} */
      var t1 = +new Date;
      /** @type {number} */
      var total = 0;
      for (; null === state.keys && (n <= 0 || total < n);) {
        if (0 === state.state) {
          var bits = null === state.p ? state.pBits : state.qBits;
          /** @type {number} */
          var bits1 = bits - 1;
          if (0 === state.pqState) {
            state.num = new BigInteger(bits, state.rng);
            if (!state.num.testBit(bits1)) {
              state.num.bitwiseTo(BigInteger.ONE.shiftLeft(bits1), op_or, state.num);
            }
            state.num.dAddOffset(31 - state.num.mod(THIRTY).byteValue(), 0);
            /** @type {number} */
            n = 0;
            ++state.pqState;
          } else {
            if (1 === state.pqState) {
              if (state.num.bitLength() > bits) {
                /** @type {number} */
                state.pqState = 0;
              } else {
                if (state.num.isProbablePrime(_getMillerRabinTests(state.num.bitLength()))) {
                  ++state.pqState;
                } else {
                  state.num.dAddOffset(GCD_30_DELTA[n++ % 8], 0);
                }
              }
            } else {
              if (2 === state.pqState) {
                /** @type {number} */
                state.pqState = 0 === state.num.subtract(BigInteger.ONE).gcd(state.e).compareTo(BigInteger.ONE) ? 3 : 0;
              } else {
                if (3 === state.pqState) {
                  /** @type {number} */
                  state.pqState = 0;
                  if (null === state.p) {
                    state.p = state.num;
                  } else {
                    state.q = state.num;
                  }
                  if (null !== state.p && null !== state.q) {
                    ++state.state;
                  }
                  /** @type {null} */
                  state.num = null;
                }
              }
            }
          }
        } else {
          if (1 === state.state) {
            if (state.p.compareTo(state.q) < 0) {
              state.num = state.p;
              state.p = state.q;
              state.q = state.num;
            }
            ++state.state;
          } else {
            if (2 === state.state) {
              state.p1 = state.p.subtract(BigInteger.ONE);
              state.q1 = state.q.subtract(BigInteger.ONE);
              state.phi = state.p1.multiply(state.q1);
              ++state.state;
            } else {
              if (3 === state.state) {
                if (0 === state.phi.gcd(state.e).compareTo(BigInteger.ONE)) {
                  ++state.state;
                } else {
                  /** @type {null} */
                  state.p = null;
                  /** @type {null} */
                  state.q = null;
                  /** @type {number} */
                  state.state = 0;
                }
              } else {
                if (4 === state.state) {
                  state.n = state.p.multiply(state.q);
                  if (state.n.bitLength() === state.bits) {
                    ++state.state;
                  } else {
                    /** @type {null} */
                    state.q = null;
                    /** @type {number} */
                    state.state = 0;
                  }
                } else {
                  if (5 === state.state) {
                    var d = state.e.modInverse(state.phi);
                    state.keys = {
                      privateKey : pki.rsa.setPrivateKey(state.n, state.e, d, state.p, state.q, d.mod(state.p1), d.mod(state.q1), state.q.modInverse(state.p)),
                      publicKey : pki.rsa.setPublicKey(state.n, state.e)
                    };
                  }
                }
              }
            }
          }
        }
        /** @type {number} */
        t2 = +new Date;
        /** @type {number} */
        total = total + (t2 - t1);
        /** @type {number} */
        t1 = t2;
      }
      return null !== state.keys;
    };
    /**
     * @param {number} value
     * @param {number} e
     * @param {number} options
     * @param {number} callback
     * @return {?}
     */
    pki.rsa.generateKeyPair = function(value, e, options, callback) {
      if (1 === arguments.length ? "object" == typeof value ? (options = value, value = void 0) : "function" == typeof value && (callback = value, value = void 0) : 2 === arguments.length ? "number" == typeof value ? "function" == typeof e ? (callback = e, e = void 0) : "number" != typeof e && (options = e, e = void 0) : (options = value, callback = e, value = void 0, e = void 0) : 3 === arguments.length && ("number" == typeof e ? "function" == typeof options && (callback = options, options = void 0) : 
      (callback = options, options = e, e = void 0)), options = options || {}, void 0 === value && (value = options.bits || 2048), void 0 === e && (e = options.e || 65537), !forge.options.usePureJavaScript && callback && value >= 256 && value <= 16384 && (65537 === e || 3 === e)) {
        if (_detectSubtleCrypto("generateKey") && _detectSubtleCrypto("exportKey")) {
          return window.crypto.subtle.generateKey({
            name : "RSASSA-PKCS1-v1_5",
            modulusLength : value,
            publicExponent : _intToUint8Array(e),
            hash : {
              name : "SHA-256"
            }
          }, true, ["sign", "verify"]).then(function(keypair) {
            return window.crypto.subtle.exportKey("pkcs8", keypair.privateKey);
          }).then(void 0, function(identifierPositions) {
            callback(identifierPositions);
          }).then(function(pkcs8) {
            if (pkcs8) {
              var privateKey = pki.privateKeyFromAsn1(asn1.fromDer(forge.util.createBuffer(pkcs8)));
              callback(null, {
                privateKey : privateKey,
                publicKey : pki.setRsaPublicKey(privateKey.n, privateKey.e)
              });
            }
          });
        }
        if (_detectSubtleMsCrypto("generateKey") && _detectSubtleMsCrypto("exportKey")) {
          var hashOp = window.msCrypto.subtle.generateKey({
            name : "RSASSA-PKCS1-v1_5",
            modulusLength : value,
            publicExponent : _intToUint8Array(e),
            hash : {
              name : "SHA-256"
            }
          }, true, ["sign", "verify"]);
          return hashOp.oncomplete = function(event) {
            var pair = event.target.result;
            var exportOp = window.msCrypto.subtle.exportKey("pkcs8", pair.privateKey);
            /**
             * @param {!Event} event
             * @return {undefined}
             */
            exportOp.oncomplete = function(event) {
              var pkcs8 = event.target.result;
              var privateKey = pki.privateKeyFromAsn1(asn1.fromDer(forge.util.createBuffer(pkcs8)));
              callback(null, {
                privateKey : privateKey,
                publicKey : pki.setRsaPublicKey(privateKey.n, privateKey.e)
              });
            };
            /**
             * @param {?} theError
             * @return {undefined}
             */
            exportOp.onerror = function(theError) {
              callback(theError);
            };
          }, void(hashOp.onerror = function(theError) {
            callback(theError);
          });
        }
      }
      var state = pki.rsa.createKeyPairGenerationState(value, e, options);
      return callback ? void _generateKeyPair(state, options, callback) : (pki.rsa.stepKeyPairGenerationState(state, 0), state.keys);
    };
    /** @type {function(string, !Function): ?} */
    pki.setRsaPublicKey = pki.rsa.setPublicKey = function(n, e) {
      var key = {
        n : n,
        e : e
      };
      return key.encrypt = function(data, val, local) {
        if ("string" == typeof val ? val = val.toUpperCase() : void 0 === val && (val = "RSAES-PKCS1-V1_5"), "RSAES-PKCS1-V1_5" === val) {
          val = {
            encode : function(m, key, reqId) {
              return _encodePkcs1_v1_5(m, key, 2).getBytes();
            }
          };
        } else {
          if ("RSA-OAEP" === val || "RSAES-OAEP" === val) {
            val = {
              encode : function(s, key) {
                return forge.pkcs1.encode_rsa_oaep(key, s, local);
              }
            };
          } else {
            if (["RAW", "NONE", "NULL", null].indexOf(val) !== -1) {
              val = {
                encode : function(str) {
                  return str;
                }
              };
            } else {
              if ("string" == typeof val) {
                throw new Error('Unsupported encryption scheme: "' + val + '".');
              }
            }
          }
        }
        var i = val.encode(data, key, true);
        return pki.rsa.encrypt(i, key, true);
      }, key.verify = function(c, data, type) {
        if ("string" == typeof type) {
          /** @type {string} */
          type = type.toUpperCase();
        } else {
          if (void 0 === type) {
            /** @type {string} */
            type = "RSASSA-PKCS1-V1_5";
          }
        }
        if ("RSASSA-PKCS1-V1_5" === type) {
          type = {
            verify : function(id, d) {
              d = _decodePkcs1_v1_5(d, key, true);
              var dname = asn1.fromDer(d);
              return id === dname.value[1].value;
            }
          };
        } else {
          if (!("NONE" !== type && "NULL" !== type && null !== type)) {
            type = {
              verify : function(c, d) {
                return d = _decodePkcs1_v1_5(d, key, true), c === d;
              }
            };
          }
        }
        var d = pki.rsa.decrypt(data, key, true, false);
        return type.verify(c, d, key.n.bitLength());
      }, key;
    };
    /** @type {function(string, number, !Object, string, string, ?, ?, ?): ?} */
    pki.setRsaPrivateKey = pki.rsa.setPrivateKey = function(n, e, d, p, q, dP, dQ, qInv) {
      var key = {
        n : n,
        e : e,
        d : d,
        p : p,
        q : q,
        dP : dP,
        dQ : dQ,
        qInv : qInv
      };
      return key.decrypt = function(input, val, self) {
        if ("string" == typeof val) {
          /** @type {string} */
          val = val.toUpperCase();
        } else {
          if (void 0 === val) {
            /** @type {string} */
            val = "RSAES-PKCS1-V1_5";
          }
        }
        var e = pki.rsa.decrypt(input, key, false, false);
        if ("RSAES-PKCS1-V1_5" === val) {
          val = {
            decode : _decodePkcs1_v1_5
          };
        } else {
          if ("RSA-OAEP" === val || "RSAES-OAEP" === val) {
            val = {
              decode : function(input, key) {
                return forge.pkcs1.decode_rsa_oaep(key, input, self);
              }
            };
          } else {
            if (["RAW", "NONE", "NULL", null].indexOf(val) === -1) {
              throw new Error('Unsupported encryption scheme: "' + val + '".');
            }
            val = {
              decode : function(obj) {
                return obj;
              }
            };
          }
        }
        return val.decode(e, key, false);
      }, key.sign = function(data, user) {
        /** @type {boolean} */
        var 1 = false;
        if ("string" == typeof user) {
          /** @type {string} */
          user = user.toUpperCase();
        }
        if (void 0 === user || "RSASSA-PKCS1-V1_5" === user) {
          user = {
            encode : emsaPkcs1v15encode
          };
          /** @type {number} */
          1 = 1;
        } else {
          if (!("NONE" !== user && "NULL" !== user && null !== user)) {
            user = {
              encode : function() {
                return data;
              }
            };
            /** @type {number} */
            1 = 1;
          }
        }
        var a = user.encode(data, key.n.bitLength());
        return pki.rsa.encrypt(a, key, 1);
      }, key;
    };
    /**
     * @param {string} rsaKey
     * @return {?}
     */
    pki.wrapRsaPrivateKey = function(rsaKey) {
      return asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, asn1.integerToDer(0).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(pki.oids.rsaEncryption).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.NULL, false, "")]), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OCTETSTRING, false, asn1.toDer(rsaKey).getBytes())]);
    };
    /**
     * @param {string} value
     * @return {?}
     */
    pki.privateKeyFromAsn1 = function(value) {
      var capture = {};
      /** @type {!Array} */
      var errors = [];
      if (asn1.validate(value, x509CertificateValidator, capture, errors) && (value = asn1.fromDer(forge.util.createBuffer(capture.privateKey))), capture = {}, errors = [], !asn1.validate(value, rsaPrivateKeyValidator, capture, errors)) {
        /** @type {!Error} */
        var error = new Error("Cannot read private key. ASN.1 object does not contain an RSAPrivateKey.");
        throw error.errors = errors, error;
      }
      var n;
      var e;
      var d;
      var p;
      var q;
      var dP;
      var dQ;
      var qInv;
      return n = forge.util.createBuffer(capture.privateKeyModulus).toHex(), e = forge.util.createBuffer(capture.privateKeyPublicExponent).toHex(), d = forge.util.createBuffer(capture.privateKeyPrivateExponent).toHex(), p = forge.util.createBuffer(capture.privateKeyPrime1).toHex(), q = forge.util.createBuffer(capture.privateKeyPrime2).toHex(), dP = forge.util.createBuffer(capture.privateKeyExponent1).toHex(), dQ = forge.util.createBuffer(capture.privateKeyExponent2).toHex(), qInv = forge.util.createBuffer(capture.privateKeyCoefficient).toHex(), 
      pki.setRsaPrivateKey(new BigInteger(n, 16), new BigInteger(e, 16), new BigInteger(d, 16), new BigInteger(p, 16), new BigInteger(q, 16), new BigInteger(dP, 16), new BigInteger(dQ, 16), new BigInteger(qInv, 16));
    };
    /** @type {function(!Object): ?} */
    pki.privateKeyToAsn1 = pki.privateKeyToRSAPrivateKey = function(key) {
      return asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, asn1.integerToDer(0).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, _bnToBytes(key.n)), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, _bnToBytes(key.e)), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, _bnToBytes(key.d)), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, _bnToBytes(key.p)), asn1.create(asn1.Class.UNIVERSAL, 
      asn1.Type.INTEGER, false, _bnToBytes(key.q)), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, _bnToBytes(key.dP)), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, _bnToBytes(key.dQ)), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, _bnToBytes(key.qInv))]);
    };
    /**
     * @param {string} obj
     * @return {?}
     */
    pki.publicKeyFromAsn1 = function(obj) {
      var capture = {};
      /** @type {!Array} */
      var errors = [];
      if (asn1.validate(obj, type, capture, errors)) {
        var oid = asn1.derToOid(capture.publicKeyOid);
        if (oid !== pki.oids.rsaEncryption) {
          /** @type {!Error} */
          var error = new Error("Cannot read public key. Unknown OID.");
          throw error.oid = oid, error;
        }
        obj = capture.rsaPublicKey;
      }
      if (errors = [], !asn1.validate(obj, rsaPublicKeyValidator, capture, errors)) {
        /** @type {!Error} */
        error = new Error("Cannot read public key. ASN.1 object does not contain an RSAPublicKey.");
        throw error.errors = errors, error;
      }
      var n = forge.util.createBuffer(capture.publicKeyModulus).toHex();
      var e = forge.util.createBuffer(capture.publicKeyExponent).toHex();
      return pki.setRsaPublicKey(new BigInteger(n, 16), new BigInteger(e, 16));
    };
    /** @type {function(!Object): ?} */
    pki.publicKeyToAsn1 = pki.publicKeyToSubjectPublicKeyInfo = function(key) {
      return asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(pki.oids.rsaEncryption).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.NULL, false, "")]), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.BITSTRING, false, [pki.publicKeyToRSAPublicKey(key)])]);
    };
    /**
     * @param {!Object} key
     * @return {?}
     */
    pki.publicKeyToRSAPublicKey = function(key) {
      return asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, _bnToBytes(key.n)), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, _bnToBytes(key.e))]);
    };
  }, function(mixin, canCreateDiscussions, FORGE) {
    var forge = FORGE(0);
    FORGE(1);
    mixin.exports = forge.cipher = forge.cipher || {};
    forge.cipher.algorithms = forge.cipher.algorithms || {};
    /**
     * @param {string} algorithm
     * @param {string} options
     * @return {?}
     */
    forge.cipher.createCipher = function(algorithm, options) {
      /** @type {string} */
      var api = algorithm;
      if ("string" == typeof api && (api = forge.cipher.getAlgorithm(api), api && (api = api())), !api) {
        throw new Error("Unsupported algorithm: " + algorithm);
      }
      return new forge.cipher.BlockCipher({
        algorithm : api,
        key : options,
        decrypt : false
      });
    };
    /**
     * @param {string} algorithm
     * @param {!Object} options
     * @return {?}
     */
    forge.cipher.createDecipher = function(algorithm, options) {
      /** @type {string} */
      var api = algorithm;
      if ("string" == typeof api && (api = forge.cipher.getAlgorithm(api), api && (api = api())), !api) {
        throw new Error("Unsupported algorithm: " + algorithm);
      }
      return new forge.cipher.BlockCipher({
        algorithm : api,
        key : options,
        decrypt : true
      });
    };
    /**
     * @param {string} name
     * @param {!Function} callback
     * @return {undefined}
     */
    forge.cipher.registerAlgorithm = function(name, callback) {
      name = name.toUpperCase();
      /** @type {!Function} */
      forge.cipher.algorithms[name] = callback;
    };
    /**
     * @param {string} name
     * @return {?}
     */
    forge.cipher.getAlgorithm = function(name) {
      return name = name.toUpperCase(), name in forge.cipher.algorithms ? forge.cipher.algorithms[name] : null;
    };
    /** @type {function(!Object): undefined} */
    var Timer = forge.cipher.BlockCipher = function(options) {
      this.algorithm = options.algorithm;
      this.mode = this.algorithm.mode;
      this.blockSize = this.mode.blockSize;
      /** @type {boolean} */
      this._finish = false;
      /** @type {null} */
      this._input = null;
      /** @type {null} */
      this.output = null;
      this._op = options.decrypt ? this.mode.decrypt : this.mode.encrypt;
      this._decrypt = options.decrypt;
      this.algorithm.initialize(options);
    };
    /**
     * @param {string} data
     * @return {undefined}
     */
    Timer.prototype.start = function(data) {
      data = data || {};
      var opts = {};
      var i;
      for (i in data) {
        opts[i] = data[i];
      }
      opts.decrypt = this._decrypt;
      /** @type {boolean} */
      this._finish = false;
      this._input = forge.util.createBuffer();
      this.output = data.output || forge.util.createBuffer();
      this.mode.start(opts);
    };
    /**
     * @param {!Object} input
     * @return {undefined}
     */
    Timer.prototype.update = function(input) {
      if (input) {
        this._input.putBuffer(input);
      }
      for (; !this._op.call(this.mode, this._input, this.output, this._finish) && !this._finish;) {
      }
      this._input.compact();
    };
    /**
     * @param {!Function} pad
     * @return {?}
     */
    Timer.prototype.finish = function(pad) {
      if (!(!pad || "ECB" !== this.mode.name && "CBC" !== this.mode.name)) {
        /**
         * @param {?} value
         * @return {?}
         */
        this.mode.pad = function(value) {
          return pad(this.blockSize, value, false);
        };
        /**
         * @param {!Array} value
         * @return {?}
         */
        this.mode.unpad = function(value) {
          return pad(this.blockSize, value, true);
        };
      }
      var options = {};
      return options.decrypt = this._decrypt, options.overflow = this._input.length() % this.blockSize, !(!this._decrypt && this.mode.pad && !this.mode.pad(this._input, options)) && (this._finish = true, this.update(), !(this._decrypt && this.mode.unpad && !this.mode.unpad(this.output, options)) && !(this.mode.afterFinish && !this.mode.afterFinish(this.output, options)));
    };
  }, function(mixin, canCreateDiscussions, FORGE) {
    /**
     * @param {number} value
     * @param {number} a
     * @param {undefined} b
     * @return {undefined}
     */
    function BigInteger(value, a, b) {
      /** @type {!Array} */
      this.data = [];
      if (null != value) {
        if ("number" == typeof value) {
          this.fromNumber(value, a, b);
        } else {
          if (null == a && "string" != typeof value) {
            this.fromString(value, 256);
          } else {
            this.fromString(value, a);
          }
        }
      }
    }
    /**
     * @return {?}
     */
    function nbi() {
      return new BigInteger(null);
    }
    /**
     * @param {number} index
     * @param {number} x
     * @param {!Object} l
     * @param {number} i
     * @param {number} s
     * @param {number} n
     * @return {?}
     */
    function r(index, x, l, i, s, n) {
      for (; --n >= 0;) {
        var seconds = x * this.data[index++] + l.data[i] + s;
        /** @type {number} */
        s = Math.floor(seconds / 67108864);
        /** @type {number} */
        l.data[i++] = 67108863 & seconds;
      }
      return s;
    }
    /**
     * @param {number} pos
     * @param {number} x
     * @param {!Object} i
     * @param {number} n
     * @param {number} c
     * @param {number} val
     * @return {?}
     */
    function am1(pos, x, i, n, c, val) {
      /** @type {number} */
      var s = 32767 & x;
      /** @type {number} */
      var m10 = x >> 15;
      for (; --val >= 0;) {
        /** @type {number} */
        var x = 32767 & this.data[pos];
        /** @type {number} */
        var m12 = this.data[pos++] >> 15;
        /** @type {number} */
        var y = m10 * x + m12 * s;
        x = s * x + ((32767 & y) << 15) + i.data[n] + (1073741823 & c);
        /** @type {number} */
        c = (x >>> 30) + (y >>> 15) + m10 * m12 + (c >>> 30);
        /** @type {number} */
        i.data[n++] = 1073741823 & x;
      }
      return c;
    }
    /**
     * @param {number} pos
     * @param {number} x
     * @param {!Object} s
     * @param {number} i
     * @param {number} value
     * @param {number} array
     * @return {?}
     */
    function getActivationStyle(pos, x, s, i, value, array) {
      /** @type {number} */
      var z = 16383 & x;
      /** @type {number} */
      var y = x >> 14;
      for (; --array >= 0;) {
        /** @type {number} */
        var x = 16383 & this.data[pos];
        /** @type {number} */
        var qy = this.data[pos++] >> 14;
        /** @type {number} */
        var b = y * x + qy * z;
        x = z * x + ((16383 & b) << 14) + s.data[i] + value;
        /** @type {number} */
        value = (x >> 28) + (b >> 14) + y * qy;
        /** @type {number} */
        s.data[i++] = 268435455 & x;
      }
      return value;
    }
    /**
     * @param {!Object} n
     * @return {?}
     */
    function int2char(n) {
      return frg.charAt(n);
    }
    /**
     * @param {string} s
     * @param {number} i
     * @return {?}
     */
    function intAt(s, i) {
      var d = dt[s.charCodeAt(i)];
      return null == d ? -1 : d;
    }
    /**
     * @param {!Object} r
     * @return {undefined}
     */
    function bnpCopyTo(r) {
      /** @type {number} */
      var i = this.t - 1;
      for (; i >= 0; --i) {
        r.data[i] = this.data[i];
      }
      r.t = this.t;
      r.s = this.s;
    }
    /**
     * @param {number} x
     * @return {undefined}
     */
    function bnpFromInt(x) {
      /** @type {number} */
      this.t = 1;
      /** @type {number} */
      this.s = x < 0 ? -1 : 0;
      if (x > 0) {
        /** @type {number} */
        this.data[0] = x;
      } else {
        if (x < -1) {
          this.data[0] = x + this.DV;
        } else {
          /** @type {number} */
          this.t = 0;
        }
      }
    }
    /**
     * @param {number} i
     * @return {?}
     */
    function nbv(i) {
      var r = nbi();
      return r.fromInt(i), r;
    }
    /**
     * @param {string} s
     * @param {number} b
     * @return {?}
     */
    function bnpFromString(s, b) {
      var k;
      if (16 == b) {
        /** @type {number} */
        k = 4;
      } else {
        if (8 == b) {
          /** @type {number} */
          k = 3;
        } else {
          if (256 == b) {
            /** @type {number} */
            k = 8;
          } else {
            if (2 == b) {
              /** @type {number} */
              k = 1;
            } else {
              if (32 == b) {
                /** @type {number} */
                k = 5;
              } else {
                if (4 != b) {
                  return void this.fromRadix(s, b);
                }
                /** @type {number} */
                k = 2;
              }
            }
          }
        }
      }
      /** @type {number} */
      this.t = 0;
      /** @type {number} */
      this.s = 0;
      var i = s.length;
      /** @type {boolean} */
      var n = false;
      /** @type {number} */
      var sh = 0;
      for (; --i >= 0;) {
        var x = 8 == k ? 255 & s[i] : intAt(s, i);
        if (x < 0) {
          if ("-" == s.charAt(i)) {
            /** @type {boolean} */
            n = true;
          }
        } else {
          /** @type {boolean} */
          n = false;
          if (0 == sh) {
            this.data[this.t++] = x;
          } else {
            if (sh + k > this.DB) {
              this.data[this.t - 1] |= (x & (1 << this.DB - sh) - 1) << sh;
              /** @type {number} */
              this.data[this.t++] = x >> this.DB - sh;
            } else {
              this.data[this.t - 1] |= x << sh;
            }
          }
          /** @type {number} */
          sh = sh + k;
          if (sh >= this.DB) {
            /** @type {number} */
            sh = sh - this.DB;
          }
        }
      }
      if (8 == k && 0 != (128 & s[0])) {
        /** @type {number} */
        this.s = -1;
        if (sh > 0) {
          this.data[this.t - 1] |= (1 << this.DB - sh) - 1 << sh;
        }
      }
      this.clamp();
      if (n) {
        BigInteger.ZERO.subTo(this, this);
      }
    }
    /**
     * @return {undefined}
     */
    function bnpClamp() {
      /** @type {number} */
      var c = this.s & this.DM;
      for (; this.t > 0 && this.data[this.t - 1] == c;) {
        --this.t;
      }
    }
    /**
     * @param {number} n
     * @return {?}
     */
    function bnToString(n) {
      if (this.s < 0) {
        return "-" + this.negate().toString(n);
      }
      var k;
      if (16 == n) {
        /** @type {number} */
        k = 4;
      } else {
        if (8 == n) {
          /** @type {number} */
          k = 3;
        } else {
          if (2 == n) {
            /** @type {number} */
            k = 1;
          } else {
            if (32 == n) {
              /** @type {number} */
              k = 5;
            } else {
              if (4 != n) {
                return this.toRadix(n);
              }
              /** @type {number} */
              k = 2;
            }
          }
        }
      }
      var d;
      /** @type {number} */
      var km = (1 << k) - 1;
      /** @type {boolean} */
      var root = false;
      /** @type {string} */
      var r = "";
      var i = this.t;
      /** @type {number} */
      var p = this.DB - i * this.DB % k;
      if (i-- > 0) {
        if (p < this.DB && (d = this.data[i] >> p) > 0) {
          /** @type {boolean} */
          root = true;
          r = int2char(d);
        }
        for (; i >= 0;) {
          if (p < k) {
            /** @type {number} */
            d = (this.data[i] & (1 << p) - 1) << k - p;
            /** @type {number} */
            d = d | this.data[--i] >> (p = p + (this.DB - k));
          } else {
            /** @type {number} */
            d = this.data[i] >> (p = p - k) & km;
            if (p <= 0) {
              p = p + this.DB;
              --i;
            }
          }
          if (d > 0) {
            /** @type {boolean} */
            root = true;
          }
          if (root) {
            r = r + int2char(d);
          }
        }
      }
      return root ? r : "0";
    }
    /**
     * @return {?}
     */
    function bnNegate() {
      var r = nbi();
      return BigInteger.ZERO.subTo(this, r), r;
    }
    /**
     * @return {?}
     */
    function bnAbs() {
      return this.s < 0 ? this.negate() : this;
    }
    /**
     * @param {!Object} a
     * @return {?}
     */
    function bnCompareTo(a) {
      /** @type {number} */
      var r = this.s - a.s;
      if (0 != r) {
        return r;
      }
      var i = this.t;
      if (r = i - a.t, 0 != r) {
        return this.s < 0 ? -r : r;
      }
      for (; --i >= 0;) {
        if (0 != (r = this.data[i] - a.data[i])) {
          return r;
        }
      }
      return 0;
    }
    /**
     * @param {number} x
     * @return {?}
     */
    function nbits(x) {
      var t;
      /** @type {number} */
      var xOff = 1;
      return 0 != (t = x >>> 16) && (x = t, xOff = xOff + 16), 0 != (t = x >> 8) && (x = t, xOff = xOff + 8), 0 != (t = x >> 4) && (x = t, xOff = xOff + 4), 0 != (t = x >> 2) && (x = t, xOff = xOff + 2), 0 != (t = x >> 1) && (x = t, xOff = xOff + 1), xOff;
    }
    /**
     * @return {?}
     */
    function bnBitLength() {
      return this.t <= 0 ? 0 : this.DB * (this.t - 1) + nbits(this.data[this.t - 1] ^ this.s & this.DM);
    }
    /**
     * @param {number} n
     * @param {!Object} r
     * @return {undefined}
     */
    function bnpDLShiftTo(n, r) {
      var i;
      /** @type {number} */
      i = this.t - 1;
      for (; i >= 0; --i) {
        r.data[i + n] = this.data[i];
      }
      /** @type {number} */
      i = n - 1;
      for (; i >= 0; --i) {
        /** @type {number} */
        r.data[i] = 0;
      }
      r.t = this.t + n;
      r.s = this.s;
    }
    /**
     * @param {number} n
     * @param {!Object} r
     * @return {undefined}
     */
    function bnpDRShiftTo(n, r) {
      /** @type {number} */
      var i = n;
      for (; i < this.t; ++i) {
        r.data[i - n] = this.data[i];
      }
      /** @type {number} */
      r.t = Math.max(this.t - n, 0);
      r.s = this.s;
    }
    /**
     * @param {number} n
     * @param {!Object} r
     * @return {undefined}
     */
    function bnpLShiftTo(n, r) {
      var i;
      /** @type {number} */
      var bs = n % this.DB;
      /** @type {number} */
      var cbs = this.DB - bs;
      /** @type {number} */
      var bm = (1 << cbs) - 1;
      /** @type {number} */
      var y = Math.floor(n / this.DB);
      /** @type {number} */
      var c = this.s << bs & this.DM;
      /** @type {number} */
      i = this.t - 1;
      for (; i >= 0; --i) {
        /** @type {number} */
        r.data[i + y + 1] = this.data[i] >> cbs | c;
        /** @type {number} */
        c = (this.data[i] & bm) << bs;
      }
      /** @type {number} */
      i = y - 1;
      for (; i >= 0; --i) {
        /** @type {number} */
        r.data[i] = 0;
      }
      /** @type {number} */
      r.data[y] = c;
      r.t = this.t + y + 1;
      r.s = this.s;
      r.clamp();
    }
    /**
     * @param {number} n
     * @param {!Object} r
     * @return {?}
     */
    function bnpRShiftTo(n, r) {
      r.s = this.s;
      /** @type {number} */
      var ds = Math.floor(n / this.DB);
      if (ds >= this.t) {
        return void(r.t = 0);
      }
      /** @type {number} */
      var bs = n % this.DB;
      /** @type {number} */
      var cbs = this.DB - bs;
      /** @type {number} */
      var bm = (1 << bs) - 1;
      /** @type {number} */
      r.data[0] = this.data[ds] >> bs;
      /** @type {number} */
      var i = ds + 1;
      for (; i < this.t; ++i) {
        r.data[i - ds - 1] |= (this.data[i] & bm) << cbs;
        /** @type {number} */
        r.data[i - ds] = this.data[i] >> bs;
      }
      if (bs > 0) {
        r.data[this.t - ds - 1] |= (this.s & bm) << cbs;
      }
      /** @type {number} */
      r.t = this.t - ds;
      r.clamp();
    }
    /**
     * @param {!Object} a
     * @param {!Object} r
     * @return {undefined}
     */
    function bnpSubTo(a, r) {
      /** @type {number} */
      var i = 0;
      /** @type {number} */
      var c = 0;
      /** @type {number} */
      var m = Math.min(a.t, this.t);
      for (; i < m;) {
        /** @type {number} */
        c = c + (this.data[i] - a.data[i]);
        /** @type {number} */
        r.data[i++] = c & this.DM;
        /** @type {number} */
        c = c >> this.DB;
      }
      if (a.t < this.t) {
        /** @type {number} */
        c = c - a.s;
        for (; i < this.t;) {
          c = c + this.data[i];
          /** @type {number} */
          r.data[i++] = c & this.DM;
          /** @type {number} */
          c = c >> this.DB;
        }
        c = c + this.s;
      } else {
        c = c + this.s;
        for (; i < a.t;) {
          /** @type {number} */
          c = c - a.data[i];
          /** @type {number} */
          r.data[i++] = c & this.DM;
          /** @type {number} */
          c = c >> this.DB;
        }
        /** @type {number} */
        c = c - a.s;
      }
      /** @type {number} */
      r.s = c < 0 ? -1 : 0;
      if (c < -1) {
        r.data[i++] = this.DV + c;
      } else {
        if (c > 0) {
          r.data[i++] = c;
        }
      }
      /** @type {number} */
      r.t = i;
      r.clamp();
    }
    /**
     * @param {!Object} a
     * @param {!Object} r
     * @return {undefined}
     */
    function bnpMultiplyTo(a, r) {
      var x = this.abs();
      var batch = a.abs();
      var i = x.t;
      r.t = i + batch.t;
      for (; --i >= 0;) {
        /** @type {number} */
        r.data[i] = 0;
      }
      /** @type {number} */
      i = 0;
      for (; i < batch.t; ++i) {
        r.data[i + x.t] = x.am(0, batch.data[i], r, i, 0, x.t);
      }
      /** @type {number} */
      r.s = 0;
      r.clamp();
      if (this.s != a.s) {
        BigInteger.ZERO.subTo(r, r);
      }
    }
    /**
     * @param {!Object} r
     * @return {undefined}
     */
    function bnpSquareTo(r) {
      var x = this.abs();
      /** @type {number} */
      var i = r.t = 2 * x.t;
      for (; --i >= 0;) {
        /** @type {number} */
        r.data[i] = 0;
      }
      /** @type {number} */
      i = 0;
      for (; i < x.t - 1; ++i) {
        var c = x.am(i, x.data[i], r, 2 * i, 0, 1);
        if ((r.data[i + x.t] += x.am(i + 1, 2 * x.data[i], r, 2 * i + 1, c, x.t - i - 1)) >= x.DV) {
          r.data[i + x.t] -= x.DV;
          /** @type {number} */
          r.data[i + x.t + 1] = 1;
        }
      }
      if (r.t > 0) {
        r.data[r.t - 1] += x.am(i, x.data[i], r, 2 * i, 0, 1);
      }
      /** @type {number} */
      r.s = 0;
      r.clamp();
    }
    /**
     * @param {!Object} m
     * @param {!Object} q
     * @param {!Object} r
     * @return {?}
     */
    function bnpDivRemTo(m, q, r) {
      var pm = m.abs();
      if (!(pm.t <= 0)) {
        var pt = this.abs();
        if (pt.t < pm.t) {
          return null != q && q.fromInt(0), void(null != r && this.copyTo(r));
        }
        if (null == r) {
          r = nbi();
        }
        var y = nbi();
        var s = this.s;
        var type = m.s;
        /** @type {number} */
        var nsh = this.DB - nbits(pm.data[pm.t - 1]);
        if (nsh > 0) {
          pm.lShiftTo(nsh, y);
          pt.lShiftTo(nsh, r);
        } else {
          pm.copyTo(y);
          pt.copyTo(r);
        }
        var ys = y.t;
        var y0 = y.data[ys - 1];
        if (0 != y0) {
          /** @type {number} */
          var yt = y0 * (1 << this.F1) + (ys > 1 ? y.data[ys - 2] >> this.F2 : 0);
          /** @type {number} */
          var d1 = this.FV / yt;
          /** @type {number} */
          var d2 = (1 << this.F1) / yt;
          /** @type {number} */
          var e = 1 << this.F2;
          var i = r.t;
          /** @type {number} */
          var j = i - ys;
          var t = null == q ? nbi() : q;
          y.dlShiftTo(j, t);
          if (r.compareTo(t) >= 0) {
            /** @type {number} */
            r.data[r.t++] = 1;
            r.subTo(t, r);
          }
          BigInteger.ONE.dlShiftTo(ys, t);
          t.subTo(y, y);
          for (; y.t < ys;) {
            /** @type {number} */
            y.data[y.t++] = 0;
          }
          for (; --j >= 0;) {
            var qd = r.data[--i] == y0 ? this.DM : Math.floor(r.data[i] * d1 + (r.data[i - 1] + e) * d2);
            if ((r.data[i] += y.am(0, qd, r, j, 0, ys)) < qd) {
              y.dlShiftTo(j, t);
              r.subTo(t, r);
              for (; r.data[i] < --qd;) {
                r.subTo(t, r);
              }
            }
          }
          if (null != q) {
            r.drShiftTo(ys, q);
            if (s != type) {
              BigInteger.ZERO.subTo(q, q);
            }
          }
          r.t = ys;
          r.clamp();
          if (nsh > 0) {
            r.rShiftTo(nsh, r);
          }
          if (s < 0) {
            BigInteger.ZERO.subTo(r, r);
          }
        }
      }
    }
    /**
     * @param {!Object} a
     * @return {?}
     */
    function bnMod(a) {
      var r = nbi();
      return this.abs().divRemTo(a, null, r), this.s < 0 && r.compareTo(BigInteger.ZERO) > 0 && a.subTo(r, r), r;
    }
    /**
     * @param {!Array} m
     * @return {undefined}
     */
    function Date(m) {
      /** @type {!Array} */
      this.m = m;
    }
    /**
     * @param {!Object} r
     * @return {?}
     */
    function _MR_inner(r) {
      return r.s < 0 || r.compareTo(this.m) >= 0 ? r.mod(this.m) : r;
    }
    /**
     * @param {?} s
     * @return {?}
     */
    function bv_trim(s) {
      return s;
    }
    /**
     * @param {string} r
     * @return {undefined}
     */
    function retrieveRecordDidComplete(r) {
      r.divRemTo(this.m, null, r);
    }
    /**
     * @param {?} x
     * @param {!Object} y
     * @param {undefined} r
     * @return {undefined}
     */
    function montMulTo(x, y, r) {
      x.multiplyTo(y, r);
      this.reduce(r);
    }
    /**
     * @param {?} x
     * @param {undefined} r
     * @return {undefined}
     */
    function mergeCircle(x, r) {
      x.squareTo(r);
      this.reduce(r);
    }
    /**
     * @return {?}
     */
    function bnpInvDigit() {
      if (this.t < 1) {
        return 0;
      }
      var x = this.data[0];
      if (0 == (1 & x)) {
        return 0;
      }
      /** @type {number} */
      var y = 3 & x;
      return y = y * (2 - (15 & x) * y) & 15, y = y * (2 - (255 & x) * y) & 255, y = y * (2 - ((65535 & x) * y & 65535)) & 65535, y = y * (2 - x * y % this.DV) % this.DV, y > 0 ? this.DV - y : -y;
    }
    /**
     * @param {!Array} m
     * @return {undefined}
     */
    function Montgomery(m) {
      /** @type {!Array} */
      this.m = m;
      this.mp = m.invDigit();
      /** @type {number} */
      this.mpl = 32767 & this.mp;
      /** @type {number} */
      this.mph = this.mp >> 15;
      /** @type {number} */
      this.um = (1 << m.DB - 15) - 1;
      /** @type {number} */
      this.mt2 = 2 * m.t;
    }
    /**
     * @param {!Object} x
     * @return {?}
     */
    function montConvert(x) {
      var r = nbi();
      return x.abs().dlShiftTo(this.m.t, r), r.divRemTo(this.m, null, r), x.s < 0 && r.compareTo(BigInteger.ZERO) > 0 && this.m.subTo(r, r), r;
    }
    /**
     * @param {!FileEntry} x
     * @return {?}
     */
    function montRevert(x) {
      var r = nbi();
      return x.copyTo(r), this.reduce(r), r;
    }
    /**
     * @param {!Object} x
     * @return {undefined}
     */
    function montReduce(x) {
      for (; x.t <= this.mt2;) {
        /** @type {number} */
        x.data[x.t++] = 0;
      }
      /** @type {number} */
      var i = 0;
      for (; i < this.m.t; ++i) {
        /** @type {number} */
        var j = 32767 & x.data[i];
        /** @type {number} */
        var u0 = j * this.mpl + ((j * this.mph + (x.data[i] >> 15) * this.mpl & this.um) << 15) & x.DM;
        j = i + this.m.t;
        x.data[j] += this.m.am(0, u0, x, i, 0, this.m.t);
        for (; x.data[j] >= x.DV;) {
          x.data[j] -= x.DV;
          x.data[++j]++;
        }
      }
      x.clamp();
      x.drShiftTo(this.m.t, x);
      if (x.compareTo(this.m) >= 0) {
        x.subTo(this.m, x);
      }
    }
    /**
     * @param {?} x
     * @param {undefined} r
     * @return {undefined}
     */
    function uniprotFeaturePainter_diamond(x, r) {
      x.squareTo(r);
      this.reduce(r);
    }
    /**
     * @param {?} x
     * @param {!Object} y
     * @param {undefined} r
     * @return {undefined}
     */
    function pasteLine(x, y, r) {
      x.multiplyTo(y, r);
      this.reduce(r);
    }
    /**
     * @return {?}
     */
    function removeLibraryRelations() {
      return 0 == (this.t > 0 ? 1 & this.data[0] : this.s);
    }
    /**
     * @param {number} i
     * @param {!Object} z
     * @return {?}
     */
    function append(i, z) {
      if (i > 4294967295 || i < 1) {
        return BigInteger.ONE;
      }
      var r = nbi();
      var r2 = nbi();
      var x = z.convert(this);
      /** @type {number} */
      var u = nbits(i) - 1;
      x.copyTo(r);
      for (; --u >= 0;) {
        if (z.sqrTo(r, r2), (i & 1 << u) > 0) {
          z.mulTo(r2, x, r);
        } else {
          var t = r;
          r = r2;
          r2 = t;
        }
      }
      return z.revert(r);
    }
    /**
     * @param {number} m
     * @param {?} b
     * @return {?}
     */
    function benchmark(m, b) {
      var currentOpeningToken;
      return currentOpeningToken = m < 256 || b.isEven() ? new Date(b) : new Montgomery(b), this.exp(m, currentOpeningToken);
    }
    /**
     * @return {?}
     */
    function clone() {
      var r = nbi();
      return this.copyTo(r), r;
    }
    /**
     * @return {?}
     */
    function cancel() {
      if (this.s < 0) {
        if (1 == this.t) {
          return this.data[0] - this.DV;
        }
        if (0 == this.t) {
          return -1;
        }
      } else {
        if (1 == this.t) {
          return this.data[0];
        }
        if (0 == this.t) {
          return 0;
        }
      }
      return (this.data[1] & (1 << 32 - this.DB) - 1) << this.DB | this.data[0];
    }
    /**
     * @return {?}
     */
    function moveLibraryRelations() {
      return 0 == this.t ? this.s : this.data[0] << 24 >> 24;
    }
    /**
     * @return {?}
     */
    function _setIEOpacityRatio() {
      return 0 == this.t ? this.s : this.data[0] << 16 >> 16;
    }
    /**
     * @param {number} num
     * @return {?}
     */
    function div(num) {
      return Math.floor(Math.LN2 * this.DB / Math.log(num));
    }
    /**
     * @return {?}
     */
    function calculateTimes() {
      return this.s < 0 ? -1 : this.t <= 0 || 1 == this.t && this.data[0] <= 0 ? 0 : 1;
    }
    /**
     * @param {number} e
     * @return {?}
     */
    function decode(e) {
      if (null == e && (e = 10), 0 == this.signum() || e < 2 || e > 36) {
        return "0";
      }
      var t = this.chunkSize(e);
      /** @type {number} */
      var b = Math.pow(e, t);
      var a = nbv(b);
      var r = nbi();
      var n = nbi();
      /** @type {string} */
      var s = "";
      this.divRemTo(a, r, n);
      for (; r.signum() > 0;) {
        s = (b + n.intValue()).toString(e).substr(1) + s;
        r.divRemTo(a, r, n);
      }
      return n.intValue().toString(e) + s;
    }
    /**
     * @param {string} s
     * @param {number} k
     * @return {undefined}
     */
    function _init(s, k) {
      this.fromInt(0);
      if (null == k) {
        /** @type {number} */
        k = 10;
      }
      var r = this.chunkSize(k);
      /** @type {number} */
      var color = Math.pow(k, r);
      /** @type {boolean} */
      var c = false;
      /** @type {number} */
      var p = 0;
      /** @type {number} */
      var z = 0;
      /** @type {number} */
      var i = 0;
      for (; i < s.length; ++i) {
        var x = intAt(s, i);
        if (x < 0) {
          if ("-" == s.charAt(i) && 0 == this.signum()) {
            /** @type {boolean} */
            c = true;
          }
        } else {
          z = k * z + x;
          if (++p >= r) {
            this.dMultiply(color);
            this.dAddOffset(z, 0);
            /** @type {number} */
            p = 0;
            /** @type {number} */
            z = 0;
          }
        }
      }
      if (p > 0) {
        this.dMultiply(Math.pow(k, p));
        this.dAddOffset(z, 0);
      }
      if (c) {
        BigInteger.ZERO.subTo(this, this);
      }
    }
    /**
     * @param {number} height
     * @param {number} data
     * @param {number} callback
     * @return {undefined}
     */
    function generateRandom(height, data, callback) {
      if ("number" == typeof data) {
        if (height < 2) {
          this.fromInt(1);
        } else {
          this.fromNumber(height, callback);
          if (!this.testBit(height - 1)) {
            this.bitwiseTo(BigInteger.ONE.shiftLeft(height - 1), t, this);
          }
          if (this.isEven()) {
            this.dAddOffset(1, 0);
          }
          for (; !this.isProbablePrime(data);) {
            this.dAddOffset(2, 0);
            if (this.bitLength() > height) {
              this.subTo(BigInteger.ONE.shiftLeft(height - 1), this);
            }
          }
        }
      } else {
        /** @type {!Array} */
        var a = new Array;
        /** @type {number} */
        var new_lo = 7 & height;
        /** @type {number} */
        a.length = (height >> 3) + 1;
        data.nextBytes(a);
        if (new_lo > 0) {
          a[0] &= (1 << new_lo) - 1;
        } else {
          /** @type {number} */
          a[0] = 0;
        }
        this.fromString(a, 256);
      }
    }
    /**
     * @return {?}
     */
    function clear() {
      var i = this.t;
      /** @type {!Array} */
      var ret = new Array;
      ret[0] = this.s;
      var index;
      /** @type {number} */
      var p = this.DB - i * this.DB % 8;
      /** @type {number} */
      var midi = 0;
      if (i-- > 0) {
        if (p < this.DB && (index = this.data[i] >> p) != (this.s & this.DM) >> p) {
          /** @type {number} */
          ret[midi++] = index | this.s << this.DB - p;
        }
        for (; i >= 0;) {
          if (p < 8) {
            /** @type {number} */
            index = (this.data[i] & (1 << p) - 1) << 8 - p;
            /** @type {number} */
            index = index | this.data[--i] >> (p = p + (this.DB - 8));
          } else {
            /** @type {number} */
            index = this.data[i] >> (p = p - 8) & 255;
            if (p <= 0) {
              p = p + this.DB;
              --i;
            }
          }
          if (0 != (128 & index)) {
            /** @type {number} */
            index = index | -256;
          }
          if (0 == midi && (128 & this.s) != (128 & index)) {
            ++midi;
          }
          if (midi > 0 || index != this.s) {
            /** @type {(number|undefined)} */
            ret[midi++] = index;
          }
        }
      }
      return ret;
    }
    /**
     * @param {!Object} target
     * @return {?}
     */
    function equals(target) {
      return 0 == this.compareTo(target);
    }
    /**
     * @param {!Object} b
     * @return {?}
     */
    function compareTo(b) {
      return this.compareTo(b) < 0 ? this : b;
    }
    /**
     * @param {!Object} target
     * @return {?}
     */
    function handleMouseOverForNode(target) {
      return this.compareTo(target) > 0 ? this : target;
    }
    /**
     * @param {!Object} options
     * @param {!Function} callback
     * @param {!Object} result
     * @return {undefined}
     */
    function add(options, callback, result) {
      var i;
      var n;
      /** @type {number} */
      var m = Math.min(options.t, this.t);
      /** @type {number} */
      i = 0;
      for (; i < m; ++i) {
        result.data[i] = callback(this.data[i], options.data[i]);
      }
      if (options.t < this.t) {
        /** @type {number} */
        n = options.s & this.DM;
        /** @type {number} */
        i = m;
        for (; i < this.t; ++i) {
          result.data[i] = callback(this.data[i], n);
        }
        result.t = this.t;
      } else {
        /** @type {number} */
        n = this.s & this.DM;
        /** @type {number} */
        i = m;
        for (; i < options.t; ++i) {
          result.data[i] = callback(n, options.data[i]);
        }
        result.t = options.t;
      }
      result.s = callback(this.s, options.s);
      result.clamp();
    }
    /**
     * @param {number} n
     * @param {number} a
     * @return {?}
     */
    function delay(n, a) {
      return n & a;
    }
    /**
     * @param {!Object} callback
     * @return {?}
     */
    function fn(callback) {
      var t = nbi();
      return this.bitwiseTo(callback, delay, t), t;
    }
    /**
     * @param {number} leftDiags
     * @param {number} columns
     * @return {?}
     */
    function t(leftDiags, columns) {
      return leftDiags | columns;
    }
    /**
     * @param {!Object} connector
     * @return {?}
     */
    function getConnectorTestFilePath(connector) {
      var n = nbi();
      return this.bitwiseTo(connector, t, n), n;
    }
    /**
     * @param {boolean} n
     * @param {boolean} elem
     * @return {?}
     */
    function options(n, elem) {
      return n ^ elem;
    }
    /**
     * @param {!Object} data
     * @return {?}
     */
    function xor(data) {
      var r = nbi();
      return this.bitwiseTo(data, options, r), r;
    }
    /**
     * @param {number} m
     * @param {?} name
     * @return {?}
     */
    function length(m, name) {
      return m & ~name;
    }
    /**
     * @param {!Object} callback
     * @return {?}
     */
    function extractPresetLocal(callback) {
      var t = nbi();
      return this.bitwiseTo(callback, length, t), t;
    }
    /**
     * @return {?}
     */
    function updateSoldierDisplay() {
      var r = nbi();
      /** @type {number} */
      var i = 0;
      for (; i < this.t; ++i) {
        /** @type {number} */
        r.data[i] = this.DM & ~this.data[i];
      }
      return r.t = this.t, r.s = ~this.s, r;
    }
    /**
     * @param {number} n
     * @return {?}
     */
    function left(n) {
      var rules = nbi();
      return n < 0 ? this.rShiftTo(-n, rules) : this.lShiftTo(n, rules), rules;
    }
    /**
     * @param {number} n
     * @return {?}
     */
    function mpi_from_buffer(n) {
      var rules = nbi();
      return n < 0 ? this.lShiftTo(-n, rules) : this.rShiftTo(n, rules), rules;
    }
    /**
     * @param {number} n
     * @return {?}
     */
    function encode(n) {
      if (0 == n) {
        return -1;
      }
      /** @type {number} */
      var destIndex = 0;
      return 0 == (65535 & n) && (n = n >> 16, destIndex = destIndex + 16), 0 == (255 & n) && (n = n >> 8, destIndex = destIndex + 8), 0 == (15 & n) && (n = n >> 4, destIndex = destIndex + 4), 0 == (3 & n) && (n = n >> 2, destIndex = destIndex + 2), 0 == (1 & n) && ++destIndex, destIndex;
    }
    /**
     * @return {?}
     */
    function error() {
      /** @type {number} */
      var i = 0;
      for (; i < this.t; ++i) {
        if (0 != this.data[i]) {
          return i * this.DB + encode(this.data[i]);
        }
      }
      return this.s < 0 ? this.t * this.DB : -1;
    }
    /**
     * @param {number} size
     * @return {?}
     */
    function leftPad(size) {
      /** @type {number} */
      var input = 0;
      for (; 0 != size;) {
        /** @type {number} */
        size = size & size - 1;
        ++input;
      }
      return input;
    }
    /**
     * @return {?}
     */
    function walk() {
      /** @type {number} */
      var ret = 0;
      /** @type {number} */
      var c = this.s & this.DM;
      /** @type {number} */
      var i = 0;
      for (; i < this.t; ++i) {
        ret = ret + leftPad(this.data[i] ^ c);
      }
      return ret;
    }
    /**
     * @param {number} n
     * @return {?}
     */
    function check(n) {
      /** @type {number} */
      var y = Math.floor(n / this.DB);
      return y >= this.t ? 0 != this.s : 0 != (this.data[y] & 1 << n % this.DB);
    }
    /**
     * @param {number} hash
     * @param {!Function} n
     * @return {?}
     */
    function encodeZigZag64(hash, n) {
      var e = BigInteger.ONE.shiftLeft(hash);
      return this.bitwiseTo(e, n, e), e;
    }
    /**
     * @param {undefined} selector
     * @return {?}
     */
    function clickWithWebdriver(selector) {
      return this.changeBit(selector, t);
    }
    /**
     * @param {undefined} selector
     * @return {?}
     */
    function clickWithJavaScript(selector) {
      return this.changeBit(selector, length);
    }
    /**
     * @param {undefined} output
     * @return {?}
     */
    function writeSearchEntry(output) {
      return this.changeBit(output, options);
    }
    /**
     * @param {!Object} a
     * @param {!Object} r
     * @return {undefined}
     */
    function bnpAddTo(a, r) {
      /** @type {number} */
      var i = 0;
      /** @type {number} */
      var c = 0;
      /** @type {number} */
      var m = Math.min(a.t, this.t);
      for (; i < m;) {
        c = c + (this.data[i] + a.data[i]);
        /** @type {number} */
        r.data[i++] = c & this.DM;
        /** @type {number} */
        c = c >> this.DB;
      }
      if (a.t < this.t) {
        c = c + a.s;
        for (; i < this.t;) {
          c = c + this.data[i];
          /** @type {number} */
          r.data[i++] = c & this.DM;
          /** @type {number} */
          c = c >> this.DB;
        }
        c = c + this.s;
      } else {
        c = c + this.s;
        for (; i < a.t;) {
          c = c + a.data[i];
          /** @type {number} */
          r.data[i++] = c & this.DM;
          /** @type {number} */
          c = c >> this.DB;
        }
        c = c + a.s;
      }
      /** @type {number} */
      r.s = c < 0 ? -1 : 0;
      if (c > 0) {
        r.data[i++] = c;
      } else {
        if (c < -1) {
          r.data[i++] = this.DV + c;
        }
      }
      /** @type {number} */
      r.t = i;
      r.clamp();
    }
    /**
     * @param {!Object} a
     * @return {?}
     */
    function bnAdd(a) {
      var r = nbi();
      return this.addTo(a, r), r;
    }
    /**
     * @param {!Object} a
     * @return {?}
     */
    function bnSubtract(a) {
      var r = nbi();
      return this.subTo(a, r), r;
    }
    /**
     * @param {!Array} a
     * @return {?}
     */
    function bnMultiply(a) {
      var r = nbi();
      return this.multiplyTo(a, r), r;
    }
    /**
     * @param {!Object} a
     * @return {?}
     */
    function bnDivide(a) {
      var r = nbi();
      return this.divRemTo(a, r, null), r;
    }
    /**
     * @param {!Object} a
     * @return {?}
     */
    function str(a) {
      var r = nbi();
      return this.divRemTo(a, null, r), r;
    }
    /**
     * @param {!Object} s
     * @return {?}
     */
    function nbs(s) {
      var r = nbi();
      var y = nbi();
      return this.divRemTo(s, r, y), new Array(r, y);
    }
    /**
     * @param {number} color
     * @return {undefined}
     */
    function setFromState(color) {
      this.data[this.t] = this.am(0, color - 1, this, 0, 0, this.t);
      ++this.t;
      this.clamp();
    }
    /**
     * @param {number} v
     * @param {number} i
     * @return {undefined}
     */
    function _setClassNameRatio(v, i) {
      if (0 != v) {
        for (; this.t <= i;) {
          /** @type {number} */
          this.data[this.t++] = 0;
        }
        this.data[i] += v;
        for (; this.data[i] >= this.DV;) {
          this.data[i] -= this.DV;
          if (++i >= this.t) {
            /** @type {number} */
            this.data[this.t++] = 0;
          }
          ++this.data[i];
        }
      }
    }
    /**
     * @return {undefined}
     */
    function self() {
    }
    /**
     * @param {?} value
     * @return {?}
     */
    function conversion(value) {
      return value;
    }
    /**
     * @param {?} x
     * @param {!Object} y
     * @param {!Object} r
     * @return {undefined}
     */
    function laneofpos(x, y, r) {
      x.multiplyTo(y, r);
    }
    /**
     * @param {?} x
     * @param {!Object} r
     * @return {undefined}
     */
    function breakOrIterNext(x, r) {
      x.squareTo(r);
    }
    /**
     * @param {number} x
     * @return {?}
     */
    function gauss(x) {
      return this.exp(x, new self);
    }
    /**
     * @param {!Object} c
     * @param {string} b
     * @param {!Object} r
     * @return {undefined}
     */
    function subtract(c, b, r) {
      /** @type {number} */
      var i = Math.min(this.t + c.t, b);
      /** @type {number} */
      r.s = 0;
      /** @type {number} */
      r.t = i;
      for (; i > 0;) {
        /** @type {number} */
        r.data[--i] = 0;
      }
      var t;
      /** @type {number} */
      t = r.t - this.t;
      for (; i < t; ++i) {
        r.data[i + this.t] = this.am(0, c.data[i], r, i, 0, this.t);
      }
      /** @type {number} */
      t = Math.min(c.t, b);
      for (; i < t; ++i) {
        this.am(0, c.data[i], r, i, 0, b - i);
      }
      r.clamp();
    }
    /**
     * @param {!Object} opts
     * @param {string} i
     * @param {!Object} x
     * @return {undefined}
     */
    function init(opts, i, x) {
      --i;
      /** @type {number} */
      var j = x.t = this.t + opts.t - i;
      /** @type {number} */
      x.s = 0;
      for (; --j >= 0;) {
        /** @type {number} */
        x.data[j] = 0;
      }
      /** @type {number} */
      j = Math.max(i - this.t, 0);
      for (; j < opts.t; ++j) {
        x.data[this.t + j - i] = this.am(i - j, opts.data[j], x, 0, 0, this.t + j - i);
      }
      x.clamp();
      x.drShiftTo(1, x);
    }
    /**
     * @param {!Array} val
     * @return {undefined}
     */
    function update(val) {
      this.r2 = nbi();
      this.q3 = nbi();
      BigInteger.ONE.dlShiftTo(2 * val.t, this.r2);
      this.mu = this.r2.divide(val);
      /** @type {!Array} */
      this.m = val;
    }
    /**
     * @param {!Object} x
     * @return {?}
     */
    function start(x) {
      if (x.s < 0 || x.t > 2 * this.m.t) {
        return x.mod(this.m);
      }
      if (x.compareTo(this.m) < 0) {
        return x;
      }
      var r = nbi();
      return x.copyTo(r), this.reduce(r), r;
    }
    /**
     * @param {?} store
     * @return {?}
     */
    function mountResponsive(store) {
      return store;
    }
    /**
     * @param {!Object} r
     * @return {undefined}
     */
    function run(r) {
      r.drShiftTo(this.m.t - 1, this.r2);
      if (r.t > this.m.t + 1) {
        r.t = this.m.t + 1;
        r.clamp();
      }
      this.mu.multiplyUpperTo(this.r2, this.m.t + 1, this.q3);
      this.m.multiplyLowerTo(this.q3, this.m.t + 1, this.r2);
      for (; r.compareTo(this.r2) < 0;) {
        r.dAddOffset(1, this.m.t + 1);
      }
      r.subTo(this.r2, r);
      for (; r.compareTo(this.m) >= 0;) {
        r.subTo(this.m, r);
      }
    }
    /**
     * @param {?} x
     * @param {undefined} r
     * @return {undefined}
     */
    function montSqrTo(x, r) {
      x.squareTo(r);
      this.reduce(r);
    }
    /**
     * @param {?} x
     * @param {!Object} y
     * @param {undefined} r
     * @return {undefined}
     */
    function log2_fold(x, y, r) {
      x.multiplyTo(y, r);
      this.reduce(r);
    }
    /**
     * @param {!Object} e
     * @param {?} m
     * @return {?}
     */
    function bnModPow(e, m) {
      var k;
      var z;
      var i = e.bitLength();
      var r = nbv(1);
      if (i <= 0) {
        return r;
      }
      /** @type {number} */
      k = i < 18 ? 1 : i < 48 ? 3 : i < 144 ? 4 : i < 768 ? 5 : 6;
      z = i < 8 ? new Date(m) : m.isEven() ? new update(m) : new Montgomery(m);
      /** @type {!Array} */
      var g = new Array;
      /** @type {number} */
      var n = 3;
      /** @type {number} */
      var k1 = k - 1;
      /** @type {number} */
      var km = (1 << k) - 1;
      if (g[1] = z.convert(this), k > 1) {
        var r2 = nbi();
        z.sqrTo(g[1], r2);
        for (; n <= km;) {
          g[n] = nbi();
          z.mulTo(r2, g[n - 2], g[n]);
          /** @type {number} */
          n = n + 2;
        }
      }
      var w;
      var radius;
      /** @type {number} */
      var j = e.t - 1;
      /** @type {boolean} */
      var context = true;
      var r2 = nbi();
      /** @type {number} */
      i = nbits(e.data[j]) - 1;
      for (; j >= 0;) {
        if (i >= k1) {
          /** @type {number} */
          w = e.data[j] >> i - k1 & km;
        } else {
          /** @type {number} */
          w = (e.data[j] & (1 << i + 1) - 1) << k1 - i;
          if (j > 0) {
            /** @type {number} */
            w = w | e.data[j - 1] >> this.DB + i - k1;
          }
        }
        /** @type {number} */
        n = k;
        for (; 0 == (1 & w);) {
          /** @type {number} */
          w = w >> 1;
          --n;
        }
        if ((i = i - n) < 0 && (i = i + this.DB, --j), context) {
          g[w].copyTo(r);
          /** @type {boolean} */
          context = false;
        } else {
          for (; n > 1;) {
            z.sqrTo(r, r2);
            z.sqrTo(r2, r);
            /** @type {number} */
            n = n - 2;
          }
          if (n > 0) {
            z.sqrTo(r, r2);
          } else {
            radius = r;
            r = r2;
            r2 = radius;
          }
          z.mulTo(r2, g[w], r);
        }
        for (; j >= 0 && 0 == (e.data[j] & 1 << i);) {
          z.sqrTo(r, r2);
          radius = r;
          r = r2;
          r2 = radius;
          if (--i < 0) {
            /** @type {number} */
            i = this.DB - 1;
            --j;
          }
        }
      }
      return z.revert(r);
    }
    /**
     * @param {!Object} color
     * @return {?}
     */
    function create(color) {
      var r = this.s < 0 ? this.negate() : this.clone();
      var t = color.s < 0 ? color.negate() : color.clone();
      if (r.compareTo(t) < 0) {
        var e = r;
        r = t;
        t = e;
      }
      var n = r.getLowestSetBit();
      var i = t.getLowestSetBit();
      if (i < 0) {
        return r;
      }
      if (n < i) {
        i = n;
      }
      if (i > 0) {
        r.rShiftTo(i, r);
        t.rShiftTo(i, t);
      }
      for (; r.signum() > 0;) {
        if ((n = r.getLowestSetBit()) > 0) {
          r.rShiftTo(n, r);
        }
        if ((n = t.getLowestSetBit()) > 0) {
          t.rShiftTo(n, t);
        }
        if (r.compareTo(t) >= 0) {
          r.subTo(t, r);
          r.rShiftTo(1, r);
        } else {
          t.subTo(r, t);
          t.rShiftTo(1, t);
        }
      }
      return i > 0 && t.lShiftTo(i, t), t;
    }
    /**
     * @param {number} s
     * @return {?}
     */
    function items2text(s) {
      if (s <= 0) {
        return 0;
      }
      /** @type {number} */
      var width = this.DV % s;
      /** @type {number} */
      var t = this.s < 0 ? s - 1 : 0;
      if (this.t > 0) {
        if (0 == width) {
          /** @type {number} */
          t = this.data[0] % s;
        } else {
          /** @type {number} */
          var i = this.t - 1;
          for (; i >= 0; --i) {
            /** @type {number} */
            t = (width * t + this.data[i]) % s;
          }
        }
      }
      return t;
    }
    /**
     * @param {!Object} b
     * @return {?}
     */
    function test(b) {
      var parent = b.isEven();
      if (this.isEven() && parent || 0 == b.signum()) {
        return BigInteger.ZERO;
      }
      var a = b.clone();
      var q = this.clone();
      var x = nbv(1);
      var t = nbv(0);
      var y = nbv(0);
      var r = nbv(1);
      for (; 0 != a.signum();) {
        for (; a.isEven();) {
          a.rShiftTo(1, a);
          if (parent) {
            if (!(x.isEven() && t.isEven())) {
              x.addTo(this, x);
              t.subTo(b, t);
            }
            x.rShiftTo(1, x);
          } else {
            if (!t.isEven()) {
              t.subTo(b, t);
            }
          }
          t.rShiftTo(1, t);
        }
        for (; q.isEven();) {
          q.rShiftTo(1, q);
          if (parent) {
            if (!(y.isEven() && r.isEven())) {
              y.addTo(this, y);
              r.subTo(b, r);
            }
            y.rShiftTo(1, y);
          } else {
            if (!r.isEven()) {
              r.subTo(b, r);
            }
          }
          r.rShiftTo(1, r);
        }
        if (a.compareTo(q) >= 0) {
          a.subTo(q, a);
          if (parent) {
            x.subTo(y, x);
          }
          t.subTo(r, t);
        } else {
          q.subTo(a, q);
          if (parent) {
            y.subTo(x, y);
          }
          r.subTo(t, r);
        }
      }
      return 0 != q.compareTo(BigInteger.ONE) ? BigInteger.ZERO : r.compareTo(b) >= 0 ? r.subtract(b) : r.signum() < 0 ? (r.addTo(b, r), r.signum() < 0 ? r.add(b) : r) : r;
    }
    /**
     * @param {number} k
     * @return {?}
     */
    function draw(k) {
      var i;
      var x = this.abs();
      if (1 == x.t && x.data[0] <= res[res.length - 1]) {
        /** @type {number} */
        i = 0;
        for (; i < res.length; ++i) {
          if (x.data[0] == res[i]) {
            return true;
          }
        }
        return false;
      }
      if (x.isEven()) {
        return false;
      }
      /** @type {number} */
      i = 1;
      for (; i < res.length;) {
        var k = res[i];
        /** @type {number} */
        var j = i + 1;
        for (; j < res.length && k < frameSize;) {
          /** @type {number} */
          k = k * res[j++];
        }
        k = x.modInt(k);
        for (; i < j;) {
          if (k % res[i++] == 0) {
            return false;
          }
        }
      }
      return x.millerRabin(k);
    }
    /**
     * @param {number} i
     * @return {?}
     */
    function finish(i) {
      var n = this.subtract(BigInteger.ONE);
      var index = n.getLowestSetBit();
      if (index <= 0) {
        return false;
      }
      var r;
      var x = n.shiftRight(index);
      var rng = load();
      /** @type {number} */
      var whichFriend = 0;
      for (; whichFriend < i; ++whichFriend) {
        do {
          r = new BigInteger(this.bitLength(), rng);
        } while (r.compareTo(BigInteger.ONE) <= 0 || r.compareTo(n) >= 0);
        var ret = r.modPow(x, this);
        if (0 != ret.compareTo(BigInteger.ONE) && 0 != ret.compareTo(n)) {
          /** @type {number} */
          var i = 1;
          for (; i++ < index && 0 != ret.compareTo(n);) {
            if (ret = ret.modPowInt(2, this), 0 == ret.compareTo(BigInteger.ONE)) {
              return false;
            }
          }
          if (0 != ret.compareTo(n)) {
            return false;
          }
        }
      }
      return true;
    }
    /**
     * @return {?}
     */
    function load() {
      return {
        nextBytes : function(array) {
          /** @type {number} */
          var i = 0;
          for (; i < array.length; ++i) {
            /** @type {number} */
            array[i] = Math.floor(256 * Math.random());
          }
        }
      };
    }
    var forge = FORGE(0);
    mixin.exports = forge.jsbn = forge.jsbn || {};
    var dbits;
    /** @type {number} */
    var at = 0xdeadbeefcafe;
    /** @type {boolean} */
    var nt = 15715070 == (16777215 & at);
    /** @type {function(number, number, undefined): undefined} */
    forge.jsbn.BigInteger = BigInteger;
    if ("undefined" == typeof navigator) {
      /** @type {function(number, number, !Object, number, number, number): ?} */
      BigInteger.prototype.am = getActivationStyle;
      /** @type {number} */
      dbits = 28;
    } else {
      if (nt && "Microsoft Internet Explorer" == navigator.appName) {
        /** @type {function(number, number, !Object, number, number, number): ?} */
        BigInteger.prototype.am = am1;
        /** @type {number} */
        dbits = 30;
      } else {
        if (nt && "Netscape" != navigator.appName) {
          /** @type {function(number, number, !Object, number, number, number): ?} */
          BigInteger.prototype.am = r;
          /** @type {number} */
          dbits = 26;
        } else {
          /** @type {function(number, number, !Object, number, number, number): ?} */
          BigInteger.prototype.am = getActivationStyle;
          /** @type {number} */
          dbits = 28;
        }
      }
    }
    BigInteger.prototype.DB = dbits;
    /** @type {number} */
    BigInteger.prototype.DM = (1 << dbits) - 1;
    /** @type {number} */
    BigInteger.prototype.DV = 1 << dbits;
    /** @type {number} */
    var BI_FP = 52;
    /** @type {number} */
    BigInteger.prototype.FV = Math.pow(2, BI_FP);
    /** @type {number} */
    BigInteger.prototype.F1 = BI_FP - dbits;
    /** @type {number} */
    BigInteger.prototype.F2 = 2 * dbits - BI_FP;
    var st;
    var a;
    /** @type {string} */
    var frg = "0123456789abcdefghijklmnopqrstuvwxyz";
    /** @type {!Array} */
    var dt = new Array;
    /** @type {number} */
    st = "0".charCodeAt(0);
    /** @type {number} */
    a = 0;
    for (; a <= 9; ++a) {
      /** @type {number} */
      dt[st++] = a;
    }
    /** @type {number} */
    st = "a".charCodeAt(0);
    /** @type {number} */
    a = 10;
    for (; a < 36; ++a) {
      /** @type {number} */
      dt[st++] = a;
    }
    /** @type {number} */
    st = "A".charCodeAt(0);
    /** @type {number} */
    a = 10;
    for (; a < 36; ++a) {
      /** @type {number} */
      dt[st++] = a;
    }
    /** @type {function(!Object): ?} */
    Date.prototype.convert = _MR_inner;
    /** @type {function(?): ?} */
    Date.prototype.revert = bv_trim;
    /** @type {function(string): undefined} */
    Date.prototype.reduce = retrieveRecordDidComplete;
    /** @type {function(?, !Object, undefined): undefined} */
    Date.prototype.mulTo = montMulTo;
    /** @type {function(?, undefined): undefined} */
    Date.prototype.sqrTo = mergeCircle;
    /** @type {function(!Object): ?} */
    Montgomery.prototype.convert = montConvert;
    /** @type {function(!FileEntry): ?} */
    Montgomery.prototype.revert = montRevert;
    /** @type {function(!Object): undefined} */
    Montgomery.prototype.reduce = montReduce;
    /** @type {function(?, !Object, undefined): undefined} */
    Montgomery.prototype.mulTo = pasteLine;
    /** @type {function(?, undefined): undefined} */
    Montgomery.prototype.sqrTo = uniprotFeaturePainter_diamond;
    /** @type {function(!Object): undefined} */
    BigInteger.prototype.copyTo = bnpCopyTo;
    /** @type {function(number): undefined} */
    BigInteger.prototype.fromInt = bnpFromInt;
    /** @type {function(string, number): ?} */
    BigInteger.prototype.fromString = bnpFromString;
    /** @type {function(): undefined} */
    BigInteger.prototype.clamp = bnpClamp;
    /** @type {function(number, !Object): undefined} */
    BigInteger.prototype.dlShiftTo = bnpDLShiftTo;
    /** @type {function(number, !Object): undefined} */
    BigInteger.prototype.drShiftTo = bnpDRShiftTo;
    /** @type {function(number, !Object): undefined} */
    BigInteger.prototype.lShiftTo = bnpLShiftTo;
    /** @type {function(number, !Object): ?} */
    BigInteger.prototype.rShiftTo = bnpRShiftTo;
    /** @type {function(!Object, !Object): undefined} */
    BigInteger.prototype.subTo = bnpSubTo;
    /** @type {function(!Object, !Object): undefined} */
    BigInteger.prototype.multiplyTo = bnpMultiplyTo;
    /** @type {function(!Object): undefined} */
    BigInteger.prototype.squareTo = bnpSquareTo;
    /** @type {function(!Object, !Object, !Object): ?} */
    BigInteger.prototype.divRemTo = bnpDivRemTo;
    /** @type {function(): ?} */
    BigInteger.prototype.invDigit = bnpInvDigit;
    /** @type {function(): ?} */
    BigInteger.prototype.isEven = removeLibraryRelations;
    /** @type {function(number, !Object): ?} */
    BigInteger.prototype.exp = append;
    /** @type {function(number): ?} */
    BigInteger.prototype.toString = bnToString;
    /** @type {function(): ?} */
    BigInteger.prototype.negate = bnNegate;
    /** @type {function(): ?} */
    BigInteger.prototype.abs = bnAbs;
    /** @type {function(!Object): ?} */
    BigInteger.prototype.compareTo = bnCompareTo;
    /** @type {function(): ?} */
    BigInteger.prototype.bitLength = bnBitLength;
    /** @type {function(!Object): ?} */
    BigInteger.prototype.mod = bnMod;
    /** @type {function(number, ?): ?} */
    BigInteger.prototype.modPowInt = benchmark;
    BigInteger.ZERO = nbv(0);
    BigInteger.ONE = nbv(1);
    /** @type {function(?): ?} */
    self.prototype.convert = conversion;
    /** @type {function(?): ?} */
    self.prototype.revert = conversion;
    /** @type {function(?, !Object, !Object): undefined} */
    self.prototype.mulTo = laneofpos;
    /** @type {function(?, !Object): undefined} */
    self.prototype.sqrTo = breakOrIterNext;
    /** @type {function(!Object): ?} */
    update.prototype.convert = start;
    /** @type {function(?): ?} */
    update.prototype.revert = mountResponsive;
    /** @type {function(!Object): undefined} */
    update.prototype.reduce = run;
    /** @type {function(?, !Object, undefined): undefined} */
    update.prototype.mulTo = log2_fold;
    /** @type {function(?, undefined): undefined} */
    update.prototype.sqrTo = montSqrTo;
    /** @type {!Array} */
    var res = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509];
    /** @type {number} */
    var frameSize = (1 << 26) / res[res.length - 1];
    /** @type {function(number): ?} */
    BigInteger.prototype.chunkSize = div;
    /** @type {function(number): ?} */
    BigInteger.prototype.toRadix = decode;
    /** @type {function(string, number): undefined} */
    BigInteger.prototype.fromRadix = _init;
    /** @type {function(number, number, number): undefined} */
    BigInteger.prototype.fromNumber = generateRandom;
    /** @type {function(!Object, !Function, !Object): undefined} */
    BigInteger.prototype.bitwiseTo = add;
    /** @type {function(number, !Function): ?} */
    BigInteger.prototype.changeBit = encodeZigZag64;
    /** @type {function(!Object, !Object): undefined} */
    BigInteger.prototype.addTo = bnpAddTo;
    /** @type {function(number): undefined} */
    BigInteger.prototype.dMultiply = setFromState;
    /** @type {function(number, number): undefined} */
    BigInteger.prototype.dAddOffset = _setClassNameRatio;
    /** @type {function(!Object, string, !Object): undefined} */
    BigInteger.prototype.multiplyLowerTo = subtract;
    /** @type {function(!Object, string, !Object): undefined} */
    BigInteger.prototype.multiplyUpperTo = init;
    /** @type {function(number): ?} */
    BigInteger.prototype.modInt = items2text;
    /** @type {function(number): ?} */
    BigInteger.prototype.millerRabin = finish;
    /** @type {function(): ?} */
    BigInteger.prototype.clone = clone;
    /** @type {function(): ?} */
    BigInteger.prototype.intValue = cancel;
    /** @type {function(): ?} */
    BigInteger.prototype.byteValue = moveLibraryRelations;
    /** @type {function(): ?} */
    BigInteger.prototype.shortValue = _setIEOpacityRatio;
    /** @type {function(): ?} */
    BigInteger.prototype.signum = calculateTimes;
    /** @type {function(): ?} */
    BigInteger.prototype.toByteArray = clear;
    /** @type {function(!Object): ?} */
    BigInteger.prototype.equals = equals;
    /** @type {function(!Object): ?} */
    BigInteger.prototype.min = compareTo;
    /** @type {function(!Object): ?} */
    BigInteger.prototype.max = handleMouseOverForNode;
    /** @type {function(!Object): ?} */
    BigInteger.prototype.and = fn;
    /** @type {function(!Object): ?} */
    BigInteger.prototype.or = getConnectorTestFilePath;
    /** @type {function(!Object): ?} */
    BigInteger.prototype.xor = xor;
    /** @type {function(!Object): ?} */
    BigInteger.prototype.andNot = extractPresetLocal;
    /** @type {function(): ?} */
    BigInteger.prototype.not = updateSoldierDisplay;
    /** @type {function(number): ?} */
    BigInteger.prototype.shiftLeft = left;
    /** @type {function(number): ?} */
    BigInteger.prototype.shiftRight = mpi_from_buffer;
    /** @type {function(): ?} */
    BigInteger.prototype.getLowestSetBit = error;
    /** @type {function(): ?} */
    BigInteger.prototype.bitCount = walk;
    /** @type {function(number): ?} */
    BigInteger.prototype.testBit = check;
    /** @type {function(undefined): ?} */
    BigInteger.prototype.setBit = clickWithWebdriver;
    /** @type {function(undefined): ?} */
    BigInteger.prototype.clearBit = clickWithJavaScript;
    /** @type {function(undefined): ?} */
    BigInteger.prototype.flipBit = writeSearchEntry;
    /** @type {function(!Object): ?} */
    BigInteger.prototype.add = bnAdd;
    /** @type {function(!Object): ?} */
    BigInteger.prototype.subtract = bnSubtract;
    /** @type {function(!Array): ?} */
    BigInteger.prototype.multiply = bnMultiply;
    /** @type {function(!Object): ?} */
    BigInteger.prototype.divide = bnDivide;
    /** @type {function(!Object): ?} */
    BigInteger.prototype.remainder = str;
    /** @type {function(!Object): ?} */
    BigInteger.prototype.divideAndRemainder = nbs;
    /** @type {function(!Object, ?): ?} */
    BigInteger.prototype.modPow = bnModPow;
    /** @type {function(!Object): ?} */
    BigInteger.prototype.modInverse = test;
    /** @type {function(number): ?} */
    BigInteger.prototype.pow = gauss;
    /** @type {function(!Object): ?} */
    BigInteger.prototype.gcd = create;
    /** @type {function(number): ?} */
    BigInteger.prototype.isProbablePrime = draw;
  }, function(mixin, canCreateDiscussions, FORGE) {
    /**
     * @return {undefined}
     */
    function _init() {
      /** @type {string} */
      _padding = String.fromCharCode(128);
      _padding = _padding + forge.util.fillString(String.fromCharCode(0), 64);
      /** @type {!Array} */
      copyProps = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 1, 6, 11, 0, 5, 10, 15, 4, 9, 14, 3, 8, 13, 2, 7, 12, 5, 8, 11, 14, 1, 4, 7, 10, 13, 0, 3, 6, 9, 12, 15, 2, 0, 7, 14, 5, 12, 3, 10, 1, 8, 15, 6, 13, 4, 11, 2, 9];
      /** @type {!Array} */
      spriteSheets = [7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21];
      /** @type {!Array} */
      _k = new Array(64);
      /** @type {number} */
      var i = 0;
      for (; i < 64; ++i) {
        /** @type {number} */
        _k[i] = Math.floor(4294967296 * Math.abs(Math.sin(i + 1)));
      }
      /** @type {boolean} */
      p = true;
    }
    /**
     * @param {!Object} s
     * @param {!Array} opts
     * @param {!NodeList} bytes
     * @return {undefined}
     */
    function _update(s, opts, bytes) {
      var t;
      var a;
      var b;
      var c;
      var d;
      var f;
      var ss;
      var i;
      var at = bytes.length();
      for (; at >= 64;) {
        a = s.h0;
        b = s.h1;
        c = s.h2;
        d = s.h3;
        /** @type {number} */
        i = 0;
        for (; i < 16; ++i) {
          opts[i] = bytes.getInt32Le();
          /** @type {number} */
          f = d ^ b & (c ^ d);
          t = a + f + _k[i] + opts[i];
          ss = spriteSheets[i];
          a = d;
          d = c;
          c = b;
          b = b + (t << ss | t >>> 32 - ss);
        }
        for (; i < 32; ++i) {
          /** @type {number} */
          f = c ^ d & (b ^ c);
          t = a + f + _k[i] + opts[copyProps[i]];
          ss = spriteSheets[i];
          a = d;
          d = c;
          c = b;
          b = b + (t << ss | t >>> 32 - ss);
        }
        for (; i < 48; ++i) {
          /** @type {number} */
          f = b ^ c ^ d;
          t = a + f + _k[i] + opts[copyProps[i]];
          ss = spriteSheets[i];
          a = d;
          d = c;
          c = b;
          b = b + (t << ss | t >>> 32 - ss);
        }
        for (; i < 64; ++i) {
          /** @type {number} */
          f = c ^ (b | ~d);
          t = a + f + _k[i] + opts[copyProps[i]];
          ss = spriteSheets[i];
          a = d;
          d = c;
          c = b;
          b = b + (t << ss | t >>> 32 - ss);
        }
        /** @type {number} */
        s.h0 = s.h0 + a | 0;
        /** @type {number} */
        s.h1 = s.h1 + b | 0;
        /** @type {number} */
        s.h2 = s.h2 + c | 0;
        /** @type {number} */
        s.h3 = s.h3 + d | 0;
        /** @type {number} */
        at = at - 64;
      }
    }
    var forge = FORGE(0);
    FORGE(4);
    FORGE(1);
    var md5 = mixin.exports = forge.md5 = forge.md5 || {};
    forge.md.md5 = forge.md.algorithms.md5 = md5;
    /**
     * @return {?}
     */
    md5.create = function() {
      if (!p) {
        _init();
      }
      /** @type {null} */
      var _state = null;
      var _input = forge.util.createBuffer();
      /** @type {!Array} */
      var path = new Array(16);
      var md = {
        algorithm : "md5",
        blockLength : 64,
        digestLength : 16,
        messageLength : 0,
        fullMessageLength : null,
        messageLengthSize : 8
      };
      return md.start = function() {
        /** @type {number} */
        md.messageLength = 0;
        /** @type {!Array} */
        md.fullMessageLength = md.messageLength64 = [];
        /** @type {number} */
        var eleSize = md.messageLengthSize / 4;
        /** @type {number} */
        var x = 0;
        for (; x < eleSize; ++x) {
          md.fullMessageLength.push(0);
        }
        return _input = forge.util.createBuffer(), _state = {
          h0 : 1732584193,
          h1 : 4023233417,
          h2 : 2562383102,
          h3 : 271733878
        }, md;
      }, md.start(), md.update = function(msg, encoding) {
        if ("utf8" === encoding) {
          msg = forge.util.encodeUtf8(msg);
        }
        var len = msg.length;
        md.messageLength += len;
        /** @type {!Array} */
        len = [len / 4294967296 >>> 0, len >>> 0];
        /** @type {number} */
        var i = md.fullMessageLength.length - 1;
        for (; i >= 0; --i) {
          md.fullMessageLength[i] += len[1];
          len[1] = len[0] + (md.fullMessageLength[i] / 4294967296 >>> 0);
          /** @type {number} */
          md.fullMessageLength[i] = md.fullMessageLength[i] >>> 0;
          /** @type {number} */
          len[0] = len[1] / 4294967296 >>> 0;
        }
        return _input.putBytes(msg), _update(_state, path, _input), (_input.read > 2048 || 0 === _input.length()) && _input.compact(), md;
      }, md.digest = function() {
        var finalBlock = forge.util.createBuffer();
        finalBlock.putBytes(_input.bytes());
        var remaining = md.fullMessageLength[md.fullMessageLength.length - 1] + md.messageLengthSize;
        /** @type {number} */
        var overflow = remaining & md.blockLength - 1;
        finalBlock.putBytes(_padding.substr(0, md.blockLength - overflow));
        var bits;
        /** @type {number} */
        var carry = 0;
        /** @type {number} */
        var i = md.fullMessageLength.length - 1;
        for (; i >= 0; --i) {
          /** @type {number} */
          bits = 8 * md.fullMessageLength[i] + carry;
          /** @type {number} */
          carry = bits / 4294967296 >>> 0;
          finalBlock.putInt32Le(bits >>> 0);
        }
        var s2 = {
          h0 : _state.h0,
          h1 : _state.h1,
          h2 : _state.h2,
          h3 : _state.h3
        };
        _update(s2, path, finalBlock);
        var rval = forge.util.createBuffer();
        return rval.putInt32Le(s2.h0), rval.putInt32Le(s2.h1), rval.putInt32Le(s2.h2), rval.putInt32Le(s2.h3), rval;
      }, md;
    };
    /** @type {null} */
    var _padding = null;
    /** @type {null} */
    var copyProps = null;
    /** @type {null} */
    var spriteSheets = null;
    /** @type {null} */
    var _k = null;
    /** @type {boolean} */
    var p = false;
  }, function(mixin, canCreateDiscussions, require) {
    var forge = require(0);
    require(8);
    require(4);
    require(1);
    var crypto;
    var compat = forge.pkcs5 = forge.pkcs5 || {};
    if (forge.util.isNodejs && !forge.options.usePureJavaScript) {
      crypto = require(32);
    }
    /** @type {function(string, ?, ?, number, string, string): ?} */
    mixin.exports = forge.pbkdf2 = compat.pbkdf2 = function(key, s, c, dkLen, md, callback) {
      /**
       * @return {?}
       */
      function outer() {
        return i > len ? callback(null, dk) : (that.start(null, null), that.update(s), that.update(forge.util.int32ToBytes(i)), xor = v = that.digest().getBytes(), x = 2, void inner());
      }
      /**
       * @return {?}
       */
      function inner() {
        return x <= c ? (that.start(null, null), that.update(v), u_c = that.digest().getBytes(), xor = forge.util.xorBytes(xor, u_c, hLen), v = u_c, ++x, forge.util.setImmediate(inner)) : (dk = dk + (i < len ? xor : xor.substr(0, r)), ++i, void outer());
      }
      if ("function" == typeof md && (callback = md, md = null), forge.util.isNodejs && !forge.options.usePureJavaScript && crypto.pbkdf2 && (null === md || "object" != typeof md) && (crypto.pbkdf2Sync.length > 4 || !md || "sha1" === md)) {
        return "string" != typeof md && (md = "sha1"), key = new Buffer(key, "binary"), s = new Buffer(s, "binary"), callback ? 4 === crypto.pbkdf2Sync.length ? crypto.pbkdf2(key, s, c, dkLen, function(value, clientResponseBuf) {
          return value ? callback(value) : void callback(null, clientResponseBuf.toString("binary"));
        }) : crypto.pbkdf2(key, s, c, dkLen, md, function(value, clientResponseBuf) {
          return value ? callback(value) : void callback(null, clientResponseBuf.toString("binary"));
        }) : 4 === crypto.pbkdf2Sync.length ? crypto.pbkdf2Sync(key, s, c, dkLen).toString("binary") : crypto.pbkdf2Sync(key, s, c, dkLen, md).toString("binary");
      }
      if ("undefined" != typeof md && null !== md || (md = "sha1"), "string" == typeof md) {
        if (!(md in forge.md.algorithms)) {
          throw new Error("Unknown hash algorithm: " + md);
        }
        md = forge.md[md].create();
      }
      var hLen = md.digestLength;
      if (dkLen > 4294967295 * hLen) {
        /** @type {!Error} */
        var p = new Error("Derived key is too long.");
        if (callback) {
          return callback(p);
        }
        throw p;
      }
      /** @type {number} */
      var len = Math.ceil(dkLen / hLen);
      /** @type {number} */
      var r = dkLen - (len - 1) * hLen;
      var that = forge.hmac.create();
      that.start(md, key);
      var xor;
      var u_c;
      var v;
      /** @type {string} */
      var dk = "";
      if (!callback) {
        /** @type {number} */
        var i = 1;
        for (; i <= len; ++i) {
          that.start(null, null);
          that.update(s);
          that.update(forge.util.int32ToBytes(i));
          xor = v = that.digest().getBytes();
          /** @type {number} */
          var x = 2;
          for (; x <= c; ++x) {
            that.start(null, null);
            that.update(v);
            u_c = that.digest().getBytes();
            xor = forge.util.xorBytes(xor, u_c, hLen);
            v = u_c;
          }
          dk = dk + (i < len ? xor : xor.substr(0, r));
        }
        return dk;
      }
      /** @type {number} */
      i = 1;
      outer();
    };
  }, function(mixin, canCreateDiscussions, FORGE) {
    var forge = FORGE(0);
    FORGE(2);
    FORGE(1);
    var directory_epub = mixin.exports = forge.pss = forge.pss || {};
    /**
     * @param {!Object} options
     * @return {?}
     */
    directory_epub.create = function(options) {
      if (3 === arguments.length) {
        options = {
          md : arguments[0],
          mgf : arguments[1],
          saltLength : arguments[2]
        };
      }
      var hash = options.md;
      var r = options.mgf;
      var hLen = hash.digestLength;
      var data = options.salt || null;
      if ("string" == typeof data) {
        data = forge.util.createBuffer(data);
      }
      var sLen;
      if ("saltLength" in options) {
        sLen = options.saltLength;
      } else {
        if (null === data) {
          throw new Error("Salt length not specified or specific salt not given.");
        }
        sLen = data.length();
      }
      if (null !== data && data.length() !== sLen) {
        throw new Error("Given salt length does not match length of given salt.");
      }
      var prng = options.prng || forge.random;
      var pss = {};
      return pss.encode = function(item, target) {
        var i;
        /** @type {number} */
        var emBits = target - 1;
        /** @type {number} */
        var emLen = Math.ceil(emBits / 8);
        var salt = item.digest().getBytes();
        if (emLen < hLen + sLen + 2) {
          throw new Error("Message is too long to encrypt.");
        }
        var bytes;
        bytes = null === data ? prng.getBytesSync(sLen) : data.bytes();
        var m_ = new forge.util.ByteBuffer;
        m_.fillWithByte(0, 8);
        m_.putBytes(salt);
        m_.putBytes(bytes);
        hash.start();
        hash.update(m_.getBytes());
        var y = hash.digest().getBytes();
        var b = new forge.util.ByteBuffer;
        b.fillWithByte(0, emLen - sLen - hLen - 2);
        b.putByte(1);
        b.putBytes(bytes);
        var converted_function = b.getBytes();
        /** @type {number} */
        var f = emLen - hLen - 1;
        var key = r.generate(y, f);
        /** @type {string} */
        var root = "";
        /** @type {number} */
        i = 0;
        for (; i < f; i++) {
          /** @type {string} */
          root = root + String.fromCharCode(converted_function.charCodeAt(i) ^ key.charCodeAt(i));
        }
        /** @type {number} */
        var S = 65280 >> 8 * emLen - emBits & 255;
        return root = String.fromCharCode(root.charCodeAt(0) & ~S) + root.substr(1), root + y + String.fromCharCode(188);
      }, pss.verify = function(data, str, val) {
        var i;
        /** @type {number} */
        var emBits = val - 1;
        /** @type {number} */
        var emLen = Math.ceil(emBits / 8);
        if (str = str.substr(-emLen), emLen < hLen + sLen + 2) {
          throw new Error("Inconsistent parameters to PSS signature verification.");
        }
        if (188 !== str.charCodeAt(emLen - 1)) {
          throw new Error("Encoded message does not end in 0xBC.");
        }
        /** @type {number} */
        var p = emLen - hLen - 1;
        var bin = str.substr(0, p);
        var f = str.substr(p, hLen);
        /** @type {number} */
        var d = 65280 >> 8 * emLen - emBits & 255;
        if (0 !== (bin.charCodeAt(0) & d)) {
          throw new Error("Bits beyond keysize not zero as expected.");
        }
        var prefix = r.generate(f, p);
        /** @type {string} */
        var db = "";
        /** @type {number} */
        i = 0;
        for (; i < p; i++) {
          /** @type {string} */
          db = db + String.fromCharCode(bin.charCodeAt(i) ^ prefix.charCodeAt(i));
        }
        /** @type {string} */
        db = String.fromCharCode(db.charCodeAt(0) & ~d) + db.substr(1);
        /** @type {number} */
        var checkLen = emLen - hLen - sLen - 2;
        /** @type {number} */
        i = 0;
        for (; i < checkLen; i++) {
          if (0 !== db.charCodeAt(i)) {
            throw new Error("Leftmost octets not zero as expected");
          }
        }
        if (1 !== db.charCodeAt(checkLen)) {
          throw new Error("Inconsistent PSS signature, 0x01 marker not found");
        }
        /** @type {string} */
        var salt = db.substr(-sLen);
        var ps = new forge.util.ByteBuffer;
        ps.fillWithByte(0, 8);
        ps.putBytes(data);
        ps.putBytes(salt);
        hash.start();
        hash.update(ps.getBytes());
        var _yz_rk = hash.digest().getBytes();
        return f === _yz_rk;
      }, pss;
    };
  }, function(mixin, canCreateDiscussions, FORGE) {
    /**
     * @param {!Object} obj
     * @param {!Object} options
     * @return {?}
     */
    function _getAttribute(obj, options) {
      if ("string" == typeof options) {
        options = {
          shortName : options
        };
      }
      var attr;
      /** @type {null} */
      var rval = null;
      /** @type {number} */
      var i = 0;
      for (; null === rval && i < obj.attributes.length; ++i) {
        attr = obj.attributes[i];
        if (options.type && options.type === attr.type) {
          rval = attr;
        } else {
          if (options.name && options.name === attr.name) {
            rval = attr;
          } else {
            if (options.shortName && options.shortName === attr.shortName) {
              rval = attr;
            }
          }
        }
      }
      return rval;
    }
    /**
     * @param {!Object} obj
     * @return {?}
     */
    function _dnToAsn1(obj) {
      var attr;
      var falseySection;
      var rval = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, []);
      var attributes = obj.attributes;
      /** @type {number} */
      var i = 0;
      for (; i < attributes.length; ++i) {
        attr = attributes[i];
        var value = attr.value;
        var valueTagClass = asn1.Type.PRINTABLESTRING;
        if ("valueTagClass" in attr) {
          valueTagClass = attr.valueTagClass;
          if (valueTagClass === asn1.Type.UTF8) {
            value = forge.util.encodeUtf8(value);
          }
        }
        falseySection = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SET, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(attr.type).getBytes()), asn1.create(asn1.Class.UNIVERSAL, valueTagClass, false, value)])]);
        rval.value.push(falseySection);
      }
      return rval;
    }
    /**
     * @param {!Object} attrs
     * @return {undefined}
     */
    function _fillMissingFields(attrs) {
      var attr;
      /** @type {number} */
      var i = 0;
      for (; i < attrs.length; ++i) {
        if (attr = attrs[i], "undefined" == typeof attr.name && (attr.type && attr.type in pki.oids ? attr.name = pki.oids[attr.type] : attr.shortName && attr.shortName in _shortNames && (attr.name = pki.oids[_shortNames[attr.shortName]])), "undefined" == typeof attr.type) {
          if (!(attr.name && attr.name in pki.oids)) {
            /** @type {!Error} */
            var error = new Error("Attribute type not specified.");
            throw error.attribute = attr, error;
          }
          attr.type = pki.oids[attr.name];
        }
        if ("undefined" == typeof attr.shortName && attr.name && attr.name in _shortNames && (attr.shortName = _shortNames[attr.name]), attr.type === oids.extensionRequest && (attr.valueConstructed = true, attr.valueTagClass = asn1.Type.SEQUENCE, !attr.value && attr.extensions)) {
          /** @type {!Array} */
          attr.value = [];
          /** @type {number} */
          var i = 0;
          for (; i < attr.extensions.length; ++i) {
            attr.value.push(pki.certificateExtensionToAsn1(_fillMissingExtensionFields(attr.extensions[i])));
          }
        }
        if ("undefined" == typeof attr.value) {
          /** @type {!Error} */
          error = new Error("Attribute value not specified.");
          throw error.attribute = attr, error;
        }
      }
    }
    /**
     * @param {!Object} e
     * @param {!Object} options
     * @return {?}
     */
    function _fillMissingExtensionFields(e, options) {
      if (options = options || {}, "undefined" == typeof e.name && e.id && e.id in pki.oids && (e.name = pki.oids[e.id]), "undefined" == typeof e.id) {
        if (!(e.name && e.name in pki.oids)) {
          /** @type {!Error} */
          var error = new Error("Extension ID not specified.");
          throw error.extension = e, error;
        }
        e.id = pki.oids[e.name];
      }
      if ("undefined" != typeof e.value) {
        return e;
      }
      if ("keyUsage" === e.name) {
        /** @type {number} */
        var unused = 0;
        /** @type {number} */
        var b = 0;
        /** @type {number} */
        var c = 0;
        if (e.digitalSignature) {
          /** @type {number} */
          b = b | 128;
          /** @type {number} */
          unused = 7;
        }
        if (e.nonRepudiation) {
          /** @type {number} */
          b = b | 64;
          /** @type {number} */
          unused = 6;
        }
        if (e.keyEncipherment) {
          /** @type {number} */
          b = b | 32;
          /** @type {number} */
          unused = 5;
        }
        if (e.dataEncipherment) {
          /** @type {number} */
          b = b | 16;
          /** @type {number} */
          unused = 4;
        }
        if (e.keyAgreement) {
          /** @type {number} */
          b = b | 8;
          /** @type {number} */
          unused = 3;
        }
        if (e.keyCertSign) {
          /** @type {number} */
          b = b | 4;
          /** @type {number} */
          unused = 2;
        }
        if (e.cRLSign) {
          /** @type {number} */
          b = b | 2;
          /** @type {number} */
          unused = 1;
        }
        if (e.encipherOnly) {
          /** @type {number} */
          b = b | 1;
          /** @type {number} */
          unused = 0;
        }
        if (e.decipherOnly) {
          /** @type {number} */
          c = c | 128;
          /** @type {number} */
          unused = 7;
        }
        /** @type {string} */
        var value = String.fromCharCode(unused);
        if (0 !== c) {
          /** @type {string} */
          value = value + (String.fromCharCode(b) + String.fromCharCode(c));
        } else {
          if (0 !== b) {
            /** @type {string} */
            value = value + String.fromCharCode(b);
          }
        }
        e.value = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.BITSTRING, false, value);
      } else {
        if ("basicConstraints" === e.name) {
          e.value = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, []);
          if (e.cA) {
            e.value.value.push(asn1.create(asn1.Class.UNIVERSAL, asn1.Type.BOOLEAN, false, String.fromCharCode(255)));
          }
          if ("pathLenConstraint" in e) {
            e.value.value.push(asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, asn1.integerToDer(e.pathLenConstraint).getBytes()));
          }
        } else {
          if ("extKeyUsage" === e.name) {
            e.value = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, []);
            var result = e.value.value;
            var key;
            for (key in e) {
              if (e[key] === true) {
                if (key in oids) {
                  result.push(asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(oids[key]).getBytes()));
                } else {
                  if (key.indexOf(".") !== -1) {
                    result.push(asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(key).getBytes()));
                  }
                }
              }
            }
          } else {
            if ("nsCertType" === e.name) {
              /** @type {number} */
              unused = 0;
              /** @type {number} */
              b = 0;
              if (e.client) {
                /** @type {number} */
                b = b | 128;
                /** @type {number} */
                unused = 7;
              }
              if (e.server) {
                /** @type {number} */
                b = b | 64;
                /** @type {number} */
                unused = 6;
              }
              if (e.email) {
                /** @type {number} */
                b = b | 32;
                /** @type {number} */
                unused = 5;
              }
              if (e.objsign) {
                /** @type {number} */
                b = b | 16;
                /** @type {number} */
                unused = 4;
              }
              if (e.reserved) {
                /** @type {number} */
                b = b | 8;
                /** @type {number} */
                unused = 3;
              }
              if (e.sslCA) {
                /** @type {number} */
                b = b | 4;
                /** @type {number} */
                unused = 2;
              }
              if (e.emailCA) {
                /** @type {number} */
                b = b | 2;
                /** @type {number} */
                unused = 1;
              }
              if (e.objCA) {
                /** @type {number} */
                b = b | 1;
                /** @type {number} */
                unused = 0;
              }
              /** @type {string} */
              value = String.fromCharCode(unused);
              if (0 !== b) {
                /** @type {string} */
                value = value + String.fromCharCode(b);
              }
              e.value = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.BITSTRING, false, value);
            } else {
              if ("subjectAltName" === e.name || "issuerAltName" === e.name) {
                e.value = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, []);
                var altName;
                /** @type {number} */
                var n = 0;
                for (; n < e.altNames.length; ++n) {
                  altName = e.altNames[n];
                  value = altName.value;
                  if (7 === altName.type && altName.ip) {
                    if (value = forge.util.bytesFromIP(altName.ip), null === value) {
                      /** @type {!Error} */
                      error = new Error('Extension "ip" value is not a valid IPv4 or IPv6 address.');
                      throw error.extension = e, error;
                    }
                  } else {
                    if (8 === altName.type) {
                      value = altName.oid ? asn1.oidToDer(asn1.oidToDer(altName.oid)) : asn1.oidToDer(value);
                    }
                  }
                  e.value.value.push(asn1.create(asn1.Class.CONTEXT_SPECIFIC, altName.type, false, value));
                }
              } else {
                if ("subjectKeyIdentifier" === e.name && options.cert) {
                  var p = options.cert.generateSubjectKeyIdentifier();
                  e.subjectKeyIdentifier = p.toHex();
                  e.value = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OCTETSTRING, false, p.getBytes());
                } else {
                  if ("authorityKeyIdentifier" === e.name && options.cert) {
                    e.value = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, []);
                    result = e.value.value;
                    if (e.keyIdentifier) {
                      var value = e.keyIdentifier === true ? options.cert.generateSubjectKeyIdentifier().getBytes() : e.keyIdentifier;
                      result.push(asn1.create(asn1.Class.CONTEXT_SPECIFIC, 0, false, value));
                    }
                    if (e.authorityCertIssuer) {
                      /** @type {!Array} */
                      var value = [asn1.create(asn1.Class.CONTEXT_SPECIFIC, 4, true, [_dnToAsn1(e.authorityCertIssuer === true ? options.cert.issuer : e.authorityCertIssuer)])];
                      result.push(asn1.create(asn1.Class.CONTEXT_SPECIFIC, 1, true, value));
                    }
                    if (e.serialNumber) {
                      var value = forge.util.hexToBytes(e.serialNumber === true ? options.cert.serialNumber : e.serialNumber);
                      result.push(asn1.create(asn1.Class.CONTEXT_SPECIFIC, 2, false, value));
                    }
                  } else {
                    if ("cRLDistributionPoints" === e.name) {
                      e.value = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, []);
                      result = e.value.value;
                      var runOption = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, []);
                      var fullNameGeneralNames = asn1.create(asn1.Class.CONTEXT_SPECIFIC, 0, true, []);
                      /** @type {number} */
                      n = 0;
                      for (; n < e.altNames.length; ++n) {
                        altName = e.altNames[n];
                        value = altName.value;
                        if (7 === altName.type && altName.ip) {
                          if (value = forge.util.bytesFromIP(altName.ip), null === value) {
                            /** @type {!Error} */
                            error = new Error('Extension "ip" value is not a valid IPv4 or IPv6 address.');
                            throw error.extension = e, error;
                          }
                        } else {
                          if (8 === altName.type) {
                            value = altName.oid ? asn1.oidToDer(asn1.oidToDer(altName.oid)) : asn1.oidToDer(value);
                          }
                        }
                        fullNameGeneralNames.value.push(asn1.create(asn1.Class.CONTEXT_SPECIFIC, altName.type, false, value));
                      }
                      runOption.value.push(asn1.create(asn1.Class.CONTEXT_SPECIFIC, 0, true, [fullNameGeneralNames]));
                      result.push(runOption);
                    }
                  }
                }
              }
            }
          }
        }
      }
      if ("undefined" == typeof e.value) {
        /** @type {!Error} */
        error = new Error("Extension value not specified.");
        throw error.extension = e, error;
      }
      return e;
    }
    /**
     * @param {?} oid
     * @param {!Object} params
     * @return {?}
     */
    function _signatureParametersToAsn1(oid, params) {
      switch(oid) {
        case oids["RSASSA-PSS"]:
          /** @type {!Array} */
          var content = [];
          return void 0 !== params.hash.algorithmOid && content.push(asn1.create(asn1.Class.CONTEXT_SPECIFIC, 0, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(params.hash.algorithmOid).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.NULL, false, "")])])), void 0 !== params.mgf.algorithmOid && content.push(asn1.create(asn1.Class.CONTEXT_SPECIFIC, 1, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, 
          true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(params.mgf.algorithmOid).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(params.mgf.hash.algorithmOid).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.NULL, false, "")])])])), void 0 !== params.saltLength && content.push(asn1.create(asn1.Class.CONTEXT_SPECIFIC, 2, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, 
          false, asn1.integerToDer(params.saltLength).getBytes())])), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, content);
        default:
          return asn1.create(asn1.Class.UNIVERSAL, asn1.Type.NULL, false, "");
      }
    }
    /**
     * @param {!Object} csr
     * @return {?}
     */
    function _CRIAttributesToAsn1(csr) {
      var rval = asn1.create(asn1.Class.CONTEXT_SPECIFIC, 0, true, []);
      if (0 === csr.attributes.length) {
        return rval;
      }
      var attrs = csr.attributes;
      /** @type {number} */
      var i1 = 0;
      for (; i1 < attrs.length; ++i1) {
        var attr = attrs[i1];
        var value = attr.value;
        var valueTagClass = asn1.Type.UTF8;
        if ("valueTagClass" in attr) {
          valueTagClass = attr.valueTagClass;
        }
        if (valueTagClass === asn1.Type.UTF8) {
          value = forge.util.encodeUtf8(value);
        }
        /** @type {boolean} */
        var valueConstructed = false;
        if ("valueConstructed" in attr) {
          valueConstructed = attr.valueConstructed;
        }
        var falseySection = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(attr.type).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SET, true, [asn1.create(asn1.Class.UNIVERSAL, valueTagClass, valueConstructed, value)])]);
        rval.value.push(falseySection);
      }
      return rval;
    }
    var forge = FORGE(0);
    FORGE(5);
    FORGE(3);
    FORGE(10);
    FORGE(4);
    FORGE(37);
    FORGE(6);
    FORGE(7);
    FORGE(16);
    FORGE(11);
    FORGE(1);
    var asn1 = forge.asn1;
    var pki = mixin.exports = forge.pki = forge.pki || {};
    var oids = pki.oids;
    var _shortNames = {};
    _shortNames.CN = oids.commonName;
    /** @type {string} */
    _shortNames.commonName = "CN";
    _shortNames.C = oids.countryName;
    /** @type {string} */
    _shortNames.countryName = "C";
    _shortNames.L = oids.localityName;
    /** @type {string} */
    _shortNames.localityName = "L";
    _shortNames.ST = oids.stateOrProvinceName;
    /** @type {string} */
    _shortNames.stateOrProvinceName = "ST";
    _shortNames.O = oids.organizationName;
    /** @type {string} */
    _shortNames.organizationName = "O";
    _shortNames.OU = oids.organizationalUnitName;
    /** @type {string} */
    _shortNames.organizationalUnitName = "OU";
    _shortNames.E = oids.emailAddress;
    /** @type {string} */
    _shortNames.emailAddress = "E";
    var publicKeyValidator = forge.pki.rsa.publicKeyValidator;
    var x509CertificateValidator = {
      name : "Certificate",
      tagClass : asn1.Class.UNIVERSAL,
      type : asn1.Type.SEQUENCE,
      constructed : true,
      value : [{
        name : "Certificate.TBSCertificate",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.SEQUENCE,
        constructed : true,
        captureAsn1 : "tbsCertificate",
        value : [{
          name : "Certificate.TBSCertificate.version",
          tagClass : asn1.Class.CONTEXT_SPECIFIC,
          type : 0,
          constructed : true,
          optional : true,
          value : [{
            name : "Certificate.TBSCertificate.version.integer",
            tagClass : asn1.Class.UNIVERSAL,
            type : asn1.Type.INTEGER,
            constructed : false,
            capture : "certVersion"
          }]
        }, {
          name : "Certificate.TBSCertificate.serialNumber",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.INTEGER,
          constructed : false,
          capture : "certSerialNumber"
        }, {
          name : "Certificate.TBSCertificate.signature",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.SEQUENCE,
          constructed : true,
          value : [{
            name : "Certificate.TBSCertificate.signature.algorithm",
            tagClass : asn1.Class.UNIVERSAL,
            type : asn1.Type.OID,
            constructed : false,
            capture : "certinfoSignatureOid"
          }, {
            name : "Certificate.TBSCertificate.signature.parameters",
            tagClass : asn1.Class.UNIVERSAL,
            optional : true,
            captureAsn1 : "certinfoSignatureParams"
          }]
        }, {
          name : "Certificate.TBSCertificate.issuer",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.SEQUENCE,
          constructed : true,
          captureAsn1 : "certIssuer"
        }, {
          name : "Certificate.TBSCertificate.validity",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.SEQUENCE,
          constructed : true,
          value : [{
            name : "Certificate.TBSCertificate.validity.notBefore (utc)",
            tagClass : asn1.Class.UNIVERSAL,
            type : asn1.Type.UTCTIME,
            constructed : false,
            optional : true,
            capture : "certValidity1UTCTime"
          }, {
            name : "Certificate.TBSCertificate.validity.notBefore (generalized)",
            tagClass : asn1.Class.UNIVERSAL,
            type : asn1.Type.GENERALIZEDTIME,
            constructed : false,
            optional : true,
            capture : "certValidity2GeneralizedTime"
          }, {
            name : "Certificate.TBSCertificate.validity.notAfter (utc)",
            tagClass : asn1.Class.UNIVERSAL,
            type : asn1.Type.UTCTIME,
            constructed : false,
            optional : true,
            capture : "certValidity3UTCTime"
          }, {
            name : "Certificate.TBSCertificate.validity.notAfter (generalized)",
            tagClass : asn1.Class.UNIVERSAL,
            type : asn1.Type.GENERALIZEDTIME,
            constructed : false,
            optional : true,
            capture : "certValidity4GeneralizedTime"
          }]
        }, {
          name : "Certificate.TBSCertificate.subject",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.SEQUENCE,
          constructed : true,
          captureAsn1 : "certSubject"
        }, publicKeyValidator, {
          name : "Certificate.TBSCertificate.issuerUniqueID",
          tagClass : asn1.Class.CONTEXT_SPECIFIC,
          type : 1,
          constructed : true,
          optional : true,
          value : [{
            name : "Certificate.TBSCertificate.issuerUniqueID.id",
            tagClass : asn1.Class.UNIVERSAL,
            type : asn1.Type.BITSTRING,
            constructed : false,
            captureBitStringValue : "certIssuerUniqueId"
          }]
        }, {
          name : "Certificate.TBSCertificate.subjectUniqueID",
          tagClass : asn1.Class.CONTEXT_SPECIFIC,
          type : 2,
          constructed : true,
          optional : true,
          value : [{
            name : "Certificate.TBSCertificate.subjectUniqueID.id",
            tagClass : asn1.Class.UNIVERSAL,
            type : asn1.Type.BITSTRING,
            constructed : false,
            captureBitStringValue : "certSubjectUniqueId"
          }]
        }, {
          name : "Certificate.TBSCertificate.extensions",
          tagClass : asn1.Class.CONTEXT_SPECIFIC,
          type : 3,
          constructed : true,
          captureAsn1 : "certExtensions",
          optional : true
        }]
      }, {
        name : "Certificate.signatureAlgorithm",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.SEQUENCE,
        constructed : true,
        value : [{
          name : "Certificate.signatureAlgorithm.algorithm",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.OID,
          constructed : false,
          capture : "certSignatureOid"
        }, {
          name : "Certificate.TBSCertificate.signature.parameters",
          tagClass : asn1.Class.UNIVERSAL,
          optional : true,
          captureAsn1 : "certSignatureParams"
        }]
      }, {
        name : "Certificate.signatureValue",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.BITSTRING,
        constructed : false,
        captureBitStringValue : "certSignature"
      }]
    };
    var rsaPrivateKeyValidator = {
      name : "rsapss",
      tagClass : asn1.Class.UNIVERSAL,
      type : asn1.Type.SEQUENCE,
      constructed : true,
      value : [{
        name : "rsapss.hashAlgorithm",
        tagClass : asn1.Class.CONTEXT_SPECIFIC,
        type : 0,
        constructed : true,
        value : [{
          name : "rsapss.hashAlgorithm.AlgorithmIdentifier",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Class.SEQUENCE,
          constructed : true,
          optional : true,
          value : [{
            name : "rsapss.hashAlgorithm.AlgorithmIdentifier.algorithm",
            tagClass : asn1.Class.UNIVERSAL,
            type : asn1.Type.OID,
            constructed : false,
            capture : "hashOid"
          }]
        }]
      }, {
        name : "rsapss.maskGenAlgorithm",
        tagClass : asn1.Class.CONTEXT_SPECIFIC,
        type : 1,
        constructed : true,
        value : [{
          name : "rsapss.maskGenAlgorithm.AlgorithmIdentifier",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Class.SEQUENCE,
          constructed : true,
          optional : true,
          value : [{
            name : "rsapss.maskGenAlgorithm.AlgorithmIdentifier.algorithm",
            tagClass : asn1.Class.UNIVERSAL,
            type : asn1.Type.OID,
            constructed : false,
            capture : "maskGenOid"
          }, {
            name : "rsapss.maskGenAlgorithm.AlgorithmIdentifier.params",
            tagClass : asn1.Class.UNIVERSAL,
            type : asn1.Type.SEQUENCE,
            constructed : true,
            value : [{
              name : "rsapss.maskGenAlgorithm.AlgorithmIdentifier.params.algorithm",
              tagClass : asn1.Class.UNIVERSAL,
              type : asn1.Type.OID,
              constructed : false,
              capture : "maskGenHashOid"
            }]
          }]
        }]
      }, {
        name : "rsapss.saltLength",
        tagClass : asn1.Class.CONTEXT_SPECIFIC,
        type : 2,
        optional : true,
        value : [{
          name : "rsapss.saltLength.saltLength",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Class.INTEGER,
          constructed : false,
          capture : "saltLength"
        }]
      }, {
        name : "rsapss.trailerField",
        tagClass : asn1.Class.CONTEXT_SPECIFIC,
        type : 3,
        optional : true,
        value : [{
          name : "rsapss.trailer.trailer",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Class.INTEGER,
          constructed : false,
          capture : "trailer"
        }]
      }]
    };
    var certificationRequestInfoValidator = {
      name : "CertificationRequestInfo",
      tagClass : asn1.Class.UNIVERSAL,
      type : asn1.Type.SEQUENCE,
      constructed : true,
      captureAsn1 : "certificationRequestInfo",
      value : [{
        name : "CertificationRequestInfo.integer",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.INTEGER,
        constructed : false,
        capture : "certificationRequestInfoVersion"
      }, {
        name : "CertificationRequestInfo.subject",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.SEQUENCE,
        constructed : true,
        captureAsn1 : "certificationRequestInfoSubject"
      }, publicKeyValidator, {
        name : "CertificationRequestInfo.attributes",
        tagClass : asn1.Class.CONTEXT_SPECIFIC,
        type : 0,
        constructed : true,
        optional : true,
        capture : "certificationRequestInfoAttributes",
        value : [{
          name : "CertificationRequestInfo.attributes",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.SEQUENCE,
          constructed : true,
          value : [{
            name : "CertificationRequestInfo.attributes.type",
            tagClass : asn1.Class.UNIVERSAL,
            type : asn1.Type.OID,
            constructed : false
          }, {
            name : "CertificationRequestInfo.attributes.value",
            tagClass : asn1.Class.UNIVERSAL,
            type : asn1.Type.SET,
            constructed : true
          }]
        }]
      }]
    };
    var certificationRequestValidator = {
      name : "CertificationRequest",
      tagClass : asn1.Class.UNIVERSAL,
      type : asn1.Type.SEQUENCE,
      constructed : true,
      captureAsn1 : "csr",
      value : [certificationRequestInfoValidator, {
        name : "CertificationRequest.signatureAlgorithm",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.SEQUENCE,
        constructed : true,
        value : [{
          name : "CertificationRequest.signatureAlgorithm.algorithm",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.OID,
          constructed : false,
          capture : "csrSignatureOid"
        }, {
          name : "CertificationRequest.signatureAlgorithm.parameters",
          tagClass : asn1.Class.UNIVERSAL,
          optional : true,
          captureAsn1 : "csrSignatureParams"
        }]
      }, {
        name : "CertificationRequest.signature",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.BITSTRING,
        constructed : false,
        captureBitStringValue : "csrSignature"
      }]
    };
    /**
     * @param {!Object} rdn
     * @param {?} md
     * @return {?}
     */
    pki.RDNAttributesAsArray = function(rdn, md) {
      var set;
      var nabe;
      var obj;
      /** @type {!Array} */
      var rval = [];
      /** @type {number} */
      var si = 0;
      for (; si < rdn.value.length; ++si) {
        set = rdn.value[si];
        /** @type {number} */
        var i = 0;
        for (; i < set.value.length; ++i) {
          obj = {};
          nabe = set.value[i];
          obj.type = asn1.derToOid(nabe.value[0].value);
          obj.value = nabe.value[1].value;
          obj.valueTagClass = nabe.value[1].type;
          if (obj.type in oids) {
            obj.name = oids[obj.type];
            if (obj.name in _shortNames) {
              obj.shortName = _shortNames[obj.name];
            }
          }
          if (md) {
            md.update(obj.type);
            md.update(obj.value);
          }
          rval.push(obj);
        }
      }
      return rval;
    };
    /**
     * @param {!NodeList} attributes
     * @return {?}
     */
    pki.CRIAttributesAsArray = function(attributes) {
      /** @type {!Array} */
      var rval = [];
      /** @type {number} */
      var si = 0;
      for (; si < attributes.length; ++si) {
        var seq = attributes[si];
        var tile_name = asn1.derToOid(seq.value[0].value);
        var x = seq.value[1].value;
        /** @type {number} */
        var i = 0;
        for (; i < x.length; ++i) {
          var obj = {};
          if (obj.type = tile_name, obj.value = x[i].value, obj.valueTagClass = x[i].type, obj.type in oids && (obj.name = oids[obj.type], obj.name in _shortNames && (obj.shortName = _shortNames[obj.name])), obj.type === oids.extensionRequest) {
            /** @type {!Array} */
            obj.extensions = [];
            /** @type {number} */
            var ei = 0;
            for (; ei < obj.value.length; ++ei) {
              obj.extensions.push(pki.certificateExtensionFromAsn1(obj.value[ei]));
            }
          }
          rval.push(obj);
        }
      }
      return rval;
    };
    /**
     * @param {?} oid
     * @param {string} obj
     * @param {boolean} fillDefaults
     * @return {?}
     */
    var _readSignatureParameters = function(oid, obj, fillDefaults) {
      var params = {};
      if (oid !== oids["RSASSA-PSS"]) {
        return params;
      }
      if (fillDefaults) {
        params = {
          hash : {
            algorithmOid : oids.sha1
          },
          mgf : {
            algorithmOid : oids.mgf1,
            hash : {
              algorithmOid : oids.sha1
            }
          },
          saltLength : 20
        };
      }
      var capture = {};
      /** @type {!Array} */
      var errors = [];
      if (!asn1.validate(obj, rsaPrivateKeyValidator, capture, errors)) {
        /** @type {!Error} */
        var error = new Error("Cannot read RSASSA-PSS parameter block.");
        throw error.errors = errors, error;
      }
      return void 0 !== capture.hashOid && (params.hash = params.hash || {}, params.hash.algorithmOid = asn1.derToOid(capture.hashOid)), void 0 !== capture.maskGenOid && (params.mgf = params.mgf || {}, params.mgf.algorithmOid = asn1.derToOid(capture.maskGenOid), params.mgf.hash = params.mgf.hash || {}, params.mgf.hash.algorithmOid = asn1.derToOid(capture.maskGenHashOid)), void 0 !== capture.saltLength && (params.saltLength = capture.saltLength.charCodeAt(0)), params;
    };
    /**
     * @param {(Object|string)} key
     * @param {boolean} computeHash
     * @param {!Object} strict
     * @return {?}
     */
    pki.certificateFromPem = function(key, computeHash, strict) {
      var msg = forge.pem.decode(key)[0];
      if ("CERTIFICATE" !== msg.type && "X509 CERTIFICATE" !== msg.type && "TRUSTED CERTIFICATE" !== msg.type) {
        /** @type {!Error} */
        var error = new Error('Could not convert certificate from PEM; PEM header type is not "CERTIFICATE", "X509 CERTIFICATE", or "TRUSTED CERTIFICATE".');
        throw error.headerType = msg.type, error;
      }
      if (msg.procType && "ENCRYPTED" === msg.procType.type) {
        throw new Error("Could not convert certificate from PEM; PEM is encrypted.");
      }
      var obj = asn1.fromDer(msg.body, strict);
      return pki.certificateFromAsn1(obj, computeHash);
    };
    /**
     * @param {!Object} cert
     * @param {number} maxline
     * @return {?}
     */
    pki.certificateToPem = function(cert, maxline) {
      var r = {
        type : "CERTIFICATE",
        body : asn1.toDer(pki.certificateToAsn1(cert)).getBytes()
      };
      return forge.pem.encode(r, {
        maxline : maxline
      });
    };
    /**
     * @param {?} e
     * @return {?}
     */
    pki.publicKeyFromPem = function(e) {
      var msg = forge.pem.decode(e)[0];
      if ("PUBLIC KEY" !== msg.type && "RSA PUBLIC KEY" !== msg.type) {
        /** @type {!Error} */
        var error = new Error('Could not convert public key from PEM; PEM header type is not "PUBLIC KEY" or "RSA PUBLIC KEY".');
        throw error.headerType = msg.type, error;
      }
      if (msg.procType && "ENCRYPTED" === msg.procType.type) {
        throw new Error("Could not convert public key from PEM; PEM is encrypted.");
      }
      var obj = asn1.fromDer(msg.body);
      return pki.publicKeyFromAsn1(obj);
    };
    /**
     * @param {?} key
     * @param {number} maxline
     * @return {?}
     */
    pki.publicKeyToPem = function(key, maxline) {
      var r = {
        type : "PUBLIC KEY",
        body : asn1.toDer(pki.publicKeyToAsn1(key)).getBytes()
      };
      return forge.pem.encode(r, {
        maxline : maxline
      });
    };
    /**
     * @param {!Object} key
     * @param {?} maxline
     * @return {?}
     */
    pki.publicKeyToRSAPublicKeyPem = function(key, maxline) {
      var r = {
        type : "RSA PUBLIC KEY",
        body : asn1.toDer(pki.publicKeyToRSAPublicKey(key)).getBytes()
      };
      return forge.pem.encode(r, {
        maxline : maxline
      });
    };
    /**
     * @param {!Object} key
     * @param {!Object} options
     * @return {?}
     */
    pki.getPublicKeyFingerprint = function(key, options) {
      options = options || {};
      var r;
      var hash = options.md || forge.md.sha1.create();
      var type = options.type || "RSAPublicKey";
      switch(type) {
        case "RSAPublicKey":
          r = asn1.toDer(pki.publicKeyToRSAPublicKey(key)).getBytes();
          break;
        case "SubjectPublicKeyInfo":
          r = asn1.toDer(pki.publicKeyToAsn1(key)).getBytes();
          break;
        default:
          throw new Error('Unknown fingerprint type "' + options.type + '".');
      }
      hash.start();
      hash.update(r);
      var digest = hash.digest();
      if ("hex" === options.encoding) {
        var s = digest.toHex();
        return options.delimiter ? s.match(/.{2}/g).join(options.delimiter) : s;
      }
      if ("binary" === options.encoding) {
        return digest.getBytes();
      }
      if (options.encoding) {
        throw new Error('Unknown encoding "' + options.encoding + '".');
      }
      return digest;
    };
    /**
     * @param {?} e
     * @param {?} computeHash
     * @param {!Object} strict
     * @return {?}
     */
    pki.certificationRequestFromPem = function(e, computeHash, strict) {
      var msg = forge.pem.decode(e)[0];
      if ("CERTIFICATE REQUEST" !== msg.type) {
        /** @type {!Error} */
        var error = new Error('Could not convert certification request from PEM; PEM header type is not "CERTIFICATE REQUEST".');
        throw error.headerType = msg.type, error;
      }
      if (msg.procType && "ENCRYPTED" === msg.procType.type) {
        throw new Error("Could not convert certification request from PEM; PEM is encrypted.");
      }
      var obj = asn1.fromDer(msg.body, strict);
      return pki.certificationRequestFromAsn1(obj, computeHash);
    };
    /**
     * @param {!Object} csr
     * @param {number} maxline
     * @return {?}
     */
    pki.certificationRequestToPem = function(csr, maxline) {
      var r = {
        type : "CERTIFICATE REQUEST",
        body : asn1.toDer(pki.certificationRequestToAsn1(csr)).getBytes()
      };
      return forge.pem.encode(r, {
        maxline : maxline
      });
    };
    /**
     * @return {?}
     */
    pki.createCertificate = function() {
      var cert = {};
      return cert.version = 2, cert.serialNumber = "00", cert.signatureOid = null, cert.signature = null, cert.siginfo = {}, cert.siginfo.algorithmOid = null, cert.validity = {}, cert.validity.notBefore = new Date, cert.validity.notAfter = new Date, cert.issuer = {}, cert.issuer.getField = function(sn) {
        return _getAttribute(cert.issuer, sn);
      }, cert.issuer.addField = function(attr) {
        _fillMissingFields([attr]);
        cert.issuer.attributes.push(attr);
      }, cert.issuer.attributes = [], cert.issuer.hash = null, cert.subject = {}, cert.subject.getField = function(sn) {
        return _getAttribute(cert.subject, sn);
      }, cert.subject.addField = function(attr) {
        _fillMissingFields([attr]);
        cert.subject.attributes.push(attr);
      }, cert.subject.attributes = [], cert.subject.hash = null, cert.extensions = [], cert.publicKey = null, cert.md = null, cert.setSubject = function(attrs, uniqueId) {
        _fillMissingFields(attrs);
        /** @type {!Object} */
        cert.subject.attributes = attrs;
        delete cert.subject.uniqueId;
        if (uniqueId) {
          /** @type {string} */
          cert.subject.uniqueId = uniqueId;
        }
        /** @type {null} */
        cert.subject.hash = null;
      }, cert.setIssuer = function(attrs, uniqueId) {
        _fillMissingFields(attrs);
        /** @type {!Object} */
        cert.issuer.attributes = attrs;
        delete cert.issuer.uniqueId;
        if (uniqueId) {
          /** @type {string} */
          cert.issuer.uniqueId = uniqueId;
        }
        /** @type {null} */
        cert.issuer.hash = null;
      }, cert.setExtensions = function(exts) {
        /** @type {number} */
        var i = 0;
        for (; i < exts.length; ++i) {
          _fillMissingExtensionFields(exts[i], {
            cert : cert
          });
        }
        /** @type {!Object} */
        cert.extensions = exts;
      }, cert.getExtension = function(options) {
        if ("string" == typeof options) {
          options = {
            name : options
          };
        }
        var ext;
        /** @type {null} */
        var rval = null;
        /** @type {number} */
        var i = 0;
        for (; null === rval && i < cert.extensions.length; ++i) {
          ext = cert.extensions[i];
          if (options.id && ext.id === options.id) {
            rval = ext;
          } else {
            if (options.name && ext.name === options.name) {
              rval = ext;
            }
          }
        }
        return rval;
      }, cert.sign = function(key, md) {
        cert.md = md || forge.md.sha1.create();
        var algorithmOid = oids[cert.md.algorithm + "WithRSAEncryption"];
        if (!algorithmOid) {
          /** @type {!Error} */
          var error = new Error("Could not compute certificate digest. Unknown message digest algorithm OID.");
          throw error.algorithm = cert.md.algorithm, error;
        }
        cert.signatureOid = cert.siginfo.algorithmOid = algorithmOid;
        cert.tbsCertificate = pki.getTBSCertificate(cert);
        var cidToGidStream = asn1.toDer(cert.tbsCertificate);
        cert.md.update(cidToGidStream.getBytes());
        cert.signature = key.sign(cert.md);
      }, cert.verify = function(child) {
        /** @type {boolean} */
        var messages = false;
        if (!cert.issued(child)) {
          var issuer = child.issuer;
          var subject = cert.subject;
          /** @type {!Error} */
          var error = new Error("The parent certificate did not issue the given child certificate; the child certificate's issuer does not match the parent's subject.");
          throw error.expectedIssuer = issuer.attributes, error.actualIssuer = subject.attributes, error;
        }
        var md = child.md;
        if (null === md) {
          if (child.signatureOid in oids) {
            var oid = oids[child.signatureOid];
            switch(oid) {
              case "sha1WithRSAEncryption":
                md = forge.md.sha1.create();
                break;
              case "md5WithRSAEncryption":
                md = forge.md.md5.create();
                break;
              case "sha256WithRSAEncryption":
                md = forge.md.sha256.create();
                break;
              case "sha512WithRSAEncryption":
                md = forge.md.sha512.create();
                break;
              case "RSASSA-PSS":
                md = forge.md.sha256.create();
            }
          }
          if (null === md) {
            /** @type {!Error} */
            error = new Error("Could not compute certificate digest. Unknown signature OID.");
            throw error.signatureOid = child.signatureOid, error;
          }
          var value = child.tbsCertificate || pki.getTBSCertificate(child);
          var bytes = asn1.toDer(value);
          md.update(bytes.getBytes());
        }
        if (null !== md) {
          var message;
          switch(child.signatureOid) {
            case oids.sha1WithRSAEncryption:
              message = void 0;
              break;
            case oids["RSASSA-PSS"]:
              var hash;
              var name;
              if (hash = oids[child.signatureParameters.mgf.hash.algorithmOid], void 0 === hash || void 0 === forge.md[hash]) {
                /** @type {!Error} */
                error = new Error("Unsupported MGF hash function.");
                throw error.oid = child.signatureParameters.mgf.hash.algorithmOid, error.name = hash, error;
              }
              if (name = oids[child.signatureParameters.mgf.algorithmOid], void 0 === name || void 0 === forge.mgf[name]) {
                /** @type {!Error} */
                error = new Error("Unsupported MGF function.");
                throw error.oid = child.signatureParameters.mgf.algorithmOid, error.name = name, error;
              }
              if (name = forge.mgf[name].create(forge.md[hash].create()), hash = oids[child.signatureParameters.hash.algorithmOid], void 0 === hash || void 0 === forge.md[hash]) {
                throw {
                  message : "Unsupported RSASSA-PSS hash function.",
                  oid : child.signatureParameters.hash.algorithmOid,
                  name : hash
                };
              }
              message = forge.pss.create(forge.md[hash].create(), name, child.signatureParameters.saltLength);
          }
          messages = cert.publicKey.verify(md.digest().getBytes(), child.signature, message);
        }
        return messages;
      }, cert.isIssuer = function(parent) {
        /** @type {boolean} */
        var allFound = false;
        var i = cert.issuer;
        var s = parent.subject;
        if (i.hash && s.hash) {
          /** @type {boolean} */
          allFound = i.hash === s.hash;
        } else {
          if (i.attributes.length === s.attributes.length) {
            /** @type {boolean} */
            allFound = true;
            var a;
            var b;
            /** @type {number} */
            var j = 0;
            for (; allFound && j < i.attributes.length; ++j) {
              a = i.attributes[j];
              b = s.attributes[j];
              if (!(a.type === b.type && a.value === b.value)) {
                /** @type {boolean} */
                allFound = false;
              }
            }
          }
        }
        return allFound;
      }, cert.issued = function(child) {
        return child.isIssuer(cert);
      }, cert.generateSubjectKeyIdentifier = function() {
        return pki.getPublicKeyFingerprint(cert.publicKey, {
          type : "RSAPublicKey"
        });
      }, cert.verifySubjectKeyIdentifier = function() {
        var oid = oids.subjectKeyIdentifier;
        /** @type {number} */
        var i = 0;
        for (; i < cert.extensions.length; ++i) {
          var ext = cert.extensions[i];
          if (ext.id === oid) {
            var n = cert.generateSubjectKeyIdentifier().getBytes();
            return forge.util.hexToBytes(ext.subjectKeyIdentifier) === n;
          }
        }
        return false;
      }, cert;
    };
    /**
     * @param {string} obj
     * @param {boolean} signatureOnly
     * @return {?}
     */
    pki.certificateFromAsn1 = function(obj, signatureOnly) {
      var capture = {};
      /** @type {!Array} */
      var errors = [];
      if (!asn1.validate(obj, x509CertificateValidator, capture, errors)) {
        /** @type {!Error} */
        var error = new Error("Cannot read X.509 certificate. ASN.1 object is not an X509v3 Certificate.");
        throw error.errors = errors, error;
      }
      var oid = asn1.derToOid(capture.publicKeyOid);
      if (oid !== pki.oids.rsaEncryption) {
        throw new Error("Cannot read public key. OID is not RSA.");
      }
      var cert = pki.createCertificate();
      cert.version = capture.certVersion ? capture.certVersion.charCodeAt(0) : 0;
      var virtualBorderColor = forge.util.createBuffer(capture.certSerialNumber);
      cert.serialNumber = virtualBorderColor.toHex();
      cert.signatureOid = forge.asn1.derToOid(capture.certSignatureOid);
      cert.signatureParameters = _readSignatureParameters(cert.signatureOid, capture.certSignatureParams, true);
      cert.siginfo.algorithmOid = forge.asn1.derToOid(capture.certinfoSignatureOid);
      cert.siginfo.parameters = _readSignatureParameters(cert.siginfo.algorithmOid, capture.certinfoSignatureParams, false);
      cert.signature = capture.certSignature;
      /** @type {!Array} */
      var validity = [];
      if (void 0 !== capture.certValidity1UTCTime && validity.push(asn1.utcTimeToDate(capture.certValidity1UTCTime)), void 0 !== capture.certValidity2GeneralizedTime && validity.push(asn1.generalizedTimeToDate(capture.certValidity2GeneralizedTime)), void 0 !== capture.certValidity3UTCTime && validity.push(asn1.utcTimeToDate(capture.certValidity3UTCTime)), void 0 !== capture.certValidity4GeneralizedTime && validity.push(asn1.generalizedTimeToDate(capture.certValidity4GeneralizedTime)), validity.length > 
      2) {
        throw new Error("Cannot read notBefore/notAfter validity times; more than two times were provided in the certificate.");
      }
      if (validity.length < 2) {
        throw new Error("Cannot read notBefore/notAfter validity times; they were not provided as either UTCTime or GeneralizedTime.");
      }
      if (cert.validity.notBefore = validity[0], cert.validity.notAfter = validity[1], cert.tbsCertificate = capture.tbsCertificate, signatureOnly) {
        if (cert.md = null, cert.signatureOid in oids) {
          oid = oids[cert.signatureOid];
          switch(oid) {
            case "sha1WithRSAEncryption":
              cert.md = forge.md.sha1.create();
              break;
            case "md5WithRSAEncryption":
              cert.md = forge.md.md5.create();
              break;
            case "sha256WithRSAEncryption":
              cert.md = forge.md.sha256.create();
              break;
            case "sha512WithRSAEncryption":
              cert.md = forge.md.sha512.create();
              break;
            case "RSASSA-PSS":
              cert.md = forge.md.sha256.create();
          }
        }
        if (null === cert.md) {
          /** @type {!Error} */
          error = new Error("Could not compute certificate digest. Unknown signature OID.");
          throw error.signatureOid = cert.signatureOid, error;
        }
        var cidToGidStream = asn1.toDer(cert.tbsCertificate);
        cert.md.update(cidToGidStream.getBytes());
      }
      var imd = forge.md.sha1.create();
      /**
       * @param {!Object} sn
       * @return {?}
       */
      cert.issuer.getField = function(sn) {
        return _getAttribute(cert.issuer, sn);
      };
      /**
       * @param {?} attr
       * @return {undefined}
       */
      cert.issuer.addField = function(attr) {
        _fillMissingFields([attr]);
        cert.issuer.attributes.push(attr);
      };
      cert.issuer.attributes = pki.RDNAttributesAsArray(capture.certIssuer, imd);
      if (capture.certIssuerUniqueId) {
        cert.issuer.uniqueId = capture.certIssuerUniqueId;
      }
      cert.issuer.hash = imd.digest().toHex();
      var smd = forge.md.sha1.create();
      return cert.subject.getField = function(sn) {
        return _getAttribute(cert.subject, sn);
      }, cert.subject.addField = function(attr) {
        _fillMissingFields([attr]);
        cert.subject.attributes.push(attr);
      }, cert.subject.attributes = pki.RDNAttributesAsArray(capture.certSubject, smd), capture.certSubjectUniqueId && (cert.subject.uniqueId = capture.certSubjectUniqueId), cert.subject.hash = smd.digest().toHex(), capture.certExtensions ? cert.extensions = pki.certificateExtensionsFromAsn1(capture.certExtensions) : cert.extensions = [], cert.publicKey = pki.publicKeyFromAsn1(capture.subjectPublicKeyInfo), cert;
    };
    /**
     * @param {!Object} exts
     * @return {?}
     */
    pki.certificateExtensionsFromAsn1 = function(exts) {
      /** @type {!Array} */
      var rval = [];
      /** @type {number} */
      var i = 0;
      for (; i < exts.value.length; ++i) {
        var extseq = exts.value[i];
        /** @type {number} */
        var ei = 0;
        for (; ei < extseq.value.length; ++ei) {
          rval.push(pki.certificateExtensionFromAsn1(extseq.value[ei]));
        }
      }
      return rval;
    };
    /**
     * @param {!Object} ext
     * @return {?}
     */
    pki.certificateExtensionFromAsn1 = function(ext) {
      var e = {};
      if (e.id = asn1.derToOid(ext.value[0].value), e.critical = false, ext.value[1].type === asn1.Type.BOOLEAN ? (e.critical = 0 !== ext.value[1].value.charCodeAt(0), e.value = ext.value[2].value) : e.value = ext.value[1].value, e.id in oids) {
        if (e.name = oids[e.id], "keyUsage" === e.name) {
          var s = asn1.fromDer(e.value);
          /** @type {number} */
          var a = 0;
          /** @type {number} */
          var n = 0;
          if (s.value.length > 1) {
            a = s.value.charCodeAt(1);
            n = s.value.length > 2 ? s.value.charCodeAt(2) : 0;
          }
          /** @type {boolean} */
          e.digitalSignature = 128 === (128 & a);
          /** @type {boolean} */
          e.nonRepudiation = 64 === (64 & a);
          /** @type {boolean} */
          e.keyEncipherment = 32 === (32 & a);
          /** @type {boolean} */
          e.dataEncipherment = 16 === (16 & a);
          /** @type {boolean} */
          e.keyAgreement = 8 === (8 & a);
          /** @type {boolean} */
          e.keyCertSign = 4 === (4 & a);
          /** @type {boolean} */
          e.cRLSign = 2 === (2 & a);
          /** @type {boolean} */
          e.encipherOnly = 1 === (1 & a);
          /** @type {boolean} */
          e.decipherOnly = 128 === (128 & n);
        } else {
          if ("basicConstraints" === e.name) {
            s = asn1.fromDer(e.value);
            if (s.value.length > 0 && s.value[0].type === asn1.Type.BOOLEAN) {
              /** @type {boolean} */
              e.cA = 0 !== s.value[0].value.charCodeAt(0);
            } else {
              /** @type {boolean} */
              e.cA = false;
            }
            /** @type {null} */
            var value = null;
            if (s.value.length > 0 && s.value[0].type === asn1.Type.INTEGER) {
              value = s.value[0].value;
            } else {
              if (s.value.length > 1) {
                value = s.value[1].value;
              }
            }
            if (null !== value) {
              e.pathLenConstraint = asn1.derToInteger(value);
            }
          } else {
            if ("extKeyUsage" === e.name) {
              s = asn1.fromDer(e.value);
              /** @type {number} */
              var i = 0;
              for (; i < s.value.length; ++i) {
                var oid = asn1.derToOid(s.value[i].value);
                if (oid in oids) {
                  /** @type {boolean} */
                  e[oids[oid]] = true;
                } else {
                  /** @type {boolean} */
                  e[oid] = true;
                }
              }
            } else {
              if ("nsCertType" === e.name) {
                s = asn1.fromDer(e.value);
                /** @type {number} */
                a = 0;
                if (s.value.length > 1) {
                  a = s.value.charCodeAt(1);
                }
                /** @type {boolean} */
                e.client = 128 === (128 & a);
                /** @type {boolean} */
                e.server = 64 === (64 & a);
                /** @type {boolean} */
                e.email = 32 === (32 & a);
                /** @type {boolean} */
                e.objsign = 16 === (16 & a);
                /** @type {boolean} */
                e.reserved = 8 === (8 & a);
                /** @type {boolean} */
                e.sslCA = 4 === (4 & a);
                /** @type {boolean} */
                e.emailCA = 2 === (2 & a);
                /** @type {boolean} */
                e.objCA = 1 === (1 & a);
              } else {
                if ("subjectAltName" === e.name || "issuerAltName" === e.name) {
                  /** @type {!Array} */
                  e.altNames = [];
                  var gn;
                  s = asn1.fromDer(e.value);
                  /** @type {number} */
                  var n = 0;
                  for (; n < s.value.length; ++n) {
                    gn = s.value[n];
                    var result = {
                      type : gn.type,
                      value : gn.value
                    };
                    switch(e.altNames.push(result), gn.type) {
                      case 1:
                      case 2:
                      case 6:
                        break;
                      case 7:
                        result.ip = forge.util.bytesToIP(gn.value);
                        break;
                      case 8:
                        result.oid = asn1.derToOid(gn.value);
                    }
                  }
                } else {
                  if ("subjectKeyIdentifier" === e.name) {
                    s = asn1.fromDer(e.value);
                    e.subjectKeyIdentifier = forge.util.bytesToHex(s.value);
                  }
                }
              }
            }
          }
        }
      }
      return e;
    };
    /**
     * @param {string} obj
     * @param {?} computeHash
     * @return {?}
     */
    pki.certificationRequestFromAsn1 = function(obj, computeHash) {
      var capture = {};
      /** @type {!Array} */
      var errors = [];
      if (!asn1.validate(obj, certificationRequestValidator, capture, errors)) {
        /** @type {!Error} */
        var error = new Error("Cannot read PKCS#10 certificate request. ASN.1 object is not a PKCS#10 CertificationRequest.");
        throw error.errors = errors, error;
      }
      var oid = asn1.derToOid(capture.publicKeyOid);
      if (oid !== pki.oids.rsaEncryption) {
        throw new Error("Cannot read public key. OID is not RSA.");
      }
      var csr = pki.createCertificationRequest();
      if (csr.version = capture.csrVersion ? capture.csrVersion.charCodeAt(0) : 0, csr.signatureOid = forge.asn1.derToOid(capture.csrSignatureOid), csr.signatureParameters = _readSignatureParameters(csr.signatureOid, capture.csrSignatureParams, true), csr.siginfo.algorithmOid = forge.asn1.derToOid(capture.csrSignatureOid), csr.siginfo.parameters = _readSignatureParameters(csr.siginfo.algorithmOid, capture.csrSignatureParams, false), csr.signature = capture.csrSignature, csr.certificationRequestInfo = 
      capture.certificationRequestInfo, computeHash) {
        if (csr.md = null, csr.signatureOid in oids) {
          oid = oids[csr.signatureOid];
          switch(oid) {
            case "sha1WithRSAEncryption":
              csr.md = forge.md.sha1.create();
              break;
            case "md5WithRSAEncryption":
              csr.md = forge.md.md5.create();
              break;
            case "sha256WithRSAEncryption":
              csr.md = forge.md.sha256.create();
              break;
            case "sha512WithRSAEncryption":
              csr.md = forge.md.sha512.create();
              break;
            case "RSASSA-PSS":
              csr.md = forge.md.sha256.create();
          }
        }
        if (null === csr.md) {
          /** @type {!Error} */
          error = new Error("Could not compute certification request digest. Unknown signature OID.");
          throw error.signatureOid = csr.signatureOid, error;
        }
        var cidToGidStream = asn1.toDer(csr.certificationRequestInfo);
        csr.md.update(cidToGidStream.getBytes());
      }
      var imd = forge.md.sha1.create();
      return csr.subject.getField = function(sn) {
        return _getAttribute(csr.subject, sn);
      }, csr.subject.addField = function(attr) {
        _fillMissingFields([attr]);
        csr.subject.attributes.push(attr);
      }, csr.subject.attributes = pki.RDNAttributesAsArray(capture.certificationRequestInfoSubject, imd), csr.subject.hash = imd.digest().toHex(), csr.publicKey = pki.publicKeyFromAsn1(capture.subjectPublicKeyInfo), csr.getAttribute = function(sn) {
        return _getAttribute(csr, sn);
      }, csr.addAttribute = function(attr) {
        _fillMissingFields([attr]);
        csr.attributes.push(attr);
      }, csr.attributes = pki.CRIAttributesAsArray(capture.certificationRequestInfoAttributes || []), csr;
    };
    /**
     * @return {?}
     */
    pki.createCertificationRequest = function() {
      var csr = {};
      return csr.version = 0, csr.signatureOid = null, csr.signature = null, csr.siginfo = {}, csr.siginfo.algorithmOid = null, csr.subject = {}, csr.subject.getField = function(sn) {
        return _getAttribute(csr.subject, sn);
      }, csr.subject.addField = function(attr) {
        _fillMissingFields([attr]);
        csr.subject.attributes.push(attr);
      }, csr.subject.attributes = [], csr.subject.hash = null, csr.publicKey = null, csr.attributes = [], csr.getAttribute = function(sn) {
        return _getAttribute(csr, sn);
      }, csr.addAttribute = function(attr) {
        _fillMissingFields([attr]);
        csr.attributes.push(attr);
      }, csr.md = null, csr.setSubject = function(attrs) {
        _fillMissingFields(attrs);
        /** @type {!Object} */
        csr.subject.attributes = attrs;
        /** @type {null} */
        csr.subject.hash = null;
      }, csr.setAttributes = function(attrs) {
        _fillMissingFields(attrs);
        /** @type {!Object} */
        csr.attributes = attrs;
      }, csr.sign = function(key, md) {
        csr.md = md || forge.md.sha1.create();
        var algorithmOid = oids[csr.md.algorithm + "WithRSAEncryption"];
        if (!algorithmOid) {
          /** @type {!Error} */
          var error = new Error("Could not compute certification request digest. Unknown message digest algorithm OID.");
          throw error.algorithm = csr.md.algorithm, error;
        }
        csr.signatureOid = csr.siginfo.algorithmOid = algorithmOid;
        csr.certificationRequestInfo = pki.getCertificationRequestInfo(csr);
        var cidToGidStream = asn1.toDer(csr.certificationRequestInfo);
        csr.md.update(cidToGidStream.getBytes());
        csr.signature = key.sign(csr.md);
      }, csr.verify = function() {
        /** @type {boolean} */
        var messages = false;
        var md = csr.md;
        if (null === md) {
          if (csr.signatureOid in oids) {
            var oid = oids[csr.signatureOid];
            switch(oid) {
              case "sha1WithRSAEncryption":
                md = forge.md.sha1.create();
                break;
              case "md5WithRSAEncryption":
                md = forge.md.md5.create();
                break;
              case "sha256WithRSAEncryption":
                md = forge.md.sha256.create();
                break;
              case "sha512WithRSAEncryption":
                md = forge.md.sha512.create();
                break;
              case "RSASSA-PSS":
                md = forge.md.sha256.create();
            }
          }
          if (null === md) {
            /** @type {!Error} */
            var error = new Error("Could not compute certification request digest. Unknown signature OID.");
            throw error.signatureOid = csr.signatureOid, error;
          }
          var value = csr.certificationRequestInfo || pki.getCertificationRequestInfo(csr);
          var bytes = asn1.toDer(value);
          md.update(bytes.getBytes());
        }
        if (null !== md) {
          var message;
          switch(csr.signatureOid) {
            case oids.sha1WithRSAEncryption:
              break;
            case oids["RSASSA-PSS"]:
              var hash;
              var name;
              if (hash = oids[csr.signatureParameters.mgf.hash.algorithmOid], void 0 === hash || void 0 === forge.md[hash]) {
                /** @type {!Error} */
                error = new Error("Unsupported MGF hash function.");
                throw error.oid = csr.signatureParameters.mgf.hash.algorithmOid, error.name = hash, error;
              }
              if (name = oids[csr.signatureParameters.mgf.algorithmOid], void 0 === name || void 0 === forge.mgf[name]) {
                /** @type {!Error} */
                error = new Error("Unsupported MGF function.");
                throw error.oid = csr.signatureParameters.mgf.algorithmOid, error.name = name, error;
              }
              if (name = forge.mgf[name].create(forge.md[hash].create()), hash = oids[csr.signatureParameters.hash.algorithmOid], void 0 === hash || void 0 === forge.md[hash]) {
                /** @type {!Error} */
                error = new Error("Unsupported RSASSA-PSS hash function.");
                throw error.oid = csr.signatureParameters.hash.algorithmOid, error.name = hash, error;
              }
              message = forge.pss.create(forge.md[hash].create(), name, csr.signatureParameters.saltLength);
          }
          messages = csr.publicKey.verify(md.digest().getBytes(), csr.signature, message);
        }
        return messages;
      }, csr;
    };
    /**
     * @param {!Object} cert
     * @return {?}
     */
    pki.getTBSCertificate = function(cert) {
      var t = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.CONTEXT_SPECIFIC, 0, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, asn1.integerToDer(cert.version).getBytes())]), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, forge.util.hexToBytes(cert.serialNumber)), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(cert.siginfo.algorithmOid).getBytes()), 
      _signatureParametersToAsn1(cert.siginfo.algorithmOid, cert.siginfo.parameters)]), _dnToAsn1(cert.issuer), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.UTCTIME, false, asn1.dateToUtcTime(cert.validity.notBefore)), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.UTCTIME, false, asn1.dateToUtcTime(cert.validity.notAfter))]), _dnToAsn1(cert.subject), pki.publicKeyToAsn1(cert.publicKey)]);
      return cert.issuer.uniqueId && t.value.push(asn1.create(asn1.Class.CONTEXT_SPECIFIC, 1, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.BITSTRING, false, String.fromCharCode(0) + cert.issuer.uniqueId)])), cert.subject.uniqueId && t.value.push(asn1.create(asn1.Class.CONTEXT_SPECIFIC, 2, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.BITSTRING, false, String.fromCharCode(0) + cert.subject.uniqueId)])), cert.extensions.length > 0 && t.value.push(pki.certificateExtensionsToAsn1(cert.extensions)), 
      t;
    };
    /**
     * @param {!Object} csr
     * @return {?}
     */
    pki.getCertificationRequestInfo = function(csr) {
      var cri = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, asn1.integerToDer(csr.version).getBytes()), _dnToAsn1(csr.subject), pki.publicKeyToAsn1(csr.publicKey), _CRIAttributesToAsn1(csr)]);
      return cri;
    };
    /**
     * @param {!Object} subject
     * @return {?}
     */
    pki.distinguishedNameToAsn1 = function(subject) {
      return _dnToAsn1(subject);
    };
    /**
     * @param {!Object} cert
     * @return {?}
     */
    pki.certificateToAsn1 = function(cert) {
      var tbsCertificate = cert.tbsCertificate || pki.getTBSCertificate(cert);
      return asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [tbsCertificate, asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(cert.signatureOid).getBytes()), _signatureParametersToAsn1(cert.signatureOid, cert.signatureParameters)]), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.BITSTRING, false, String.fromCharCode(0) + cert.signature)]);
    };
    /**
     * @param {!NodeList} exts
     * @return {?}
     */
    pki.certificateExtensionsToAsn1 = function(exts) {
      var rval = asn1.create(asn1.Class.CONTEXT_SPECIFIC, 3, true, []);
      var r = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, []);
      rval.value.push(r);
      /** @type {number} */
      var i = 0;
      for (; i < exts.length; ++i) {
        r.value.push(pki.certificateExtensionToAsn1(exts[i]));
      }
      return rval;
    };
    /**
     * @param {!Object} ext
     * @return {?}
     */
    pki.certificateExtensionToAsn1 = function(ext) {
      var t = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, []);
      t.value.push(asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(ext.id).getBytes()));
      if (ext.critical) {
        t.value.push(asn1.create(asn1.Class.UNIVERSAL, asn1.Type.BOOLEAN, false, String.fromCharCode(255)));
      }
      var value = ext.value;
      return "string" != typeof ext.value && (value = asn1.toDer(value).getBytes()), t.value.push(asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OCTETSTRING, false, value)), t;
    };
    /**
     * @param {!Object} csr
     * @return {?}
     */
    pki.certificationRequestToAsn1 = function(csr) {
      var tbsCertificate = csr.certificationRequestInfo || pki.getCertificationRequestInfo(csr);
      return asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [tbsCertificate, asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(csr.signatureOid).getBytes()), _signatureParametersToAsn1(csr.signatureOid, csr.signatureParameters)]), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.BITSTRING, false, String.fromCharCode(0) + csr.signature)]);
    };
    /**
     * @param {!NodeList} certs
     * @return {?}
     */
    pki.createCaStore = function(certs) {
      /**
       * @param {!Object} subject
       * @return {?}
       */
      function getBySubject(subject) {
        return ensureSubjectHasHash(subject), caStore.certs[subject.hash] || null;
      }
      /**
       * @param {!Object} subject
       * @return {undefined}
       */
      function ensureSubjectHasHash(subject) {
        if (!subject.hash) {
          var md = forge.md.sha1.create();
          subject.attributes = pki.RDNAttributesAsArray(_dnToAsn1(subject), md);
          subject.hash = md.digest().toHex();
        }
      }
      var caStore = {
        certs : {}
      };
      if (caStore.getIssuer = function(cert) {
        var rval = getBySubject(cert.issuer);
        return rval;
      }, caStore.addCertificate = function(cert) {
        if ("string" == typeof cert && (cert = forge.pki.certificateFromPem(cert)), ensureSubjectHasHash(cert.subject), !caStore.hasCertificate(cert)) {
          if (cert.subject.hash in caStore.certs) {
            var chain = caStore.certs[cert.subject.hash];
            if (!forge.util.isArray(chain)) {
              /** @type {!Array} */
              chain = [chain];
            }
            chain.push(cert);
            caStore.certs[cert.subject.hash] = chain;
          } else {
            /** @type {!Object} */
            caStore.certs[cert.subject.hash] = cert;
          }
        }
      }, caStore.hasCertificate = function(cert) {
        if ("string" == typeof cert) {
          cert = forge.pki.certificateFromPem(cert);
        }
        var match = getBySubject(cert.subject);
        if (!match) {
          return false;
        }
        if (!forge.util.isArray(match)) {
          /** @type {!Array} */
          match = [match];
        }
        var a = asn1.toDer(pki.certificateToAsn1(cert)).getBytes();
        /** @type {number} */
        var i = 0;
        for (; i < match.length; ++i) {
          var __add_node = asn1.toDer(pki.certificateToAsn1(match[i])).getBytes();
          if (a === __add_node) {
            return true;
          }
        }
        return false;
      }, caStore.listAllCertificates = function() {
        /** @type {!Array} */
        var all = [];
        var hash;
        for (hash in caStore.certs) {
          if (caStore.certs.hasOwnProperty(hash)) {
            var arr = caStore.certs[hash];
            if (forge.util.isArray(arr)) {
              /** @type {number} */
              var i = 0;
              for (; i < arr.length; ++i) {
                all.push(arr[i]);
              }
            } else {
              all.push(arr);
            }
          }
        }
        return all;
      }, caStore.removeCertificate = function(cert) {
        var n;
        if ("string" == typeof cert && (cert = forge.pki.certificateFromPem(cert)), ensureSubjectHasHash(cert.subject), !caStore.hasCertificate(cert)) {
          return null;
        }
        var match = getBySubject(cert.subject);
        if (!forge.util.isArray(match)) {
          return n = caStore.certs[cert.subject.hash], delete caStore.certs[cert.subject.hash], n;
        }
        var s = asn1.toDer(pki.certificateToAsn1(cert)).getBytes();
        /** @type {number} */
        var i = 0;
        for (; i < match.length; ++i) {
          var ATTR_EQ = asn1.toDer(pki.certificateToAsn1(match[i])).getBytes();
          if (s === ATTR_EQ) {
            n = match[i];
            match.splice(i, 1);
          }
        }
        return 0 === match.length && delete caStore.certs[cert.subject.hash], n;
      }, certs) {
        /** @type {number} */
        var i = 0;
        for (; i < certs.length; ++i) {
          var cert = certs[i];
          caStore.addCertificate(cert);
        }
      }
      return caStore;
    };
    pki.certificateError = {
      bad_certificate : "forge.pki.BadCertificate",
      unsupported_certificate : "forge.pki.UnsupportedCertificate",
      certificate_revoked : "forge.pki.CertificateRevoked",
      certificate_expired : "forge.pki.CertificateExpired",
      certificate_unknown : "forge.pki.CertificateUnknown",
      unknown_ca : "forge.pki.UnknownCertificateAuthority"
    };
    /**
     * @param {!Object} caStore
     * @param {!Array} chain
     * @param {!Function} verify
     * @return {?}
     */
    pki.verifyCertificateChain = function(caStore, chain, verify) {
      chain = chain.slice(0);
      var certs = chain.slice(0);
      /** @type {!Date} */
      var now = new Date;
      /** @type {boolean} */
      var i = true;
      /** @type {null} */
      var error = null;
      /** @type {number} */
      var depth = 0;
      do {
        var cert = chain.shift();
        /** @type {null} */
        var parent = null;
        /** @type {boolean} */
        var node = false;
        if ((now < cert.validity.notBefore || now > cert.validity.notAfter) && (error = {
          message : "Certificate is not valid yet or has expired.",
          error : pki.certificateError.certificate_expired,
          notBefore : cert.validity.notBefore,
          notAfter : cert.validity.notAfter,
          now : now
        }), null === error) {
          if (parent = chain[0] || caStore.getIssuer(cert), null === parent && cert.isIssuer(cert) && (node = true, parent = cert), parent) {
            var p = parent;
            if (!forge.util.isArray(p)) {
              /** @type {!Array} */
              p = [p];
            }
            /** @type {boolean} */
            var key = false;
            for (; !key && p.length > 0;) {
              parent = p.shift();
              try {
                key = parent.verify(cert);
              } catch (e) {
              }
            }
            if (!key) {
              error = {
                message : "Certificate signature is invalid.",
                error : pki.certificateError.bad_certificate
              };
            }
          }
          if (!(null !== error || parent && !node || caStore.hasCertificate(cert))) {
            error = {
              message : "Certificate is not trusted.",
              error : pki.certificateError.unknown_ca
            };
          }
        }
        if (null === error && parent && !cert.isIssuer(parent) && (error = {
          message : "Certificate issuer is invalid.",
          error : pki.certificateError.bad_certificate
        }), null === error) {
          var se = {
            keyUsage : true,
            basicConstraints : true
          };
          /** @type {number} */
          var i = 0;
          for (; null === error && i < cert.extensions.length; ++i) {
            var ext = cert.extensions[i];
            if (!(!ext.critical || ext.name in se)) {
              error = {
                message : "Certificate has an unsupported critical extension.",
                error : pki.certificateError.unsupported_certificate
              };
            }
          }
        }
        if (null === error && (!i || 0 === chain.length && (!parent || node))) {
          var bcExt = cert.getExtension("basicConstraints");
          var keyUsageExt = cert.getExtension("keyUsage");
          if (null !== keyUsageExt && (keyUsageExt.keyCertSign && null !== bcExt || (error = {
            message : "Certificate keyUsage or basicConstraints conflict or indicate that the certificate is not a CA. If the certificate is the only one in the chain or isn't the first then the certificate must be a valid CA.",
            error : pki.certificateError.bad_certificate
          })), null !== error || null === bcExt || bcExt.cA || (error = {
            message : "Certificate basicConstraints indicates the certificate is not a CA.",
            error : pki.certificateError.bad_certificate
          }), null === error && null !== keyUsageExt && "pathLenConstraint" in bcExt) {
            /** @type {number} */
            var pathLen = depth - 1;
            if (pathLen > bcExt.pathLenConstraint) {
              error = {
                message : "Certificate basicConstraints pathLenConstraint violated.",
                error : pki.certificateError.bad_certificate
              };
            }
          }
        }
        var vfd = null === error || error.error;
        var ret = verify ? verify(vfd, depth, certs) : vfd;
        if (ret !== true) {
          throw vfd === true && (error = {
            message : "The application rejected the certificate.",
            error : pki.certificateError.bad_certificate
          }), (ret || 0 === ret) && ("object" != typeof ret || forge.util.isArray(ret) ? "string" == typeof ret && (error.error = ret) : (ret.message && (error.message = ret.message), ret.error && (error.error = ret.error))), error;
        }
        /** @type {null} */
        error = null;
        /** @type {boolean} */
        i = false;
        ++depth;
      } while (chain.length > 0);
      return true;
    };
  }, function(mixin, canCreateDiscussions, FORGE) {
    /**
     * @param {!Array} bytes
     * @return {?}
     */
    function transformIV(bytes) {
      if ("string" == typeof bytes && (bytes = forge.util.createBuffer(bytes)), forge.util.isArray(bytes) && bytes.length > 4) {
        /** @type {!Array} */
        var valueBytes = bytes;
        bytes = forge.util.createBuffer();
        /** @type {number} */
        var i = 0;
        for (; i < valueBytes.length; ++i) {
          bytes.putByte(valueBytes[i]);
        }
      }
      return forge.util.isArray(bytes) || (bytes = [bytes.getInt32(), bytes.getInt32(), bytes.getInt32(), bytes.getInt32()]), bytes;
    }
    /**
     * @param {!Object} block
     * @return {undefined}
     */
    function inc32(block) {
      /** @type {number} */
      block[block.length - 1] = block[block.length - 1] + 1 & 4294967295;
    }
    /**
     * @param {number} num
     * @return {?}
     */
    function from64To32(num) {
      return [num / 4294967296 | 0, 4294967295 & num];
    }
    var forge = FORGE(0);
    FORGE(1);
    forge.cipher = forge.cipher || {};
    var modes = mixin.exports = forge.cipher.modes = forge.cipher.modes || {};
    /**
     * @param {!Object} options
     * @return {undefined}
     */
    modes.ecb = function(options) {
      options = options || {};
      /** @type {string} */
      this.name = "ECB";
      this.cipher = options.cipher;
      this.blockSize = options.blockSize || 16;
      /** @type {number} */
      this._ints = this.blockSize / 4;
      /** @type {!Array} */
      this._inBlock = new Array(this._ints);
      /** @type {!Array} */
      this._outBlock = new Array(this._ints);
    };
    /**
     * @param {string} fn
     * @return {undefined}
     */
    modes.ecb.prototype.start = function(fn) {
    };
    /**
     * @param {!Array} a
     * @param {!Object} b
     * @param {boolean} m
     * @return {?}
     */
    modes.ecb.prototype.encrypt = function(a, b, m) {
      if (a.length() < this.blockSize && !(m && a.length() > 0)) {
        return true;
      }
      /** @type {number} */
      var i = 0;
      for (; i < this._ints; ++i) {
        this._inBlock[i] = a.getInt32();
      }
      this.cipher.encrypt(this._inBlock, this._outBlock);
      /** @type {number} */
      i = 0;
      for (; i < this._ints; ++i) {
        b.putInt32(this._outBlock[i]);
      }
    };
    /**
     * @param {!Array} a
     * @param {!Object} b
     * @param {boolean} input
     * @return {?}
     */
    modes.ecb.prototype.decrypt = function(a, b, input) {
      if (a.length() < this.blockSize && !(input && a.length() > 0)) {
        return true;
      }
      /** @type {number} */
      var i = 0;
      for (; i < this._ints; ++i) {
        this._inBlock[i] = a.getInt32();
      }
      this.cipher.decrypt(this._inBlock, this._outBlock);
      /** @type {number} */
      i = 0;
      for (; i < this._ints; ++i) {
        b.putInt32(this._outBlock[i]);
      }
    };
    /**
     * @param {!NodeList} _input
     * @param {?} adding
     * @return {?}
     */
    modes.ecb.prototype.pad = function(_input, adding) {
      var padding = _input.length() === this.blockSize ? this.blockSize : this.blockSize - _input.length();
      return _input.fillWithByte(padding, padding), true;
    };
    /**
     * @param {!Object} data
     * @param {!Object} options
     * @return {?}
     */
    modes.ecb.prototype.unpad = function(data, options) {
      if (options.overflow > 0) {
        return false;
      }
      var len = data.length();
      var count = data.at(len - 1);
      return !(count > this.blockSize << 2) && (data.truncate(count), true);
    };
    /**
     * @param {!Object} options
     * @return {undefined}
     */
    modes.cbc = function(options) {
      options = options || {};
      /** @type {string} */
      this.name = "CBC";
      this.cipher = options.cipher;
      this.blockSize = options.blockSize || 16;
      /** @type {number} */
      this._ints = this.blockSize / 4;
      /** @type {!Array} */
      this._inBlock = new Array(this._ints);
      /** @type {!Array} */
      this._outBlock = new Array(this._ints);
    };
    /**
     * @param {!Object} options
     * @return {undefined}
     */
    modes.cbc.prototype.start = function(options) {
      if (null === options.iv) {
        if (!this._prev) {
          throw new Error("Invalid IV parameter.");
        }
        this._iv = this._prev.slice(0);
      } else {
        if (!("iv" in options)) {
          throw new Error("Invalid IV parameter.");
        }
        this._iv = transformIV(options.iv);
        this._prev = this._iv.slice(0);
      }
    };
    /**
     * @param {!Array} a
     * @param {!Object} b
     * @param {boolean} m
     * @return {?}
     */
    modes.cbc.prototype.encrypt = function(a, b, m) {
      if (a.length() < this.blockSize && !(m && a.length() > 0)) {
        return true;
      }
      /** @type {number} */
      var i = 0;
      for (; i < this._ints; ++i) {
        /** @type {number} */
        this._inBlock[i] = this._prev[i] ^ a.getInt32();
      }
      this.cipher.encrypt(this._inBlock, this._outBlock);
      /** @type {number} */
      i = 0;
      for (; i < this._ints; ++i) {
        b.putInt32(this._outBlock[i]);
      }
      this._prev = this._outBlock;
    };
    /**
     * @param {!Array} a
     * @param {!Object} b
     * @param {boolean} input
     * @return {?}
     */
    modes.cbc.prototype.decrypt = function(a, b, input) {
      if (a.length() < this.blockSize && !(input && a.length() > 0)) {
        return true;
      }
      /** @type {number} */
      var i = 0;
      for (; i < this._ints; ++i) {
        this._inBlock[i] = a.getInt32();
      }
      this.cipher.decrypt(this._inBlock, this._outBlock);
      /** @type {number} */
      i = 0;
      for (; i < this._ints; ++i) {
        b.putInt32(this._prev[i] ^ this._outBlock[i]);
      }
      this._prev = this._inBlock.slice(0);
    };
    /**
     * @param {!NodeList} _input
     * @param {?} adding
     * @return {?}
     */
    modes.cbc.prototype.pad = function(_input, adding) {
      var padding = _input.length() === this.blockSize ? this.blockSize : this.blockSize - _input.length();
      return _input.fillWithByte(padding, padding), true;
    };
    /**
     * @param {!Object} data
     * @param {!Object} options
     * @return {?}
     */
    modes.cbc.prototype.unpad = function(data, options) {
      if (options.overflow > 0) {
        return false;
      }
      var len = data.length();
      var count = data.at(len - 1);
      return !(count > this.blockSize << 2) && (data.truncate(count), true);
    };
    /**
     * @param {!Object} options
     * @return {undefined}
     */
    modes.cfb = function(options) {
      options = options || {};
      /** @type {string} */
      this.name = "CFB";
      this.cipher = options.cipher;
      this.blockSize = options.blockSize || 16;
      /** @type {number} */
      this._ints = this.blockSize / 4;
      /** @type {null} */
      this._inBlock = null;
      /** @type {!Array} */
      this._outBlock = new Array(this._ints);
      /** @type {!Array} */
      this._partialBlock = new Array(this._ints);
      this._partialOutput = forge.util.createBuffer();
      /** @type {number} */
      this._partialBytes = 0;
    };
    /**
     * @param {!Object} options
     * @return {undefined}
     */
    modes.cfb.prototype.start = function(options) {
      if (!("iv" in options)) {
        throw new Error("Invalid IV parameter.");
      }
      this._iv = transformIV(options.iv);
      this._inBlock = this._iv.slice(0);
      /** @type {number} */
      this._partialBytes = 0;
    };
    /**
     * @param {!Object} input
     * @param {!Object} output
     * @param {boolean} finish
     * @return {?}
     */
    modes.cfb.prototype.encrypt = function(input, output, finish) {
      var inputLength = input.length();
      if (0 === inputLength) {
        return true;
      }
      if (this.cipher.encrypt(this._inBlock, this._outBlock), 0 === this._partialBytes && inputLength >= this.blockSize) {
        /** @type {number} */
        var i = 0;
        for (; i < this._ints; ++i) {
          /** @type {number} */
          this._inBlock[i] = input.getInt32() ^ this._outBlock[i];
          output.putInt32(this._inBlock[i]);
        }
      } else {
        /** @type {number} */
        var partialBytes = (this.blockSize - inputLength) % this.blockSize;
        if (partialBytes > 0) {
          /** @type {number} */
          partialBytes = this.blockSize - partialBytes;
        }
        this._partialOutput.clear();
        /** @type {number} */
        i = 0;
        for (; i < this._ints; ++i) {
          /** @type {number} */
          this._partialBlock[i] = input.getInt32() ^ this._outBlock[i];
          this._partialOutput.putInt32(this._partialBlock[i]);
        }
        if (partialBytes > 0) {
          input.read -= this.blockSize;
        } else {
          /** @type {number} */
          i = 0;
          for (; i < this._ints; ++i) {
            this._inBlock[i] = this._partialBlock[i];
          }
        }
        if (this._partialBytes > 0 && this._partialOutput.getBytes(this._partialBytes), partialBytes > 0 && !finish) {
          return output.putBytes(this._partialOutput.getBytes(partialBytes - this._partialBytes)), this._partialBytes = partialBytes, true;
        }
        output.putBytes(this._partialOutput.getBytes(inputLength - this._partialBytes));
        /** @type {number} */
        this._partialBytes = 0;
      }
    };
    /**
     * @param {!Object} input
     * @param {!Object} output
     * @param {boolean} finish
     * @return {?}
     */
    modes.cfb.prototype.decrypt = function(input, output, finish) {
      var inputLength = input.length();
      if (0 === inputLength) {
        return true;
      }
      if (this.cipher.encrypt(this._inBlock, this._outBlock), 0 === this._partialBytes && inputLength >= this.blockSize) {
        /** @type {number} */
        var i = 0;
        for (; i < this._ints; ++i) {
          this._inBlock[i] = input.getInt32();
          output.putInt32(this._inBlock[i] ^ this._outBlock[i]);
        }
      } else {
        /** @type {number} */
        var partialBytes = (this.blockSize - inputLength) % this.blockSize;
        if (partialBytes > 0) {
          /** @type {number} */
          partialBytes = this.blockSize - partialBytes;
        }
        this._partialOutput.clear();
        /** @type {number} */
        i = 0;
        for (; i < this._ints; ++i) {
          this._partialBlock[i] = input.getInt32();
          this._partialOutput.putInt32(this._partialBlock[i] ^ this._outBlock[i]);
        }
        if (partialBytes > 0) {
          input.read -= this.blockSize;
        } else {
          /** @type {number} */
          i = 0;
          for (; i < this._ints; ++i) {
            this._inBlock[i] = this._partialBlock[i];
          }
        }
        if (this._partialBytes > 0 && this._partialOutput.getBytes(this._partialBytes), partialBytes > 0 && !finish) {
          return output.putBytes(this._partialOutput.getBytes(partialBytes - this._partialBytes)), this._partialBytes = partialBytes, true;
        }
        output.putBytes(this._partialOutput.getBytes(inputLength - this._partialBytes));
        /** @type {number} */
        this._partialBytes = 0;
      }
    };
    /**
     * @param {!Object} options
     * @return {undefined}
     */
    modes.ofb = function(options) {
      options = options || {};
      /** @type {string} */
      this.name = "OFB";
      this.cipher = options.cipher;
      this.blockSize = options.blockSize || 16;
      /** @type {number} */
      this._ints = this.blockSize / 4;
      /** @type {null} */
      this._inBlock = null;
      /** @type {!Array} */
      this._outBlock = new Array(this._ints);
      this._partialOutput = forge.util.createBuffer();
      /** @type {number} */
      this._partialBytes = 0;
    };
    /**
     * @param {!Object} options
     * @return {undefined}
     */
    modes.ofb.prototype.start = function(options) {
      if (!("iv" in options)) {
        throw new Error("Invalid IV parameter.");
      }
      this._iv = transformIV(options.iv);
      this._inBlock = this._iv.slice(0);
      /** @type {number} */
      this._partialBytes = 0;
    };
    /**
     * @param {!Object} input
     * @param {!Object} output
     * @param {boolean} finish
     * @return {?}
     */
    modes.ofb.prototype.encrypt = function(input, output, finish) {
      var inputLength = input.length();
      if (0 === input.length()) {
        return true;
      }
      if (this.cipher.encrypt(this._inBlock, this._outBlock), 0 === this._partialBytes && inputLength >= this.blockSize) {
        /** @type {number} */
        var i = 0;
        for (; i < this._ints; ++i) {
          output.putInt32(input.getInt32() ^ this._outBlock[i]);
          this._inBlock[i] = this._outBlock[i];
        }
      } else {
        /** @type {number} */
        var partialBytes = (this.blockSize - inputLength) % this.blockSize;
        if (partialBytes > 0) {
          /** @type {number} */
          partialBytes = this.blockSize - partialBytes;
        }
        this._partialOutput.clear();
        /** @type {number} */
        i = 0;
        for (; i < this._ints; ++i) {
          this._partialOutput.putInt32(input.getInt32() ^ this._outBlock[i]);
        }
        if (partialBytes > 0) {
          input.read -= this.blockSize;
        } else {
          /** @type {number} */
          i = 0;
          for (; i < this._ints; ++i) {
            this._inBlock[i] = this._outBlock[i];
          }
        }
        if (this._partialBytes > 0 && this._partialOutput.getBytes(this._partialBytes), partialBytes > 0 && !finish) {
          return output.putBytes(this._partialOutput.getBytes(partialBytes - this._partialBytes)), this._partialBytes = partialBytes, true;
        }
        output.putBytes(this._partialOutput.getBytes(inputLength - this._partialBytes));
        /** @type {number} */
        this._partialBytes = 0;
      }
    };
    /** @type {function(!Object, !Object, boolean): ?} */
    modes.ofb.prototype.decrypt = modes.ofb.prototype.encrypt;
    /**
     * @param {!Object} options
     * @return {undefined}
     */
    modes.ctr = function(options) {
      options = options || {};
      /** @type {string} */
      this.name = "CTR";
      this.cipher = options.cipher;
      this.blockSize = options.blockSize || 16;
      /** @type {number} */
      this._ints = this.blockSize / 4;
      /** @type {null} */
      this._inBlock = null;
      /** @type {!Array} */
      this._outBlock = new Array(this._ints);
      this._partialOutput = forge.util.createBuffer();
      /** @type {number} */
      this._partialBytes = 0;
    };
    /**
     * @param {!Object} options
     * @return {undefined}
     */
    modes.ctr.prototype.start = function(options) {
      if (!("iv" in options)) {
        throw new Error("Invalid IV parameter.");
      }
      this._iv = transformIV(options.iv);
      this._inBlock = this._iv.slice(0);
      /** @type {number} */
      this._partialBytes = 0;
    };
    /**
     * @param {!Object} input
     * @param {!Object} output
     * @param {boolean} finish
     * @return {?}
     */
    modes.ctr.prototype.encrypt = function(input, output, finish) {
      var inputLength = input.length();
      if (0 === inputLength) {
        return true;
      }
      if (this.cipher.encrypt(this._inBlock, this._outBlock), 0 === this._partialBytes && inputLength >= this.blockSize) {
        /** @type {number} */
        var i = 0;
        for (; i < this._ints; ++i) {
          output.putInt32(input.getInt32() ^ this._outBlock[i]);
        }
      } else {
        /** @type {number} */
        var partialBytes = (this.blockSize - inputLength) % this.blockSize;
        if (partialBytes > 0) {
          /** @type {number} */
          partialBytes = this.blockSize - partialBytes;
        }
        this._partialOutput.clear();
        /** @type {number} */
        i = 0;
        for (; i < this._ints; ++i) {
          this._partialOutput.putInt32(input.getInt32() ^ this._outBlock[i]);
        }
        if (partialBytes > 0 && (input.read -= this.blockSize), this._partialBytes > 0 && this._partialOutput.getBytes(this._partialBytes), partialBytes > 0 && !finish) {
          return output.putBytes(this._partialOutput.getBytes(partialBytes - this._partialBytes)), this._partialBytes = partialBytes, true;
        }
        output.putBytes(this._partialOutput.getBytes(inputLength - this._partialBytes));
        /** @type {number} */
        this._partialBytes = 0;
      }
      inc32(this._inBlock);
    };
    /** @type {function(!Object, !Object, boolean): ?} */
    modes.ctr.prototype.decrypt = modes.ctr.prototype.encrypt;
    /**
     * @param {!Object} options
     * @return {undefined}
     */
    modes.gcm = function(options) {
      options = options || {};
      /** @type {string} */
      this.name = "GCM";
      this.cipher = options.cipher;
      this.blockSize = options.blockSize || 16;
      /** @type {number} */
      this._ints = this.blockSize / 4;
      /** @type {!Array} */
      this._inBlock = new Array(this._ints);
      /** @type {!Array} */
      this._outBlock = new Array(this._ints);
      this._partialOutput = forge.util.createBuffer();
      /** @type {number} */
      this._partialBytes = 0;
      /** @type {number} */
      this._R = 3774873600;
    };
    /**
     * @param {!Object} options
     * @return {undefined}
     */
    modes.gcm.prototype.start = function(options) {
      if (!("iv" in options)) {
        throw new Error("Invalid IV parameter.");
      }
      var _input = forge.util.createBuffer(options.iv);
      /** @type {number} */
      this._cipherLength = 0;
      var additionalData;
      if (additionalData = "additionalData" in options ? forge.util.createBuffer(options.additionalData) : forge.util.createBuffer(), "tagLength" in options ? this._tagLength = options.tagLength : this._tagLength = 128, this._tag = null, options.decrypt && (this._tag = forge.util.createBuffer(options.tag).getBytes(), this._tag.length !== this._tagLength / 8)) {
        throw new Error("Authentication tag does not match tag length.");
      }
      /** @type {!Array} */
      this._hashBlock = new Array(this._ints);
      /** @type {null} */
      this.tag = null;
      /** @type {!Array} */
      this._hashSubkey = new Array(this._ints);
      this.cipher.encrypt([0, 0, 0, 0], this._hashSubkey);
      /** @type {number} */
      this.componentBits = 4;
      this._m = this.generateHashTable(this._hashSubkey, this.componentBits);
      var a = _input.length();
      if (12 === a) {
        /** @type {!Array} */
        this._j0 = [_input.getInt32(), _input.getInt32(), _input.getInt32(), 1];
      } else {
        /** @type {!Array} */
        this._j0 = [0, 0, 0, 0];
        for (; _input.length() > 0;) {
          this._j0 = this.ghash(this._hashSubkey, this._j0, [_input.getInt32(), _input.getInt32(), _input.getInt32(), _input.getInt32()]);
        }
        this._j0 = this.ghash(this._hashSubkey, this._j0, [0, 0].concat(from64To32(8 * a)));
      }
      this._inBlock = this._j0.slice(0);
      inc32(this._inBlock);
      /** @type {number} */
      this._partialBytes = 0;
      additionalData = forge.util.createBuffer(additionalData);
      this._aDataLength = from64To32(8 * additionalData.length());
      /** @type {number} */
      var overflow = additionalData.length() % this.blockSize;
      if (overflow) {
        additionalData.fillWithByte(0, this.blockSize - overflow);
      }
      /** @type {!Array} */
      this._s = [0, 0, 0, 0];
      for (; additionalData.length() > 0;) {
        this._s = this.ghash(this._hashSubkey, this._s, [additionalData.getInt32(), additionalData.getInt32(), additionalData.getInt32(), additionalData.getInt32()]);
      }
    };
    /**
     * @param {!Object} input
     * @param {!Object} output
     * @param {boolean} finish
     * @return {?}
     */
    modes.gcm.prototype.encrypt = function(input, output, finish) {
      var inputLength = input.length();
      if (0 === inputLength) {
        return true;
      }
      if (this.cipher.encrypt(this._inBlock, this._outBlock), 0 === this._partialBytes && inputLength >= this.blockSize) {
        /** @type {number} */
        var i = 0;
        for (; i < this._ints; ++i) {
          output.putInt32(this._outBlock[i] ^= input.getInt32());
        }
        this._cipherLength += this.blockSize;
      } else {
        /** @type {number} */
        var partialBytes = (this.blockSize - inputLength) % this.blockSize;
        if (partialBytes > 0) {
          /** @type {number} */
          partialBytes = this.blockSize - partialBytes;
        }
        this._partialOutput.clear();
        /** @type {number} */
        i = 0;
        for (; i < this._ints; ++i) {
          this._partialOutput.putInt32(input.getInt32() ^ this._outBlock[i]);
        }
        if (0 === partialBytes || finish) {
          if (finish) {
            /** @type {number} */
            var overflow = inputLength % this.blockSize;
            this._cipherLength += overflow;
            this._partialOutput.truncate(this.blockSize - overflow);
          } else {
            this._cipherLength += this.blockSize;
          }
          /** @type {number} */
          i = 0;
          for (; i < this._ints; ++i) {
            this._outBlock[i] = this._partialOutput.getInt32();
          }
          this._partialOutput.read -= this.blockSize;
        }
        if (this._partialBytes > 0 && this._partialOutput.getBytes(this._partialBytes), partialBytes > 0 && !finish) {
          return input.read -= this.blockSize, output.putBytes(this._partialOutput.getBytes(partialBytes - this._partialBytes)), this._partialBytes = partialBytes, true;
        }
        output.putBytes(this._partialOutput.getBytes(inputLength - this._partialBytes));
        /** @type {number} */
        this._partialBytes = 0;
      }
      this._s = this.ghash(this._hashSubkey, this._s, this._outBlock);
      inc32(this._inBlock);
    };
    /**
     * @param {!Array} input
     * @param {!Object} val
     * @param {boolean} finish
     * @return {?}
     */
    modes.gcm.prototype.decrypt = function(input, val, finish) {
      var inputLength = input.length();
      if (inputLength < this.blockSize && !(finish && inputLength > 0)) {
        return true;
      }
      this.cipher.encrypt(this._inBlock, this._outBlock);
      inc32(this._inBlock);
      this._hashBlock[0] = input.getInt32();
      this._hashBlock[1] = input.getInt32();
      this._hashBlock[2] = input.getInt32();
      this._hashBlock[3] = input.getInt32();
      this._s = this.ghash(this._hashSubkey, this._s, this._hashBlock);
      /** @type {number} */
      var i = 0;
      for (; i < this._ints; ++i) {
        val.putInt32(this._outBlock[i] ^ this._hashBlock[i]);
      }
      if (inputLength < this.blockSize) {
        this._cipherLength += inputLength % this.blockSize;
      } else {
        this._cipherLength += this.blockSize;
      }
    };
    /**
     * @param {!FileWriter} output
     * @param {!Object} options
     * @return {?}
     */
    modes.gcm.prototype.afterFinish = function(output, options) {
      /** @type {boolean} */
      var r = true;
      if (options.decrypt && options.overflow) {
        output.truncate(this.blockSize - options.overflow);
      }
      this.tag = forge.util.createBuffer();
      var lengths = this._aDataLength.concat(from64To32(8 * this._cipherLength));
      this._s = this.ghash(this._hashSubkey, this._s, lengths);
      /** @type {!Array} */
      var tag = [];
      this.cipher.encrypt(this._j0, tag);
      /** @type {number} */
      var i = 0;
      for (; i < this._ints; ++i) {
        this.tag.putInt32(this._s[i] ^ tag[i]);
      }
      return this.tag.truncate(this.tag.length() % (this._tagLength / 8)), options.decrypt && this.tag.bytes() !== this._tag && (r = false), r;
    };
    /**
     * @param {!Array} str
     * @param {string} c
     * @return {?}
     */
    modes.gcm.prototype.multiply = function(str, c) {
      /** @type {!Array} */
      var z_i = [0, 0, 0, 0];
      var v_i = c.slice(0);
      /** @type {number} */
      var n = 0;
      for (; n < 128; ++n) {
        /** @type {number} */
        var i = str[n / 32 | 0] & 1 << 31 - n % 32;
        if (i) {
          z_i[0] ^= v_i[0];
          z_i[1] ^= v_i[1];
          z_i[2] ^= v_i[2];
          z_i[3] ^= v_i[3];
        }
        this.pow(v_i, v_i);
      }
      return z_i;
    };
    /**
     * @param {!Object} x
     * @param {number} out
     * @return {undefined}
     */
    modes.gcm.prototype.pow = function(x, out) {
      /** @type {number} */
      var r = 1 & x[3];
      /** @type {number} */
      var p = 3;
      for (; p > 0; --p) {
        /** @type {number} */
        out[p] = x[p] >>> 1 | (1 & x[p - 1]) << 31;
      }
      /** @type {number} */
      out[0] = x[0] >>> 1;
      if (r) {
        out[0] ^= this._R;
      }
    };
    /**
     * @param {!Array} x
     * @return {?}
     */
    modes.gcm.prototype.tableMultiply = function(x) {
      /** @type {!Array} */
      var z = [0, 0, 0, 0];
      /** @type {number} */
      var i = 0;
      for (; i < 32; ++i) {
        /** @type {number} */
        var tag_iter = i / 8 | 0;
        /** @type {number} */
        var x_i = x[tag_iter] >>> 4 * (7 - i % 8) & 15;
        var ah = this._m[i][x_i];
        z[0] ^= ah[0];
        z[1] ^= ah[1];
        z[2] ^= ah[2];
        z[3] ^= ah[3];
      }
      return z;
    };
    /**
     * @param {!Array} h
     * @param {!Array} y
     * @param {!Array} x
     * @return {?}
     */
    modes.gcm.prototype.ghash = function(h, y, x) {
      return y[0] ^= x[0], y[1] ^= x[1], y[2] ^= x[2], y[3] ^= x[3], this.tableMultiply(y);
    };
    /**
     * @param {boolean} h
     * @param {number} bits
     * @return {?}
     */
    modes.gcm.prototype.generateHashTable = function(h, bits) {
      /** @type {number} */
      var multiplier = 8 / bits;
      /** @type {number} */
      var perInt = 4 * multiplier;
      /** @type {number} */
      var size = 16 * multiplier;
      /** @type {!Array} */
      var m = new Array(size);
      /** @type {number} */
      var i = 0;
      for (; i < size; ++i) {
        /** @type {!Array} */
        var tmp = [0, 0, 0, 0];
        /** @type {number} */
        var idx = i / perInt | 0;
        /** @type {number} */
        var shft = (perInt - 1 - i % perInt) * bits;
        /** @type {number} */
        tmp[idx] = 1 << bits - 1 << shft;
        m[i] = this.generateSubHashTable(this.multiply(tmp, h), bits);
      }
      return m;
    };
    /**
     * @param {!Array} mid
     * @param {number} bits
     * @return {?}
     */
    modes.gcm.prototype.generateSubHashTable = function(mid, bits) {
      /** @type {number} */
      var size = 1 << bits;
      /** @type {number} */
      var half = size >>> 1;
      /** @type {!Array} */
      var m = new Array(size);
      m[half] = mid.slice(0);
      /** @type {number} */
      var i = half >>> 1;
      for (; i > 0;) {
        this.pow(m[2 * i], m[i] = []);
        /** @type {number} */
        i = i >> 1;
      }
      /** @type {number} */
      i = 2;
      for (; i < half;) {
        /** @type {number} */
        var j = 1;
        for (; j < i; ++j) {
          var a = m[i];
          var c = m[j];
          /** @type {!Array} */
          m[i + j] = [a[0] ^ c[0], a[1] ^ c[1], a[2] ^ c[2], a[3] ^ c[3]];
        }
        /** @type {number} */
        i = i * 2;
      }
      /** @type {!Array} */
      m[0] = [0, 0, 0, 0];
      /** @type {number} */
      i = half + 1;
      for (; i < size; ++i) {
        var c = m[i ^ half];
        /** @type {!Array} */
        m[i] = [mid[0] ^ c[0], mid[1] ^ c[1], mid[2] ^ c[2], mid[3] ^ c[3]];
      }
      return m;
    };
  }, function(mixin, canCreateDiscussions, $) {
    var self = $(0);
    mixin.exports = self.debug = self.debug || {};
    self.debug.storage = {};
    /**
     * @param {?} index
     * @param {?} position
     * @return {?}
     */
    self.debug.get = function(index, position) {
      var test;
      return "undefined" == typeof index ? test = self.debug.storage : index in self.debug.storage && (test = "undefined" == typeof position ? self.debug.storage[index] : self.debug.storage[index][position]), test;
    };
    /**
     * @param {?} key
     * @param {string} name
     * @param {?} module
     * @return {undefined}
     */
    self.debug.set = function(key, name, module) {
      if (!(key in self.debug.storage)) {
        self.debug.storage[key] = {};
      }
      self.debug.storage[key][name] = module;
    };
    /**
     * @param {?} cat
     * @param {?} name
     * @return {undefined}
     */
    self.debug.clear = function(cat, name) {
      if ("undefined" == typeof cat) {
        self.debug.storage = {};
      } else {
        if (cat in self.debug.storage) {
          if ("undefined" == typeof name) {
            delete self.debug.storage[cat];
          } else {
            delete self.debug.storage[cat][name];
          }
        }
      }
    };
  }, function(mixin, canCreateDiscussions, FORGE) {
    var forge = FORGE(0);
    FORGE(1);
    mixin.exports = forge.log = forge.log || {};
    /** @type {!Array} */
    forge.log.levels = ["none", "error", "warning", "info", "debug", "verbose", "max"];
    var sLevelInfo = {};
    /** @type {!Array} */
    var loggers = [];
    /** @type {null} */
    var sConsoleLogger = null;
    /** @type {number} */
    forge.log.LEVEL_LOCKED = 2;
    /** @type {number} */
    forge.log.NO_LEVEL_CHECK = 4;
    /** @type {number} */
    forge.log.INTERPOLATE = 8;
    /** @type {number} */
    var i = 0;
    for (; i < forge.log.levels.length; ++i) {
      var level = forge.log.levels[i];
      sLevelInfo[level] = {
        index : i,
        name : level.toUpperCase()
      };
    }
    /**
     * @param {!Object} message
     * @return {undefined}
     */
    forge.log.logMessage = function(message) {
      var bufferBindIndex = sLevelInfo[message.level].index;
      /** @type {number} */
      var i = 0;
      for (; i < loggers.length; ++i) {
        var logger = loggers[i];
        if (logger.flags & forge.log.NO_LEVEL_CHECK) {
          logger.f(message);
        } else {
          var tvrt = sLevelInfo[logger.level].index;
          if (bufferBindIndex <= tvrt) {
            logger.f(logger, message);
          }
        }
      }
    };
    /**
     * @param {!Object} message
     * @return {undefined}
     */
    forge.log.prepareStandard = function(message) {
      if (!("standard" in message)) {
        /** @type {string} */
        message.standard = sLevelInfo[message.level].name + " [" + message.category + "] " + message.message;
      }
    };
    /**
     * @param {!Object} message
     * @return {undefined}
     */
    forge.log.prepareFull = function(message) {
      if (!("full" in message)) {
        /** @type {!Array} */
        var args = [message.message];
        /** @type {!Array<?>} */
        args = args.concat([] || message.arguments);
        message.full = forge.util.format.apply(this, args);
      }
    };
    /**
     * @param {!Object} message
     * @return {undefined}
     */
    forge.log.prepareStandardFull = function(message) {
      if (!("standardFull" in message)) {
        forge.log.prepareStandard(message);
        message.standardFull = message.standard;
      }
    };
    /** @type {!Array} */
    var levels = ["error", "warning", "info", "debug", "verbose"];
    /** @type {number} */
    i = 0;
    for (; i < levels.length; ++i) {
      !function(level) {
        /**
         * @param {string} aCatergorySlug
         * @param {string} notMessage
         * @return {undefined}
         */
        forge.log[level] = function(aCatergorySlug, notMessage) {
          /** @type {!Array<?>} */
          var PL$73 = Array.prototype.slice.call(arguments).slice(2);
          var msg = {
            timestamp : new Date,
            level : level,
            category : aCatergorySlug,
            message : notMessage,
            arguments : PL$73
          };
          forge.log.logMessage(msg);
        };
      }(levels[i]);
    }
    if (forge.log.makeLogger = function(p) {
      var logger = {
        flags : 0,
        f : p
      };
      return forge.log.setLevel(logger, "none"), logger;
    }, forge.log.setLevel = function(logger, level) {
      /** @type {boolean} */
      var rval = false;
      if (logger && !(logger.flags & forge.log.LEVEL_LOCKED)) {
        /** @type {number} */
        var i = 0;
        for (; i < forge.log.levels.length; ++i) {
          var aValidLevel = forge.log.levels[i];
          if (level == aValidLevel) {
            /** @type {string} */
            logger.level = level;
            /** @type {boolean} */
            rval = true;
            break;
          }
        }
      }
      return rval;
    }, forge.log.lock = function(logger, val) {
      if ("undefined" == typeof val || val) {
        logger.flags |= forge.log.LEVEL_LOCKED;
      } else {
        logger.flags &= ~forge.log.LEVEL_LOCKED;
      }
    }, forge.log.addLogger = function(logger) {
      loggers.push(logger);
    }, "undefined" != typeof console && "log" in console) {
      var logger;
      if (console.error && console.warn && console.info && console.debug) {
        var levelHandlers = {
          error : console.error,
          warning : console.warn,
          info : console.info,
          debug : console.debug,
          verbose : console.debug
        };
        /**
         * @param {?} logger
         * @param {!Object} message
         * @return {undefined}
         */
        var f = function(logger, message) {
          forge.log.prepareStandard(message);
          var handler = levelHandlers[message.level];
          /** @type {!Array} */
          var _args = [message.standard];
          /** @type {!Array<?>} */
          _args = _args.concat(message.arguments.slice());
          handler.apply(console, _args);
        };
        logger = forge.log.makeLogger(f);
      } else {
        /**
         * @param {?} logger
         * @param {!Object} message
         * @return {undefined}
         */
        f = function(logger, message) {
          forge.log.prepareStandardFull(message);
          console.log(message.standardFull);
        };
        logger = forge.log.makeLogger(f);
      }
      forge.log.setLevel(logger, "debug");
      forge.log.addLogger(logger);
      sConsoleLogger = logger;
    } else {
      console = {
        log : function() {
        }
      };
    }
    if (null !== sConsoleLogger) {
      var valuesExp = forge.util.getQueryVariables();
      if ("console.level" in valuesExp && forge.log.setLevel(sConsoleLogger, valuesExp["console.level"].slice(-1)[0]), "console.lock" in valuesExp) {
        var showOpacity = valuesExp["console.lock"].slice(-1)[0];
        if ("true" == showOpacity) {
          forge.log.lock(sConsoleLogger);
        }
      }
    }
    forge.log.consoleLogger = sConsoleLogger;
  }, function(mixin, canCreateDiscussions, FORGE) {
    var forge = FORGE(0);
    FORGE(1);
    forge.mgf = forge.mgf || {};
    var directory_epub = mixin.exports = forge.mgf.mgf1 = forge.mgf1 = forge.mgf1 || {};
    /**
     * @param {!Object} algorithm
     * @return {?}
     */
    directory_epub.create = function(algorithm) {
      var mgf = {
        generate : function(x, n) {
          var result = new forge.util.ByteBuffer;
          /** @type {number} */
          var op = Math.ceil(n / algorithm.digestLength);
          /** @type {number} */
          var bits = 0;
          for (; bits < op; bits++) {
            var out = new forge.util.ByteBuffer;
            out.putInt32(bits);
            algorithm.start();
            algorithm.update(x + out.getBytes());
            result.putBuffer(algorithm.digest());
          }
          return result.truncate(result.length() - n), result.getBytes();
        }
      };
      return mgf;
    };
  }, function(mixin, canCreateDiscussions, FORGE) {
    /**
     * @param {!Object} e
     * @param {undefined} v
     * @return {?}
     */
    function hash(e, v) {
      return e.start().update(v).digest().getBytes();
    }
    /**
     * @param {?} prfOid
     * @return {?}
     */
    function prfOidToMessageDigest(prfOid) {
      var prfAlgorithm;
      if (prfOid) {
        if (prfAlgorithm = pki.oids[asn1.derToOid(prfOid)], !prfAlgorithm) {
          /** @type {!Error} */
          var error = new Error("Unsupported PRF OID.");
          throw error.oid = prfOid, error.supported = ["hmacWithSHA1", "hmacWithSHA224", "hmacWithSHA256", "hmacWithSHA384", "hmacWithSHA512"], error;
        }
      } else {
        /** @type {string} */
        prfAlgorithm = "hmacWithSHA1";
      }
      return prfAlgorithmToMessageDigest(prfAlgorithm);
    }
    /**
     * @param {string} prfAlgorithm
     * @return {?}
     */
    function prfAlgorithmToMessageDigest(prfAlgorithm) {
      var factory = forge.md;
      switch(prfAlgorithm) {
        case "hmacWithSHA224":
          factory = forge.md.sha512;
        case "hmacWithSHA1":
        case "hmacWithSHA256":
        case "hmacWithSHA384":
        case "hmacWithSHA512":
          prfAlgorithm = prfAlgorithm.substr(8).toLowerCase();
          break;
        default:
          /** @type {!Error} */
          var error = new Error("Unsupported PRF algorithm.");
          throw error.algorithm = prfAlgorithm, error.supported = ["hmacWithSHA1", "hmacWithSHA224", "hmacWithSHA256", "hmacWithSHA384", "hmacWithSHA512"], error;
      }
      if (!(factory && prfAlgorithm in factory)) {
        throw new Error("Unknown hash algorithm: " + prfAlgorithm);
      }
      return factory[prfAlgorithm].create();
    }
    /**
     * @param {string} salt
     * @param {string} countBytes
     * @param {number} dkLen
     * @param {string} prfAlgorithm
     * @return {?}
     */
    function createPbkdf2Params(salt, countBytes, dkLen, prfAlgorithm) {
      var n = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OCTETSTRING, false, salt), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, countBytes.getBytes())]);
      return "hmacWithSHA1" !== prfAlgorithm && n.value.push(asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, forge.util.hexToBytes(dkLen.toString(16))), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(pki.oids[prfAlgorithm]).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.NULL, false, "")])), n;
    }
    var forge = FORGE(0);
    if (FORGE(5), FORGE(3), FORGE(10), FORGE(4), FORGE(6), FORGE(15), FORGE(7), FORGE(2), FORGE(29), FORGE(11), FORGE(1), "undefined" == typeof BigInteger) {
      var BigInteger = forge.jsbn.BigInteger;
    }
    var asn1 = forge.asn1;
    var pki = forge.pki = forge.pki || {};
    mixin.exports = pki.pbe = forge.pbe = forge.pbe || {};
    var oids = pki.oids;
    var x509CertificateValidator = {
      name : "EncryptedPrivateKeyInfo",
      tagClass : asn1.Class.UNIVERSAL,
      type : asn1.Type.SEQUENCE,
      constructed : true,
      value : [{
        name : "EncryptedPrivateKeyInfo.encryptionAlgorithm",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.SEQUENCE,
        constructed : true,
        value : [{
          name : "AlgorithmIdentifier.algorithm",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.OID,
          constructed : false,
          capture : "encryptionOid"
        }, {
          name : "AlgorithmIdentifier.parameters",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.SEQUENCE,
          constructed : true,
          captureAsn1 : "encryptionParams"
        }]
      }, {
        name : "EncryptedPrivateKeyInfo.encryptedData",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.OCTETSTRING,
        constructed : false,
        capture : "encryptedData"
      }]
    };
    var rsaPrivateKeyValidator = {
      name : "PBES2Algorithms",
      tagClass : asn1.Class.UNIVERSAL,
      type : asn1.Type.SEQUENCE,
      constructed : true,
      value : [{
        name : "PBES2Algorithms.keyDerivationFunc",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.SEQUENCE,
        constructed : true,
        value : [{
          name : "PBES2Algorithms.keyDerivationFunc.oid",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.OID,
          constructed : false,
          capture : "kdfOid"
        }, {
          name : "PBES2Algorithms.params",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.SEQUENCE,
          constructed : true,
          value : [{
            name : "PBES2Algorithms.params.salt",
            tagClass : asn1.Class.UNIVERSAL,
            type : asn1.Type.OCTETSTRING,
            constructed : false,
            capture : "kdfSalt"
          }, {
            name : "PBES2Algorithms.params.iterationCount",
            tagClass : asn1.Class.UNIVERSAL,
            type : asn1.Type.INTEGER,
            constructed : false,
            capture : "kdfIterationCount"
          }, {
            name : "PBES2Algorithms.params.keyLength",
            tagClass : asn1.Class.UNIVERSAL,
            type : asn1.Type.INTEGER,
            constructed : false,
            optional : true,
            capture : "keyLength"
          }, {
            name : "PBES2Algorithms.params.prf",
            tagClass : asn1.Class.UNIVERSAL,
            type : asn1.Type.SEQUENCE,
            constructed : true,
            optional : true,
            value : [{
              name : "PBES2Algorithms.params.prf.algorithm",
              tagClass : asn1.Class.UNIVERSAL,
              type : asn1.Type.OID,
              constructed : false,
              capture : "prfOid"
            }]
          }]
        }]
      }, {
        name : "PBES2Algorithms.encryptionScheme",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.SEQUENCE,
        constructed : true,
        value : [{
          name : "PBES2Algorithms.encryptionScheme.oid",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.OID,
          constructed : false,
          capture : "encOid"
        }, {
          name : "PBES2Algorithms.encryptionScheme.iv",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.OCTETSTRING,
          constructed : false,
          capture : "encIv"
        }]
      }]
    };
    var rsaPublicKeyValidator = {
      name : "pkcs-12PbeParams",
      tagClass : asn1.Class.UNIVERSAL,
      type : asn1.Type.SEQUENCE,
      constructed : true,
      value : [{
        name : "pkcs-12PbeParams.salt",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.OCTETSTRING,
        constructed : false,
        capture : "salt"
      }, {
        name : "pkcs-12PbeParams.iterations",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.INTEGER,
        constructed : false,
        capture : "iterations"
      }]
    };
    /**
     * @param {string} obj
     * @param {!Array} password
     * @param {!Object} options
     * @return {?}
     */
    pki.encryptPrivateKeyInfo = function(obj, password, options) {
      options = options || {};
      options.saltSize = options.saltSize || 8;
      options.count = options.count || 2048;
      options.algorithm = options.algorithm || "aes128";
      options.prfAlgorithm = options.prfAlgorithm || "sha1";
      var dkLen;
      var tbsCertificate;
      var value;
      var salt = forge.random.getBytesSync(options.saltSize);
      var count = options.count;
      var countBytes = asn1.integerToDer(count);
      if (0 === options.algorithm.indexOf("aes") || "des" === options.algorithm) {
        var ivLen;
        var encOid;
        var cipherFn;
        switch(options.algorithm) {
          case "aes128":
            /** @type {number} */
            dkLen = 16;
            /** @type {number} */
            ivLen = 16;
            encOid = oids["aes128-CBC"];
            cipherFn = forge.aes.createEncryptionCipher;
            break;
          case "aes192":
            /** @type {number} */
            dkLen = 24;
            /** @type {number} */
            ivLen = 16;
            encOid = oids["aes192-CBC"];
            cipherFn = forge.aes.createEncryptionCipher;
            break;
          case "aes256":
            /** @type {number} */
            dkLen = 32;
            /** @type {number} */
            ivLen = 16;
            encOid = oids["aes256-CBC"];
            cipherFn = forge.aes.createEncryptionCipher;
            break;
          case "des":
            /** @type {number} */
            dkLen = 8;
            /** @type {number} */
            ivLen = 8;
            encOid = oids.desCBC;
            cipherFn = forge.des.createEncryptionCipher;
            break;
          default:
            /** @type {!Error} */
            var error = new Error("Cannot encrypt private key. Unknown encryption algorithm.");
            throw error.algorithm = options.algorithm, error;
        }
        /** @type {string} */
        var prfAlgorithm = "hmacWith" + options.prfAlgorithm.toUpperCase();
        var md = prfAlgorithmToMessageDigest(prfAlgorithm);
        var dk = forge.pkcs5.pbkdf2(password, salt, count, dkLen, md);
        var iv = forge.random.getBytesSync(ivLen);
        var cipher = cipherFn(dk);
        cipher.start(iv);
        cipher.update(asn1.toDer(obj));
        cipher.finish();
        value = cipher.output.getBytes();
        var params = createPbkdf2Params(salt, countBytes, dkLen, prfAlgorithm);
        tbsCertificate = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(oids.pkcs5PBES2).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(oids.pkcs5PBKDF2).getBytes()), params]), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, 
        asn1.Type.OID, false, asn1.oidToDer(encOid).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OCTETSTRING, false, iv)])])]);
      } else {
        if ("3des" !== options.algorithm) {
          /** @type {!Error} */
          error = new Error("Cannot encrypt private key. Unknown encryption algorithm.");
          throw error.algorithm = options.algorithm, error;
        }
        /** @type {number} */
        dkLen = 24;
        var saltBytes = new forge.util.ByteBuffer(salt);
        dk = pki.pbe.generatePkcs12Key(password, saltBytes, 1, count, dkLen);
        iv = pki.pbe.generatePkcs12Key(password, saltBytes, 2, count, dkLen);
        cipher = forge.des.createEncryptionCipher(dk);
        cipher.start(iv);
        cipher.update(asn1.toDer(obj));
        cipher.finish();
        value = cipher.output.getBytes();
        tbsCertificate = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(oids["pbeWithSHAAnd3-KeyTripleDES-CBC"]).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OCTETSTRING, false, salt), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, countBytes.getBytes())])]);
      }
      var rval = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [tbsCertificate, asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OCTETSTRING, false, value)]);
      return rval;
    };
    /**
     * @param {string} obj
     * @param {string} password
     * @return {?}
     */
    pki.decryptPrivateKeyInfo = function(obj, password) {
      /** @type {null} */
      var r = null;
      var capture = {};
      /** @type {!Array} */
      var errors = [];
      if (!asn1.validate(obj, x509CertificateValidator, capture, errors)) {
        /** @type {!Error} */
        var error = new Error("Cannot read encrypted private key. ASN.1 object is not a supported EncryptedPrivateKeyInfo.");
        throw error.errors = errors, error;
      }
      var oid = asn1.derToOid(capture.encryptionOid);
      var cipher = pki.pbe.getCipher(oid, capture.encryptionParams, password);
      var d = forge.util.createBuffer(capture.encryptedData);
      return cipher.update(d), cipher.finish() && (r = asn1.fromDer(cipher.output)), r;
    };
    /**
     * @param {string} epki
     * @param {number} maxline
     * @return {?}
     */
    pki.encryptedPrivateKeyToPem = function(epki, maxline) {
      var r = {
        type : "ENCRYPTED PRIVATE KEY",
        body : asn1.toDer(epki).getBytes()
      };
      return forge.pem.encode(r, {
        maxline : maxline
      });
    };
    /**
     * @param {?} e
     * @return {?}
     */
    pki.encryptedPrivateKeyFromPem = function(e) {
      var msg = forge.pem.decode(e)[0];
      if ("ENCRYPTED PRIVATE KEY" !== msg.type) {
        /** @type {!Error} */
        var error = new Error('Could not convert encrypted private key from PEM; PEM header type is "ENCRYPTED PRIVATE KEY".');
        throw error.headerType = msg.type, error;
      }
      if (msg.procType && "ENCRYPTED" === msg.procType.type) {
        throw new Error("Could not convert encrypted private key from PEM; PEM is encrypted.");
      }
      return asn1.fromDer(msg.body);
    };
    /**
     * @param {?} key
     * @param {!Array} password
     * @param {!Object} options
     * @return {?}
     */
    pki.encryptRsaPrivateKey = function(key, password, options) {
      if (options = options || {}, !options.legacy) {
        var rval = pki.wrapRsaPrivateKey(pki.privateKeyToAsn1(key));
        return rval = pki.encryptPrivateKeyInfo(rval, password, options), pki.encryptedPrivateKeyToPem(rval);
      }
      var algorithm;
      var data;
      var dkLen;
      var cipherFn;
      switch(options.algorithm) {
        case "aes128":
          /** @type {string} */
          algorithm = "AES-128-CBC";
          /** @type {number} */
          dkLen = 16;
          data = forge.random.getBytesSync(16);
          cipherFn = forge.aes.createEncryptionCipher;
          break;
        case "aes192":
          /** @type {string} */
          algorithm = "AES-192-CBC";
          /** @type {number} */
          dkLen = 24;
          data = forge.random.getBytesSync(16);
          cipherFn = forge.aes.createEncryptionCipher;
          break;
        case "aes256":
          /** @type {string} */
          algorithm = "AES-256-CBC";
          /** @type {number} */
          dkLen = 32;
          data = forge.random.getBytesSync(16);
          cipherFn = forge.aes.createEncryptionCipher;
          break;
        case "3des":
          /** @type {string} */
          algorithm = "DES-EDE3-CBC";
          /** @type {number} */
          dkLen = 24;
          data = forge.random.getBytesSync(8);
          cipherFn = forge.des.createEncryptionCipher;
          break;
        case "des":
          /** @type {string} */
          algorithm = "DES-CBC";
          /** @type {number} */
          dkLen = 8;
          data = forge.random.getBytesSync(8);
          cipherFn = forge.des.createEncryptionCipher;
          break;
        default:
          /** @type {!Error} */
          var error = new Error('Could not encrypt RSA private key; unsupported encryption algorithm "' + options.algorithm + '".');
          throw error.algorithm = options.algorithm, error;
      }
      var dk = forge.pbe.opensslDeriveBytes(password, data.substr(0, 8), dkLen);
      var cipher = cipherFn(dk);
      cipher.start(data);
      cipher.update(asn1.toDer(pki.privateKeyToAsn1(key)));
      cipher.finish();
      var msg = {
        type : "RSA PRIVATE KEY",
        procType : {
          version : "4",
          type : "ENCRYPTED"
        },
        dekInfo : {
          algorithm : algorithm,
          parameters : forge.util.bytesToHex(data).toUpperCase()
        },
        body : cipher.output.getBytes()
      };
      return forge.pem.encode(msg);
    };
    /**
     * @param {?} url
     * @param {string} password
     * @return {?}
     */
    pki.decryptRsaPrivateKey = function(url, password) {
      /** @type {null} */
      var rval = null;
      var msg = forge.pem.decode(url)[0];
      if ("ENCRYPTED PRIVATE KEY" !== msg.type && "PRIVATE KEY" !== msg.type && "RSA PRIVATE KEY" !== msg.type) {
        /** @type {!Error} */
        var error = new Error('Could not convert private key from PEM; PEM header type is not "ENCRYPTED PRIVATE KEY", "PRIVATE KEY", or "RSA PRIVATE KEY".');
        throw error.headerType = error, error;
      }
      if (msg.procType && "ENCRYPTED" === msg.procType.type) {
        var dkLen;
        var cipherFn;
        switch(msg.dekInfo.algorithm) {
          case "DES-CBC":
            /** @type {number} */
            dkLen = 8;
            cipherFn = forge.des.createDecryptionCipher;
            break;
          case "DES-EDE3-CBC":
            /** @type {number} */
            dkLen = 24;
            cipherFn = forge.des.createDecryptionCipher;
            break;
          case "AES-128-CBC":
            /** @type {number} */
            dkLen = 16;
            cipherFn = forge.aes.createDecryptionCipher;
            break;
          case "AES-192-CBC":
            /** @type {number} */
            dkLen = 24;
            cipherFn = forge.aes.createDecryptionCipher;
            break;
          case "AES-256-CBC":
            /** @type {number} */
            dkLen = 32;
            cipherFn = forge.aes.createDecryptionCipher;
            break;
          case "RC2-40-CBC":
            /** @type {number} */
            dkLen = 5;
            /**
             * @param {string} key
             * @return {?}
             */
            cipherFn = function(key) {
              return forge.rc2.createDecryptionCipher(key, 40);
            };
            break;
          case "RC2-64-CBC":
            /** @type {number} */
            dkLen = 8;
            /**
             * @param {string} key
             * @return {?}
             */
            cipherFn = function(key) {
              return forge.rc2.createDecryptionCipher(key, 64);
            };
            break;
          case "RC2-128-CBC":
            /** @type {number} */
            dkLen = 16;
            /**
             * @param {string} key
             * @return {?}
             */
            cipherFn = function(key) {
              return forge.rc2.createDecryptionCipher(key, 128);
            };
            break;
          default:
            /** @type {!Error} */
            error = new Error('Could not decrypt private key; unsupported encryption algorithm "' + msg.dekInfo.algorithm + '".');
            throw error.algorithm = msg.dekInfo.algorithm, error;
        }
        var data = forge.util.hexToBytes(msg.dekInfo.parameters);
        var dk = forge.pbe.opensslDeriveBytes(password, data.substr(0, 8), dkLen);
        var cipher = cipherFn(dk);
        if (cipher.start(data), cipher.update(forge.util.createBuffer(msg.body)), !cipher.finish()) {
          return rval;
        }
        rval = cipher.output.getBytes();
      } else {
        rval = msg.body;
      }
      return rval = "ENCRYPTED PRIVATE KEY" === msg.type ? pki.decryptPrivateKeyInfo(asn1.fromDer(rval), password) : asn1.fromDer(rval), null !== rval && (rval = pki.privateKeyFromAsn1(rval)), rval;
    };
    /**
     * @param {?} password
     * @param {!Object} salt
     * @param {number} id
     * @param {number} iter
     * @param {number} n
     * @param {!Object} md
     * @return {?}
     */
    pki.pbe.generatePkcs12Key = function(password, salt, id, iter, n, md) {
      var w;
      var l;
      if ("undefined" == typeof md || null === md) {
        if (!("sha1" in forge.md)) {
          throw new Error('"sha1" hash algorithm unavailable.');
        }
        md = forge.md.sha1.create();
      }
      var u = md.digestLength;
      var v = md.blockLength;
      var result = new forge.util.ByteBuffer;
      var passBuf = new forge.util.ByteBuffer;
      if (null !== password && void 0 !== password) {
        /** @type {number} */
        l = 0;
        for (; l < password.length; l++) {
          passBuf.putInt16(password.charCodeAt(l));
        }
        passBuf.putInt16(0);
      }
      var p = passBuf.length();
      var s = salt.length();
      var D = new forge.util.ByteBuffer;
      D.fillWithByte(id, v);
      /** @type {number} */
      var Slen = v * Math.ceil(s / v);
      var b = new forge.util.ByteBuffer;
      /** @type {number} */
      l = 0;
      for (; l < Slen; l++) {
        b.putByte(salt.at(l % s));
      }
      /** @type {number} */
      var Plen = v * Math.ceil(p / v);
      var value = new forge.util.ByteBuffer;
      /** @type {number} */
      l = 0;
      for (; l < Plen; l++) {
        value.putByte(passBuf.at(l % p));
      }
      var bytes = b;
      bytes.putBuffer(value);
      /** @type {number} */
      var S = Math.ceil(n / u);
      /** @type {number} */
      var period = 1;
      for (; period <= S; period++) {
        var buf = new forge.util.ByteBuffer;
        buf.putBytes(D.bytes());
        buf.putBytes(bytes.bytes());
        /** @type {number} */
        var iterCount = 0;
        for (; iterCount < iter; iterCount++) {
          md.start();
          md.update(buf.getBytes());
          buf = md.digest();
        }
        var B = new forge.util.ByteBuffer;
        /** @type {number} */
        l = 0;
        for (; l < v; l++) {
          B.putByte(buf.at(l % u));
        }
        /** @type {number} */
        var imgWidth = Math.ceil(s / v) + Math.ceil(p / v);
        var b = new forge.util.ByteBuffer;
        /** @type {number} */
        w = 0;
        for (; w < imgWidth; w++) {
          var chunk = new forge.util.ByteBuffer(bytes.getBytes(v));
          /** @type {number} */
          var height = 511;
          /** @type {number} */
          l = B.length() - 1;
          for (; l >= 0; l--) {
            /** @type {number} */
            height = height >> 8;
            height = height + (B.at(l) + chunk.at(l));
            chunk.setAt(l, 255 & height);
          }
          b.putBuffer(chunk);
        }
        bytes = b;
        result.putBuffer(buf);
      }
      return result.truncate(result.length() - n), result;
    };
    /**
     * @param {?} oid
     * @param {string} params
     * @param {string} password
     * @return {?}
     */
    pki.pbe.getCipher = function(oid, params, password) {
      switch(oid) {
        case pki.oids.pkcs5PBES2:
          return pki.pbe.getCipherForPBES2(oid, params, password);
        case pki.oids["pbeWithSHAAnd3-KeyTripleDES-CBC"]:
        case pki.oids["pbewithSHAAnd40BitRC2-CBC"]:
          return pki.pbe.getCipherForPKCS12PBE(oid, params, password);
        default:
          /** @type {!Error} */
          var error = new Error("Cannot read encrypted PBE data block. Unsupported OID.");
          throw error.oid = oid, error.supportedOids = ["pkcs5PBES2", "pbeWithSHAAnd3-KeyTripleDES-CBC", "pbewithSHAAnd40BitRC2-CBC"], error;
      }
    };
    /**
     * @param {!Function} oid
     * @param {string} params
     * @param {string} password
     * @return {?}
     */
    pki.pbe.getCipherForPBES2 = function(oid, params, password) {
      var capture = {};
      /** @type {!Array} */
      var errors = [];
      if (!asn1.validate(params, rsaPrivateKeyValidator, capture, errors)) {
        /** @type {!Error} */
        var error = new Error("Cannot read password-based-encryption algorithm parameters. ASN.1 object is not a supported EncryptedPrivateKeyInfo.");
        throw error.errors = errors, error;
      }
      if (oid = asn1.derToOid(capture.kdfOid), oid !== pki.oids.pkcs5PBKDF2) {
        /** @type {!Error} */
        error = new Error("Cannot read encrypted private key. Unsupported key derivation function OID.");
        throw error.oid = oid, error.supportedOids = ["pkcs5PBKDF2"], error;
      }
      if (oid = asn1.derToOid(capture.encOid), oid !== pki.oids["aes128-CBC"] && oid !== pki.oids["aes192-CBC"] && oid !== pki.oids["aes256-CBC"] && oid !== pki.oids["des-EDE3-CBC"] && oid !== pki.oids.desCBC) {
        /** @type {!Error} */
        error = new Error("Cannot read encrypted private key. Unsupported encryption scheme OID.");
        throw error.oid = oid, error.supportedOids = ["aes128-CBC", "aes192-CBC", "aes256-CBC", "des-EDE3-CBC", "desCBC"], error;
      }
      var salt = capture.kdfSalt;
      var count = forge.util.createBuffer(capture.kdfIterationCount);
      count = count.getInt(count.length() << 3);
      var dkLen;
      var cipherFn;
      switch(pki.oids[oid]) {
        case "aes128-CBC":
          /** @type {number} */
          dkLen = 16;
          cipherFn = forge.aes.createDecryptionCipher;
          break;
        case "aes192-CBC":
          /** @type {number} */
          dkLen = 24;
          cipherFn = forge.aes.createDecryptionCipher;
          break;
        case "aes256-CBC":
          /** @type {number} */
          dkLen = 32;
          cipherFn = forge.aes.createDecryptionCipher;
          break;
        case "des-EDE3-CBC":
          /** @type {number} */
          dkLen = 24;
          cipherFn = forge.des.createDecryptionCipher;
          break;
        case "desCBC":
          /** @type {number} */
          dkLen = 8;
          cipherFn = forge.des.createDecryptionCipher;
      }
      var md = prfOidToMessageDigest(capture.prfOid);
      var dk = forge.pkcs5.pbkdf2(password, salt, count, dkLen, md);
      var iv = capture.encIv;
      var cipher = cipherFn(dk);
      return cipher.start(iv), cipher;
    };
    /**
     * @param {?} oid
     * @param {string} params
     * @param {string} password
     * @return {?}
     */
    pki.pbe.getCipherForPKCS12PBE = function(oid, params, password) {
      var capture = {};
      /** @type {!Array} */
      var errors = [];
      if (!asn1.validate(params, rsaPublicKeyValidator, capture, errors)) {
        /** @type {!Error} */
        var error = new Error("Cannot read password-based-encryption algorithm parameters. ASN.1 object is not a supported EncryptedPrivateKeyInfo.");
        throw error.errors = errors, error;
      }
      var salt = forge.util.createBuffer(capture.salt);
      var count = forge.util.createBuffer(capture.iterations);
      count = count.getInt(count.length() << 3);
      var dkLen;
      var dIvLen;
      var cipherFn;
      switch(oid) {
        case pki.oids["pbeWithSHAAnd3-KeyTripleDES-CBC"]:
          /** @type {number} */
          dkLen = 24;
          /** @type {number} */
          dIvLen = 8;
          cipherFn = forge.des.startDecrypting;
          break;
        case pki.oids["pbewithSHAAnd40BitRC2-CBC"]:
          /** @type {number} */
          dkLen = 5;
          /** @type {number} */
          dIvLen = 8;
          /**
           * @param {string} t
           * @param {undefined} key
           * @return {?}
           */
          cipherFn = function(t, key) {
            var value = forge.rc2.createDecryptionCipher(t, 40);
            return value.start(key, null), value;
          };
          break;
        default:
          /** @type {!Error} */
          error = new Error("Cannot read PKCS #12 PBE data block. Unsupported OID.");
          throw error.oid = oid, error;
      }
      var md = prfOidToMessageDigest(capture.prfOid);
      var key = pki.pbe.generatePkcs12Key(password, salt, 1, count, dkLen, md);
      md.start();
      var iv = pki.pbe.generatePkcs12Key(password, salt, 2, count, dIvLen, md);
      return cipherFn(key, iv);
    };
    /**
     * @param {string} type
     * @param {string} s
     * @param {number} dkLen
     * @param {!Array} md
     * @return {?}
     */
    pki.pbe.opensslDeriveBytes = function(type, s, dkLen, md) {
      if ("undefined" == typeof md || null === md) {
        if (!("md5" in forge.md)) {
          throw new Error('"md5" hash algorithm unavailable.');
        }
        md = forge.md.md5.create();
      }
      if (null === s) {
        /** @type {string} */
        s = "";
      }
      /** @type {!Array} */
      var digests = [hash(md, type + s)];
      /** @type {number} */
      var length = 16;
      /** @type {number} */
      var i = 1;
      for (; length < dkLen; ++i, length = length + 16) {
        digests.push(hash(md, digests[i - 1] + type + s));
      }
      return digests.join("").substr(0, dkLen);
    };
  }, function(mixin, canCreateDiscussions, FORGE) {
    /**
     * @param {string} seed
     * @param {number} maskLength
     * @param {!Object} hash
     * @return {?}
     */
    function rsa_mgf1(seed, maskLength, hash) {
      if (!hash) {
        hash = forge.md.sha1.create();
      }
      /** @type {string} */
      var T = "";
      /** @type {number} */
      var i = Math.ceil(maskLength / hash.digestLength);
      /** @type {number} */
      var nextCreation = 0;
      for (; nextCreation < i; ++nextCreation) {
        /** @type {string} */
        var i = String.fromCharCode(nextCreation >> 24 & 255, nextCreation >> 16 & 255, nextCreation >> 8 & 255, 255 & nextCreation);
        hash.start();
        hash.update(seed + i);
        T = T + hash.digest().getBytes();
      }
      return T.substring(0, maskLength);
    }
    var forge = FORGE(0);
    FORGE(1);
    FORGE(2);
    FORGE(9);
    var pkcs1 = mixin.exports = forge.pkcs1 = forge.pkcs1 || {};
    /**
     * @param {!Object} key
     * @param {?} message
     * @param {!Object} options
     * @return {?}
     */
    pkcs1.encode_rsa_oaep = function(key, message, options) {
      var i;
      var seed;
      var md;
      var mgf1Md;
      if ("string" == typeof options) {
        /** @type {!Object} */
        i = options;
        seed = arguments[3] || void 0;
        md = arguments[4] || void 0;
      } else {
        if (options) {
          i = options.label || void 0;
          seed = options.seed || void 0;
          md = options.md || void 0;
          if (options.mgf1 && options.mgf1.md) {
            mgf1Md = options.mgf1.md;
          }
        }
      }
      if (md) {
        md.start();
      } else {
        md = forge.md.sha1.create();
      }
      if (!mgf1Md) {
        mgf1Md = md;
      }
      /** @type {number} */
      var keyLength = Math.ceil(key.n.bitLength() / 8);
      /** @type {number} */
      var maxLength = keyLength - 2 * md.digestLength - 2;
      if (message.length > maxLength) {
        /** @type {!Error} */
        var error = new Error("RSAES-OAEP input message length is too long.");
        throw error.length = message.length, error.maxLength = maxLength, error;
      }
      if (!i) {
        /** @type {string} */
        i = "";
      }
      md.update(i, "raw");
      var cidToGidStream = md.digest();
      /** @type {string} */
      var pix_color = "";
      /** @type {number} */
      var blockCountY = maxLength - message.length;
      /** @type {number} */
      var y = 0;
      for (; y < blockCountY; y++) {
        /** @type {string} */
        pix_color = pix_color + "\x00";
      }
      /** @type {string} */
      var DB = cidToGidStream.getBytes() + pix_color + "\u0001" + message;
      if (seed) {
        if (seed.length !== md.digestLength) {
          /** @type {!Error} */
          error = new Error("Invalid RSAES-OAEP seed. The seed length must match the digest length.");
          throw error.seedLength = seed.length, error.digestLength = md.digestLength, error;
        }
      } else {
        seed = forge.random.getBytes(md.digestLength);
      }
      var dbMask = rsa_mgf1(seed, keyLength - md.digestLength - 1, mgf1Md);
      var maskedDB = forge.util.xorBytes(DB, dbMask, DB.length);
      var seedMask = rsa_mgf1(maskedDB, md.digestLength, mgf1Md);
      var maskedSeed = forge.util.xorBytes(seed, seedMask, seed.length);
      return "\x00" + maskedSeed + maskedDB;
    };
    /**
     * @param {!Object} key
     * @param {!Object} em
     * @param {!Object} options
     * @return {?}
     */
    pkcs1.decode_rsa_oaep = function(key, em, options) {
      var e;
      var md;
      var mgf1Md;
      if ("string" == typeof options) {
        /** @type {!Object} */
        e = options;
        md = arguments[3] || void 0;
      } else {
        if (options) {
          e = options.label || void 0;
          md = options.md || void 0;
          if (options.mgf1 && options.mgf1.md) {
            mgf1Md = options.mgf1.md;
          }
        }
      }
      /** @type {number} */
      var keyLength = Math.ceil(key.n.bitLength() / 8);
      if (em.length !== keyLength) {
        /** @type {!Error} */
        var error = new Error("RSAES-OAEP encoded message length is invalid.");
        throw error.length = em.length, error.expectedLength = keyLength, error;
      }
      if (void 0 === md ? md = forge.md.sha1.create() : md.start(), mgf1Md || (mgf1Md = md), keyLength < 2 * md.digestLength + 2) {
        throw new Error("RSAES-OAEP key is too short for the hash function.");
      }
      if (!e) {
        /** @type {string} */
        e = "";
      }
      md.update(e, "raw");
      var posPattern = md.digest().getBytes();
      var fn = em.charAt(0);
      var DB = em.substring(1, md.digestLength + 1);
      var seed = em.substring(1 + md.digestLength);
      var dbMask = rsa_mgf1(seed, md.digestLength, mgf1Md);
      var maskedDB = forge.util.xorBytes(DB, dbMask, DB.length);
      var seedMask = rsa_mgf1(maskedDB, keyLength - md.digestLength - 1, mgf1Md);
      var db = forge.util.xorBytes(seed, seedMask, seed.length);
      var pohja = db.substring(0, md.digestLength);
      /** @type {boolean} */
      error = "\x00" !== fn;
      /** @type {number} */
      var i = 0;
      for (; i < md.digestLength; ++i) {
        /** @type {number} */
        error = error | posPattern.charAt(i) !== pohja.charAt(i);
      }
      /** @type {number} */
      var in_ps = 1;
      var index = md.digestLength;
      var j = md.digestLength;
      for (; j < db.length; j++) {
        var code = db.charCodeAt(j);
        /** @type {number} */
        var is_0 = 1 & code ^ 1;
        /** @type {number} */
        var error_mask = in_ps ? 65534 : 0;
        /** @type {number} */
        error = error | code & error_mask;
        /** @type {number} */
        in_ps = in_ps & is_0;
        /** @type {string} */
        index = index + in_ps;
      }
      if (error || 1 !== db.charCodeAt(index)) {
        throw new Error("Invalid RSAES-OAEP padding.");
      }
      return db.substring(index + 1);
    };
  }, function(mixin, canCreateDiscussions, FORGE) {
    /**
     * @param {!Array} safeContents
     * @param {!Object} attrName
     * @param {!Object} attrValue
     * @param {number} bagType
     * @return {?}
     */
    function _getBagsByAttribute(safeContents, attrName, attrValue, bagType) {
      /** @type {!Array} */
      var result = [];
      /** @type {number} */
      var i = 0;
      for (; i < safeContents.length; i++) {
        /** @type {number} */
        var j = 0;
        for (; j < safeContents[i].safeBags.length; j++) {
          var bag = safeContents[i].safeBags[j];
          if (!(void 0 !== bagType && bag.type !== bagType)) {
            if (null !== attrName) {
              if (void 0 !== bag.attributes[attrName] && bag.attributes[attrName].indexOf(attrValue) >= 0) {
                result.push(bag);
              }
            } else {
              result.push(bag);
            }
          }
        }
      }
      return result;
    }
    /**
     * @param {string} data
     * @return {?}
     */
    function _decodePkcs7Data(data) {
      if (data.composed || data.constructed) {
        var ed = forge.util.createBuffer();
        /** @type {number} */
        var i = 0;
        for (; i < data.value.length; ++i) {
          ed.putBytes(data.value[i].value);
        }
        /** @type {boolean} */
        data.composed = data.constructed = false;
        data.value = ed.getBytes();
      }
      return data;
    }
    /**
     * @param {?} pfx
     * @param {string} authSafe
     * @param {?} strict
     * @param {!Function} password
     * @return {undefined}
     */
    function _decodeAuthenticatedSafe(pfx, authSafe, strict, password) {
      if (authSafe = asn1.fromDer(authSafe, strict), authSafe.tagClass !== asn1.Class.UNIVERSAL || authSafe.type !== asn1.Type.SEQUENCE || authSafe.constructed !== true) {
        throw new Error("PKCS#12 AuthenticatedSafe expected to be a SEQUENCE OF ContentInfo");
      }
      /** @type {number} */
      var i = 0;
      for (; i < authSafe.value.length; i++) {
        var value = authSafe.value[i];
        var capture = {};
        /** @type {!Array} */
        var errors = [];
        if (!asn1.validate(value, x509CertificateValidator, capture, errors)) {
          /** @type {!Error} */
          var error = new Error("Cannot read ContentInfo.");
          throw error.errors = errors, error;
        }
        var obj = {
          encrypted : false
        };
        /** @type {null} */
        var safeContents = null;
        var data = capture.content.value[0];
        switch(asn1.derToOid(capture.contentType)) {
          case pki.oids.data:
            if (data.tagClass !== asn1.Class.UNIVERSAL || data.type !== asn1.Type.OCTETSTRING) {
              throw new Error("PKCS#12 SafeContents Data is not an OCTET STRING.");
            }
            safeContents = _decodePkcs7Data(data).value;
            break;
          case pki.oids.encryptedData:
            safeContents = _decryptSafeContents(data, password);
            /** @type {boolean} */
            obj.encrypted = true;
            break;
          default:
            /** @type {!Error} */
            error = new Error("Unsupported PKCS#12 contentType.");
            throw error.contentType = asn1.derToOid(capture.contentType), error;
        }
        obj.safeBags = _decodeSafeContents(safeContents, strict, password);
        pfx.safeContents.push(obj);
      }
    }
    /**
     * @param {string} data
     * @param {!Function} password
     * @return {?}
     */
    function _decryptSafeContents(data, password) {
      var capture = {};
      /** @type {!Array} */
      var errors = [];
      if (!asn1.validate(data, forge.pkcs7.asn1.encryptedDataValidator, capture, errors)) {
        /** @type {!Error} */
        var error = new Error("Cannot read EncryptedContentInfo.");
        throw error.errors = errors, error;
      }
      var oid = asn1.derToOid(capture.contentType);
      if (oid !== pki.oids.data) {
        /** @type {!Error} */
        error = new Error("PKCS#12 EncryptedContentInfo ContentType is not Data.");
        throw error.oid = oid, error;
      }
      oid = asn1.derToOid(capture.encAlgorithm);
      var cipher = pki.pbe.getCipher(oid, capture.encParameter, password);
      var encryptedContentAsn1 = _decodePkcs7Data(capture.encryptedContentAsn1);
      var d = forge.util.createBuffer(encryptedContentAsn1.value);
      if (cipher.update(d), !cipher.finish()) {
        throw new Error("Failed to decrypt PKCS#12 SafeContents.");
      }
      return cipher.output.getBytes();
    }
    /**
     * @param {string} safeContents
     * @param {!Array} strict
     * @param {!Function} password
     * @return {?}
     */
    function _decodeSafeContents(safeContents, strict, password) {
      if (!strict && 0 === safeContents.length) {
        return [];
      }
      if (safeContents = asn1.fromDer(safeContents, strict), safeContents.tagClass !== asn1.Class.UNIVERSAL || safeContents.type !== asn1.Type.SEQUENCE || safeContents.constructed !== true) {
        throw new Error("PKCS#12 SafeContents expected to be a SEQUENCE OF SafeBag.");
      }
      /** @type {!Array} */
      var res = [];
      /** @type {number} */
      var i = 0;
      for (; i < safeContents.value.length; i++) {
        var value = safeContents.value[i];
        var capture = {};
        /** @type {!Array} */
        var errors = [];
        if (!asn1.validate(value, rsaPrivateKeyValidator, capture, errors)) {
          /** @type {!Error} */
          var error = new Error("Cannot read SafeBag.");
          throw error.errors = errors, error;
        }
        var bag = {
          type : asn1.derToOid(capture.bagId),
          attributes : _decodeBagAttributes(capture.bagAttributes)
        };
        res.push(bag);
        var type;
        var decoder;
        var bagAsn1 = capture.bagValue.value[0];
        switch(bag.type) {
          case pki.oids.pkcs8ShroudedKeyBag:
            if (bagAsn1 = pki.decryptPrivateKeyInfo(bagAsn1, password), null === bagAsn1) {
              throw new Error("Unable to decrypt PKCS#8 ShroudedKeyBag, wrong password?");
            }
          case pki.oids.keyBag:
            try {
              bag.key = pki.privateKeyFromAsn1(bagAsn1);
            } catch (e) {
              /** @type {null} */
              bag.key = null;
              bag.asn1 = bagAsn1;
            }
            continue;
          case pki.oids.certBag:
            type = signerValidator;
            /**
             * @return {undefined}
             */
            decoder = function() {
              if (asn1.derToOid(capture.certId) !== pki.oids.x509Certificate) {
                /** @type {!Error} */
                var error = new Error("Unsupported certificate type, only X.509 supported.");
                throw error.oid = asn1.derToOid(capture.certId), error;
              }
              var certAsn1 = asn1.fromDer(capture.cert, strict);
              try {
                bag.cert = pki.certificateFromAsn1(certAsn1, true);
              } catch (e) {
                /** @type {null} */
                bag.cert = null;
                bag.asn1 = certAsn1;
              }
            };
            break;
          default:
            /** @type {!Error} */
            error = new Error("Unsupported PKCS#12 SafeBag type.");
            throw error.oid = bag.type, error;
        }
        if (void 0 !== type && !asn1.validate(bagAsn1, type, capture, errors)) {
          /** @type {!Error} */
          error = new Error("Cannot read PKCS#12 " + type.name);
          throw error.errors = errors, error;
        }
        decoder();
      }
      return res;
    }
    /**
     * @param {number} attributes
     * @return {?}
     */
    function _decodeBagAttributes(attributes) {
      var decodedAttrs = {};
      if (void 0 !== attributes) {
        /** @type {number} */
        var i = 0;
        for (; i < attributes.length; ++i) {
          var capture = {};
          /** @type {!Array} */
          var errors = [];
          if (!asn1.validate(attributes[i], rsaPublicKeyValidator, capture, errors)) {
            /** @type {!Error} */
            var error = new Error("Cannot read PKCS#12 BagAttribute.");
            throw error.errors = errors, error;
          }
          var oid = asn1.derToOid(capture.oid);
          if (void 0 !== pki.oids[oid]) {
            /** @type {!Array} */
            decodedAttrs[pki.oids[oid]] = [];
            /** @type {number} */
            var i = 0;
            for (; i < capture.values.length; ++i) {
              decodedAttrs[pki.oids[oid]].push(capture.values[i].value);
            }
          }
        }
      }
      return decodedAttrs;
    }
    var forge = FORGE(0);
    FORGE(3);
    FORGE(8);
    FORGE(6);
    FORGE(25);
    FORGE(22);
    FORGE(2);
    FORGE(11);
    FORGE(9);
    FORGE(1);
    FORGE(17);
    var asn1 = forge.asn1;
    var pki = forge.pki;
    var p12 = mixin.exports = forge.pkcs12 = forge.pkcs12 || {};
    var x509CertificateValidator = {
      name : "ContentInfo",
      tagClass : asn1.Class.UNIVERSAL,
      type : asn1.Type.SEQUENCE,
      constructed : true,
      value : [{
        name : "ContentInfo.contentType",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.OID,
        constructed : false,
        capture : "contentType"
      }, {
        name : "ContentInfo.content",
        tagClass : asn1.Class.CONTEXT_SPECIFIC,
        constructed : true,
        captureAsn1 : "content"
      }]
    };
    var privateKeyValidator = {
      name : "PFX",
      tagClass : asn1.Class.UNIVERSAL,
      type : asn1.Type.SEQUENCE,
      constructed : true,
      value : [{
        name : "PFX.version",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.INTEGER,
        constructed : false,
        capture : "version"
      }, x509CertificateValidator, {
        name : "PFX.macData",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.SEQUENCE,
        constructed : true,
        optional : true,
        captureAsn1 : "mac",
        value : [{
          name : "PFX.macData.mac",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.SEQUENCE,
          constructed : true,
          value : [{
            name : "PFX.macData.mac.digestAlgorithm",
            tagClass : asn1.Class.UNIVERSAL,
            type : asn1.Type.SEQUENCE,
            constructed : true,
            value : [{
              name : "PFX.macData.mac.digestAlgorithm.algorithm",
              tagClass : asn1.Class.UNIVERSAL,
              type : asn1.Type.OID,
              constructed : false,
              capture : "macAlgorithm"
            }, {
              name : "PFX.macData.mac.digestAlgorithm.parameters",
              tagClass : asn1.Class.UNIVERSAL,
              captureAsn1 : "macAlgorithmParameters"
            }]
          }, {
            name : "PFX.macData.mac.digest",
            tagClass : asn1.Class.UNIVERSAL,
            type : asn1.Type.OCTETSTRING,
            constructed : false,
            capture : "macDigest"
          }]
        }, {
          name : "PFX.macData.macSalt",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.OCTETSTRING,
          constructed : false,
          capture : "macSalt"
        }, {
          name : "PFX.macData.iterations",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.INTEGER,
          constructed : false,
          optional : true,
          capture : "macIterations"
        }]
      }]
    };
    var rsaPrivateKeyValidator = {
      name : "SafeBag",
      tagClass : asn1.Class.UNIVERSAL,
      type : asn1.Type.SEQUENCE,
      constructed : true,
      value : [{
        name : "SafeBag.bagId",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.OID,
        constructed : false,
        capture : "bagId"
      }, {
        name : "SafeBag.bagValue",
        tagClass : asn1.Class.CONTEXT_SPECIFIC,
        constructed : true,
        captureAsn1 : "bagValue"
      }, {
        name : "SafeBag.bagAttributes",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.SET,
        constructed : true,
        optional : true,
        capture : "bagAttributes"
      }]
    };
    var rsaPublicKeyValidator = {
      name : "Attribute",
      tagClass : asn1.Class.UNIVERSAL,
      type : asn1.Type.SEQUENCE,
      constructed : true,
      value : [{
        name : "Attribute.attrId",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.OID,
        constructed : false,
        capture : "oid"
      }, {
        name : "Attribute.attrValues",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.SET,
        constructed : true,
        capture : "values"
      }]
    };
    var signerValidator = {
      name : "CertBag",
      tagClass : asn1.Class.UNIVERSAL,
      type : asn1.Type.SEQUENCE,
      constructed : true,
      value : [{
        name : "CertBag.certId",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.OID,
        constructed : false,
        capture : "certId"
      }, {
        name : "CertBag.certValue",
        tagClass : asn1.Class.CONTEXT_SPECIFIC,
        constructed : true,
        value : [{
          name : "CertBag.certValue[0]",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Class.OCTETSTRING,
          constructed : false,
          capture : "cert"
        }]
      }]
    };
    /**
     * @param {string} obj
     * @param {number} strict
     * @param {number} password
     * @return {?}
     */
    p12.pkcs12FromAsn1 = function(obj, strict, password) {
      if ("string" == typeof strict) {
        /** @type {number} */
        password = strict;
        /** @type {boolean} */
        strict = true;
      } else {
        if (void 0 === strict) {
          /** @type {boolean} */
          strict = true;
        }
      }
      var capture = {};
      /** @type {!Array} */
      var errors = [];
      if (!asn1.validate(obj, privateKeyValidator, capture, errors)) {
        /** @type {!Error} */
        var error = new Error("Cannot read PKCS#12 PFX. ASN.1 object is not an PKCS#12 PFX.");
        throw error.errors = error, error;
      }
      var pfx = {
        version : capture.version.charCodeAt(0),
        safeContents : [],
        getBags : function(filter) {
          var localKeyId;
          var rval = {};
          return "localKeyId" in filter ? localKeyId = filter.localKeyId : "localKeyIdHex" in filter && (localKeyId = forge.util.hexToBytes(filter.localKeyIdHex)), void 0 === localKeyId && !("friendlyName" in filter) && "bagType" in filter && (rval[filter.bagType] = _getBagsByAttribute(pfx.safeContents, null, null, filter.bagType)), void 0 !== localKeyId && (rval.localKeyId = _getBagsByAttribute(pfx.safeContents, "localKeyId", localKeyId, filter.bagType)), "friendlyName" in filter && (rval.friendlyName = 
          _getBagsByAttribute(pfx.safeContents, "friendlyName", filter.friendlyName, filter.bagType)), rval;
        },
        getBagsByFriendlyName : function(friendlyName, bagType) {
          return _getBagsByAttribute(pfx.safeContents, "friendlyName", friendlyName, bagType);
        },
        getBagsByLocalKeyId : function(localKeyId, bagType) {
          return _getBagsByAttribute(pfx.safeContents, "localKeyId", localKeyId, bagType);
        }
      };
      if (3 !== capture.version.charCodeAt(0)) {
        /** @type {!Error} */
        error = new Error("PKCS#12 PFX of version other than 3 not supported.");
        throw error.version = capture.version.charCodeAt(0), error;
      }
      if (asn1.derToOid(capture.contentType) !== pki.oids.data) {
        /** @type {!Error} */
        error = new Error("Only PKCS#12 PFX in password integrity mode supported.");
        throw error.oid = asn1.derToOid(capture.contentType), error;
      }
      var data = capture.content.value[0];
      if (data.tagClass !== asn1.Class.UNIVERSAL || data.type !== asn1.Type.OCTETSTRING) {
        throw new Error("PKCS#12 authSafe content data is not an OCTET STRING.");
      }
      if (data = _decodePkcs7Data(data), capture.mac) {
        /** @type {null} */
        var md = null;
        /** @type {number} */
        var macKeyBytes = 0;
        var to3 = asn1.derToOid(capture.macAlgorithm);
        switch(to3) {
          case pki.oids.sha1:
            md = forge.md.sha1.create();
            /** @type {number} */
            macKeyBytes = 20;
            break;
          case pki.oids.sha256:
            md = forge.md.sha256.create();
            /** @type {number} */
            macKeyBytes = 32;
            break;
          case pki.oids.sha384:
            md = forge.md.sha384.create();
            /** @type {number} */
            macKeyBytes = 48;
            break;
          case pki.oids.sha512:
            md = forge.md.sha512.create();
            /** @type {number} */
            macKeyBytes = 64;
            break;
          case pki.oids.md5:
            md = forge.md.md5.create();
            /** @type {number} */
            macKeyBytes = 16;
        }
        if (null === md) {
          throw new Error("PKCS#12 uses unsupported MAC algorithm: " + to3);
        }
        var salt = new forge.util.ByteBuffer(capture.macSalt);
        /** @type {number} */
        var count = "macIterations" in capture ? parseInt(forge.util.bytesToHex(capture.macIterations), 16) : 1;
        var key = p12.generateKey(password, salt, 3, count, macKeyBytes, md);
        var mac = forge.hmac.create();
        mac.start(md, key);
        mac.update(data.value);
        var cidToGidStream = mac.getMac();
        if (cidToGidStream.getBytes() !== capture.macDigest) {
          throw new Error("PKCS#12 MAC could not be verified. Invalid password?");
        }
      }
      return _decodeAuthenticatedSafe(pfx, data.value, strict, password), pfx;
    };
    /**
     * @param {!Object} key
     * @param {!Object} cert
     * @param {!Array} password
     * @param {!Object} options
     * @return {?}
     */
    p12.toPkcs12Asn1 = function(key, cert, password, options) {
      options = options || {};
      options.saltSize = options.saltSize || 8;
      options.count = options.count || 2048;
      options.algorithm = options.algorithm || options.encAlgorithm || "aes128";
      if (!("useMac" in options)) {
        /** @type {boolean} */
        options.useMac = true;
      }
      if (!("localKeyId" in options)) {
        /** @type {null} */
        options.localKeyId = null;
      }
      if (!("generateLocalKeyId" in options)) {
        /** @type {boolean} */
        options.generateLocalKeyId = true;
      }
      var bagAttrs;
      var salt = options.localKeyId;
      if (null !== salt) {
        salt = forge.util.hexToBytes(salt);
      } else {
        if (options.generateLocalKeyId) {
          if (cert) {
            var key = forge.util.isArray(cert) ? cert[0] : cert;
            if ("string" == typeof key) {
              key = pki.certificateFromPem(key);
            }
            var md = forge.md.sha1.create();
            md.update(asn1.toDer(pki.certificateToAsn1(key)).getBytes());
            salt = md.digest().getBytes();
          } else {
            salt = forge.random.getBytes(20);
          }
        }
      }
      /** @type {!Array} */
      var attrs = [];
      if (null !== salt) {
        attrs.push(asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(pki.oids.localKeyId).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SET, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OCTETSTRING, false, salt)])]));
      }
      if ("friendlyName" in options) {
        attrs.push(asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(pki.oids.friendlyName).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SET, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.BMPSTRING, false, options.friendlyName)])]));
      }
      if (attrs.length > 0) {
        bagAttrs = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SET, true, attrs);
      }
      /** @type {!Array} */
      var value = [];
      /** @type {!Array} */
      var certs = [];
      if (null !== cert) {
        certs = forge.util.isArray(cert) ? cert : [cert];
      }
      /** @type {!Array} */
      var content = [];
      /** @type {number} */
      var i = 0;
      for (; i < certs.length; ++i) {
        cert = certs[i];
        if ("string" == typeof cert) {
          cert = pki.certificateFromPem(cert);
        }
        var certBagAttrs = 0 === i ? bagAttrs : void 0;
        var value = pki.certificateToAsn1(cert);
        var inline_element = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(pki.oids.certBag).getBytes()), asn1.create(asn1.Class.CONTEXT_SPECIFIC, 0, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(pki.oids.x509Certificate).getBytes()), asn1.create(asn1.Class.CONTEXT_SPECIFIC, 0, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OCTETSTRING, 
        false, asn1.toDer(value).getBytes())])])]), certBagAttrs]);
        content.push(inline_element);
      }
      if (content.length > 0) {
        var safe = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, content);
        var flawed = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(pki.oids.data).getBytes()), asn1.create(asn1.Class.CONTEXT_SPECIFIC, 0, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OCTETSTRING, false, asn1.toDer(safe).getBytes())])]);
        value.push(flawed);
      }
      /** @type {null} */
      var tbsCertificate = null;
      if (null !== key) {
        var pkAsn1 = pki.wrapRsaPrivateKey(pki.privateKeyToAsn1(key));
        tbsCertificate = null === password ? asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(pki.oids.keyBag).getBytes()), asn1.create(asn1.Class.CONTEXT_SPECIFIC, 0, true, [pkAsn1]), bagAttrs]) : asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(pki.oids.pkcs8ShroudedKeyBag).getBytes()), asn1.create(asn1.Class.CONTEXT_SPECIFIC, 0, true, 
        [pki.encryptPrivateKeyInfo(pkAsn1, password, options)]), bagAttrs]);
        var safe = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [tbsCertificate]);
        var flawed = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(pki.oids.data).getBytes()), asn1.create(asn1.Class.CONTEXT_SPECIFIC, 0, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OCTETSTRING, false, asn1.toDer(safe).getBytes())])]);
        value.push(flawed);
      }
      var certBagAttrs;
      var safe = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, value);
      if (options.useMac) {
        md = forge.md.sha1.create();
        var macSalt = new forge.util.ByteBuffer(forge.random.getBytes(options.saltSize));
        var count = options.count;
        key = p12.generateKey(password, macSalt, 3, count, 20);
        var mac = forge.hmac.create();
        mac.start(md, key);
        mac.update(asn1.toDer(safe).getBytes());
        var cidToGidStream = mac.getMac();
        certBagAttrs = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(pki.oids.sha1).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.NULL, false, "")]), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OCTETSTRING, false, cidToGidStream.getBytes())]), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OCTETSTRING, 
        false, macSalt.getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, asn1.integerToDer(count).getBytes())]);
      }
      return asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, asn1.integerToDer(3).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(pki.oids.data).getBytes()), asn1.create(asn1.Class.CONTEXT_SPECIFIC, 0, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OCTETSTRING, false, asn1.toDer(safe).getBytes())])]), certBagAttrs]);
    };
    p12.generateKey = forge.pbe.generatePkcs12Key;
  }, function(mixin, canCreateDiscussions, FORGE) {
    var forge = FORGE(0);
    FORGE(3);
    FORGE(1);
    var asn1 = forge.asn1;
    var p7v = mixin.exports = forge.pkcs7asn1 = forge.pkcs7asn1 || {};
    forge.pkcs7 = forge.pkcs7 || {};
    forge.pkcs7.asn1 = p7v;
    var contentInfoValidator = {
      name : "ContentInfo",
      tagClass : asn1.Class.UNIVERSAL,
      type : asn1.Type.SEQUENCE,
      constructed : true,
      value : [{
        name : "ContentInfo.ContentType",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.OID,
        constructed : false,
        capture : "contentType"
      }, {
        name : "ContentInfo.content",
        tagClass : asn1.Class.CONTEXT_SPECIFIC,
        type : 0,
        constructed : true,
        optional : true,
        captureAsn1 : "content"
      }]
    };
    p7v.contentInfoValidator = contentInfoValidator;
    var encryptedContentInfoValidator = {
      name : "EncryptedContentInfo",
      tagClass : asn1.Class.UNIVERSAL,
      type : asn1.Type.SEQUENCE,
      constructed : true,
      value : [{
        name : "EncryptedContentInfo.contentType",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.OID,
        constructed : false,
        capture : "contentType"
      }, {
        name : "EncryptedContentInfo.contentEncryptionAlgorithm",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.SEQUENCE,
        constructed : true,
        value : [{
          name : "EncryptedContentInfo.contentEncryptionAlgorithm.algorithm",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.OID,
          constructed : false,
          capture : "encAlgorithm"
        }, {
          name : "EncryptedContentInfo.contentEncryptionAlgorithm.parameter",
          tagClass : asn1.Class.UNIVERSAL,
          captureAsn1 : "encParameter"
        }]
      }, {
        name : "EncryptedContentInfo.encryptedContent",
        tagClass : asn1.Class.CONTEXT_SPECIFIC,
        type : 0,
        capture : "encryptedContent",
        captureAsn1 : "encryptedContentAsn1"
      }]
    };
    p7v.envelopedDataValidator = {
      name : "EnvelopedData",
      tagClass : asn1.Class.UNIVERSAL,
      type : asn1.Type.SEQUENCE,
      constructed : true,
      value : [{
        name : "EnvelopedData.Version",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.INTEGER,
        constructed : false,
        capture : "version"
      }, {
        name : "EnvelopedData.RecipientInfos",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.SET,
        constructed : true,
        captureAsn1 : "recipientInfos"
      }].concat(encryptedContentInfoValidator)
    };
    p7v.encryptedDataValidator = {
      name : "EncryptedData",
      tagClass : asn1.Class.UNIVERSAL,
      type : asn1.Type.SEQUENCE,
      constructed : true,
      value : [{
        name : "EncryptedData.Version",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.INTEGER,
        constructed : false,
        capture : "version"
      }].concat(encryptedContentInfoValidator)
    };
    var signerValidator = {
      name : "SignerInfo",
      tagClass : asn1.Class.UNIVERSAL,
      type : asn1.Type.SEQUENCE,
      constructed : true,
      value : [{
        name : "SignerInfo.version",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.INTEGER,
        constructed : false
      }, {
        name : "SignerInfo.issuerAndSerialNumber",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.SEQUENCE,
        constructed : true,
        value : [{
          name : "SignerInfo.issuerAndSerialNumber.issuer",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.SEQUENCE,
          constructed : true,
          captureAsn1 : "issuer"
        }, {
          name : "SignerInfo.issuerAndSerialNumber.serialNumber",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.INTEGER,
          constructed : false,
          capture : "serial"
        }]
      }, {
        name : "SignerInfo.digestAlgorithm",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.SEQUENCE,
        constructed : true,
        value : [{
          name : "SignerInfo.digestAlgorithm.algorithm",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.OID,
          constructed : false,
          capture : "digestAlgorithm"
        }, {
          name : "SignerInfo.digestAlgorithm.parameter",
          tagClass : asn1.Class.UNIVERSAL,
          constructed : false,
          captureAsn1 : "digestParameter",
          optional : true
        }]
      }, {
        name : "SignerInfo.authenticatedAttributes",
        tagClass : asn1.Class.CONTEXT_SPECIFIC,
        type : 0,
        constructed : true,
        optional : true,
        capture : "authenticatedAttributes"
      }, {
        name : "SignerInfo.digestEncryptionAlgorithm",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.SEQUENCE,
        constructed : true,
        capture : "signatureAlgorithm"
      }, {
        name : "SignerInfo.encryptedDigest",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.OCTETSTRING,
        constructed : false,
        capture : "signature"
      }, {
        name : "SignerInfo.unauthenticatedAttributes",
        tagClass : asn1.Class.CONTEXT_SPECIFIC,
        type : 1,
        constructed : true,
        optional : true,
        capture : "unauthenticatedAttributes"
      }]
    };
    p7v.signedDataValidator = {
      name : "SignedData",
      tagClass : asn1.Class.UNIVERSAL,
      type : asn1.Type.SEQUENCE,
      constructed : true,
      value : [{
        name : "SignedData.Version",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.INTEGER,
        constructed : false,
        capture : "version"
      }, {
        name : "SignedData.DigestAlgorithms",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.SET,
        constructed : true,
        captureAsn1 : "digestAlgorithms"
      }, contentInfoValidator, {
        name : "SignedData.Certificates",
        tagClass : asn1.Class.CONTEXT_SPECIFIC,
        type : 0,
        optional : true,
        captureAsn1 : "certificates"
      }, {
        name : "SignedData.CertificateRevocationLists",
        tagClass : asn1.Class.CONTEXT_SPECIFIC,
        type : 1,
        optional : true,
        captureAsn1 : "crls"
      }, {
        name : "SignedData.SignerInfos",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.SET,
        capture : "signerInfos",
        optional : true,
        value : [signerValidator]
      }]
    };
    p7v.recipientInfoValidator = {
      name : "RecipientInfo",
      tagClass : asn1.Class.UNIVERSAL,
      type : asn1.Type.SEQUENCE,
      constructed : true,
      value : [{
        name : "RecipientInfo.version",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.INTEGER,
        constructed : false,
        capture : "version"
      }, {
        name : "RecipientInfo.issuerAndSerial",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.SEQUENCE,
        constructed : true,
        value : [{
          name : "RecipientInfo.issuerAndSerial.issuer",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.SEQUENCE,
          constructed : true,
          captureAsn1 : "issuer"
        }, {
          name : "RecipientInfo.issuerAndSerial.serialNumber",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.INTEGER,
          constructed : false,
          capture : "serial"
        }]
      }, {
        name : "RecipientInfo.keyEncryptionAlgorithm",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.SEQUENCE,
        constructed : true,
        value : [{
          name : "RecipientInfo.keyEncryptionAlgorithm.algorithm",
          tagClass : asn1.Class.UNIVERSAL,
          type : asn1.Type.OID,
          constructed : false,
          capture : "encAlgorithm"
        }, {
          name : "RecipientInfo.keyEncryptionAlgorithm.parameter",
          tagClass : asn1.Class.UNIVERSAL,
          constructed : false,
          captureAsn1 : "encParameter"
        }]
      }, {
        name : "RecipientInfo.encryptedKey",
        tagClass : asn1.Class.UNIVERSAL,
        type : asn1.Type.OCTETSTRING,
        constructed : false,
        capture : "encKey"
      }]
    };
  }, function(mixin, canCreateDiscussions, FORGE) {
    var forge = FORGE(0);
    FORGE(3);
    FORGE(6);
    FORGE(22);
    FORGE(7);
    FORGE(15);
    FORGE(24);
    FORGE(16);
    FORGE(11);
    FORGE(1);
    FORGE(17);
    var asn1 = forge.asn1;
    var pki = mixin.exports = forge.pki = forge.pki || {};
    /**
     * @param {?} e
     * @return {?}
     */
    pki.pemToDer = function(e) {
      var msg = forge.pem.decode(e)[0];
      if (msg.procType && "ENCRYPTED" === msg.procType.type) {
        throw new Error("Could not convert PEM to DER; PEM is encrypted.");
      }
      return forge.util.createBuffer(msg.body);
    };
    /**
     * @param {?} key
     * @return {?}
     */
    pki.privateKeyFromPem = function(key) {
      var msg = forge.pem.decode(key)[0];
      if ("PRIVATE KEY" !== msg.type && "RSA PRIVATE KEY" !== msg.type) {
        /** @type {!Error} */
        var error = new Error('Could not convert private key from PEM; PEM header type is not "PRIVATE KEY" or "RSA PRIVATE KEY".');
        throw error.headerType = msg.type, error;
      }
      if (msg.procType && "ENCRYPTED" === msg.procType.type) {
        throw new Error("Could not convert private key from PEM; PEM is encrypted.");
      }
      var rval = asn1.fromDer(msg.body);
      return pki.privateKeyFromAsn1(rval);
    };
    /**
     * @param {?} key
     * @param {?} maxline
     * @return {?}
     */
    pki.privateKeyToPem = function(key, maxline) {
      var r = {
        type : "RSA PRIVATE KEY",
        body : asn1.toDer(pki.privateKeyToAsn1(key)).getBytes()
      };
      return forge.pem.encode(r, {
        maxline : maxline
      });
    };
    /**
     * @param {string} pki
     * @param {?} maxline
     * @return {?}
     */
    pki.privateKeyInfoToPem = function(pki, maxline) {
      var r = {
        type : "PRIVATE KEY",
        body : asn1.toDer(pki).getBytes()
      };
      return forge.pem.encode(r, {
        maxline : maxline
      });
    };
  }, function(module, canCreateDiscussions, FORGE) {
    var forge = FORGE(0);
    FORGE(1);
    FORGE(13);
    FORGE(2);
    (function() {
      /**
       * @param {undefined} bits
       * @param {undefined} rng
       * @param {!Object} options
       * @param {!Object} callback
       * @return {?}
       */
      function primeincFindPrime(bits, rng, options, callback) {
        return "workers" in options ? primeincFindPrimeWithWorkers(bits, rng, options, callback) : primeincFindPrimeWithoutWorkers(bits, rng, options, callback);
      }
      /**
       * @param {number} bits
       * @param {number} rng
       * @param {!Object} options
       * @param {!Object} callback
       * @return {undefined}
       */
      function primeincFindPrimeWithoutWorkers(bits, rng, options, callback) {
        var num = generateRandom(bits, rng);
        /** @type {number} */
        var deltaIdx = 0;
        var mrTests = getMillerRabinTests(num.bitLength());
        if ("millerRabinTests" in options) {
          mrTests = options.millerRabinTests;
        }
        /** @type {number} */
        var maxBlockTime = 10;
        if ("maxBlockTime" in options) {
          maxBlockTime = options.maxBlockTime;
        }
        _primeinc(num, bits, rng, deltaIdx, mrTests, maxBlockTime, callback);
      }
      /**
       * @param {?} num
       * @param {number} bits
       * @param {number} rng
       * @param {number} deltaIdx
       * @param {number} mrTests
       * @param {number} maxBlockTime
       * @param {!Object} callback
       * @return {?}
       */
      function _primeinc(num, bits, rng, deltaIdx, mrTests, maxBlockTime, callback) {
        /** @type {number} */
        var start = +new Date;
        do {
          if (num.bitLength() > bits && (num = generateRandom(bits, rng)), num.isProbablePrime(mrTests)) {
            return callback(null, num);
          }
          num.dAddOffset(GCD_30_DELTA[deltaIdx++ % 8], 0);
        } while (maxBlockTime < 0 || +new Date - start < maxBlockTime);
        forge.util.setImmediate(function() {
          _primeinc(num, bits, rng, deltaIdx, mrTests, maxBlockTime, callback);
        });
      }
      /**
       * @param {number} bits
       * @param {number} rng
       * @param {!Object} options
       * @param {!Object} callback
       * @return {?}
       */
      function primeincFindPrimeWithWorkers(bits, rng, options, callback) {
        /**
         * @return {undefined}
         */
        function generate() {
          /**
           * @param {!Object} event
           * @return {?}
           */
          function workerMessage(event) {
            if (!done) {
              --running;
              var data = event.data;
              if (data.found) {
                /** @type {number} */
                var i = 0;
                for (; i < workers.length; ++i) {
                  workers[i].terminate();
                }
                return done = true, callback(null, new BigInteger(data.prime, 16));
              }
              if (num.bitLength() > bits) {
                num = generateRandom(bits, rng);
              }
              var original = num.toString(16);
              event.target.postMessage({
                hex : original,
                workLoad : workLoad
              });
              num.dAddOffset(range, 0);
            }
          }
          /** @type {number} */
          numWorkers = Math.max(1, numWorkers);
          /** @type {!Array} */
          var workers = [];
          /** @type {number} */
          var i = 0;
          for (; i < numWorkers; ++i) {
            /** @type {!Worker} */
            workers[i] = new Worker(workerScript);
          }
          /** @type {number} */
          var running = numWorkers;
          /** @type {number} */
          i = 0;
          for (; i < numWorkers; ++i) {
            workers[i].addEventListener("message", workerMessage);
          }
          /** @type {boolean} */
          var done = false;
        }
        if ("undefined" == typeof Worker) {
          return primeincFindPrimeWithoutWorkers(bits, rng, options, callback);
        }
        var num = generateRandom(bits, rng);
        var numWorkers = options.workers;
        var workLoad = options.workLoad || 100;
        /** @type {number} */
        var range = 30 * workLoad / 8;
        var workerScript = options.workerScript || "forge/prime.worker.js";
        return numWorkers === -1 ? forge.util.estimateCores(function(canCreateDiscussions, cores) {
          if (canCreateDiscussions) {
            /** @type {number} */
            cores = 2;
          }
          /** @type {number} */
          numWorkers = cores - 1;
          generate();
        }) : void generate();
      }
      /**
       * @param {number} bits
       * @param {number} rng
       * @return {?}
       */
      function generateRandom(bits, rng) {
        var num = new BigInteger(bits, rng);
        /** @type {number} */
        var bits1 = bits - 1;
        return num.testBit(bits1) || num.bitwiseTo(BigInteger.ONE.shiftLeft(bits1), m, num), num.dAddOffset(31 - num.mod(THIRTY).byteValue(), 0), num;
      }
      /**
       * @param {number} bits
       * @return {?}
       */
      function getMillerRabinTests(bits) {
        return bits <= 100 ? 27 : bits <= 150 ? 18 : bits <= 200 ? 15 : bits <= 250 ? 12 : bits <= 300 ? 9 : bits <= 350 ? 8 : bits <= 400 ? 7 : bits <= 500 ? 6 : bits <= 600 ? 5 : bits <= 800 ? 4 : bits <= 1250 ? 3 : 2;
      }
      if (forge.prime) {
        return void(module.exports = forge.prime);
      }
      var prime = module.exports = forge.prime = forge.prime || {};
      var BigInteger = forge.jsbn.BigInteger;
      /** @type {!Array} */
      var GCD_30_DELTA = [6, 4, 2, 4, 2, 4, 6, 2];
      var THIRTY = new BigInteger(null);
      THIRTY.fromInt(30);
      /**
       * @param {number} mask
       * @param {number} value
       * @return {?}
       */
      var m = function(mask, value) {
        return mask | value;
      };
      /**
       * @param {?} bits
       * @param {!Object} options
       * @param {!Object} callback
       * @return {?}
       */
      prime.generateProbablePrime = function(bits, options, callback) {
        if ("function" == typeof options) {
          /** @type {!Object} */
          callback = options;
          options = {};
        }
        options = options || {};
        var algorithm = options.algorithm || "PRIMEINC";
        if ("string" == typeof algorithm) {
          algorithm = {
            name : algorithm
          };
        }
        algorithm.options = algorithm.options || {};
        var prng = options.prng || forge.random;
        var rng = {
          nextBytes : function(x) {
            var stripSlashes = prng.getBytesSync(x.length);
            /** @type {number} */
            var i = 0;
            for (; i < x.length; ++i) {
              x[i] = stripSlashes.charCodeAt(i);
            }
          }
        };
        if ("PRIMEINC" === algorithm.name) {
          return primeincFindPrime(bits, rng, algorithm.options, callback);
        }
        throw new Error("Invalid prime generation algorithm: " + algorithm.name);
      };
    })();
  }, function(mixin, canCreateDiscussions, require) {
    var forge = require(0);
    require(1);
    /** @type {null} */
    var crypto = null;
    if (!(!forge.util.isNodejs || forge.options.usePureJavaScript || process.versions["node-webkit"])) {
      crypto = require(32);
    }
    var directory_epub = mixin.exports = forge.prng = forge.prng || {};
    /**
     * @param {!Object} algorithm
     * @return {?}
     */
    directory_epub.create = function(algorithm) {
      /**
       * @param {!Function} callback
       * @return {?}
       */
      function _reseed(callback) {
        if (ctx.pools[0].messageLength >= 32) {
          return _seed(), callback();
        }
        /** @type {number} */
        var needed = 32 - ctx.pools[0].messageLength << 5;
        ctx.seedFile(needed, function(err, functions) {
          return err ? callback(err) : (ctx.collect(functions), _seed(), void callback());
        });
      }
      /**
       * @return {?}
       */
      function _reseedSync() {
        if (ctx.pools[0].messageLength >= 32) {
          return _seed();
        }
        /** @type {number} */
        var needed = 32 - ctx.pools[0].messageLength << 5;
        ctx.collect(ctx.seedFileSync(needed));
        _seed();
      }
      /**
       * @return {undefined}
       */
      function _seed() {
        var layer = ctx.plugin.md.create();
        layer.update(ctx.pools[0].digest().getBytes());
        ctx.pools[0].start();
        /** @type {number} */
        var k = 1;
        /** @type {number} */
        var name = 1;
        for (; name < 32; ++name) {
          /** @type {number} */
          k = 31 === k ? 2147483648 : k << 2;
          if (k % ctx.reseeds === 0) {
            layer.update(ctx.pools[name].digest().getBytes());
            ctx.pools[name].start();
          }
        }
        var i = layer.digest().getBytes();
        layer.start();
        layer.update(i);
        var seedBytes = layer.digest().getBytes();
        ctx.key = ctx.plugin.formatKey(i);
        ctx.seed = ctx.plugin.formatSeed(seedBytes);
        /** @type {number} */
        ctx.reseeds = 4294967295 === ctx.reseeds ? 0 : ctx.reseeds + 1;
        /** @type {number} */
        ctx.generated = 0;
      }
      /**
       * @param {number} needed
       * @return {?}
       */
      function defaultSeedFile(needed) {
        /** @type {null} */
        var getRandomValues = null;
        if ("undefined" != typeof window) {
          var _crypto = window.crypto || window.msCrypto;
          if (_crypto && _crypto.getRandomValues) {
            /**
             * @param {(ArrayBufferView|number)} arr
             * @return {?}
             */
            getRandomValues = function(arr) {
              return _crypto.getRandomValues(arr);
            };
          }
        }
        var b = forge.util.createBuffer();
        if (getRandomValues) {
          for (; b.length() < needed;) {
            /** @type {number} */
            var count = Math.max(1, Math.min(needed - b.length(), 65536) / 4);
            /** @type {!Uint32Array} */
            var entropy = new Uint32Array(Math.floor(count));
            try {
              getRandomValues(entropy);
              /** @type {number} */
              var i = 0;
              for (; i < entropy.length; ++i) {
                b.putInt32(entropy[i]);
              }
            } catch (e) {
              if (!("undefined" != typeof QuotaExceededError && e instanceof QuotaExceededError)) {
                throw e;
              }
            }
          }
        }
        if (b.length() < needed) {
          var numPrimitives;
          var numTotalPrimitives;
          var mask;
          /** @type {number} */
          var seed = Math.floor(65536 * Math.random());
          for (; b.length() < needed;) {
            /** @type {number} */
            numTotalPrimitives = 16807 * (65535 & seed);
            /** @type {number} */
            numPrimitives = 16807 * (seed >> 16);
            /** @type {number} */
            numTotalPrimitives = numTotalPrimitives + ((32767 & numPrimitives) << 16);
            /** @type {number} */
            numTotalPrimitives = numTotalPrimitives + (numPrimitives >> 15);
            /** @type {number} */
            numTotalPrimitives = (2147483647 & numTotalPrimitives) + (numTotalPrimitives >> 31);
            /** @type {number} */
            seed = 4294967295 & numTotalPrimitives;
            /** @type {number} */
            i = 0;
            for (; i < 3; ++i) {
              /** @type {number} */
              mask = seed >>> (i << 3);
              /** @type {number} */
              mask = mask ^ Math.floor(256 * Math.random());
              b.putByte(String.fromCharCode(255 & mask));
            }
          }
        }
        return b.getBytes(needed);
      }
      var ctx = {
        plugin : algorithm,
        key : null,
        seed : null,
        time : null,
        reseeds : 0,
        generated : 0
      };
      var md = algorithm.md;
      /** @type {!Array} */
      var pools = new Array(32);
      /** @type {number} */
      var id = 0;
      for (; id < 32; ++id) {
        pools[id] = md.create();
      }
      return ctx.pools = pools, ctx.pool = 0, ctx.generate = function(i, callback) {
        /**
         * @param {?} start_string
         * @return {?}
         */
        function generate(start_string) {
          if (start_string) {
            return callback(start_string);
          }
          if (b.length() >= i) {
            return callback(null, b.getBytes(i));
          }
          if (ctx.generated > 1048575 && (ctx.key = null), null === ctx.key) {
            return forge.util.nextTick(function() {
              _reseed(generate);
            });
          }
          var bytes = cipher(ctx.key, ctx.seed);
          ctx.generated += bytes.length;
          b.putBytes(bytes);
          ctx.key = formatKey(cipher(ctx.key, increment(ctx.seed)));
          ctx.seed = formatSeed(cipher(ctx.key, ctx.seed));
          forge.util.setImmediate(generate);
        }
        if (!callback) {
          return ctx.generateSync(i);
        }
        var cipher = ctx.plugin.cipher;
        var increment = ctx.plugin.increment;
        var formatKey = ctx.plugin.formatKey;
        var formatSeed = ctx.plugin.formatSeed;
        var b = forge.util.createBuffer();
        /** @type {null} */
        ctx.key = null;
        generate();
      }, ctx.generateSync = function(count) {
        var cipher = ctx.plugin.cipher;
        var increment = ctx.plugin.increment;
        var formatKey = ctx.plugin.formatKey;
        var formatSeed = ctx.plugin.formatSeed;
        /** @type {null} */
        ctx.key = null;
        var buffer = forge.util.createBuffer();
        for (; buffer.length() < count;) {
          if (ctx.generated > 1048575) {
            /** @type {null} */
            ctx.key = null;
          }
          if (null === ctx.key) {
            _reseedSync();
          }
          var bytes = cipher(ctx.key, ctx.seed);
          ctx.generated += bytes.length;
          buffer.putBytes(bytes);
          ctx.key = formatKey(cipher(ctx.key, increment(ctx.seed)));
          ctx.seed = formatSeed(cipher(ctx.key, ctx.seed));
        }
        return buffer.getBytes(count);
      }, crypto ? (ctx.seedFile = function(needed, callback) {
        crypto.randomBytes(needed, function(err, pingErr) {
          return err ? callback(err) : void callback(null, pingErr.toString());
        });
      }, ctx.seedFileSync = function(needed) {
        return crypto.randomBytes(needed).toString();
      }) : (ctx.seedFile = function(needed, callback) {
        try {
          callback(null, defaultSeedFile(needed));
        } catch (identifierPositions) {
          callback(identifierPositions);
        }
      }, ctx.seedFileSync = defaultSeedFile), ctx.collect = function(name) {
        var length = name.length;
        /** @type {number} */
        var i = 0;
        for (; i < length; ++i) {
          ctx.pools[ctx.pool].update(name.substr(i, 1));
          ctx.pool = 31 === ctx.pool ? 0 : ctx.pool + 1;
        }
      }, ctx.collectInt = function(i, n) {
        /** @type {string} */
        var table = "";
        /** @type {number} */
        var j = 0;
        for (; j < n; j = j + 8) {
          /** @type {string} */
          table = table + String.fromCharCode(i >> j & 255);
        }
        ctx.collect(table);
      }, ctx.registerWorker = function(worker) {
        if (worker === self) {
          /**
           * @param {number} needed
           * @param {!Function} callback
           * @return {undefined}
           */
          ctx.seedFile = function(needed, callback) {
            /**
             * @param {!Object} event
             * @return {undefined}
             */
            function listener(event) {
              var data = event.data;
              if (data.forge && data.forge.prng) {
                self.removeEventListener("message", listener);
                callback(data.forge.prng.err, data.forge.prng.bytes);
              }
            }
            self.addEventListener("message", listener);
            self.postMessage({
              forge : {
                prng : {
                  needed : needed
                }
              }
            });
          };
        } else {
          /**
           * @param {!Object} event
           * @return {undefined}
           */
          var listener = function(event) {
            var data = event.data;
            if (data.forge && data.forge.prng) {
              ctx.seedFile(data.forge.prng.needed, function(error, b) {
                worker.postMessage({
                  forge : {
                    prng : {
                      err : error,
                      bytes : b
                    }
                  }
                });
              });
            }
          };
          worker.addEventListener("message", listener);
        }
      }, ctx;
    };
  }, function(mixin, canCreateDiscussions, FORGE) {
    var forge = FORGE(0);
    FORGE(1);
    /** @type {!Array} */
    var piTable = [217, 120, 249, 196, 25, 221, 181, 237, 40, 233, 253, 121, 74, 160, 216, 157, 198, 126, 55, 131, 43, 118, 83, 142, 98, 76, 100, 136, 68, 139, 251, 162, 23, 154, 89, 245, 135, 179, 79, 19, 97, 69, 109, 141, 9, 129, 125, 50, 189, 143, 64, 235, 134, 183, 123, 11, 240, 149, 33, 34, 92, 107, 78, 130, 84, 214, 101, 147, 206, 96, 178, 28, 115, 86, 192, 20, 167, 140, 241, 220, 18, 117, 202, 31, 59, 190, 228, 209, 66, 61, 212, 48, 163, 60, 182, 38, 111, 191, 14, 218, 70, 105, 7, 87, 39, 
    242, 29, 155, 188, 148, 67, 3, 248, 17, 199, 246, 144, 239, 62, 231, 6, 195, 213, 47, 200, 102, 30, 215, 8, 232, 234, 222, 128, 82, 238, 247, 132, 170, 114, 172, 53, 77, 106, 42, 150, 26, 210, 113, 90, 21, 73, 116, 75, 159, 208, 94, 4, 24, 164, 236, 194, 224, 65, 110, 15, 81, 203, 204, 36, 145, 175, 80, 161, 244, 112, 57, 153, 124, 58, 133, 35, 184, 180, 122, 252, 2, 54, 91, 37, 85, 151, 49, 45, 93, 250, 152, 227, 138, 146, 174, 5, 223, 41, 16, 103, 108, 186, 201, 211, 0, 230, 207, 225, 158, 
    168, 44, 99, 22, 1, 63, 88, 226, 137, 169, 13, 56, 52, 27, 171, 51, 255, 176, 187, 72, 12, 95, 185, 177, 205, 46, 197, 243, 219, 71, 229, 165, 156, 119, 10, 166, 32, 104, 254, 127, 193, 173];
    /** @type {!Array} */
    var s = [1, 2, 3, 5];
    /**
     * @param {number} word
     * @param {number} bits
     * @return {?}
     */
    var rol = function(word, bits) {
      return word << bits & 65535 | (65535 & word) >> 16 - bits;
    };
    /**
     * @param {number} word
     * @param {number} bits
     * @return {?}
     */
    var ror = function(word, bits) {
      return (65535 & word) >> bits | word << 16 - bits & 65535;
    };
    mixin.exports = forge.rc2 = forge.rc2 || {};
    /**
     * @param {string} key
     * @param {number} effKeyBits
     * @return {?}
     */
    forge.rc2.expandKey = function(key, effKeyBits) {
      if ("string" == typeof key) {
        key = forge.util.createBuffer(key);
      }
      effKeyBits = effKeyBits || 128;
      var i;
      /** @type {string} */
      var L = key;
      var T = key.length();
      /** @type {number} */
      var T1 = effKeyBits;
      /** @type {number} */
      var T8 = Math.ceil(T1 / 8);
      /** @type {number} */
      var TM = 255 >> (7 & T1);
      i = T;
      for (; i < 128; i++) {
        L.putByte(piTable[L.at(i - 1) + L.at(i - T) & 255]);
      }
      L.setAt(128 - T8, piTable[L.at(128 - T8) & TM]);
      /** @type {number} */
      i = 127 - T8;
      for (; i >= 0; i--) {
        L.setAt(i, piTable[L.at(i + 1) ^ L.at(i + T8)]);
      }
      return L;
    };
    /**
     * @param {string} key
     * @param {number} bits
     * @param {boolean} encrypt
     * @return {?}
     */
    var createCipher = function(key, bits, encrypt) {
      var mixRound;
      var mashRound;
      var i;
      var j;
      /** @type {boolean} */
      var p = false;
      /** @type {null} */
      var _input = null;
      /** @type {null} */
      var _output = null;
      /** @type {null} */
      var _iv = null;
      /** @type {!Array} */
      var K = [];
      key = forge.rc2.expandKey(key, bits);
      /** @type {number} */
      i = 0;
      for (; i < 64; i++) {
        K.push(key.getInt16Le());
      }
      if (encrypt) {
        /**
         * @param {!Object} R
         * @return {undefined}
         */
        mixRound = function(R) {
          /** @type {number} */
          i = 0;
          for (; i < 4; i++) {
            R[i] += K[j] + (R[(i + 3) % 4] & R[(i + 2) % 4]) + (~R[(i + 3) % 4] & R[(i + 1) % 4]);
            R[i] = rol(R[i], s[i]);
            j++;
          }
        };
        /**
         * @param {!Array} R
         * @return {undefined}
         */
        mashRound = function(R) {
          /** @type {number} */
          i = 0;
          for (; i < 4; i++) {
            R[i] += K[63 & R[(i + 3) % 4]];
          }
        };
      } else {
        /**
         * @param {!Object} R
         * @return {undefined}
         */
        mixRound = function(R) {
          /** @type {number} */
          i = 3;
          for (; i >= 0; i--) {
            R[i] = ror(R[i], s[i]);
            R[i] -= K[j] + (R[(i + 3) % 4] & R[(i + 2) % 4]) + (~R[(i + 3) % 4] & R[(i + 1) % 4]);
            j--;
          }
        };
        /**
         * @param {!Array} R
         * @return {undefined}
         */
        mashRound = function(R) {
          /** @type {number} */
          i = 3;
          for (; i >= 0; i--) {
            R[i] -= K[63 & R[(i + 3) % 4]];
          }
        };
      }
      /**
       * @param {!Array} plan
       * @return {undefined}
       */
      var runPlan = function(plan) {
        /** @type {!Array} */
        var R = [];
        /** @type {number} */
        i = 0;
        for (; i < 4; i++) {
          var val = _input.getInt16Le();
          if (null !== _iv) {
            if (encrypt) {
              /** @type {number} */
              val = val ^ _iv.getInt16Le();
            } else {
              _iv.putInt16Le(val);
            }
          }
          R.push(65535 & val);
        }
        /** @type {number} */
        j = encrypt ? 0 : 63;
        /** @type {number} */
        var ptr = 0;
        for (; ptr < plan.length; ptr++) {
          /** @type {number} */
          var i = 0;
          for (; i < plan[ptr][0]; i++) {
            plan[ptr][1](R);
          }
        }
        /** @type {number} */
        i = 0;
        for (; i < 4; i++) {
          if (null !== _iv) {
            if (encrypt) {
              _iv.putInt16Le(R[i]);
            } else {
              R[i] ^= _iv.getInt16Le();
            }
          }
          _output.putInt16Le(R[i]);
        }
      };
      /** @type {null} */
      var cipher = null;
      return cipher = {
        start : function(fn, key) {
          if (fn && "string" == typeof fn) {
            fn = forge.util.createBuffer(fn);
          }
          /** @type {boolean} */
          p = false;
          _input = forge.util.createBuffer();
          _output = key || new forge.util.createBuffer;
          /** @type {string} */
          _iv = fn;
          cipher.output = _output;
        },
        update : function(input) {
          if (!p) {
            _input.putBuffer(input);
          }
          for (; _input.length() >= 8;) {
            runPlan([[5, mixRound], [1, mashRound], [6, mixRound], [1, mashRound], [5, mixRound]]);
          }
        },
        finish : function(pad) {
          /** @type {boolean} */
          var rval = true;
          if (encrypt) {
            if (pad) {
              rval = pad(8, _input, !encrypt);
            } else {
              /** @type {number} */
              var padding = 8 === _input.length() ? 8 : 8 - _input.length();
              _input.fillWithByte(padding, padding);
            }
          }
          if (rval && (p = true, cipher.update()), !encrypt && (rval = 0 === _input.length())) {
            if (pad) {
              rval = pad(8, _output, !encrypt);
            } else {
              var len = _output.length();
              var count = _output.at(len - 1);
              if (count > len) {
                /** @type {boolean} */
                rval = false;
              } else {
                _output.truncate(count);
              }
            }
          }
          return rval;
        }
      };
    };
    /**
     * @param {undefined} key
     * @param {undefined} x
     * @param {!Object} y
     * @return {?}
     */
    forge.rc2.startEncrypting = function(key, x, y) {
      var point = forge.rc2.createEncryptionCipher(key, 128);
      return point.start(x, y), point;
    };
    /**
     * @param {!Function} key
     * @param {?} bits
     * @return {?}
     */
    forge.rc2.createEncryptionCipher = function(key, bits) {
      return createCipher(key, bits, true);
    };
    /**
     * @param {string} key
     * @param {undefined} callback
     * @param {!Object} e
     * @return {?}
     */
    forge.rc2.startDecrypting = function(key, callback, e) {
      var t = forge.rc2.createDecryptionCipher(key, 128);
      return t.start(callback, e), t;
    };
    /**
     * @param {string} key
     * @param {number} bits
     * @return {?}
     */
    forge.rc2.createDecryptionCipher = function(key, bits) {
      return createCipher(key, bits, false);
    };
  }, function(mixin, canCreateDiscussions, FORGE) {
    /**
     * @return {undefined}
     */
    function _init() {
      /** @type {string} */
      _padding = String.fromCharCode(128);
      _padding = _padding + forge.util.fillString(String.fromCharCode(0), 64);
      /** @type {!Array} */
      segs = [1116352408, 1899447441, 3049323471, 3921009573, 961987163, 1508970993, 2453635748, 2870763221, 3624381080, 310598401, 607225278, 1426881987, 1925078388, 2162078206, 2614888103, 3248222580, 3835390401, 4022224774, 264347078, 604807628, 770255983, 1249150122, 1555081692, 1996064986, 2554220882, 2821834349, 2952996808, 3210313671, 3336571891, 3584528711, 113926993, 338241895, 666307205, 773529912, 1294757372, 1396182291, 1695183700, 1986661051, 2177026350, 2456956037, 2730485921, 2820302411, 
      3259730800, 3345764771, 3516065817, 3600352804, 4094571909, 275423344, 430227734, 506948616, 659060556, 883997877, 958139571, 1322822218, 1537002063, 1747873779, 1955562222, 2024104815, 2227730452, 2361852424, 2428436474, 2756734187, 3204031479, 3329325298];
      /** @type {boolean} */
      c = true;
    }
    /**
     * @param {!Object} s
     * @param {!Object} list
     * @param {!NodeList} bytes
     * @return {undefined}
     */
    function _update(s, list, bytes) {
      var name;
      var op;
      var opbeg;
      var sigma1;
      var ch;
      var preCopy;
      var i;
      var a;
      var b;
      var c;
      var d;
      var e;
      var f;
      var g;
      var h;
      var val = bytes.length();
      for (; val >= 64;) {
        /** @type {number} */
        i = 0;
        for (; i < 16; ++i) {
          list[i] = bytes.getInt32();
        }
        for (; i < 64; ++i) {
          name = list[i - 2];
          /** @type {number} */
          name = (name >>> 17 | name << 15) ^ (name >>> 19 | name << 13) ^ name >>> 10;
          op = list[i - 15];
          /** @type {number} */
          op = (op >>> 7 | op << 25) ^ (op >>> 18 | op << 14) ^ op >>> 3;
          /** @type {number} */
          list[i] = name + list[i - 7] + op + list[i - 16] | 0;
        }
        a = s.h0;
        b = s.h1;
        c = s.h2;
        d = s.h3;
        e = s.h4;
        f = s.h5;
        g = s.h6;
        h = s.h7;
        /** @type {number} */
        i = 0;
        for (; i < 64; ++i) {
          /** @type {number} */
          sigma1 = (e >>> 6 | e << 26) ^ (e >>> 11 | e << 21) ^ (e >>> 25 | e << 7);
          /** @type {number} */
          ch = g ^ e & (f ^ g);
          /** @type {number} */
          opbeg = (a >>> 2 | a << 30) ^ (a >>> 13 | a << 19) ^ (a >>> 22 | a << 10);
          /** @type {number} */
          preCopy = a & b | c & (a ^ b);
          name = h + sigma1 + ch + segs[i] + list[i];
          /** @type {number} */
          op = opbeg + preCopy;
          h = g;
          g = f;
          f = e;
          /** @type {number} */
          e = d + name >>> 0;
          d = c;
          c = b;
          b = a;
          /** @type {number} */
          a = name + op >>> 0;
        }
        /** @type {number} */
        s.h0 = s.h0 + a | 0;
        /** @type {number} */
        s.h1 = s.h1 + b | 0;
        /** @type {number} */
        s.h2 = s.h2 + c | 0;
        /** @type {number} */
        s.h3 = s.h3 + d | 0;
        /** @type {number} */
        s.h4 = s.h4 + e | 0;
        /** @type {number} */
        s.h5 = s.h5 + f | 0;
        /** @type {number} */
        s.h6 = s.h6 + g | 0;
        /** @type {number} */
        s.h7 = s.h7 + h | 0;
        /** @type {number} */
        val = val - 64;
      }
    }
    var forge = FORGE(0);
    FORGE(4);
    FORGE(1);
    var sha256 = mixin.exports = forge.sha256 = forge.sha256 || {};
    forge.md.sha256 = forge.md.algorithms.sha256 = sha256;
    /**
     * @return {?}
     */
    sha256.create = function() {
      if (!c) {
        _init();
      }
      /** @type {null} */
      var _state = null;
      var _input = forge.util.createBuffer();
      /** @type {!Array} */
      var data = new Array(64);
      var md = {
        algorithm : "sha256",
        blockLength : 64,
        digestLength : 32,
        messageLength : 0,
        fullMessageLength : null,
        messageLengthSize : 8
      };
      return md.start = function() {
        /** @type {number} */
        md.messageLength = 0;
        /** @type {!Array} */
        md.fullMessageLength = md.messageLength64 = [];
        /** @type {number} */
        var eleSize = md.messageLengthSize / 4;
        /** @type {number} */
        var x = 0;
        for (; x < eleSize; ++x) {
          md.fullMessageLength.push(0);
        }
        return _input = forge.util.createBuffer(), _state = {
          h0 : 1779033703,
          h1 : 3144134277,
          h2 : 1013904242,
          h3 : 2773480762,
          h4 : 1359893119,
          h5 : 2600822924,
          h6 : 528734635,
          h7 : 1541459225
        }, md;
      }, md.start(), md.update = function(msg, encoding) {
        if ("utf8" === encoding) {
          msg = forge.util.encodeUtf8(msg);
        }
        var len = msg.length;
        md.messageLength += len;
        /** @type {!Array} */
        len = [len / 4294967296 >>> 0, len >>> 0];
        /** @type {number} */
        var i = md.fullMessageLength.length - 1;
        for (; i >= 0; --i) {
          md.fullMessageLength[i] += len[1];
          len[1] = len[0] + (md.fullMessageLength[i] / 4294967296 >>> 0);
          /** @type {number} */
          md.fullMessageLength[i] = md.fullMessageLength[i] >>> 0;
          /** @type {number} */
          len[0] = len[1] / 4294967296 >>> 0;
        }
        return _input.putBytes(msg), _update(_state, data, _input), (_input.read > 2048 || 0 === _input.length()) && _input.compact(), md;
      }, md.digest = function() {
        var finalBlock = forge.util.createBuffer();
        finalBlock.putBytes(_input.bytes());
        var remaining = md.fullMessageLength[md.fullMessageLength.length - 1] + md.messageLengthSize;
        /** @type {number} */
        var overflow = remaining & md.blockLength - 1;
        finalBlock.putBytes(_padding.substr(0, md.blockLength - overflow));
        var next;
        var carry;
        /** @type {number} */
        var bits = 8 * md.fullMessageLength[0];
        /** @type {number} */
        var i = 0;
        for (; i < md.fullMessageLength.length - 1; ++i) {
          /** @type {number} */
          next = 8 * md.fullMessageLength[i + 1];
          /** @type {number} */
          carry = next / 4294967296 >>> 0;
          /** @type {number} */
          bits = bits + carry;
          finalBlock.putInt32(bits >>> 0);
          /** @type {number} */
          bits = next >>> 0;
        }
        finalBlock.putInt32(bits);
        var s2 = {
          h0 : _state.h0,
          h1 : _state.h1,
          h2 : _state.h2,
          h3 : _state.h3,
          h4 : _state.h4,
          h5 : _state.h5,
          h6 : _state.h6,
          h7 : _state.h7
        };
        _update(s2, data, finalBlock);
        var rval = forge.util.createBuffer();
        return rval.putInt32(s2.h0), rval.putInt32(s2.h1), rval.putInt32(s2.h2), rval.putInt32(s2.h3), rval.putInt32(s2.h4), rval.putInt32(s2.h5), rval.putInt32(s2.h6), rval.putInt32(s2.h7), rval;
      }, md;
    };
    /** @type {null} */
    var _padding = null;
    /** @type {boolean} */
    var c = false;
    /** @type {null} */
    var segs = null;
  }, function(mixin, canCreateDiscussions, FORGE) {
    var forge = FORGE(0);
    FORGE(3);
    FORGE(8);
    FORGE(14);
    FORGE(7);
    FORGE(26);
    FORGE(2);
    FORGE(9);
    FORGE(1);
    /**
     * @param {string} secret
     * @param {string} label
     * @param {!Object} seed
     * @param {number} length
     * @return {?}
     */
    var prf_TLS1 = function(secret, label, seed, length) {
      var rval = forge.util.createBuffer();
      /** @type {number} */
      var idx = secret.length >> 1;
      /** @type {number} */
      var slen = idx + (1 & secret.length);
      var s1 = secret.substr(0, slen);
      var key = secret.substr(idx, slen);
      var ai = forge.util.createBuffer();
      var hmac = forge.hmac.create();
      seed = label + seed;
      /** @type {number} */
      var clientWidth = Math.ceil(length / 16);
      /** @type {number} */
      var clientHeight = Math.ceil(length / 20);
      hmac.start("MD5", s1);
      var t = forge.util.createBuffer();
      ai.putBytes(seed);
      /** @type {number} */
      var targetOffsetHeight = 0;
      for (; targetOffsetHeight < clientWidth; ++targetOffsetHeight) {
        hmac.start(null, null);
        hmac.update(ai.getBytes());
        ai.putBuffer(hmac.digest());
        hmac.start(null, null);
        hmac.update(ai.bytes() + seed);
        t.putBuffer(hmac.digest());
      }
      hmac.start("SHA1", key);
      var bytes = forge.util.createBuffer();
      ai.clear();
      ai.putBytes(seed);
      /** @type {number} */
      targetOffsetHeight = 0;
      for (; targetOffsetHeight < clientHeight; ++targetOffsetHeight) {
        hmac.start(null, null);
        hmac.update(ai.getBytes());
        ai.putBuffer(hmac.digest());
        hmac.start(null, null);
        hmac.update(ai.bytes() + seed);
        bytes.putBuffer(hmac.digest());
      }
      return rval.putBytes(forge.util.xorBytes(t.getBytes(), bytes.getBytes(), length)), rval;
    };
    /**
     * @param {!Object} key
     * @param {!Object} seqNum
     * @param {!Object} record
     * @return {?}
     */
    var hmac_sha1 = function(key, seqNum, record) {
      var hmac = forge.hmac.create();
      hmac.start("SHA1", key);
      var b = forge.util.createBuffer();
      return b.putInt32(seqNum[0]), b.putInt32(seqNum[1]), b.putByte(record.type), b.putByte(record.version.major), b.putByte(record.version.minor), b.putInt16(record.length), b.putBytes(record.fragment.bytes()), hmac.update(b.getBytes()), hmac.digest().getBytes();
    };
    /**
     * @param {!Object} c
     * @param {!Object} record
     * @param {?} options
     * @return {?}
     */
    var deflate = function(c, record, options) {
      /** @type {boolean} */
      var Z_STREAM_END = false;
      try {
        var bytes = c.deflate(record.fragment.getBytes());
        record.fragment = forge.util.createBuffer(bytes);
        record.length = bytes.length;
        /** @type {boolean} */
        Z_STREAM_END = true;
      } catch (e) {
      }
      return Z_STREAM_END;
    };
    /**
     * @param {!Object} c
     * @param {!Object} record
     * @param {?} data
     * @return {?}
     */
    var inflate = function(c, record, data) {
      /** @type {boolean} */
      var ranges = false;
      try {
        var bytes = c.inflate(record.fragment.getBytes());
        record.fragment = forge.util.createBuffer(bytes);
        record.length = bytes.length;
        /** @type {boolean} */
        ranges = true;
      } catch (e) {
      }
      return ranges;
    };
    /**
     * @param {string} b
     * @param {number} data
     * @return {?}
     */
    var readVector = function(b, data) {
      /** @type {number} */
      var len = 0;
      switch(data) {
        case 1:
          len = b.getByte();
          break;
        case 2:
          len = b.getInt16();
          break;
        case 3:
          len = b.getInt24();
          break;
        case 4:
          len = b.getInt32();
      }
      return forge.util.createBuffer(b.getBytes(len));
    };
    /**
     * @param {!Object} b
     * @param {number} lenBytes
     * @param {string} v
     * @return {undefined}
     */
    var writeVector = function(b, lenBytes, v) {
      b.putInt(v.length(), lenBytes << 3);
      b.putBuffer(v);
    };
    var tls = {};
    tls.Versions = {
      TLS_1_0 : {
        major : 3,
        minor : 1
      },
      TLS_1_1 : {
        major : 3,
        minor : 2
      },
      TLS_1_2 : {
        major : 3,
        minor : 3
      }
    };
    /** @type {!Array} */
    tls.SupportedVersions = [tls.Versions.TLS_1_1, tls.Versions.TLS_1_0];
    tls.Version = tls.SupportedVersions[0];
    /** @type {number} */
    tls.MaxFragment = 15360;
    tls.ConnectionEnd = {
      server : 0,
      client : 1
    };
    tls.PRFAlgorithm = {
      tls_prf_sha256 : 0
    };
    tls.BulkCipherAlgorithm = {
      none : null,
      rc4 : 0,
      des3 : 1,
      aes : 2
    };
    tls.CipherType = {
      stream : 0,
      block : 1,
      aead : 2
    };
    tls.MACAlgorithm = {
      none : null,
      hmac_md5 : 0,
      hmac_sha1 : 1,
      hmac_sha256 : 2,
      hmac_sha384 : 3,
      hmac_sha512 : 4
    };
    tls.CompressionMethod = {
      none : 0,
      deflate : 1
    };
    tls.ContentType = {
      change_cipher_spec : 20,
      alert : 21,
      handshake : 22,
      application_data : 23,
      heartbeat : 24
    };
    tls.HandshakeType = {
      hello_request : 0,
      client_hello : 1,
      server_hello : 2,
      certificate : 11,
      server_key_exchange : 12,
      certificate_request : 13,
      server_hello_done : 14,
      certificate_verify : 15,
      client_key_exchange : 16,
      finished : 20
    };
    tls.Alert = {};
    tls.Alert.Level = {
      warning : 1,
      fatal : 2
    };
    tls.Alert.Description = {
      close_notify : 0,
      unexpected_message : 10,
      bad_record_mac : 20,
      decryption_failed : 21,
      record_overflow : 22,
      decompression_failure : 30,
      handshake_failure : 40,
      bad_certificate : 42,
      unsupported_certificate : 43,
      certificate_revoked : 44,
      certificate_expired : 45,
      certificate_unknown : 46,
      illegal_parameter : 47,
      unknown_ca : 48,
      access_denied : 49,
      decode_error : 50,
      decrypt_error : 51,
      export_restriction : 60,
      protocol_version : 70,
      insufficient_security : 71,
      internal_error : 80,
      user_canceled : 90,
      no_renegotiation : 100
    };
    tls.HeartbeatMessageType = {
      heartbeat_request : 1,
      heartbeat_response : 2
    };
    tls.CipherSuites = {};
    /**
     * @param {string} twoBytes
     * @return {?}
     */
    tls.getCipherSuite = function(twoBytes) {
      /** @type {null} */
      var lastTrackTitle = null;
      var key;
      for (key in tls.CipherSuites) {
        var track = tls.CipherSuites[key];
        if (track.id[0] === twoBytes.charCodeAt(0) && track.id[1] === twoBytes.charCodeAt(1)) {
          lastTrackTitle = track;
          break;
        }
      }
      return lastTrackTitle;
    };
    /**
     * @param {!Object} c
     * @param {!Object} record
     * @return {undefined}
     */
    tls.handleUnexpected = function(c, record) {
      /** @type {boolean} */
      var r = !c.open && c.entity === tls.ConnectionEnd.client;
      if (!r) {
        c.error(c, {
          message : "Unexpected message. Received TLS record out of order.",
          send : true,
          alert : {
            level : tls.Alert.Level.fatal,
            description : tls.Alert.Description.unexpected_message
          }
        });
      }
    };
    /**
     * @param {!Object} c
     * @param {?} record
     * @param {?} length
     * @return {undefined}
     */
    tls.handleHelloRequest = function(c, record, length) {
      if (!c.handshaking && c.handshakes > 0) {
        tls.queue(c, tls.createAlert(c, {
          level : tls.Alert.Level.warning,
          description : tls.Alert.Description.no_renegotiation
        }));
        tls.flush(c);
      }
      c.process();
    };
    /**
     * @param {!Object} c
     * @param {!Object} record
     * @param {number} length
     * @return {?}
     */
    tls.parseHelloMessage = function(c, record, length) {
      /** @type {null} */
      var msg = null;
      /** @type {boolean} */
      var validation_result = c.entity === tls.ConnectionEnd.client;
      if (length < 38) {
        c.error(c, {
          message : validation_result ? "Invalid ServerHello message. Message too short." : "Invalid ClientHello message. Message too short.",
          send : true,
          alert : {
            level : tls.Alert.Level.fatal,
            description : tls.Alert.Description.illegal_parameter
          }
        });
      } else {
        var b = record.fragment;
        var newLength = b.length();
        if (msg = {
          version : {
            major : b.getByte(),
            minor : b.getByte()
          },
          random : forge.util.createBuffer(b.getBytes(32)),
          session_id : readVector(b, 1),
          extensions : []
        }, validation_result ? (msg.cipher_suite = b.getBytes(2), msg.compression_method = b.getByte()) : (msg.cipher_suites = readVector(b, 2), msg.compression_methods = readVector(b, 1)), newLength = length - (newLength - b.length()), newLength > 0) {
          var exts = readVector(b, 2);
          for (; exts.length() > 0;) {
            msg.extensions.push({
              type : [exts.getByte(), exts.getByte()],
              data : readVector(exts, 2)
            });
          }
          if (!validation_result) {
            /** @type {number} */
            var i = 0;
            for (; i < msg.extensions.length; ++i) {
              var ext = msg.extensions[i];
              if (0 === ext.type[0] && 0 === ext.type[1]) {
                var snl = readVector(ext.data, 2);
                for (; snl.length() > 0;) {
                  var d = snl.getByte();
                  if (0 !== d) {
                    break;
                  }
                  c.session.extensions.server_name.serverNameList.push(readVector(snl, 2).getBytes());
                }
              }
            }
          }
        }
        if (c.session.version && (msg.version.major !== c.session.version.major || msg.version.minor !== c.session.version.minor)) {
          return c.error(c, {
            message : "TLS version change is disallowed during renegotiation.",
            send : true,
            alert : {
              level : tls.Alert.Level.fatal,
              description : tls.Alert.Description.protocol_version
            }
          });
        }
        if (validation_result) {
          c.session.cipherSuite = tls.getCipherSuite(msg.cipher_suite);
        } else {
          var cmapObj = forge.util.createBuffer(msg.cipher_suites.bytes());
          for (; cmapObj.length() > 0 && (c.session.cipherSuite = tls.getCipherSuite(cmapObj.getBytes(2)), null === c.session.cipherSuite);) {
          }
        }
        if (null === c.session.cipherSuite) {
          return c.error(c, {
            message : "No cipher suites in common.",
            send : true,
            alert : {
              level : tls.Alert.Level.fatal,
              description : tls.Alert.Description.handshake_failure
            },
            cipherSuite : forge.util.bytesToHex(msg.cipher_suite)
          });
        }
        if (validation_result) {
          c.session.compressionMethod = msg.compression_method;
        } else {
          /** @type {number} */
          c.session.compressionMethod = tls.CompressionMethod.none;
        }
      }
      return msg;
    };
    /**
     * @param {!Object} c
     * @param {!Object} msg
     * @return {undefined}
     */
    tls.createSecurityParameters = function(c, msg) {
      /** @type {boolean} */
      var client = c.entity === tls.ConnectionEnd.client;
      var msgRandom = msg.random.bytes();
      var cRandom = client ? c.session.sp.client_random : msgRandom;
      var sRandom = client ? msgRandom : tls.createRandom().getBytes();
      c.session.sp = {
        entity : c.entity,
        prf_algorithm : tls.PRFAlgorithm.tls_prf_sha256,
        bulk_cipher_algorithm : null,
        cipher_type : null,
        enc_key_length : null,
        block_length : null,
        fixed_iv_length : null,
        record_iv_length : null,
        mac_algorithm : null,
        mac_length : null,
        mac_key_length : null,
        compression_algorithm : c.session.compressionMethod,
        pre_master_secret : null,
        master_secret : null,
        client_random : cRandom,
        server_random : sRandom
      };
    };
    /**
     * @param {!Object} c
     * @param {!Object} record
     * @param {undefined} length
     * @return {?}
     */
    tls.handleServerHello = function(c, record, length) {
      var msg = tls.parseHelloMessage(c, record, length);
      if (!c.fail) {
        if (!(msg.version.minor <= c.version.minor)) {
          return c.error(c, {
            message : "Incompatible TLS version.",
            send : true,
            alert : {
              level : tls.Alert.Level.fatal,
              description : tls.Alert.Description.protocol_version
            }
          });
        }
        c.version.minor = msg.version.minor;
        c.session.version = c.version;
        var systemId = msg.session_id.bytes();
        if (systemId.length > 0 && systemId === c.session.id) {
          /** @type {number} */
          c.expect = SCC;
          /** @type {boolean} */
          c.session.resuming = true;
          c.session.sp.server_random = msg.random.bytes();
        } else {
          /** @type {number} */
          c.expect = SCE;
          /** @type {boolean} */
          c.session.resuming = false;
          tls.createSecurityParameters(c, msg);
        }
        c.session.id = systemId;
        c.process();
      }
    };
    /**
     * @param {!Object} c
     * @param {!Object} record
     * @param {undefined} length
     * @return {undefined}
     */
    tls.handleClientHello = function(c, record, length) {
      var msg = tls.parseHelloMessage(c, record, length);
      if (!c.fail) {
        var count = msg.session_id.bytes();
        /** @type {null} */
        var data = null;
        if (c.sessionCache && (data = c.sessionCache.getSession(count), null === data ? count = "" : (data.version.major !== msg.version.major || data.version.minor > msg.version.minor) && (data = null, count = "")), 0 === count.length && (count = forge.random.getBytes(32)), c.session.id = count, c.session.clientHelloVersion = msg.version, c.session.sp = {}, data) {
          c.version = c.session.version = data.version;
          c.session.sp = data.sp;
        } else {
          var ua;
          /** @type {number} */
          var i = 1;
          for (; i < tls.SupportedVersions.length && (ua = tls.SupportedVersions[i], !(ua.minor <= msg.version.minor)); ++i) {
          }
          c.version = {
            major : ua.major,
            minor : ua.minor
          };
          c.session.version = c.version;
        }
        if (null !== data) {
          /** @type {number} */
          c.expect = SHD;
          /** @type {boolean} */
          c.session.resuming = true;
          c.session.sp.client_random = msg.random.bytes();
        } else {
          /** @type {number} */
          c.expect = c.verifyClient !== false ? SHE : CAD;
          /** @type {boolean} */
          c.session.resuming = false;
          tls.createSecurityParameters(c, msg);
        }
        /** @type {boolean} */
        c.open = true;
        tls.queue(c, tls.createRecord(c, {
          type : tls.ContentType.handshake,
          data : tls.createServerHello(c)
        }));
        if (c.session.resuming) {
          tls.queue(c, tls.createRecord(c, {
            type : tls.ContentType.change_cipher_spec,
            data : tls.createChangeCipherSpec()
          }));
          c.state.pending = tls.createConnectionState(c);
          c.state.current.write = c.state.pending.write;
          tls.queue(c, tls.createRecord(c, {
            type : tls.ContentType.handshake,
            data : tls.createFinished(c)
          }));
        } else {
          tls.queue(c, tls.createRecord(c, {
            type : tls.ContentType.handshake,
            data : tls.createCertificate(c)
          }));
          if (!c.fail) {
            tls.queue(c, tls.createRecord(c, {
              type : tls.ContentType.handshake,
              data : tls.createServerKeyExchange(c)
            }));
            if (c.verifyClient !== false) {
              tls.queue(c, tls.createRecord(c, {
                type : tls.ContentType.handshake,
                data : tls.createCertificateRequest(c)
              }));
            }
            tls.queue(c, tls.createRecord(c, {
              type : tls.ContentType.handshake,
              data : tls.createServerHelloDone(c)
            }));
          }
        }
        tls.flush(c);
        c.process();
      }
    };
    /**
     * @param {!Object} c
     * @param {!Object} record
     * @param {number} length
     * @return {?}
     */
    tls.handleCertificate = function(c, record, length) {
      if (length < 3) {
        return c.error(c, {
          message : "Invalid Certificate message. Message too short.",
          send : true,
          alert : {
            level : tls.Alert.Level.fatal,
            description : tls.Alert.Description.illegal_parameter
          }
        });
      }
      var cert;
      var asn1;
      var b = record.fragment;
      var msg = {
        certificate_list : readVector(b, 3)
      };
      /** @type {!Array} */
      var certs = [];
      try {
        for (; msg.certificate_list.length() > 0;) {
          cert = readVector(msg.certificate_list, 3);
          asn1 = forge.asn1.fromDer(cert);
          cert = forge.pki.certificateFromAsn1(asn1, true);
          certs.push(cert);
        }
      } catch (err) {
        return c.error(c, {
          message : "Could not parse certificate list.",
          cause : err,
          send : true,
          alert : {
            level : tls.Alert.Level.fatal,
            description : tls.Alert.Description.bad_certificate
          }
        });
      }
      /** @type {boolean} */
      var client = c.entity === tls.ConnectionEnd.client;
      if (!client && c.verifyClient !== true || 0 !== certs.length) {
        if (0 === certs.length) {
          /** @type {number} */
          c.expect = client ? SKE : CAD;
        } else {
          if (client) {
            c.session.serverCertificate = certs[0];
          } else {
            c.session.clientCertificate = certs[0];
          }
          if (tls.verifyCertificateChain(c, certs)) {
            /** @type {number} */
            c.expect = client ? SKE : CAD;
          }
        }
      } else {
        c.error(c, {
          message : client ? "No server certificate provided." : "No client certificate provided.",
          send : true,
          alert : {
            level : tls.Alert.Level.fatal,
            description : tls.Alert.Description.illegal_parameter
          }
        });
      }
      c.process();
    };
    /**
     * @param {!Object} c
     * @param {?} record
     * @param {number} length
     * @return {?}
     */
    tls.handleServerKeyExchange = function(c, record, length) {
      return length > 0 ? c.error(c, {
        message : "Invalid key parameters. Only RSA is supported.",
        send : true,
        alert : {
          level : tls.Alert.Level.fatal,
          description : tls.Alert.Description.unsupported_certificate
        }
      }) : (c.expect = SCR, void c.process());
    };
    /**
     * @param {!Object} c
     * @param {!Object} record
     * @param {number} length
     * @return {?}
     */
    tls.handleClientKeyExchange = function(c, record, length) {
      if (length < 48) {
        return c.error(c, {
          message : "Invalid key parameters. Only RSA is supported.",
          send : true,
          alert : {
            level : tls.Alert.Level.fatal,
            description : tls.Alert.Description.unsupported_certificate
          }
        });
      }
      var b = record.fragment;
      var msg = {
        enc_pre_master_secret : readVector(b, 2).getBytes()
      };
      /** @type {null} */
      var privateKey = null;
      if (c.getPrivateKey) {
        try {
          privateKey = c.getPrivateKey(c, c.session.serverCertificate);
          privateKey = forge.pki.privateKeyFromPem(privateKey);
        } catch (err) {
          c.error(c, {
            message : "Could not get private key.",
            cause : err,
            send : true,
            alert : {
              level : tls.Alert.Level.fatal,
              description : tls.Alert.Description.internal_error
            }
          });
        }
      }
      if (null === privateKey) {
        return c.error(c, {
          message : "No private key set.",
          send : true,
          alert : {
            level : tls.Alert.Level.fatal,
            description : tls.Alert.Description.internal_error
          }
        });
      }
      try {
        var sp = c.session.sp;
        sp.pre_master_secret = privateKey.decrypt(msg.enc_pre_master_secret);
        var version = c.session.clientHelloVersion;
        if (version.major !== sp.pre_master_secret.charCodeAt(0) || version.minor !== sp.pre_master_secret.charCodeAt(1)) {
          throw new Error("TLS version rollback attack detected.");
        }
      } catch (e) {
        sp.pre_master_secret = forge.random.getBytes(48);
      }
      /** @type {number} */
      c.expect = SHD;
      if (null !== c.session.clientCertificate) {
        /** @type {number} */
        c.expect = CCV;
      }
      c.process();
    };
    /**
     * @param {!Object} c
     * @param {!Object} record
     * @param {number} length
     * @return {?}
     */
    tls.handleCertificateRequest = function(c, record, length) {
      if (length < 3) {
        return c.error(c, {
          message : "Invalid CertificateRequest. Message too short.",
          send : true,
          alert : {
            level : tls.Alert.Level.fatal,
            description : tls.Alert.Description.illegal_parameter
          }
        });
      }
      var b = record.fragment;
      var msg = {
        certificate_types : readVector(b, 1),
        certificate_authorities : readVector(b, 2)
      };
      c.session.certificateRequest = msg;
      /** @type {number} */
      c.expect = SER;
      c.process();
    };
    /**
     * @param {!Object} c
     * @param {!Object} record
     * @param {number} length
     * @return {?}
     */
    tls.handleCertificateVerify = function(c, record, length) {
      if (length < 2) {
        return c.error(c, {
          message : "Invalid CertificateVerify. Message too short.",
          send : true,
          alert : {
            level : tls.Alert.Level.fatal,
            description : tls.Alert.Description.illegal_parameter
          }
        });
      }
      var b = record.fragment;
      b.read -= 4;
      var i = b.bytes();
      b.read += 4;
      var header = {
        signature : readVector(b, 2).getBytes()
      };
      var value = forge.util.createBuffer();
      value.putBuffer(c.session.md5.digest());
      value.putBuffer(c.session.sha1.digest());
      value = value.getBytes();
      try {
        var cert = c.session.clientCertificate;
        if (!cert.publicKey.verify(value, header.signature, "NONE")) {
          throw new Error("CertificateVerify signature does not match.");
        }
        c.session.md5.update(i);
        c.session.sha1.update(i);
      } catch (t) {
        return c.error(c, {
          message : "Bad signature in CertificateVerify.",
          send : true,
          alert : {
            level : tls.Alert.Level.fatal,
            description : tls.Alert.Description.handshake_failure
          }
        });
      }
      /** @type {number} */
      c.expect = SHD;
      c.process();
    };
    /**
     * @param {!Object} c
     * @param {string} record
     * @param {number} length
     * @return {?}
     */
    tls.handleServerHelloDone = function(c, record, length) {
      if (length > 0) {
        return c.error(c, {
          message : "Invalid ServerHelloDone message. Invalid length.",
          send : true,
          alert : {
            level : tls.Alert.Level.fatal,
            description : tls.Alert.Description.record_overflow
          }
        });
      }
      if (null === c.serverCertificate) {
        var error = {
          message : "No server certificate provided. Not enough security.",
          send : true,
          alert : {
            level : tls.Alert.Level.fatal,
            description : tls.Alert.Description.insufficient_security
          }
        };
        /** @type {number} */
        var done = 0;
        var data = c.verify(c, error.alert.description, done, []);
        if (data !== true) {
          return (data || 0 === data) && ("object" != typeof data || forge.util.isArray(data) ? "number" == typeof data && (error.alert.description = data) : (data.message && (error.message = data.message), data.alert && (error.alert.description = data.alert))), c.error(c, error);
        }
      }
      if (null !== c.session.certificateRequest) {
        record = tls.createRecord(c, {
          type : tls.ContentType.handshake,
          data : tls.createCertificate(c)
        });
        tls.queue(c, record);
      }
      record = tls.createRecord(c, {
        type : tls.ContentType.handshake,
        data : tls.createClientKeyExchange(c)
      });
      tls.queue(c, record);
      /** @type {number} */
      c.expect = CCC;
      /**
       * @param {!Object} c
       * @param {!Object} signature
       * @return {undefined}
       */
      var callback = function(c, signature) {
        if (null !== c.session.certificateRequest && null !== c.session.clientCertificate) {
          tls.queue(c, tls.createRecord(c, {
            type : tls.ContentType.handshake,
            data : tls.createCertificateVerify(c, signature)
          }));
        }
        tls.queue(c, tls.createRecord(c, {
          type : tls.ContentType.change_cipher_spec,
          data : tls.createChangeCipherSpec()
        }));
        c.state.pending = tls.createConnectionState(c);
        c.state.current.write = c.state.pending.write;
        tls.queue(c, tls.createRecord(c, {
          type : tls.ContentType.handshake,
          data : tls.createFinished(c)
        }));
        /** @type {number} */
        c.expect = SCC;
        tls.flush(c);
        c.process();
      };
      return null === c.session.certificateRequest || null === c.session.clientCertificate ? callback(c, null) : void tls.getClientSignature(c, callback);
    };
    /**
     * @param {!Object} c
     * @param {!Object} record
     * @return {?}
     */
    tls.handleChangeCipherSpec = function(c, record) {
      if (1 !== record.fragment.getByte()) {
        return c.error(c, {
          message : "Invalid ChangeCipherSpec message received.",
          send : true,
          alert : {
            level : tls.Alert.Level.fatal,
            description : tls.Alert.Description.illegal_parameter
          }
        });
      }
      /** @type {boolean} */
      var client = c.entity === tls.ConnectionEnd.client;
      if (c.session.resuming && client || !c.session.resuming && !client) {
        c.state.pending = tls.createConnectionState(c);
      }
      c.state.current.read = c.state.pending.read;
      if (!c.session.resuming && client || c.session.resuming && !client) {
        /** @type {null} */
        c.state.pending = null;
      }
      /** @type {number} */
      c.expect = client ? SFI : CKE;
      c.process();
    };
    /**
     * @param {!Object} c
     * @param {!Object} record
     * @param {?} url
     * @return {?}
     */
    tls.handleFinished = function(c, record, url) {
      var b = record.fragment;
      b.read -= 4;
      var s = b.bytes();
      b.read += 4;
      var keydown = record.fragment.getBytes();
      b = forge.util.createBuffer();
      b.putBuffer(c.session.md5.digest());
      b.putBuffer(c.session.sha1.digest());
      /** @type {boolean} */
      var client = c.entity === tls.ConnectionEnd.client;
      /** @type {string} */
      var label = client ? "server finished" : "client finished";
      var sp = c.session.sp;
      /** @type {number} */
      var vdl = 12;
      /** @type {function(string, string, !Object, number): ?} */
      var prf = prf_TLS1;
      return b = prf(sp.master_secret, label, b.getBytes(), vdl), b.getBytes() !== keydown ? c.error(c, {
        message : "Invalid verify_data in Finished message.",
        send : true,
        alert : {
          level : tls.Alert.Level.fatal,
          description : tls.Alert.Description.decrypt_error
        }
      }) : (c.session.md5.update(s), c.session.sha1.update(s), (c.session.resuming && client || !c.session.resuming && !client) && (tls.queue(c, tls.createRecord(c, {
        type : tls.ContentType.change_cipher_spec,
        data : tls.createChangeCipherSpec()
      })), c.state.current.write = c.state.pending.write, c.state.pending = null, tls.queue(c, tls.createRecord(c, {
        type : tls.ContentType.handshake,
        data : tls.createFinished(c)
      }))), c.expect = client ? SAD : CFI, c.handshaking = false, ++c.handshakes, c.peerCertificate = client ? c.session.serverCertificate : c.session.clientCertificate, tls.flush(c), c.isConnected = true, c.connected(c), void c.process());
    };
    /**
     * @param {!Object} c
     * @param {!Object} record
     * @return {?}
     */
    tls.handleAlert = function(c, record) {
      var msg_obj;
      var b = record.fragment;
      var alert = {
        level : b.getByte(),
        description : b.getByte()
      };
      switch(alert.description) {
        case tls.Alert.Description.close_notify:
          /** @type {string} */
          msg_obj = "Connection closed.";
          break;
        case tls.Alert.Description.unexpected_message:
          /** @type {string} */
          msg_obj = "Unexpected message.";
          break;
        case tls.Alert.Description.bad_record_mac:
          /** @type {string} */
          msg_obj = "Bad record MAC.";
          break;
        case tls.Alert.Description.decryption_failed:
          /** @type {string} */
          msg_obj = "Decryption failed.";
          break;
        case tls.Alert.Description.record_overflow:
          /** @type {string} */
          msg_obj = "Record overflow.";
          break;
        case tls.Alert.Description.decompression_failure:
          /** @type {string} */
          msg_obj = "Decompression failed.";
          break;
        case tls.Alert.Description.handshake_failure:
          /** @type {string} */
          msg_obj = "Handshake failure.";
          break;
        case tls.Alert.Description.bad_certificate:
          /** @type {string} */
          msg_obj = "Bad certificate.";
          break;
        case tls.Alert.Description.unsupported_certificate:
          /** @type {string} */
          msg_obj = "Unsupported certificate.";
          break;
        case tls.Alert.Description.certificate_revoked:
          /** @type {string} */
          msg_obj = "Certificate revoked.";
          break;
        case tls.Alert.Description.certificate_expired:
          /** @type {string} */
          msg_obj = "Certificate expired.";
          break;
        case tls.Alert.Description.certificate_unknown:
          /** @type {string} */
          msg_obj = "Certificate unknown.";
          break;
        case tls.Alert.Description.illegal_parameter:
          /** @type {string} */
          msg_obj = "Illegal parameter.";
          break;
        case tls.Alert.Description.unknown_ca:
          /** @type {string} */
          msg_obj = "Unknown certificate authority.";
          break;
        case tls.Alert.Description.access_denied:
          /** @type {string} */
          msg_obj = "Access denied.";
          break;
        case tls.Alert.Description.decode_error:
          /** @type {string} */
          msg_obj = "Decode error.";
          break;
        case tls.Alert.Description.decrypt_error:
          /** @type {string} */
          msg_obj = "Decrypt error.";
          break;
        case tls.Alert.Description.export_restriction:
          /** @type {string} */
          msg_obj = "Export restriction.";
          break;
        case tls.Alert.Description.protocol_version:
          /** @type {string} */
          msg_obj = "Unsupported protocol version.";
          break;
        case tls.Alert.Description.insufficient_security:
          /** @type {string} */
          msg_obj = "Insufficient security.";
          break;
        case tls.Alert.Description.internal_error:
          /** @type {string} */
          msg_obj = "Internal error.";
          break;
        case tls.Alert.Description.user_canceled:
          /** @type {string} */
          msg_obj = "User canceled.";
          break;
        case tls.Alert.Description.no_renegotiation:
          /** @type {string} */
          msg_obj = "Renegotiation not supported.";
          break;
        default:
          /** @type {string} */
          msg_obj = "Unknown error.";
      }
      return alert.description === tls.Alert.Description.close_notify ? c.close() : (c.error(c, {
        message : msg_obj,
        send : false,
        origin : c.entity === tls.ConnectionEnd.client ? "server" : "client",
        alert : alert
      }), void c.process());
    };
    /**
     * @param {!Object} c
     * @param {!Object} record
     * @return {?}
     */
    tls.handleHandshake = function(c, record) {
      var b = record.fragment;
      var type = b.getByte();
      var length = b.getInt24();
      if (length > b.length()) {
        return c.fragmented = record, record.fragment = forge.util.createBuffer(), b.read -= 4, c.process();
      }
      /** @type {null} */
      c.fragmented = null;
      b.read -= 4;
      var s = b.bytes(length + 4);
      b.read += 4;
      if (type in hsTable[c.entity][c.expect]) {
        if (!(c.entity !== tls.ConnectionEnd.server || c.open || c.fail)) {
          /** @type {boolean} */
          c.handshaking = true;
          c.session = {
            version : null,
            extensions : {
              server_name : {
                serverNameList : []
              }
            },
            cipherSuite : null,
            compressionMethod : null,
            serverCertificate : null,
            clientCertificate : null,
            md5 : forge.md.md5.create(),
            sha1 : forge.md.sha1.create()
          };
        }
        if (type !== tls.HandshakeType.hello_request && type !== tls.HandshakeType.certificate_verify && type !== tls.HandshakeType.finished) {
          c.session.md5.update(s);
          c.session.sha1.update(s);
        }
        hsTable[c.entity][c.expect][type](c, record, length);
      } else {
        tls.handleUnexpected(c, record);
      }
    };
    /**
     * @param {!Object} c
     * @param {!Object} record
     * @return {undefined}
     */
    tls.handleApplicationData = function(c, record) {
      c.data.putBuffer(record.fragment);
      c.dataReady(c);
      c.process();
    };
    /**
     * @param {!Object} c
     * @param {!Object} data
     * @return {?}
     */
    tls.handleHeartbeat = function(c, data) {
      var b = data.fragment;
      var n = b.getByte();
      var i = b.getInt16();
      var msg = b.getBytes(i);
      if (n === tls.HeartbeatMessageType.heartbeat_request) {
        if (c.handshaking || i > msg.length) {
          return c.process();
        }
        tls.queue(c, tls.createRecord(c, {
          type : tls.ContentType.heartbeat,
          data : tls.createHeartbeat(tls.HeartbeatMessageType.heartbeat_response, msg)
        }));
        tls.flush(c);
      } else {
        if (n === tls.HeartbeatMessageType.heartbeat_response) {
          if (msg !== c.expectedHeartbeatPayload) {
            return c.process();
          }
          if (c.heartbeatReceived) {
            c.heartbeatReceived(c, forge.util.createBuffer(msg));
          }
        }
      }
      c.process();
    };
    /** @type {number} */
    var CCE = 0;
    /** @type {number} */
    var SCE = 1;
    /** @type {number} */
    var SKE = 2;
    /** @type {number} */
    var SCR = 3;
    /** @type {number} */
    var SER = 4;
    /** @type {number} */
    var SCC = 5;
    /** @type {number} */
    var SFI = 6;
    /** @type {number} */
    var SAD = 7;
    /** @type {number} */
    var CCC = 8;
    /** @type {number} */
    var CHE = 0;
    /** @type {number} */
    var SHE = 1;
    /** @type {number} */
    var CAD = 2;
    /** @type {number} */
    var CCV = 3;
    /** @type {number} */
    var SHD = 4;
    /** @type {number} */
    var CKE = 5;
    /** @type {number} */
    var CFI = 6;
    /** @type {function(!Object, !Object): undefined} */
    var __ = tls.handleUnexpected;
    /** @type {function(!Object, !Object): ?} */
    var F0 = tls.handleChangeCipherSpec;
    /** @type {function(!Object, !Object): ?} */
    var F1 = tls.handleAlert;
    /** @type {function(!Object, !Object): ?} */
    var F2 = tls.handleHandshake;
    /** @type {function(!Object, !Object): undefined} */
    var F3 = tls.handleApplicationData;
    /** @type {function(!Object, !Object): ?} */
    var tlsCfg = tls.handleHeartbeat;
    /** @type {!Array} */
    var ctTable = [];
    /** @type {!Array} */
    ctTable[tls.ConnectionEnd.client] = [[__, F1, F2, __, tlsCfg], [__, F1, F2, __, tlsCfg], [__, F1, F2, __, tlsCfg], [__, F1, F2, __, tlsCfg], [__, F1, F2, __, tlsCfg], [F0, F1, __, __, tlsCfg], [__, F1, F2, __, tlsCfg], [__, F1, F2, F3, tlsCfg], [__, F1, F2, __, tlsCfg]];
    /** @type {!Array} */
    ctTable[tls.ConnectionEnd.server] = [[__, F1, F2, __, tlsCfg], [__, F1, F2, __, tlsCfg], [__, F1, F2, __, tlsCfg], [__, F1, F2, __, tlsCfg], [F0, F1, __, __, tlsCfg], [__, F1, F2, __, tlsCfg], [__, F1, F2, F3, tlsCfg], [__, F1, F2, __, tlsCfg]];
    /** @type {function(!Object, ?, ?): undefined} */
    var F4 = tls.handleHelloRequest;
    /** @type {function(!Object, !Object, undefined): ?} */
    var F5 = tls.handleServerHello;
    /** @type {function(!Object, !Object, number): ?} */
    var F6 = tls.handleCertificate;
    /** @type {function(!Object, ?, number): ?} */
    var F7 = tls.handleServerKeyExchange;
    /** @type {function(!Object, !Object, number): ?} */
    var F8 = tls.handleCertificateRequest;
    /** @type {function(!Object, string, number): ?} */
    var F9 = tls.handleServerHelloDone;
    /** @type {function(!Object, !Object, ?): ?} */
    var FA = tls.handleFinished;
    /** @type {!Array} */
    var hsTable = [];
    /** @type {!Array} */
    hsTable[tls.ConnectionEnd.client] = [[__, __, F5, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __], [F4, __, __, __, __, __, __, __, __, __, __, F6, F7, F8, F9, __, __, __, __, __, __], [F4, __, __, __, __, __, __, __, __, __, __, __, F7, F8, F9, __, __, __, __, __, __], [F4, __, __, __, __, __, __, __, __, __, __, __, __, F8, F9, __, __, __, __, __, __], [F4, __, __, __, __, __, __, __, __, __, __, __, __, __, F9, __, __, __, __, __, __], [F4, __, __, __, __, __, __, __, 
    __, __, __, __, __, __, __, __, __, __, __, __, __], [F4, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, FA], [F4, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __], [F4, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __]];
    /** @type {function(!Object, !Object, undefined): undefined} */
    var FB = tls.handleClientHello;
    /** @type {function(!Object, !Object, number): ?} */
    var FC = tls.handleClientKeyExchange;
    /** @type {function(!Object, !Object, number): ?} */
    var FD = tls.handleCertificateVerify;
    /** @type {!Array} */
    hsTable[tls.ConnectionEnd.server] = [[__, FB, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __], [__, __, __, __, __, __, __, __, __, __, __, F6, __, __, __, __, __, __, __, __, __], [__, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, FC, __, __, __, __], [__, __, __, __, __, __, __, __, __, __, __, __, __, __, __, FD, __, __, __, __, __], [__, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __], [__, __, __, __, __, __, __, __, 
    __, __, __, __, __, __, __, __, __, __, __, __, FA], [__, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __], [__, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __, __]];
    /**
     * @param {!Object} c
     * @param {?} sp
     * @return {?}
     */
    tls.generateKeys = function(c, sp) {
      /** @type {function(string, string, !Object, number): ?} */
      var prf = prf_TLS1;
      var random = sp.client_random + sp.server_random;
      if (!c.session.resuming) {
        sp.master_secret = prf(sp.pre_master_secret, "master secret", random, 48).bytes();
        /** @type {null} */
        sp.pre_master_secret = null;
      }
      random = sp.server_random + sp.client_random;
      /** @type {number} */
      var length = 2 * sp.mac_key_length + 2 * sp.enc_key_length;
      /** @type {boolean} */
      var fromGroup = c.version.major === tls.Versions.TLS_1_0.major && c.version.minor === tls.Versions.TLS_1_0.minor;
      if (fromGroup) {
        /** @type {number} */
        length = length + 2 * sp.fixed_iv_length;
      }
      var km = prf(sp.master_secret, "key expansion", random, length);
      var isvalid = {
        client_write_MAC_key : km.getBytes(sp.mac_key_length),
        server_write_MAC_key : km.getBytes(sp.mac_key_length),
        client_write_key : km.getBytes(sp.enc_key_length),
        server_write_key : km.getBytes(sp.enc_key_length)
      };
      return fromGroup && (isvalid.client_write_IV = km.getBytes(sp.fixed_iv_length), isvalid.server_write_IV = km.getBytes(sp.fixed_iv_length)), isvalid;
    };
    /**
     * @param {!Object} c
     * @return {?}
     */
    tls.createConnectionState = function(c) {
      /** @type {boolean} */
      var client = c.entity === tls.ConnectionEnd.client;
      /**
       * @return {?}
       */
      var createMode = function() {
        var mode = {
          sequenceNumber : [0, 0],
          macKey : null,
          macLength : 0,
          macFunction : null,
          cipherState : null,
          cipherFunction : function(record) {
            return true;
          },
          compressionState : null,
          compressFunction : function(token) {
            return true;
          },
          updateSequenceNumber : function() {
            if (4294967295 === mode.sequenceNumber[1]) {
              /** @type {number} */
              mode.sequenceNumber[1] = 0;
              ++mode.sequenceNumber[0];
            } else {
              ++mode.sequenceNumber[1];
            }
          }
        };
        return mode;
      };
      var state = {
        read : createMode(),
        write : createMode()
      };
      if (state.read.update = function(c, record) {
        return state.read.cipherFunction(record, state.read) ? state.read.compressFunction(c, record, state.read) || c.error(c, {
          message : "Could not decompress record.",
          send : true,
          alert : {
            level : tls.Alert.Level.fatal,
            description : tls.Alert.Description.decompression_failure
          }
        }) : c.error(c, {
          message : "Could not decrypt record or bad MAC.",
          send : true,
          alert : {
            level : tls.Alert.Level.fatal,
            description : tls.Alert.Description.bad_record_mac
          }
        }), !c.fail;
      }, state.write.update = function(c, record) {
        return state.write.compressFunction(c, record, state.write) ? state.write.cipherFunction(record, state.write) || c.error(c, {
          message : "Could not encrypt record.",
          send : false,
          alert : {
            level : tls.Alert.Level.fatal,
            description : tls.Alert.Description.internal_error
          }
        }) : c.error(c, {
          message : "Could not compress record.",
          send : false,
          alert : {
            level : tls.Alert.Level.fatal,
            description : tls.Alert.Description.internal_error
          }
        }), !c.fail;
      }, c.session) {
        var sp = c.session.sp;
        switch(c.session.cipherSuite.initSecurityParameters(sp), sp.keys = tls.generateKeys(c, sp), state.read.macKey = client ? sp.keys.server_write_MAC_key : sp.keys.client_write_MAC_key, state.write.macKey = client ? sp.keys.client_write_MAC_key : sp.keys.server_write_MAC_key, c.session.cipherSuite.initConnectionState(state, c, sp), sp.compression_algorithm) {
          case tls.CompressionMethod.none:
            break;
          case tls.CompressionMethod.deflate:
            /** @type {function(!Object, !Object, ?): ?} */
            state.read.compressFunction = inflate;
            /** @type {function(!Object, !Object, ?): ?} */
            state.write.compressFunction = deflate;
            break;
          default:
            throw new Error("Unsupported compression algorithm.");
        }
      }
      return state;
    };
    /**
     * @return {?}
     */
    tls.createRandom = function() {
      /** @type {!Date} */
      var dCurrent = new Date;
      /** @type {number} */
      var i = +dCurrent + 6e4 * dCurrent.getTimezoneOffset();
      var padBytes = forge.util.createBuffer();
      return padBytes.putInt32(i), padBytes.putBytes(forge.random.getBytes(28)), padBytes;
    };
    /**
     * @param {!Object} req
     * @param {!Object} data
     * @return {?}
     */
    tls.createRecord = function(req, data) {
      if (!data.data) {
        return null;
      }
      var record = {
        type : data.type,
        version : {
          major : req.version.major,
          minor : req.version.minor
        },
        length : data.data.length(),
        fragment : data.data
      };
      return record;
    };
    /**
     * @param {!Object} message
     * @param {!Object} alert
     * @return {?}
     */
    tls.createAlert = function(message, alert) {
      var b = forge.util.createBuffer();
      return b.putByte(alert.level), b.putByte(alert.description), tls.createRecord(message, {
        type : tls.ContentType.alert,
        data : b
      });
    };
    /**
     * @param {!Object} c
     * @return {?}
     */
    tls.createClientHello = function(c) {
      c.session.clientHelloVersion = {
        major : c.version.major,
        minor : c.version.minor
      };
      var certTypes = forge.util.createBuffer();
      /** @type {number} */
      var i = 0;
      for (; i < c.cipherSuites.length; ++i) {
        var beforeTab = c.cipherSuites[i];
        certTypes.putByte(beforeTab.id[0]);
        certTypes.putByte(beforeTab.id[1]);
      }
      var e = certTypes.length();
      var compressionMethods = forge.util.createBuffer();
      compressionMethods.putByte(tls.CompressionMethod.none);
      var varWithEqual = compressionMethods.length();
      var certList = forge.util.createBuffer();
      if (c.virtualHost) {
        var value = forge.util.createBuffer();
        value.putByte(0);
        value.putByte(0);
        var serverName = forge.util.createBuffer();
        serverName.putByte(0);
        writeVector(serverName, 2, forge.util.createBuffer(c.virtualHost));
        var snList = forge.util.createBuffer();
        writeVector(snList, 2, serverName);
        writeVector(value, 2, snList);
        certList.putBuffer(value);
      }
      var varVal = certList.length();
      if (varVal > 0) {
        varVal = varVal + 2;
      }
      var sessionId = c.session.id;
      var length = sessionId.length + 1 + 2 + 4 + 28 + 2 + e + 1 + varWithEqual + varVal;
      var rval = forge.util.createBuffer();
      return rval.putByte(tls.HandshakeType.client_hello), rval.putInt24(length), rval.putByte(c.version.major), rval.putByte(c.version.minor), rval.putBytes(c.session.sp.client_random), writeVector(rval, 1, forge.util.createBuffer(sessionId)), writeVector(rval, 2, certTypes), writeVector(rval, 1, compressionMethods), varVal > 0 && writeVector(rval, 2, certList), rval;
    };
    /**
     * @param {!Object} c
     * @return {?}
     */
    tls.createServerHello = function(c) {
      var sessionId = c.session.id;
      var length = sessionId.length + 1 + 2 + 4 + 28 + 2 + 1;
      var rval = forge.util.createBuffer();
      return rval.putByte(tls.HandshakeType.server_hello), rval.putInt24(length), rval.putByte(c.version.major), rval.putByte(c.version.minor), rval.putBytes(c.session.sp.server_random), writeVector(rval, 1, forge.util.createBuffer(sessionId)), rval.putByte(c.session.cipherSuite.id[0]), rval.putByte(c.session.cipherSuite.id[1]), rval.putByte(c.session.compressionMethod), rval;
    };
    /**
     * @param {!Object} c
     * @return {?}
     */
    tls.createCertificate = function(c) {
      /** @type {boolean} */
      var client = c.entity === tls.ConnectionEnd.client;
      /** @type {null} */
      var cert = null;
      if (c.getCertificate) {
        var callback;
        callback = client ? c.session.certificateRequest : c.session.extensions.server_name.serverNameList;
        cert = c.getCertificate(c, callback);
      }
      var cAs = forge.util.createBuffer();
      if (null !== cert) {
        try {
          if (!forge.util.isArray(cert)) {
            /** @type {!Array} */
            cert = [cert];
          }
          /** @type {null} */
          var data = null;
          /** @type {number} */
          var i = 0;
          for (; i < cert.length; ++i) {
            var msg = forge.pem.decode(cert[i])[0];
            if ("CERTIFICATE" !== msg.type && "X509 CERTIFICATE" !== msg.type && "TRUSTED CERTIFICATE" !== msg.type) {
              /** @type {!Error} */
              var error = new Error('Could not convert certificate from PEM; PEM header type is not "CERTIFICATE", "X509 CERTIFICATE", or "TRUSTED CERTIFICATE".');
              throw error.headerType = msg.type, error;
            }
            if (msg.procType && "ENCRYPTED" === msg.procType.type) {
              throw new Error("Could not convert certificate from PEM; PEM is encrypted.");
            }
            var der = forge.util.createBuffer(msg.body);
            if (null === data) {
              data = forge.asn1.fromDer(der.bytes(), false);
            }
            var buf = forge.util.createBuffer();
            writeVector(buf, 3, der);
            cAs.putBuffer(buf);
          }
          cert = forge.pki.certificateFromAsn1(data);
          if (client) {
            c.session.clientCertificate = cert;
          } else {
            c.session.serverCertificate = cert;
          }
        } catch (err) {
          return c.error(c, {
            message : "Could not send certificate list.",
            cause : err,
            send : true,
            alert : {
              level : tls.Alert.Level.fatal,
              description : tls.Alert.Description.bad_certificate
            }
          });
        }
      }
      var length = 3 + cAs.length();
      var rval = forge.util.createBuffer();
      return rval.putByte(tls.HandshakeType.certificate), rval.putInt24(length), writeVector(rval, 3, cAs), rval;
    };
    /**
     * @param {!Object} c
     * @return {?}
     */
    tls.createClientKeyExchange = function(c) {
      var b = forge.util.createBuffer();
      b.putByte(c.session.clientHelloVersion.major);
      b.putByte(c.session.clientHelloVersion.minor);
      b.putBytes(forge.random.getBytes(46));
      var sp = c.session.sp;
      sp.pre_master_secret = b.getBytes();
      var key = c.session.serverCertificate.publicKey;
      b = key.encrypt(sp.pre_master_secret);
      var length = b.length + 2;
      var rval = forge.util.createBuffer();
      return rval.putByte(tls.HandshakeType.client_key_exchange), rval.putInt24(length), rval.putInt16(b.length), rval.putBytes(b), rval;
    };
    /**
     * @param {!Object} c
     * @return {?}
     */
    tls.createServerKeyExchange = function(c) {
      /** @type {number} */
      var length = 0;
      var rval = forge.util.createBuffer();
      return length > 0 && (rval.putByte(tls.HandshakeType.server_key_exchange), rval.putInt24(length)), rval;
    };
    /**
     * @param {!Object} c
     * @param {!Function} callback
     * @return {undefined}
     */
    tls.getClientSignature = function(c, callback) {
      var b = forge.util.createBuffer();
      b.putBuffer(c.session.md5.digest());
      b.putBuffer(c.session.sha1.digest());
      b = b.getBytes();
      c.getSignature = c.getSignature || function(c, data, callback) {
        /** @type {null} */
        var privateKey = null;
        if (c.getPrivateKey) {
          try {
            privateKey = c.getPrivateKey(c, c.session.clientCertificate);
            privateKey = forge.pki.privateKeyFromPem(privateKey);
          } catch (err) {
            c.error(c, {
              message : "Could not get private key.",
              cause : err,
              send : true,
              alert : {
                level : tls.Alert.Level.fatal,
                description : tls.Alert.Description.internal_error
              }
            });
          }
        }
        if (null === privateKey) {
          c.error(c, {
            message : "No private key set.",
            send : true,
            alert : {
              level : tls.Alert.Level.fatal,
              description : tls.Alert.Description.internal_error
            }
          });
        } else {
          data = privateKey.sign(data, null);
        }
        callback(c, data);
      };
      c.getSignature(c, b, callback);
    };
    /**
     * @param {!Object} c
     * @param {!Object} signature
     * @return {?}
     */
    tls.createCertificateVerify = function(c, signature) {
      var length = signature.length + 2;
      var rval = forge.util.createBuffer();
      return rval.putByte(tls.HandshakeType.certificate_verify), rval.putInt24(length), rval.putInt16(signature.length), rval.putBytes(signature), rval;
    };
    /**
     * @param {!Object} c
     * @return {?}
     */
    tls.createCertificateRequest = function(c) {
      var certTypes = forge.util.createBuffer();
      certTypes.putByte(1);
      var value = forge.util.createBuffer();
      var key;
      for (key in c.caStore.certs) {
        var cert = c.caStore.certs[key];
        var obj = forge.pki.distinguishedNameToAsn1(cert.subject);
        var buffer = forge.asn1.toDer(obj);
        value.putInt16(buffer.length());
        value.putBuffer(buffer);
      }
      var length = 1 + certTypes.length() + 2 + value.length();
      var rval = forge.util.createBuffer();
      return rval.putByte(tls.HandshakeType.certificate_request), rval.putInt24(length), writeVector(rval, 1, certTypes), writeVector(rval, 2, value), rval;
    };
    /**
     * @param {!Object} c
     * @return {?}
     */
    tls.createServerHelloDone = function(c) {
      var rval = forge.util.createBuffer();
      return rval.putByte(tls.HandshakeType.server_hello_done), rval.putInt24(0), rval;
    };
    /**
     * @return {?}
     */
    tls.createChangeCipherSpec = function() {
      var value = forge.util.createBuffer();
      return value.putByte(1), value;
    };
    /**
     * @param {!Object} c
     * @return {?}
     */
    tls.createFinished = function(c) {
      var b = forge.util.createBuffer();
      b.putBuffer(c.session.md5.digest());
      b.putBuffer(c.session.sha1.digest());
      /** @type {boolean} */
      var opt_padHours = c.entity === tls.ConnectionEnd.client;
      var sp = c.session.sp;
      /** @type {number} */
      var vdl = 12;
      /** @type {function(string, string, !Object, number): ?} */
      var prf = prf_TLS1;
      /** @type {string} */
      var label = opt_padHours ? "client finished" : "server finished";
      b = prf(sp.master_secret, label, b.getBytes(), vdl);
      var rval = forge.util.createBuffer();
      return rval.putByte(tls.HandshakeType.finished), rval.putInt24(b.length()), rval.putBuffer(b), rval;
    };
    /**
     * @param {number} num
     * @param {!Object} data
     * @param {number} count
     * @return {?}
     */
    tls.createHeartbeat = function(num, data, count) {
      if ("undefined" == typeof count) {
        count = data.length;
      }
      var buffer = forge.util.createBuffer();
      buffer.putByte(num);
      buffer.putInt16(count);
      buffer.putBytes(data);
      var start_count = buffer.length();
      /** @type {number} */
      var length = Math.max(16, start_count - count - 3);
      return buffer.putBytes(forge.random.getBytes(length)), buffer;
    };
    /**
     * @param {!Object} c
     * @param {string} record
     * @return {undefined}
     */
    tls.queue = function(c, record) {
      if (record && (0 !== record.fragment.length() || record.type !== tls.ContentType.handshake && record.type !== tls.ContentType.alert && record.type !== tls.ContentType.change_cipher_spec)) {
        if (record.type === tls.ContentType.handshake) {
          var r = record.fragment.bytes();
          c.session.md5.update(r);
          c.session.sha1.update(r);
          /** @type {null} */
          r = null;
        }
        var results;
        if (record.fragment.length() <= tls.MaxFragment) {
          /** @type {!Array} */
          results = [record];
        } else {
          /** @type {!Array} */
          results = [];
          var data = record.fragment.bytes();
          for (; data.length > tls.MaxFragment;) {
            results.push(tls.createRecord(c, {
              type : record.type,
              data : forge.util.createBuffer(data.slice(0, tls.MaxFragment))
            }));
            data = data.slice(tls.MaxFragment);
          }
          if (data.length > 0) {
            results.push(tls.createRecord(c, {
              type : record.type,
              data : forge.util.createBuffer(data)
            }));
          }
        }
        /** @type {number} */
        var i = 0;
        for (; i < results.length && !c.fail; ++i) {
          var info = results[i];
          var t = c.state.current.write;
          if (t.update(c, info)) {
            c.records.push(info);
          }
        }
      }
    };
    /**
     * @param {!Object} c
     * @return {?}
     */
    tls.flush = function(c) {
      /** @type {number} */
      var i = 0;
      for (; i < c.records.length; ++i) {
        var record = c.records[i];
        c.tlsData.putByte(record.type);
        c.tlsData.putByte(record.version.major);
        c.tlsData.putByte(record.version.minor);
        c.tlsData.putInt16(record.fragment.length());
        c.tlsData.putBuffer(c.records[i].fragment);
      }
      return c.records = [], c.tlsDataReady(c);
    };
    /**
     * @param {!Object} error
     * @return {?}
     */
    var _certErrorToAlertDesc = function(error) {
      switch(error) {
        case true:
          return true;
        case forge.pki.certificateError.bad_certificate:
          return tls.Alert.Description.bad_certificate;
        case forge.pki.certificateError.unsupported_certificate:
          return tls.Alert.Description.unsupported_certificate;
        case forge.pki.certificateError.certificate_revoked:
          return tls.Alert.Description.certificate_revoked;
        case forge.pki.certificateError.certificate_expired:
          return tls.Alert.Description.certificate_expired;
        case forge.pki.certificateError.certificate_unknown:
          return tls.Alert.Description.certificate_unknown;
        case forge.pki.certificateError.unknown_ca:
          return tls.Alert.Description.unknown_ca;
        default:
          return tls.Alert.Description.bad_certificate;
      }
    };
    /**
     * @param {?} desc
     * @return {?}
     */
    var _alertDescToCertError = function(desc) {
      switch(desc) {
        case true:
          return true;
        case tls.Alert.Description.bad_certificate:
          return forge.pki.certificateError.bad_certificate;
        case tls.Alert.Description.unsupported_certificate:
          return forge.pki.certificateError.unsupported_certificate;
        case tls.Alert.Description.certificate_revoked:
          return forge.pki.certificateError.certificate_revoked;
        case tls.Alert.Description.certificate_expired:
          return forge.pki.certificateError.certificate_expired;
        case tls.Alert.Description.certificate_unknown:
          return forge.pki.certificateError.certificate_unknown;
        case tls.Alert.Description.unknown_ca:
          return forge.pki.certificateError.unknown_ca;
        default:
          return forge.pki.certificateError.bad_certificate;
      }
    };
    /**
     * @param {!Object} c
     * @param {!Array} chain
     * @return {?}
     */
    tls.verifyCertificateChain = function(c, chain) {
      try {
        forge.pki.verifyCertificateChain(c.caStore, chain, function(result, done, chain) {
          var ret = (_certErrorToAlertDesc(result), c.verify(c, result, done, chain));
          if (ret !== true) {
            if ("object" == typeof ret && !forge.util.isArray(ret)) {
              /** @type {!Error} */
              var self = new Error("The application rejected the certificate.");
              throw self.send = true, self.alert = {
                level : tls.Alert.Level.fatal,
                description : tls.Alert.Description.bad_certificate
              }, ret.message && (self.message = ret.message), ret.alert && (self.alert.description = ret.alert), self;
            }
            if (ret !== result) {
              ret = _alertDescToCertError(ret);
            }
          }
          return ret;
        });
      } catch (e) {
        var ex = e;
        if ("object" != typeof ex || forge.util.isArray(ex)) {
          ex = {
            send : true,
            alert : {
              level : tls.Alert.Level.fatal,
              description : _certErrorToAlertDesc(e)
            }
          };
        }
        if (!("send" in ex)) {
          /** @type {boolean} */
          ex.send = true;
        }
        if (!("alert" in ex)) {
          ex.alert = {
            level : tls.Alert.Level.fatal,
            description : _certErrorToAlertDesc(ex.error)
          };
        }
        c.error(c, ex);
      }
      return !c.fail;
    };
    /**
     * @param {!Object} cache
     * @param {number} capacity
     * @return {?}
     */
    tls.createSessionCache = function(cache, capacity) {
      /** @type {null} */
      var rval = null;
      if (cache && cache.getSession && cache.setSession && cache.order) {
        /** @type {!Object} */
        rval = cache;
      } else {
        rval = {};
        rval.cache = cache || {};
        /** @type {number} */
        rval.capacity = Math.max(capacity || 100, 1);
        /** @type {!Array} */
        rval.order = [];
        var layerKey;
        for (layerKey in cache) {
          if (rval.order.length <= capacity) {
            rval.order.push(layerKey);
          } else {
            delete cache[layerKey];
          }
        }
        /**
         * @param {(Object|string)} name
         * @return {?}
         */
        rval.getSession = function(name) {
          /** @type {null} */
          var session = null;
          /** @type {null} */
          var key = null;
          if (name ? key = forge.util.bytesToHex(name) : rval.order.length > 0 && (key = rval.order[0]), null !== key && key in rval.cache) {
            session = rval.cache[key];
            delete rval.cache[key];
            var i;
            for (i in rval.order) {
              if (rval.order[i] === key) {
                rval.order.splice(i, 1);
                break;
              }
            }
          }
          return session;
        };
        /**
         * @param {(Object|string)} name
         * @param {!Object} key
         * @return {undefined}
         */
        rval.setSession = function(name, key) {
          if (rval.order.length === rval.capacity) {
            var i = rval.order.shift();
            delete rval.cache[i];
          }
          i = forge.util.bytesToHex(name);
          rval.order.push(i);
          /** @type {!Object} */
          rval.cache[i] = key;
        };
      }
      return rval;
    };
    /**
     * @param {!Object} options
     * @return {?}
     */
    tls.createConnection = function(options) {
      /** @type {null} */
      var caStore = null;
      caStore = options.caStore ? forge.util.isArray(options.caStore) ? forge.pki.createCaStore(options.caStore) : options.caStore : forge.pki.createCaStore();
      var cipherSuites = options.cipherSuites || null;
      if (null === cipherSuites) {
        /** @type {!Array} */
        cipherSuites = [];
        var key;
        for (key in tls.CipherSuites) {
          cipherSuites.push(tls.CipherSuites[key]);
        }
      }
      /** @type {number} */
      var _shapename = options.server ? tls.ConnectionEnd.server : tls.ConnectionEnd.client;
      var sessionCache = options.sessionCache ? tls.createSessionCache(options.sessionCache) : null;
      var c = {
        version : {
          major : tls.Version.major,
          minor : tls.Version.minor
        },
        entity : _shapename,
        sessionId : options.sessionId,
        caStore : caStore,
        sessionCache : sessionCache,
        cipherSuites : cipherSuites,
        connected : options.connected,
        virtualHost : options.virtualHost || null,
        verifyClient : options.verifyClient || false,
        verify : options.verify || function(uri, user, val, stat) {
          return user;
        },
        getCertificate : options.getCertificate || null,
        getPrivateKey : options.getPrivateKey || null,
        getSignature : options.getSignature || null,
        input : forge.util.createBuffer(),
        tlsData : forge.util.createBuffer(),
        data : forge.util.createBuffer(),
        tlsDataReady : options.tlsDataReady,
        dataReady : options.dataReady,
        heartbeatReceived : options.heartbeatReceived,
        closed : options.closed,
        error : function(c, ex) {
          ex.origin = ex.origin || (c.entity === tls.ConnectionEnd.client ? "client" : "server");
          if (ex.send) {
            tls.queue(c, tls.createAlert(c, ex.alert));
            tls.flush(c);
          }
          /** @type {boolean} */
          var a = ex.fatal !== false;
          if (a) {
            /** @type {boolean} */
            c.fail = true;
          }
          options.error(c, ex);
          if (a) {
            c.close(false);
          }
        },
        deflate : options.deflate || null,
        inflate : options.inflate || null
      };
      /**
       * @param {string} b
       * @return {undefined}
       */
      c.reset = function(b) {
        c.version = {
          major : tls.Version.major,
          minor : tls.Version.minor
        };
        /** @type {null} */
        c.record = null;
        /** @type {null} */
        c.session = null;
        /** @type {null} */
        c.peerCertificate = null;
        c.state = {
          pending : null,
          current : null
        };
        /** @type {number} */
        c.expect = c.entity === tls.ConnectionEnd.client ? CCE : CHE;
        /** @type {null} */
        c.fragmented = null;
        /** @type {!Array} */
        c.records = [];
        /** @type {boolean} */
        c.open = false;
        /** @type {number} */
        c.handshakes = 0;
        /** @type {boolean} */
        c.handshaking = false;
        /** @type {boolean} */
        c.isConnected = false;
        /** @type {boolean} */
        c.fail = !(b || "undefined" == typeof b);
        c.input.clear();
        c.tlsData.clear();
        c.data.clear();
        c.state.current = tls.createConnectionState(c);
      };
      c.reset();
      /**
       * @param {!Object} c
       * @param {!Object} record
       * @return {undefined}
       */
      var _update = function(c, record) {
        /** @type {number} */
        var type = record.type - tls.ContentType.change_cipher_spec;
        var handlers = ctTable[c.entity][c.expect];
        if (type in handlers) {
          handlers[type](c, record);
        } else {
          tls.handleUnexpected(c, record);
        }
      };
      /**
       * @param {!Object} c
       * @return {?}
       */
      var _readRecordHeader = function(c) {
        /** @type {number} */
        var rval = 0;
        var b = c.input;
        var len = b.length();
        if (len < 5) {
          /** @type {number} */
          rval = 5 - len;
        } else {
          c.record = {
            type : b.getByte(),
            version : {
              major : b.getByte(),
              minor : b.getByte()
            },
            length : b.getInt16(),
            fragment : forge.util.createBuffer(),
            ready : false
          };
          /** @type {boolean} */
          var config = c.record.version.major === c.version.major;
          if (config && c.session && c.session.version) {
            /** @type {boolean} */
            config = c.record.version.minor === c.version.minor;
          }
          if (!config) {
            c.error(c, {
              message : "Incompatible TLS version.",
              send : true,
              alert : {
                level : tls.Alert.Level.fatal,
                description : tls.Alert.Description.protocol_version
              }
            });
          }
        }
        return rval;
      };
      /**
       * @param {!Object} c
       * @return {?}
       */
      var _readRecord = function(c) {
        /** @type {number} */
        var contextDistance = 0;
        var b = c.input;
        var i = b.length();
        if (i < c.record.length) {
          /** @type {number} */
          contextDistance = c.record.length - i;
        } else {
          c.record.fragment.putBytes(b.getBytes(c.record.length));
          b.compact();
          var s = c.state.current.read;
          if (s.update(c, c.record)) {
            if (null !== c.fragmented) {
              if (c.fragmented.type === c.record.type) {
                c.fragmented.fragment.putBuffer(c.record.fragment);
                c.record = c.fragmented;
              } else {
                c.error(c, {
                  message : "Invalid fragmented record.",
                  send : true,
                  alert : {
                    level : tls.Alert.Level.fatal,
                    description : tls.Alert.Description.unexpected_message
                  }
                });
              }
            }
            /** @type {boolean} */
            c.record.ready = true;
          }
        }
        return contextDistance;
      };
      return c.handshake = function(url) {
        if (c.entity !== tls.ConnectionEnd.client) {
          c.error(c, {
            message : "Cannot initiate handshake as a server.",
            fatal : false
          });
        } else {
          if (c.handshaking) {
            c.error(c, {
              message : "Handshake already in progress.",
              fatal : false
            });
          } else {
            if (c.fail && !c.open && 0 === c.handshakes) {
              /** @type {boolean} */
              c.fail = false;
            }
            /** @type {boolean} */
            c.handshaking = true;
            url = url || "";
            /** @type {null} */
            var self = null;
            if (url.length > 0) {
              if (c.sessionCache) {
                self = c.sessionCache.getSession(url);
              }
              if (null === self) {
                /** @type {string} */
                url = "";
              }
            }
            if (0 === url.length && c.sessionCache) {
              self = c.sessionCache.getSession();
              if (null !== self) {
                url = self.id;
              }
            }
            c.session = {
              id : url,
              version : null,
              cipherSuite : null,
              compressionMethod : null,
              serverCertificate : null,
              certificateRequest : null,
              clientCertificate : null,
              sp : {},
              md5 : forge.md.md5.create(),
              sha1 : forge.md.sha1.create()
            };
            if (self) {
              c.version = self.version;
              c.session.sp = self.sp;
            }
            c.session.sp.client_random = tls.createRandom().getBytes();
            /** @type {boolean} */
            c.open = true;
            tls.queue(c, tls.createRecord(c, {
              type : tls.ContentType.handshake,
              data : tls.createClientHello(c)
            }));
            tls.flush(c);
          }
        }
      }, c.process = function(data) {
        /** @type {number} */
        var rval = 0;
        return data && c.input.putBytes(data), c.fail || (null !== c.record && c.record.ready && c.record.fragment.isEmpty() && (c.record = null), null === c.record && (rval = _readRecordHeader(c)), c.fail || null === c.record || c.record.ready || (rval = _readRecord(c)), !c.fail && null !== c.record && c.record.ready && _update(c, c.record)), rval;
      }, c.prepare = function(data) {
        return tls.queue(c, tls.createRecord(c, {
          type : tls.ContentType.application_data,
          data : forge.util.createBuffer(data)
        })), tls.flush(c);
      }, c.prepareHeartbeatRequest = function(options, limit) {
        return options instanceof forge.util.ByteBuffer && (options = options.bytes()), "undefined" == typeof limit && (limit = options.length), c.expectedHeartbeatPayload = options, tls.queue(c, tls.createRecord(c, {
          type : tls.ContentType.heartbeat,
          data : tls.createHeartbeat(tls.HeartbeatMessageType.heartbeat_request, options, limit)
        })), tls.flush(c);
      }, c.close = function(e) {
        if (!c.fail && c.sessionCache && c.session) {
          var data = {
            id : c.session.id,
            version : c.session.version,
            sp : c.session.sp
          };
          /** @type {null} */
          data.sp.keys = null;
          c.sessionCache.setSession(data.id, data);
        }
        if (c.open) {
          /** @type {boolean} */
          c.open = false;
          c.input.clear();
          if (c.isConnected || c.handshaking) {
            /** @type {boolean} */
            c.isConnected = c.handshaking = false;
            tls.queue(c, tls.createAlert(c, {
              level : tls.Alert.Level.warning,
              description : tls.Alert.Description.close_notify
            }));
            tls.flush(c);
          }
          c.closed(c);
        }
        c.reset(e);
      }, c;
    };
    mixin.exports = forge.tls = forge.tls || {};
    var key;
    for (key in tls) {
      if ("function" != typeof tls[key]) {
        forge.tls[key] = tls[key];
      }
    }
    /** @type {function(string, string, !Object, number): ?} */
    forge.tls.prf_tls1 = prf_TLS1;
    /** @type {function(!Object, !Object, !Object): ?} */
    forge.tls.hmac_sha1 = hmac_sha1;
    /** @type {function(!Object, number): ?} */
    forge.tls.createSessionCache = tls.createSessionCache;
    /** @type {function(!Object): ?} */
    forge.tls.createConnection = tls.createConnection;
  }, function(canCreateDiscussions, isSlidingUp) {
  }, function(module, canCreateDiscussions, factory) {
    module.exports = factory(0);
    factory(5);
    factory(34);
    factory(3);
    factory(12);
    factory(19);
    factory(10);
    factory(8);
    factory(35);
    factory(20);
    factory(36);
    factory(21);
    factory(15);
    factory(7);
    factory(23);
    factory(24);
    factory(38);
    factory(26);
    factory(27);
    factory(28);
    factory(16);
    factory(2);
    factory(29);
    factory(40);
    factory(41);
    factory(31);
    factory(1);
  }, function(module, canCreateDiscussions, FORGE) {
    /**
     * @param {!File} state
     * @param {!Object} c
     * @param {!Object} sp
     * @return {undefined}
     */
    function initConnectionState(state, c, sp) {
      /** @type {boolean} */
      var client = c.entity === forge.tls.ConnectionEnd.client;
      state.read.cipherState = {
        init : false,
        cipher : forge.cipher.createDecipher("AES-CBC", client ? sp.keys.server_write_key : sp.keys.client_write_key),
        iv : client ? sp.keys.server_write_IV : sp.keys.client_write_IV
      };
      state.write.cipherState = {
        init : false,
        cipher : forge.cipher.createCipher("AES-CBC", client ? sp.keys.client_write_key : sp.keys.server_write_key),
        iv : client ? sp.keys.client_write_IV : sp.keys.server_write_IV
      };
      /** @type {function(!Object, ?): ?} */
      state.read.cipherFunction = decrypt_aes_cbc_sha1;
      /** @type {function(!Object, ?): ?} */
      state.write.cipherFunction = encrypt_aes_cbc_sha1;
      state.read.macLength = state.write.macLength = sp.mac_length;
      state.read.macFunction = state.write.macFunction = tls.hmac_sha1;
    }
    /**
     * @param {!Object} record
     * @param {?} s
     * @return {?}
     */
    function encrypt_aes_cbc_sha1(record, s) {
      /** @type {boolean} */
      var r = false;
      var data = s.macFunction(s.macKey, s.sequenceNumber, record);
      record.fragment.putBytes(data);
      s.updateSequenceNumber();
      var iv;
      iv = record.version.minor === tls.Versions.TLS_1_0.minor ? s.cipherState.init ? null : s.cipherState.iv : forge.random.getBytesSync(16);
      /** @type {boolean} */
      s.cipherState.init = true;
      var cipher = s.cipherState.cipher;
      return cipher.start({
        iv : iv
      }), record.version.minor >= tls.Versions.TLS_1_1.minor && cipher.output.putBytes(iv), cipher.update(record.fragment), cipher.finish(encrypt_aes_cbc_sha1_padding) && (record.fragment = cipher.output, record.length = record.fragment.length(), r = true), r;
    }
    /**
     * @param {number} blockSize
     * @param {!NodeList} input
     * @param {?} decrypt
     * @return {?}
     */
    function encrypt_aes_cbc_sha1_padding(blockSize, input, decrypt) {
      if (!decrypt) {
        /** @type {number} */
        var padding = blockSize - input.length() % blockSize;
        input.fillWithByte(padding - 1, padding);
      }
      return true;
    }
    /**
     * @param {?} cond
     * @param {!Object} t
     * @param {?} xgh2
     * @return {?}
     */
    function decrypt_aes_cbc_sha1_padding(cond, t, xgh2) {
      /** @type {boolean} */
      var dataColumn = true;
      if (xgh2) {
        var len2 = t.length();
        var i = t.last();
        /** @type {number} */
        var j = len2 - 1 - i;
        for (; j < len2 - 1; ++j) {
          /** @type {boolean} */
          dataColumn = dataColumn && t.at(j) == i;
        }
        if (dataColumn) {
          t.truncate(i + 1);
        }
      }
      return dataColumn;
    }
    /**
     * @param {!Object} record
     * @param {?} s
     * @return {?}
     */
    function decrypt_aes_cbc_sha1(record, s) {
      /** @type {boolean} */
      var rval = false;
      ++p;
      var iv;
      iv = record.version.minor === tls.Versions.TLS_1_0.minor ? s.cipherState.init ? null : s.cipherState.iv : record.fragment.getBytes(16);
      /** @type {boolean} */
      s.cipherState.init = true;
      var cipher = s.cipherState.cipher;
      cipher.start({
        iv : iv
      });
      cipher.update(record.fragment);
      rval = cipher.finish(decrypt_aes_cbc_sha1_padding);
      var length = s.macLength;
      var mac = forge.random.getBytesSync(length);
      var i = cipher.output.length();
      if (i >= length) {
        record.fragment = cipher.output.getBytes(i - length);
        mac = cipher.output.getBytes(length);
      } else {
        record.fragment = cipher.output.getBytes();
      }
      record.fragment = forge.util.createBuffer(record.fragment);
      record.length = record.fragment.length();
      var mac2 = s.macFunction(s.macKey, s.sequenceNumber, record);
      return s.updateSequenceNumber(), rval = compareMacs(s.macKey, mac, mac2) && rval;
    }
    /**
     * @param {!Object} key
     * @param {!Object} x
     * @param {!Object} type
     * @return {?}
     */
    function compareMacs(key, x, type) {
      var hmac = forge.hmac.create();
      return hmac.start("SHA1", key), hmac.update(x), x = hmac.digest().getBytes(), hmac.start(null, null), hmac.update(type), type = hmac.digest().getBytes(), x === type;
    }
    var forge = FORGE(0);
    FORGE(5);
    FORGE(31);
    var tls = module.exports = forge.tls;
    tls.CipherSuites.TLS_RSA_WITH_AES_128_CBC_SHA = {
      id : [0, 47],
      name : "TLS_RSA_WITH_AES_128_CBC_SHA",
      initSecurityParameters : function(sp) {
        sp.bulk_cipher_algorithm = tls.BulkCipherAlgorithm.aes;
        sp.cipher_type = tls.CipherType.block;
        /** @type {number} */
        sp.enc_key_length = 16;
        /** @type {number} */
        sp.block_length = 16;
        /** @type {number} */
        sp.fixed_iv_length = 16;
        /** @type {number} */
        sp.record_iv_length = 16;
        sp.mac_algorithm = tls.MACAlgorithm.hmac_sha1;
        /** @type {number} */
        sp.mac_length = 20;
        /** @type {number} */
        sp.mac_key_length = 20;
      },
      initConnectionState : initConnectionState
    };
    tls.CipherSuites.TLS_RSA_WITH_AES_256_CBC_SHA = {
      id : [0, 53],
      name : "TLS_RSA_WITH_AES_256_CBC_SHA",
      initSecurityParameters : function(sp) {
        sp.bulk_cipher_algorithm = tls.BulkCipherAlgorithm.aes;
        sp.cipher_type = tls.CipherType.block;
        /** @type {number} */
        sp.enc_key_length = 32;
        /** @type {number} */
        sp.block_length = 16;
        /** @type {number} */
        sp.fixed_iv_length = 16;
        /** @type {number} */
        sp.record_iv_length = 16;
        sp.mac_algorithm = tls.MACAlgorithm.hmac_sha1;
        /** @type {number} */
        sp.mac_length = 20;
        /** @type {number} */
        sp.mac_key_length = 20;
      },
      initConnectionState : initConnectionState
    };
    /** @type {number} */
    var p = 0;
  }, function(mixin, canCreateDiscussions, FORGE) {
    /**
     * @param {!Object} kdf
     * @param {!Object} md
     * @param {number} counterStart
     * @param {number} length
     * @return {undefined}
     */
    function _createKDF(kdf, md, counterStart, length) {
      /**
       * @param {number} x
       * @param {number} n
       * @return {?}
       */
      kdf.generate = function(x, n) {
        var result = new forge.util.ByteBuffer;
        var k = Math.ceil(n / length) + counterStart;
        var b = new forge.util.ByteBuffer;
        /** @type {number} */
        var i = counterStart;
        for (; i < k; ++i) {
          b.putInt32(i);
          md.start();
          md.update(x + b.getBytes());
          var bytes = md.digest();
          result.putBytes(bytes.getBytes(length));
        }
        return result.truncate(result.length() - n), result.getBytes();
      };
    }
    var forge = FORGE(0);
    FORGE(1);
    FORGE(2);
    FORGE(13);
    mixin.exports = forge.kem = forge.kem || {};
    var BigInteger = forge.jsbn.BigInteger;
    forge.kem.rsa = {};
    /**
     * @param {!Object} a
     * @param {number} options
     * @return {?}
     */
    forge.kem.rsa.create = function(a, options) {
      options = options || {};
      var ctx = options.prng || forge.random;
      var FileCrypto = {};
      return FileCrypto.encrypt = function(key, val) {
        var r;
        /** @type {number} */
        var size = Math.ceil(key.n.bitLength() / 8);
        do {
          r = (new BigInteger(forge.util.bytesToHex(ctx.getBytesSync(size)), 16)).mod(key.n);
        } while (r.equals(BigInteger.ZERO));
        r = forge.util.hexToBytes(r.toString(16));
        /** @type {number} */
        var col = size - r.length;
        if (col > 0) {
          r = forge.util.fillString(String.fromCharCode(0), col) + r;
        }
        var encapsulation = key.encrypt(r, "NONE");
        var list = a.generate(r, val);
        return {
          encapsulation : encapsulation,
          key : list
        };
      }, FileCrypto.decrypt = function(key, val, next) {
        var r = key.decrypt(val, "NONE");
        return a.generate(r, next);
      }, FileCrypto;
    };
    /**
     * @param {!Object} md
     * @param {number} digestLength
     * @return {undefined}
     */
    forge.kem.kdf1 = function(md, digestLength) {
      _createKDF(this, md, 0, digestLength || md.digestLength);
    };
    /**
     * @param {!Object} md
     * @param {number} digestLength
     * @return {undefined}
     */
    forge.kem.kdf2 = function(md, digestLength) {
      _createKDF(this, md, 1, digestLength || md.digestLength);
    };
  }, function(module, canCreateDiscussions, factory) {
    module.exports = factory(4);
    factory(14);
    factory(9);
    factory(30);
    factory(39);
  }, function(mixin, canCreateDiscussions, FORGE) {
    var forge = FORGE(0);
    FORGE(21);
    mixin.exports = forge.mgf = forge.mgf || {};
    forge.mgf.mgf1 = forge.mgf1;
  }, function(mixin, canCreateDiscussions, FORGE) {
    /**
     * @param {string} obj
     * @return {?}
     */
    function _recipientFromAsn1(obj) {
      var capture = {};
      /** @type {!Array} */
      var errors = [];
      if (!asn1.validate(obj, p7.asn1.recipientInfoValidator, capture, errors)) {
        /** @type {!Error} */
        var error = new Error("Cannot read PKCS#7 RecipientInfo. ASN.1 object is not an PKCS#7 RecipientInfo.");
        throw error.errors = errors, error;
      }
      return {
        version : capture.version.charCodeAt(0),
        issuer : forge.pki.RDNAttributesAsArray(capture.issuer),
        serialNumber : forge.util.createBuffer(capture.serial).toHex(),
        encryptedContent : {
          algorithm : asn1.derToOid(capture.encAlgorithm),
          parameter : capture.encParameter.value,
          content : capture.encKey
        }
      };
    }
    /**
     * @param {!Object} obj
     * @return {?}
     */
    function _recipientToAsn1(obj) {
      return asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, asn1.integerToDer(obj.version).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [forge.pki.distinguishedNameToAsn1({
        attributes : obj.issuer
      }), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, forge.util.hexToBytes(obj.serialNumber))]), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(obj.encryptedContent.algorithm).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.NULL, false, "")]), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OCTETSTRING, false, obj.encryptedContent.content)]);
    }
    /**
     * @param {!NodeList} infos
     * @return {?}
     */
    function _recipientsFromAsn1(infos) {
      /** @type {!Array} */
      var ret = [];
      /** @type {number} */
      var i = 0;
      for (; i < infos.length; ++i) {
        ret.push(_recipientFromAsn1(infos[i]));
      }
      return ret;
    }
    /**
     * @param {!NodeList} recipients
     * @return {?}
     */
    function _recipientsToAsn1(recipients) {
      /** @type {!Array} */
      var ret = [];
      /** @type {number} */
      var i = 0;
      for (; i < recipients.length; ++i) {
        ret.push(_recipientToAsn1(recipients[i]));
      }
      return ret;
    }
    /**
     * @param {!Object} obj
     * @return {?}
     */
    function _signerToAsn1(obj) {
      var rval = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, asn1.integerToDer(obj.version).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [forge.pki.distinguishedNameToAsn1({
        attributes : obj.issuer
      }), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, forge.util.hexToBytes(obj.serialNumber))]), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(obj.digestAlgorithm).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.NULL, false, "")])]);
      if (obj.authenticatedAttributesAsn1 && rval.value.push(obj.authenticatedAttributesAsn1), rval.value.push(asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(obj.signatureAlgorithm).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.NULL, false, "")])), rval.value.push(asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OCTETSTRING, false, obj.signature)), obj.unauthenticatedAttributes.length > 0) {
        var r = asn1.create(asn1.Class.CONTEXT_SPECIFIC, 1, true, []);
        /** @type {number} */
        var i = 0;
        for (; i < obj.unauthenticatedAttributes.length; ++i) {
          var attr = obj.unauthenticatedAttributes[i];
          r.values.push(_attributeToAsn1(attr));
        }
        rval.value.push(r);
      }
      return rval;
    }
    /**
     * @param {!NodeList} signers
     * @return {?}
     */
    function _signersToAsn1(signers) {
      /** @type {!Array} */
      var ret = [];
      /** @type {number} */
      var i = 0;
      for (; i < signers.length; ++i) {
        ret.push(_signerToAsn1(signers[i]));
      }
      return ret;
    }
    /**
     * @param {!Object} attr
     * @return {?}
     */
    function _attributeToAsn1(attr) {
      var tbsCertificate;
      if (attr.type === forge.pki.oids.contentType) {
        tbsCertificate = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(attr.value).getBytes());
      } else {
        if (attr.type === forge.pki.oids.messageDigest) {
          tbsCertificate = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OCTETSTRING, false, attr.value.bytes());
        } else {
          if (attr.type === forge.pki.oids.signingTime) {
            /** @type {!Date} */
            var jan_1_1950 = new Date("1950-01-01T00:00:00Z");
            /** @type {!Date} */
            var jan_1_2050 = new Date("2050-01-01T00:00:00Z");
            var date = attr.value;
            if ("string" == typeof date) {
              /** @type {number} */
              var str = Date.parse(date);
              date = isNaN(str) ? 13 === date.length ? asn1.utcTimeToDate(date) : asn1.generalizedTimeToDate(date) : new Date(str);
            }
            tbsCertificate = date >= jan_1_1950 && date < jan_1_2050 ? asn1.create(asn1.Class.UNIVERSAL, asn1.Type.UTCTIME, false, asn1.dateToUtcTime(date)) : asn1.create(asn1.Class.UNIVERSAL, asn1.Type.GENERALIZEDTIME, false, asn1.dateToGeneralizedTime(date));
          }
        }
      }
      return asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(attr.type).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SET, true, [tbsCertificate])]);
    }
    /**
     * @param {!Object} ec
     * @return {?}
     */
    function _encryptedContentToAsn1(ec) {
      return [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(forge.pki.oids.data).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(ec.algorithm).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OCTETSTRING, false, ec.parameter.getBytes())]), asn1.create(asn1.Class.CONTEXT_SPECIFIC, 0, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OCTETSTRING, false, ec.content.getBytes())])];
    }
    /**
     * @param {!Object} msg
     * @param {string} obj
     * @param {string} validator
     * @return {?}
     */
    function _fromAsn1(msg, obj, validator) {
      var capture = {};
      /** @type {!Array} */
      var errors = [];
      if (!asn1.validate(obj, validator, capture, errors)) {
        /** @type {!Error} */
        var error = new Error("Cannot read PKCS#7 message. ASN.1 object is not a supported PKCS#7 message.");
        throw error.errors = error, error;
      }
      var contentType = asn1.derToOid(capture.contentType);
      if (contentType !== forge.pki.oids.data) {
        throw new Error("Unsupported PKCS#7 message. Only wrapped ContentType Data supported.");
      }
      if (capture.encryptedContent) {
        /** @type {string} */
        var content = "";
        if (forge.util.isArray(capture.encryptedContent)) {
          /** @type {number} */
          var i = 0;
          for (; i < capture.encryptedContent.length; ++i) {
            if (capture.encryptedContent[i].type !== asn1.Type.OCTETSTRING) {
              throw new Error("Malformed PKCS#7 message, expecting encrypted content constructed of only OCTET STRING objects.");
            }
            /** @type {string} */
            content = content + capture.encryptedContent[i].value;
          }
        } else {
          content = capture.encryptedContent;
        }
        msg.encryptedContent = {
          algorithm : asn1.derToOid(capture.encAlgorithm),
          parameter : forge.util.createBuffer(capture.encParameter.value),
          content : forge.util.createBuffer(content)
        };
      }
      if (capture.content) {
        /** @type {string} */
        content = "";
        if (forge.util.isArray(capture.content)) {
          /** @type {number} */
          i = 0;
          for (; i < capture.content.length; ++i) {
            if (capture.content[i].type !== asn1.Type.OCTETSTRING) {
              throw new Error("Malformed PKCS#7 message, expecting content constructed of only OCTET STRING objects.");
            }
            /** @type {string} */
            content = content + capture.content[i].value;
          }
        } else {
          content = capture.content;
        }
        msg.content = forge.util.createBuffer(content);
      }
      return msg.version = capture.version.charCodeAt(0), msg.rawCapture = capture, capture;
    }
    /**
     * @param {!Object} msg
     * @return {undefined}
     */
    function _decryptContent(msg) {
      if (void 0 === msg.encryptedContent.key) {
        throw new Error("Symmetric key not available.");
      }
      if (void 0 === msg.content) {
        var ciph;
        switch(msg.encryptedContent.algorithm) {
          case forge.pki.oids["aes128-CBC"]:
          case forge.pki.oids["aes192-CBC"]:
          case forge.pki.oids["aes256-CBC"]:
            ciph = forge.aes.createDecryptionCipher(msg.encryptedContent.key);
            break;
          case forge.pki.oids.desCBC:
          case forge.pki.oids["des-EDE3-CBC"]:
            ciph = forge.des.createDecryptionCipher(msg.encryptedContent.key);
            break;
          default:
            throw new Error("Unsupported symmetric cipher, OID " + msg.encryptedContent.algorithm);
        }
        if (ciph.start(msg.encryptedContent.parameter), ciph.update(msg.encryptedContent.content), !ciph.finish()) {
          throw new Error("Symmetric decryption failed.");
        }
        msg.content = ciph.output;
      }
    }
    var forge = FORGE(0);
    FORGE(5);
    FORGE(3);
    FORGE(10);
    FORGE(6);
    FORGE(7);
    FORGE(25);
    FORGE(2);
    FORGE(1);
    FORGE(17);
    var asn1 = forge.asn1;
    var p7 = mixin.exports = forge.pkcs7 = forge.pkcs7 || {};
    /**
     * @param {?} e
     * @return {?}
     */
    p7.messageFromPem = function(e) {
      var msg = forge.pem.decode(e)[0];
      if ("PKCS7" !== msg.type) {
        /** @type {!Error} */
        var error = new Error('Could not convert PKCS#7 message from PEM; PEM header type is not "PKCS#7".');
        throw error.headerType = msg.type, error;
      }
      if (msg.procType && "ENCRYPTED" === msg.procType.type) {
        throw new Error("Could not convert PKCS#7 message from PEM; PEM is encrypted.");
      }
      var obj = asn1.fromDer(msg.body);
      return p7.messageFromAsn1(obj);
    };
    /**
     * @param {?} msg
     * @param {?} maxline
     * @return {?}
     */
    p7.messageToPem = function(msg, maxline) {
      var r = {
        type : "PKCS7",
        body : asn1.toDer(msg.toAsn1()).getBytes()
      };
      return forge.pem.encode(r, {
        maxline : maxline
      });
    };
    /**
     * @param {string} obj
     * @return {?}
     */
    p7.messageFromAsn1 = function(obj) {
      var capture = {};
      /** @type {!Array} */
      var errors = [];
      if (!asn1.validate(obj, p7.asn1.contentInfoValidator, capture, errors)) {
        /** @type {!Error} */
        var error = new Error("Cannot read PKCS#7 message. ASN.1 object is not an PKCS#7 ContentInfo.");
        throw error.errors = errors, error;
      }
      var msg;
      var i = asn1.derToOid(capture.contentType);
      switch(i) {
        case forge.pki.oids.envelopedData:
          msg = p7.createEnvelopedData();
          break;
        case forge.pki.oids.encryptedData:
          msg = p7.createEncryptedData();
          break;
        case forge.pki.oids.signedData:
          msg = p7.createSignedData();
          break;
        default:
          throw new Error("Cannot read PKCS#7 message. ContentType with OID " + i + " is not (yet) supported.");
      }
      return msg.fromAsn1(capture.content.value[0]), msg;
    };
    /**
     * @return {?}
     */
    p7.createSignedData = function() {
      /**
       * @return {?}
       */
      function addDigestAlgorithmIds() {
        var mds = {};
        /** @type {number} */
        var i = 0;
        for (; i < msg.signers.length; ++i) {
          var signer = msg.signers[i];
          var oid = signer.digestAlgorithm;
          if (!(oid in mds)) {
            mds[oid] = forge.md[forge.pki.oids[oid]].create();
          }
          if (0 === signer.authenticatedAttributes.length) {
            signer.md = mds[oid];
          } else {
            signer.md = forge.md[forge.pki.oids[oid]].create();
          }
        }
        /** @type {!Array} */
        msg.digestAlgorithmIdentifiers = [];
        for (oid in mds) {
          msg.digestAlgorithmIdentifiers.push(asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(oid).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.NULL, false, "")]));
        }
        return mds;
      }
      /**
       * @param {!Object} mds
       * @return {undefined}
       */
      function addSignerInfos(mds) {
        if (msg.contentInfo.value.length < 2) {
          throw new Error("Could not sign PKCS#7 message; there is no content to sign.");
        }
        var contentType = asn1.derToOid(msg.contentInfo.value[0].value);
        var value = msg.contentInfo.value[1];
        value = value.value[0];
        var bytes = asn1.toDer(value);
        bytes.getByte();
        asn1.getBerValueLength(bytes);
        bytes = bytes.getBytes();
        var oid;
        for (oid in mds) {
          mds[oid].start().update(bytes);
        }
        /** @type {!Date} */
        var uid = new Date;
        /** @type {number} */
        var i = 0;
        for (; i < msg.signers.length; ++i) {
          var signer = msg.signers[i];
          if (0 === signer.authenticatedAttributes.length) {
            if (contentType !== forge.pki.oids.data) {
              throw new Error("Invalid signer; authenticatedAttributes must be present when the ContentInfo content type is not PKCS#7 Data.");
            }
          } else {
            signer.authenticatedAttributesAsn1 = asn1.create(asn1.Class.CONTEXT_SPECIFIC, 0, true, []);
            var value = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SET, true, []);
            /** @type {number} */
            var ai = 0;
            for (; ai < signer.authenticatedAttributes.length; ++ai) {
              var attr = signer.authenticatedAttributes[ai];
              if (attr.type === forge.pki.oids.messageDigest) {
                attr.value = mds[signer.digestAlgorithm].digest();
              } else {
                if (attr.type === forge.pki.oids.signingTime) {
                  if (!attr.value) {
                    /** @type {!Date} */
                    attr.value = uid;
                  }
                }
              }
              value.value.push(_attributeToAsn1(attr));
              signer.authenticatedAttributesAsn1.value.push(_attributeToAsn1(attr));
            }
            bytes = asn1.toDer(value).getBytes();
            signer.md.start().update(bytes);
          }
          signer.signature = signer.key.sign(signer.md, "RSASSA-PKCS1-V1_5");
        }
        msg.signerInfos = _signersToAsn1(msg.signers);
      }
      /** @type {null} */
      var msg = null;
      return msg = {
        type : forge.pki.oids.signedData,
        version : 1,
        certificates : [],
        crls : [],
        signers : [],
        digestAlgorithmIdentifiers : [],
        contentInfo : null,
        signerInfos : [],
        fromAsn1 : function(obj) {
          _fromAsn1(msg, obj, p7.asn1.signedDataValidator);
          /** @type {!Array} */
          msg.certificates = [];
          /** @type {!Array} */
          msg.crls = [];
          /** @type {!Array} */
          msg.digestAlgorithmIdentifiers = [];
          /** @type {null} */
          msg.contentInfo = null;
          /** @type {!Array} */
          msg.signerInfos = [];
          var certs = msg.rawCapture.certificates.value;
          /** @type {number} */
          var i = 0;
          for (; i < certs.length; ++i) {
            msg.certificates.push(forge.pki.certificateFromAsn1(certs[i]));
          }
        },
        toAsn1 : function() {
          if (!msg.contentInfo) {
            msg.sign();
          }
          /** @type {!Array} */
          var content = [];
          /** @type {number} */
          var i = 0;
          for (; i < msg.certificates.length; ++i) {
            content.push(forge.pki.certificateToAsn1(msg.certificates[i]));
          }
          /** @type {!Array} */
          var value = [];
          var signedData = asn1.create(asn1.Class.CONTEXT_SPECIFIC, 0, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, asn1.integerToDer(msg.version).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SET, true, msg.digestAlgorithmIdentifiers), msg.contentInfo])]);
          return content.length > 0 && signedData.value[0].value.push(asn1.create(asn1.Class.CONTEXT_SPECIFIC, 0, true, content)), value.length > 0 && signedData.value[0].value.push(asn1.create(asn1.Class.CONTEXT_SPECIFIC, 1, true, value)), signedData.value[0].value.push(asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SET, true, msg.signerInfos)), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(msg.type).getBytes()), signedData]);
        },
        addSigner : function(signer) {
          var issuer = signer.issuer;
          var serialNumber = signer.serialNumber;
          if (signer.certificate) {
            var cert = signer.certificate;
            if ("string" == typeof cert) {
              cert = forge.pki.certificateFromPem(cert);
            }
            issuer = cert.issuer.attributes;
            serialNumber = cert.serialNumber;
          }
          var key = signer.key;
          if (!key) {
            throw new Error("Could not add PKCS#7 signer; no private key specified.");
          }
          if ("string" == typeof key) {
            key = forge.pki.privateKeyFromPem(key);
          }
          var digestAlgorithm = signer.digestAlgorithm || forge.pki.oids.sha1;
          switch(digestAlgorithm) {
            case forge.pki.oids.sha1:
            case forge.pki.oids.sha256:
            case forge.pki.oids.sha384:
            case forge.pki.oids.sha512:
            case forge.pki.oids.md5:
              break;
            default:
              throw new Error("Could not add PKCS#7 signer; unknown message digest algorithm: " + digestAlgorithm);
          }
          var authenticatedAttributes = signer.authenticatedAttributes || [];
          if (authenticatedAttributes.length > 0) {
            /** @type {boolean} */
            var c = false;
            /** @type {boolean} */
            var ch = false;
            /** @type {number} */
            var i = 0;
            for (; i < authenticatedAttributes.length; ++i) {
              var attr = authenticatedAttributes[i];
              if (c || attr.type !== forge.pki.oids.contentType) {
                if (ch || attr.type !== forge.pki.oids.messageDigest) {
                } else {
                  if (ch = true, c) {
                    break;
                  }
                }
              } else {
                if (c = true, ch) {
                  break;
                }
              }
            }
            if (!c || !ch) {
              throw new Error("Invalid signer.authenticatedAttributes. If signer.authenticatedAttributes is specified, then it must contain at least two attributes, PKCS #9 content-type and PKCS #9 message-digest.");
            }
          }
          msg.signers.push({
            key : key,
            version : 1,
            issuer : issuer,
            serialNumber : serialNumber,
            digestAlgorithm : digestAlgorithm,
            signatureAlgorithm : forge.pki.oids.rsaEncryption,
            signature : null,
            authenticatedAttributes : authenticatedAttributes,
            unauthenticatedAttributes : []
          });
        },
        sign : function() {
          if (("object" != typeof msg.content || null === msg.contentInfo) && (msg.contentInfo = asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(forge.pki.oids.data).getBytes())]), "content" in msg)) {
            var value;
            if (msg.content instanceof forge.util.ByteBuffer) {
              value = msg.content.bytes();
            } else {
              if ("string" == typeof msg.content) {
                value = forge.util.encodeUtf8(msg.content);
              }
            }
            msg.contentInfo.value.push(asn1.create(asn1.Class.CONTEXT_SPECIFIC, 0, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OCTETSTRING, false, value)]));
          }
          if (0 !== msg.signers.length) {
            var mds = addDigestAlgorithmIds();
            addSignerInfos(mds);
          }
        },
        verify : function() {
          throw new Error("PKCS#7 signature verification not yet implemented.");
        },
        addCertificate : function(cert) {
          if ("string" == typeof cert) {
            cert = forge.pki.certificateFromPem(cert);
          }
          msg.certificates.push(cert);
        },
        addCertificateRevokationList : function(crl) {
          throw new Error("PKCS#7 CRL support not yet implemented.");
        }
      };
    };
    /**
     * @return {?}
     */
    p7.createEncryptedData = function() {
      /** @type {null} */
      var msg = null;
      return msg = {
        type : forge.pki.oids.encryptedData,
        version : 0,
        encryptedContent : {
          algorithm : forge.pki.oids["aes256-CBC"]
        },
        fromAsn1 : function(obj) {
          _fromAsn1(msg, obj, p7.asn1.encryptedDataValidator);
        },
        decrypt : function(data) {
          if (void 0 !== data) {
            msg.encryptedContent.key = data;
          }
          _decryptContent(msg);
        }
      };
    };
    /**
     * @return {?}
     */
    p7.createEnvelopedData = function() {
      /** @type {null} */
      var msg = null;
      return msg = {
        type : forge.pki.oids.envelopedData,
        version : 0,
        recipients : [],
        encryptedContent : {
          algorithm : forge.pki.oids["aes256-CBC"]
        },
        fromAsn1 : function(obj) {
          var capture = _fromAsn1(msg, obj, p7.asn1.envelopedDataValidator);
          msg.recipients = _recipientsFromAsn1(capture.recipientInfos.value);
        },
        toAsn1 : function() {
          return asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.OID, false, asn1.oidToDer(msg.type).getBytes()), asn1.create(asn1.Class.CONTEXT_SPECIFIC, 0, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SEQUENCE, true, [asn1.create(asn1.Class.UNIVERSAL, asn1.Type.INTEGER, false, asn1.integerToDer(msg.version).getBytes()), asn1.create(asn1.Class.UNIVERSAL, asn1.Type.SET, true, _recipientsToAsn1(msg.recipients)), asn1.create(asn1.Class.UNIVERSAL, 
          asn1.Type.SEQUENCE, true, _encryptedContentToAsn1(msg.encryptedContent))])])]);
        },
        findRecipient : function(cert) {
          var s = cert.issuer.attributes;
          /** @type {number} */
          var i = 0;
          for (; i < msg.recipients.length; ++i) {
            var r = msg.recipients[i];
            var p = r.issuer;
            if (r.serialNumber === cert.serialNumber && p.length === s.length) {
              /** @type {boolean} */
              var s = true;
              /** @type {number} */
              var i = 0;
              for (; i < s.length; ++i) {
                if (p[i].type !== s[i].type || p[i].value !== s[i].value) {
                  /** @type {boolean} */
                  s = false;
                  break;
                }
              }
              if (s) {
                return r;
              }
            }
          }
          return null;
        },
        decrypt : function(obj, val) {
          if (void 0 === msg.encryptedContent.key && void 0 !== obj && void 0 !== val) {
            switch(obj.encryptedContent.algorithm) {
              case forge.pki.oids.rsaEncryption:
              case forge.pki.oids.desCBC:
                var key = val.decrypt(obj.encryptedContent.content);
                msg.encryptedContent.key = forge.util.createBuffer(key);
                break;
              default:
                throw new Error("Unsupported asymmetric cipher, OID " + obj.encryptedContent.algorithm);
            }
          }
          _decryptContent(msg);
        },
        addRecipient : function(cert) {
          msg.recipients.push({
            version : 0,
            issuer : cert.issuer.attributes,
            serialNumber : cert.serialNumber,
            encryptedContent : {
              algorithm : forge.pki.oids.rsaEncryption,
              key : cert.publicKey
            }
          });
        },
        encrypt : function(key, val) {
          if (void 0 === msg.encryptedContent.content) {
            val = val || msg.encryptedContent.algorithm;
            key = key || msg.encryptedContent.key;
            var len;
            var length;
            var ciphFn;
            switch(val) {
              case forge.pki.oids["aes128-CBC"]:
                /** @type {number} */
                len = 16;
                /** @type {number} */
                length = 16;
                ciphFn = forge.aes.createEncryptionCipher;
                break;
              case forge.pki.oids["aes192-CBC"]:
                /** @type {number} */
                len = 24;
                /** @type {number} */
                length = 16;
                ciphFn = forge.aes.createEncryptionCipher;
                break;
              case forge.pki.oids["aes256-CBC"]:
                /** @type {number} */
                len = 32;
                /** @type {number} */
                length = 16;
                ciphFn = forge.aes.createEncryptionCipher;
                break;
              case forge.pki.oids["des-EDE3-CBC"]:
                /** @type {number} */
                len = 24;
                /** @type {number} */
                length = 8;
                ciphFn = forge.des.createEncryptionCipher;
                break;
              default:
                throw new Error("Unsupported symmetric cipher, OID " + val);
            }
            if (void 0 === key) {
              key = forge.util.createBuffer(forge.random.getBytes(len));
            } else {
              if (key.length() != len) {
                throw new Error("Symmetric key has wrong length; got " + key.length() + " bytes, expected " + len + ".");
              }
            }
            /** @type {string} */
            msg.encryptedContent.algorithm = val;
            /** @type {number} */
            msg.encryptedContent.key = key;
            msg.encryptedContent.parameter = forge.util.createBuffer(forge.random.getBytes(length));
            var ciph = ciphFn(key);
            if (ciph.start(msg.encryptedContent.parameter.copy()), ciph.update(msg.content), !ciph.finish()) {
              throw new Error("Symmetric encryption failed.");
            }
            msg.encryptedContent.content = ciph.output;
          }
          /** @type {number} */
          var i = 0;
          for (; i < msg.recipients.length; ++i) {
            var recipient = msg.recipients[i];
            if (void 0 === recipient.encryptedContent.content) {
              switch(recipient.encryptedContent.algorithm) {
                case forge.pki.oids.rsaEncryption:
                  recipient.encryptedContent.content = recipient.encryptedContent.key.encrypt(msg.encryptedContent.key.data);
                  break;
                default:
                  throw new Error("Unsupported asymmetric cipher, OID " + recipient.encryptedContent.algorithm);
              }
            }
          }
        }
      };
    };
  }, function(mixin, canCreateDiscussions, FORGE) {
    /**
     * @return {undefined}
     */
    function _init() {
      /** @type {string} */
      _padding = String.fromCharCode(128);
      _padding = _padding + forge.util.fillString(String.fromCharCode(0), 128);
      /** @type {!Array} */
      _checks = [[1116352408, 3609767458], [1899447441, 602891725], [3049323471, 3964484399], [3921009573, 2173295548], [961987163, 4081628472], [1508970993, 3053834265], [2453635748, 2937671579], [2870763221, 3664609560], [3624381080, 2734883394], [310598401, 1164996542], [607225278, 1323610764], [1426881987, 3590304994], [1925078388, 4068182383], [2162078206, 991336113], [2614888103, 633803317], [3248222580, 3479774868], [3835390401, 2666613458], [4022224774, 944711139], [264347078, 2341262773], 
      [604807628, 2007800933], [770255983, 1495990901], [1249150122, 1856431235], [1555081692, 3175218132], [1996064986, 2198950837], [2554220882, 3999719339], [2821834349, 766784016], [2952996808, 2566594879], [3210313671, 3203337956], [3336571891, 1034457026], [3584528711, 2466948901], [113926993, 3758326383], [338241895, 168717936], [666307205, 1188179964], [773529912, 1546045734], [1294757372, 1522805485], [1396182291, 2643833823], [1695183700, 2343527390], [1986661051, 1014477480], [2177026350, 
      1206759142], [2456956037, 344077627], [2730485921, 1290863460], [2820302411, 3158454273], [3259730800, 3505952657], [3345764771, 106217008], [3516065817, 3606008344], [3600352804, 1432725776], [4094571909, 1467031594], [275423344, 851169720], [430227734, 3100823752], [506948616, 1363258195], [659060556, 3750685593], [883997877, 3785050280], [958139571, 3318307427], [1322822218, 3812723403], [1537002063, 2003034995], [1747873779, 3602036899], [1955562222, 1575990012], [2024104815, 1125592928], 
      [2227730452, 2716904306], [2361852424, 442776044], [2428436474, 593698344], [2756734187, 3733110249], [3204031479, 2999351573], [3329325298, 3815920427], [3391569614, 3928383900], [3515267271, 566280711], [3940187606, 3454069534], [4118630271, 4000239992], [116418474, 1914138554], [174292421, 2731055270], [289380356, 3203993006], [460393269, 320620315], [685471733, 587496836], [852142971, 1086792851], [1017036298, 365543100], [1126000580, 2618297676], [1288033470, 3409855158], [1501505948, 
      4234509866], [1607167915, 987167468], [1816402316, 1246189591]];
      _states = {};
      /** @type {!Array} */
      _states["SHA-512"] = [[1779033703, 4089235720], [3144134277, 2227873595], [1013904242, 4271175723], [2773480762, 1595750129], [1359893119, 2917565137], [2600822924, 725511199], [528734635, 4215389547], [1541459225, 327033209]];
      /** @type {!Array} */
      _states["SHA-384"] = [[3418070365, 3238371032], [1654270250, 914150663], [2438529370, 812702999], [355462360, 4144912697], [1731405415, 4290775857], [2394180231, 1750603025], [3675008525, 1694076839], [1203062813, 3204075428]];
      /** @type {!Array} */
      _states["SHA-512/256"] = [[573645204, 4230739756], [2673172387, 3360449730], [596883563, 1867755857], [2520282905, 1497426621], [2519219938, 2827943907], [3193839141, 1401305490], [721525244, 746961066], [246885852, 2177182882]];
      /** @type {!Array} */
      _states["SHA-512/224"] = [[2352822216, 424955298], [1944164710, 2312950998], [502970286, 855612546], [1738396948, 1479516111], [258812777, 2077511080], [2011393907, 79989058], [1067287976, 1780299464], [286451373, 2446758561]];
      /** @type {boolean} */
      u = true;
    }
    /**
     * @param {!Array} s
     * @param {!Object} t
     * @param {!NodeList} bytes
     * @return {undefined}
     */
    function _update(s, t, bytes) {
      var Ytitle;
      var s;
      var Ymain;
      var label;
      var WaveDrom_Display_;
      var app_extend_button_;
      var middlePathName;
      var pgelem2;
      var dupeNameCount;
      var pgelem3;
      var index;
      var size;
      var a_lo;
      var a_hi;
      var b_lo;
      var b_hi;
      var c_lo;
      var c_hi;
      var d_lo;
      var _;
      var e_lo;
      var e_hi;
      var f_lo;
      var f_hi;
      var g_lo;
      var g_hi;
      var groupNamePrefix;
      var pgelem1;
      var check;
      var uv;
      var name;
      var segs;
      var value;
      var ts;
      var currentSquare;
      var K = bytes.length();
      for (; K >= 128;) {
        /** @type {number} */
        check = 0;
        for (; check < 16; ++check) {
          /** @type {number} */
          t[check][0] = bytes.getInt32() >>> 0;
          /** @type {number} */
          t[check][1] = bytes.getInt32() >>> 0;
        }
        for (; check < 80; ++check) {
          segs = t[check - 2];
          uv = segs[0];
          name = segs[1];
          /** @type {number} */
          Ytitle = ((uv >>> 19 | name << 13) ^ (name >>> 29 | uv << 3) ^ uv >>> 6) >>> 0;
          /** @type {number} */
          s = ((uv << 13 | name >>> 19) ^ (name << 3 | uv >>> 29) ^ (uv << 26 | name >>> 6)) >>> 0;
          ts = t[check - 15];
          uv = ts[0];
          name = ts[1];
          /** @type {number} */
          Ymain = ((uv >>> 1 | name << 31) ^ (uv >>> 8 | name << 24) ^ uv >>> 7) >>> 0;
          /** @type {number} */
          label = ((uv << 31 | name >>> 1) ^ (uv << 24 | name >>> 8) ^ (uv << 25 | name >>> 7)) >>> 0;
          value = t[check - 7];
          currentSquare = t[check - 16];
          name = s + value[1] + label + currentSquare[1];
          /** @type {number} */
          t[check][0] = Ytitle + value[0] + Ymain + currentSquare[0] + (name / 4294967296 >>> 0) >>> 0;
          /** @type {number} */
          t[check][1] = name >>> 0;
        }
        a_lo = s[0][0];
        a_hi = s[0][1];
        b_lo = s[1][0];
        b_hi = s[1][1];
        c_lo = s[2][0];
        c_hi = s[2][1];
        d_lo = s[3][0];
        _ = s[3][1];
        e_lo = s[4][0];
        e_hi = s[4][1];
        f_lo = s[5][0];
        f_hi = s[5][1];
        g_lo = s[6][0];
        g_hi = s[6][1];
        groupNamePrefix = s[7][0];
        pgelem1 = s[7][1];
        /** @type {number} */
        check = 0;
        for (; check < 80; ++check) {
          /** @type {number} */
          middlePathName = ((e_lo >>> 14 | e_hi << 18) ^ (e_lo >>> 18 | e_hi << 14) ^ (e_hi >>> 9 | e_lo << 23)) >>> 0;
          /** @type {number} */
          pgelem2 = ((e_lo << 18 | e_hi >>> 14) ^ (e_lo << 14 | e_hi >>> 18) ^ (e_hi << 23 | e_lo >>> 9)) >>> 0;
          /** @type {number} */
          dupeNameCount = (g_lo ^ e_lo & (f_lo ^ g_lo)) >>> 0;
          /** @type {number} */
          pgelem3 = (g_hi ^ e_hi & (f_hi ^ g_hi)) >>> 0;
          /** @type {number} */
          WaveDrom_Display_ = ((a_lo >>> 28 | a_hi << 4) ^ (a_hi >>> 2 | a_lo << 30) ^ (a_hi >>> 7 | a_lo << 25)) >>> 0;
          /** @type {number} */
          app_extend_button_ = ((a_lo << 4 | a_hi >>> 28) ^ (a_hi << 30 | a_lo >>> 2) ^ (a_hi << 25 | a_lo >>> 7)) >>> 0;
          /** @type {number} */
          index = (a_lo & b_lo | c_lo & (a_lo ^ b_lo)) >>> 0;
          /** @type {number} */
          size = (a_hi & b_hi | c_hi & (a_hi ^ b_hi)) >>> 0;
          name = pgelem1 + pgelem2 + pgelem3 + _checks[check][1] + t[check][1];
          /** @type {number} */
          Ytitle = groupNamePrefix + middlePathName + dupeNameCount + _checks[check][0] + t[check][0] + (name / 4294967296 >>> 0) >>> 0;
          /** @type {number} */
          s = name >>> 0;
          /** @type {number} */
          name = app_extend_button_ + size;
          /** @type {number} */
          Ymain = WaveDrom_Display_ + index + (name / 4294967296 >>> 0) >>> 0;
          /** @type {number} */
          label = name >>> 0;
          groupNamePrefix = g_lo;
          pgelem1 = g_hi;
          g_lo = f_lo;
          g_hi = f_hi;
          f_lo = e_lo;
          f_hi = e_hi;
          name = _ + s;
          /** @type {number} */
          e_lo = d_lo + Ytitle + (name / 4294967296 >>> 0) >>> 0;
          /** @type {number} */
          e_hi = name >>> 0;
          d_lo = c_lo;
          _ = c_hi;
          c_lo = b_lo;
          c_hi = b_hi;
          b_lo = a_lo;
          b_hi = a_hi;
          /** @type {number} */
          name = s + label;
          /** @type {number} */
          a_lo = Ytitle + Ymain + (name / 4294967296 >>> 0) >>> 0;
          /** @type {number} */
          a_hi = name >>> 0;
        }
        name = s[0][1] + a_hi;
        /** @type {number} */
        s[0][0] = s[0][0] + a_lo + (name / 4294967296 >>> 0) >>> 0;
        /** @type {number} */
        s[0][1] = name >>> 0;
        name = s[1][1] + b_hi;
        /** @type {number} */
        s[1][0] = s[1][0] + b_lo + (name / 4294967296 >>> 0) >>> 0;
        /** @type {number} */
        s[1][1] = name >>> 0;
        name = s[2][1] + c_hi;
        /** @type {number} */
        s[2][0] = s[2][0] + c_lo + (name / 4294967296 >>> 0) >>> 0;
        /** @type {number} */
        s[2][1] = name >>> 0;
        name = s[3][1] + _;
        /** @type {number} */
        s[3][0] = s[3][0] + d_lo + (name / 4294967296 >>> 0) >>> 0;
        /** @type {number} */
        s[3][1] = name >>> 0;
        name = s[4][1] + e_hi;
        /** @type {number} */
        s[4][0] = s[4][0] + e_lo + (name / 4294967296 >>> 0) >>> 0;
        /** @type {number} */
        s[4][1] = name >>> 0;
        name = s[5][1] + f_hi;
        /** @type {number} */
        s[5][0] = s[5][0] + f_lo + (name / 4294967296 >>> 0) >>> 0;
        /** @type {number} */
        s[5][1] = name >>> 0;
        name = s[6][1] + g_hi;
        /** @type {number} */
        s[6][0] = s[6][0] + g_lo + (name / 4294967296 >>> 0) >>> 0;
        /** @type {number} */
        s[6][1] = name >>> 0;
        name = s[7][1] + pgelem1;
        /** @type {number} */
        s[7][0] = s[7][0] + groupNamePrefix + (name / 4294967296 >>> 0) >>> 0;
        /** @type {number} */
        s[7][1] = name >>> 0;
        /** @type {number} */
        K = K - 128;
      }
    }
    var forge = FORGE(0);
    FORGE(4);
    FORGE(1);
    var sha512 = mixin.exports = forge.sha512 = forge.sha512 || {};
    forge.md.sha512 = forge.md.algorithms.sha512 = sha512;
    var sha384 = forge.sha384 = forge.sha512.sha384 = forge.sha512.sha384 || {};
    /**
     * @return {?}
     */
    sha384.create = function() {
      return sha512.create("SHA-384");
    };
    forge.md.sha384 = forge.md.algorithms.sha384 = sha384;
    forge.sha512.sha256 = forge.sha512.sha256 || {
      create : function() {
        return sha512.create("SHA-512/256");
      }
    };
    forge.md["sha512/256"] = forge.md.algorithms["sha512/256"] = forge.sha512.sha256;
    forge.sha512.sha224 = forge.sha512.sha224 || {
      create : function() {
        return sha512.create("SHA-512/224");
      }
    };
    forge.md["sha512/224"] = forge.md.algorithms["sha512/224"] = forge.sha512.sha224;
    /**
     * @param {string} algorithm
     * @return {?}
     */
    sha512.create = function(algorithm) {
      if (u || _init(), "undefined" == typeof algorithm && (algorithm = "SHA-512"), !(algorithm in _states)) {
        throw new Error("Invalid SHA-512 algorithm: " + algorithm);
      }
      var _state = _states[algorithm];
      /** @type {null} */
      var _h = null;
      var _input = forge.util.createBuffer();
      /** @type {!Array} */
      var data = new Array(80);
      /** @type {number} */
      var zoom = 0;
      for (; zoom < 80; ++zoom) {
        /** @type {!Array} */
        data[zoom] = new Array(2);
      }
      /** @type {number} */
      var digestLength = 64;
      switch(algorithm) {
        case "SHA-384":
          /** @type {number} */
          digestLength = 48;
          break;
        case "SHA-512/256":
          /** @type {number} */
          digestLength = 32;
          break;
        case "SHA-512/224":
          /** @type {number} */
          digestLength = 28;
      }
      var md = {
        algorithm : algorithm.replace("-", "").toLowerCase(),
        blockLength : 128,
        digestLength : digestLength,
        messageLength : 0,
        fullMessageLength : null,
        messageLengthSize : 16
      };
      return md.start = function() {
        /** @type {number} */
        md.messageLength = 0;
        /** @type {!Array} */
        md.fullMessageLength = md.messageLength128 = [];
        /** @type {number} */
        var cell_amount = md.messageLengthSize / 4;
        /** @type {number} */
        var i = 0;
        for (; i < cell_amount; ++i) {
          md.fullMessageLength.push(0);
        }
        _input = forge.util.createBuffer();
        /** @type {!Array} */
        _h = new Array(_state.length);
        /** @type {number} */
        i = 0;
        for (; i < _state.length; ++i) {
          _h[i] = _state[i].slice(0);
        }
        return md;
      }, md.start(), md.update = function(msg, encoding) {
        if ("utf8" === encoding) {
          msg = forge.util.encodeUtf8(msg);
        }
        var len = msg.length;
        md.messageLength += len;
        /** @type {!Array} */
        len = [len / 4294967296 >>> 0, len >>> 0];
        /** @type {number} */
        var i = md.fullMessageLength.length - 1;
        for (; i >= 0; --i) {
          md.fullMessageLength[i] += len[1];
          len[1] = len[0] + (md.fullMessageLength[i] / 4294967296 >>> 0);
          /** @type {number} */
          md.fullMessageLength[i] = md.fullMessageLength[i] >>> 0;
          /** @type {number} */
          len[0] = len[1] / 4294967296 >>> 0;
        }
        return _input.putBytes(msg), _update(_h, data, _input), (_input.read > 2048 || 0 === _input.length()) && _input.compact(), md;
      }, md.digest = function() {
        var finalBlock = forge.util.createBuffer();
        finalBlock.putBytes(_input.bytes());
        var remaining = md.fullMessageLength[md.fullMessageLength.length - 1] + md.messageLengthSize;
        /** @type {number} */
        var overflow = remaining & md.blockLength - 1;
        finalBlock.putBytes(_padding.substr(0, md.blockLength - overflow));
        var next;
        var carry;
        /** @type {number} */
        var bits = 8 * md.fullMessageLength[0];
        /** @type {number} */
        var i = 0;
        for (; i < md.fullMessageLength.length - 1; ++i) {
          /** @type {number} */
          next = 8 * md.fullMessageLength[i + 1];
          /** @type {number} */
          carry = next / 4294967296 >>> 0;
          /** @type {number} */
          bits = bits + carry;
          finalBlock.putInt32(bits >>> 0);
          /** @type {number} */
          bits = next >>> 0;
        }
        finalBlock.putInt32(bits);
        /** @type {!Array} */
        var d = new Array(_h.length);
        /** @type {number} */
        i = 0;
        for (; i < _h.length; ++i) {
          d[i] = _h[i].slice(0);
        }
        _update(d, data, finalBlock);
        var DISPLAY_STATIONS;
        var rval = forge.util.createBuffer();
        /** @type {number} */
        DISPLAY_STATIONS = "SHA-512" === algorithm ? d.length : "SHA-384" === algorithm ? d.length - 2 : d.length - 4;
        /** @type {number} */
        i = 0;
        for (; i < DISPLAY_STATIONS; ++i) {
          rval.putInt32(d[i][0]);
          if (!(i === DISPLAY_STATIONS - 1 && "SHA-512/224" === algorithm)) {
            rval.putInt32(d[i][1]);
          }
        }
        return rval;
      }, md;
    };
    /** @type {null} */
    var _padding = null;
    /** @type {boolean} */
    var u = false;
    /** @type {null} */
    var _checks = null;
    /** @type {null} */
    var _states = null;
  }, function(mixin, canCreateDiscussions, FORGE) {
    /**
     * @param {?} buffer
     * @param {string} val
     * @return {undefined}
     */
    function _addBigIntegerToBuffer(buffer, val) {
      var str = val.toString(16);
      if (str[0] >= "8") {
        /** @type {string} */
        str = "00" + str;
      }
      var bytes = forge.util.hexToBytes(str);
      buffer.putInt32(bytes.length);
      buffer.putBytes(bytes);
    }
    /**
     * @param {?} buffer
     * @param {!Object} val
     * @return {undefined}
     */
    function _addStringToBuffer(buffer, val) {
      buffer.putInt32(val.length);
      buffer.putString(val);
    }
    /**
     * @return {?}
     */
    function _sha1() {
      var hash = forge.md.sha1.create();
      /** @type {number} */
      var argl = arguments.length;
      /** @type {number} */
      var i = 0;
      for (; i < argl; ++i) {
        hash.update(arguments[i]);
      }
      return hash.digest();
    }
    var forge = FORGE(0);
    FORGE(5);
    FORGE(8);
    FORGE(14);
    FORGE(9);
    FORGE(1);
    var ssh = mixin.exports = forge.ssh = forge.ssh || {};
    /**
     * @param {!Object} privateKey
     * @param {!Object} passphrase
     * @param {!Object} comment
     * @return {?}
     */
    ssh.privateKeyToPutty = function(privateKey, passphrase, comment) {
      comment = comment || "";
      passphrase = passphrase || "";
      /** @type {string} */
      var type = "ssh-rsa";
      /** @type {string} */
      var encryptionAlgorithm = "" === passphrase ? "none" : "aes256-cbc";
      /** @type {string} */
      var tick = "PuTTY-User-Key-File-2: " + type + "\r\n";
      /** @type {string} */
      tick = tick + ("Encryption: " + encryptionAlgorithm + "\r\n");
      /** @type {string} */
      tick = tick + ("Comment: " + comment + "\r\n");
      var pubbuffer = forge.util.createBuffer();
      _addStringToBuffer(pubbuffer, type);
      _addBigIntegerToBuffer(pubbuffer, privateKey.e);
      _addBigIntegerToBuffer(pubbuffer, privateKey.n);
      var delta = forge.util.encode64(pubbuffer.bytes(), 64);
      /** @type {number} */
      var h = Math.floor(delta.length / 66) + 1;
      /** @type {string} */
      tick = tick + ("Public-Lines: " + h + "\r\n");
      /** @type {string} */
      tick = tick + delta;
      var privbuffer = forge.util.createBuffer();
      _addBigIntegerToBuffer(privbuffer, privateKey.d);
      _addBigIntegerToBuffer(privbuffer, privateKey.p);
      _addBigIntegerToBuffer(privbuffer, privateKey.q);
      _addBigIntegerToBuffer(privbuffer, privateKey.qInv);
      var step;
      if (passphrase) {
        /** @type {number} */
        var bottom = privbuffer.length() + 16 - 1;
        /** @type {number} */
        bottom = bottom - bottom % 16;
        var input = _sha1(privbuffer.bytes());
        input.truncate(input.length() - bottom + privbuffer.length());
        privbuffer.putBuffer(input);
        var aeskey = forge.util.createBuffer();
        aeskey.putBuffer(_sha1("\x00\x00\x00\x00", passphrase));
        aeskey.putBuffer(_sha1("\x00\x00\x00\u0001", passphrase));
        var cipher = forge.aes.createEncryptionCipher(aeskey.truncate(8), "CBC");
        cipher.start(forge.util.createBuffer().fillWithByte(0, 16));
        cipher.update(privbuffer.copy());
        cipher.finish();
        var encrypted = cipher.output;
        encrypted.truncate(16);
        step = forge.util.encode64(encrypted.bytes(), 64);
      } else {
        step = forge.util.encode64(privbuffer.bytes(), 64);
      }
      /** @type {number} */
      h = Math.floor(step.length / 66) + 1;
      /** @type {string} */
      tick = tick + ("\r\nPrivate-Lines: " + h + "\r\n");
      /** @type {string} */
      tick = tick + step;
      var key = _sha1("putty-private-key-file-mac-key", passphrase);
      var macbuffer = forge.util.createBuffer();
      _addStringToBuffer(macbuffer, type);
      _addStringToBuffer(macbuffer, encryptionAlgorithm);
      _addStringToBuffer(macbuffer, comment);
      macbuffer.putInt32(pubbuffer.length());
      macbuffer.putBuffer(pubbuffer);
      macbuffer.putInt32(privbuffer.length());
      macbuffer.putBuffer(privbuffer);
      var hmac = forge.hmac.create();
      return hmac.start("sha1", key), hmac.update(macbuffer.bytes()), tick = tick + ("\r\nPrivate-MAC: " + hmac.digest().toHex() + "\r\n");
    };
    /**
     * @param {!Object} key
     * @param {!Object} comment
     * @return {?}
     */
    ssh.publicKeyToOpenSSH = function(key, comment) {
      /** @type {string} */
      var type = "ssh-rsa";
      comment = comment || "";
      var buffer = forge.util.createBuffer();
      return _addStringToBuffer(buffer, type), _addBigIntegerToBuffer(buffer, key.e), _addBigIntegerToBuffer(buffer, key.n), type + " " + forge.util.encode64(buffer.bytes()) + " " + comment;
    };
    /**
     * @param {?} privateKey
     * @param {!Array} password
     * @return {?}
     */
    ssh.privateKeyToOpenSSH = function(privateKey, password) {
      return password ? forge.pki.encryptRsaPrivateKey(privateKey, password, {
        legacy : true,
        algorithm : "aes128"
      }) : forge.pki.privateKeyToPem(privateKey);
    };
    /**
     * @param {!Object} key
     * @param {!Object} options
     * @return {?}
     */
    ssh.getPublicKeyFingerprint = function(key, options) {
      options = options || {};
      var hmac = options.md || forge.md.md5.create();
      /** @type {string} */
      var type = "ssh-rsa";
      var buffer = forge.util.createBuffer();
      _addStringToBuffer(buffer, type);
      _addBigIntegerToBuffer(buffer, key.e);
      _addBigIntegerToBuffer(buffer, key.n);
      hmac.start();
      hmac.update(buffer.getBytes());
      var digest = hmac.digest();
      if ("hex" === options.encoding) {
        var hierarchySelector = digest.toHex();
        return options.delimiter ? hierarchySelector.match(/.{2}/g).join(options.delimiter) : hierarchySelector;
      }
      if ("binary" === options.encoding) {
        return digest.getBytes();
      }
      if (options.encoding) {
        throw new Error('Unknown encoding "' + options.encoding + '".');
      }
      return digest;
    };
  }, function(mixin, canCreateDiscussions, FORGE) {
    var forge = FORGE(0);
    FORGE(19);
    FORGE(20);
    FORGE(1);
    /** @type {string} */
    var r = "forge.task";
    /** @type {number} */
    var i = 0;
    var object = {};
    /** @type {number} */
    var nextProfileItemId = 0;
    forge.debug.set(r, "tasks", object);
    var sTaskQueues = {};
    forge.debug.set(r, "queues", sTaskQueues);
    /** @type {string} */
    var sNoTaskName = "?";
    /** @type {number} */
    var sMaxRecursions = 30;
    /** @type {number} */
    var sTimeSlice = 20;
    /** @type {string} */
    var READY = "ready";
    /** @type {string} */
    var RUNNING = "running";
    /** @type {string} */
    var BLOCKED = "blocked";
    /** @type {string} */
    var SLEEPING = "sleeping";
    /** @type {string} */
    var DONE = "done";
    /** @type {string} */
    var ERROR = "error";
    /** @type {string} */
    var STOP = "stop";
    /** @type {string} */
    var START = "start";
    /** @type {string} */
    var BLOCK = "block";
    /** @type {string} */
    var UNBLOCK = "unblock";
    /** @type {string} */
    var SLEEP = "sleep";
    /** @type {string} */
    var WAKEUP = "wakeup";
    /** @type {string} */
    var CANCEL = "cancel";
    /** @type {string} */
    var FAIL = "fail";
    var sStateTable = {};
    sStateTable[READY] = {};
    /** @type {string} */
    sStateTable[READY][STOP] = READY;
    /** @type {string} */
    sStateTable[READY][START] = RUNNING;
    /** @type {string} */
    sStateTable[READY][CANCEL] = DONE;
    /** @type {string} */
    sStateTable[READY][FAIL] = ERROR;
    sStateTable[RUNNING] = {};
    /** @type {string} */
    sStateTable[RUNNING][STOP] = READY;
    /** @type {string} */
    sStateTable[RUNNING][START] = RUNNING;
    /** @type {string} */
    sStateTable[RUNNING][BLOCK] = BLOCKED;
    /** @type {string} */
    sStateTable[RUNNING][UNBLOCK] = RUNNING;
    /** @type {string} */
    sStateTable[RUNNING][SLEEP] = SLEEPING;
    /** @type {string} */
    sStateTable[RUNNING][WAKEUP] = RUNNING;
    /** @type {string} */
    sStateTable[RUNNING][CANCEL] = DONE;
    /** @type {string} */
    sStateTable[RUNNING][FAIL] = ERROR;
    sStateTable[BLOCKED] = {};
    /** @type {string} */
    sStateTable[BLOCKED][STOP] = BLOCKED;
    /** @type {string} */
    sStateTable[BLOCKED][START] = BLOCKED;
    /** @type {string} */
    sStateTable[BLOCKED][BLOCK] = BLOCKED;
    /** @type {string} */
    sStateTable[BLOCKED][UNBLOCK] = BLOCKED;
    /** @type {string} */
    sStateTable[BLOCKED][SLEEP] = BLOCKED;
    /** @type {string} */
    sStateTable[BLOCKED][WAKEUP] = BLOCKED;
    /** @type {string} */
    sStateTable[BLOCKED][CANCEL] = DONE;
    /** @type {string} */
    sStateTable[BLOCKED][FAIL] = ERROR;
    sStateTable[SLEEPING] = {};
    /** @type {string} */
    sStateTable[SLEEPING][STOP] = SLEEPING;
    /** @type {string} */
    sStateTable[SLEEPING][START] = SLEEPING;
    /** @type {string} */
    sStateTable[SLEEPING][BLOCK] = SLEEPING;
    /** @type {string} */
    sStateTable[SLEEPING][UNBLOCK] = SLEEPING;
    /** @type {string} */
    sStateTable[SLEEPING][SLEEP] = SLEEPING;
    /** @type {string} */
    sStateTable[SLEEPING][WAKEUP] = SLEEPING;
    /** @type {string} */
    sStateTable[SLEEPING][CANCEL] = DONE;
    /** @type {string} */
    sStateTable[SLEEPING][FAIL] = ERROR;
    sStateTable[DONE] = {};
    /** @type {string} */
    sStateTable[DONE][STOP] = DONE;
    /** @type {string} */
    sStateTable[DONE][START] = DONE;
    /** @type {string} */
    sStateTable[DONE][BLOCK] = DONE;
    /** @type {string} */
    sStateTable[DONE][UNBLOCK] = DONE;
    /** @type {string} */
    sStateTable[DONE][SLEEP] = DONE;
    /** @type {string} */
    sStateTable[DONE][WAKEUP] = DONE;
    /** @type {string} */
    sStateTable[DONE][CANCEL] = DONE;
    /** @type {string} */
    sStateTable[DONE][FAIL] = ERROR;
    sStateTable[ERROR] = {};
    /** @type {string} */
    sStateTable[ERROR][STOP] = ERROR;
    /** @type {string} */
    sStateTable[ERROR][START] = ERROR;
    /** @type {string} */
    sStateTable[ERROR][BLOCK] = ERROR;
    /** @type {string} */
    sStateTable[ERROR][UNBLOCK] = ERROR;
    /** @type {string} */
    sStateTable[ERROR][SLEEP] = ERROR;
    /** @type {string} */
    sStateTable[ERROR][WAKEUP] = ERROR;
    /** @type {string} */
    sStateTable[ERROR][CANCEL] = ERROR;
    /** @type {string} */
    sStateTable[ERROR][FAIL] = ERROR;
    /**
     * @param {!Object} options
     * @return {undefined}
     */
    var Task = function(options) {
      /** @type {number} */
      this.id = -1;
      this.name = options.name || sNoTaskName;
      this.parent = options.parent || null;
      this.run = options.run;
      /** @type {!Array} */
      this.subtasks = [];
      /** @type {boolean} */
      this.error = false;
      /** @type {string} */
      this.state = READY;
      /** @type {number} */
      this.blocks = 0;
      /** @type {null} */
      this.timeoutId = null;
      /** @type {null} */
      this.swapTime = null;
      /** @type {null} */
      this.userData = null;
      /** @type {number} */
      this.id = nextProfileItemId++;
      object[this.id] = this;
      if (i >= 1) {
        forge.log.verbose(r, "[%s][%s] init", this.id, this.name, this);
      }
    };
    /**
     * @param {!Object} value
     * @return {undefined}
     */
    Task.prototype.debug = function(value) {
      value = value || "";
      forge.log.debug(r, value, "[%s][%s] task:", this.id, this.name, this, "subtasks:", this.subtasks.length, "queue:", sTaskQueues);
    };
    /**
     * @param {string} name
     * @param {!Function} message
     * @return {?}
     */
    Task.prototype.next = function(name, message) {
      if ("function" == typeof name) {
        /** @type {string} */
        message = name;
        name = this.name;
      }
      var subtask = new Task({
        run : message,
        name : name,
        parent : this
      });
      return subtask.state = RUNNING, subtask.type = this.type, subtask.successCallback = this.successCallback || null, subtask.failureCallback = this.failureCallback || null, this.subtasks.push(subtask), this;
    };
    /**
     * @param {undefined} name
     * @param {string} subrun
     * @return {?}
     */
    Task.prototype.parallel = function(name, subrun) {
      return forge.util.isArray(name) && (subrun = name, name = this.name), this.next(name, function(task) {
        /** @type {!Object} */
        var ptask = task;
        ptask.block(subrun.length);
        /**
         * @param {string} pname
         * @param {number} pi
         * @return {undefined}
         */
        var startParallelTask = function(pname, pi) {
          forge.task.start({
            type : pname,
            run : function(task) {
              subrun[pi](task);
            },
            success : function(index) {
              ptask.unblock();
            },
            failure : function(input) {
              ptask.unblock();
            }
          });
        };
        /** @type {number} */
        var i = 0;
        for (; i < subrun.length; i++) {
          /** @type {string} */
          var pname = name + "__parallel-" + task.id + "-" + i;
          /** @type {number} */
          var pi = i;
          startParallelTask(pname, pi);
        }
      });
    };
    /**
     * @return {undefined}
     */
    Task.prototype.stop = function() {
      this.state = sStateTable[this.state][STOP];
    };
    /**
     * @return {undefined}
     */
    Task.prototype.start = function() {
      /** @type {boolean} */
      this.error = false;
      this.state = sStateTable[this.state][START];
      if (this.state === RUNNING) {
        /** @type {!Date} */
        this.start = new Date;
        this.run(this);
        runNext(this, 0);
      }
    };
    /**
     * @param {number} n
     * @return {undefined}
     */
    Task.prototype.block = function(n) {
      n = "undefined" == typeof n ? 1 : n;
      this.blocks += n;
      if (this.blocks > 0) {
        this.state = sStateTable[this.state][BLOCK];
      }
    };
    /**
     * @param {number} n
     * @return {?}
     */
    Task.prototype.unblock = function(n) {
      return n = "undefined" == typeof n ? 1 : n, this.blocks -= n, 0 === this.blocks && this.state !== DONE && (this.state = RUNNING, runNext(this, 0)), this.blocks;
    };
    /**
     * @param {number} n
     * @return {undefined}
     */
    Task.prototype.sleep = function(n) {
      n = "undefined" == typeof n ? 0 : n;
      this.state = sStateTable[this.state][SLEEP];
      var self = this;
      /** @type {number} */
      this.timeoutId = setTimeout(function() {
        /** @type {null} */
        self.timeoutId = null;
        /** @type {string} */
        self.state = RUNNING;
        runNext(self, 0);
      }, n);
    };
    /**
     * @param {?} container
     * @return {undefined}
     */
    Task.prototype.wait = function(container) {
      container.wait(this);
    };
    /**
     * @return {undefined}
     */
    Task.prototype.wakeup = function() {
      if (this.state === SLEEPING) {
        cancelTimeout(this.timeoutId);
        /** @type {null} */
        this.timeoutId = null;
        /** @type {string} */
        this.state = RUNNING;
        runNext(this, 0);
      }
    };
    /**
     * @return {undefined}
     */
    Task.prototype.cancel = function() {
      this.state = sStateTable[this.state][CANCEL];
      /** @type {number} */
      this.permitsNeeded = 0;
      if (null !== this.timeoutId) {
        cancelTimeout(this.timeoutId);
        /** @type {null} */
        this.timeoutId = null;
      }
      /** @type {!Array} */
      this.subtasks = [];
    };
    /**
     * @param {!Object} next
     * @return {undefined}
     */
    Task.prototype.fail = function(next) {
      if (this.error = true, finish(this, true), next) {
        /** @type {boolean} */
        next.error = this.error;
        next.swapTime = this.swapTime;
        next.userData = this.userData;
        runNext(next, 0);
      } else {
        if (null !== this.parent) {
          var parent = this.parent;
          for (; null !== parent.parent;) {
            /** @type {boolean} */
            parent.error = this.error;
            parent.swapTime = this.swapTime;
            parent.userData = this.userData;
            parent = parent.parent;
          }
          finish(parent, true);
        }
        if (this.failureCallback) {
          this.failureCallback(this);
        }
      }
    };
    /**
     * @param {!Object} task
     * @return {undefined}
     */
    var start = function(task) {
      /** @type {boolean} */
      task.error = false;
      task.state = sStateTable[task.state][START];
      setTimeout(function() {
        if (task.state === RUNNING) {
          /** @type {number} */
          task.swapTime = +new Date;
          task.run(task);
          runNext(task, 0);
        }
      }, 0);
    };
    /**
     * @param {!Object} task
     * @param {number} recurse
     * @return {undefined}
     */
    var runNext = function(task, recurse) {
      /** @type {boolean} */
      var r = recurse > sMaxRecursions || +new Date - task.swapTime > sTimeSlice;
      /**
       * @param {number} recurse
       * @return {undefined}
       */
      var doNext = function(recurse) {
        if (recurse++, task.state === RUNNING) {
          if (r && (task.swapTime = +new Date), task.subtasks.length > 0) {
            var next = task.subtasks.shift();
            next.error = task.error;
            next.swapTime = task.swapTime;
            next.userData = task.userData;
            next.run(next);
            if (!next.error) {
              runNext(next, recurse);
            }
          } else {
            finish(task);
            if (!task.error) {
              if (null !== task.parent) {
                task.parent.error = task.error;
                task.parent.swapTime = task.swapTime;
                task.parent.userData = task.userData;
                runNext(task.parent, recurse);
              }
            }
          }
        }
      };
      if (r) {
        setTimeout(doNext, 0);
      } else {
        doNext(recurse);
      }
    };
    /**
     * @param {!Object} task
     * @param {boolean} position
     * @return {undefined}
     */
    var finish = function(task, position) {
      /** @type {string} */
      task.state = DONE;
      delete object[task.id];
      if (i >= 1) {
        forge.log.verbose(r, "[%s][%s] finish", task.id, task.name, task);
      }
      if (null === task.parent) {
        if (task.type in sTaskQueues) {
          if (0 === sTaskQueues[task.type].length) {
            forge.log.error(r, "[%s][%s] task queue empty [%s]", task.id, task.name, task.type);
          } else {
            if (sTaskQueues[task.type][0] !== task) {
              forge.log.error(r, "[%s][%s] task not first in queue [%s]", task.id, task.name, task.type);
            } else {
              sTaskQueues[task.type].shift();
              if (0 === sTaskQueues[task.type].length) {
                if (i >= 1) {
                  forge.log.verbose(r, "[%s][%s] delete queue [%s]", task.id, task.name, task.type);
                }
                delete sTaskQueues[task.type];
              } else {
                if (i >= 1) {
                  forge.log.verbose(r, "[%s][%s] queue start next [%s] remain:%s", task.id, task.name, task.type, sTaskQueues[task.type].length);
                }
                sTaskQueues[task.type][0].start();
              }
            }
          }
        } else {
          forge.log.error(r, "[%s][%s] task queue missing [%s]", task.id, task.name, task.type);
        }
        if (!position) {
          if (task.error && task.failureCallback) {
            task.failureCallback(task);
          } else {
            if (!task.error && task.successCallback) {
              task.successCallback(task);
            }
          }
        }
      }
    };
    mixin.exports = forge.task = forge.task || {};
    /**
     * @param {!Object} options
     * @return {undefined}
     */
    forge.task.start = function(options) {
      var task = new Task({
        run : options.run,
        name : options.name || sNoTaskName
      });
      task.type = options.type;
      task.successCallback = options.success || null;
      task.failureCallback = options.failure || null;
      if (task.type in sTaskQueues) {
        sTaskQueues[options.type].push(task);
      } else {
        if (i >= 1) {
          forge.log.verbose(r, "[%s][%s] create queue [%s]", task.id, task.name, task.type);
        }
        /** @type {!Array} */
        sTaskQueues[task.type] = [task];
        start(task);
      }
    };
    /**
     * @param {?} type
     * @return {undefined}
     */
    forge.task.cancel = function(type) {
      if (type in sTaskQueues) {
        /** @type {!Array} */
        sTaskQueues[type] = [sTaskQueues[type][0]];
      }
    };
    /**
     * @return {?}
     */
    forge.task.createCondition = function() {
      var cond = {
        tasks : {}
      };
      return cond.wait = function(task) {
        if (!(task.id in cond.tasks)) {
          task.block();
          /** @type {!Object} */
          cond.tasks[task.id] = task;
        }
      }, cond.notify = function() {
        var tmp = cond.tasks;
        cond.tasks = {};
        var id;
        for (id in tmp) {
          tmp[id].unblock();
        }
      }, cond;
    };
  }, function(module, canCreateDiscussions, factory) {
    module.exports = factory(33);
  }]);
});

