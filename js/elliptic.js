'use strict';
!function(factory) {
  if ("object" == typeof exports && "undefined" != typeof module) {
    module.exports = factory();
  } else {
    if ("function" == typeof define && define.amd) {
      define([], factory);
    } else {
      ("undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : this).elliptic = factory();
    }
  }
}(function() {
  return function e(data, t, r) {
    /**
     * @param {string} n
     * @param {?} r
     * @return {?}
     */
    function s(n, r) {
      if (!t[n]) {
        if (!data[n]) {
          var o = "function" == typeof require && require;
          if (!r && o) {
            return o(n, true);
          }
          if (a) {
            return a(n, true);
          }
          throw (o = new Error("Cannot find module '" + n + "'")).code = "MODULE_NOT_FOUND", o;
        }
        o = t[n] = {
          exports : {}
        };
        data[n][0].call(o.exports, function(e) {
          return s(data[n][1][e] || e);
        }, o, o.exports, e, data, t, r);
      }
      return t[n].exports;
    }
    var a = "function" == typeof require && require;
    /** @type {number} */
    var o = 0;
    for (; o < r.length; o++) {
      s(r[o]);
    }
    return s;
  }({
    1 : [function(require, canCreateDiscussions, self) {
      self.version = require("../package.json").version;
      self.utils = require("./elliptic/utils");
      self.rand = require("brorand");
      self.curve = require("./elliptic/curve");
      self.curves = require("./elliptic/curves");
      self.ec = require("./elliptic/ec");
      self.eddsa = require("./elliptic/eddsa");
    }, {
      "../package.json" : 35,
      "./elliptic/curve" : 4,
      "./elliptic/curves" : 7,
      "./elliptic/ec" : 8,
      "./elliptic/eddsa" : 11,
      "./elliptic/utils" : 15,
      brorand : 17
    }],
    2 : [function(require, module, i) {
      /**
       * @param {string} storage
       * @param {!Object} conf
       * @return {undefined}
       */
      function test(storage, conf) {
        /** @type {string} */
        this.type = storage;
        this.p = new BN(conf.p, 16);
        this.red = conf.prime ? BN.red(conf.prime) : BN.mont(this.p);
        this.zero = (new BN(0)).toRed(this.red);
        this.one = (new BN(1)).toRed(this.red);
        this.two = (new BN(2)).toRed(this.red);
        this.n = conf.n && new BN(conf.n, 16);
        this.g = conf.g && this.pointFromJSON(conf.g, conf.gRed);
        /** @type {!Array} */
        this._wnafT1 = new Array(4);
        /** @type {!Array} */
        this._wnafT2 = new Array(4);
        /** @type {!Array} */
        this._wnafT3 = new Array(4);
        /** @type {!Array} */
        this._wnafT4 = new Array(4);
        this._bitLength = this.n ? this.n.bitLength() : 0;
        conf = this.n && this.p.div(this.n);
        if (!conf || 0 < conf.cmpn(100)) {
          /** @type {null} */
          this.redN = null;
        } else {
          /** @type {boolean} */
          this._maxwellTrick = true;
          this.redN = this.n.toRed(this.red);
        }
      }
      /**
       * @param {!Object} n
       * @param {string} a
       * @return {undefined}
       */
      function _(n, a) {
        /** @type {!Object} */
        this.curve = n;
        /** @type {string} */
        this.type = a;
        /** @type {null} */
        this.precomputed = null;
      }
      var BN = require("bn.js");
      var utils = require("../utils");
      var getNAF = utils.getNAF;
      var cmp = utils.getJSF;
      var assert = utils.assert;
      /**
       * @return {?}
       */
      (module.exports = test).prototype.point = function() {
        throw new Error("Not implemented");
      };
      /**
       * @return {?}
       */
      test.prototype.validate = function() {
        throw new Error("Not implemented");
      };
      /**
       * @param {?} p
       * @param {?} k
       * @return {?}
       */
      test.prototype._fixedNafMul = function(p, k) {
        assert(p.precomputed);
        var doubles = p._getDoubles();
        var naf = getNAF(k, 1, this._bitLength);
        /** @type {number} */
        k = (1 << doubles.step + 1) - (doubles.step % 2 == 0 ? 2 : 1);
        /** @type {number} */
        k = k / 3;
        var nafW;
        /** @type {!Array} */
        var repr = [];
        /** @type {number} */
        var j = 0;
        for (; j < naf.length; j = j + doubles.step) {
          /** @type {number} */
          nafW = 0;
          /** @type {number} */
          var k = j + doubles.step - 1;
          for (; j <= k; k--) {
            nafW = (nafW << 1) + naf[k];
          }
          repr.push(nafW);
        }
        var a = this.jpoint(null, null, null);
        var b = this.jpoint(null, null, null);
        /** @type {number} */
        var i = k;
        for (; 0 < i; i--) {
          /** @type {number} */
          j = 0;
          for (; j < repr.length; j++) {
            if ((nafW = repr[j]) === i) {
              b = b.mixedAdd(doubles.points[j]);
            } else {
              if (nafW === -i) {
                b = b.mixedAdd(doubles.points[j].neg());
              }
            }
          }
          a = a.add(b);
        }
        return a.toP();
      };
      /**
       * @param {!Object} p
       * @param {string} k
       * @return {?}
       */
      test.prototype._wnafMul = function(p, k) {
        var options = p._getNAFPoints(4);
        var w = options.wnd;
        var points = options.points;
        var o = getNAF(k, w, this._bitLength);
        var acc = this.jpoint(null, null, null);
        /** @type {number} */
        var name = o.length - 1;
        for (; 0 <= name; name--) {
          /** @type {number} */
          var k = 0;
          for (; 0 <= name && 0 === o[name]; name--) {
            k++;
          }
          if (0 <= name && k++, acc = acc.dblp(k), name < 0) {
            break;
          }
          var fn = o[name];
          assert(0 !== fn);
          acc = "affine" === p.type ? 0 < fn ? acc.mixedAdd(points[fn - 1 >> 1]) : acc.mixedAdd(points[-fn - 1 >> 1].neg()) : 0 < fn ? acc.add(points[fn - 1 >> 1]) : acc.add(points[-fn - 1 >> 1].neg());
        }
        return "affine" === p.type ? acc.toP() : acc;
      };
      /**
       * @param {number} w
       * @param {!Array} points
       * @param {!Array} array
       * @param {number} len
       * @param {boolean} depth
       * @return {?}
       */
      test.prototype._wnafMulAdd = function(w, points, array, len, depth) {
        var p;
        var wndWidth = this._wnafT1;
        var wnd = this._wnafT2;
        var naf = this._wnafT3;
        /** @type {number} */
        var max = 0;
        /** @type {number} */
        var i = 0;
        for (; i < len; i++) {
          var nafPoints = (p = points[i])._getNAFPoints(w);
          wndWidth[i] = nafPoints.wnd;
          wnd[i] = nafPoints.points;
        }
        /** @type {number} */
        i = len - 1;
        for (; 1 <= i; i = i - 2) {
          /** @type {number} */
          var a = i - 1;
          /** @type {number} */
          var b = i;
          if (1 === wndWidth[a] && 1 === wndWidth[b]) {
            /** @type {!Array} */
            var comb = [points[a], null, null, points[b]];
            if (0 === points[a].y.cmp(points[b].y)) {
              comb[1] = points[a].add(points[b]);
              comb[2] = points[a].toJ().mixedAdd(points[b].neg());
            } else {
              if (0 === points[a].y.cmp(points[b].y.redNeg())) {
                comb[1] = points[a].toJ().mixedAdd(points[b]);
                comb[2] = points[a].add(points[b].neg());
              } else {
                comb[1] = points[a].toJ().mixedAdd(points[b]);
                comb[2] = points[a].toJ().mixedAdd(points[b].neg());
              }
            }
            /** @type {!Array} */
            var lab = [-3, -1, -5, -7, 0, 7, 5, 1, 3];
            var res = cmp(array[a], array[b]);
            /** @type {number} */
            max = Math.max(res[0].length, max);
            /** @type {!Array} */
            naf[a] = new Array(max);
            /** @type {!Array} */
            naf[b] = new Array(max);
            /** @type {number} */
            j = 0;
            for (; j < max; j++) {
              /** @type {number} */
              var v = 0 | res[0][j];
              /** @type {number} */
              var g = 0 | res[1][j];
              naf[a][j] = lab[3 * (1 + v) + (1 + g)];
              /** @type {number} */
              naf[b][j] = 0;
              /** @type {!Array} */
              wnd[a] = comb;
            }
          } else {
            naf[a] = getNAF(array[a], wndWidth[a], this._bitLength);
            naf[b] = getNAF(array[b], wndWidth[b], this._bitLength);
            /** @type {number} */
            max = Math.max(naf[a].length, max);
            /** @type {number} */
            max = Math.max(naf[b].length, max);
          }
        }
        var acc = this.jpoint(null, null, null);
        var tmp = this._wnafT4;
        /** @type {number} */
        i = max;
        for (; 0 <= i; i--) {
          /** @type {number} */
          var k = 0;
          for (; 0 <= i;) {
            /** @type {boolean} */
            var S = true;
            /** @type {number} */
            var j = 0;
            for (; j < len; j++) {
              /** @type {number} */
              tmp[j] = 0 | naf[j][i];
              if (0 !== tmp[j]) {
                /** @type {boolean} */
                S = false;
              }
            }
            if (!S) {
              break;
            }
            k++;
            i--;
          }
          if (0 <= i && k++, acc = acc.dblp(k), i < 0) {
            break;
          }
          /** @type {number} */
          j = 0;
          for (; j < len; j++) {
            var type = tmp[j];
            if (0 !== type) {
              if (0 < type) {
                p = wnd[j][type - 1 >> 1];
              } else {
                if (type < 0) {
                  p = wnd[j][-type - 1 >> 1].neg();
                }
              }
              acc = "affine" === p.type ? acc.mixedAdd(p) : acc.add(p);
            }
          }
        }
        /** @type {number} */
        i = 0;
        for (; i < len; i++) {
          /** @type {null} */
          wnd[i] = null;
        }
        return depth ? acc : acc.toP();
      };
      /**
       * @return {?}
       */
      (test.BasePoint = _).prototype.eq = function() {
        throw new Error("Not implemented");
      };
      /**
       * @return {?}
       */
      _.prototype.validate = function() {
        return this.curve.validate(this);
      };
      /**
       * @param {string} value
       * @param {number} offset
       * @return {?}
       */
      test.prototype.decodePoint = function(value, offset) {
        value = utils.toArray(value, offset);
        offset = this.p.byteLength();
        if ((4 === value[0] || 6 === value[0] || 7 === value[0]) && value.length - 1 == 2 * offset) {
          return 6 === value[0] ? assert(value[value.length - 1] % 2 == 0) : 7 === value[0] && assert(value[value.length - 1] % 2 == 1), this.point(value.slice(1, 1 + offset), value.slice(1 + offset, 1 + 2 * offset));
        }
        if ((2 === value[0] || 3 === value[0]) && value.length - 1 === offset) {
          return this.pointFromX(value.slice(1, 1 + offset), 3 === value[0]);
        }
        throw new Error("Unknown point format");
      };
      /**
       * @param {string} value
       * @return {?}
       */
      _.prototype.encodeCompressed = function(value) {
        return this.encode(value, true);
      };
      /**
       * @param {string} cb
       * @return {?}
       */
      _.prototype._encode = function(cb) {
        var key = this.curve.p.byteLength();
        var err = this.getX().toArray("be", key);
        return cb ? [this.getY().isEven() ? 2 : 3].concat(err) : [4].concat(err, this.getY().toArray("be", key));
      };
      /**
       * @param {string} value
       * @param {string} input
       * @return {?}
       */
      _.prototype.encode = function(value, input) {
        return utils.encode(this._encode(input), value);
      };
      /**
       * @param {undefined} power
       * @return {?}
       */
      _.prototype.precompute = function(power) {
        if (this.precomputed) {
          return this;
        }
        var precomputed = {
          doubles : null,
          naf : null,
          beta : null
        };
        return precomputed.naf = this._getNAFPoints(8), precomputed.doubles = this._getDoubles(4, power), precomputed.beta = this._getBeta(), this.precomputed = precomputed, this;
      };
      /**
       * @param {string} k
       * @return {?}
       */
      _.prototype._hasDoubles = function(k) {
        if (!this.precomputed) {
          return false;
        }
        var doubles = this.precomputed.doubles;
        return !!doubles && doubles.points.length >= Math.ceil((k.bitLength() + 1) / doubles.step);
      };
      /**
       * @param {number} step
       * @param {number} power
       * @return {?}
       */
      _.prototype._getDoubles = function(step, power) {
        if (this.precomputed && this.precomputed.doubles) {
          return this.precomputed.doubles;
        }
        /** @type {!Array} */
        var res = [this];
        var buf = this;
        /** @type {number} */
        var i = 0;
        for (; i < power; i = i + step) {
          /** @type {number} */
          var index = 0;
          for (; index < step; index++) {
            buf = buf.dbl();
          }
          res.push(buf);
        }
        return {
          step : step,
          points : res
        };
      };
      /**
       * @param {number} wnd
       * @return {?}
       */
      _.prototype._getNAFPoints = function(wnd) {
        if (this.precomputed && this.precomputed.naf) {
          return this.precomputed.naf;
        }
        /** @type {!Array} */
        var points = [this];
        /** @type {number} */
        var end_flight_idx = (1 << wnd) - 1;
        var l = 1 == end_flight_idx ? null : this.dbl();
        /** @type {number} */
        var i = 1;
        for (; i < end_flight_idx; i++) {
          points[i] = points[i - 1].add(l);
        }
        return {
          wnd : wnd,
          points : points
        };
      };
      /**
       * @return {?}
       */
      _.prototype._getBeta = function() {
        return null;
      };
      /**
       * @param {number} k
       * @return {?}
       */
      _.prototype.dblp = function(k) {
        var r = this;
        /** @type {number} */
        var l = 0;
        for (; l < k; l++) {
          r = r.dbl();
        }
        return r;
      };
    }, {
      "../utils" : 15,
      "bn.js" : 16
    }],
    3 : [function(require, module, i) {
      /**
       * @param {!Object} x
       * @return {undefined}
       */
      function verify(x) {
        /** @type {boolean} */
        this.twisted = 1 != (0 | x.a);
        /** @type {boolean} */
        this.mOneA = this.twisted && -1 == (0 | x.a);
        /** @type {boolean} */
        this.extended = this.mOneA;
        t.call(this, "edwards", x);
        this.a = (new Date(x.a, 16)).umod(this.red.m);
        this.a = this.a.toRed(this.red);
        this.c = (new Date(x.c, 16)).toRed(this.red);
        this.c2 = this.c.redSqr();
        this.d = (new Date(x.d, 16)).toRed(this.red);
        this.dd = this.d.redAdd(this.d);
        assert(!this.twisted || 0 === this.c.fromRed().cmpn(1));
        /** @type {boolean} */
        this.oneC = 1 == (0 | x.c);
      }
      /**
       * @param {?} data
       * @param {number} raw
       * @param {number} index
       * @param {number} str
       * @param {string} date
       * @return {undefined}
       */
      function self(data, raw, index, str, date) {
        t.BasePoint.call(this, data, "projective");
        if (null === raw && null === index && null === str) {
          this.x = this.curve.zero;
          this.y = this.curve.one;
          this.z = this.curve.one;
          this.t = this.curve.zero;
          /** @type {boolean} */
          this.zOne = true;
        } else {
          this.x = new Date(raw, 16);
          this.y = new Date(index, 16);
          this.z = str ? new Date(str, 16) : this.curve.one;
          this.t = date && new Date(date, 16);
          if (!this.x.red) {
            this.x = this.x.toRed(this.curve.red);
          }
          if (!this.y.red) {
            this.y = this.y.toRed(this.curve.red);
          }
          if (!this.z.red) {
            this.z = this.z.toRed(this.curve.red);
          }
          if (this.t && !this.t.red) {
            this.t = this.t.toRed(this.curve.red);
          }
          /** @type {boolean} */
          this.zOne = this.z === this.curve.one;
          if (this.curve.extended && !this.t) {
            this.t = this.x.redMul(this.y);
            if (!this.zOne) {
              this.t = this.t.redMul(this.z.redInvm());
            }
          }
        }
      }
      var chai = require("../utils");
      var Date = require("bn.js");
      var $ = require("inherits");
      var t = require("./base");
      var assert = chai.assert;
      $(verify, t);
      /**
       * @param {!Object} y
       * @return {?}
       */
      (module.exports = verify).prototype._mulA = function(y) {
        return this.mOneA ? y.redNeg() : this.a.redMul(y);
      };
      /**
       * @param {!Object} c
       * @return {?}
       */
      verify.prototype._mulC = function(c) {
        return this.oneC ? c : this.c.redMul(c);
      };
      /**
       * @param {!Object} val
       * @param {!Object} x
       * @param {!Object} y
       * @param {!Object} r
       * @return {?}
       */
      verify.prototype.jpoint = function(val, x, y, r) {
        return this.point(val, x, y, r);
      };
      /**
       * @param {string} val
       * @param {boolean} o
       * @return {?}
       */
      verify.prototype.pointFromX = function(val, o) {
        var y = (val = !(val = new Date(val, 16)).red ? val.toRed(this.red) : val).redSqr();
        var n = this.c2.redSub(this.a.redMul(y));
        y = this.one.redSub(this.c2.redMul(this.d).redMul(y));
        n = n.redMul(y.redInvm());
        y = n.redSqrt();
        if (0 !== y.redSqr().redSub(n).cmp(this.zero)) {
          throw new Error("invalid point");
        }
        n = y.fromRed().isOdd();
        return (o && !n || !o && n) && (y = y.redNeg()), this.point(val, y);
      };
      /**
       * @param {string} x
       * @param {number} indent
       * @return {?}
       */
      verify.prototype.pointFromY = function(x, indent) {
        var y = (x = !(x = new Date(x, 16)).red ? x.toRed(this.red) : x).redSqr();
        var r = y.redSub(this.c2);
        y = y.redMul(this.d).redMul(this.c2).redSub(this.a);
        r = r.redMul(y.redInvm());
        if (0 === r.cmp(this.zero)) {
          if (indent) {
            throw new Error("invalid point");
          }
          return this.point(this.zero, x);
        }
        y = r.redSqrt();
        if (0 !== y.redSqr().redSub(r).cmp(this.zero)) {
          throw new Error("invalid point");
        }
        return y.fromRed().isOdd() !== indent && (y = y.redNeg()), this.point(y, x);
      };
      /**
       * @param {!Object} p
       * @return {?}
       */
      verify.prototype.validate = function(p) {
        if (p.isInfinity()) {
          return true;
        }
        p.normalize();
        var c = p.x.redSqr();
        var b = p.y.redSqr();
        p = c.redMul(this.a).redAdd(b);
        b = this.c2.redMul(this.one.redAdd(this.d.redMul(c).redMul(b)));
        return 0 === p.cmp(b);
      };
      $(self, t.BasePoint);
      /**
       * @param {!Array} cb
       * @return {?}
       */
      verify.prototype.pointFromJSON = function(cb) {
        return self.fromJSON(this, cb);
      };
      /**
       * @param {!Object} x
       * @param {!Object} r
       * @param {!Object} val
       * @param {!Object} str
       * @return {?}
       */
      verify.prototype.point = function(x, r, val, str) {
        return new self(this, x, r, val, str);
      };
      /**
       * @param {?} obj
       * @param {!Array} data
       * @return {?}
       */
      self.fromJSON = function(obj, data) {
        return new self(obj, data[0], data[1], data[2]);
      };
      /**
       * @return {?}
       */
      self.prototype.inspect = function() {
        return this.isInfinity() ? "<EC Point Infinity>" : "<EC Point x: " + this.x.fromRed().toString(16, 2) + " y: " + this.y.fromRed().toString(16, 2) + " z: " + this.z.fromRed().toString(16, 2) + ">";
      };
      /**
       * @return {?}
       */
      self.prototype.isInfinity = function() {
        return 0 === this.x.cmpn(0) && (0 === this.y.cmp(this.z) || this.zOne && 0 === this.y.cmp(this.curve.c));
      };
      /**
       * @return {?}
       */
      self.prototype._extDbl = function() {
        var t = this.x.redSqr();
        var j = this.y.redSqr();
        var c = (c = this.z.redSqr()).redIAdd(c);
        var position = this.curve._mulA(t);
        var r = this.x.redAdd(this.y).redSqr().redISub(t).redISub(j);
        var b = position.redAdd(j);
        t = b.redSub(c);
        c = position.redSub(j);
        position = r.redMul(t);
        j = b.redMul(c);
        c = r.redMul(c);
        b = t.redMul(b);
        return this.curve.point(position, j, b, c);
      };
      /**
       * @return {?}
       */
      self.prototype._projDbl = function() {
        var x;
        var y;
        var r;
        var n;
        var t;
        var i;
        var PHTML = this.x.redAdd(this.y).redSqr();
        var length = this.x.redSqr();
        var str = this.y.redSqr();
        return i = this.curve.twisted ? (t = (r = this.curve._mulA(length)).redAdd(str), this.zOne ? (x = PHTML.redSub(length).redSub(str).redMul(t.redSub(this.curve.two)), y = t.redMul(r.redSub(str)), t.redSqr().redSub(t).redSub(t)) : (n = this.z.redSqr(), i = t.redSub(n).redISub(n), x = PHTML.redSub(length).redISub(str).redMul(i), y = t.redMul(r.redSub(str)), t.redMul(i))) : (r = length.redAdd(str), n = this.curve._mulC(this.z).redSqr(), i = r.redSub(n).redSub(n), x = this.curve._mulC(PHTML.redISub(r)).redMul(i), 
        y = this.curve._mulC(r).redMul(length.redISub(str)), r.redMul(i)), this.curve.point(x, y, i);
      };
      /**
       * @return {?}
       */
      self.prototype.dbl = function() {
        return this.isInfinity() ? this : this.curve.extended ? this._extDbl() : this._projDbl();
      };
      /**
       * @param {!Object} t
       * @return {?}
       */
      self.prototype._extAdd = function(t) {
        var j = this.y.redSub(this.x).redMul(t.y.redSub(t.x));
        var val = this.y.redAdd(this.x).redMul(t.y.redAdd(t.x));
        var c = this.t.redMul(this.curve.dd).redMul(t.t);
        var b = this.z.redMul(t.z.redAdd(t.z));
        var r = val.redSub(j);
        t = b.redSub(c);
        b = b.redAdd(c);
        c = val.redAdd(j);
        val = r.redMul(t);
        j = b.redMul(c);
        c = r.redMul(c);
        b = t.redMul(b);
        return this.curve.point(val, j, b, c);
      };
      /**
       * @param {!Object} to
       * @return {?}
       */
      self.prototype._projAdd = function(to) {
        var j;
        var t = this.z.redMul(to.z);
        var brightColors = t.redSqr();
        var y = this.x.redMul(to.x);
        var element = this.y.redMul(to.y);
        var c = this.curve.d.redMul(y).redMul(element);
        var r = brightColors.redSub(c);
        c = brightColors.redAdd(c);
        to = this.x.redAdd(this.y).redMul(to.x.redAdd(to.y)).redISub(y).redISub(element);
        to = t.redMul(r).redMul(to);
        c = this.curve.twisted ? (j = t.redMul(c).redMul(element.redSub(this.curve._mulA(y))), r.redMul(c)) : (j = t.redMul(c).redMul(element.redSub(y)), this.curve._mulC(r).redMul(c));
        return this.curve.point(to, j, c);
      };
      /**
       * @param {!Object} p
       * @return {?}
       */
      self.prototype.add = function(p) {
        return this.isInfinity() ? p : p.isInfinity() ? this : this.curve.extended ? this._extAdd(p) : this._projAdd(p);
      };
      /**
       * @param {string} value
       * @return {?}
       */
      self.prototype.mul = function(value) {
        return this._hasDoubles(value) ? this.curve._fixedNafMul(this, value) : this.curve._wnafMul(this, value);
      };
      /**
       * @param {string} output
       * @param {?} mulInput
       * @param {string} addInput
       * @return {?}
       */
      self.prototype.mulAdd = function(output, mulInput, addInput) {
        return this.curve._wnafMulAdd(1, [this, mulInput], [output, addInput], 2, false);
      };
      /**
       * @param {string} e
       * @param {?} islongclick
       * @param {string} n32
       * @return {?}
       */
      self.prototype.jmulAdd = function(e, islongclick, n32) {
        return this.curve._wnafMulAdd(1, [this, islongclick], [e, n32], 2, true);
      };
      /**
       * @return {?}
       */
      self.prototype.normalize = function() {
        if (this.zOne) {
          return this;
        }
        var precision = this.z.redInvm();
        return this.x = this.x.redMul(precision), this.y = this.y.redMul(precision), this.t && (this.t = this.t.redMul(precision)), this.z = this.curve.one, this.zOne = true, this;
      };
      /**
       * @return {?}
       */
      self.prototype.neg = function() {
        return this.curve.point(this.x.redNeg(), this.y, this.z, this.t && this.t.redNeg());
      };
      /**
       * @return {?}
       */
      self.prototype.getX = function() {
        return this.normalize(), this.x.fromRed();
      };
      /**
       * @return {?}
       */
      self.prototype.getY = function() {
        return this.normalize(), this.y.fromRed();
      };
      /**
       * @param {!Object} message
       * @return {?}
       */
      self.prototype.eq = function(message) {
        return this === message || 0 === this.getX().cmp(message.getX()) && 0 === this.getY().cmp(message.getY());
      };
      /**
       * @param {string} y
       * @return {?}
       */
      self.prototype.eqXToP = function(y) {
        var x = y.toRed(this.curve.red).redMul(this.z);
        if (0 === this.x.cmp(x)) {
          return true;
        }
        var r = y.clone();
        var res = this.curve.redN.redMul(this.z);
        for (;;) {
          if (r.iadd(this.curve.n), 0 <= r.cmp(this.curve.p)) {
            return false;
          }
          if (x.redIAdd(res), 0 === this.x.cmp(x)) {
            return true;
          }
        }
      };
      /** @type {function(): ?} */
      self.prototype.toP = self.prototype.normalize;
      /** @type {function(!Object): ?} */
      self.prototype.mixedAdd = self.prototype.add;
    }, {
      "../utils" : 15,
      "./base" : 2,
      "bn.js" : 16,
      inherits : 32
    }],
    4 : [function(require, canCreateDiscussions, def) {
      def.base = require("./base");
      def.short = require("./short");
      def.mont = require("./mont");
      def.edwards = require("./edwards");
    }, {
      "./base" : 2,
      "./edwards" : 3,
      "./mont" : 5,
      "./short" : 6
    }],
    5 : [function(require, module, i) {
      /**
       * @param {!Object} response
       * @return {undefined}
       */
      function test(response) {
        server.call(this, "mont", response);
        this.a = (new Error(response.a, 16)).toRed(this.red);
        this.b = (new Error(response.b, 16)).toRed(this.red);
        this.i4 = (new Error(4)).toRed(this.red).redInvm();
        this.two = (new Error(2)).toRed(this.red);
        this.a24 = this.i4.redMul(this.a.redAdd(this.two));
      }
      /**
       * @param {?} name
       * @param {number} val
       * @param {number} desc
       * @return {undefined}
       */
      function self(name, val, desc) {
        server.BasePoint.call(this, name, "projective");
        if (null === val && null === desc) {
          this.x = this.curve.one;
          this.z = this.curve.zero;
        } else {
          this.x = new Error(val, 16);
          this.z = new Error(desc, 16);
          if (!this.x.red) {
            this.x = this.x.toRed(this.curve.red);
          }
          if (!this.z.red) {
            this.z = this.z.toRed(this.curve.red);
          }
        }
      }
      var Error = require("bn.js");
      var cb = require("inherits");
      var server = require("./base");
      var utils = require("../utils");
      cb(test, server);
      /**
       * @param {number} t
       * @return {?}
       */
      (module.exports = test).prototype.validate = function(t) {
        var b = t.normalize().x;
        t = b.redSqr();
        b = t.redMul(b).redAdd(t.redMul(this.a)).redAdd(b);
        return 0 === b.redSqrt().redSqr().cmp(b);
      };
      cb(self, server.BasePoint);
      /**
       * @param {boolean} bytes
       * @param {boolean} enc
       * @return {?}
       */
      test.prototype.decodePoint = function(bytes, enc) {
        return this.point(utils.toArray(bytes, enc), 1);
      };
      /**
       * @param {!Object} str
       * @param {!Object} val
       * @return {?}
       */
      test.prototype.point = function(str, val) {
        return new self(this, str, val);
      };
      /**
       * @param {!Array} cb
       * @return {?}
       */
      test.prototype.pointFromJSON = function(cb) {
        return self.fromJSON(this, cb);
      };
      /**
       * @return {undefined}
       */
      self.prototype.precompute = function() {
      };
      /**
       * @return {?}
       */
      self.prototype._encode = function() {
        return this.getX().toArray("be", this.curve.p.byteLength());
      };
      /**
       * @param {!Object} obj
       * @param {!Array} data
       * @return {?}
       */
      self.fromJSON = function(obj, data) {
        return new self(obj, data[0], data[1] || obj.one);
      };
      /**
       * @return {?}
       */
      self.prototype.inspect = function() {
        return this.isInfinity() ? "<EC Point Infinity>" : "<EC Point x: " + this.x.fromRed().toString(16, 2) + " z: " + this.z.fromRed().toString(16, 2) + ">";
      };
      /**
       * @return {?}
       */
      self.prototype.isInfinity = function() {
        return 0 === this.z.cmpn(0);
      };
      /**
       * @return {?}
       */
      self.prototype.dbl = function() {
        var t = this.x.redAdd(this.z).redSqr();
        var b = this.x.redSub(this.z).redSqr();
        var r = t.redSub(b);
        t = t.redMul(b);
        r = r.redMul(b.redAdd(this.curve.a24.redMul(r)));
        return this.curve.point(t, r);
      };
      /**
       * @return {?}
       */
      self.prototype.add = function() {
        throw new Error("Not supported on Montgomery curve");
      };
      /**
       * @param {!Object} t
       * @param {!Object} node
       * @return {?}
       */
      self.prototype.diffAdd = function(t, node) {
        var c = this.x.redAdd(this.z);
        var res = this.x.redSub(this.z);
        var r = t.x.redAdd(t.z);
        c = t.x.redSub(t.z).redMul(c);
        r = r.redMul(res);
        res = node.z.redMul(c.redAdd(r).redSqr());
        r = node.x.redMul(c.redISub(r).redSqr());
        return this.curve.point(res, r);
      };
      /**
       * @param {string} value
       * @return {?}
       */
      self.prototype.mul = function(value) {
        var num = value.clone();
        var sureMsg = this;
        var r = this.curve.point(null, null);
        /** @type {!Array} */
        var d3MapTileLayer = [];
        for (; 0 !== num.cmpn(0); num.iushrn(1)) {
          d3MapTileLayer.push(num.andln(1));
        }
        /** @type {number} */
        var x = d3MapTileLayer.length - 1;
        for (; 0 <= x; x--) {
          if (0 === d3MapTileLayer[x]) {
            sureMsg = sureMsg.diffAdd(r, this);
            r = r.dbl();
          } else {
            r = sureMsg.diffAdd(r, this);
            sureMsg = sureMsg.dbl();
          }
        }
        return r;
      };
      /**
       * @return {?}
       */
      self.prototype.mulAdd = function() {
        throw new Error("Not supported on Montgomery curve");
      };
      /**
       * @return {?}
       */
      self.prototype.jumlAdd = function() {
        throw new Error("Not supported on Montgomery curve");
      };
      /**
       * @param {!Object} message
       * @return {?}
       */
      self.prototype.eq = function(message) {
        return 0 === this.getX().cmp(message.getX());
      };
      /**
       * @return {?}
       */
      self.prototype.normalize = function() {
        return this.x = this.x.redMul(this.z.redInvm()), this.z = this.curve.one, this;
      };
      /**
       * @return {?}
       */
      self.prototype.getX = function() {
        return this.normalize(), this.x.fromRed();
      };
    }, {
      "../utils" : 15,
      "./base" : 2,
      "bn.js" : 16,
      inherits : 32
    }],
    6 : [function(require, module, i) {
      /**
       * @param {!Object} config
       * @return {undefined}
       */
      function test(config) {
        state.call(this, "short", config);
        this.a = (new Buffer(config.a, 16)).toRed(this.red);
        this.b = (new Buffer(config.b, 16)).toRed(this.red);
        this.tinv = this.two.redInvm();
        /** @type {boolean} */
        this.zeroA = 0 === this.a.fromRed().cmpn(0);
        /** @type {boolean} */
        this.threeA = 0 === this.a.fromRed().sub(this.p).cmpn(-3);
        this.endo = this._getEndomorphism(config);
        /** @type {!Array} */
        this._endoWnafT1 = new Array(4);
        /** @type {!Array} */
        this._endoWnafT2 = new Array(4);
      }
      /**
       * @param {?} name
       * @param {number} val
       * @param {number} desc
       * @param {?} count
       * @return {undefined}
       */
      function self(name, val, desc, count) {
        state.BasePoint.call(this, name, "affine");
        if (null === val && null === desc) {
          /** @type {null} */
          this.x = null;
          /** @type {null} */
          this.y = null;
          /** @type {boolean} */
          this.inf = true;
        } else {
          this.x = new Buffer(val, 16);
          this.y = new Buffer(desc, 16);
          if (count) {
            this.x.forceRed(this.curve.red);
            this.y.forceRed(this.curve.red);
          }
          if (!this.x.red) {
            this.x = this.x.toRed(this.curve.red);
          }
          if (!this.y.red) {
            this.y = this.y.toRed(this.curve.red);
          }
          /** @type {boolean} */
          this.inf = false;
        }
      }
      /**
       * @param {?} message
       * @param {number} stream
       * @param {number} x
       * @param {number} z
       * @return {undefined}
       */
      function Point(message, stream, x, z) {
        state.BasePoint.call(this, message, "jacobian");
        if (null === stream && null === x && null === z) {
          this.x = this.curve.one;
          this.y = this.curve.one;
          this.z = new Buffer(0);
        } else {
          this.x = new Buffer(stream, 16);
          this.y = new Buffer(x, 16);
          this.z = new Buffer(z, 16);
        }
        if (!this.x.red) {
          this.x = this.x.toRed(this.curve.red);
        }
        if (!this.y.red) {
          this.y = this.y.toRed(this.curve.red);
        }
        if (!this.z.red) {
          this.z = this.z.toRed(this.curve.red);
        }
        /** @type {boolean} */
        this.zOne = this.z === this.curve.one;
      }
      var chai = require("../utils");
      var Buffer = require("bn.js");
      var extend = require("inherits");
      var state = require("./base");
      var assert = chai.assert;
      extend(test, state);
      /**
       * @param {!Object} options
       * @return {?}
       */
      (module.exports = test).prototype._getEndomorphism = function(options) {
        var a;
        var b;
        var out;
        if (this.zeroA && this.g && this.n && 1 === this.p.modn(3)) {
          return b = (options.beta ? new Buffer(options.beta, 16) : b = (out = this._getEndoRoots(this.p))[0].cmp(out[1]) < 0 ? out[0] : out[1]).toRed(this.red), options.lambda ? a = new Buffer(options.lambda, 16) : (out = this._getEndoRoots(this.n), 0 === this.g.mul(out[0]).x.cmp(this.g.x.redMul(b)) ? a = out[0] : (a = out[1], assert(0 === this.g.mul(a).x.cmp(this.g.x.redMul(b))))), {
            beta : b,
            lambda : a,
            basis : options.basis ? options.basis.map(function(value) {
              return {
                a : new Buffer(value.a, 16),
                b : new Buffer(value.b, 16)
              };
            }) : this._getEndoBasis(a)
          };
        }
      };
      /**
       * @param {number} key
       * @return {?}
       */
      test.prototype._getEndoRoots = function(key) {
        var newOperators = key === this.p ? this.red : Buffer.mont(key);
        var D = (new Buffer(2)).toRed(newOperators).redInvm();
        key = D.redNeg();
        D = (new Buffer(3)).toRed(newOperators).redNeg().redSqrt().redMul(D);
        return [key.redAdd(D).fromRed(), key.redSub(D).fromRed()];
      };
      /**
       * @param {!Object} a
       * @return {?}
       */
      test.prototype._getEndoBasis = function(a) {
        var tempMatch;
        var i;
        var d;
        var e;
        var match;
        var index;
        var f;
        var na = this.n.ushrn(Math.floor(this.n.bitLength() / 2));
        /** @type {!Object} */
        var p = a;
        var content = this.n.clone();
        var x = new Buffer(1);
        var c = new Buffer(0);
        var b = new Buffer(0);
        var s = new Buffer(1);
        /** @type {number} */
        var m = 0;
        for (; 0 !== p.cmpn(0);) {
          var w = content.div(p);
          var self = content.sub(w.mul(p));
          var end = b.sub(w.mul(x));
          w = s.sub(w.mul(c));
          if (!d && self.cmp(na) < 0) {
            tempMatch = f.neg();
            i = x;
            d = self.neg();
            e = end;
          } else {
            if (d && 2 == ++m) {
              break;
            }
          }
          content = p;
          p = f = self;
          b = x;
          x = end;
          s = c;
          c = w;
        }
        match = self.neg();
        index = end;
        a = d.sqr().add(e.sqr());
        return 0 <= match.sqr().add(index.sqr()).cmp(a) && (match = tempMatch, index = i), d.negative && (d = d.neg(), e = e.neg()), match.negative && (match = match.neg(), index = index.neg()), [{
          a : d,
          b : e
        }, {
          a : match,
          b : index
        }];
      };
      /**
       * @param {!Array} e
       * @return {?}
       */
      test.prototype._endoSplit = function(e) {
        var f = this.endo.basis;
        var res = f[0];
        var p1 = f[1];
        var d = p1.b.mul(e).divRound(this.n);
        var that = res.b.neg().mul(e).divRound(this.n);
        var t = d.mul(res.a);
        f = that.mul(p1.a);
        res = d.mul(res.b);
        p1 = that.mul(p1.b);
        return {
          k1 : e.sub(t).sub(f),
          k2 : res.add(p1).neg()
        };
      };
      /**
       * @param {string} c
       * @param {boolean} o
       * @return {?}
       */
      test.prototype.pointFromX = function(c, o) {
        var n = (c = !(c = new Buffer(c, 16)).red ? c.toRed(this.red) : c).redSqr().redMul(c).redIAdd(c.redMul(this.a)).redIAdd(this.b);
        var y = n.redSqrt();
        if (0 !== y.redSqr().redSub(n).cmp(this.zero)) {
          throw new Error("invalid point");
        }
        n = y.fromRed().isOdd();
        return (o && !n || !o && n) && (y = y.redNeg()), this.point(c, y);
      };
      /**
       * @param {!Object} obj
       * @return {?}
       */
      test.prototype.validate = function(obj) {
        if (obj.inf) {
          return true;
        }
        var x = obj.x;
        var b = obj.y;
        obj = this.a.redMul(x);
        obj = x.redSqr().redMul(x).redIAdd(obj).redIAdd(this.b);
        return 0 === b.redSqr().redISub(obj).cmpn(0);
      };
      /**
       * @param {!Array} l
       * @param {!Array} t
       * @param {boolean} i
       * @return {?}
       */
      test.prototype._endoWnafMulAdd = function(l, t, i) {
        var vertices = this._endoWnafT1;
        var row = this._endoWnafT2;
        /** @type {number} */
        var k = 0;
        for (; k < l.length; k++) {
          var params = this._endoSplit(t[k]);
          var r = l[k];
          var vStart = r._getBeta();
          if (params.k1.negative) {
            params.k1.ineg();
            r = r.neg(true);
          }
          if (params.k2.negative) {
            params.k2.ineg();
            vStart = vStart.neg(true);
          }
          vertices[2 * k] = r;
          vertices[2 * k + 1] = vStart;
          row[2 * k] = params.k1;
          row[2 * k + 1] = params.k2;
        }
        i = this._wnafMulAdd(1, vertices, row, 2 * k, i);
        /** @type {number} */
        var c = 0;
        for (; c < 2 * k; c++) {
          /** @type {null} */
          vertices[c] = null;
          /** @type {null} */
          row[c] = null;
        }
        return i;
      };
      extend(self, state.BasePoint);
      /**
       * @param {!Object} str
       * @param {!Object} val
       * @param {!Object} p
       * @return {?}
       */
      test.prototype.point = function(str, val, p) {
        return new self(this, str, val, p);
      };
      /**
       * @param {!Array} json
       * @param {!Object} target
       * @return {?}
       */
      test.prototype.pointFromJSON = function(json, target) {
        return self.fromJSON(this, json, target);
      };
      /**
       * @return {?}
       */
      self.prototype._getBeta = function() {
        if (this.curve.endo) {
          var e = this.precomputed;
          if (e && e.beta) {
            return e.beta;
          }
          var m;
          var point;
          var t = this.curve.point(this.x.redMul(this.curve.endo.beta), this.y);
          return e && (m = this.curve, point = function(p) {
            return m.point(p.x.redMul(m.endo.beta), p.y);
          }, (e.beta = t).precomputed = {
            beta : null,
            naf : e.naf && {
              wnd : e.naf.wnd,
              points : e.naf.points.map(point)
            },
            doubles : e.doubles && {
              step : e.doubles.step,
              points : e.doubles.points.map(point)
            }
          }), t;
        }
      };
      /**
       * @return {?}
       */
      self.prototype.toJSON = function() {
        return this.precomputed ? [this.x, this.y, this.precomputed && {
          doubles : this.precomputed.doubles && {
            step : this.precomputed.doubles.step,
            points : this.precomputed.doubles.points.slice(1)
          },
          naf : this.precomputed.naf && {
            wnd : this.precomputed.naf.wnd,
            points : this.precomputed.naf.points.slice(1)
          }
        }] : [this.x, this.y];
      };
      /**
       * @param {!Object} stream
       * @param {!Array} data
       * @param {!Object} key
       * @return {?}
       */
      self.fromJSON = function(stream, data, key) {
        /**
         * @param {!Object} coordinate
         * @return {?}
         */
        function parameters(coordinate) {
          return stream.point(coordinate[0], coordinate[1], key);
        }
        if ("string" == typeof data) {
          /** @type {*} */
          data = JSON.parse(data);
        }
        var r = stream.point(data[0], data[1], key);
        if (!data[2]) {
          return r;
        }
        data = data[2];
        return r.precomputed = {
          beta : null,
          doubles : data.doubles && {
            step : data.doubles.step,
            points : [r].concat(data.doubles.points.map(parameters))
          },
          naf : data.naf && {
            wnd : data.naf.wnd,
            points : [r].concat(data.naf.points.map(parameters))
          }
        }, r;
      };
      /**
       * @return {?}
       */
      self.prototype.inspect = function() {
        return this.isInfinity() ? "<EC Point Infinity>" : "<EC Point x: " + this.x.fromRed().toString(16, 2) + " y: " + this.y.fromRed().toString(16, 2) + ">";
      };
      /**
       * @return {?}
       */
      self.prototype.isInfinity = function() {
        return this.inf;
      };
      /**
       * @param {!Object} a
       * @return {?}
       */
      self.prototype.add = function(a) {
        if (this.inf) {
          return a;
        }
        if (a.inf) {
          return this;
        }
        if (this.eq(a)) {
          return this.dbl();
        }
        if (this.neg().eq(a)) {
          return this.curve.point(null, null);
        }
        if (0 === this.x.cmp(a.x)) {
          return this.curve.point(null, null);
        }
        var r = this.y.redSub(a.y);
        a = (r = 0 !== r.cmpn(0) ? r.redMul(this.x.redSub(a.x).redInvm()) : r).redSqr().redISub(this.x).redISub(a.x);
        r = r.redMul(this.x.redSub(a)).redISub(this.y);
        return this.curve.point(a, r);
      };
      /**
       * @return {?}
       */
      self.prototype.dbl = function() {
        if (this.inf) {
          return this;
        }
        var r = this.y.redAdd(this.y);
        if (0 === r.cmpn(0)) {
          return this.curve.point(null, null);
        }
        var y = this.curve.a;
        var x = this.x.redSqr();
        r = r.redInvm();
        y = x.redAdd(x).redIAdd(x).redIAdd(y).redMul(r);
        r = y.redSqr().redISub(this.x.redAdd(this.x));
        y = y.redMul(this.x.redSub(r)).redISub(this.y);
        return this.curve.point(r, y);
      };
      /**
       * @return {?}
       */
      self.prototype.getX = function() {
        return this.x.fromRed();
      };
      /**
       * @return {?}
       */
      self.prototype.getY = function() {
        return this.y.fromRed();
      };
      /**
       * @param {string} value
       * @return {?}
       */
      self.prototype.mul = function(value) {
        return value = new Buffer(value, 16), this.isInfinity() ? this : this._hasDoubles(value) ? this.curve._fixedNafMul(this, value) : this.curve.endo ? this.curve._endoWnafMulAdd([this], [value]) : this.curve._wnafMul(this, value);
      };
      /**
       * @param {string} c
       * @param {!Array} r
       * @param {?} data
       * @return {?}
       */
      self.prototype.mulAdd = function(c, r, data) {
        /** @type {!Array} */
        r = [this, r];
        /** @type {!Array} */
        data = [c, data];
        return this.curve.endo ? this.curve._endoWnafMulAdd(r, data) : this.curve._wnafMulAdd(1, r, data, 2);
      };
      /**
       * @param {string} id
       * @param {!Array} r
       * @param {?} value
       * @return {?}
       */
      self.prototype.jmulAdd = function(id, r, value) {
        /** @type {!Array} */
        r = [this, r];
        /** @type {!Array} */
        value = [id, value];
        return this.curve.endo ? this.curve._endoWnafMulAdd(r, value, true) : this.curve._wnafMulAdd(1, r, value, 2, true);
      };
      /**
       * @param {!Object} result
       * @return {?}
       */
      self.prototype.eq = function(result) {
        return this === result || this.inf === result.inf && (this.inf || 0 === this.x.cmp(result.x) && 0 === this.y.cmp(result.y));
      };
      /**
       * @param {string} x
       * @return {?}
       */
      self.prototype.neg = function(x) {
        if (this.inf) {
          return this;
        }
        var $scope;
        var p = this.curve.point(this.x, this.y.redNeg());
        return x && this.precomputed && ($scope = this.precomputed, x = function(result) {
          return result.neg();
        }, p.precomputed = {
          naf : $scope.naf && {
            wnd : $scope.naf.wnd,
            points : $scope.naf.points.map(x)
          },
          doubles : $scope.doubles && {
            step : $scope.doubles.step,
            points : $scope.doubles.points.map(x)
          }
        }), p;
      };
      /**
       * @return {?}
       */
      self.prototype.toJ = function() {
        return this.inf ? this.curve.jpoint(null, null, null) : this.curve.jpoint(this.x, this.y, this.curve.one);
      };
      extend(Point, state.BasePoint);
      /**
       * @param {!Object} d
       * @param {!Object} x
       * @param {!Object} y
       * @return {?}
       */
      test.prototype.jpoint = function(d, x, y) {
        return new Point(this, d, x, y);
      };
      /**
       * @return {?}
       */
      Point.prototype.toP = function() {
        if (this.isInfinity()) {
          return this.curve.point(null, null);
        }
        var y = this.z.redInvm();
        var d = y.redSqr();
        var left = this.x.redMul(d);
        y = this.y.redMul(d).redMul(y);
        return this.curve.point(left, y);
      };
      /**
       * @return {?}
       */
      Point.prototype.neg = function() {
        return this.curve.jpoint(this.x, this.y.redNeg(), this.z);
      };
      /**
       * @param {!Object} point
       * @return {?}
       */
      Point.prototype.add = function(point) {
        if (this.isInfinity()) {
          return point;
        }
        if (point.isInfinity()) {
          return this;
        }
        var x = point.z.redSqr();
        var b = this.z.redSqr();
        var n = this.x.redMul(x);
        var r = point.x.redMul(b);
        var t = this.y.redMul(x.redMul(point.z));
        var c = point.y.redMul(b.redMul(this.z));
        x = n.redSub(r);
        b = t.redSub(c);
        if (0 === x.cmpn(0)) {
          return 0 !== b.cmpn(0) ? this.curve.jpoint(null, null, null) : this.dbl();
        }
        r = x.redSqr();
        c = r.redMul(x);
        n = n.redMul(r);
        r = b.redSqr().redIAdd(c).redISub(n).redISub(n);
        c = b.redMul(n.redISub(r)).redISub(t.redMul(c));
        x = this.z.redMul(point.z).redMul(x);
        return this.curve.jpoint(r, c, x);
      };
      /**
       * @param {!Object} b
       * @return {?}
       */
      Point.prototype.mixedAdd = function(b) {
        if (this.isInfinity()) {
          return b.toJ();
        }
        if (b.isInfinity()) {
          return this;
        }
        var r = this.z.redSqr();
        var x = this.x;
        var t = b.x.redMul(r);
        var pa = this.y;
        var a = b.y.redMul(r).redMul(this.z);
        b = x.redSub(t);
        r = pa.redSub(a);
        if (0 === b.cmpn(0)) {
          return 0 !== r.cmpn(0) ? this.curve.jpoint(null, null, null) : this.dbl();
        }
        t = b.redSqr();
        a = t.redMul(b);
        x = x.redMul(t);
        t = r.redSqr().redIAdd(a).redISub(x).redISub(x);
        a = r.redMul(x.redISub(t)).redISub(pa.redMul(a));
        b = this.z.redMul(b);
        return this.curve.jpoint(t, a, b);
      };
      /**
       * @param {number} k
       * @return {?}
       */
      Point.prototype.dblp = function(k) {
        if (0 === k) {
          return this;
        }
        if (this.isInfinity()) {
          return this;
        }
        if (!k) {
          return this.dbl();
        }
        if (this.curve.zeroA || this.curve.threeA) {
          var r = this;
          /** @type {number} */
          var leftSums = 0;
          for (; leftSums < k; leftSums++) {
            r = r.dbl();
          }
          return r;
        }
        var sigr = this.curve.a;
        var s = this.curve.tinv;
        var t = this.x;
        var v = this.y;
        var x = this.z;
        var z2 = x.redSqr().redSqr();
        var y = v.redAdd(v);
        /** @type {number} */
        leftSums = 0;
        for (; leftSums < k; leftSums++) {
          var c = t.redSqr();
          var b = y.redSqr();
          var mid = b.redSqr();
          var r = c.redAdd(c).redIAdd(c).redIAdd(sigr.redMul(z2));
          c = t.redMul(b);
          b = r.redSqr().redISub(c.redAdd(c));
          c = c.redISub(b);
          r = (r = r.redMul(c)).redIAdd(r).redISub(mid);
          c = y.redMul(x);
          if (leftSums + 1 < k) {
            z2 = z2.redMul(mid);
          }
          t = b;
          x = c;
          y = r;
        }
        return this.curve.jpoint(t, y.redMul(s), x);
      };
      /**
       * @return {?}
       */
      Point.prototype.dbl = function() {
        return this.isInfinity() ? this : this.curve.zeroA ? this._zeroDbl() : this.curve.threeA ? this._threeDbl() : this._dbl();
      };
      /**
       * @return {?}
       */
      Point.prototype._zeroDbl = function() {
        var t;
        var r;
        var x;
        var n;
        var i;
        var z = this.zOne ? (x = this.x.redSqr(), t = (n = this.y.redSqr()).redSqr(), i = (i = this.x.redAdd(n).redSqr().redISub(x).redISub(t)).redIAdd(i), n = (r = x.redAdd(x).redIAdd(x)).redSqr().redISub(i).redISub(i), x = (x = (x = t.redIAdd(t)).redIAdd(x)).redIAdd(x), t = n, r = r.redMul(i.redISub(n)).redISub(x), this.y.redAdd(this.y)) : (i = this.x.redSqr(), z = (n = this.y.redSqr()).redSqr(), x = (x = this.x.redAdd(n).redSqr().redISub(i).redISub(z)).redIAdd(x), i = (n = i.redAdd(i).redIAdd(i)).redSqr(), 
        z = (z = (z = z.redIAdd(z)).redIAdd(z)).redIAdd(z), t = i.redISub(x).redISub(x), r = n.redMul(x.redISub(t)).redISub(z), (z = this.y.redMul(this.z)).redIAdd(z));
        return this.curve.jpoint(t, r, z);
      };
      /**
       * @return {?}
       */
      Point.prototype._threeDbl = function() {
        var y;
        var w;
        var i;
        var z;
        var x;
        var s;
        var text;
        var b;
        return this.zOne ? (y = this.x.redSqr(), z = (w = this.y.redSqr()).redSqr(), text = (text = this.x.redAdd(w).redSqr().redISub(y).redISub(z)).redIAdd(text), i = x = (b = y.redAdd(y).redIAdd(y).redIAdd(this.curve.a)).redSqr().redISub(text).redISub(text), s = (s = (s = z.redIAdd(z)).redIAdd(s)).redIAdd(s), w = b.redMul(text.redISub(x)).redISub(s), y = this.y.redAdd(this.y)) : (z = this.z.redSqr(), b = this.y.redSqr(), text = this.x.redMul(b), x = (x = this.x.redSub(z).redMul(this.x.redAdd(z))).redAdd(x).redIAdd(x), 
        text = (s = (s = text.redIAdd(text)).redIAdd(s)).redAdd(s), i = x.redSqr().redISub(text), y = this.y.redAdd(this.z).redSqr().redISub(b).redISub(z), b = (b = (b = (b = b.redSqr()).redIAdd(b)).redIAdd(b)).redIAdd(b), w = x.redMul(s.redISub(i)).redISub(b)), this.curve.jpoint(i, w, y);
      };
      /**
       * @return {?}
       */
      Point.prototype._dbl = function() {
        var t = this.curve.a;
        var x = this.x;
        var y = this.y;
        var b = this.z;
        var r = b.redSqr().redSqr();
        var nOne = x.redSqr();
        var z = y.redSqr();
        t = nOne.redAdd(nOne).redIAdd(nOne).redIAdd(t.redMul(r));
        r = x.redAdd(x);
        x = (r = r.redIAdd(r)).redMul(z);
        r = t.redSqr().redISub(x.redAdd(x));
        x = x.redISub(r);
        z = z.redSqr();
        z = (z = (z = z.redIAdd(z)).redIAdd(z)).redIAdd(z);
        z = t.redMul(x).redISub(z);
        b = y.redAdd(y).redMul(b);
        return this.curve.jpoint(r, z, b);
      };
      /**
       * @return {?}
       */
      Point.prototype.trpl = function() {
        if (!this.curve.zeroA) {
          return this.dbl().add(this);
        }
        var x = this.x.redSqr();
        var t = this.y.redSqr();
        var incDays = this.z.redSqr();
        var n = t.redSqr();
        var b = x.redAdd(x).redIAdd(x);
        var r = b.redSqr();
        var z = this.x.redAdd(t).redSqr().redISub(x).redISub(n);
        x = (z = (z = (z = z.redIAdd(z)).redAdd(z).redIAdd(z)).redISub(r)).redSqr();
        n = n.redIAdd(n);
        n = (n = (n = n.redIAdd(n)).redIAdd(n)).redIAdd(n);
        r = b.redIAdd(z).redSqr().redISub(r).redISub(x).redISub(n);
        t = t.redMul(r);
        t = (t = t.redIAdd(t)).redIAdd(t);
        t = this.x.redMul(x).redISub(t);
        t = (t = t.redIAdd(t)).redIAdd(t);
        r = this.y.redMul(r.redMul(n.redISub(r)).redISub(z.redMul(x)));
        r = (r = (r = r.redIAdd(r)).redIAdd(r)).redIAdd(r);
        x = this.z.redAdd(z).redSqr().redISub(incDays).redISub(x);
        return this.curve.jpoint(t, r, x);
      };
      /**
       * @param {string} b
       * @param {!Object} a
       * @return {?}
       */
      Point.prototype.mul = function(b, a) {
        return b = new Buffer(b, a), this.curve._wnafMul(this, b);
      };
      /**
       * @param {!Object} point
       * @return {?}
       */
      Point.prototype.eq = function(point) {
        if ("affine" === point.type) {
          return this.eq(point.toJ());
        }
        if (this === point) {
          return true;
        }
        var b = this.z.redSqr();
        var newScale = point.z.redSqr();
        if (0 !== this.x.redMul(newScale).redISub(point.x.redMul(b)).cmpn(0)) {
          return false;
        }
        b = b.redMul(this.z);
        newScale = newScale.redMul(point.z);
        return 0 === this.y.redMul(newScale).redISub(point.y.redMul(b)).cmpn(0);
      };
      /**
       * @param {string} y
       * @return {?}
       */
      Point.prototype.eqXToP = function(y) {
        var b = this.z.redSqr();
        var d = y.toRed(this.curve.red).redMul(b);
        if (0 === this.x.cmp(d)) {
          return true;
        }
        var r = y.clone();
        var f = this.curve.redN.redMul(b);
        for (;;) {
          if (r.iadd(this.curve.n), 0 <= r.cmp(this.curve.p)) {
            return false;
          }
          if (d.redIAdd(f), 0 === this.x.cmp(d)) {
            return true;
          }
        }
      };
      /**
       * @return {?}
       */
      Point.prototype.inspect = function() {
        return this.isInfinity() ? "<EC JPoint Infinity>" : "<EC JPoint x: " + this.x.toString(16, 2) + " y: " + this.y.toString(16, 2) + " z: " + this.z.toString(16, 2) + ">";
      };
      /**
       * @return {?}
       */
      Point.prototype.isInfinity = function() {
        return 0 === this.z.cmpn(0);
      };
    }, {
      "../utils" : 15,
      "./base" : 2,
      "bn.js" : 16,
      inherits : 32
    }],
    7 : [function(require, canCreateDiscussions, hash) {
      /**
       * @param {!Object} options
       * @return {undefined}
       */
      function sign(options) {
        if ("short" === options.type) {
          this.curve = new BN.short(options);
        } else {
          if ("edwards" === options.type) {
            this.curve = new BN.edwards(options);
          } else {
            this.curve = new BN.mont(options);
          }
        }
        this.g = this.curve.g;
        this.n = this.curve.n;
        this.hash = options.hash;
        assert(this.g.validate(), "Invalid curve");
        assert(this.g.mul(this.n).isInfinity(), "Invalid curve, G*N != O");
      }
      /**
       * @param {string} name
       * @param {?} result
       * @return {undefined}
       */
      function test(name, result) {
        Object.defineProperty(self, name, {
          configurable : true,
          enumerable : true,
          get : function() {
            var commandResult = new sign(result);
            return Object.defineProperty(self, name, {
              configurable : true,
              enumerable : true,
              value : commandResult
            }), commandResult;
          }
        });
      }
      var HeadlessApi;
      var self = hash;
      hash = require("hash.js");
      var BN = require("./curve");
      var assert = require("./utils").assert;
      /** @type {function(!Object): undefined} */
      self.PresetCurve = sign;
      test("p192", {
        type : "short",
        prime : "p192",
        p : "ffffffff ffffffff ffffffff fffffffe ffffffff ffffffff",
        a : "ffffffff ffffffff ffffffff fffffffe ffffffff fffffffc",
        b : "64210519 e59c80e7 0fa7e9ab 72243049 feb8deec c146b9b1",
        n : "ffffffff ffffffff ffffffff 99def836 146bc9b1 b4d22831",
        hash : hash.sha256,
        gRed : false,
        g : ["188da80e b03090f6 7cbf20eb 43a18800 f4ff0afd 82ff1012", "07192b95 ffc8da78 631011ed 6b24cdd5 73f977a1 1e794811"]
      });
      test("p224", {
        type : "short",
        prime : "p224",
        p : "ffffffff ffffffff ffffffff ffffffff 00000000 00000000 00000001",
        a : "ffffffff ffffffff ffffffff fffffffe ffffffff ffffffff fffffffe",
        b : "b4050a85 0c04b3ab f5413256 5044b0b7 d7bfd8ba 270b3943 2355ffb4",
        n : "ffffffff ffffffff ffffffff ffff16a2 e0b8f03e 13dd2945 5c5c2a3d",
        hash : hash.sha256,
        gRed : false,
        g : ["b70e0cbd 6bb4bf7f 321390b9 4a03c1d3 56c21122 343280d6 115c1d21", "bd376388 b5f723fb 4c22dfe6 cd4375a0 5a074764 44d58199 85007e34"]
      });
      test("p256", {
        type : "short",
        prime : null,
        p : "ffffffff 00000001 00000000 00000000 00000000 ffffffff ffffffff ffffffff",
        a : "ffffffff 00000001 00000000 00000000 00000000 ffffffff ffffffff fffffffc",
        b : "5ac635d8 aa3a93e7 b3ebbd55 769886bc 651d06b0 cc53b0f6 3bce3c3e 27d2604b",
        n : "ffffffff 00000000 ffffffff ffffffff bce6faad a7179e84 f3b9cac2 fc632551",
        hash : hash.sha256,
        gRed : false,
        g : ["6b17d1f2 e12c4247 f8bce6e5 63a440f2 77037d81 2deb33a0 f4a13945 d898c296", "4fe342e2 fe1a7f9b 8ee7eb4a 7c0f9e16 2bce3357 6b315ece cbb64068 37bf51f5"]
      });
      test("p384", {
        type : "short",
        prime : null,
        p : "ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff fffffffe ffffffff 00000000 00000000 ffffffff",
        a : "ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff fffffffe ffffffff 00000000 00000000 fffffffc",
        b : "b3312fa7 e23ee7e4 988e056b e3f82d19 181d9c6e fe814112 0314088f 5013875a c656398d 8a2ed19d 2a85c8ed d3ec2aef",
        n : "ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff c7634d81 f4372ddf 581a0db2 48b0a77a ecec196a ccc52973",
        hash : hash.sha384,
        gRed : false,
        g : ["aa87ca22 be8b0537 8eb1c71e f320ad74 6e1d3b62 8ba79b98 59f741e0 82542a38 5502f25d bf55296c 3a545e38 72760ab7", "3617de4a 96262c6f 5d9e98bf 9292dc29 f8f41dbd 289a147c e9da3113 b5f0b8c0 0a60b1ce 1d7e819d 7a431d7c 90ea0e5f"]
      });
      test("p521", {
        type : "short",
        prime : null,
        p : "000001ff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff",
        a : "000001ff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff fffffffc",
        b : "00000051 953eb961 8e1c9a1f 929a21a0 b68540ee a2da725b 99b315f3 b8b48991 8ef109e1 56193951 ec7e937b 1652c0bd 3bb1bf07 3573df88 3d2c34f1 ef451fd4 6b503f00",
        n : "000001ff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff fffffffa 51868783 bf2f966b 7fcc0148 f709a5d0 3bb5c9b8 899c47ae bb6fb71e 91386409",
        hash : hash.sha512,
        gRed : false,
        g : ["000000c6 858e06b7 0404e9cd 9e3ecb66 2395b442 9c648139 053fb521 f828af60 6b4d3dba a14b5e77 efe75928 fe1dc127 a2ffa8de 3348b3c1 856a429b f97e7e31 c2e5bd66", "00000118 39296a78 9a3bc004 5c8a5fb4 2c7d1bd9 98f54449 579b4468 17afbd17 273e662c 97ee7299 5ef42640 c550b901 3fad0761 353c7086 a272c240 88be9476 9fd16650"]
      });
      test("curve25519", {
        type : "mont",
        prime : "p25519",
        p : "7fffffffffffffff ffffffffffffffff ffffffffffffffff ffffffffffffffed",
        a : "76d06",
        b : "1",
        n : "1000000000000000 0000000000000000 14def9dea2f79cd6 5812631a5cf5d3ed",
        hash : hash.sha256,
        gRed : false,
        g : ["9"]
      });
      test("ed25519", {
        type : "edwards",
        prime : "p25519",
        p : "7fffffffffffffff ffffffffffffffff ffffffffffffffff ffffffffffffffed",
        a : "-1",
        c : "1",
        d : "52036cee2b6ffe73 8cc740797779e898 00700a4d4141d8ab 75eb4dca135978a3",
        n : "1000000000000000 0000000000000000 14def9dea2f79cd6 5812631a5cf5d3ed",
        hash : hash.sha256,
        gRed : false,
        g : ["216936d3cd6e53fec0a4e231fdd6dc5c692cc7609525a7b2c9562d608f25d51a", "6666666666666666666666666666666666666666666666666666666666666658"]
      });
      try {
        HeadlessApi = require("./precomputed/secp256k1");
      } catch (e) {
        HeadlessApi = void 0;
      }
      test("secp256k1", {
        type : "short",
        prime : "k256",
        p : "ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff fffffffe fffffc2f",
        a : "0",
        b : "7",
        n : "ffffffff ffffffff ffffffff fffffffe baaedce6 af48a03b bfd25e8c d0364141",
        h : "1",
        hash : hash.sha256,
        beta : "7ae96a2b657c07106e64479eac3434e99cf0497512f58995c1396c28719501ee",
        lambda : "5363ad4cc05c30e0a5261c028812645a122e22ea20816678df02967c1b23bd72",
        basis : [{
          a : "3086d221a7d46bcde86c90e49284eb15",
          b : "-e4437ed6010e88286f547fa90abfe4c3"
        }, {
          a : "114ca50f7a8e2f3f657c1108d9d44cfd8",
          b : "3086d221a7d46bcde86c90e49284eb15"
        }],
        gRed : false,
        g : ["79be667ef9dcbbac55a06295ce870b07029bfcdb2dce28d959f2815b16f81798", "483ada7726a3c4655da4fbfc0e1108a8fd17b448a68554199c47d08ffb10d4b8", HeadlessApi]
      });
    }, {
      "./curve" : 4,
      "./precomputed/secp256k1" : 14,
      "./utils" : 15,
      "hash.js" : 19
    }],
    8 : [function(require, module, i) {
      /**
       * @param {!Object} options
       * @return {?}
       */
      function test(options) {
        if (!(this instanceof test)) {
          return new test(options);
        }
        if ("string" == typeof options) {
          assert(Object.prototype.hasOwnProperty.call(url, options), "Unknown curve " + options);
          options = url[options];
        }
        if (options instanceof url.PresetCurve) {
          options = {
            curve : options
          };
        }
        this.curve = options.curve.curve;
        this.n = this.curve.n;
        this.nh = this.n.ushrn(1);
        this.g = this.curve.g;
        this.g = options.curve.g;
        this.g.precompute(options.curve.n.bitLength() + 1);
        this.hash = options.hash || options.curve.hash;
      }
      var bn = require("bn.js");
      var BN = require("hmac-drbg");
      var chai = require("../utils");
      var url = require("../curves");
      var generate = require("brorand");
      var assert = chai.assert;
      var KeyPair = require("./key");
      var Buffer = require("./signature");
      /**
       * @param {?} options
       * @return {?}
       */
      (module.exports = test).prototype.keyPair = function(options) {
        return new KeyPair(this, options);
      };
      /**
       * @param {string} priv
       * @param {string} enc
       * @return {?}
       */
      test.prototype.keyFromPrivate = function(priv, enc) {
        return KeyPair.fromPrivate(this, priv, enc);
      };
      /**
       * @param {string} pub
       * @param {string} enc
       * @return {?}
       */
      test.prototype.keyFromPublic = function(pub, enc) {
        return KeyPair.fromPublic(this, pub, enc);
      };
      /**
       * @param {!Object} options
       * @return {?}
       */
      test.prototype.genKeyPair = function(options) {
        options = options || {};
        var c = new BN({
          hash : this.hash,
          pers : options.pers,
          persEnc : options.persEnc || "utf8",
          entropy : options.entropy || generate(this.hash.hmacStrength),
          entropyEnc : options.entropy && options.entropyEnc || "utf8",
          nonce : this.n.toArray()
        });
        var len = this.n.byteLength();
        var r = this.n.sub(new bn(2));
        for (;;) {
          var priv = new bn(c.generate(len));
          if (!(0 < priv.cmp(r))) {
            return priv.iaddn(1), this.keyFromPrivate(priv);
          }
        }
      };
      /**
       * @param {!Object} msg
       * @param {boolean} isFromBackend
       * @return {?}
       */
      test.prototype._truncateToN = function(msg, isFromBackend) {
        /** @type {number} */
        var delta = 8 * msg.byteLength() - this.n.bitLength();
        return 0 < delta && (msg = msg.ushrn(delta)), !isFromBackend && 0 <= msg.cmp(this.n) ? msg.sub(this.n) : msg;
      };
      /**
       * @param {!Object} msg
       * @param {string} key
       * @param {string} enc
       * @param {!Object} options
       * @return {?}
       */
      test.prototype.sign = function(msg, key, enc, options) {
        if ("object" == typeof enc) {
          /** @type {string} */
          options = enc;
          /** @type {null} */
          enc = null;
        }
        options = options || {};
        key = this.keyFromPrivate(key, enc);
        msg = this._truncateToN(new bn(msg, 16));
        var bytes = this.n.byteLength();
        enc = key.getPrivate().toArray("be", bytes);
        bytes = msg.toArray("be", bytes);
        var r = new BN({
          hash : this.hash,
          entropy : enc,
          nonce : bytes,
          pers : options.pers,
          persEnc : options.persEnc || "utf8"
        });
        var one = this.n.sub(new bn(1));
        /** @type {number} */
        var iter = 0;
        for (;; iter++) {
          var s = options.k ? options.k(iter) : new bn(r.generate(this.n.byteLength()));
          if (!((s = this._truncateToN(s, true)).cmpn(1) <= 0 || 0 <= s.cmp(one))) {
            var p = this.g.mul(s);
            if (!p.isInfinity()) {
              var x = p.getX();
              var r = x.umod(this.n);
              if (0 !== r.cmpn(0)) {
                s = s.invm(this.n).mul(r.mul(key.getPrivate()).iadd(msg));
                if (0 !== (s = s.umod(this.n)).cmpn(0)) {
                  /** @type {number} */
                  x = (p.getY().isOdd() ? 1 : 0) | (0 !== x.cmp(r) ? 2 : 0);
                  return options.canonical && 0 < s.cmp(this.nh) && (s = this.n.sub(s), x = x ^ 1), new Buffer({
                    r : r,
                    s : s,
                    recoveryParam : x
                  });
                }
              }
            }
          }
        }
      };
      /**
       * @param {string} msg
       * @param {string} s
       * @param {string} key
       * @param {string} r
       * @return {?}
       */
      test.prototype.verify = function(msg, s, key, r) {
        msg = this._truncateToN(new bn(msg, 16));
        key = this.keyFromPublic(key, r);
        r = (s = new Buffer(s, "hex")).r;
        s = s.s;
        if (r.cmpn(1) < 0 || 0 <= r.cmp(this.n)) {
          return false;
        }
        if (s.cmpn(1) < 0 || 0 <= s.cmp(this.n)) {
          return false;
        }
        var f;
        s = s.invm(this.n);
        msg = s.mul(msg).umod(this.n);
        s = s.mul(r).umod(this.n);
        return this.curve._maxwellTrick ? !(f = this.g.jmulAdd(msg, key.getPublic(), s)).isInfinity() && f.eqXToP(r) : !(f = this.g.mulAdd(msg, key.getPublic(), s)).isInfinity() && 0 === f.getX().umod(this.n).cmp(r);
      };
      /**
       * @param {?} name
       * @param {!Object} msg
       * @param {number} value
       * @param {string} to
       * @return {?}
       */
      test.prototype.recoverPubKey = function(name, msg, value, to) {
        assert((3 & value) === value, "The recovery param is more than two bits");
        msg = new Buffer(msg, to);
        var n = this.n;
        var e = new bn(name);
        var r = msg.r;
        to = msg.s;
        /** @type {number} */
        name = 1 & value;
        /** @type {number} */
        value = value >> 1;
        if (0 <= r.cmp(this.curve.p.umod(this.curve.n)) && value) {
          throw new Error("Unable to find sencond key candinate");
        }
        r = value ? this.curve.pointFromX(r.add(this.curve.n), name) : this.curve.pointFromX(r, name);
        msg = msg.r.invm(n);
        e = n.sub(e).mul(msg).umod(n);
        n = to.mul(msg).umod(n);
        return this.g.mulAdd(e, r, n);
      };
      /**
       * @param {?} e
       * @param {!Object} signature
       * @param {!Object} msg
       * @param {string} enc
       * @return {?}
       */
      test.prototype.getKeyRecoveryParam = function(e, signature, msg, enc) {
        if (null !== (signature = new Buffer(signature, enc)).recoveryParam) {
          return signature.recoveryParam;
        }
        var result;
        /** @type {number} */
        var i = 0;
        for (; i < 4; i++) {
          try {
            result = this.recoverPubKey(e, signature, i);
          } catch (e) {
            continue;
          }
          if (result.eq(msg)) {
            return i;
          }
        }
        throw new Error("Unable to find valid recovery factor");
      };
    }, {
      "../curves" : 7,
      "../utils" : 15,
      "./key" : 9,
      "./signature" : 10,
      "bn.js" : 16,
      brorand : 17,
      "hmac-drbg" : 31
    }],
    9 : [function(require, context, i) {
      /**
       * @param {!Node} max
       * @param {!Object} options
       * @return {undefined}
       */
      function Buffer(max, options) {
        /** @type {!Node} */
        this.ec = max;
        /** @type {null} */
        this.priv = null;
        /** @type {null} */
        this.pub = null;
        if (options.priv) {
          this._importPrivate(options.priv, options.privEnc);
        }
        if (options.pub) {
          this._importPublic(options.pub, options.pubEnc);
        }
      }
      var BN = require("bn.js");
      var isArray = require("../utils").assert;
      /**
       * @param {string} value
       * @param {string} key
       * @param {string} enc
       * @return {?}
       */
      (context.exports = Buffer).fromPublic = function(value, key, enc) {
        return key instanceof Buffer ? key : new Buffer(value, {
          pub : key,
          pubEnc : enc
        });
      };
      /**
       * @param {string} key
       * @param {string} data
       * @param {string} enc
       * @return {?}
       */
      Buffer.fromPrivate = function(key, data, enc) {
        return data instanceof Buffer ? data : new Buffer(key, {
          priv : data,
          privEnc : enc
        });
      };
      /**
       * @return {?}
       */
      Buffer.prototype.validate = function() {
        var pub = this.getPublic();
        return pub.isInfinity() ? {
          result : false,
          reason : "Invalid public key"
        } : pub.validate() ? pub.mul(this.ec.curve.n).isInfinity() ? {
          result : true,
          reason : null
        } : {
          result : false,
          reason : "Public key * N != O"
        } : {
          result : false,
          reason : "Public key is not a point"
        };
      };
      /**
       * @param {string} callback
       * @param {string} name
       * @return {?}
       */
      Buffer.prototype.getPublic = function(callback, name) {
        return "string" == typeof callback && (name = callback, callback = null), this.pub || (this.pub = this.ec.g.mul(this.priv)), name ? this.pub.encode(name, callback) : this.pub;
      };
      /**
       * @param {string} data
       * @return {?}
       */
      Buffer.prototype.getPrivate = function(data) {
        return "hex" === data ? this.priv.toString(16, 2) : this.priv;
      };
      /**
       * @param {number} key
       * @param {number} enc
       * @return {undefined}
       */
      Buffer.prototype._importPrivate = function(key, enc) {
        this.priv = new BN(key, enc || 16);
        this.priv = this.priv.umod(this.ec.curve.n);
      };
      /**
       * @param {!Object} key
       * @param {undefined} enc
       * @return {?}
       */
      Buffer.prototype._importPublic = function(key, enc) {
        if (key.x || key.y) {
          return "mont" === this.ec.curve.type ? isArray(key.x, "Need x coordinate") : "short" !== this.ec.curve.type && "edwards" !== this.ec.curve.type || isArray(key.x && key.y, "Need both x and y coordinate"), void(this.pub = this.ec.curve.point(key.x, key.y));
        }
        this.pub = this.ec.curve.decodePoint(key, enc);
      };
      /**
       * @param {!Object} pub
       * @return {?}
       */
      Buffer.prototype.derive = function(pub) {
        return pub.validate() || isArray(pub.validate(), "public point not validated"), pub.mul(this.priv).getX();
      };
      /**
       * @param {!Object} msg
       * @param {undefined} enc
       * @param {!Object} options
       * @return {?}
       */
      Buffer.prototype.sign = function(msg, enc, options) {
        return this.ec.sign(msg, this, enc, options);
      };
      /**
       * @param {string} msg
       * @param {string} signature
       * @return {?}
       */
      Buffer.prototype.verify = function(msg, signature) {
        return this.ec.verify(msg, signature, this);
      };
      /**
       * @return {?}
       */
      Buffer.prototype.inspect = function() {
        return "<Key priv: " + (this.priv && this.priv.toString(16, 2)) + " pub: " + (this.pub && this.pub.inspect()) + " >";
      };
    }, {
      "../utils" : 15,
      "bn.js" : 16
    }],
    10 : [function(__webpack_require__, module, i) {
      /**
       * @param {!Object} options
       * @param {boolean} s
       * @return {?}
       */
      function test(options, s) {
        if (options instanceof test) {
          return options;
        }
        if (!this._importDER(options, s)) {
          assert(options.r && options.s, "Signature without r or s");
          this.r = new Uint8Array(options.r, 16);
          this.s = new Uint8Array(options.s, 16);
          if (void 0 === options.recoveryParam) {
            /** @type {null} */
            this.recoveryParam = null;
          } else {
            this.recoveryParam = options.recoveryParam;
          }
        }
      }
      /**
       * @return {undefined}
       */
      function bn() {
        /** @type {number} */
        this.place = 0;
      }
      /**
       * @param {string} data
       * @param {!Object} p
       * @return {?}
       */
      function next(data, p) {
        var total = data[p.place++];
        if (!(128 & total)) {
          return total;
        }
        /** @type {number} */
        var clientHeight = 15 & total;
        if (0 == clientHeight || 4 < clientHeight) {
          return false;
        }
        /** @type {number} */
        var crc = 0;
        /** @type {number} */
        var targetOffsetHeight = 0;
        var off = p.place;
        for (; targetOffsetHeight < clientHeight; targetOffsetHeight++, off++) {
          /** @type {number} */
          crc = crc << 8;
          /** @type {number} */
          crc = crc | data[off];
          /** @type {number} */
          crc = crc >>> 0;
        }
        return !(crc <= 127) && (p.place = off, crc);
      }
      /**
       * @param {string} x
       * @return {?}
       */
      function toString(x) {
        /** @type {number} */
        var j = 0;
        /** @type {number} */
        var rown = x.length - 1;
        for (; !x[j] && !(128 & x[j + 1]) && j < rown;) {
          j++;
        }
        return 0 === j ? x : x.slice(j);
      }
      /**
       * @param {!Array} ctx
       * @param {number} value
       * @return {undefined}
       */
      function debug(ctx, value) {
        if (value < 128) {
          ctx.push(value);
        } else {
          /** @type {number} */
          var odd = 1 + (Math.log(value) / Math.LN2 >>> 3);
          ctx.push(128 | odd);
          for (; --odd;) {
            ctx.push(value >>> (odd << 3) & 255);
          }
          ctx.push(value);
        }
      }
      var Uint8Array = __webpack_require__("bn.js");
      var utils = __webpack_require__("../utils");
      var assert = utils.assert;
      /**
       * @param {string} data
       * @param {boolean} i
       * @return {?}
       */
      (module.exports = test).prototype._importDER = function(data, i) {
        data = utils.toArray(data, i);
        var p = new bn;
        if (48 !== data[p.place++]) {
          return false;
        }
        var len = next(data, p);
        if (false === len) {
          return false;
        }
        if (len + p.place !== data.length) {
          return false;
        }
        if (2 !== data[p.place++]) {
          return false;
        }
        i = next(data, p);
        if (false === i) {
          return false;
        }
        len = data.slice(p.place, i + p.place);
        if (p.place += i, 2 !== data[p.place++]) {
          return false;
        }
        i = next(data, p);
        if (false === i) {
          return false;
        }
        if (data.length !== i + p.place) {
          return false;
        }
        p = data.slice(p.place, i + p.place);
        if (0 === len[0]) {
          if (!(128 & len[1])) {
            return false;
          }
          len = len.slice(1);
        }
        if (0 === p[0]) {
          if (!(128 & p[1])) {
            return false;
          }
          p = p.slice(1);
        }
        return this.r = new Uint8Array(len), this.s = new Uint8Array(p), !(this.recoveryParam = null);
      };
      /**
       * @param {string} s
       * @return {?}
       */
      test.prototype.toDER = function(s) {
        var b = this.r.toArray();
        var node = this.s.toArray();
        if (128 & b[0]) {
          /** @type {!Array<?>} */
          b = [0].concat(b);
        }
        if (128 & node[0]) {
          /** @type {!Array<?>} */
          node = [0].concat(node);
        }
        b = toString(b);
        node = toString(node);
        for (; !(node[0] || 128 & node[1]);) {
          node = node.slice(1);
        }
        /** @type {!Array} */
        var a = [2];
        debug(a, b.length);
        (a = a.concat(b)).push(2);
        debug(a, node.length);
        /** @type {!Array<?>} */
        b = a.concat(node);
        /** @type {!Array} */
        a = [48];
        return debug(a, b.length), a = a.concat(b), utils.encode(a, s);
      };
    }, {
      "../utils" : 15,
      "bn.js" : 16
    }],
    11 : [function(require, module, i) {
      /**
       * @param {!Object} curve
       * @return {?}
       */
      function sign(curve) {
        if (assert("ed25519" === curve, "only tested with ed25519 so far"), !(this instanceof sign)) {
          return new sign(curve);
        }
        curve = Easing[curve].curve;
        /** @type {!Object} */
        this.curve = curve;
        this.g = curve.g;
        this.g.precompute(curve.n.bitLength() + 1);
        this.pointClass = curve.point().constructor;
        /** @type {number} */
        this.encodingLength = Math.ceil(curve.n.bitLength() / 8);
        this.hash = configs.sha512;
      }
      var configs = require("hash.js");
      var Easing = require("../curves");
      var $ = require("../utils");
      var assert = $.assert;
      var f = $.parseBytes;
      var model = require("./key");
      var Point = require("./signature");
      /**
       * @param {!Object} message
       * @param {string} secret
       * @return {?}
       */
      (module.exports = sign).prototype.sign = function(message, secret) {
        message = f(message);
        var key = this.keyFromSecret(secret);
        var r = this.hashInt(key.messagePrefix(), message);
        var R = this.g.mul(r);
        secret = this.encodePoint(R);
        key = this.hashInt(secret, key.pubBytes(), message).mul(key.priv());
        key = r.add(key).umod(this.curve.n);
        return this.makeSignature({
          R : R,
          S : key,
          Rencoded : secret
        });
      };
      /**
       * @param {!Object} obj
       * @param {!Object} sig
       * @param {boolean} key
       * @return {?}
       */
      sign.prototype.verify = function(obj, sig, key) {
        obj = f(obj);
        sig = this.makeSignature(sig);
        var pair = this.keyFromPublic(key);
        key = this.hashInt(sig.Rencoded(), pair.pubBytes(), obj);
        obj = this.g.mul(sig.S());
        return sig.R().add(pair.pub().mul(key)).eq(obj);
      };
      /**
       * @return {?}
       */
      sign.prototype.hashInt = function() {
        var hash = this.hash();
        /** @type {number} */
        var i = 0;
        for (; i < arguments.length; i++) {
          hash.update(arguments[i]);
        }
        return $.intFromLE(hash.digest()).umod(this.curve.n);
      };
      /**
       * @param {string} pub
       * @return {?}
       */
      sign.prototype.keyFromPublic = function(pub) {
        return model.fromPublic(this, pub);
      };
      /**
       * @param {string} secret
       * @return {?}
       */
      sign.prototype.keyFromSecret = function(secret) {
        return model.fromSecret(this, secret);
      };
      /**
       * @param {!Function} s
       * @return {?}
       */
      sign.prototype.makeSignature = function(s) {
        return s instanceof Point ? s : new Point(this, s);
      };
      /**
       * @param {!Object} point
       * @return {?}
       */
      sign.prototype.encodePoint = function(point) {
        var enc = point.getY().toArray("le", this.encodingLength);
        return enc[this.encodingLength - 1] |= point.getX().isOdd() ? 128 : 0, enc;
      };
      /**
       * @param {!Object} bytes
       * @return {?}
       */
      sign.prototype.decodePoint = function(bytes) {
        /** @type {number} */
        var i = (bytes = $.parseBytes(bytes)).length - 1;
        var y = bytes.slice(0, i).concat(-129 & bytes[i]);
        /** @type {boolean} */
        i = 0 != (128 & bytes[i]);
        y = $.intFromLE(y);
        return this.curve.pointFromY(y, i);
      };
      /**
       * @param {?} num
       * @return {?}
       */
      sign.prototype.encodeInt = function(num) {
        return num.toArray("le", this.encodingLength);
      };
      /**
       * @param {string} buffer
       * @return {?}
       */
      sign.prototype.decodeInt = function(buffer) {
        return $.intFromLE(buffer);
      };
      /**
       * @param {?} val
       * @return {?}
       */
      sign.prototype.isPoint = function(val) {
        return val instanceof this.pointClass;
      };
    }, {
      "../curves" : 7,
      "../utils" : 15,
      "./key" : 12,
      "./signature" : 13,
      "hash.js" : 19
    }],
    12 : [function(callback, module, i) {
      /**
       * @param {!Object} eddsa
       * @param {!Object} params
       * @return {undefined}
       */
      function User(eddsa, params) {
        /** @type {!Object} */
        this.eddsa = eddsa;
        this._secret = prop(params.secret);
        if (eddsa.isPoint(params.pub)) {
          this._pub = params.pub;
        } else {
          this._pubBytes = prop(params.pub);
        }
      }
      var self = callback("../utils");
      var assert = self.assert;
      var prop = self.parseBytes;
      callback = self.cachedProperty;
      /**
       * @param {string} path
       * @param {string} key
       * @return {?}
       */
      User.fromPublic = function(path, key) {
        return key instanceof User ? key : new User(path, {
          pub : key
        });
      };
      /**
       * @param {string} data
       * @param {string} userData
       * @return {?}
       */
      User.fromSecret = function(data, userData) {
        return userData instanceof User ? userData : new User(data, {
          secret : userData
        });
      };
      /**
       * @return {?}
       */
      User.prototype.secret = function() {
        return this._secret;
      };
      callback(User, "pubBytes", function() {
        return this.eddsa.encodePoint(this.pub());
      });
      callback(User, "pub", function() {
        return this._pubBytes ? this.eddsa.decodePoint(this._pubBytes) : this.eddsa.g.mul(this.priv());
      });
      callback(User, "privBytes", function() {
        var eddsa = this.eddsa;
        var hash = this.hash();
        /** @type {number} */
        var i = eddsa.encodingLength - 1;
        eddsa = hash.slice(0, eddsa.encodingLength);
        return eddsa[0] &= 248, eddsa[i] &= 127, eddsa[i] |= 64, eddsa;
      });
      callback(User, "priv", function() {
        return this.eddsa.decodeInt(this.privBytes());
      });
      callback(User, "hash", function() {
        return this.eddsa.hash().update(this.secret()).digest();
      });
      callback(User, "messagePrefix", function() {
        return this.hash().slice(this.eddsa.encodingLength);
      });
      /**
       * @param {!Object} message
       * @return {?}
       */
      User.prototype.sign = function(message) {
        return assert(this._secret, "KeyPair can only verify"), this.eddsa.sign(message, this);
      };
      /**
       * @param {string} message
       * @param {string} sig
       * @return {?}
       */
      User.prototype.verify = function(message, sig) {
        return this.eddsa.verify(message, sig, this);
      };
      /**
       * @param {string} data
       * @return {?}
       */
      User.prototype.getSecret = function(data) {
        return assert(this._secret, "KeyPair is public only"), self.encode(this.secret(), data);
      };
      /**
       * @param {string} data
       * @return {?}
       */
      User.prototype.getPublic = function(data) {
        return self.encode(this.pubBytes(), data);
      };
      /** @type {function(!Object, !Object): undefined} */
      module.exports = User;
    }, {
      "../utils" : 15
    }],
    13 : [function(jQuery, module, i) {
      /**
       * @param {!Object} eddsa
       * @param {!Object} sig
       * @return {undefined}
       */
      function Color(eddsa, sig) {
        /** @type {!Object} */
        this.eddsa = eddsa;
        if ("object" != typeof sig) {
          sig = parseInt(sig);
        }
        if (Array.isArray(sig)) {
          sig = {
            R : sig.slice(0, eddsa.encodingLength),
            S : sig.slice(eddsa.encodingLength)
          };
        }
        test(sig.R && sig.S, "Signature without R or S");
        if (eddsa.isPoint(sig.R)) {
          this._R = sig.R;
        }
        if (sig.S instanceof subsetQuery) {
          this._S = sig.S;
        }
        this._Rencoded = Array.isArray(sig.R) ? sig.R : sig.Rencoded;
        this._Sencoded = Array.isArray(sig.S) ? sig.S : sig.Sencoded;
      }
      var subsetQuery = jQuery("bn.js");
      var self = jQuery("../utils");
      var test = self.assert;
      jQuery = self.cachedProperty;
      var parseInt = self.parseBytes;
      jQuery(Color, "S", function() {
        return this.eddsa.decodeInt(this.Sencoded());
      });
      jQuery(Color, "R", function() {
        return this.eddsa.decodePoint(this.Rencoded());
      });
      jQuery(Color, "Rencoded", function() {
        return this.eddsa.encodePoint(this.R());
      });
      jQuery(Color, "Sencoded", function() {
        return this.eddsa.encodeInt(this.S());
      });
      /**
       * @return {?}
       */
      Color.prototype.toBytes = function() {
        return this.Rencoded().concat(this.Sencoded());
      };
      /**
       * @return {?}
       */
      Color.prototype.toHex = function() {
        return self.encode(this.toBytes(), "hex").toUpperCase();
      };
      /** @type {function(!Object, !Object): undefined} */
      module.exports = Color;
    }, {
      "../utils" : 15,
      "bn.js" : 16
    }],
    14 : [function(canCreateDiscussions, mixin, i) {
      mixin.exports = {
        doubles : {
          step : 4,
          points : [["e60fce93b59e9ec53011aabc21c23e97b2a31369b87a5ae9c44ee89e2a6dec0a", "f7e3507399e595929db99f34f57937101296891e44d23f0be1f32cce69616821"], ["8282263212c609d9ea2a6e3e172de238d8c39cabd5ac1ca10646e23fd5f51508", "11f8a8098557dfe45e8256e830b60ace62d613ac2f7b17bed31b6eaff6e26caf"], ["175e159f728b865a72f99cc6c6fc846de0b93833fd2222ed73fce5b551e5b739", "d3506e0d9e3c79eba4ef97a51ff71f5eacb5955add24345c6efa6ffee9fed695"], ["363d90d447b00c9c99ceac05b6262ee053441c7e55552ffe526bad8f83ff4640", 
          "4e273adfc732221953b445397f3363145b9a89008199ecb62003c7f3bee9de9"], ["8b4b5f165df3c2be8c6244b5b745638843e4a781a15bcd1b69f79a55dffdf80c", "4aad0a6f68d308b4b3fbd7813ab0da04f9e336546162ee56b3eff0c65fd4fd36"], ["723cbaa6e5db996d6bf771c00bd548c7b700dbffa6c0e77bcb6115925232fcda", "96e867b5595cc498a921137488824d6e2660a0653779494801dc069d9eb39f5f"], ["eebfa4d493bebf98ba5feec812c2d3b50947961237a919839a533eca0e7dd7fa", "5d9a8ca3970ef0f269ee7edaf178089d9ae4cdc3a711f712ddfd4fdae1de8999"], ["100f44da696e71672791d0a09b7bde459f1215a29b3c03bfefd7835b39a48db0", 
          "cdd9e13192a00b772ec8f3300c090666b7ff4a18ff5195ac0fbd5cd62bc65a09"], ["e1031be262c7ed1b1dc9227a4a04c017a77f8d4464f3b3852c8acde6e534fd2d", "9d7061928940405e6bb6a4176597535af292dd419e1ced79a44f18f29456a00d"], ["feea6cae46d55b530ac2839f143bd7ec5cf8b266a41d6af52d5e688d9094696d", "e57c6b6c97dce1bab06e4e12bf3ecd5c981c8957cc41442d3155debf18090088"], ["da67a91d91049cdcb367be4be6ffca3cfeed657d808583de33fa978bc1ec6cb1", "9bacaa35481642bc41f463f7ec9780e5dec7adc508f740a17e9ea8e27a68be1d"], ["53904faa0b334cdda6e000935ef22151ec08d0f7bb11069f57545ccc1a37b7c0", 
          "5bc087d0bc80106d88c9eccac20d3c1c13999981e14434699dcb096b022771c8"], ["8e7bcd0bd35983a7719cca7764ca906779b53a043a9b8bcaeff959f43ad86047", "10b7770b2a3da4b3940310420ca9514579e88e2e47fd68b3ea10047e8460372a"], ["385eed34c1cdff21e6d0818689b81bde71a7f4f18397e6690a841e1599c43862", "283bebc3e8ea23f56701de19e9ebf4576b304eec2086dc8cc0458fe5542e5453"], ["6f9d9b803ecf191637c73a4413dfa180fddf84a5947fbc9c606ed86c3fac3a7", "7c80c68e603059ba69b8e2a30e45c4d47ea4dd2f5c281002d86890603a842160"], ["3322d401243c4e2582a2147c104d6ecbf774d163db0f5e5313b7e0e742d0e6bd", 
          "56e70797e9664ef5bfb019bc4ddaf9b72805f63ea2873af624f3a2e96c28b2a0"], ["85672c7d2de0b7da2bd1770d89665868741b3f9af7643397721d74d28134ab83", "7c481b9b5b43b2eb6374049bfa62c2e5e77f17fcc5298f44c8e3094f790313a6"], ["948bf809b1988a46b06c9f1919413b10f9226c60f668832ffd959af60c82a0a", "53a562856dcb6646dc6b74c5d1c3418c6d4dff08c97cd2bed4cb7f88d8c8e589"], ["6260ce7f461801c34f067ce0f02873a8f1b0e44dfc69752accecd819f38fd8e8", "bc2da82b6fa5b571a7f09049776a1ef7ecd292238051c198c1a84e95b2b4ae17"], ["e5037de0afc1d8d43d8348414bbf4103043ec8f575bfdc432953cc8d2037fa2d", 
          "4571534baa94d3b5f9f98d09fb990bddbd5f5b03ec481f10e0e5dc841d755bda"], ["e06372b0f4a207adf5ea905e8f1771b4e7e8dbd1c6a6c5b725866a0ae4fce725", "7a908974bce18cfe12a27bb2ad5a488cd7484a7787104870b27034f94eee31dd"], ["213c7a715cd5d45358d0bbf9dc0ce02204b10bdde2a3f58540ad6908d0559754", "4b6dad0b5ae462507013ad06245ba190bb4850f5f36a7eeddff2c27534b458f2"], ["4e7c272a7af4b34e8dbb9352a5419a87e2838c70adc62cddf0cc3a3b08fbd53c", "17749c766c9d0b18e16fd09f6def681b530b9614bff7dd33e0b3941817dcaae6"], ["fea74e3dbe778b1b10f238ad61686aa5c76e3db2be43057632427e2840fb27b6", 
          "6e0568db9b0b13297cf674deccb6af93126b596b973f7b77701d3db7f23cb96f"], ["76e64113f677cf0e10a2570d599968d31544e179b760432952c02a4417bdde39", "c90ddf8dee4e95cf577066d70681f0d35e2a33d2b56d2032b4b1752d1901ac01"], ["c738c56b03b2abe1e8281baa743f8f9a8f7cc643df26cbee3ab150242bcbb891", "893fb578951ad2537f718f2eacbfbbbb82314eef7880cfe917e735d9699a84c3"], ["d895626548b65b81e264c7637c972877d1d72e5f3a925014372e9f6588f6c14b", "febfaa38f2bc7eae728ec60818c340eb03428d632bb067e179363ed75d7d991f"], ["b8da94032a957518eb0f6433571e8761ceffc73693e84edd49150a564f676e03", 
          "2804dfa44805a1e4d7c99cc9762808b092cc584d95ff3b511488e4e74efdf6e7"], ["e80fea14441fb33a7d8adab9475d7fab2019effb5156a792f1a11778e3c0df5d", "eed1de7f638e00771e89768ca3ca94472d155e80af322ea9fcb4291b6ac9ec78"], ["a301697bdfcd704313ba48e51d567543f2a182031efd6915ddc07bbcc4e16070", "7370f91cfb67e4f5081809fa25d40f9b1735dbf7c0a11a130c0d1a041e177ea1"], ["90ad85b389d6b936463f9d0512678de208cc330b11307fffab7ac63e3fb04ed4", "e507a3620a38261affdcbd9427222b839aefabe1582894d991d4d48cb6ef150"], ["8f68b9d2f63b5f339239c1ad981f162ee88c5678723ea3351b7b444c9ec4c0da", 
          "662a9f2dba063986de1d90c2b6be215dbbea2cfe95510bfdf23cbf79501fff82"], ["e4f3fb0176af85d65ff99ff9198c36091f48e86503681e3e6686fd5053231e11", "1e63633ad0ef4f1c1661a6d0ea02b7286cc7e74ec951d1c9822c38576feb73bc"], ["8c00fa9b18ebf331eb961537a45a4266c7034f2f0d4e1d0716fb6eae20eae29e", "efa47267fea521a1a9dc343a3736c974c2fadafa81e36c54e7d2a4c66702414b"], ["e7a26ce69dd4829f3e10cec0a9e98ed3143d084f308b92c0997fddfc60cb3e41", "2a758e300fa7984b471b006a1aafbb18d0a6b2c0420e83e20e8a9421cf2cfd51"], ["b6459e0ee3662ec8d23540c223bcbdc571cbcb967d79424f3cf29eb3de6b80ef", 
          "67c876d06f3e06de1dadf16e5661db3c4b3ae6d48e35b2ff30bf0b61a71ba45"], ["d68a80c8280bb840793234aa118f06231d6f1fc67e73c5a5deda0f5b496943e8", "db8ba9fff4b586d00c4b1f9177b0e28b5b0e7b8f7845295a294c84266b133120"], ["324aed7df65c804252dc0270907a30b09612aeb973449cea4095980fc28d3d5d", "648a365774b61f2ff130c0c35aec1f4f19213b0c7e332843967224af96ab7c84"], ["4df9c14919cde61f6d51dfdbe5fee5dceec4143ba8d1ca888e8bd373fd054c96", "35ec51092d8728050974c23a1d85d4b5d506cdc288490192ebac06cad10d5d"], ["9c3919a84a474870faed8a9c1cc66021523489054d7f0308cbfc99c8ac1f98cd", 
          "ddb84f0f4a4ddd57584f044bf260e641905326f76c64c8e6be7e5e03d4fc599d"], ["6057170b1dd12fdf8de05f281d8e06bb91e1493a8b91d4cc5a21382120a959e5", "9a1af0b26a6a4807add9a2daf71df262465152bc3ee24c65e899be932385a2a8"], ["a576df8e23a08411421439a4518da31880cef0fba7d4df12b1a6973eecb94266", "40a6bf20e76640b2c92b97afe58cd82c432e10a7f514d9f3ee8be11ae1b28ec8"], ["7778a78c28dec3e30a05fe9629de8c38bb30d1f5cf9a3a208f763889be58ad71", "34626d9ab5a5b22ff7098e12f2ff580087b38411ff24ac563b513fc1fd9f43ac"], ["928955ee637a84463729fd30e7afd2ed5f96274e5ad7e5cb09eda9c06d903ac", 
          "c25621003d3f42a827b78a13093a95eeac3d26efa8a8d83fc5180e935bcd091f"], ["85d0fef3ec6db109399064f3a0e3b2855645b4a907ad354527aae75163d82751", "1f03648413a38c0be29d496e582cf5663e8751e96877331582c237a24eb1f962"], ["ff2b0dce97eece97c1c9b6041798b85dfdfb6d8882da20308f5404824526087e", "493d13fef524ba188af4c4dc54d07936c7b7ed6fb90e2ceb2c951e01f0c29907"], ["827fbbe4b1e880ea9ed2b2e6301b212b57f1ee148cd6dd28780e5e2cf856e241", "c60f9c923c727b0b71bef2c67d1d12687ff7a63186903166d605b68baec293ec"], ["eaa649f21f51bdbae7be4ae34ce6e5217a58fdce7f47f9aa7f3b58fa2120e2b3", 
          "be3279ed5bbbb03ac69a80f89879aa5a01a6b965f13f7e59d47a5305ba5ad93d"], ["e4a42d43c5cf169d9391df6decf42ee541b6d8f0c9a137401e23632dda34d24f", "4d9f92e716d1c73526fc99ccfb8ad34ce886eedfa8d8e4f13a7f7131deba9414"], ["1ec80fef360cbdd954160fadab352b6b92b53576a88fea4947173b9d4300bf19", "aeefe93756b5340d2f3a4958a7abbf5e0146e77f6295a07b671cdc1cc107cefd"], ["146a778c04670c2f91b00af4680dfa8bce3490717d58ba889ddb5928366642be", "b318e0ec3354028add669827f9d4b2870aaa971d2f7e5ed1d0b297483d83efd0"], ["fa50c0f61d22e5f07e3acebb1aa07b128d0012209a28b9776d76a8793180eef9", 
          "6b84c6922397eba9b72cd2872281a68a5e683293a57a213b38cd8d7d3f4f2811"], ["da1d61d0ca721a11b1a5bf6b7d88e8421a288ab5d5bba5220e53d32b5f067ec2", "8157f55a7c99306c79c0766161c91e2966a73899d279b48a655fba0f1ad836f1"], ["a8e282ff0c9706907215ff98e8fd416615311de0446f1e062a73b0610d064e13", "7f97355b8db81c09abfb7f3c5b2515888b679a3e50dd6bd6cef7c73111f4cc0c"], ["174a53b9c9a285872d39e56e6913cab15d59b1fa512508c022f382de8319497c", "ccc9dc37abfc9c1657b4155f2c47f9e6646b3a1d8cb9854383da13ac079afa73"], ["959396981943785c3d3e57edf5018cdbe039e730e4918b3d884fdff09475b7ba", 
          "2e7e552888c331dd8ba0386a4b9cd6849c653f64c8709385e9b8abf87524f2fd"], ["d2a63a50ae401e56d645a1153b109a8fcca0a43d561fba2dbb51340c9d82b151", "e82d86fb6443fcb7565aee58b2948220a70f750af484ca52d4142174dcf89405"], ["64587e2335471eb890ee7896d7cfdc866bacbdbd3839317b3436f9b45617e073", "d99fcdd5bf6902e2ae96dd6447c299a185b90a39133aeab358299e5e9faf6589"], ["8481bde0e4e4d885b3a546d3e549de042f0aa6cea250e7fd358d6c86dd45e458", "38ee7b8cba5404dd84a25bf39cecb2ca900a79c42b262e556d64b1b59779057e"], ["13464a57a78102aa62b6979ae817f4637ffcfed3c4b1ce30bcd6303f6caf666b", 
          "69be159004614580ef7e433453ccb0ca48f300a81d0942e13f495a907f6ecc27"], ["bc4a9df5b713fe2e9aef430bcc1dc97a0cd9ccede2f28588cada3a0d2d83f366", "d3a81ca6e785c06383937adf4b798caa6e8a9fbfa547b16d758d666581f33c1"], ["8c28a97bf8298bc0d23d8c749452a32e694b65e30a9472a3954ab30fe5324caa", "40a30463a3305193378fedf31f7cc0eb7ae784f0451cb9459e71dc73cbef9482"], ["8ea9666139527a8c1dd94ce4f071fd23c8b350c5a4bb33748c4ba111faccae0", "620efabbc8ee2782e24e7c0cfb95c5d735b783be9cf0f8e955af34a30e62b945"], ["dd3625faef5ba06074669716bbd3788d89bdde815959968092f76cc4eb9a9787", 
          "7a188fa3520e30d461da2501045731ca941461982883395937f68d00c644a573"], ["f710d79d9eb962297e4f6232b40e8f7feb2bc63814614d692c12de752408221e", "ea98e67232d3b3295d3b535532115ccac8612c721851617526ae47a9c77bfc82"]]
        },
        naf : {
          wnd : 7,
          points : [["f9308a019258c31049344f85f89d5229b531c845836f99b08601f113bce036f9", "388f7b0f632de8140fe337e62a37f3566500a99934c2231b6cb9fd7584b8e672"], ["2f8bde4d1a07209355b4a7250a5c5128e88b84bddc619ab7cba8d569b240efe4", "d8ac222636e5e3d6d4dba9dda6c9c426f788271bab0d6840dca87d3aa6ac62d6"], ["5cbdf0646e5db4eaa398f365f2ea7a0e3d419b7e0330e39ce92bddedcac4f9bc", "6aebca40ba255960a3178d6d861a54dba813d0b813fde7b5a5082628087264da"], ["acd484e2f0c7f65309ad178a9f559abde09796974c57e714c35f110dfc27ccbe", 
          "cc338921b0a7d9fd64380971763b61e9add888a4375f8e0f05cc262ac64f9c37"], ["774ae7f858a9411e5ef4246b70c65aac5649980be5c17891bbec17895da008cb", "d984a032eb6b5e190243dd56d7b7b365372db1e2dff9d6a8301d74c9c953c61b"], ["f28773c2d975288bc7d1d205c3748651b075fbc6610e58cddeeddf8f19405aa8", "ab0902e8d880a89758212eb65cdaf473a1a06da521fa91f29b5cb52db03ed81"], ["d7924d4f7d43ea965a465ae3095ff41131e5946f3c85f79e44adbcf8e27e080e", "581e2872a86c72a683842ec228cc6defea40af2bd896d3a5c504dc9ff6a26b58"], ["defdea4cdb677750a420fee807eacf21eb9898ae79b9768766e4faa04a2d4a34", 
          "4211ab0694635168e997b0ead2a93daeced1f4a04a95c0f6cfb199f69e56eb77"], ["2b4ea0a797a443d293ef5cff444f4979f06acfebd7e86d277475656138385b6c", "85e89bc037945d93b343083b5a1c86131a01f60c50269763b570c854e5c09b7a"], ["352bbf4a4cdd12564f93fa332ce333301d9ad40271f8107181340aef25be59d5", "321eb4075348f534d59c18259dda3e1f4a1b3b2e71b1039c67bd3d8bcf81998c"], ["2fa2104d6b38d11b0230010559879124e42ab8dfeff5ff29dc9cdadd4ecacc3f", "2de1068295dd865b64569335bd5dd80181d70ecfc882648423ba76b532b7d67"], ["9248279b09b4d68dab21a9b066edda83263c3d84e09572e269ca0cd7f5453714", 
          "73016f7bf234aade5d1aa71bdea2b1ff3fc0de2a887912ffe54a32ce97cb3402"], ["daed4f2be3a8bf278e70132fb0beb7522f570e144bf615c07e996d443dee8729", "a69dce4a7d6c98e8d4a1aca87ef8d7003f83c230f3afa726ab40e52290be1c55"], ["c44d12c7065d812e8acf28d7cbb19f9011ecd9e9fdf281b0e6a3b5e87d22e7db", "2119a460ce326cdc76c45926c982fdac0e106e861edf61c5a039063f0e0e6482"], ["6a245bf6dc698504c89a20cfded60853152b695336c28063b61c65cbd269e6b4", "e022cf42c2bd4a708b3f5126f16a24ad8b33ba48d0423b6efd5e6348100d8a82"], ["1697ffa6fd9de627c077e3d2fe541084ce13300b0bec1146f95ae57f0d0bd6a5", 
          "b9c398f186806f5d27561506e4557433a2cf15009e498ae7adee9d63d01b2396"], ["605bdb019981718b986d0f07e834cb0d9deb8360ffb7f61df982345ef27a7479", "2972d2de4f8d20681a78d93ec96fe23c26bfae84fb14db43b01e1e9056b8c49"], ["62d14dab4150bf497402fdc45a215e10dcb01c354959b10cfe31c7e9d87ff33d", "80fc06bd8cc5b01098088a1950eed0db01aa132967ab472235f5642483b25eaf"], ["80c60ad0040f27dade5b4b06c408e56b2c50e9f56b9b8b425e555c2f86308b6f", "1c38303f1cc5c30f26e66bad7fe72f70a65eed4cbe7024eb1aa01f56430bd57a"], ["7a9375ad6167ad54aa74c6348cc54d344cc5dc9487d847049d5eabb0fa03c8fb", 
          "d0e3fa9eca8726909559e0d79269046bdc59ea10c70ce2b02d499ec224dc7f7"], ["d528ecd9b696b54c907a9ed045447a79bb408ec39b68df504bb51f459bc3ffc9", "eecf41253136e5f99966f21881fd656ebc4345405c520dbc063465b521409933"], ["49370a4b5f43412ea25f514e8ecdad05266115e4a7ecb1387231808f8b45963", "758f3f41afd6ed428b3081b0512fd62a54c3f3afbb5b6764b653052a12949c9a"], ["77f230936ee88cbbd73df930d64702ef881d811e0e1498e2f1c13eb1fc345d74", "958ef42a7886b6400a08266e9ba1b37896c95330d97077cbbe8eb3c7671c60d6"], ["f2dac991cc4ce4b9ea44887e5c7c0bce58c80074ab9d4dbaeb28531b7739f530", 
          "e0dedc9b3b2f8dad4da1f32dec2531df9eb5fbeb0598e4fd1a117dba703a3c37"], ["463b3d9f662621fb1b4be8fbbe2520125a216cdfc9dae3debcba4850c690d45b", "5ed430d78c296c3543114306dd8622d7c622e27c970a1de31cb377b01af7307e"], ["f16f804244e46e2a09232d4aff3b59976b98fac14328a2d1a32496b49998f247", "cedabd9b82203f7e13d206fcdf4e33d92a6c53c26e5cce26d6579962c4e31df6"], ["caf754272dc84563b0352b7a14311af55d245315ace27c65369e15f7151d41d1", "cb474660ef35f5f2a41b643fa5e460575f4fa9b7962232a5c32f908318a04476"], ["2600ca4b282cb986f85d0f1709979d8b44a09c07cb86d7c124497bc86f082120", 
          "4119b88753c15bd6a693b03fcddbb45d5ac6be74ab5f0ef44b0be9475a7e4b40"], ["7635ca72d7e8432c338ec53cd12220bc01c48685e24f7dc8c602a7746998e435", "91b649609489d613d1d5e590f78e6d74ecfc061d57048bad9e76f302c5b9c61"], ["754e3239f325570cdbbf4a87deee8a66b7f2b33479d468fbc1a50743bf56cc18", "673fb86e5bda30fb3cd0ed304ea49a023ee33d0197a695d0c5d98093c536683"], ["e3e6bd1071a1e96aff57859c82d570f0330800661d1c952f9fe2694691d9b9e8", "59c9e0bba394e76f40c0aa58379a3cb6a5a2283993e90c4167002af4920e37f5"], ["186b483d056a033826ae73d88f732985c4ccb1f32ba35f4b4cc47fdcf04aa6eb", 
          "3b952d32c67cf77e2e17446e204180ab21fb8090895138b4a4a797f86e80888b"], ["df9d70a6b9876ce544c98561f4be4f725442e6d2b737d9c91a8321724ce0963f", "55eb2dafd84d6ccd5f862b785dc39d4ab157222720ef9da217b8c45cf2ba2417"], ["5edd5cc23c51e87a497ca815d5dce0f8ab52554f849ed8995de64c5f34ce7143", "efae9c8dbc14130661e8cec030c89ad0c13c66c0d17a2905cdc706ab7399a868"], ["290798c2b6476830da12fe02287e9e777aa3fba1c355b17a722d362f84614fba", "e38da76dcd440621988d00bcf79af25d5b29c094db2a23146d003afd41943e7a"], ["af3c423a95d9f5b3054754efa150ac39cd29552fe360257362dfdecef4053b45", 
          "f98a3fd831eb2b749a93b0e6f35cfb40c8cd5aa667a15581bc2feded498fd9c6"], ["766dbb24d134e745cccaa28c99bf274906bb66b26dcf98df8d2fed50d884249a", "744b1152eacbe5e38dcc887980da38b897584a65fa06cedd2c924f97cbac5996"], ["59dbf46f8c94759ba21277c33784f41645f7b44f6c596a58ce92e666191abe3e", "c534ad44175fbc300f4ea6ce648309a042ce739a7919798cd85e216c4a307f6e"], ["f13ada95103c4537305e691e74e9a4a8dd647e711a95e73cb62dc6018cfd87b8", "e13817b44ee14de663bf4bc808341f326949e21a6a75c2570778419bdaf5733d"], ["7754b4fa0e8aced06d4167a2c59cca4cda1869c06ebadfb6488550015a88522c", 
          "30e93e864e669d82224b967c3020b8fa8d1e4e350b6cbcc537a48b57841163a2"], ["948dcadf5990e048aa3874d46abef9d701858f95de8041d2a6828c99e2262519", "e491a42537f6e597d5d28a3224b1bc25df9154efbd2ef1d2cbba2cae5347d57e"], ["7962414450c76c1689c7b48f8202ec37fb224cf5ac0bfa1570328a8a3d7c77ab", "100b610ec4ffb4760d5c1fc133ef6f6b12507a051f04ac5760afa5b29db83437"], ["3514087834964b54b15b160644d915485a16977225b8847bb0dd085137ec47ca", "ef0afbb2056205448e1652c48e8127fc6039e77c15c2378b7e7d15a0de293311"], ["d3cc30ad6b483e4bc79ce2c9dd8bc54993e947eb8df787b442943d3f7b527eaf", 
          "8b378a22d827278d89c5e9be8f9508ae3c2ad46290358630afb34db04eede0a4"], ["1624d84780732860ce1c78fcbfefe08b2b29823db913f6493975ba0ff4847610", "68651cf9b6da903e0914448c6cd9d4ca896878f5282be4c8cc06e2a404078575"], ["733ce80da955a8a26902c95633e62a985192474b5af207da6df7b4fd5fc61cd4", "f5435a2bd2badf7d485a4d8b8db9fcce3e1ef8e0201e4578c54673bc1dc5ea1d"], ["15d9441254945064cf1a1c33bbd3b49f8966c5092171e699ef258dfab81c045c", "d56eb30b69463e7234f5137b73b84177434800bacebfc685fc37bbe9efe4070d"], ["a1d0fcf2ec9de675b612136e5ce70d271c21417c9d2b8aaaac138599d0717940", 
          "edd77f50bcb5a3cab2e90737309667f2641462a54070f3d519212d39c197a629"], ["e22fbe15c0af8ccc5780c0735f84dbe9a790badee8245c06c7ca37331cb36980", "a855babad5cd60c88b430a69f53a1a7a38289154964799be43d06d77d31da06"], ["311091dd9860e8e20ee13473c1155f5f69635e394704eaa74009452246cfa9b3", "66db656f87d1f04fffd1f04788c06830871ec5a64feee685bd80f0b1286d8374"], ["34c1fd04d301be89b31c0442d3e6ac24883928b45a9340781867d4232ec2dbdf", "9414685e97b1b5954bd46f730174136d57f1ceeb487443dc5321857ba73abee"], ["f219ea5d6b54701c1c14de5b557eb42a8d13f3abbcd08affcc2a5e6b049b8d63", 
          "4cb95957e83d40b0f73af4544cccf6b1f4b08d3c07b27fb8d8c2962a400766d1"], ["d7b8740f74a8fbaab1f683db8f45de26543a5490bca627087236912469a0b448", "fa77968128d9c92ee1010f337ad4717eff15db5ed3c049b3411e0315eaa4593b"], ["32d31c222f8f6f0ef86f7c98d3a3335ead5bcd32abdd94289fe4d3091aa824bf", "5f3032f5892156e39ccd3d7915b9e1da2e6dac9e6f26e961118d14b8462e1661"], ["7461f371914ab32671045a155d9831ea8793d77cd59592c4340f86cbc18347b5", "8ec0ba238b96bec0cbdddcae0aa442542eee1ff50c986ea6b39847b3cc092ff6"], ["ee079adb1df1860074356a25aa38206a6d716b2c3e67453d287698bad7b2b2d6", 
          "8dc2412aafe3be5c4c5f37e0ecc5f9f6a446989af04c4e25ebaac479ec1c8c1e"], ["16ec93e447ec83f0467b18302ee620f7e65de331874c9dc72bfd8616ba9da6b5", "5e4631150e62fb40d0e8c2a7ca5804a39d58186a50e497139626778e25b0674d"], ["eaa5f980c245f6f038978290afa70b6bd8855897f98b6aa485b96065d537bd99", "f65f5d3e292c2e0819a528391c994624d784869d7e6ea67fb18041024edc07dc"], ["78c9407544ac132692ee1910a02439958ae04877151342ea96c4b6b35a49f51", "f3e0319169eb9b85d5404795539a5e68fa1fbd583c064d2462b675f194a3ddb4"], ["494f4be219a1a77016dcd838431aea0001cdc8ae7a6fc688726578d9702857a5", 
          "42242a969283a5f339ba7f075e36ba2af925ce30d767ed6e55f4b031880d562c"], ["a598a8030da6d86c6bc7f2f5144ea549d28211ea58faa70ebf4c1e665c1fe9b5", "204b5d6f84822c307e4b4a7140737aec23fc63b65b35f86a10026dbd2d864e6b"], ["c41916365abb2b5d09192f5f2dbeafec208f020f12570a184dbadc3e58595997", "4f14351d0087efa49d245b328984989d5caf9450f34bfc0ed16e96b58fa9913"], ["841d6063a586fa475a724604da03bc5b92a2e0d2e0a36acfe4c73a5514742881", "73867f59c0659e81904f9a1c7543698e62562d6744c169ce7a36de01a8d6154"], ["5e95bb399a6971d376026947f89bde2f282b33810928be4ded112ac4d70e20d5", 
          "39f23f366809085beebfc71181313775a99c9aed7d8ba38b161384c746012865"], ["36e4641a53948fd476c39f8a99fd974e5ec07564b5315d8bf99471bca0ef2f66", "d2424b1b1abe4eb8164227b085c9aa9456ea13493fd563e06fd51cf5694c78fc"], ["336581ea7bfbbb290c191a2f507a41cf5643842170e914faeab27c2c579f726", "ead12168595fe1be99252129b6e56b3391f7ab1410cd1e0ef3dcdcabd2fda224"], ["8ab89816dadfd6b6a1f2634fcf00ec8403781025ed6890c4849742706bd43ede", "6fdcef09f2f6d0a044e654aef624136f503d459c3e89845858a47a9129cdd24e"], ["1e33f1a746c9c5778133344d9299fcaa20b0938e8acff2544bb40284b8c5fb94", 
          "60660257dd11b3aa9c8ed618d24edff2306d320f1d03010e33a7d2057f3b3b6"], ["85b7c1dcb3cec1b7ee7f30ded79dd20a0ed1f4cc18cbcfcfa410361fd8f08f31", "3d98a9cdd026dd43f39048f25a8847f4fcafad1895d7a633c6fed3c35e999511"], ["29df9fbd8d9e46509275f4b125d6d45d7fbe9a3b878a7af872a2800661ac5f51", "b4c4fe99c775a606e2d8862179139ffda61dc861c019e55cd2876eb2a27d84b"], ["a0b1cae06b0a847a3fea6e671aaf8adfdfe58ca2f768105c8082b2e449fce252", "ae434102edde0958ec4b19d917a6a28e6b72da1834aff0e650f049503a296cf2"], ["4e8ceafb9b3e9a136dc7ff67e840295b499dfb3b2133e4ba113f2e4c0e121e5", 
          "cf2174118c8b6d7a4b48f6d534ce5c79422c086a63460502b827ce62a326683c"], ["d24a44e047e19b6f5afb81c7ca2f69080a5076689a010919f42725c2b789a33b", "6fb8d5591b466f8fc63db50f1c0f1c69013f996887b8244d2cdec417afea8fa3"], ["ea01606a7a6c9cdd249fdfcfacb99584001edd28abbab77b5104e98e8e3b35d4", "322af4908c7312b0cfbfe369f7a7b3cdb7d4494bc2823700cfd652188a3ea98d"], ["af8addbf2b661c8a6c6328655eb96651252007d8c5ea31be4ad196de8ce2131f", "6749e67c029b85f52a034eafd096836b2520818680e26ac8f3dfbcdb71749700"], ["e3ae1974566ca06cc516d47e0fb165a674a3dabcfca15e722f0e3450f45889", 
          "2aeabe7e4531510116217f07bf4d07300de97e4874f81f533420a72eeb0bd6a4"], ["591ee355313d99721cf6993ffed1e3e301993ff3ed258802075ea8ced397e246", "b0ea558a113c30bea60fc4775460c7901ff0b053d25ca2bdeee98f1a4be5d196"], ["11396d55fda54c49f19aa97318d8da61fa8584e47b084945077cf03255b52984", "998c74a8cd45ac01289d5833a7beb4744ff536b01b257be4c5767bea93ea57a4"], ["3c5d2a1ba39c5a1790000738c9e0c40b8dcdfd5468754b6405540157e017aa7a", "b2284279995a34e2f9d4de7396fc18b80f9b8b9fdd270f6661f79ca4c81bd257"], ["cc8704b8a60a0defa3a99a7299f2e9c3fbc395afb04ac078425ef8a1793cc030", 
          "bdd46039feed17881d1e0862db347f8cf395b74fc4bcdc4e940b74e3ac1f1b13"], ["c533e4f7ea8555aacd9777ac5cad29b97dd4defccc53ee7ea204119b2889b197", "6f0a256bc5efdf429a2fb6242f1a43a2d9b925bb4a4b3a26bb8e0f45eb596096"], ["c14f8f2ccb27d6f109f6d08d03cc96a69ba8c34eec07bbcf566d48e33da6593", "c359d6923bb398f7fd4473e16fe1c28475b740dd098075e6c0e8649113dc3a38"], ["a6cbc3046bc6a450bac24789fa17115a4c9739ed75f8f21ce441f72e0b90e6ef", "21ae7f4680e889bb130619e2c0f95a360ceb573c70603139862afd617fa9b9f"], ["347d6d9a02c48927ebfb86c1359b1caf130a3c0267d11ce6344b39f99d43cc38", 
          "60ea7f61a353524d1c987f6ecec92f086d565ab687870cb12689ff1e31c74448"], ["da6545d2181db8d983f7dcb375ef5866d47c67b1bf31c8cf855ef7437b72656a", "49b96715ab6878a79e78f07ce5680c5d6673051b4935bd897fea824b77dc208a"], ["c40747cc9d012cb1a13b8148309c6de7ec25d6945d657146b9d5994b8feb1111", "5ca560753be2a12fc6de6caf2cb489565db936156b9514e1bb5e83037e0fa2d4"], ["4e42c8ec82c99798ccf3a610be870e78338c7f713348bd34c8203ef4037f3502", "7571d74ee5e0fb92a7a8b33a07783341a5492144cc54bcc40a94473693606437"], ["3775ab7089bc6af823aba2e1af70b236d251cadb0c86743287522a1b3b0dedea", 
          "be52d107bcfa09d8bcb9736a828cfa7fac8db17bf7a76a2c42ad961409018cf7"], ["cee31cbf7e34ec379d94fb814d3d775ad954595d1314ba8846959e3e82f74e26", "8fd64a14c06b589c26b947ae2bcf6bfa0149ef0be14ed4d80f448a01c43b1c6d"], ["b4f9eaea09b6917619f6ea6a4eb5464efddb58fd45b1ebefcdc1a01d08b47986", "39e5c9925b5a54b07433a4f18c61726f8bb131c012ca542eb24a8ac07200682a"], ["d4263dfc3d2df923a0179a48966d30ce84e2515afc3dccc1b77907792ebcc60e", "62dfaf07a0f78feb30e30d6295853ce189e127760ad6cf7fae164e122a208d54"], ["48457524820fa65a4f8d35eb6930857c0032acc0a4a2de422233eeda897612c4", 
          "25a748ab367979d98733c38a1fa1c2e7dc6cc07db2d60a9ae7a76aaa49bd0f77"], ["dfeeef1881101f2cb11644f3a2afdfc2045e19919152923f367a1767c11cceda", "ecfb7056cf1de042f9420bab396793c0c390bde74b4bbdff16a83ae09a9a7517"], ["6d7ef6b17543f8373c573f44e1f389835d89bcbc6062ced36c82df83b8fae859", "cd450ec335438986dfefa10c57fea9bcc521a0959b2d80bbf74b190dca712d10"], ["e75605d59102a5a2684500d3b991f2e3f3c88b93225547035af25af66e04541f", "f5c54754a8f71ee540b9b48728473e314f729ac5308b06938360990e2bfad125"], ["eb98660f4c4dfaa06a2be453d5020bc99a0c2e60abe388457dd43fefb1ed620c", 
          "6cb9a8876d9cb8520609af3add26cd20a0a7cd8a9411131ce85f44100099223e"], ["13e87b027d8514d35939f2e6892b19922154596941888336dc3563e3b8dba942", "fef5a3c68059a6dec5d624114bf1e91aac2b9da568d6abeb2570d55646b8adf1"], ["ee163026e9fd6fe017c38f06a5be6fc125424b371ce2708e7bf4491691e5764a", "1acb250f255dd61c43d94ccc670d0f58f49ae3fa15b96623e5430da0ad6c62b2"], ["b268f5ef9ad51e4d78de3a750c2dc89b1e626d43505867999932e5db33af3d80", "5f310d4b3c99b9ebb19f77d41c1dee018cf0d34fd4191614003e945a1216e423"], ["ff07f3118a9df035e9fad85eb6c7bfe42b02f01ca99ceea3bf7ffdba93c4750d", 
          "438136d603e858a3a5c440c38eccbaddc1d2942114e2eddd4740d098ced1f0d8"], ["8d8b9855c7c052a34146fd20ffb658bea4b9f69e0d825ebec16e8c3ce2b526a1", "cdb559eedc2d79f926baf44fb84ea4d44bcf50fee51d7ceb30e2e7f463036758"], ["52db0b5384dfbf05bfa9d472d7ae26dfe4b851ceca91b1eba54263180da32b63", "c3b997d050ee5d423ebaf66a6db9f57b3180c902875679de924b69d84a7b375"], ["e62f9490d3d51da6395efd24e80919cc7d0f29c3f3fa48c6fff543becbd43352", "6d89ad7ba4876b0b22c2ca280c682862f342c8591f1daf5170e07bfd9ccafa7d"], ["7f30ea2476b399b4957509c88f77d0191afa2ff5cb7b14fd6d8e7d65aaab1193", 
          "ca5ef7d4b231c94c3b15389a5f6311e9daff7bb67b103e9880ef4bff637acaec"], ["5098ff1e1d9f14fb46a210fada6c903fef0fb7b4a1dd1d9ac60a0361800b7a00", "9731141d81fc8f8084d37c6e7542006b3ee1b40d60dfe5362a5b132fd17ddc0"], ["32b78c7de9ee512a72895be6b9cbefa6e2f3c4ccce445c96b9f2c81e2778ad58", "ee1849f513df71e32efc3896ee28260c73bb80547ae2275ba497237794c8753c"], ["e2cb74fddc8e9fbcd076eef2a7c72b0ce37d50f08269dfc074b581550547a4f7", "d3aa2ed71c9dd2247a62df062736eb0baddea9e36122d2be8641abcb005cc4a4"], ["8438447566d4d7bedadc299496ab357426009a35f235cb141be0d99cd10ae3a8", 
          "c4e1020916980a4da5d01ac5e6ad330734ef0d7906631c4f2390426b2edd791f"], ["4162d488b89402039b584c6fc6c308870587d9c46f660b878ab65c82c711d67e", "67163e903236289f776f22c25fb8a3afc1732f2b84b4e95dbda47ae5a0852649"], ["3fad3fa84caf0f34f0f89bfd2dcf54fc175d767aec3e50684f3ba4a4bf5f683d", "cd1bc7cb6cc407bb2f0ca647c718a730cf71872e7d0d2a53fa20efcdfe61826"], ["674f2600a3007a00568c1a7ce05d0816c1fb84bf1370798f1c69532faeb1a86b", "299d21f9413f33b3edf43b257004580b70db57da0b182259e09eecc69e0d38a5"], ["d32f4da54ade74abb81b815ad1fb3b263d82d6c692714bcff87d29bd5ee9f08f", 
          "f9429e738b8e53b968e99016c059707782e14f4535359d582fc416910b3eea87"], ["30e4e670435385556e593657135845d36fbb6931f72b08cb1ed954f1e3ce3ff6", "462f9bce619898638499350113bbc9b10a878d35da70740dc695a559eb88db7b"], ["be2062003c51cc3004682904330e4dee7f3dcd10b01e580bf1971b04d4cad297", "62188bc49d61e5428573d48a74e1c655b1c61090905682a0d5558ed72dccb9bc"], ["93144423ace3451ed29e0fb9ac2af211cb6e84a601df5993c419859fff5df04a", "7c10dfb164c3425f5c71a3f9d7992038f1065224f72bb9d1d902a6d13037b47c"], ["b015f8044f5fcbdcf21ca26d6c34fb8197829205c7b7d2a7cb66418c157b112c", 
          "ab8c1e086d04e813744a655b2df8d5f83b3cdc6faa3088c1d3aea1454e3a1d5f"], ["d5e9e1da649d97d89e4868117a465a3a4f8a18de57a140d36b3f2af341a21b52", "4cb04437f391ed73111a13cc1d4dd0db1693465c2240480d8955e8592f27447a"], ["d3ae41047dd7ca065dbf8ed77b992439983005cd72e16d6f996a5316d36966bb", "bd1aeb21ad22ebb22a10f0303417c6d964f8cdd7df0aca614b10dc14d125ac46"], ["463e2763d885f958fc66cdd22800f0a487197d0a82e377b49f80af87c897b065", "bfefacdb0e5d0fd7df3a311a94de062b26b80c61fbc97508b79992671ef7ca7f"], ["7985fdfd127c0567c6f53ec1bb63ec3158e597c40bfe747c83cddfc910641917", 
          "603c12daf3d9862ef2b25fe1de289aed24ed291e0ec6708703a5bd567f32ed03"], ["74a1ad6b5f76e39db2dd249410eac7f99e74c59cb83d2d0ed5ff1543da7703e9", "cc6157ef18c9c63cd6193d83631bbea0093e0968942e8c33d5737fd790e0db08"], ["30682a50703375f602d416664ba19b7fc9bab42c72747463a71d0896b22f6da3", "553e04f6b018b4fa6c8f39e7f311d3176290d0e0f19ca73f17714d9977a22ff8"], ["9e2158f0d7c0d5f26c3791efefa79597654e7a2b2464f52b1ee6c1347769ef57", "712fcdd1b9053f09003a3481fa7762e9ffd7c8ef35a38509e2fbf2629008373"], ["176e26989a43c9cfeba4029c202538c28172e566e3c4fce7322857f3be327d66", 
          "ed8cc9d04b29eb877d270b4878dc43c19aefd31f4eee09ee7b47834c1fa4b1c3"], ["75d46efea3771e6e68abb89a13ad747ecf1892393dfc4f1b7004788c50374da8", "9852390a99507679fd0b86fd2b39a868d7efc22151346e1a3ca4726586a6bed8"], ["809a20c67d64900ffb698c4c825f6d5f2310fb0451c869345b7319f645605721", "9e994980d9917e22b76b061927fa04143d096ccc54963e6a5ebfa5f3f8e286c1"], ["1b38903a43f7f114ed4500b4eac7083fdefece1cf29c63528d563446f972c180", "4036edc931a60ae889353f77fd53de4a2708b26b6f5da72ad3394119daf408f9"]]
        }
      };
    }, {}],
    15 : [function(p, canCreateDiscussions, s) {
      /** @type {!Object} */
      var self = s;
      var BN = p("bn.js");
      s = p("minimalistic-assert");
      p = p("minimalistic-crypto-utils");
      /** @type {!Object} */
      self.assert = s;
      self.toArray = p.toArray;
      self.zero2 = p.zero2;
      self.toHex = p.toHex;
      self.encode = p.encode;
      /**
       * @param {!Object} n
       * @param {number} v
       * @param {undefined} i
       * @return {?}
       */
      self.getNAF = function(n, v, i) {
        /** @type {!Array} */
        var r = new Array(Math.max(n.bitLength(), i) + 1);
        r.fill(0);
        /** @type {number} */
        var prec = 1 << v + 1;
        var num = n.clone();
        /** @type {number} */
        var k = 0;
        for (; k < r.length; k++) {
          var value;
          var stop = num.andln(prec - 1);
          if (num.isOdd()) {
            value = (prec >> 1) - 1 < stop ? (prec >> 1) - stop : stop;
            num.isubn(value);
          } else {
            /** @type {number} */
            value = 0;
          }
          r[k] = value;
          num.iushrn(1);
        }
        return r;
      };
      /**
       * @param {string} num
       * @param {string} target
       * @return {?}
       */
      self.getJSF = function(num, target) {
        /** @type {!Array} */
        var allAttachPoints = [[], []];
        num = num.clone();
        target = target.clone();
        var r;
        /** @type {number} */
        var nump = 0;
        /** @type {number} */
        var value = 0;
        for (; 0 < num.cmpn(-nump) || 0 < target.cmpn(-value);) {
          var n;
          /** @type {number} */
          var b = num.andln(3) + nump & 3;
          /** @type {number} */
          var falseySection = target.andln(3) + value & 3;
          if (3 === falseySection) {
            /** @type {number} */
            falseySection = -1;
          }
          /** @type {number} */
          n = 0 == (1 & (b = 3 === b ? -1 : b)) ? 0 : 3 !== (r = num.andln(7) + nump & 7) && 5 !== r || 2 !== falseySection ? b : -b;
          allAttachPoints[0].push(n);
          /** @type {number} */
          falseySection = 0 == (1 & falseySection) ? 0 : 3 !== (r = target.andln(7) + value & 7) && 5 !== r || 2 !== b ? falseySection : -falseySection;
          allAttachPoints[1].push(falseySection);
          if (2 * nump === n + 1) {
            /** @type {number} */
            nump = 1 - nump;
          }
          if (2 * value === falseySection + 1) {
            /** @type {number} */
            value = 1 - value;
          }
          num.iushrn(1);
          target.iushrn(1);
        }
        return allAttachPoints;
      };
      /**
       * @param {string} type
       * @param {string} key
       * @param {!Function} name
       * @return {undefined}
       */
      self.cachedProperty = function(type, key, name) {
        /** @type {string} */
        var prop = "_" + key;
        /**
         * @return {?}
         */
        type.prototype[key] = function() {
          return void 0 !== this[prop] ? this[prop] : this[prop] = name.call(this);
        };
      };
      /**
       * @param {!Function} str
       * @return {?}
       */
      self.parseBytes = function(str) {
        return "string" == typeof str ? self.toArray(str, "hex") : str;
      };
      /**
       * @param {string} m
       * @return {?}
       */
      self.intFromLE = function(m) {
        return new BN(m, "hex", "le");
      };
    }, {
      "bn.js" : 16,
      "minimalistic-assert" : 33,
      "minimalistic-crypto-utils" : 34
    }],
    16 : [function(require, canCreateDiscussions, isSlidingUp) {
      !function(module, exports) {
        /**
         * @param {boolean} expected_true
         * @param {string} message
         * @return {undefined}
         */
        function assert(expected_true, message) {
          if (!expected_true) {
            throw new Error(message || "Assertion failed");
          }
        }
        /**
         * @param {!Object} ctor
         * @param {!Function} superCtor
         * @return {undefined}
         */
        function inherits(ctor, superCtor) {
          /**
           * @return {undefined}
           */
          function TempCtor() {
          }
          /** @type {!Function} */
          ctor.super_ = superCtor;
          TempCtor.prototype = superCtor.prototype;
          ctor.prototype = new TempCtor;
          /** @type {!Object} */
          ctor.prototype.constructor = ctor;
        }
        /**
         * @param {number} number
         * @param {string} base
         * @param {string} endian
         * @return {?}
         */
        function BN(number, base, endian) {
          if (BN.isBN(number)) {
            return number;
          }
          /** @type {number} */
          this.negative = 0;
          /** @type {null} */
          this.words = null;
          /** @type {number} */
          this.length = 0;
          if ((this.red = null) !== number) {
            if (!("le" !== base && "be" !== base)) {
              /** @type {string} */
              endian = base;
              /** @type {number} */
              base = 10;
            }
            this._init(number || 0, base || 10, endian || "be");
          }
        }
        /**
         * @param {string} s
         * @param {number} index
         * @param {number} start
         * @return {?}
         */
        function parseHex(s, index, start) {
          /** @type {number} */
          var res = 0;
          /** @type {number} */
          var sides = Math.min(s.length, start);
          /** @type {number} */
          var i = index;
          for (; i < sides; i++) {
            /** @type {number} */
            var n = s.charCodeAt(i) - 48;
            /** @type {number} */
            res = res << 4;
            /** @type {number} */
            res = res | (49 <= n && n <= 54 ? n - 49 + 10 : 17 <= n && n <= 22 ? n - 17 + 10 : 15 & n);
          }
          return res;
        }
        /**
         * @param {string} str
         * @param {?} end
         * @param {?} start
         * @param {number} base
         * @return {?}
         */
        function parseBase(str, end, start, base) {
          /** @type {number} */
          var r = 0;
          /** @type {number} */
          var sides = Math.min(str.length, start);
          var i = end;
          for (; i < sides; i++) {
            /** @type {number} */
            var a = str.charCodeAt(i) - 48;
            /** @type {number} */
            r = r * base;
            /** @type {number} */
            r = r + (49 <= a ? a - 49 + 10 : 17 <= a ? a - 17 + 10 : a);
          }
          return r;
        }
        /**
         * @param {!Object} self
         * @param {!Object} num
         * @param {!Object} out
         * @return {?}
         */
        function smallMulTo(self, num, out) {
          /** @type {number} */
          out.negative = num.negative ^ self.negative;
          /** @type {number} */
          var r = self.length + num.length | 0;
          /** @type {number} */
          r = (out.length = r) - 1 | 0;
          /** @type {number} */
          var f = 67108863 & (valstr = (0 | self.words[0]) * (0 | num.words[0]));
          /** @type {number} */
          var carry = valstr / 67108864 | 0;
          /** @type {number} */
          out.words[0] = f;
          /** @type {number} */
          var i = 1;
          for (; i < r; i++) {
            /** @type {number} */
            var ncarry = carry >>> 26;
            /** @type {number} */
            var val = 67108863 & carry;
            /** @type {number} */
            var y = Math.min(i, num.length - 1);
            /** @type {number} */
            var j = Math.max(0, i - self.length + 1);
            for (; j <= y; j++) {
              var valstr;
              /** @type {number} */
              var word = i - j | 0;
              /** @type {number} */
              ncarry = ncarry + ((valstr = (0 | self.words[word]) * (0 | num.words[j]) + val) / 67108864 | 0);
              /** @type {number} */
              val = 67108863 & valstr;
            }
            /** @type {number} */
            out.words[i] = 0 | val;
            /** @type {number} */
            carry = 0 | ncarry;
          }
          return 0 !== carry ? out.words[i] = 0 | carry : out.length--, out.strip();
        }
        /**
         * @param {!Object} self
         * @param {!Array} num
         * @param {!Object} out
         * @return {?}
         */
        function jumboMulTo(self, num, out) {
          return (new FFTM).mulp(self, num, out);
        }
        /**
         * @param {string} x
         * @param {string} y
         * @return {undefined}
         */
        function FFTM(x, y) {
          /** @type {string} */
          this.x = x;
          /** @type {string} */
          this.y = y;
        }
        /**
         * @param {string} name
         * @param {!Image} p
         * @return {undefined}
         */
        function MPrime(name, p) {
          /** @type {string} */
          this.name = name;
          this.p = new BN(p, 16);
          this.n = this.p.bitLength();
          this.k = (new BN(1)).iushln(this.n).isub(this.p);
          this.tmp = this._tmp();
        }
        /**
         * @return {undefined}
         */
        function K256() {
          MPrime.call(this, "k256", "ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff fffffffe fffffc2f");
        }
        /**
         * @return {undefined}
         */
        function P224() {
          MPrime.call(this, "p224", "ffffffff ffffffff ffffffff ffffffff 00000000 00000000 00000001");
        }
        /**
         * @return {undefined}
         */
        function P192() {
          MPrime.call(this, "p192", "ffffffff ffffffff ffffffff fffffffe ffffffff ffffffff");
        }
        /**
         * @return {undefined}
         */
        function P25519() {
          MPrime.call(this, "25519", "7fffffffffffffff ffffffffffffffff ffffffffffffffff ffffffffffffffed");
        }
        /**
         * @param {!Array} m
         * @return {undefined}
         */
        function Red(m) {
          var prime;
          if ("string" == typeof m) {
            prime = BN._prime(m);
            this.m = prime.p;
            this.prime = prime;
          } else {
            assert(m.gtn(1), "modulus must be greater than 1");
            /** @type {!Array} */
            this.m = m;
            /** @type {null} */
            this.prime = null;
          }
        }
        /**
         * @param {?} m
         * @return {undefined}
         */
        function Mont(m) {
          Red.call(this, m);
          this.shift = this.m.bitLength();
          if (this.shift % 26 != 0) {
            this.shift += 26 - this.shift % 26;
          }
          this.r = (new BN(1)).iushln(this.shift);
          this.r2 = this.imod(this.r.sqr());
          this.rinv = this.r._invmp(this.m);
          this.minv = this.rinv.mul(this.r).isubn(1).div(this.m);
          this.minv = this.minv.umod(this.r);
          this.minv = this.r.sub(this.minv);
        }
        var Buffer;
        if ("object" == typeof module) {
          /** @type {function(number, string, string): ?} */
          module.exports = BN;
        } else {
          /** @type {function(number, string, string): ?} */
          exports.BN = BN;
        }
        /** @type {number} */
        (BN.BN = BN).wordSize = 26;
        try {
          Buffer = require("buffer").Buffer;
        } catch (e) {
        }
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.isBN = function(num) {
          return num instanceof BN || null !== num && "object" == typeof num && num.constructor.wordSize === BN.wordSize && Array.isArray(num.words);
        };
        /**
         * @param {!Object} x
         * @param {number} key
         * @return {?}
         */
        BN.max = function(x, key) {
          return 0 < x.cmp(key) ? x : key;
        };
        /**
         * @param {number} x
         * @param {?} key
         * @return {?}
         */
        BN.min = function(x, key) {
          return x.cmp(key) < 0 ? x : key;
        };
        /**
         * @param {number} number
         * @param {number} base
         * @param {string} endian
         * @return {?}
         */
        BN.prototype._init = function(number, base, endian) {
          if ("number" == typeof number) {
            return this._initNumber(number, base, endian);
          }
          if ("object" == typeof number) {
            return this._initArray(number, base, endian);
          }
          assert((base = "hex" === base ? 16 : base) === (0 | base) && 2 <= base && base <= 36);
          /** @type {number} */
          var start = 0;
          if ("-" === (number = number.toString().replace(/\s+/g, ""))[0]) {
            start++;
          }
          if (16 === base) {
            this._parseHex(number, start);
          } else {
            this._parseBase(number, base, start);
          }
          if ("-" === number[0]) {
            /** @type {number} */
            this.negative = 1;
          }
          this.strip();
          if ("le" === endian) {
            this._initArray(this.toArray(), base, endian);
          }
        };
        /**
         * @param {number} number
         * @param {number} base
         * @param {string} endian
         * @return {undefined}
         */
        BN.prototype._initNumber = function(number, base, endian) {
          if (number < 0) {
            /** @type {number} */
            this.negative = 1;
            /** @type {number} */
            number = -number;
          }
          if (number < 67108864) {
            /** @type {!Array} */
            this.words = [67108863 & number];
            /** @type {number} */
            this.length = 1;
          } else {
            if (number < 4503599627370496) {
              /** @type {!Array} */
              this.words = [67108863 & number, number / 67108864 & 67108863];
              /** @type {number} */
              this.length = 2;
            } else {
              assert(number < 9007199254740992);
              /** @type {!Array} */
              this.words = [67108863 & number, number / 67108864 & 67108863, 1];
              /** @type {number} */
              this.length = 3;
            }
          }
          if ("le" === endian) {
            this._initArray(this.toArray(), base, endian);
          }
        };
        /**
         * @param {number} number
         * @param {number} base
         * @param {string} endian
         * @return {?}
         */
        BN.prototype._initArray = function(number, base, endian) {
          if (assert("number" == typeof number.length), number.length <= 0) {
            return this.words = [0], this.length = 1, this;
          }
          /** @type {number} */
          this.length = Math.ceil(number.length / 3);
          /** @type {!Array} */
          this.words = new Array(this.length);
          var j;
          var val;
          /** @type {number} */
          var i = 0;
          for (; i < this.length; i++) {
            /** @type {number} */
            this.words[i] = 0;
          }
          /** @type {number} */
          var bi_valid = 0;
          if ("be" === endian) {
            /** @type {number} */
            i = number.length - 1;
            /** @type {number} */
            j = 0;
            for (; 0 <= i; i = i - 3) {
              /** @type {number} */
              val = number[i] | number[i - 1] << 8 | number[i - 2] << 16;
              this.words[j] |= val << bi_valid & 67108863;
              /** @type {number} */
              this.words[j + 1] = val >>> 26 - bi_valid & 67108863;
              if (26 <= (bi_valid = bi_valid + 24)) {
                /** @type {number} */
                bi_valid = bi_valid - 26;
                j++;
              }
            }
          } else {
            if ("le" === endian) {
              /** @type {number} */
              j = i = 0;
              for (; i < number.length; i = i + 3) {
                /** @type {number} */
                val = number[i] | number[i + 1] << 8 | number[i + 2] << 16;
                this.words[j] |= val << bi_valid & 67108863;
                /** @type {number} */
                this.words[j + 1] = val >>> 26 - bi_valid & 67108863;
                if (26 <= (bi_valid = bi_valid + 24)) {
                  /** @type {number} */
                  bi_valid = bi_valid - 26;
                  j++;
                }
              }
            }
          }
          return this.strip();
        };
        /**
         * @param {!Function} number
         * @param {number} start
         * @return {undefined}
         */
        BN.prototype._parseHex = function(number, start) {
          /** @type {number} */
          this.length = Math.ceil((number.length - start) / 6);
          /** @type {!Array} */
          this.words = new Array(this.length);
          var w;
          /** @type {number} */
          var i = 0;
          for (; i < this.length; i++) {
            /** @type {number} */
            this.words[i] = 0;
          }
          /** @type {number} */
          var off = 0;
          /** @type {number} */
          i = number.length - 6;
          /** @type {number} */
          var j = 0;
          for (; start <= i; i = i - 6) {
            w = parseHex(number, i, i + 6);
            this.words[j] |= w << off & 67108863;
            this.words[j + 1] |= w >>> 26 - off & 4194303;
            if (26 <= (off = off + 24)) {
              /** @type {number} */
              off = off - 26;
              j++;
            }
          }
          if (i + 6 !== start) {
            w = parseHex(number, start, i + 6);
            this.words[j] |= w << off & 67108863;
            this.words[j + 1] |= w >>> 26 - off & 4194303;
          }
          this.strip();
        };
        /**
         * @param {!Function} number
         * @param {number} base
         * @param {number} start
         * @return {undefined}
         */
        BN.prototype._parseBase = function(number, base, start) {
          /** @type {!Array} */
          this.words = [0];
          /** @type {number} */
          var len = 0;
          /** @type {number} */
          var limbPow = this.length = 1;
          for (; limbPow <= 67108863; limbPow = limbPow * base) {
            len++;
          }
          len--;
          /** @type {number} */
          limbPow = limbPow / base | 0;
          /** @type {number} */
          var count = number.length - start;
          /** @type {number} */
          var n = count % len;
          var page = Math.min(count, count - n) + start;
          /** @type {number} */
          var word = 0;
          /** @type {number} */
          var i = start;
          for (; i < page; i = i + len) {
            word = parseBase(number, i, i + len, base);
            this.imuln(limbPow);
            if (this.words[0] + word < 67108864) {
              this.words[0] += word;
            } else {
              this._iaddn(word);
            }
          }
          if (0 != n) {
            /** @type {number} */
            var limbPow = 1;
            word = parseBase(number, i, number.length, base);
            /** @type {number} */
            i = 0;
            for (; i < n; i++) {
              /** @type {number} */
              limbPow = limbPow * base;
            }
            this.imuln(limbPow);
            if (this.words[0] + word < 67108864) {
              this.words[0] += word;
            } else {
              this._iaddn(word);
            }
          }
        };
        /**
         * @param {!Object} dest
         * @return {undefined}
         */
        BN.prototype.copy = function(dest) {
          /** @type {!Array} */
          dest.words = new Array(this.length);
          /** @type {number} */
          var i = 0;
          for (; i < this.length; i++) {
            dest.words[i] = this.words[i];
          }
          dest.length = this.length;
          dest.negative = this.negative;
          dest.red = this.red;
        };
        /**
         * @return {?}
         */
        BN.prototype.clone = function() {
          var r = new BN(null);
          return this.copy(r), r;
        };
        /**
         * @param {number} i
         * @return {?}
         */
        BN.prototype._expand = function(i) {
          for (; this.length < i;) {
            /** @type {number} */
            this.words[this.length++] = 0;
          }
          return this;
        };
        /**
         * @return {?}
         */
        BN.prototype.strip = function() {
          for (; 1 < this.length && 0 === this.words[this.length - 1];) {
            this.length--;
          }
          return this._normSign();
        };
        /**
         * @return {?}
         */
        BN.prototype._normSign = function() {
          return 1 === this.length && 0 === this.words[0] && (this.negative = 0), this;
        };
        /**
         * @return {?}
         */
        BN.prototype.inspect = function() {
          return (this.red ? "<BN-R: " : "<BN: ") + this.toString(16) + ">";
        };
        /** @type {!Array} */
        var prefix = ["", "0", "00", "000", "0000", "00000", "000000", "0000000", "00000000", "000000000", "0000000000", "00000000000", "000000000000", "0000000000000", "00000000000000", "000000000000000", "0000000000000000", "00000000000000000", "000000000000000000", "0000000000000000000", "00000000000000000000", "000000000000000000000", "0000000000000000000000", "00000000000000000000000", "000000000000000000000000", "0000000000000000000000000"];
        /** @type {!Array} */
        var snippetList = [0, 0, 25, 16, 12, 11, 10, 9, 8, 8, 7, 7, 7, 7, 6, 6, 6, 6, 6, 6, 6, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5];
        /** @type {!Array} */
        var arraySpectrum = [0, 0, 33554432, 43046721, 16777216, 48828125, 60466176, 40353607, 16777216, 43046721, 1E7, 19487171, 35831808, 62748517, 7529536, 11390625, 16777216, 24137569, 34012224, 47045881, 64E6, 4084101, 5153632, 6436343, 7962624, 9765625, 11881376, 14348907, 17210368, 20511149, 243E5, 28629151, 33554432, 39135393, 45435424, 52521875, 60466176];
        /**
         * @param {number} i
         * @param {number} n
         * @return {?}
         */
        BN.prototype.toString = function(i, n) {
          if (n = 0 | n || 1, 16 === (i = i || 10) || "hex" === i) {
            /** @type {string} */
            suffix = "";
            /** @type {number} */
            var off = 0;
            /** @type {number} */
            var carry = 0;
            /** @type {number} */
            var i = 0;
            for (; i < this.length; i++) {
              var w = this.words[i];
              /** @type {string} */
              var _ = (16777215 & (w << off | carry)).toString(16);
              /** @type {string} */
              var suffix = 0 !== (carry = w >>> 24 - off & 16777215) || i !== this.length - 1 ? prefix[6 - _.length] + _ + suffix : _ + suffix;
              if (26 <= (off = off + 2)) {
                /** @type {number} */
                off = off - 26;
                i--;
              }
            }
            if (0 !== carry) {
              /** @type {string} */
              suffix = carry.toString(16) + suffix;
            }
            for (; suffix.length % n != 0;) {
              /** @type {string} */
              suffix = "0" + suffix;
            }
            return suffix = 0 !== this.negative ? "-" + suffix : suffix;
          }
          if (i === (0 | i) && 2 <= i && i <= 36) {
            var s = snippetList[i];
            var value = arraySpectrum[i];
            /** @type {string} */
            suffix = "";
            /** @type {number} */
            (c = this.clone()).negative = 0;
            for (; !c.isZero();) {
              var c;
              var prop = c.modn(value).toString(i);
              /** @type {string} */
              suffix = (c = c.idivn(value)).isZero() ? prop + suffix : prefix[s - prop.length] + prop + suffix;
            }
            if (this.isZero()) {
              /** @type {string} */
              suffix = "0" + suffix;
            }
            for (; suffix.length % n != 0;) {
              /** @type {string} */
              suffix = "0" + suffix;
            }
            return suffix = 0 !== this.negative ? "-" + suffix : suffix;
          }
          assert(false, "Base should be between 2 and 36");
        };
        /**
         * @return {?}
         */
        BN.prototype.toNumber = function() {
          var ret = this.words[0];
          return 2 === this.length ? ret = ret + 67108864 * this.words[1] : 3 === this.length && 1 === this.words[2] ? ret = ret + (4503599627370496 + 67108864 * this.words[1]) : 2 < this.length && assert(false, "Number can only safely store up to 53 bits"), 0 !== this.negative ? -ret : ret;
        };
        /**
         * @return {?}
         */
        BN.prototype.toJSON = function() {
          return this.toString(16);
        };
        /**
         * @param {!Object} value
         * @param {undefined} length
         * @return {?}
         */
        BN.prototype.toBuffer = function(value, length) {
          return assert(void 0 !== Buffer), this.toArrayLike(Buffer, value, length);
        };
        /**
         * @param {string} key
         * @param {?} type
         * @return {?}
         */
        BN.prototype.toArray = function(key, type) {
          return this.toArrayLike(Array, key, type);
        };
        /**
         * @param {!Function} ArrayType
         * @param {!Object} value
         * @param {number} length
         * @return {?}
         */
        BN.prototype.toArrayLike = function(ArrayType, value, length) {
          var byteLength = this.byteLength();
          var reqLength = length || Math.max(1, byteLength);
          assert(byteLength <= reqLength, "byte array longer than desired length");
          assert(0 < reqLength, "Requested array length <= 0");
          this.strip();
          var broken_ipv6;
          var i;
          /** @type {boolean} */
          value = "le" === value;
          var res = new ArrayType(reqLength);
          var q = this.clone();
          if (value) {
            /** @type {number} */
            i = 0;
            for (; !q.isZero(); i++) {
              broken_ipv6 = q.andln(255);
              q.iushrn(8);
              res[i] = broken_ipv6;
            }
            for (; i < reqLength; i++) {
              /** @type {number} */
              res[i] = 0;
            }
          } else {
            /** @type {number} */
            i = 0;
            for (; i < reqLength - byteLength; i++) {
              /** @type {number} */
              res[i] = 0;
            }
            /** @type {number} */
            i = 0;
            for (; !q.isZero(); i++) {
              broken_ipv6 = q.andln(255);
              q.iushrn(8);
              res[reqLength - i - 1] = broken_ipv6;
            }
          }
          return res;
        };
        if (Math.clz32) {
          /**
           * @param {number} w
           * @return {?}
           */
          BN.prototype._countBits = function(w) {
            return 32 - Math.clz32(w);
          };
        } else {
          /**
           * @param {number} w
           * @return {?}
           */
          BN.prototype._countBits = function(w) {
            /** @type {number} */
            var h = w;
            /** @type {number} */
            w = 0;
            return 4096 <= h && (w = w + 13, h = h >>> 13), 64 <= h && (w = w + 7, h = h >>> 7), 8 <= h && (w = w + 4, h = h >>> 4), 2 <= h && (w = w + 2, h = h >>> 2), w + h;
          };
        }
        /**
         * @param {number} w
         * @return {?}
         */
        BN.prototype._zeroBits = function(w) {
          if (0 === w) {
            return 26;
          }
          /** @type {number} */
          var h = w;
          /** @type {number} */
          w = 0;
          return 0 == (8191 & h) && (w = w + 13, h = h >>> 13), 0 == (127 & h) && (w = w + 7, h = h >>> 7), 0 == (15 & h) && (w = w + 4, h = h >>> 4), 0 == (3 & h) && (w = w + 2, h = h >>> 2), 0 == (1 & h) && w++, w;
        };
        /**
         * @return {?}
         */
        BN.prototype.bitLength = function() {
          var w = this.words[this.length - 1];
          w = this._countBits(w);
          return 26 * (this.length - 1) + w;
        };
        /**
         * @return {?}
         */
        BN.prototype.zeroBits = function() {
          if (this.isZero()) {
            return 0;
          }
          /** @type {number} */
          var r = 0;
          /** @type {number} */
          var i = 0;
          for (; i < this.length; i++) {
            var b = this._zeroBits(this.words[i]);
            if (r = r + b, 26 !== b) {
              break;
            }
          }
          return r;
        };
        /**
         * @return {?}
         */
        BN.prototype.byteLength = function() {
          return Math.ceil(this.bitLength() / 8);
        };
        /**
         * @param {undefined} width
         * @return {?}
         */
        BN.prototype.toTwos = function(width) {
          return 0 !== this.negative ? this.abs().inotn(width).iaddn(1) : this.clone();
        };
        /**
         * @param {number} width
         * @return {?}
         */
        BN.prototype.fromTwos = function(width) {
          return this.testn(width - 1) ? this.notn(width).iaddn(1).ineg() : this.clone();
        };
        /**
         * @return {?}
         */
        BN.prototype.isNeg = function() {
          return 0 !== this.negative;
        };
        /**
         * @return {?}
         */
        BN.prototype.neg = function() {
          return this.clone().ineg();
        };
        /**
         * @return {?}
         */
        BN.prototype.ineg = function() {
          return this.isZero() || (this.negative ^= 1), this;
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.iuor = function(num) {
          for (; this.length < num.length;) {
            /** @type {number} */
            this.words[this.length++] = 0;
          }
          /** @type {number} */
          var i = 0;
          for (; i < num.length; i++) {
            /** @type {number} */
            this.words[i] = this.words[i] | num.words[i];
          }
          return this.strip();
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.ior = function(num) {
          return assert(0 == (this.negative | num.negative)), this.iuor(num);
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.or = function(num) {
          return this.length > num.length ? this.clone().ior(num) : num.clone().ior(this);
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.uor = function(num) {
          return this.length > num.length ? this.clone().iuor(num) : num.clone().iuor(this);
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.iuand = function(num) {
          var $images = this.length > num.length ? num : this;
          /** @type {number} */
          var i = 0;
          for (; i < $images.length; i++) {
            /** @type {number} */
            this.words[i] = this.words[i] & num.words[i];
          }
          return this.length = $images.length, this.strip();
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.iand = function(num) {
          return assert(0 == (this.negative | num.negative)), this.iuand(num);
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.and = function(num) {
          return this.length > num.length ? this.clone().iand(num) : num.clone().iand(this);
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.uand = function(num) {
          return this.length > num.length ? this.clone().iuand(num) : num.clone().iuand(this);
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.iuxor = function(num) {
          var options;
          var bitset = this.length > num.length ? (options = this, num) : (options = num, this);
          /** @type {number} */
          var i = 0;
          for (; i < bitset.length; i++) {
            /** @type {number} */
            this.words[i] = options.words[i] ^ bitset.words[i];
          }
          if (this !== options) {
            for (; i < options.length; i++) {
              this.words[i] = options.words[i];
            }
          }
          return this.length = options.length, this.strip();
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.ixor = function(num) {
          return assert(0 == (this.negative | num.negative)), this.iuxor(num);
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.xor = function(num) {
          return this.length > num.length ? this.clone().ixor(num) : num.clone().ixor(this);
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.uxor = function(num) {
          return this.length > num.length ? this.clone().iuxor(num) : num.clone().iuxor(this);
        };
        /**
         * @param {number} b
         * @return {?}
         */
        BN.prototype.inotn = function(b) {
          assert("number" == typeof b && 0 <= b);
          /** @type {number} */
          var len = 0 | Math.ceil(b / 26);
          /** @type {number} */
          b = b % 26;
          this._expand(len);
          if (0 < b) {
            len--;
          }
          /** @type {number} */
          var i = 0;
          for (; i < len; i++) {
            /** @type {number} */
            this.words[i] = 67108863 & ~this.words[i];
          }
          return 0 < b && (this.words[i] = ~this.words[i] & 67108863 >> 26 - b), this.strip();
        };
        /**
         * @param {number} width
         * @return {?}
         */
        BN.prototype.notn = function(width) {
          return this.clone().inotn(width);
        };
        /**
         * @param {number} val
         * @param {number} type
         * @return {?}
         */
        BN.prototype.setn = function(val, type) {
          assert("number" == typeof val && 0 <= val);
          /** @type {number} */
          var j = val / 26 | 0;
          /** @type {number} */
          val = val % 26;
          return this._expand(1 + j), this.words[j] = type ? this.words[j] | 1 << val : this.words[j] & ~(1 << val), this.strip();
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.iadd = function(num) {
          var r;
          var options;
          var bitset;
          if (0 !== this.negative && 0 === num.negative) {
            return this.negative = 0, r = this.isub(num), this.negative ^= 1, this._normSign();
          }
          if (0 === this.negative && 0 !== num.negative) {
            return num.negative = 0, r = this.isub(num), num.negative = 1, r._normSign();
          }
          bitset = this.length > num.length ? (options = this, num) : (options = num, this);
          /** @type {number} */
          var carry = 0;
          /** @type {number} */
          var i = 0;
          for (; i < bitset.length; i++) {
            /** @type {number} */
            r = (0 | options.words[i]) + (0 | bitset.words[i]) + carry;
            /** @type {number} */
            this.words[i] = 67108863 & r;
            /** @type {number} */
            carry = r >>> 26;
          }
          for (; 0 !== carry && i < options.length; i++) {
            /** @type {number} */
            r = (0 | options.words[i]) + carry;
            /** @type {number} */
            this.words[i] = 67108863 & r;
            /** @type {number} */
            carry = r >>> 26;
          }
          if (this.length = options.length, 0 !== carry) {
            /** @type {number} */
            this.words[this.length] = carry;
            this.length++;
          } else {
            if (options !== this) {
              for (; i < options.length; i++) {
                this.words[i] = options.words[i];
              }
            }
          }
          return this;
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.add = function(num) {
          var res;
          return 0 !== num.negative && 0 === this.negative ? (num.negative = 0, res = this.sub(num), num.negative ^= 1, res) : 0 === num.negative && 0 !== this.negative ? (this.negative = 0, res = num.sub(this), this.negative = 1, res) : this.length > num.length ? this.clone().iadd(num) : num.clone().iadd(this);
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.isub = function(num) {
          if (0 !== num.negative) {
            /** @type {number} */
            num.negative = 0;
            var r = this.iadd(num);
            return num.negative = 1, r._normSign();
          }
          if (0 !== this.negative) {
            return this.negative = 0, this.iadd(num), this.negative = 1, this._normSign();
          }
          var options;
          var bitset;
          var cmp = this.cmp(num);
          if (0 === cmp) {
            return this.negative = 0, this.length = 1, this.words[0] = 0, this;
          }
          bitset = 0 < cmp ? (options = this, num) : (options = num, this);
          /** @type {number} */
          var string = 0;
          /** @type {number} */
          var i = 0;
          for (; i < bitset.length; i++) {
            /** @type {number} */
            string = (r = (0 | options.words[i]) - (0 | bitset.words[i]) + string) >> 26;
            /** @type {number} */
            this.words[i] = 67108863 & r;
          }
          for (; 0 !== string && i < options.length; i++) {
            /** @type {number} */
            string = (r = (0 | options.words[i]) + string) >> 26;
            /** @type {number} */
            this.words[i] = 67108863 & r;
          }
          if (0 === string && i < options.length && options !== this) {
            for (; i < options.length; i++) {
              this.words[i] = options.words[i];
            }
          }
          return this.length = Math.max(this.length, i), options !== this && (this.negative = 1), this.strip();
        };
        /**
         * @param {!Object} b
         * @return {?}
         */
        BN.prototype.sub = function(b) {
          return this.clone().isub(b);
        };
        /**
         * @param {number} self
         * @param {number} num
         * @param {!Object} out
         * @return {?}
         */
        var comb10MulTo = function(self, num, out) {
          var h = self.words;
          var e = num.words;
          var data = out.words;
          /** @type {number} */
          var i = 0 | h[0];
          /** @type {number} */
          var v = 8191 & i;
          /** @type {number} */
          var al2 = i >>> 13;
          /** @type {number} */
          var bh0 = 0 | h[1];
          /** @type {number} */
          var left = 8191 & bh0;
          /** @type {number} */
          var right = bh0 >>> 13;
          /** @type {number} */
          var a = 0 | h[2];
          /** @type {number} */
          var obj = 8191 & a;
          /** @type {number} */
          var str = a >>> 13;
          /** @type {number} */
          var b = 0 | h[3];
          /** @type {number} */
          var m = 8191 & b;
          /** @type {number} */
          var arr = b >>> 13;
          /** @type {number} */
          var bl4 = 0 | h[4];
          /** @type {number} */
          var current = 8191 & bl4;
          /** @type {number} */
          var al0 = bl4 >>> 13;
          /** @type {number} */
          var f = 0 | h[5];
          /** @type {number} */
          var c = 8191 & f;
          /** @type {number} */
          var r = f >>> 13;
          /** @type {number} */
          var map = 0 | h[6];
          /** @type {number} */
          var val = 8191 & map;
          /** @type {number} */
          var al9 = map >>> 13;
          /** @type {number} */
          var bh5 = 0 | h[7];
          /** @type {number} */
          var value = 8191 & bh5;
          /** @type {number} */
          var key = bh5 >>> 13;
          /** @type {number} */
          var type = 0 | h[8];
          /** @type {number} */
          var x = 8191 & type;
          /** @type {number} */
          var elem = type >>> 13;
          /** @type {number} */
          var n = 0 | h[9];
          /** @type {number} */
          var g = 8191 & n;
          /** @type {number} */
          var item = n >>> 13;
          /** @type {number} */
          var bh6 = 0 | e[0];
          /** @type {number} */
          var bl7 = 8191 & bh6;
          /** @type {number} */
          var bh7 = bh6 >>> 13;
          /** @type {number} */
          var ret = 0 | e[1];
          /** @type {number} */
          var bh4 = 8191 & ret;
          /** @type {number} */
          var bl8 = ret >>> 13;
          /** @type {number} */
          var html = 0 | e[2];
          /** @type {number} */
          var bl9 = 8191 & html;
          /** @type {number} */
          var bl5 = html >>> 13;
          /** @type {number} */
          var b1 = 0 | e[3];
          /** @type {number} */
          var bl0 = 8191 & b1;
          /** @type {number} */
          var bh1 = b1 >>> 13;
          /** @type {number} */
          var result = 0 | e[4];
          /** @type {number} */
          var bl6 = 8191 & result;
          /** @type {number} */
          var bl3 = result >>> 13;
          /** @type {number} */
          var b3 = 0 | e[5];
          /** @type {number} */
          var bh2 = 8191 & b3;
          /** @type {number} */
          var bh3 = b3 >>> 13;
          /** @type {number} */
          i = 0 | e[6];
          /** @type {number} */
          bh0 = 8191 & i;
          /** @type {number} */
          a = i >>> 13;
          /** @type {number} */
          b = 0 | e[7];
          /** @type {number} */
          bl4 = 8191 & b;
          /** @type {number} */
          f = b >>> 13;
          /** @type {number} */
          map = 0 | e[8];
          /** @type {number} */
          bh5 = 8191 & map;
          /** @type {number} */
          type = map >>> 13;
          /** @type {number} */
          h = 0 | e[9];
          /** @type {number} */
          n = 8191 & h;
          /** @type {number} */
          bh6 = h >>> 13;
          /** @type {number} */
          out.negative = self.negative ^ num.negative;
          /** @type {number} */
          out.length = 19;
          /** @type {number} */
          b1 = (0 + Math.imul(v, bl7) | 0) + ((8191 & (html = Math.imul(v, bh7) + Math.imul(al2, bl7) | 0)) << 13) | 0;
          /** @type {number} */
          var o = (Math.imul(al2, bh7) + (html >>> 13) | 0) + (b1 >>> 26) | 0;
          /** @type {number} */
          b1 = b1 & 67108863;
          /** @type {number} */
          ret = Math.imul(left, bl7);
          /** @type {number} */
          html = Math.imul(left, bh7) + Math.imul(right, bl7) | 0;
          /** @type {number} */
          result = Math.imul(right, bh7);
          /** @type {number} */
          b3 = (o + (ret + Math.imul(v, bh4) | 0) | 0) + ((8191 & (html = (html + Math.imul(v, bl8) | 0) + Math.imul(al2, bh4) | 0)) << 13) | 0;
          /** @type {number} */
          o = ((result + Math.imul(al2, bl8) | 0) + (html >>> 13) | 0) + (b3 >>> 26) | 0;
          /** @type {number} */
          b3 = b3 & 67108863;
          /** @type {number} */
          ret = Math.imul(obj, bl7);
          /** @type {number} */
          html = Math.imul(obj, bh7) + Math.imul(str, bl7) | 0;
          /** @type {number} */
          result = Math.imul(str, bh7);
          /** @type {number} */
          ret = ret + Math.imul(left, bh4) | 0;
          /** @type {number} */
          html = (html + Math.imul(left, bl8) | 0) + Math.imul(right, bh4) | 0;
          /** @type {number} */
          result = result + Math.imul(right, bl8) | 0;
          /** @type {number} */
          i = (o + (ret + Math.imul(v, bl9) | 0) | 0) + ((8191 & (html = (html + Math.imul(v, bl5) | 0) + Math.imul(al2, bl9) | 0)) << 13) | 0;
          /** @type {number} */
          o = ((result + Math.imul(al2, bl5) | 0) + (html >>> 13) | 0) + (i >>> 26) | 0;
          /** @type {number} */
          i = i & 67108863;
          /** @type {number} */
          ret = Math.imul(m, bl7);
          /** @type {number} */
          html = Math.imul(m, bh7) + Math.imul(arr, bl7) | 0;
          /** @type {number} */
          result = Math.imul(arr, bh7);
          /** @type {number} */
          ret = ret + Math.imul(obj, bh4) | 0;
          /** @type {number} */
          html = (html + Math.imul(obj, bl8) | 0) + Math.imul(str, bh4) | 0;
          /** @type {number} */
          result = result + Math.imul(str, bl8) | 0;
          /** @type {number} */
          ret = ret + Math.imul(left, bl9) | 0;
          /** @type {number} */
          html = (html + Math.imul(left, bl5) | 0) + Math.imul(right, bl9) | 0;
          /** @type {number} */
          result = result + Math.imul(right, bl5) | 0;
          /** @type {number} */
          b = (o + (ret + Math.imul(v, bl0) | 0) | 0) + ((8191 & (html = (html + Math.imul(v, bh1) | 0) + Math.imul(al2, bl0) | 0)) << 13) | 0;
          /** @type {number} */
          o = ((result + Math.imul(al2, bh1) | 0) + (html >>> 13) | 0) + (b >>> 26) | 0;
          /** @type {number} */
          b = b & 67108863;
          /** @type {number} */
          ret = Math.imul(current, bl7);
          /** @type {number} */
          html = Math.imul(current, bh7) + Math.imul(al0, bl7) | 0;
          /** @type {number} */
          result = Math.imul(al0, bh7);
          /** @type {number} */
          ret = ret + Math.imul(m, bh4) | 0;
          /** @type {number} */
          html = (html + Math.imul(m, bl8) | 0) + Math.imul(arr, bh4) | 0;
          /** @type {number} */
          result = result + Math.imul(arr, bl8) | 0;
          /** @type {number} */
          ret = ret + Math.imul(obj, bl9) | 0;
          /** @type {number} */
          html = (html + Math.imul(obj, bl5) | 0) + Math.imul(str, bl9) | 0;
          /** @type {number} */
          result = result + Math.imul(str, bl5) | 0;
          /** @type {number} */
          ret = ret + Math.imul(left, bl0) | 0;
          /** @type {number} */
          html = (html + Math.imul(left, bh1) | 0) + Math.imul(right, bl0) | 0;
          /** @type {number} */
          result = result + Math.imul(right, bh1) | 0;
          /** @type {number} */
          map = (o + (ret + Math.imul(v, bl6) | 0) | 0) + ((8191 & (html = (html + Math.imul(v, bl3) | 0) + Math.imul(al2, bl6) | 0)) << 13) | 0;
          /** @type {number} */
          o = ((result + Math.imul(al2, bl3) | 0) + (html >>> 13) | 0) + (map >>> 26) | 0;
          /** @type {number} */
          map = map & 67108863;
          /** @type {number} */
          ret = Math.imul(c, bl7);
          /** @type {number} */
          html = Math.imul(c, bh7) + Math.imul(r, bl7) | 0;
          /** @type {number} */
          result = Math.imul(r, bh7);
          /** @type {number} */
          ret = ret + Math.imul(current, bh4) | 0;
          /** @type {number} */
          html = (html + Math.imul(current, bl8) | 0) + Math.imul(al0, bh4) | 0;
          /** @type {number} */
          result = result + Math.imul(al0, bl8) | 0;
          /** @type {number} */
          ret = ret + Math.imul(m, bl9) | 0;
          /** @type {number} */
          html = (html + Math.imul(m, bl5) | 0) + Math.imul(arr, bl9) | 0;
          /** @type {number} */
          result = result + Math.imul(arr, bl5) | 0;
          /** @type {number} */
          ret = ret + Math.imul(obj, bl0) | 0;
          /** @type {number} */
          html = (html + Math.imul(obj, bh1) | 0) + Math.imul(str, bl0) | 0;
          /** @type {number} */
          result = result + Math.imul(str, bh1) | 0;
          /** @type {number} */
          ret = ret + Math.imul(left, bl6) | 0;
          /** @type {number} */
          html = (html + Math.imul(left, bl3) | 0) + Math.imul(right, bl6) | 0;
          /** @type {number} */
          result = result + Math.imul(right, bl3) | 0;
          /** @type {number} */
          e = (o + (ret + Math.imul(v, bh2) | 0) | 0) + ((8191 & (html = (html + Math.imul(v, bh3) | 0) + Math.imul(al2, bh2) | 0)) << 13) | 0;
          /** @type {number} */
          o = ((result + Math.imul(al2, bh3) | 0) + (html >>> 13) | 0) + (e >>> 26) | 0;
          /** @type {number} */
          e = e & 67108863;
          /** @type {number} */
          ret = Math.imul(val, bl7);
          /** @type {number} */
          html = Math.imul(val, bh7) + Math.imul(al9, bl7) | 0;
          /** @type {number} */
          result = Math.imul(al9, bh7);
          /** @type {number} */
          ret = ret + Math.imul(c, bh4) | 0;
          /** @type {number} */
          html = (html + Math.imul(c, bl8) | 0) + Math.imul(r, bh4) | 0;
          /** @type {number} */
          result = result + Math.imul(r, bl8) | 0;
          /** @type {number} */
          ret = ret + Math.imul(current, bl9) | 0;
          /** @type {number} */
          html = (html + Math.imul(current, bl5) | 0) + Math.imul(al0, bl9) | 0;
          /** @type {number} */
          result = result + Math.imul(al0, bl5) | 0;
          /** @type {number} */
          ret = ret + Math.imul(m, bl0) | 0;
          /** @type {number} */
          html = (html + Math.imul(m, bh1) | 0) + Math.imul(arr, bl0) | 0;
          /** @type {number} */
          result = result + Math.imul(arr, bh1) | 0;
          /** @type {number} */
          ret = ret + Math.imul(obj, bl6) | 0;
          /** @type {number} */
          html = (html + Math.imul(obj, bl3) | 0) + Math.imul(str, bl6) | 0;
          /** @type {number} */
          result = result + Math.imul(str, bl3) | 0;
          /** @type {number} */
          ret = ret + Math.imul(left, bh2) | 0;
          /** @type {number} */
          html = (html + Math.imul(left, bh3) | 0) + Math.imul(right, bh2) | 0;
          /** @type {number} */
          result = result + Math.imul(right, bh3) | 0;
          /** @type {number} */
          h = (o + (ret + Math.imul(v, bh0) | 0) | 0) + ((8191 & (html = (html + Math.imul(v, a) | 0) + Math.imul(al2, bh0) | 0)) << 13) | 0;
          /** @type {number} */
          o = ((result + Math.imul(al2, a) | 0) + (html >>> 13) | 0) + (h >>> 26) | 0;
          /** @type {number} */
          h = h & 67108863;
          /** @type {number} */
          ret = Math.imul(value, bl7);
          /** @type {number} */
          html = Math.imul(value, bh7) + Math.imul(key, bl7) | 0;
          /** @type {number} */
          result = Math.imul(key, bh7);
          /** @type {number} */
          ret = ret + Math.imul(val, bh4) | 0;
          /** @type {number} */
          html = (html + Math.imul(val, bl8) | 0) + Math.imul(al9, bh4) | 0;
          /** @type {number} */
          result = result + Math.imul(al9, bl8) | 0;
          /** @type {number} */
          ret = ret + Math.imul(c, bl9) | 0;
          /** @type {number} */
          html = (html + Math.imul(c, bl5) | 0) + Math.imul(r, bl9) | 0;
          /** @type {number} */
          result = result + Math.imul(r, bl5) | 0;
          /** @type {number} */
          ret = ret + Math.imul(current, bl0) | 0;
          /** @type {number} */
          html = (html + Math.imul(current, bh1) | 0) + Math.imul(al0, bl0) | 0;
          /** @type {number} */
          result = result + Math.imul(al0, bh1) | 0;
          /** @type {number} */
          ret = ret + Math.imul(m, bl6) | 0;
          /** @type {number} */
          html = (html + Math.imul(m, bl3) | 0) + Math.imul(arr, bl6) | 0;
          /** @type {number} */
          result = result + Math.imul(arr, bl3) | 0;
          /** @type {number} */
          ret = ret + Math.imul(obj, bh2) | 0;
          /** @type {number} */
          html = (html + Math.imul(obj, bh3) | 0) + Math.imul(str, bh2) | 0;
          /** @type {number} */
          result = result + Math.imul(str, bh3) | 0;
          /** @type {number} */
          ret = ret + Math.imul(left, bh0) | 0;
          /** @type {number} */
          html = (html + Math.imul(left, a) | 0) + Math.imul(right, bh0) | 0;
          /** @type {number} */
          result = result + Math.imul(right, a) | 0;
          /** @type {number} */
          self = (o + (ret + Math.imul(v, bl4) | 0) | 0) + ((8191 & (html = (html + Math.imul(v, f) | 0) + Math.imul(al2, bl4) | 0)) << 13) | 0;
          /** @type {number} */
          o = ((result + Math.imul(al2, f) | 0) + (html >>> 13) | 0) + (self >>> 26) | 0;
          /** @type {number} */
          self = self & 67108863;
          /** @type {number} */
          ret = Math.imul(x, bl7);
          /** @type {number} */
          html = Math.imul(x, bh7) + Math.imul(elem, bl7) | 0;
          /** @type {number} */
          result = Math.imul(elem, bh7);
          /** @type {number} */
          ret = ret + Math.imul(value, bh4) | 0;
          /** @type {number} */
          html = (html + Math.imul(value, bl8) | 0) + Math.imul(key, bh4) | 0;
          /** @type {number} */
          result = result + Math.imul(key, bl8) | 0;
          /** @type {number} */
          ret = ret + Math.imul(val, bl9) | 0;
          /** @type {number} */
          html = (html + Math.imul(val, bl5) | 0) + Math.imul(al9, bl9) | 0;
          /** @type {number} */
          result = result + Math.imul(al9, bl5) | 0;
          /** @type {number} */
          ret = ret + Math.imul(c, bl0) | 0;
          /** @type {number} */
          html = (html + Math.imul(c, bh1) | 0) + Math.imul(r, bl0) | 0;
          /** @type {number} */
          result = result + Math.imul(r, bh1) | 0;
          /** @type {number} */
          ret = ret + Math.imul(current, bl6) | 0;
          /** @type {number} */
          html = (html + Math.imul(current, bl3) | 0) + Math.imul(al0, bl6) | 0;
          /** @type {number} */
          result = result + Math.imul(al0, bl3) | 0;
          /** @type {number} */
          ret = ret + Math.imul(m, bh2) | 0;
          /** @type {number} */
          html = (html + Math.imul(m, bh3) | 0) + Math.imul(arr, bh2) | 0;
          /** @type {number} */
          result = result + Math.imul(arr, bh3) | 0;
          /** @type {number} */
          ret = ret + Math.imul(obj, bh0) | 0;
          /** @type {number} */
          html = (html + Math.imul(obj, a) | 0) + Math.imul(str, bh0) | 0;
          /** @type {number} */
          result = result + Math.imul(str, a) | 0;
          /** @type {number} */
          ret = ret + Math.imul(left, bl4) | 0;
          /** @type {number} */
          html = (html + Math.imul(left, f) | 0) + Math.imul(right, bl4) | 0;
          /** @type {number} */
          result = result + Math.imul(right, f) | 0;
          /** @type {number} */
          num = (o + (ret + Math.imul(v, bh5) | 0) | 0) + ((8191 & (html = (html + Math.imul(v, type) | 0) + Math.imul(al2, bh5) | 0)) << 13) | 0;
          /** @type {number} */
          o = ((result + Math.imul(al2, type) | 0) + (html >>> 13) | 0) + (num >>> 26) | 0;
          /** @type {number} */
          num = num & 67108863;
          /** @type {number} */
          ret = Math.imul(g, bl7);
          /** @type {number} */
          html = Math.imul(g, bh7) + Math.imul(item, bl7) | 0;
          /** @type {number} */
          result = Math.imul(item, bh7);
          /** @type {number} */
          ret = ret + Math.imul(x, bh4) | 0;
          /** @type {number} */
          html = (html + Math.imul(x, bl8) | 0) + Math.imul(elem, bh4) | 0;
          /** @type {number} */
          result = result + Math.imul(elem, bl8) | 0;
          /** @type {number} */
          ret = ret + Math.imul(value, bl9) | 0;
          /** @type {number} */
          html = (html + Math.imul(value, bl5) | 0) + Math.imul(key, bl9) | 0;
          /** @type {number} */
          result = result + Math.imul(key, bl5) | 0;
          /** @type {number} */
          ret = ret + Math.imul(val, bl0) | 0;
          /** @type {number} */
          html = (html + Math.imul(val, bh1) | 0) + Math.imul(al9, bl0) | 0;
          /** @type {number} */
          result = result + Math.imul(al9, bh1) | 0;
          /** @type {number} */
          ret = ret + Math.imul(c, bl6) | 0;
          /** @type {number} */
          html = (html + Math.imul(c, bl3) | 0) + Math.imul(r, bl6) | 0;
          /** @type {number} */
          result = result + Math.imul(r, bl3) | 0;
          /** @type {number} */
          ret = ret + Math.imul(current, bh2) | 0;
          /** @type {number} */
          html = (html + Math.imul(current, bh3) | 0) + Math.imul(al0, bh2) | 0;
          /** @type {number} */
          result = result + Math.imul(al0, bh3) | 0;
          /** @type {number} */
          ret = ret + Math.imul(m, bh0) | 0;
          /** @type {number} */
          html = (html + Math.imul(m, a) | 0) + Math.imul(arr, bh0) | 0;
          /** @type {number} */
          result = result + Math.imul(arr, a) | 0;
          /** @type {number} */
          ret = ret + Math.imul(obj, bl4) | 0;
          /** @type {number} */
          html = (html + Math.imul(obj, f) | 0) + Math.imul(str, bl4) | 0;
          /** @type {number} */
          result = result + Math.imul(str, f) | 0;
          /** @type {number} */
          ret = ret + Math.imul(left, bh5) | 0;
          /** @type {number} */
          html = (html + Math.imul(left, type) | 0) + Math.imul(right, bh5) | 0;
          /** @type {number} */
          result = result + Math.imul(right, type) | 0;
          /** @type {number} */
          v = (o + (ret + Math.imul(v, n) | 0) | 0) + ((8191 & (html = (html + Math.imul(v, bh6) | 0) + Math.imul(al2, n) | 0)) << 13) | 0;
          /** @type {number} */
          o = ((result + Math.imul(al2, bh6) | 0) + (html >>> 13) | 0) + (v >>> 26) | 0;
          /** @type {number} */
          v = v & 67108863;
          /** @type {number} */
          ret = Math.imul(g, bh4);
          /** @type {number} */
          html = Math.imul(g, bl8) + Math.imul(item, bh4) | 0;
          /** @type {number} */
          result = Math.imul(item, bl8);
          /** @type {number} */
          ret = ret + Math.imul(x, bl9) | 0;
          /** @type {number} */
          html = (html + Math.imul(x, bl5) | 0) + Math.imul(elem, bl9) | 0;
          /** @type {number} */
          result = result + Math.imul(elem, bl5) | 0;
          /** @type {number} */
          ret = ret + Math.imul(value, bl0) | 0;
          /** @type {number} */
          html = (html + Math.imul(value, bh1) | 0) + Math.imul(key, bl0) | 0;
          /** @type {number} */
          result = result + Math.imul(key, bh1) | 0;
          /** @type {number} */
          ret = ret + Math.imul(val, bl6) | 0;
          /** @type {number} */
          html = (html + Math.imul(val, bl3) | 0) + Math.imul(al9, bl6) | 0;
          /** @type {number} */
          result = result + Math.imul(al9, bl3) | 0;
          /** @type {number} */
          ret = ret + Math.imul(c, bh2) | 0;
          /** @type {number} */
          html = (html + Math.imul(c, bh3) | 0) + Math.imul(r, bh2) | 0;
          /** @type {number} */
          result = result + Math.imul(r, bh3) | 0;
          /** @type {number} */
          ret = ret + Math.imul(current, bh0) | 0;
          /** @type {number} */
          html = (html + Math.imul(current, a) | 0) + Math.imul(al0, bh0) | 0;
          /** @type {number} */
          result = result + Math.imul(al0, a) | 0;
          /** @type {number} */
          ret = ret + Math.imul(m, bl4) | 0;
          /** @type {number} */
          html = (html + Math.imul(m, f) | 0) + Math.imul(arr, bl4) | 0;
          /** @type {number} */
          result = result + Math.imul(arr, f) | 0;
          /** @type {number} */
          ret = ret + Math.imul(obj, bh5) | 0;
          /** @type {number} */
          html = (html + Math.imul(obj, type) | 0) + Math.imul(str, bh5) | 0;
          /** @type {number} */
          result = result + Math.imul(str, type) | 0;
          /** @type {number} */
          left = (o + (ret + Math.imul(left, n) | 0) | 0) + ((8191 & (html = (html + Math.imul(left, bh6) | 0) + Math.imul(right, n) | 0)) << 13) | 0;
          /** @type {number} */
          o = ((result + Math.imul(right, bh6) | 0) + (html >>> 13) | 0) + (left >>> 26) | 0;
          /** @type {number} */
          left = left & 67108863;
          /** @type {number} */
          ret = Math.imul(g, bl9);
          /** @type {number} */
          html = Math.imul(g, bl5) + Math.imul(item, bl9) | 0;
          /** @type {number} */
          result = Math.imul(item, bl5);
          /** @type {number} */
          ret = ret + Math.imul(x, bl0) | 0;
          /** @type {number} */
          html = (html + Math.imul(x, bh1) | 0) + Math.imul(elem, bl0) | 0;
          /** @type {number} */
          result = result + Math.imul(elem, bh1) | 0;
          /** @type {number} */
          ret = ret + Math.imul(value, bl6) | 0;
          /** @type {number} */
          html = (html + Math.imul(value, bl3) | 0) + Math.imul(key, bl6) | 0;
          /** @type {number} */
          result = result + Math.imul(key, bl3) | 0;
          /** @type {number} */
          ret = ret + Math.imul(val, bh2) | 0;
          /** @type {number} */
          html = (html + Math.imul(val, bh3) | 0) + Math.imul(al9, bh2) | 0;
          /** @type {number} */
          result = result + Math.imul(al9, bh3) | 0;
          /** @type {number} */
          ret = ret + Math.imul(c, bh0) | 0;
          /** @type {number} */
          html = (html + Math.imul(c, a) | 0) + Math.imul(r, bh0) | 0;
          /** @type {number} */
          result = result + Math.imul(r, a) | 0;
          /** @type {number} */
          ret = ret + Math.imul(current, bl4) | 0;
          /** @type {number} */
          html = (html + Math.imul(current, f) | 0) + Math.imul(al0, bl4) | 0;
          /** @type {number} */
          result = result + Math.imul(al0, f) | 0;
          /** @type {number} */
          ret = ret + Math.imul(m, bh5) | 0;
          /** @type {number} */
          html = (html + Math.imul(m, type) | 0) + Math.imul(arr, bh5) | 0;
          /** @type {number} */
          result = result + Math.imul(arr, type) | 0;
          /** @type {number} */
          obj = (o + (ret + Math.imul(obj, n) | 0) | 0) + ((8191 & (html = (html + Math.imul(obj, bh6) | 0) + Math.imul(str, n) | 0)) << 13) | 0;
          /** @type {number} */
          o = ((result + Math.imul(str, bh6) | 0) + (html >>> 13) | 0) + (obj >>> 26) | 0;
          /** @type {number} */
          obj = obj & 67108863;
          /** @type {number} */
          ret = Math.imul(g, bl0);
          /** @type {number} */
          html = Math.imul(g, bh1) + Math.imul(item, bl0) | 0;
          /** @type {number} */
          result = Math.imul(item, bh1);
          /** @type {number} */
          ret = ret + Math.imul(x, bl6) | 0;
          /** @type {number} */
          html = (html + Math.imul(x, bl3) | 0) + Math.imul(elem, bl6) | 0;
          /** @type {number} */
          result = result + Math.imul(elem, bl3) | 0;
          /** @type {number} */
          ret = ret + Math.imul(value, bh2) | 0;
          /** @type {number} */
          html = (html + Math.imul(value, bh3) | 0) + Math.imul(key, bh2) | 0;
          /** @type {number} */
          result = result + Math.imul(key, bh3) | 0;
          /** @type {number} */
          ret = ret + Math.imul(val, bh0) | 0;
          /** @type {number} */
          html = (html + Math.imul(val, a) | 0) + Math.imul(al9, bh0) | 0;
          /** @type {number} */
          result = result + Math.imul(al9, a) | 0;
          /** @type {number} */
          ret = ret + Math.imul(c, bl4) | 0;
          /** @type {number} */
          html = (html + Math.imul(c, f) | 0) + Math.imul(r, bl4) | 0;
          /** @type {number} */
          result = result + Math.imul(r, f) | 0;
          /** @type {number} */
          ret = ret + Math.imul(current, bh5) | 0;
          /** @type {number} */
          html = (html + Math.imul(current, type) | 0) + Math.imul(al0, bh5) | 0;
          /** @type {number} */
          result = result + Math.imul(al0, type) | 0;
          /** @type {number} */
          m = (o + (ret + Math.imul(m, n) | 0) | 0) + ((8191 & (html = (html + Math.imul(m, bh6) | 0) + Math.imul(arr, n) | 0)) << 13) | 0;
          /** @type {number} */
          o = ((result + Math.imul(arr, bh6) | 0) + (html >>> 13) | 0) + (m >>> 26) | 0;
          /** @type {number} */
          m = m & 67108863;
          /** @type {number} */
          ret = Math.imul(g, bl6);
          /** @type {number} */
          html = Math.imul(g, bl3) + Math.imul(item, bl6) | 0;
          /** @type {number} */
          result = Math.imul(item, bl3);
          /** @type {number} */
          ret = ret + Math.imul(x, bh2) | 0;
          /** @type {number} */
          html = (html + Math.imul(x, bh3) | 0) + Math.imul(elem, bh2) | 0;
          /** @type {number} */
          result = result + Math.imul(elem, bh3) | 0;
          /** @type {number} */
          ret = ret + Math.imul(value, bh0) | 0;
          /** @type {number} */
          html = (html + Math.imul(value, a) | 0) + Math.imul(key, bh0) | 0;
          /** @type {number} */
          result = result + Math.imul(key, a) | 0;
          /** @type {number} */
          ret = ret + Math.imul(val, bl4) | 0;
          /** @type {number} */
          html = (html + Math.imul(val, f) | 0) + Math.imul(al9, bl4) | 0;
          /** @type {number} */
          result = result + Math.imul(al9, f) | 0;
          /** @type {number} */
          ret = ret + Math.imul(c, bh5) | 0;
          /** @type {number} */
          html = (html + Math.imul(c, type) | 0) + Math.imul(r, bh5) | 0;
          /** @type {number} */
          result = result + Math.imul(r, type) | 0;
          /** @type {number} */
          current = (o + (ret + Math.imul(current, n) | 0) | 0) + ((8191 & (html = (html + Math.imul(current, bh6) | 0) + Math.imul(al0, n) | 0)) << 13) | 0;
          /** @type {number} */
          o = ((result + Math.imul(al0, bh6) | 0) + (html >>> 13) | 0) + (current >>> 26) | 0;
          /** @type {number} */
          current = current & 67108863;
          /** @type {number} */
          ret = Math.imul(g, bh2);
          /** @type {number} */
          html = Math.imul(g, bh3) + Math.imul(item, bh2) | 0;
          /** @type {number} */
          result = Math.imul(item, bh3);
          /** @type {number} */
          ret = ret + Math.imul(x, bh0) | 0;
          /** @type {number} */
          html = (html + Math.imul(x, a) | 0) + Math.imul(elem, bh0) | 0;
          /** @type {number} */
          result = result + Math.imul(elem, a) | 0;
          /** @type {number} */
          ret = ret + Math.imul(value, bl4) | 0;
          /** @type {number} */
          html = (html + Math.imul(value, f) | 0) + Math.imul(key, bl4) | 0;
          /** @type {number} */
          result = result + Math.imul(key, f) | 0;
          /** @type {number} */
          ret = ret + Math.imul(val, bh5) | 0;
          /** @type {number} */
          html = (html + Math.imul(val, type) | 0) + Math.imul(al9, bh5) | 0;
          /** @type {number} */
          result = result + Math.imul(al9, type) | 0;
          /** @type {number} */
          c = (o + (ret + Math.imul(c, n) | 0) | 0) + ((8191 & (html = (html + Math.imul(c, bh6) | 0) + Math.imul(r, n) | 0)) << 13) | 0;
          /** @type {number} */
          o = ((result + Math.imul(r, bh6) | 0) + (html >>> 13) | 0) + (c >>> 26) | 0;
          /** @type {number} */
          c = c & 67108863;
          /** @type {number} */
          ret = Math.imul(g, bh0);
          /** @type {number} */
          html = Math.imul(g, a) + Math.imul(item, bh0) | 0;
          /** @type {number} */
          result = Math.imul(item, a);
          /** @type {number} */
          ret = ret + Math.imul(x, bl4) | 0;
          /** @type {number} */
          html = (html + Math.imul(x, f) | 0) + Math.imul(elem, bl4) | 0;
          /** @type {number} */
          result = result + Math.imul(elem, f) | 0;
          /** @type {number} */
          ret = ret + Math.imul(value, bh5) | 0;
          /** @type {number} */
          html = (html + Math.imul(value, type) | 0) + Math.imul(key, bh5) | 0;
          /** @type {number} */
          result = result + Math.imul(key, type) | 0;
          /** @type {number} */
          val = (o + (ret + Math.imul(val, n) | 0) | 0) + ((8191 & (html = (html + Math.imul(val, bh6) | 0) + Math.imul(al9, n) | 0)) << 13) | 0;
          /** @type {number} */
          o = ((result + Math.imul(al9, bh6) | 0) + (html >>> 13) | 0) + (val >>> 26) | 0;
          /** @type {number} */
          val = val & 67108863;
          /** @type {number} */
          ret = Math.imul(g, bl4);
          /** @type {number} */
          html = Math.imul(g, f) + Math.imul(item, bl4) | 0;
          /** @type {number} */
          result = Math.imul(item, f);
          /** @type {number} */
          ret = ret + Math.imul(x, bh5) | 0;
          /** @type {number} */
          html = (html + Math.imul(x, type) | 0) + Math.imul(elem, bh5) | 0;
          /** @type {number} */
          result = result + Math.imul(elem, type) | 0;
          /** @type {number} */
          value = (o + (ret + Math.imul(value, n) | 0) | 0) + ((8191 & (html = (html + Math.imul(value, bh6) | 0) + Math.imul(key, n) | 0)) << 13) | 0;
          /** @type {number} */
          o = ((result + Math.imul(key, bh6) | 0) + (html >>> 13) | 0) + (value >>> 26) | 0;
          /** @type {number} */
          value = value & 67108863;
          /** @type {number} */
          ret = Math.imul(g, bh5);
          /** @type {number} */
          html = Math.imul(g, type) + Math.imul(item, bh5) | 0;
          /** @type {number} */
          result = Math.imul(item, type);
          /** @type {number} */
          x = (o + (ret + Math.imul(x, n) | 0) | 0) + ((8191 & (html = (html + Math.imul(x, bh6) | 0) + Math.imul(elem, n) | 0)) << 13) | 0;
          /** @type {number} */
          o = ((result + Math.imul(elem, bh6) | 0) + (html >>> 13) | 0) + (x >>> 26) | 0;
          /** @type {number} */
          x = x & 67108863;
          /** @type {number} */
          n = (o + Math.imul(g, n) | 0) + ((8191 & (html = Math.imul(g, bh6) + Math.imul(item, n) | 0)) << 13) | 0;
          return o = (Math.imul(item, bh6) + (html >>> 13) | 0) + (n >>> 26) | 0, n = n & 67108863, data[0] = b1, data[1] = b3, data[2] = i, data[3] = b, data[4] = map, data[5] = e, data[6] = h, data[7] = self, data[8] = num, data[9] = v, data[10] = left, data[11] = obj, data[12] = m, data[13] = current, data[14] = c, data[15] = val, data[16] = value, data[17] = x, data[18] = n, 0 != o && (data[19] = o, out.length++), out;
        };
        if (!Math.imul) {
          /** @type {function(!Object, !Object, !Object): ?} */
          comb10MulTo = smallMulTo;
        }
        /**
         * @param {?} num
         * @param {?} res
         * @return {?}
         */
        BN.prototype.mulTo = function(num, res) {
          var l = this.length + num.length;
          res = (10 === this.length && 10 === num.length ? comb10MulTo : l < 63 ? smallMulTo : l < 1024 ? function(self, num, a) {
            /** @type {number} */
            a.negative = num.negative ^ self.negative;
            a.length = self.length + num.length;
            /** @type {number} */
            var val = 0;
            /** @type {number} */
            var opt_index = 0;
            /** @type {number} */
            var i = 0;
            for (; i < a.length - 1; i++) {
              /** @type {number} */
              var index = opt_index;
              /** @type {number} */
              opt_index = 0;
              /** @type {number} */
              var v = 67108863 & val;
              /** @type {number} */
              var m = Math.min(i, num.length - 1);
              /** @type {number} */
              var j = Math.max(0, i - self.length + 1);
              for (; j <= m; j++) {
                /** @type {number} */
                var k = i - j;
                /** @type {number} */
                var d = (0 | self.words[k]) * (0 | num.words[j]);
                /** @type {number} */
                k = 67108863 & d;
                /** @type {number} */
                v = 67108863 & (k = k + v | 0);
                /** @type {number} */
                opt_index = opt_index + ((index = (index = index + (d / 67108864 | 0) | 0) + (k >>> 26) | 0) >>> 26);
                /** @type {number} */
                index = index & 67108863;
              }
              /** @type {number} */
              a.words[i] = v;
              /** @type {number} */
              val = index;
              /** @type {number} */
              index = opt_index;
            }
            return 0 !== val ? a.words[i] = val : a.length--, a.strip();
          } : jumboMulTo)(this, num, res);
          return res;
        };
        /**
         * @param {number} N
         * @return {?}
         */
        FFTM.prototype.makeRBT = function(N) {
          /** @type {!Array} */
          var t = new Array(N);
          /** @type {number} */
          var l = BN.prototype._countBits(N) - 1;
          /** @type {number} */
          var i = 0;
          for (; i < N; i++) {
            t[i] = this.revBin(i, l, N);
          }
          return t;
        };
        /**
         * @param {number} x
         * @param {number} l
         * @param {number} N
         * @return {?}
         */
        FFTM.prototype.revBin = function(x, l, N) {
          if (0 === x || x === N - 1) {
            return x;
          }
          /** @type {number} */
          var rb = 0;
          /** @type {number} */
          var i = 0;
          for (; i < l; i++) {
            /** @type {number} */
            rb = rb | (1 & x) << l - i - 1;
            /** @type {number} */
            x = x >> 1;
          }
          return rb;
        };
        /**
         * @param {!NodeList} rbt
         * @param {!Array} rws
         * @param {!Array} iws
         * @param {!Object} rtws
         * @param {!Object} itws
         * @param {number} N
         * @return {undefined}
         */
        FFTM.prototype.permute = function(rbt, rws, iws, rtws, itws, N) {
          /** @type {number} */
          var i = 0;
          for (; i < N; i++) {
            rtws[i] = rws[rbt[i]];
            itws[i] = iws[rbt[i]];
          }
        };
        /**
         * @param {!Array} rws
         * @param {!Array} iws
         * @param {!Object} rtws
         * @param {!Object} itws
         * @param {number} N
         * @param {(Node|NodeList|string)} rbt
         * @return {undefined}
         */
        FFTM.prototype.transform = function(rws, iws, rtws, itws, N, rbt) {
          this.permute(rbt, rws, iws, rtws, itws, N);
          /** @type {number} */
          var s = 1;
          for (; s < N; s = s << 1) {
            /** @type {number} */
            var l = s << 1;
            /** @type {number} */
            var rtwdf = Math.cos(2 * Math.PI / l);
            /** @type {number} */
            var itwdf = Math.sin(2 * Math.PI / l);
            /** @type {number} */
            var p = 0;
            for (; p < N; p = p + l) {
              /** @type {number} */
              var rtwdf_ = rtwdf;
              /** @type {number} */
              var itwdf_ = itwdf;
              /** @type {number} */
              var j = 0;
              for (; j < s; j++) {
                var re = rtws[p + j];
                var ie = itws[p + j];
                /** @type {number} */
                var rx = rtwdf_ * (ro = rtws[p + j + s]) - itwdf_ * (io = itws[p + j + s]);
                /** @type {number} */
                var io = rtwdf_ * io + itwdf_ * ro;
                /** @type {number} */
                var ro = rx;
                rtws[p + j] = re + ro;
                itws[p + j] = ie + io;
                /** @type {number} */
                rtws[p + j + s] = re - ro;
                /** @type {number} */
                itws[p + j + s] = ie - io;
                if (j !== l) {
                  /** @type {number} */
                  rx = rtwdf * rtwdf_ - itwdf * itwdf_;
                  /** @type {number} */
                  itwdf_ = rtwdf * itwdf_ + itwdf * rtwdf_;
                  /** @type {number} */
                  rtwdf_ = rx;
                }
              }
            }
          }
        };
        /**
         * @param {number} m
         * @param {!Object} n
         * @return {?}
         */
        FFTM.prototype.guessLen13b = function(m, n) {
          /** @type {number} */
          m = 1 & (carry = 1 | Math.max(n, m));
          /** @type {number} */
          var h = 0;
          /** @type {number} */
          var carry = carry / 2 | 0;
          for (; carry; carry = carry >>> 1) {
            h++;
          }
          return 1 << h + 1 + m;
        };
        /**
         * @param {!Array} rws
         * @param {!Array} iws
         * @param {number} N
         * @return {undefined}
         */
        FFTM.prototype.conjugate = function(rws, iws, N) {
          if (!(N <= 1)) {
            /** @type {number} */
            var i = 0;
            for (; i < N / 2; i++) {
              var t = rws[i];
              rws[i] = rws[N - i - 1];
              rws[N - i - 1] = t;
              t = iws[i];
              /** @type {number} */
              iws[i] = -iws[N - i - 1];
              /** @type {number} */
              iws[N - i - 1] = -t;
            }
          }
        };
        /**
         * @param {!Array} ws
         * @param {number} N
         * @return {?}
         */
        FFTM.prototype.normalize13b = function(ws, N) {
          /** @type {number} */
          var baseName = 0;
          /** @type {number} */
          var i = 0;
          for (; i < N / 2; i++) {
            /** @type {number} */
            var middlePathName = 8192 * Math.round(ws[2 * i + 1] / N) + Math.round(ws[2 * i] / N) + baseName;
            /** @type {number} */
            ws[i] = 67108863 & middlePathName;
            /** @type {number} */
            baseName = middlePathName < 67108864 ? 0 : middlePathName / 67108864 | 0;
          }
          return ws;
        };
        /**
         * @param {!NodeList} ws
         * @param {number} len
         * @param {!Array} rws
         * @param {number} N
         * @return {undefined}
         */
        FFTM.prototype.convert13b = function(ws, len, rws, N) {
          /** @type {number} */
          var b = 0;
          /** @type {number} */
          var i = 0;
          for (; i < len; i++) {
            /** @type {number} */
            b = b + (0 | ws[i]);
            /** @type {number} */
            rws[2 * i] = 8191 & b;
            /** @type {number} */
            b = b >>> 13;
            /** @type {number} */
            rws[2 * i + 1] = 8191 & b;
            /** @type {number} */
            b = b >>> 13;
          }
          /** @type {number} */
          i = 2 * len;
          for (; i < N; ++i) {
            /** @type {number} */
            rws[i] = 0;
          }
          assert(0 === b);
          assert(0 == (-8192 & b));
        };
        /**
         * @param {number} N
         * @return {?}
         */
        FFTM.prototype.stub = function(N) {
          /** @type {!Array} */
          var ph = new Array(N);
          /** @type {number} */
          var i = 0;
          for (; i < N; i++) {
            /** @type {number} */
            ph[i] = 0;
          }
          return ph;
        };
        /**
         * @param {!Object} x
         * @param {!Object} y
         * @param {!Object} out
         * @return {?}
         */
        FFTM.prototype.mulp = function(x, y, out) {
          /** @type {number} */
          var N = 2 * this.guessLen13b(x.length, y.length);
          var rbt = this.makeRBT(N);
          var _ = this.stub(N);
          /** @type {!Array} */
          var nrws = new Array(N);
          /** @type {!Array} */
          var rwst = new Array(N);
          /** @type {!Array} */
          var iwst = new Array(N);
          /** @type {!Array} */
          var rws = new Array(N);
          /** @type {!Array} */
          var nrwst = new Array(N);
          /** @type {!Array} */
          var niwst = new Array(N);
          var rmws = out.words;
          /** @type {number} */
          rmws.length = N;
          this.convert13b(x.words, x.length, nrws, N);
          this.convert13b(y.words, y.length, rws, N);
          this.transform(nrws, _, rwst, iwst, N, rbt);
          this.transform(rws, _, nrwst, niwst, N, rbt);
          /** @type {number} */
          var i = 0;
          for (; i < N; i++) {
            /** @type {number} */
            var rx = rwst[i] * nrwst[i] - iwst[i] * niwst[i];
            /** @type {number} */
            iwst[i] = rwst[i] * niwst[i] + iwst[i] * nrwst[i];
            /** @type {number} */
            rwst[i] = rx;
          }
          return this.conjugate(rwst, iwst, N), this.transform(rwst, iwst, rmws, _, N, rbt), this.conjugate(rmws, _, N), this.normalize13b(rmws, N), out.negative = x.negative ^ y.negative, out.length = x.length + y.length, out.strip();
        };
        /**
         * @param {?} num
         * @return {?}
         */
        BN.prototype.mul = function(num) {
          var out = new BN(null);
          return out.words = new Array(this.length + num.length), this.mulTo(num, out);
        };
        /**
         * @param {!Array} num
         * @return {?}
         */
        BN.prototype.mulf = function(num) {
          var out = new BN(null);
          return out.words = new Array(this.length + num.length), jumboMulTo(this, num, out);
        };
        /**
         * @param {number} num
         * @return {?}
         */
        BN.prototype.imul = function(num) {
          return this.clone().mulTo(num, this);
        };
        /**
         * @param {number} num
         * @return {?}
         */
        BN.prototype.imuln = function(num) {
          assert("number" == typeof num);
          assert(num < 67108864);
          /** @type {number} */
          var carry = 0;
          /** @type {number} */
          var i = 0;
          for (; i < this.length; i++) {
            /** @type {number} */
            var x = (0 | this.words[i]) * num;
            /** @type {number} */
            var wideValue = (67108863 & x) + (67108863 & carry);
            /** @type {number} */
            carry = carry >> 26;
            /** @type {number} */
            carry = carry + (x / 67108864 | 0);
            /** @type {number} */
            carry = carry + (wideValue >>> 26);
            /** @type {number} */
            this.words[i] = 67108863 & wideValue;
          }
          return 0 !== carry && (this.words[i] = carry, this.length++), this;
        };
        /**
         * @param {undefined} value
         * @return {?}
         */
        BN.prototype.muln = function(value) {
          return this.clone().imuln(value);
        };
        /**
         * @return {?}
         */
        BN.prototype.sqr = function() {
          return this.mul(this);
        };
        /**
         * @return {?}
         */
        BN.prototype.isqr = function() {
          return this.imul(this.clone());
        };
        /**
         * @param {!Array} a
         * @return {?}
         */
        BN.prototype.pow = function(a) {
          var ids = function(num) {
            /** @type {!Array} */
            var w = new Array(num.bitLength());
            /** @type {number} */
            var bit = 0;
            for (; bit < w.length; bit++) {
              /** @type {number} */
              var off = bit / 26 | 0;
              /** @type {number} */
              var wbit = bit % 26;
              /** @type {number} */
              w[bit] = (num.words[off] & 1 << wbit) >>> wbit;
            }
            return w;
          }(a);
          if (0 === ids.length) {
            return new BN(1);
          }
          var res = this;
          /** @type {number} */
          var i = 0;
          for (; i < ids.length && 0 === ids[i]; i++, res = res.sqr()) {
          }
          if (++i < ids.length) {
            var q = res.sqr();
            for (; i < ids.length; i++, q = q.sqr()) {
              if (0 !== ids[i]) {
                res = res.mul(q);
              }
            }
          }
          return res;
        };
        /**
         * @param {number} bits
         * @return {?}
         */
        BN.prototype.iushln = function(bits) {
          assert("number" == typeof bits && 0 <= bits);
          /** @type {number} */
          var r = bits % 26;
          /** @type {number} */
          var s = (bits - r) / 26;
          /** @type {number} */
          var carryMask = 67108863 >>> 26 - r << 26 - r;
          if (0 != r) {
            /** @type {number} */
            var carry = 0;
            /** @type {number} */
            var i = 0;
            for (; i < this.length; i++) {
              /** @type {number} */
              var newCarry = this.words[i] & carryMask;
              /** @type {number} */
              var c = (0 | this.words[i]) - newCarry << r;
              /** @type {number} */
              this.words[i] = c | carry;
              /** @type {number} */
              carry = newCarry >>> 26 - r;
            }
            if (carry) {
              /** @type {number} */
              this.words[i] = carry;
              this.length++;
            }
          }
          if (0 != s) {
            /** @type {number} */
            i = this.length - 1;
            for (; 0 <= i; i--) {
              this.words[i + s] = this.words[i];
            }
            /** @type {number} */
            i = 0;
            for (; i < s; i++) {
              /** @type {number} */
              this.words[i] = 0;
            }
            this.length += s;
          }
          return this.strip();
        };
        /**
         * @param {number} bits
         * @return {?}
         */
        BN.prototype.ishln = function(bits) {
          return assert(0 === this.negative), this.iushln(bits);
        };
        /**
         * @param {number} bits
         * @param {number} hint
         * @param {!Object} extended
         * @return {?}
         */
        BN.prototype.iushrn = function(bits, hint, extended) {
          var c;
          assert("number" == typeof bits && 0 <= bits);
          /** @type {number} */
          c = hint ? (hint - hint % 26) / 26 : 0;
          /** @type {number} */
          var r = bits % 26;
          /** @type {number} */
          var s = Math.min((bits - r) / 26, this.length);
          /** @type {number} */
          var mask = 67108863 ^ 67108863 >>> r << r;
          /** @type {!Object} */
          var maskedWords = extended;
          if (c = c - s, c = Math.max(0, c), maskedWords) {
            /** @type {number} */
            var i = 0;
            for (; i < s; i++) {
              maskedWords.words[i] = this.words[i];
            }
            /** @type {number} */
            maskedWords.length = s;
          }
          if (0 !== s) {
            if (this.length > s) {
              this.length -= s;
              /** @type {number} */
              i = 0;
              for (; i < this.length; i++) {
                this.words[i] = this.words[i + s];
              }
            } else {
              /** @type {number} */
              this.words[0] = 0;
              /** @type {number} */
              this.length = 1;
            }
          }
          /** @type {number} */
          var carry = 0;
          /** @type {number} */
          i = this.length - 1;
          for (; 0 <= i && (0 !== carry || c <= i); i--) {
            /** @type {number} */
            var word = 0 | this.words[i];
            /** @type {number} */
            this.words[i] = carry << 26 - r | word >>> r;
            /** @type {number} */
            carry = word & mask;
          }
          return maskedWords && 0 !== carry && (maskedWords.words[maskedWords.length++] = carry), 0 === this.length && (this.words[0] = 0, this.length = 1), this.strip();
        };
        /**
         * @param {number} bits
         * @param {undefined} hint
         * @param {!Object} extended
         * @return {?}
         */
        BN.prototype.ishrn = function(bits, hint, extended) {
          return assert(0 === this.negative), this.iushrn(bits, hint, extended);
        };
        /**
         * @param {undefined} bits
         * @return {?}
         */
        BN.prototype.shln = function(bits) {
          return this.clone().ishln(bits);
        };
        /**
         * @param {number} value
         * @return {?}
         */
        BN.prototype.ushln = function(value) {
          return this.clone().iushln(value);
        };
        /**
         * @param {undefined} bits
         * @return {?}
         */
        BN.prototype.shrn = function(bits) {
          return this.clone().ishrn(bits);
        };
        /**
         * @param {number} value
         * @return {?}
         */
        BN.prototype.ushrn = function(value) {
          return this.clone().iushrn(value);
        };
        /**
         * @param {number} s
         * @return {?}
         */
        BN.prototype.testn = function(s) {
          assert("number" == typeof s && 0 <= s);
          /** @type {number} */
          var s60 = s % 26;
          /** @type {number} */
          s = (s - s60) / 26;
          /** @type {number} */
          s60 = 1 << s60;
          return !(this.length <= s) && !!(this.words[s] & s60);
        };
        /**
         * @param {number} length
         * @return {?}
         */
        BN.prototype.imaskn = function(length) {
          assert("number" == typeof length && 0 <= length);
          /** @type {number} */
          var leftoverLength = length % 26;
          /** @type {number} */
          length = (length - leftoverLength) / 26;
          return assert(0 === this.negative, "imaskn works only with positive numbers"), this.length <= length ? this : (0 != leftoverLength && length++, this.length = Math.min(length, this.length), 0 != leftoverLength && (leftoverLength = 67108863 ^ 67108863 >>> leftoverLength << leftoverLength, this.words[this.length - 1] &= leftoverLength), this.strip());
        };
        /**
         * @param {undefined} bits
         * @return {?}
         */
        BN.prototype.maskn = function(bits) {
          return this.clone().imaskn(bits);
        };
        /**
         * @param {number} num
         * @return {?}
         */
        BN.prototype.iaddn = function(num) {
          return assert("number" == typeof num), assert(num < 67108864), num < 0 ? this.isubn(-num) : 0 !== this.negative ? (1 === this.length && (0 | this.words[0]) < num ? (this.words[0] = num - (0 | this.words[0]), this.negative = 0) : (this.negative = 0, this.isubn(num), this.negative = 1), this) : this._iaddn(num);
        };
        /**
         * @param {number} num
         * @return {?}
         */
        BN.prototype._iaddn = function(num) {
          this.words[0] += num;
          /** @type {number} */
          var i = 0;
          for (; i < this.length && 67108864 <= this.words[i]; i++) {
            this.words[i] -= 67108864;
            if (i === this.length - 1) {
              /** @type {number} */
              this.words[i + 1] = 1;
            } else {
              this.words[i + 1]++;
            }
          }
          return this.length = Math.max(this.length, i + 1), this;
        };
        /**
         * @param {number} num
         * @return {?}
         */
        BN.prototype.isubn = function(num) {
          if (assert("number" == typeof num), assert(num < 67108864), num < 0) {
            return this.iaddn(-num);
          }
          if (0 !== this.negative) {
            return this.negative = 0, this.iaddn(num), this.negative = 1, this;
          }
          if (this.words[0] -= num, 1 === this.length && this.words[0] < 0) {
            /** @type {number} */
            this.words[0] = -this.words[0];
            /** @type {number} */
            this.negative = 1;
          } else {
            /** @type {number} */
            var i = 0;
            for (; i < this.length && this.words[i] < 0; i++) {
              this.words[i] += 67108864;
              --this.words[i + 1];
            }
          }
          return this.strip();
        };
        /**
         * @param {number} num
         * @return {?}
         */
        BN.prototype.addn = function(num) {
          return this.clone().iaddn(num);
        };
        /**
         * @param {number} num
         * @return {?}
         */
        BN.prototype.subn = function(num) {
          return this.clone().isubn(num);
        };
        /**
         * @return {?}
         */
        BN.prototype.iabs = function() {
          return this.negative = 0, this;
        };
        /**
         * @return {?}
         */
        BN.prototype.abs = function() {
          return this.clone().iabs();
        };
        /**
         * @param {!Object} num
         * @param {number} mul
         * @param {number} shift
         * @return {?}
         */
        BN.prototype._ishlnsubmul = function(num, mul, shift) {
          var t;
          var len = num.length + shift;
          this._expand(len);
          /** @type {number} */
          var x = 0;
          /** @type {number} */
          var i = 0;
          for (; i < num.length; i++) {
            /** @type {number} */
            t = (0 | this.words[i + shift]) + x;
            /** @type {number} */
            var mask = (0 | num.words[i]) * mul;
            /** @type {number} */
            x = ((t = t - (67108863 & mask)) >> 26) - (mask / 67108864 | 0);
            /** @type {number} */
            this.words[i + shift] = 67108863 & t;
          }
          for (; i < this.length - shift; i++) {
            /** @type {number} */
            x = (t = (0 | this.words[i + shift]) + x) >> 26;
            /** @type {number} */
            this.words[i + shift] = 67108863 & t;
          }
          if (0 === x) {
            return this.strip();
          }
          assert(-1 === x);
          /** @type {number} */
          i = x = 0;
          for (; i < this.length; i++) {
            /** @type {number} */
            x = (t = -(0 | this.words[i]) + x) >> 26;
            /** @type {number} */
            this.words[i] = 67108863 & t;
          }
          return this.negative = 1, this.strip();
        };
        /**
         * @param {!Object} num
         * @param {string} mode
         * @return {?}
         */
        BN.prototype._wordDiv = function(num, mode) {
          /** @type {number} */
          var shift = this.length - num.length;
          var a = this.clone();
          /** @type {!Object} */
          var b = num;
          /** @type {number} */
          var bhi = 0 | b.words[b.length - 1];
          if (0 != (shift = 26 - this._countBits(bhi))) {
            b = b.ushln(shift);
            a.iushln(shift);
            /** @type {number} */
            bhi = 0 | b.words[b.length - 1];
          }
          var q;
          /** @type {number} */
          var m = a.length - b.length;
          if ("mod" !== mode) {
            /** @type {number} */
            (q = new BN(null)).length = 1 + m;
            /** @type {!Array} */
            q.words = new Array(q.length);
            /** @type {number} */
            var i = 0;
            for (; i < q.length; i++) {
              /** @type {number} */
              q.words[i] = 0;
            }
          }
          num = a.clone()._ishlnsubmul(b, 1, m);
          if (0 === num.negative) {
            /** @type {!Object} */
            a = num;
            if (q) {
              /** @type {number} */
              q.words[m] = 1;
            }
          }
          /** @type {number} */
          var j = m - 1;
          for (; 0 <= j; j--) {
            /** @type {number} */
            var qj = 67108864 * (0 | a.words[b.length + j]) + (0 | a.words[b.length + j - 1]);
            /** @type {number} */
            qj = Math.min(qj / bhi | 0, 67108863);
            a._ishlnsubmul(b, qj, j);
            for (; 0 !== a.negative;) {
              qj--;
              /** @type {number} */
              a.negative = 0;
              a._ishlnsubmul(b, 1, j);
              if (!a.isZero()) {
                a.negative ^= 1;
              }
            }
            if (q) {
              /** @type {number} */
              q.words[j] = qj;
            }
          }
          return q && q.strip(), a.strip(), "div" !== mode && 0 != shift && a.iushrn(shift), {
            div : q || null,
            mod : a
          };
        };
        /**
         * @param {!Object} num
         * @param {string} mode
         * @param {boolean} positive
         * @return {?}
         */
        BN.prototype.divmod = function(num, mode, positive) {
          return assert(!num.isZero()), this.isZero() ? {
            div : new BN(0),
            mod : new BN(0)
          } : 0 !== this.negative && 0 === num.negative ? (res = this.neg().divmod(num, mode), "mod" !== mode && (container = res.div.neg()), "div" !== mode && (mod = res.mod.neg(), positive && 0 !== mod.negative && mod.iadd(num)), {
            div : container,
            mod : mod
          }) : 0 === this.negative && 0 !== num.negative ? (res = this.divmod(num.neg(), mode), {
            div : container = "mod" !== mode ? res.div.neg() : container,
            mod : res.mod
          }) : 0 != (this.negative & num.negative) ? (res = this.neg().divmod(num.neg(), mode), "div" !== mode && (mod = res.mod.neg(), positive && 0 !== mod.negative && mod.isub(num)), {
            div : res.div,
            mod : mod
          }) : num.length > this.length || this.cmp(num) < 0 ? {
            div : new BN(0),
            mod : this
          } : 1 === num.length ? "div" === mode ? {
            div : this.divn(num.words[0]),
            mod : null
          } : "mod" === mode ? {
            div : null,
            mod : new BN(this.modn(num.words[0]))
          } : {
            div : this.divn(num.words[0]),
            mod : new BN(this.modn(num.words[0]))
          } : this._wordDiv(num, mode);
          var container;
          var mod;
          var res;
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.div = function(num) {
          return this.divmod(num, "div", false).div;
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.mod = function(num) {
          return this.divmod(num, "mod", false).mod;
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.umod = function(num) {
          return this.divmod(num, "mod", true).mod;
        };
        /**
         * @param {?} num
         * @return {?}
         */
        BN.prototype.divRound = function(num) {
          var dm = this.divmod(num);
          if (dm.mod.isZero()) {
            return dm.div;
          }
          var hashKey = 0 !== dm.div.negative ? dm.mod.isub(num) : dm.mod;
          var key = num.ushrn(1);
          num = num.andln(1);
          key = hashKey.cmp(key);
          return key < 0 || 1 === num && 0 === key ? dm.div : 0 !== dm.div.negative ? dm.div.isubn(1) : dm.div.iaddn(1);
        };
        /**
         * @param {number} num
         * @return {?}
         */
        BN.prototype.modn = function(num) {
          assert(num <= 67108863);
          /** @type {number} */
          var p = (1 << 26) % num;
          /** @type {number} */
          var acc = 0;
          /** @type {number} */
          var i = this.length - 1;
          for (; 0 <= i; i--) {
            /** @type {number} */
            acc = (p * acc + (0 | this.words[i])) % num;
          }
          return acc;
        };
        /**
         * @param {number} num
         * @return {?}
         */
        BN.prototype.idivn = function(num) {
          assert(num <= 67108863);
          /** @type {number} */
          var carry = 0;
          /** @type {number} */
          var i = this.length - 1;
          for (; 0 <= i; i--) {
            /** @type {number} */
            var w = (0 | this.words[i]) + 67108864 * carry;
            /** @type {number} */
            this.words[i] = w / num | 0;
            /** @type {number} */
            carry = w % num;
          }
          return this.strip();
        };
        /**
         * @param {undefined} value
         * @return {?}
         */
        BN.prototype.divn = function(value) {
          return this.clone().idivn(value);
        };
        /**
         * @param {!Object} p
         * @return {?}
         */
        BN.prototype.egcd = function(p) {
          assert(0 === p.negative);
          assert(!p.isZero());
          var x = this;
          var y = p.clone();
          x = 0 !== x.negative ? x.umod(p) : x.clone();
          var A = new BN(1);
          var B = new BN(0);
          var C = new BN(0);
          var D = new BN(1);
          /** @type {number} */
          var g = 0;
          for (; x.isEven() && y.isEven();) {
            x.iushrn(1);
            y.iushrn(1);
            ++g;
          }
          var yp = y.clone();
          var xp = x.clone();
          for (; !x.isZero();) {
            /** @type {number} */
            var i = 0;
            /** @type {number} */
            var res = 1;
            for (; 0 == (x.words[0] & res) && i < 26; ++i, res = res << 1) {
            }
            if (0 < i) {
              x.iushrn(i);
              for (; 0 < i--;) {
                if (A.isOdd() || B.isOdd()) {
                  A.iadd(yp);
                  B.isub(xp);
                }
                A.iushrn(1);
                B.iushrn(1);
              }
            }
            /** @type {number} */
            var j = 0;
            /** @type {number} */
            var k = 1;
            for (; 0 == (y.words[0] & k) && j < 26; ++j, k = k << 1) {
            }
            if (0 < j) {
              y.iushrn(j);
              for (; 0 < j--;) {
                if (C.isOdd() || D.isOdd()) {
                  C.iadd(yp);
                  D.isub(xp);
                }
                C.iushrn(1);
                D.iushrn(1);
              }
            }
            if (0 <= x.cmp(y)) {
              x.isub(y);
              A.isub(C);
              B.isub(D);
            } else {
              y.isub(x);
              C.isub(A);
              D.isub(B);
            }
          }
          return {
            a : C,
            b : D,
            gcd : y.iushln(g)
          };
        };
        /**
         * @param {!Object} p
         * @return {?}
         */
        BN.prototype._invmp = function(p) {
          assert(0 === p.negative);
          assert(!p.isZero());
          var frame;
          var a = this;
          var b = p.clone();
          a = 0 !== a.negative ? a.umod(p) : a.clone();
          var C = new BN(1);
          var A = new BN(0);
          var yp = b.clone();
          for (; 0 < a.cmpn(1) && 0 < b.cmpn(1);) {
            /** @type {number} */
            var j = 0;
            /** @type {number} */
            var k = 1;
            for (; 0 == (a.words[0] & k) && j < 26; ++j, k = k << 1) {
            }
            if (0 < j) {
              a.iushrn(j);
              for (; 0 < j--;) {
                if (C.isOdd()) {
                  C.iadd(yp);
                }
                C.iushrn(1);
              }
            }
            /** @type {number} */
            var i = 0;
            /** @type {number} */
            var res = 1;
            for (; 0 == (b.words[0] & res) && i < 26; ++i, res = res << 1) {
            }
            if (0 < i) {
              b.iushrn(i);
              for (; 0 < i--;) {
                if (A.isOdd()) {
                  A.iadd(yp);
                }
                A.iushrn(1);
              }
            }
            if (0 <= a.cmp(b)) {
              a.isub(b);
              C.isub(A);
            } else {
              b.isub(a);
              A.isub(C);
            }
          }
          return (frame = 0 === a.cmpn(1) ? C : A).cmpn(0) < 0 && frame.iadd(p), frame;
        };
        /**
         * @param {?} num
         * @return {?}
         */
        BN.prototype.gcd = function(num) {
          if (this.isZero()) {
            return num.abs();
          }
          if (num.isZero()) {
            return this.abs();
          }
          var a = this.clone();
          var b = num.clone();
          /** @type {number} */
          a.negative = 0;
          /** @type {number} */
          var shift = b.negative = 0;
          for (; a.isEven() && b.isEven(); shift++) {
            a.iushrn(1);
            b.iushrn(1);
          }
          for (;;) {
            for (; a.isEven();) {
              a.iushrn(1);
            }
            for (; b.isEven();) {
              b.iushrn(1);
            }
            var originalB = a.cmp(b);
            if (originalB < 0) {
              var bytes = a;
              a = b;
              b = bytes;
            } else {
              if (0 === originalB || 0 === b.cmpn(1)) {
                break;
              }
            }
            a.isub(b);
          }
          return b.iushln(shift);
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.invm = function(num) {
          return this.egcd(num).a.umod(num);
        };
        /**
         * @return {?}
         */
        BN.prototype.isEven = function() {
          return 0 == (1 & this.words[0]);
        };
        /**
         * @return {?}
         */
        BN.prototype.isOdd = function() {
          return 1 == (1 & this.words[0]);
        };
        /**
         * @param {number} num
         * @return {?}
         */
        BN.prototype.andln = function(num) {
          return this.words[0] & num;
        };
        /**
         * @param {number} s
         * @return {?}
         */
        BN.prototype.bincn = function(s) {
          assert("number" == typeof s);
          /** @type {number} */
          var q = s % 26;
          /** @type {number} */
          s = (s - q) / 26;
          /** @type {number} */
          q = 1 << q;
          if (this.length <= s) {
            return this._expand(1 + s), this.words[s] |= q, this;
          }
          /** @type {number} */
          var carry = q;
          /** @type {number} */
          var i = s;
          for (; 0 !== carry && i < this.length; i++) {
            /** @type {number} */
            var w = 0 | this.words[i];
            /** @type {number} */
            carry = (w = w + carry) >>> 26;
            /** @type {number} */
            w = w & 67108863;
            /** @type {number} */
            this.words[i] = w;
          }
          return 0 !== carry && (this.words[i] = carry, this.length++), this;
        };
        /**
         * @return {?}
         */
        BN.prototype.isZero = function() {
          return 1 === this.length && 0 === this.words[0];
        };
        /**
         * @param {number} num
         * @return {?}
         */
        BN.prototype.cmpn = function(num) {
          /** @type {boolean} */
          var is_negative = num < 0;
          return 0 === this.negative || is_negative ? 0 === this.negative && is_negative ? 1 : (this.strip(), num = 1 < this.length ? 1 : (assert((num = is_negative ? -num : num) <= 67108863, "Number is too big"), (is_negative = 0 | this.words[0]) === num ? 0 : is_negative < num ? -1 : 1), 0 !== this.negative ? 0 | -num : num) : -1;
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.cmp = function(num) {
          if (0 !== this.negative && 0 === num.negative) {
            return -1;
          }
          if (0 === this.negative && 0 !== num.negative) {
            return 1;
          }
          num = this.ucmp(num);
          return 0 !== this.negative ? 0 | -num : num;
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.ucmp = function(num) {
          if (this.length > num.length) {
            return 1;
          }
          if (this.length < num.length) {
            return -1;
          }
          /** @type {number} */
          var 1 = 0;
          /** @type {number} */
          var i = this.length - 1;
          for (; 0 <= i; i--) {
            /** @type {number} */
            var f = 0 | this.words[i];
            /** @type {number} */
            var g = 0 | num.words[i];
            if (f != g) {
              if (f < g) {
                /** @type {number} */
                1 = -1;
              } else {
                if (g < f) {
                  /** @type {number} */
                  1 = 1;
                }
              }
              break;
            }
          }
          return 1;
        };
        /**
         * @param {number} num
         * @return {?}
         */
        BN.prototype.gtn = function(num) {
          return 1 === this.cmpn(num);
        };
        /**
         * @param {!Object} value
         * @return {?}
         */
        BN.prototype.gt = function(value) {
          return 1 === this.cmp(value);
        };
        /**
         * @param {undefined} num
         * @return {?}
         */
        BN.prototype.gten = function(num) {
          return 0 <= this.cmpn(num);
        };
        /**
         * @param {!Object} value
         * @return {?}
         */
        BN.prototype.gte = function(value) {
          return 0 <= this.cmp(value);
        };
        /**
         * @param {undefined} num
         * @return {?}
         */
        BN.prototype.ltn = function(num) {
          return -1 === this.cmpn(num);
        };
        /**
         * @param {!Object} value
         * @return {?}
         */
        BN.prototype.lt = function(value) {
          return -1 === this.cmp(value);
        };
        /**
         * @param {undefined} num
         * @return {?}
         */
        BN.prototype.lten = function(num) {
          return this.cmpn(num) <= 0;
        };
        /**
         * @param {!Object} value
         * @return {?}
         */
        BN.prototype.lte = function(value) {
          return this.cmp(value) <= 0;
        };
        /**
         * @param {undefined} num
         * @return {?}
         */
        BN.prototype.eqn = function(num) {
          return 0 === this.cmpn(num);
        };
        /**
         * @param {!Object} value
         * @return {?}
         */
        BN.prototype.eq = function(value) {
          return 0 === this.cmp(value);
        };
        /**
         * @param {?} num
         * @return {?}
         */
        BN.red = function(num) {
          return new Red(num);
        };
        /**
         * @param {!Function} ctx
         * @return {?}
         */
        BN.prototype.toRed = function(ctx) {
          return assert(!this.red, "Already a number in reduction context"), assert(0 === this.negative, "red works only with positives"), ctx.convertTo(this)._forceRed(ctx);
        };
        /**
         * @return {?}
         */
        BN.prototype.fromRed = function() {
          return assert(this.red, "fromRed works only with numbers in reduction context"), this.red.convertFrom(this);
        };
        /**
         * @param {number} ctx
         * @return {?}
         */
        BN.prototype._forceRed = function(ctx) {
          return this.red = ctx, this;
        };
        /**
         * @param {undefined} ctx
         * @return {?}
         */
        BN.prototype.forceRed = function(ctx) {
          return assert(!this.red, "Already a number in reduction context"), this._forceRed(ctx);
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.redAdd = function(num) {
          return assert(this.red, "redAdd works only with red numbers"), this.red.add(this, num);
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.redIAdd = function(num) {
          return assert(this.red, "redIAdd works only with red numbers"), this.red.iadd(this, num);
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.redSub = function(num) {
          return assert(this.red, "redSub works only with red numbers"), this.red.sub(this, num);
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.redISub = function(num) {
          return assert(this.red, "redISub works only with red numbers"), this.red.isub(this, num);
        };
        /**
         * @param {undefined} num
         * @return {?}
         */
        BN.prototype.redShl = function(num) {
          return assert(this.red, "redShl works only with red numbers"), this.red.shl(this, num);
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.redMul = function(num) {
          return assert(this.red, "redMul works only with red numbers"), this.red._verify2(this, num), this.red.mul(this, num);
        };
        /**
         * @param {undefined} num
         * @return {?}
         */
        BN.prototype.redIMul = function(num) {
          return assert(this.red, "redMul works only with red numbers"), this.red._verify2(this, num), this.red.imul(this, num);
        };
        /**
         * @return {?}
         */
        BN.prototype.redSqr = function() {
          return assert(this.red, "redSqr works only with red numbers"), this.red._verify1(this), this.red.sqr(this);
        };
        /**
         * @return {?}
         */
        BN.prototype.redISqr = function() {
          return assert(this.red, "redISqr works only with red numbers"), this.red._verify1(this), this.red.isqr(this);
        };
        /**
         * @return {?}
         */
        BN.prototype.redSqrt = function() {
          return assert(this.red, "redSqrt works only with red numbers"), this.red._verify1(this), this.red.sqrt(this);
        };
        /**
         * @return {?}
         */
        BN.prototype.redInvm = function() {
          return assert(this.red, "redInvm works only with red numbers"), this.red._verify1(this), this.red.invm(this);
        };
        /**
         * @return {?}
         */
        BN.prototype.redNeg = function() {
          return assert(this.red, "redNeg works only with red numbers"), this.red._verify1(this), this.red.neg(this);
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        BN.prototype.redPow = function(num) {
          return assert(this.red && !num.red, "redPow(normalNum)"), this.red._verify1(this), this.red.pow(this, num);
        };
        var primes = {
          k256 : null,
          p224 : null,
          p192 : null,
          p25519 : null
        };
        /**
         * @return {?}
         */
        MPrime.prototype._tmp = function() {
          var c = new BN(null);
          return c.words = new Array(Math.ceil(this.n / 13)), c;
        };
        /**
         * @param {string} num
         * @return {?}
         */
        MPrime.prototype.ireduce = function(num) {
          var rlen;
          /** @type {string} */
          var r = num;
          for (; this.split(r, this.tmp), rlen = (r = (r = this.imulK(r)).iadd(this.tmp)).bitLength(), rlen > this.n;) {
          }
          num = rlen < this.n ? -1 : r.ucmp(this.p);
          return 0 === num ? (r.words[0] = 0, r.length = 1) : 0 < num ? r.isub(this.p) : void 0 !== r.strip ? r.strip() : r._strip(), r;
        };
        /**
         * @param {?} input
         * @param {!Object} out
         * @return {undefined}
         */
        MPrime.prototype.split = function(input, out) {
          input.iushrn(this.n, 0, out);
        };
        /**
         * @param {?} num
         * @return {?}
         */
        MPrime.prototype.imulK = function(num) {
          return num.imul(this.k);
        };
        inherits(K256, MPrime);
        /**
         * @param {!Object} node
         * @param {!Object} options
         * @return {?}
         */
        K256.prototype.split = function(node, options) {
          /** @type {number} */
          var length = Math.min(node.length, 9);
          /** @type {number} */
          var i = 0;
          for (; i < length; i++) {
            options.words[i] = node.words[i];
          }
          if (options.length = length, node.length <= 9) {
            return node.words[0] = 0, void(node.length = 1);
          }
          var a = node.words[9];
          /** @type {number} */
          options.words[options.length++] = 4194303 & a;
          /** @type {number} */
          i = 10;
          for (; i < node.length; i++) {
            /** @type {number} */
            var nativeObjectObject = 0 | node.words[i];
            /** @type {number} */
            node.words[i - 10] = (4194303 & nativeObjectObject) << 4 | a >>> 22;
            /** @type {number} */
            a = nativeObjectObject;
          }
          /** @type {number} */
          a = a >>> 22;
          if (0 === (node.words[i - 10] = a) && 10 < node.length) {
            node.length -= 10;
          } else {
            node.length -= 9;
          }
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        K256.prototype.imulK = function(num) {
          /** @type {number} */
          num.words[num.length] = 0;
          /** @type {number} */
          num.words[num.length + 1] = 0;
          num.length += 2;
          /** @type {number} */
          var maxN = 0;
          /** @type {number} */
          var i = 0;
          for (; i < num.length; i++) {
            /** @type {number} */
            var inc = 0 | num.words[i];
            /** @type {number} */
            maxN = maxN + 977 * inc;
            /** @type {number} */
            num.words[i] = 67108863 & maxN;
            /** @type {number} */
            maxN = 64 * inc + (maxN / 67108864 | 0);
          }
          return 0 === num.words[num.length - 1] && (num.length--, 0 === num.words[num.length - 1] && num.length--), num;
        };
        inherits(P224, MPrime);
        inherits(P192, MPrime);
        inherits(P25519, MPrime);
        /**
         * @param {!Object} num
         * @return {?}
         */
        P25519.prototype.imulK = function(num) {
          /** @type {number} */
          var longname = 0;
          /** @type {number} */
          var i = 0;
          for (; i < num.length; i++) {
            /** @type {number} */
            var carry = 19 * (0 | num.words[i]) + longname;
            /** @type {number} */
            var objectiveF = 67108863 & carry;
            /** @type {number} */
            carry = carry >>> 26;
            /** @type {number} */
            num.words[i] = objectiveF;
            /** @type {number} */
            longname = carry;
          }
          return 0 !== longname && (num.words[num.length++] = longname), num;
        };
        /**
         * @param {!Object} name
         * @return {?}
         */
        BN._prime = function(name) {
          if (primes[name]) {
            return primes[name];
          }
          var prime;
          if ("k256" === name) {
            prime = new K256;
          } else {
            if ("p224" === name) {
              prime = new P224;
            } else {
              if ("p192" === name) {
                prime = new P192;
              } else {
                if ("p25519" !== name) {
                  throw new Error("Unknown prime " + name);
                }
                prime = new P25519;
              }
            }
          }
          return primes[name] = prime;
        };
        /**
         * @param {!Object} a
         * @return {undefined}
         */
        Red.prototype._verify1 = function(a) {
          assert(0 === a.negative, "red works only with positives");
          assert(a.red, "red works only with red numbers");
        };
        /**
         * @param {!Object} a
         * @param {!Object} b
         * @return {undefined}
         */
        Red.prototype._verify2 = function(a, b) {
          assert(0 == (a.negative | b.negative), "red works only with positives");
          assert(a.red && a.red === b.red, "red works only with red numbers");
        };
        /**
         * @param {boolean} a
         * @return {?}
         */
        Red.prototype.imod = function(a) {
          return (this.prime ? this.prime.ireduce(a) : a.umod(this.m))._forceRed(this);
        };
        /**
         * @param {?} a
         * @return {?}
         */
        Red.prototype.neg = function(a) {
          return a.isZero() ? a.clone() : this.m.sub(a)._forceRed(this);
        };
        /**
         * @param {!Object} b
         * @param {!Object} a
         * @return {?}
         */
        Red.prototype.add = function(b, a) {
          this._verify2(b, a);
          a = b.add(a);
          return 0 <= a.cmp(this.m) && a.isub(this.m), a._forceRed(this);
        };
        /**
         * @param {!Object} a
         * @param {!Object} b
         * @return {?}
         */
        Red.prototype.iadd = function(a, b) {
          this._verify2(a, b);
          b = a.iadd(b);
          return 0 <= b.cmp(this.m) && b.isub(this.m), b;
        };
        /**
         * @param {!Object} a
         * @param {!Object} res
         * @return {?}
         */
        Red.prototype.sub = function(a, res) {
          this._verify2(a, res);
          res = a.sub(res);
          return res.cmpn(0) < 0 && res.iadd(this.m), res._forceRed(this);
        };
        /**
         * @param {!Object} a
         * @param {!Object} num
         * @return {?}
         */
        Red.prototype.isub = function(a, num) {
          this._verify2(a, num);
          num = a.isub(num);
          return num.cmpn(0) < 0 && num.iadd(this.m), num;
        };
        /**
         * @param {!Object} a
         * @param {number} num
         * @return {?}
         */
        Red.prototype.shl = function(a, num) {
          return this._verify1(a), this.imod(a.ushln(num));
        };
        /**
         * @param {number} a
         * @param {number} b
         * @return {?}
         */
        Red.prototype.imul = function(a, b) {
          return this._verify2(a, b), this.imod(a.imul(b));
        };
        /**
         * @param {string} a
         * @param {boolean} b
         * @return {?}
         */
        Red.prototype.mul = function(a, b) {
          return this._verify2(a, b), this.imod(a.mul(b));
        };
        /**
         * @param {!Array} a
         * @return {?}
         */
        Red.prototype.isqr = function(a) {
          return this.imul(a, a.clone());
        };
        /**
         * @param {undefined} a
         * @return {?}
         */
        Red.prototype.sqr = function(a) {
          return this.mul(a, a);
        };
        /**
         * @param {?} a
         * @return {?}
         */
        Red.prototype.sqrt = function(a) {
          if (a.isZero()) {
            return a.clone();
          }
          var pow = this.m.andln(3);
          if (assert(pow % 2 == 1), 3 === pow) {
            pow = this.m.add(new BN(1)).iushrn(2);
            return this.pow(a, pow);
          }
          var q = this.m.subn(1);
          /** @type {number} */
          var listHeight = 0;
          for (; !q.isZero() && 0 === q.andln(1);) {
            listHeight++;
            q.iushrn(1);
          }
          assert(!q.isZero());
          var one = (new BN(1)).toRed(this);
          var nOne = one.redNeg();
          var lpow = this.m.subn(1).iushrn(1);
          var z = (new BN(2 * (z = this.m.bitLength()) * z)).toRed(this);
          for (; 0 !== this.pow(z, lpow).cmp(nOne);) {
            z.redIAdd(nOne);
          }
          var c = this.pow(z, q);
          var r = this.pow(a, q.addn(1).iushrn(1));
          var t = this.pow(a, q);
          /** @type {number} */
          var clientHeight = listHeight;
          for (; 0 !== t.cmp(one);) {
            var tmp = t;
            /** @type {number} */
            var targetOffsetHeight = 0;
            for (; 0 !== tmp.cmp(one); targetOffsetHeight++) {
              tmp = tmp.redSqr();
            }
            assert(targetOffsetHeight < clientHeight);
            var b = this.pow(c, (new BN(1)).iushln(clientHeight - targetOffsetHeight - 1));
            r = r.redMul(b);
            c = b.redSqr();
            t = t.redMul(c);
            /** @type {number} */
            clientHeight = targetOffsetHeight;
          }
          return r;
        };
        /**
         * @param {!Object} a
         * @return {?}
         */
        Red.prototype.invm = function(a) {
          a = a._invmp(this.m);
          return 0 !== a.negative ? (a.negative = 0, this.imod(a).redNeg()) : this.imod(a);
        };
        /**
         * @param {!Array} a
         * @param {!Object} num
         * @return {?}
         */
        Red.prototype.pow = function(a, num) {
          if (num.isZero()) {
            return (new BN(1)).toRed(this);
          }
          if (0 === num.cmpn(1)) {
            return a.clone();
          }
          /** @type {!Array} */
          var wnd = new Array(16);
          wnd[0] = (new BN(1)).toRed(this);
          /** @type {!Array} */
          wnd[1] = a;
          /** @type {number} */
          var i = 2;
          for (; i < wnd.length; i++) {
            wnd[i] = this.mul(wnd[i - 1], a);
          }
          var res = wnd[0];
          /** @type {number} */
          var current = 0;
          /** @type {number} */
          var furthest = 0;
          /** @type {number} */
          var BOARD_ROWS = num.bitLength() % 26;
          if (0 === BOARD_ROWS) {
            /** @type {number} */
            BOARD_ROWS = 26;
          }
          /** @type {number} */
          i = num.length - 1;
          for (; 0 <= i; i--) {
            var k = num.words[i];
            /** @type {number} */
            var j = BOARD_ROWS - 1;
            for (; 0 <= j; j--) {
              /** @type {number} */
              var currentBit = k >> j & 1;
              if (res !== wnd[0]) {
                res = this.sqr(res);
              }
              if (0 != currentBit || 0 !== current) {
                /** @type {number} */
                current = current << 1;
                /** @type {number} */
                current = current | currentBit;
                if (4 === ++furthest || 0 === i && 0 === j) {
                  res = this.mul(res, wnd[current]);
                  /** @type {number} */
                  current = furthest = 0;
                }
              } else {
                /** @type {number} */
                furthest = 0;
              }
            }
            /** @type {number} */
            BOARD_ROWS = 26;
          }
          return res;
        };
        /**
         * @param {?} num
         * @return {?}
         */
        Red.prototype.convertTo = function(num) {
          var r = num.umod(this.m);
          return r === num ? r.clone() : r;
        };
        /**
         * @param {!Object} num
         * @return {?}
         */
        Red.prototype.convertFrom = function(num) {
          num = num.clone();
          return num.red = null, num;
        };
        /**
         * @param {number} num
         * @return {?}
         */
        BN.mont = function(num) {
          return new Mont(num);
        };
        inherits(Mont, Red);
        /**
         * @param {?} num
         * @return {?}
         */
        Mont.prototype.convertTo = function(num) {
          return this.imod(num.ushln(this.shift));
        };
        /**
         * @param {?} num
         * @return {?}
         */
        Mont.prototype.convertFrom = function(num) {
          num = this.imod(num.mul(this.rinv));
          return num.red = null, num;
        };
        /**
         * @param {number} a
         * @param {number} b
         * @return {?}
         */
        Mont.prototype.imul = function(a, b) {
          if (a.isZero() || b.isZero()) {
            return a.words[0] = 0, a.length = 1, a;
          }
          a = a.imul(b);
          b = a.maskn(this.shift).mul(this.minv).imaskn(this.shift).mul(this.m);
          a = a.isub(b).iushrn(this.shift);
          /** @type {number} */
          b = a;
          return 0 <= a.cmp(this.m) ? b = a.isub(this.m) : a.cmpn(0) < 0 && (b = a.iadd(this.m)), b._forceRed(this);
        };
        /**
         * @param {number} a
         * @param {number} b
         * @return {?}
         */
        Mont.prototype.mul = function(a, b) {
          if (a.isZero() || b.isZero()) {
            return (new BN(0))._forceRed(this);
          }
          a = a.mul(b);
          b = a.maskn(this.shift).mul(this.minv).imaskn(this.shift).mul(this.m);
          a = a.isub(b).iushrn(this.shift);
          /** @type {number} */
          b = a;
          return 0 <= a.cmp(this.m) ? b = a.isub(this.m) : a.cmpn(0) < 0 && (b = a.iadd(this.m)), b._forceRed(this);
        };
        /**
         * @param {?} a
         * @return {?}
         */
        Mont.prototype.invm = function(a) {
          return this.imod(a._invmp(this.m).mul(this.r2))._forceRed(this);
        };
      }(void 0 === canCreateDiscussions || canCreateDiscussions, this);
    }, {
      buffer : 18
    }],
    17 : [function(require, mixin, i) {
      /**
       * @param {(number|string)} canCreateDiscussions
       * @return {undefined}
       */
      function Bledroid(canCreateDiscussions) {
        /** @type {(number|string)} */
        this.rand = canCreateDiscussions;
      }
      var bledroid;
      if (mixin.exports = function(count) {
        return (bledroid = bledroid || new Bledroid(null)).generate(count);
      }, (mixin.exports.Rand = Bledroid).prototype.generate = function(value) {
        return this._rand(value);
      }, Bledroid.prototype._rand = function(num) {
        if (this.rand.getBytes) {
          return this.rand.getBytes(num);
        }
        /** @type {!Uint8Array} */
        var arr = new Uint8Array(num);
        /** @type {number} */
        var i = 0;
        for (; i < arr.length; i++) {
          arr[i] = this.rand.getByte();
        }
        return arr;
      }, "object" == typeof self) {
        if (self.crypto && self.crypto.getRandomValues) {
          /**
           * @param {!Array} array
           * @return {?}
           */
          Bledroid.prototype._rand = function(array) {
            /** @type {!Uint8Array} */
            array = new Uint8Array(array);
            return self.crypto.getRandomValues(array), array;
          };
        } else {
          if (self.msCrypto && self.msCrypto.getRandomValues) {
            /**
             * @param {!Array} array
             * @return {?}
             */
            Bledroid.prototype._rand = function(array) {
              /** @type {!Uint8Array} */
              array = new Uint8Array(array);
              return self.msCrypto.getRandomValues(array), array;
            };
          } else {
            if ("object" == typeof window) {
              /**
               * @return {?}
               */
              Bledroid.prototype._rand = function() {
                throw new Error("Not implemented yet");
              };
            }
          }
        }
      } else {
        try {
          var crypto = require("crypto");
          if ("function" != typeof crypto.randomBytes) {
            throw new Error("Not supported");
          }
          /**
           * @param {!Array} bytes
           * @return {?}
           */
          Bledroid.prototype._rand = function(bytes) {
            return crypto.randomBytes(bytes);
          };
        } catch (e) {
        }
      }
    }, {
      crypto : 18
    }],
    18 : [function(canCreateDiscussions, isSlidingUp, i) {
    }, {}],
    19 : [function(require, canCreateDiscussions, forge) {
      forge.utils = require("./hash/utils");
      forge.common = require("./hash/common");
      forge.sha = require("./hash/sha");
      forge.ripemd = require("./hash/ripemd");
      forge.hmac = require("./hash/hmac");
      forge.sha1 = forge.sha.sha1;
      forge.sha256 = forge.sha.sha256;
      forge.sha224 = forge.sha.sha224;
      forge.sha384 = forge.sha.sha384;
      forge.sha512 = forge.sha.sha512;
      forge.ripemd160 = forge.ripemd.ripemd160;
    }, {
      "./hash/common" : 20,
      "./hash/hmac" : 21,
      "./hash/ripemd" : 22,
      "./hash/sha" : 23,
      "./hash/utils" : 30
    }],
    20 : [function(require, canCreateDiscussions, ownerContext) {
      /**
       * @return {undefined}
       */
      function pad() {
        /** @type {null} */
        this.pending = null;
        /** @type {number} */
        this.pendingTotal = 0;
        this.blockSize = this.constructor.blockSize;
        this.outSize = this.constructor.outSize;
        this.hmacStrength = this.constructor.hmacStrength;
        /** @type {number} */
        this.padLength = this.constructor.padLength / 8;
        /** @type {string} */
        this.endian = "big";
        /** @type {number} */
        this._delta8 = this.blockSize / 8;
        /** @type {number} */
        this._delta32 = this.blockSize / 32;
      }
      var args = require("./utils");
      var assert = require("minimalistic-assert");
      /**
       * @param {string} msg
       * @param {string} len
       * @return {?}
       */
      (ownerContext.BlockHash = pad).prototype.update = function(msg, len) {
        if (msg = args.toArray(msg, len), this.pending ? this.pending = this.pending.concat(msg) : this.pending = msg, this.pendingTotal += msg.length, this.pending.length >= this._delta8) {
          /** @type {number} */
          len = (msg = this.pending).length % this._delta8;
          this.pending = msg.slice(msg.length - len, msg.length);
          if (0 === this.pending.length) {
            /** @type {null} */
            this.pending = null;
          }
          msg = args.join32(msg, 0, msg.length - len, this.endian);
          /** @type {number} */
          var i = 0;
          for (; i < msg.length; i = i + this._delta32) {
            this._update(msg, i, i + this._delta32);
          }
        }
        return this;
      };
      /**
       * @param {string} pid_key
       * @return {?}
       */
      pad.prototype.digest = function(pid_key) {
        return this.update(this._pad()), assert(null === this.pending), this._digest(pid_key);
      };
      /**
       * @return {?}
       */
      pad.prototype._pad = function() {
        var len = this.pendingTotal;
        var bytes = this._delta8;
        /** @type {number} */
        var k = bytes - (len + this.padLength) % bytes;
        /** @type {!Array} */
        var result = new Array(k + this.padLength);
        /** @type {number} */
        result[0] = 128;
        /** @type {number} */
        var j = 1;
        for (; j < k; j++) {
          /** @type {number} */
          result[j] = 0;
        }
        if (len = len << 3, "big" === this.endian) {
          /** @type {number} */
          var t = 8;
          for (; t < this.padLength; t++) {
            /** @type {number} */
            result[j++] = 0;
          }
          /** @type {number} */
          result[j++] = 0;
          /** @type {number} */
          result[j++] = 0;
          /** @type {number} */
          result[j++] = 0;
          /** @type {number} */
          result[j++] = 0;
          /** @type {number} */
          result[j++] = len >>> 24 & 255;
          /** @type {number} */
          result[j++] = len >>> 16 & 255;
          /** @type {number} */
          result[j++] = len >>> 8 & 255;
          /** @type {number} */
          result[j++] = 255 & len;
        } else {
          /** @type {number} */
          result[j++] = 255 & len;
          /** @type {number} */
          result[j++] = len >>> 8 & 255;
          /** @type {number} */
          result[j++] = len >>> 16 & 255;
          /** @type {number} */
          result[j++] = len >>> 24 & 255;
          /** @type {number} */
          result[j++] = 0;
          /** @type {number} */
          result[j++] = 0;
          /** @type {number} */
          result[j++] = 0;
          /** @type {number} */
          result[j++] = 0;
          /** @type {number} */
          t = 8;
          for (; t < this.padLength; t++) {
            /** @type {number} */
            result[j++] = 0;
          }
        }
        return result;
      };
    }, {
      "./utils" : 30,
      "minimalistic-assert" : 33
    }],
    21 : [function(require, module, i) {
      /**
       * @param {!Object} hash
       * @param {string} key
       * @param {boolean} enc
       * @return {?}
       */
      function Hmac(hash, key, enc) {
        if (!(this instanceof Hmac)) {
          return new Hmac(hash, key, enc);
        }
        /** @type {!Object} */
        this.Hash = hash;
        /** @type {number} */
        this.blockSize = hash.blockSize / 8;
        /** @type {number} */
        this.outSize = hash.outSize / 8;
        /** @type {null} */
        this.inner = null;
        /** @type {null} */
        this.outer = null;
        this._init(List.toArray(key, enc));
      }
      var List = require("./utils");
      var assert = require("minimalistic-assert");
      /**
       * @param {string} key
       * @return {undefined}
       */
      (module.exports = Hmac).prototype._init = function(key) {
        if (key.length > this.blockSize) {
          key = (new this.Hash).update(key).digest();
        }
        assert(key.length <= this.blockSize);
        var i = key.length;
        for (; i < this.blockSize; i++) {
          key.push(0);
        }
        /** @type {number} */
        i = 0;
        for (; i < key.length; i++) {
          key[i] ^= 54;
        }
        this.inner = (new this.Hash).update(key);
        /** @type {number} */
        i = 0;
        for (; i < key.length; i++) {
          key[i] ^= 106;
        }
        this.outer = (new this.Hash).update(key);
      };
      /**
       * @param {string} type
       * @param {string} options
       * @return {?}
       */
      Hmac.prototype.update = function(type, options) {
        return this.inner.update(type, options), this;
      };
      /**
       * @param {string} enc
       * @return {?}
       */
      Hmac.prototype.digest = function(enc) {
        return this.outer.update(this.inner.digest()), this.outer.digest(enc);
      };
    }, {
      "./utils" : 30,
      "minimalistic-assert" : 33
    }],
    22 : [function(definition, canCreateDiscussions, options) {
      /**
       * @return {?}
       */
      function Slider() {
        if (!(this instanceof Slider)) {
          return new Slider;
        }
        proto.call(this);
        /** @type {!Array} */
        this.h = [1732584193, 4023233417, 2562383102, 271733878, 3285377520];
        /** @type {string} */
        this.endian = "little";
      }
      /**
       * @param {number} story
       * @param {number} e
       * @param {number} a
       * @param {number} b
       * @return {?}
       */
      function add(story, e, a, b) {
        return story <= 15 ? e ^ a ^ b : story <= 31 ? e & a | ~e & b : story <= 47 ? (e | ~a) ^ b : story <= 63 ? e & b | a & ~b : e ^ (a | ~b);
      }
      var self = definition("./utils");
      definition = definition("./common");
      var $ = self.rotl32;
      var getDomainObject = self.sum32;
      var filter = self.sum32_3;
      var equal = self.sum32_4;
      var proto = definition.BlockHash;
      self.inherits(Slider, proto);
      /** @type {number} */
      (options.ripemd160 = Slider).blockSize = 512;
      /** @type {number} */
      Slider.outSize = 160;
      /** @type {number} */
      Slider.hmacStrength = 192;
      /** @type {number} */
      Slider.padLength = 64;
      /**
       * @param {string} args
       * @param {number} l
       * @return {undefined}
       */
      Slider.prototype._update = function(args, l) {
        var QClass;
        var id = r = this.h[0];
        var c = i = this.h[1];
        var f = s = this.h[2];
        var g = n = this.h[3];
        var key = name = this.h[4];
        /** @type {number} */
        var obj = 0;
        for (; obj < 80; obj++) {
          var value = getDomainObject($(equal(r, add(obj, i, s, n), args[b[obj] + l], (QClass = obj) <= 15 ? 0 : QClass <= 31 ? 1518500249 : QClass <= 47 ? 1859775393 : QClass <= 63 ? 2400959708 : 2840853838), group[obj]), name);
          var r = name;
          var name = n;
          var n = $(s, 10);
          var s = i;
          var i = value;
          value = getDomainObject($(equal(id, add(79 - obj, c, f, g), args[e[obj] + l], (QClass = obj) <= 15 ? 1352829926 : QClass <= 31 ? 1548603684 : QClass <= 47 ? 1836072691 : QClass <= 63 ? 2053994217 : 0), elem[obj]), key);
          id = key;
          key = g;
          g = $(f, 10);
          f = c;
          c = value;
        }
        value = filter(this.h[1], s, g);
        this.h[1] = filter(this.h[2], n, key);
        this.h[2] = filter(this.h[3], name, id);
        this.h[3] = filter(this.h[4], r, c);
        this.h[4] = filter(this.h[0], i, f);
        this.h[0] = value;
      };
      /**
       * @param {string} hex2
       * @return {?}
       */
      Slider.prototype._digest = function(hex2) {
        return "hex" === hex2 ? self.toHex32(this.h, "little") : self.split32(this.h, "little");
      };
      /** @type {!Array} */
      var b = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 7, 4, 13, 1, 10, 6, 15, 3, 12, 0, 9, 5, 2, 14, 11, 8, 3, 10, 14, 4, 9, 15, 8, 1, 2, 7, 0, 6, 13, 11, 5, 12, 1, 9, 11, 10, 0, 8, 12, 4, 13, 3, 7, 15, 14, 5, 6, 2, 4, 0, 5, 9, 7, 12, 2, 10, 14, 1, 3, 8, 11, 6, 15, 13];
      /** @type {!Array} */
      var e = [5, 14, 7, 0, 9, 2, 11, 4, 13, 6, 15, 8, 1, 10, 3, 12, 6, 11, 3, 7, 0, 13, 5, 10, 14, 15, 8, 12, 4, 9, 1, 2, 15, 5, 1, 3, 7, 14, 6, 9, 11, 8, 12, 2, 10, 0, 4, 13, 8, 6, 4, 1, 3, 11, 15, 0, 5, 12, 2, 13, 9, 7, 10, 14, 12, 15, 10, 4, 1, 5, 8, 7, 6, 2, 13, 14, 0, 3, 9, 11];
      /** @type {!Array} */
      var group = [11, 14, 15, 12, 5, 8, 7, 9, 11, 13, 14, 15, 6, 7, 9, 8, 7, 6, 8, 13, 11, 9, 7, 15, 7, 12, 15, 9, 11, 7, 13, 12, 11, 13, 6, 7, 14, 9, 13, 15, 14, 8, 13, 6, 5, 12, 7, 5, 11, 12, 14, 15, 14, 15, 9, 8, 9, 14, 5, 6, 8, 6, 5, 12, 9, 15, 5, 11, 6, 8, 13, 12, 5, 12, 13, 14, 11, 8, 5, 6];
      /** @type {!Array} */
      var elem = [8, 9, 9, 11, 13, 15, 15, 5, 7, 7, 8, 11, 14, 14, 12, 6, 9, 13, 15, 7, 12, 8, 9, 11, 7, 7, 12, 7, 6, 15, 13, 11, 9, 7, 15, 11, 8, 6, 6, 14, 12, 13, 5, 14, 13, 13, 7, 5, 15, 5, 8, 11, 14, 14, 6, 14, 6, 9, 12, 9, 12, 5, 15, 8, 8, 5, 12, 9, 12, 5, 14, 6, 8, 13, 6, 5, 15, 13, 11, 11];
    }, {
      "./common" : 20,
      "./utils" : 30
    }],
    23 : [function(saveNotifs, canCreateDiscussions, hash_fns) {
      hash_fns.sha1 = saveNotifs("./sha/1");
      hash_fns.sha224 = saveNotifs("./sha/224");
      hash_fns.sha256 = saveNotifs("./sha/256");
      hash_fns.sha384 = saveNotifs("./sha/384");
      hash_fns.sha512 = saveNotifs("./sha/512");
    }, {
      "./sha/1" : 24,
      "./sha/224" : 25,
      "./sha/256" : 26,
      "./sha/384" : 27,
      "./sha/512" : 28
    }],
    24 : [function(f, module, i) {
      /**
       * @return {?}
       */
      function Slider() {
        if (!(this instanceof Slider)) {
          return new Slider;
        }
        Inherited.call(this);
        /** @type {!Array} */
        this.h = [1732584193, 4023233417, 2562383102, 271733878, 3285377520];
        /** @type {!Array} */
        this.W = new Array(80);
      }
      var utils = f("../utils");
      var message = f("../common");
      f = f("./common");
      var parse = utils.rotl32;
      var func = utils.sum32;
      var callback = utils.sum32_5;
      var d = f.ft_1;
      var Inherited = message.BlockHash;
      /** @type {!Array} */
      var tree = [1518500249, 1859775393, 2400959708, 3395469782];
      utils.inherits(Slider, Inherited);
      /** @type {number} */
      (module.exports = Slider).blockSize = 512;
      /** @type {number} */
      Slider.outSize = 160;
      /** @type {number} */
      Slider.hmacStrength = 80;
      /** @type {number} */
      Slider.padLength = 64;
      /**
       * @param {string} data
       * @param {number} key
       * @return {undefined}
       */
      Slider.prototype._update = function(data, key) {
        var buffer = this.W;
        /** @type {number} */
        var i = 0;
        for (; i < 16; i++) {
          buffer[i] = data[key + i];
        }
        for (; i < buffer.length; i++) {
          buffer[i] = parse(buffer[i - 3] ^ buffer[i - 8] ^ buffer[i - 14] ^ buffer[i - 16], 1);
        }
        var a = this.h[0];
        var b = this.h[1];
        var n = this.h[2];
        var value = this.h[3];
        var key = this.h[4];
        /** @type {number} */
        i = 0;
        for (; i < buffer.length; i++) {
          /** @type {number} */
          var r = ~~(i / 20);
          r = callback(parse(a, 5), d(r, b, n, value), key, buffer[i], tree[r]);
          key = value;
          value = n;
          n = parse(b, 30);
          b = a;
          a = r;
        }
        this.h[0] = func(this.h[0], a);
        this.h[1] = func(this.h[1], b);
        this.h[2] = func(this.h[2], n);
        this.h[3] = func(this.h[3], value);
        this.h[4] = func(this.h[4], key);
      };
      /**
       * @param {string} hex2
       * @return {?}
       */
      Slider.prototype._digest = function(hex2) {
        return "hex" === hex2 ? utils.toHex32(this.h, "big") : utils.split32(this.h, "big");
      };
    }, {
      "../common" : 20,
      "../utils" : 30,
      "./common" : 29
    }],
    25 : [function(require, module, i) {
      /**
       * @return {?}
       */
      function Config() {
        if (!(this instanceof Config)) {
          return new Config;
        }
        WS.call(this);
        /** @type {!Array} */
        this.h = [3238371032, 914150663, 812702999, 4144912697, 4290775857, 1750603025, 1694076839, 3204075428];
      }
      var utils = require("../utils");
      var WS = require("./256");
      utils.inherits(Config, WS);
      /** @type {number} */
      (module.exports = Config).blockSize = 512;
      /** @type {number} */
      Config.outSize = 224;
      /** @type {number} */
      Config.hmacStrength = 192;
      /** @type {number} */
      Config.padLength = 64;
      /**
       * @param {string} hex2
       * @return {?}
       */
      Config.prototype._digest = function(hex2) {
        return "hex" === hex2 ? utils.toHex32(this.h.slice(0, 7), "big") : utils.split32(this.h.slice(0, 7), "big");
      };
    }, {
      "../utils" : 30,
      "./256" : 26
    }],
    26 : [function(require, module, i) {
      /**
       * @return {?}
       */
      function Slider() {
        if (!(this instanceof Slider)) {
          return new Slider;
        }
        Inherited.call(this);
        /** @type {!Array} */
        this.h = [1779033703, 3144134277, 1013904242, 2773480762, 1359893119, 2600822924, 528734635, 1541459225];
        /** @type {!Array} */
        this.k = k;
        /** @type {!Array} */
        this.W = new Array(64);
      }
      var utils = require("../utils");
      var TagHourlyStat = require("../common");
      var self = require("./common");
      var flow = require("minimalistic-assert");
      var next = utils.sum32;
      var parse = utils.sum32_4;
      var callback = utils.sum32_5;
      var $ = self.ch32;
      var cb = self.maj32;
      var f = self.s0_256;
      var parseInt = self.s1_256;
      var _ = self.g0_256;
      var template = self.g1_256;
      var Inherited = TagHourlyStat.BlockHash;
      /** @type {!Array} */
      var k = [1116352408, 1899447441, 3049323471, 3921009573, 961987163, 1508970993, 2453635748, 2870763221, 3624381080, 310598401, 607225278, 1426881987, 1925078388, 2162078206, 2614888103, 3248222580, 3835390401, 4022224774, 264347078, 604807628, 770255983, 1249150122, 1555081692, 1996064986, 2554220882, 2821834349, 2952996808, 3210313671, 3336571891, 3584528711, 113926993, 338241895, 666307205, 773529912, 1294757372, 1396182291, 1695183700, 1986661051, 2177026350, 2456956037, 2730485921, 2820302411, 
      3259730800, 3345764771, 3516065817, 3600352804, 4094571909, 275423344, 430227734, 506948616, 659060556, 883997877, 958139571, 1322822218, 1537002063, 1747873779, 1955562222, 2024104815, 2227730452, 2361852424, 2428436474, 2756734187, 3204031479, 3329325298];
      utils.inherits(Slider, Inherited);
      /** @type {number} */
      (module.exports = Slider).blockSize = 512;
      /** @type {number} */
      Slider.outSize = 256;
      /** @type {number} */
      Slider.hmacStrength = 192;
      /** @type {number} */
      Slider.padLength = 64;
      /**
       * @param {string} item
       * @param {number} index
       * @return {undefined}
       */
      Slider.prototype._update = function(item, index) {
        var obj = this.W;
        /** @type {number} */
        var i = 0;
        for (; i < 16; i++) {
          obj[i] = item[index + i];
        }
        for (; i < obj.length; i++) {
          obj[i] = parse(template(obj[i - 2]), obj[i - 7], _(obj[i - 15]), obj[i - 16]);
        }
        var b = this.h[0];
        var m = this.h[1];
        var url = this.h[2];
        var a = this.h[3];
        var res = this.h[4];
        var result = this.h[5];
        var options = this.h[6];
        var o = this.h[7];
        flow(this.k.length === obj.length);
        /** @type {number} */
        i = 0;
        for (; i < obj.length; i++) {
          var r = callback(o, parseInt(res), $(res, result, options), this.k[i], obj[i]);
          var len = next(f(b), cb(b, m, url));
          o = options;
          options = result;
          result = res;
          res = next(a, r);
          a = url;
          url = m;
          m = b;
          b = next(r, len);
        }
        this.h[0] = next(this.h[0], b);
        this.h[1] = next(this.h[1], m);
        this.h[2] = next(this.h[2], url);
        this.h[3] = next(this.h[3], a);
        this.h[4] = next(this.h[4], res);
        this.h[5] = next(this.h[5], result);
        this.h[6] = next(this.h[6], options);
        this.h[7] = next(this.h[7], o);
      };
      /**
       * @param {string} hex2
       * @return {?}
       */
      Slider.prototype._digest = function(hex2) {
        return "hex" === hex2 ? utils.toHex32(this.h, "big") : utils.split32(this.h, "big");
      };
    }, {
      "../common" : 20,
      "../utils" : 30,
      "./common" : 29,
      "minimalistic-assert" : 33
    }],
    27 : [function(require, module, i) {
      /**
       * @return {?}
       */
      function Config() {
        if (!(this instanceof Config)) {
          return new Config;
        }
        WS.call(this);
        /** @type {!Array} */
        this.h = [3418070365, 3238371032, 1654270250, 914150663, 2438529370, 812702999, 355462360, 4144912697, 1731405415, 4290775857, 2394180231, 1750603025, 3675008525, 1694076839, 1203062813, 3204075428];
      }
      var utils = require("../utils");
      var WS = require("./512");
      utils.inherits(Config, WS);
      /** @type {number} */
      (module.exports = Config).blockSize = 1024;
      /** @type {number} */
      Config.outSize = 384;
      /** @type {number} */
      Config.hmacStrength = 192;
      /** @type {number} */
      Config.padLength = 128;
      /**
       * @param {string} hex2
       * @return {?}
       */
      Config.prototype._digest = function(hex2) {
        return "hex" === hex2 ? utils.toHex32(this.h.slice(0, 12), "big") : utils.split32(this.h.slice(0, 12), "big");
      };
    }, {
      "../utils" : 30,
      "./512" : 28
    }],
    28 : [function(require, module, i) {
      /**
       * @return {?}
       */
      function Slider() {
        if (!(this instanceof Slider)) {
          return new Slider;
        }
        Mirror.call(this);
        /** @type {!Array} */
        this.h = [1779033703, 4089235720, 3144134277, 2227873595, 1013904242, 4271175723, 2773480762, 1595750129, 1359893119, 2917565137, 2600822924, 725511199, 528734635, 4215389547, 1541459225, 327033209];
        /** @type {!Array} */
        this.k = k;
        /** @type {!Array} */
        this.W = new Array(160);
      }
      var options = require("../utils");
      var TagHourlyStat = require("../common");
      var flow = require("minimalistic-assert");
      var $ = options.rotr64_hi;
      var map = options.rotr64_lo;
      var i4mul = options.shr64_hi;
      var translate = options.shr64_lo;
      var callback = options.sum64;
      var f = options.sum64_hi;
      var merge = options.sum64_lo;
      var render = options.sum64_4_hi;
      var func = options.sum64_4_lo;
      var debug = options.sum64_5_hi;
      var log = options.sum64_5_lo;
      var Mirror = TagHourlyStat.BlockHash;
      /** @type {!Array} */
      var k = [1116352408, 3609767458, 1899447441, 602891725, 3049323471, 3964484399, 3921009573, 2173295548, 961987163, 4081628472, 1508970993, 3053834265, 2453635748, 2937671579, 2870763221, 3664609560, 3624381080, 2734883394, 310598401, 1164996542, 607225278, 1323610764, 1426881987, 3590304994, 1925078388, 4068182383, 2162078206, 991336113, 2614888103, 633803317, 3248222580, 3479774868, 3835390401, 2666613458, 4022224774, 944711139, 264347078, 2341262773, 604807628, 2007800933, 770255983, 1495990901, 
      1249150122, 1856431235, 1555081692, 3175218132, 1996064986, 2198950837, 2554220882, 3999719339, 2821834349, 766784016, 2952996808, 2566594879, 3210313671, 3203337956, 3336571891, 1034457026, 3584528711, 2466948901, 113926993, 3758326383, 338241895, 168717936, 666307205, 1188179964, 773529912, 1546045734, 1294757372, 1522805485, 1396182291, 2643833823, 1695183700, 2343527390, 1986661051, 1014477480, 2177026350, 1206759142, 2456956037, 344077627, 2730485921, 1290863460, 2820302411, 3158454273, 
      3259730800, 3505952657, 3345764771, 106217008, 3516065817, 3606008344, 3600352804, 1432725776, 4094571909, 1467031594, 275423344, 851169720, 430227734, 3100823752, 506948616, 1363258195, 659060556, 3750685593, 883997877, 3785050280, 958139571, 3318307427, 1322822218, 3812723403, 1537002063, 2003034995, 1747873779, 3602036899, 1955562222, 1575990012, 2024104815, 1125592928, 2227730452, 2716904306, 2361852424, 442776044, 2428436474, 593698344, 2756734187, 3733110249, 3204031479, 2999351573, 3329325298, 
      3815920427, 3391569614, 3928383900, 3515267271, 566280711, 3940187606, 3454069534, 4118630271, 4000239992, 116418474, 1914138554, 174292421, 2731055270, 289380356, 3203993006, 460393269, 320620315, 685471733, 587496836, 852142971, 1086792851, 1017036298, 365543100, 1126000580, 2618297676, 1288033470, 3409855158, 1501505948, 4234509866, 1607167915, 987167468, 1816402316, 1246189591];
      options.inherits(Slider, Mirror);
      /** @type {number} */
      (module.exports = Slider).blockSize = 1024;
      /** @type {number} */
      Slider.outSize = 512;
      /** @type {number} */
      Slider.hmacStrength = 192;
      /** @type {number} */
      Slider.padLength = 128;
      /**
       * @param {string} data
       * @param {number} type
       * @return {undefined}
       */
      Slider.prototype._prepareBlock = function(data, type) {
        var result = this.W;
        /** @type {number} */
        var i = 0;
        for (; i < 32; i++) {
          result[i] = data[type + i];
        }
        for (; i < result.length; i = i + 2) {
          var index = function(b, d) {
            var c = $(b, d, 19);
            var e = $(d, b, 29);
            d = i4mul(b, d, 6);
            /** @type {number} */
            d = c ^ e ^ d;
            if (d < 0) {
              /** @type {number} */
              d = d + 4294967296;
            }
            return d;
          }(result[i - 4], result[i - 3]);
          var item = function(i, key) {
            var x = map(i, key, 19);
            var y = map(key, i, 29);
            key = translate(i, key, 6);
            /** @type {number} */
            key = x ^ y ^ key;
            if (key < 0) {
              /** @type {number} */
              key = key + 4294967296;
            }
            return key;
          }(result[i - 4], result[i - 3]);
          var value = result[i - 14];
          var data = result[i - 13];
          var rx = function(b, d) {
            var c = $(b, d, 1);
            var e = $(b, d, 8);
            d = i4mul(b, d, 7);
            /** @type {number} */
            d = c ^ e ^ d;
            if (d < 0) {
              /** @type {number} */
              d = d + 4294967296;
            }
            return d;
          }(result[i - 30], result[i - 29]);
          var A = function(value, v) {
            var x = map(value, v, 1);
            var y = map(value, v, 8);
            v = translate(value, v, 7);
            /** @type {number} */
            v = x ^ y ^ v;
            if (v < 0) {
              /** @type {number} */
              v = v + 4294967296;
            }
            return v;
          }(result[i - 30], result[i - 29]);
          var description = result[i - 32];
          var C = result[i - 31];
          result[i] = render(index, item, value, data, rx, A, description, C);
          result[i + 1] = func(index, item, value, data, rx, A, description, C);
        }
      };
      /**
       * @param {string} message
       * @param {number} part
       * @return {undefined}
       */
      Slider.prototype._update = function(message, part) {
        this._prepareBlock(message, part);
        var matches = this.W;
        var s = this.h[0];
        var data = this.h[1];
        var l = this.h[2];
        var file = this.h[3];
        var j = this.h[4];
        var p = this.h[5];
        var g = this.h[6];
        var b = this.h[7];
        var t = this.h[8];
        var result = this.h[9];
        var d = this.h[10];
        var a = this.h[11];
        var k = this.h[12];
        var args = this.h[13];
        var target = this.h[14];
        var idA = this.h[15];
        flow(this.k.length === matches.length);
        /** @type {number} */
        var i = 0;
        for (; i < matches.length; i = i + 2) {
          var obj = target;
          var c = idA;
          var key = function(e, r) {
            var c = $(e, r, 14);
            var d = $(e, r, 18);
            e = $(r, e, 9);
            /** @type {number} */
            e = c ^ d ^ e;
            if (e < 0) {
              /** @type {number} */
              e = e + 4294967296;
            }
            return e;
          }(t, result);
          var options = function(result, i) {
            var x = map(result, i, 14);
            var y = map(result, i, 18);
            result = map(i, result, 9);
            /** @type {number} */
            result = x ^ y ^ result;
            if (result < 0) {
              /** @type {number} */
              result = result + 4294967296;
            }
            return result;
          }(t, result);
          var e = function(eh, fh, gh) {
            /** @type {number} */
            gh = eh & fh ^ ~eh & gh;
            if (gh < 0) {
              /** @type {number} */
              gh = gh + 4294967296;
            }
            return gh;
          }(t, d, k);
          var m = function(t, a, b) {
            /** @type {number} */
            b = t & a ^ ~t & b;
            if (b < 0) {
              /** @type {number} */
              b = b + 4294967296;
            }
            return b;
          }(result, a, args);
          var error = this.k[i];
          var err = this.k[i + 1];
          var v = matches[i];
          var value = matches[i + 1];
          var val = debug(obj, c, key, options, e, m, error, err, v, value);
          v = log(obj, c, key, options, e, m, error, err, v, value);
          obj = function(d, a) {
            var b = $(d, a, 28);
            var c = $(a, d, 2);
            d = $(a, d, 7);
            /** @type {number} */
            d = b ^ c ^ d;
            if (d < 0) {
              /** @type {number} */
              d = d + 4294967296;
            }
            return d;
          }(s, data);
          c = function(a, f) {
            var x = map(a, f, 28);
            var y = map(f, a, 2);
            a = map(f, a, 7);
            /** @type {number} */
            a = x ^ y ^ a;
            if (a < 0) {
              /** @type {number} */
              a = a + 4294967296;
            }
            return a;
          }(s, data);
          key = function(al, bl, cl) {
            /** @type {number} */
            cl = al & bl ^ al & cl ^ bl & cl;
            if (cl < 0) {
              /** @type {number} */
              cl = cl + 4294967296;
            }
            return cl;
          }(s, l, j);
          options = function(ah, bh, ch) {
            /** @type {number} */
            ch = ah & bh ^ ah & ch ^ bh & ch;
            if (ch < 0) {
              /** @type {number} */
              ch = ch + 4294967296;
            }
            return ch;
          }(data, file, p);
          value = f(obj, c, key, options);
          options = merge(obj, c, key, options);
          target = k;
          idA = args;
          k = d;
          args = a;
          d = t;
          a = result;
          t = f(g, b, val, v);
          result = merge(b, b, val, v);
          g = j;
          b = p;
          j = l;
          p = file;
          l = s;
          file = data;
          s = f(val, v, value, options);
          data = merge(val, v, value, options);
        }
        callback(this.h, 0, s, data);
        callback(this.h, 2, l, file);
        callback(this.h, 4, j, p);
        callback(this.h, 6, g, b);
        callback(this.h, 8, t, result);
        callback(this.h, 10, d, a);
        callback(this.h, 12, k, args);
        callback(this.h, 14, target, idA);
      };
      /**
       * @param {string} hex2
       * @return {?}
       */
      Slider.prototype._digest = function(hex2) {
        return "hex" === hex2 ? options.toHex32(this.h, "big") : options.split32(this.h, "big");
      };
    }, {
      "../common" : 20,
      "../utils" : 30,
      "minimalistic-assert" : 33
    }],
    29 : [function(saveNotifs, canCreateDiscussions, exports) {
      /**
       * @param {number} t
       * @param {number} a
       * @param {number} b
       * @return {?}
       */
      function check(t, a, b) {
        return t & a ^ ~t & b;
      }
      /**
       * @param {number} a
       * @param {number} b
       * @param {number} c
       * @return {?}
       */
      function d(a, b, c) {
        return a & b ^ a & c ^ b & c;
      }
      /**
       * @param {boolean} n
       * @param {boolean} t
       * @param {number} a
       * @return {?}
       */
      function x(n, t, a) {
        return n ^ t ^ a;
      }
      var S = saveNotifs("../utils").rotr32;
      /**
       * @param {number} number
       * @param {number} a
       * @param {number} b
       * @param {number} r
       * @return {?}
       */
      exports.ft_1 = function(number, a, b, r) {
        return 0 === number ? check(a, b, r) : 1 === number || 3 === number ? a ^ b ^ r : 2 === number ? d(a, b, r) : void 0;
      };
      /** @type {function(number, number, number): ?} */
      exports.ch32 = check;
      /** @type {function(number, number, number): ?} */
      exports.maj32 = d;
      /** @type {function(boolean, boolean, number): ?} */
      exports.p32 = x;
      /**
       * @param {undefined} x
       * @return {?}
       */
      exports.s0_256 = function(x) {
        return S(x, 2) ^ S(x, 13) ^ S(x, 22);
      };
      /**
       * @param {undefined} x
       * @return {?}
       */
      exports.s1_256 = function(x) {
        return S(x, 6) ^ S(x, 11) ^ S(x, 25);
      };
      /**
       * @param {number} x
       * @return {?}
       */
      exports.g0_256 = function(x) {
        return S(x, 7) ^ S(x, 18) ^ x >>> 3;
      };
      /**
       * @param {number} x
       * @return {?}
       */
      exports.g1_256 = function(x) {
        return S(x, 17) ^ S(x, 19) ^ x >>> 10;
      };
    }, {
      "../utils" : 30
    }],
    30 : [function(require, canCreateDiscussions, exports) {
      /**
       * @param {number} keys
       * @return {?}
       */
      function serialize(keys) {
        return (keys >>> 24 | keys >>> 8 & 65280 | keys << 8 & 16711680 | (255 & keys) << 24) >>> 0;
      }
      /**
       * @param {string} errorText
       * @return {?}
       */
      function encode(errorText) {
        return 1 === errorText.length ? "0" + errorText : errorText;
      }
      /**
       * @param {string} options
       * @return {?}
       */
      function Number(options) {
        return 7 === options.length ? "0" + options : 6 === options.length ? "00" + options : 5 === options.length ? "000" + options : 4 === options.length ? "0000" + options : 3 === options.length ? "00000" + options : 2 === options.length ? "000000" + options : 1 === options.length ? "0000000" + options : options;
      }
      var assert = require("minimalistic-assert");
      require = require("inherits");
      /** @type {string} */
      exports.inherits = require;
      /**
       * @param {string} data
       * @param {string} obj
       * @return {?}
       */
      exports.toArray = function(data, obj) {
        if (Array.isArray(data)) {
          return data.slice();
        }
        if (!data) {
          return [];
        }
        var buf;
        var i;
        /** @type {!Array} */
        var a = [];
        if ("string" == typeof data) {
          if (obj) {
            if ("hex" === obj) {
              if ((data = data.replace(/[^a-z0-9]+/gi, "")).length % 2 != 0) {
                /** @type {string} */
                data = "0" + data;
              }
              /** @type {number} */
              index = 0;
              for (; index < data.length; index = index + 2) {
                a.push(parseInt(data[index] + data[index + 1], 16));
              }
            }
          } else {
            /** @type {number} */
            var cnt = 0;
            /** @type {number} */
            var index = 0;
            for (; index < data.length; index++) {
              /** @type {number} */
              var e = data.charCodeAt(index);
              if (e < 128) {
                /** @type {number} */
                a[cnt++] = e;
              } else {
                if (e < 2048) {
                  /** @type {number} */
                  a[cnt++] = e >> 6 | 192;
                  /** @type {number} */
                  a[cnt++] = 63 & e | 128;
                } else {
                  /** @type {number} */
                  i = index;
                  if (55296 != (64512 & (buf = data).charCodeAt(i)) || i < 0 || i + 1 >= buf.length || 56320 != (64512 & buf.charCodeAt(i + 1))) {
                    /** @type {number} */
                    a[cnt++] = e >> 12 | 224;
                  } else {
                    /** @type {number} */
                    e = 65536 + ((1023 & e) << 10) + (1023 & data.charCodeAt(++index));
                    /** @type {number} */
                    a[cnt++] = e >> 18 | 240;
                    /** @type {number} */
                    a[cnt++] = e >> 12 & 63 | 128;
                  }
                  /** @type {number} */
                  a[cnt++] = e >> 6 & 63 | 128;
                  /** @type {number} */
                  a[cnt++] = 63 & e | 128;
                }
              }
            }
          }
        } else {
          /** @type {number} */
          index = 0;
          for (; index < data.length; index++) {
            /** @type {number} */
            a[index] = 0 | data[index];
          }
        }
        return a;
      };
      /**
       * @param {string} num
       * @return {?}
       */
      exports.toHex = function(num) {
        /** @type {string} */
        var output = "";
        /** @type {number} */
        var offset = 0;
        for (; offset < num.length; offset++) {
          /** @type {string} */
          output = output + encode(num[offset].toString(16));
        }
        return output;
      };
      /** @type {function(number): ?} */
      exports.htonl = serialize;
      /**
       * @param {!NodeList} ws
       * @param {string} object
       * @return {?}
       */
      exports.toHex32 = function(ws, object) {
        /** @type {string} */
        var pending = "";
        /** @type {number} */
        var i = 0;
        for (; i < ws.length; i++) {
          var val = ws[i];
          /** @type {string} */
          pending = pending + Number((val = "little" === object ? serialize(val) : val).toString(16));
        }
        return pending;
      };
      /** @type {function(string): ?} */
      exports.zero2 = encode;
      /** @type {function(string): ?} */
      exports.zero8 = Number;
      /**
       * @param {string} e
       * @param {number} index
       * @param {number} count
       * @param {string} bounceEnabled
       * @return {?}
       */
      exports.join32 = function(e, index, count, bounceEnabled) {
        assert((count = count - index) % 4 == 0);
        /** @type {!Array} */
        var dat = new Array(count / 4);
        /** @type {number} */
        var j = 0;
        /** @type {number} */
        var k = index;
        for (; j < dat.length; j++, k = k + 4) {
          /** @type {number} */
          var tmp = "big" === bounceEnabled ? e[k] << 24 | e[k + 1] << 16 | e[k + 2] << 8 | e[k + 3] : e[k + 3] << 24 | e[k + 2] << 16 | e[k + 1] << 8 | e[k];
          /** @type {number} */
          dat[j] = tmp >>> 0;
        }
        return dat;
      };
      /**
       * @param {!NodeList} cssPropValueList
       * @param {string} endianess
       * @return {?}
       */
      exports.split32 = function(cssPropValueList, endianess) {
        /** @type {!Array} */
        var data = new Array(4 * cssPropValueList.length);
        /** @type {number} */
        var i = 0;
        /** @type {number} */
        var offset = 0;
        for (; i < cssPropValueList.length; i++, offset = offset + 4) {
          var value = cssPropValueList[i];
          if ("big" === endianess) {
            /** @type {number} */
            data[offset] = value >>> 24;
            /** @type {number} */
            data[offset + 1] = value >>> 16 & 255;
            /** @type {number} */
            data[offset + 2] = value >>> 8 & 255;
            /** @type {number} */
            data[offset + 3] = 255 & value;
          } else {
            /** @type {number} */
            data[offset + 3] = value >>> 24;
            /** @type {number} */
            data[offset + 2] = value >>> 16 & 255;
            /** @type {number} */
            data[offset + 1] = value >>> 8 & 255;
            /** @type {number} */
            data[offset] = 255 & value;
          }
        }
        return data;
      };
      /**
       * @param {number} x
       * @param {number} n
       * @return {?}
       */
      exports.rotr32 = function(x, n) {
        return x >>> n | x << 32 - n;
      };
      /**
       * @param {number} number
       * @param {number} bits
       * @return {?}
       */
      exports.rotl32 = function(number, bits) {
        return number << bits | number >>> 32 - bits;
      };
      /**
       * @param {(Object|number)} marker
       * @param {!Object} name
       * @return {?}
       */
      exports.sum32 = function(marker, name) {
        return marker + name >>> 0;
      };
      /**
       * @param {(Object|number)} operator
       * @param {!Object} name
       * @param {?} val
       * @return {?}
       */
      exports.sum32_3 = function(operator, name, val) {
        return operator + name + val >>> 0;
      };
      /**
       * @param {(Object|number)} store
       * @param {!Object} model_name
       * @param {?} plural_name
       * @param {number} primary_key_name
       * @return {?}
       */
      exports.sum32_4 = function(store, model_name, plural_name, primary_key_name) {
        return store + model_name + plural_name + primary_key_name >>> 0;
      };
      /**
       * @param {(Object|number)} indexVarValue
       * @param {!Object} previousElement
       * @param {?} node
       * @param {?} records
       * @param {?} showShortEvents
       * @return {?}
       */
      exports.sum32_5 = function(indexVarValue, previousElement, node, records, showShortEvents) {
        return indexVarValue + previousElement + node + records + showShortEvents >>> 0;
      };
      /**
       * @param {!NodeList} data
       * @param {number} pos
       * @param {number} id
       * @param {number} i
       * @return {undefined}
       */
      exports.sum64 = function(data, pos, id, i) {
        var tmp = data[pos];
        /** @type {number} */
        var n = i + data[pos + 1] >>> 0;
        tmp = (n < i ? 1 : 0) + id + tmp;
        /** @type {number} */
        data[pos] = tmp >>> 0;
        /** @type {number} */
        data[pos + 1] = n;
      };
      /**
       * @param {number} x
       * @param {number} y
       * @param {?} value
       * @param {number} dir
       * @return {?}
       */
      exports.sum64_hi = function(x, y, value, dir) {
        return (y + dir >>> 0 < y ? 1 : 0) + x + value >>> 0;
      };
      /**
       * @param {?} a
       * @param {(Object|number)} f
       * @param {?} n
       * @param {!Object} k
       * @return {?}
       */
      exports.sum64_lo = function(a, f, n, k) {
        return f + k >>> 0;
      };
      /**
       * @param {(Object|number)} color
       * @param {number} n
       * @param {!Object} type
       * @param {?} s
       * @param {?} num
       * @param {number} length
       * @param {?} text
       * @param {number} len
       * @return {?}
       */
      exports.sum64_4_hi = function(color, n, type, s, num, length, text, len) {
        /** @type {number} */
        var dataIdCounter = 0;
        /** @type {number} */
        var i = n;
        return dataIdCounter = dataIdCounter + ((i = i + s >>> 0) < n ? 1 : 0), dataIdCounter = dataIdCounter + ((i = i + length >>> 0) < length ? 1 : 0), color + type + num + text + (dataIdCounter = dataIdCounter + ((i = i + len >>> 0) < len ? 1 : 0)) >>> 0;
      };
      /**
       * @param {?} apiKey
       * @param {(Object|number)} key
       * @param {?} instance
       * @param {!Object} type
       * @param {?} fn
       * @param {?} id
       * @param {?} index
       * @param {?} value
       * @return {?}
       */
      exports.sum64_4_lo = function(apiKey, key, instance, type, fn, id, index, value) {
        return key + type + id + value >>> 0;
      };
      /**
       * @param {(Object|number)} s
       * @param {number} n
       * @param {!Object} path
       * @param {?} interval
       * @param {?} name
       * @param {number} length
       * @param {?} params
       * @param {number} len
       * @param {?} _
       * @param {number} l
       * @return {?}
       */
      exports.sum64_5_hi = function(s, n, path, interval, name, length, params, len, _, l) {
        /** @type {number} */
        var dataIdCounter = 0;
        /** @type {number} */
        var i = n;
        return dataIdCounter = dataIdCounter + ((i = i + interval >>> 0) < n ? 1 : 0), dataIdCounter = dataIdCounter + ((i = i + length >>> 0) < length ? 1 : 0), dataIdCounter = dataIdCounter + ((i = i + len >>> 0) < len ? 1 : 0), s + path + name + params + _ + (dataIdCounter = dataIdCounter + ((i = i + l >>> 0) < l ? 1 : 0)) >>> 0;
      };
      /**
       * @param {?} exit
       * @param {(Object|number)} code
       * @param {?} uri
       * @param {!Object} c
       * @param {?} t
       * @param {?} val
       * @param {?} type
       * @param {?} _
       * @param {?} reason
       * @param {?} name
       * @return {?}
       */
      exports.sum64_5_lo = function(exit, code, uri, c, t, val, type, _, reason, name) {
        return code + c + val + _ + name >>> 0;
      };
      /**
       * @param {number} a
       * @param {number} n
       * @param {number} b
       * @return {?}
       */
      exports.rotr64_hi = function(a, n, b) {
        return (n << 32 - b | a >>> b) >>> 0;
      };
      /**
       * @param {number} n
       * @param {number} a
       * @param {number} b
       * @return {?}
       */
      exports.rotr64_lo = function(n, a, b) {
        return (n << 32 - b | a >>> b) >>> 0;
      };
      /**
       * @param {number} b
       * @param {number} depth
       * @param {number} s
       * @return {?}
       */
      exports.shr64_hi = function(b, depth, s) {
        return b >>> s;
      };
      /**
       * @param {number} n
       * @param {number} a
       * @param {number} b
       * @return {?}
       */
      exports.shr64_lo = function(n, a, b) {
        return (n << 32 - b | a >>> b) >>> 0;
      };
    }, {
      inherits : 32,
      "minimalistic-assert" : 33
    }],
    31 : [function(require, context, i) {
      /**
       * @param {!Object} options
       * @return {?}
       */
      function init(options) {
        if (!(this instanceof init)) {
          return new init(options);
        }
        this.hash = options.hash;
        /** @type {boolean} */
        this.predResist = !!options.predResist;
        this.outLen = this.hash.outSize;
        this.minEntropy = options.minEntropy || this.hash.hmacStrength;
        /** @type {null} */
        this._reseed = null;
        /** @type {null} */
        this.reseedInterval = null;
        /** @type {null} */
        this.K = null;
        /** @type {null} */
        this.V = null;
        var w = t.toArray(options.entropy, options.entropyEnc || "hex");
        var h = t.toArray(options.nonce, options.nonceEnc || "hex");
        options = t.toArray(options.pers, options.persEnc || "hex");
        assert(w.length >= this.minEntropy / 8, "Not enough entropy. Minimum is: " + this.minEntropy + " bits");
        this._init(w, h, options);
      }
      var crypto = require("hash.js");
      var t = require("minimalistic-crypto-utils");
      var assert = require("minimalistic-assert");
      /**
       * @param {!Array} key
       * @param {?} dir
       * @param {boolean} data
       * @return {undefined}
       */
      (context.exports = init).prototype._init = function(key, dir, data) {
        data = key.concat(dir).concat(data);
        /** @type {!Array} */
        this.K = new Array(this.outLen / 8);
        /** @type {!Array} */
        this.V = new Array(this.outLen / 8);
        /** @type {number} */
        var i = 0;
        for (; i < this.V.length; i++) {
          /** @type {number} */
          this.K[i] = 0;
          /** @type {number} */
          this.V[i] = 1;
        }
        this._update(data);
        /** @type {number} */
        this._reseed = 1;
        /** @type {number} */
        this.reseedInterval = 281474976710656;
      };
      /**
       * @return {?}
       */
      init.prototype._hmac = function() {
        return new crypto.hmac(this.hash, this.K);
      };
      /**
       * @param {string} message
       * @return {undefined}
       */
      init.prototype._update = function(message) {
        var sig = this._hmac().update(this.V).update([0]);
        if (message) {
          sig = sig.update(message);
        }
        this.K = sig.digest();
        this.V = this._hmac().update(this.V).digest();
        if (message) {
          this.K = this._hmac().update(this.V).update([1]).update(message).digest();
          this.V = this._hmac().update(this.V).digest();
        }
      };
      /**
       * @param {string} res
       * @param {string} options
       * @param {string} message
       * @param {string} result
       * @return {undefined}
       */
      init.prototype.reseed = function(res, options, message, result) {
        if ("string" != typeof options) {
          /** @type {string} */
          result = message;
          /** @type {string} */
          message = options;
          /** @type {null} */
          options = null;
        }
        res = t.toArray(res, options);
        message = t.toArray(message, result);
        assert(res.length >= this.minEntropy / 8, "Not enough entropy. Minimum is: " + this.minEntropy + " bits");
        this._update(res.concat(message || []));
        /** @type {number} */
        this._reseed = 1;
      };
      /**
       * @param {number} len
       * @param {string} data
       * @param {string} value
       * @param {string} html
       * @return {?}
       */
      init.prototype.generate = function(len, data, value, html) {
        if (this._reseed > this.reseedInterval) {
          throw new Error("Reseed is required");
        }
        if ("string" != typeof data) {
          /** @type {string} */
          html = value;
          /** @type {string} */
          value = data;
          /** @type {null} */
          data = null;
        }
        if (value) {
          value = t.toArray(value, html || "hex");
          this._update(value);
        }
        /** @type {!Array} */
        var temp = [];
        for (; temp.length < len;) {
          this.V = this._hmac().update(this.V).digest();
          /** @type {!Array<?>} */
          temp = temp.concat(this.V);
        }
        /** @type {!Array<?>} */
        html = temp.slice(0, len);
        return this._update(value), this._reseed++, t.encode(html, data);
      };
    }, {
      "hash.js" : 19,
      "minimalistic-assert" : 33,
      "minimalistic-crypto-utils" : 34
    }],
    32 : [function(canCreateDiscussions, mixin, i) {
      if ("function" == typeof Object.create) {
        /**
         * @param {!Function} instance
         * @param {!Function} Constructor
         * @return {undefined}
         */
        mixin.exports = function(instance, Constructor) {
          if (Constructor) {
            /** @type {!Function} */
            instance.super_ = Constructor;
            /** @type {!Object} */
            instance.prototype = Object.create(Constructor.prototype, {
              constructor : {
                value : instance,
                enumerable : false,
                writable : true,
                configurable : true
              }
            });
          }
        };
      } else {
        /**
         * @param {!Object} self
         * @param {!Function} source
         * @return {undefined}
         */
        mixin.exports = function(self, source) {
          var Event;
          if (source) {
            /** @type {!Function} */
            self.super_ = source;
            (Event = function() {
            }).prototype = source.prototype;
            self.prototype = new Event;
            /** @type {!Object} */
            self.prototype.constructor = self;
          }
        };
      }
    }, {}],
    33 : [function(canCreateDiscussions, module, i) {
      /**
       * @param {?} subreddit
       * @param {string} text
       * @return {undefined}
       */
      function search(subreddit, text) {
        if (!subreddit) {
          throw new Error(text || "Assertion failed");
        }
      }
      /**
       * @param {string} a
       * @param {string} b
       * @param {string} msg
       * @return {undefined}
       */
      (module.exports = search).equal = function(a, b, msg) {
        if (a != b) {
          throw new Error(msg || "Assertion failed: " + a + " != " + b);
        }
      };
    }, {}],
    34 : [function(canCreateDiscussions, isSlidingUp, c) {
      /**
       * @param {string} errorText
       * @return {?}
       */
      function encode(errorText) {
        return 1 === errorText.length ? "0" + errorText : errorText;
      }
      /**
       * @param {string} num
       * @return {?}
       */
      function get(num) {
        /** @type {string} */
        var textContent = "";
        /** @type {number} */
        var offset = 0;
        for (; offset < num.length; offset++) {
          /** @type {string} */
          textContent = textContent + encode(num[offset].toString(16));
        }
        return textContent;
      }
      /**
       * @param {string} data
       * @param {string} obj
       * @return {?}
       */
      c.toArray = function(data, obj) {
        if (Array.isArray(data)) {
          return data.slice();
        }
        if (!data) {
          return [];
        }
        /** @type {!Array} */
        var result = [];
        if ("string" != typeof data) {
          /** @type {number} */
          var i = 0;
          for (; i < data.length; i++) {
            /** @type {number} */
            result[i] = 0 | data[i];
          }
          return result;
        }
        if ("hex" === obj) {
          if ((data = data.replace(/[^a-z0-9]+/gi, "")).length % 2 != 0) {
            /** @type {string} */
            data = "0" + data;
          }
          /** @type {number} */
          i = 0;
          for (; i < data.length; i = i + 2) {
            result.push(parseInt(data[i] + data[i + 1], 16));
          }
        } else {
          /** @type {number} */
          i = 0;
          for (; i < data.length; i++) {
            /** @type {number} */
            var key = data.charCodeAt(i);
            /** @type {number} */
            var enrichedListItem = key >> 8;
            /** @type {number} */
            key = 255 & key;
            if (enrichedListItem) {
              result.push(enrichedListItem, key);
            } else {
              result.push(key);
            }
          }
        }
        return result;
      };
      /** @type {function(string): ?} */
      c.zero2 = encode;
      /** @type {function(string): ?} */
      c.toHex = get;
      /**
       * @param {string} obj
       * @param {string} string
       * @return {?}
       */
      c.encode = function(obj, string) {
        return "hex" === string ? get(obj) : obj;
      };
    }, {}],
    35 : [function(canCreateDiscussions, module, i) {
      module.exports = {
        name : "elliptic",
        version : "6.5.4",
        description : "EC cryptography",
        main : "lib/elliptic.js",
        files : ["lib"],
        scripts : {
          lint : "eslint lib test",
          "lint:fix" : "npm run lint -- --fix",
          unit : "istanbul test _mocha --reporter=spec test/index.js",
          test : "npm run lint && npm run unit",
          version : "grunt dist && git add dist/"
        },
        repository : {
          type : "git",
          url : "git@github.com:indutny/elliptic"
        },
        keywords : ["EC", "Elliptic", "curve", "Cryptography"],
        author : "Fedor Indutny <fedor@indutny.com>",
        license : "MIT",
        bugs : {
          url : "https://github.com/indutny/elliptic/issues"
        },
        homepage : "https://github.com/indutny/elliptic",
        devDependencies : {
          brfs : "^2.0.2",
          coveralls : "^3.1.0",
          eslint : "^7.6.0",
          grunt : "^1.2.1",
          "grunt-browserify" : "^5.3.0",
          "grunt-cli" : "^1.3.2",
          "grunt-contrib-connect" : "^3.0.0",
          "grunt-contrib-copy" : "^1.0.0",
          "grunt-contrib-uglify" : "^5.0.0",
          "grunt-mocha-istanbul" : "^5.0.2",
          "grunt-saucelabs" : "^9.0.1",
          istanbul : "^0.4.5",
          mocha : "^8.0.1"
        },
        dependencies : {
          "bn.js" : "^4.11.9",
          brorand : "^1.1.0",
          "hash.js" : "^1.0.0",
          "hmac-drbg" : "^1.0.1",
          inherits : "^2.0.4",
          "minimalistic-assert" : "^1.0.1",
          "minimalistic-crypto-utils" : "^1.0.1"
        }
      };
    }, {}]
  }, {}, [1])(1);
});

