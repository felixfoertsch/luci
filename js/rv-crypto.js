'use strict';

function generateRandomData() {
	let data = '030101';
	let random_seed_from_date = Math.floor(Date.now() / 1000).toString(16);
	random_seed_from_date.match(/(..?)/g).reverse().join('');
	data = data + random_seed_from_date;
	data = data + getRandomHexString(178);
	data = data + forge.util.bytesToHex(sha256(parseHexString(data))).slice(0, 8);

	return data;
}

function getRandomHexString(string_length) {
	let output = [];
	let allowed_hex_characters = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'];

	for (let _t_ix = 0; _t_ix < string_length; _t_ix++) {
		output.push(allowed_hex_characters[Math.floor(Math.random() * 16)]);
	}

	return output.join('');
}

function parseHexString(input) {
	var output = [];
	while(input.length >= 2) {
		output.push(parseInt(input.substring(0, 2), 16));
		input = input.substring(2, input.length);
	}

	return output;
}

function generateDeviceData(random_data) {
	const random_data_bytes = forge.util.hexToBytes(random_data);
	let byte_buffer = forge.util.createBuffer(random_data_bytes);
	byte_buffer.getBytes(2);
	const keyId = byte_buffer.getByte();
	const timestamp = byte_buffer.getInt32Le();
	const traceId = forge.util.encode64(byte_buffer.getBytes(16));
	const data = forge.util.encode64(byte_buffer.getBytes(32));
	const publicKey = forge.util.encode64(byte_buffer.getBytes(33));
	const verificationTag = forge.util.encode64(byte_buffer.getBytes(8));
	return {
		'v': 3,
		'data': data,
		'keyId': keyId,
		'traceId': traceId,
		'publicKey': publicKey,
		'timestamp': timestamp,
		'deviceType': 1,
		'verificationTag': verificationTag
	};
}

function generateCheckin(venue, device) {
	// https://luca-app.de/securityoverview/processes/guest_app_checkin.html#qr-code-scanning-validation-and-check-in-upload
	const version = '03';
	const check_in_data = version
		.concat(forge.util.bytesToHex([device['keyId']]))
		.concat(forge.util.bytesToHex(forge.util.decode64(venue['publicKey'])))
		.concat(forge.util.bytesToHex(forge.util.decode64(device['verificationTag'])));

	const venue_data = generateVenueData(forge.util.bytesToHex(forge.util.decode64(venue['publicKey'])), check_in_data);

	return {
		'traceId': device['traceId'],
		'scannerId': venue['scannerId'],
		'timestamp': device['timestamp'],
		'data': forge.util.encode64(forge.util.hexToBytes(venue_data['data'])),
		'iv': forge.util.encode64(forge.util.hexToBytes(venue_data['iv'])),
		'mac': forge.util.encode64(forge.util.hexToBytes(venue_data['mac'])),
		'publicKey': forge.util.encode64(forge.util.hexToBytes(venue_data['publicKey'])),
		'deviceType': device['deviceType']
	};
}

function generateVenueData(venue_public_key, check_in_data) {
	const elliptic_curve = new elliptic.ec('p256');
	const eph_scanner_keys = elliptic_curve.genKeyPair();
	const dh_key = eph_scanner_keys.derive(elliptic_curve.keyFromPublic(venue_public_key, 'hex').getPublic()).toString('hex');
	const enc_key = forge.util.bytesToHex(sha256(dh_key + '01')).slice(0, 32);
	const auth_key = forge.util.bytesToHex(sha256(dh_key + '02'));
	const iv = forge.util.bytesToHex(forge.random.getBytesSync(16));
	const venue_enc_data = encryptWithAES(check_in_data, enc_key, iv);
	var hmac = forge.hmac.create();
	hmac.start('sha256', auth_key);
	hmac.update(venue_enc_data);
	const venue_enc_data_mac = hmac.digest().toHex();

	return {
		'publicKey': eph_scanner_keys.getPublic('hex'),
		'data': venue_enc_data,
		'iv': iv,
		'mac': venue_enc_data_mac
	};
}

function encryptWithAES(data, enc_key, iv) {
	var cipher = forge.cipher.createCipher('AES-CTR', forge.util.hexToBytes(enc_key));
	
	cipher.start({'iv': forge.util.hexToBytes(iv)});
	cipher.update(forge.util.createBuffer(forge.util.hexToBytes(data)));
	cipher.finish();

	return cipher.output.toHex();
};