'use strict';
!function(exports) {
  var data = {};
  !function(options) {
    /**
     * @param {!Object} pattern
     * @param {!Array} arr
     * @param {!Array} a
     * @param {number} b
     * @param {number} prop
     * @return {?}
     */
    function callback(pattern, arr, a, b, prop) {
      var t;
      var r;
      var e;
      var s;
      var x;
      var y;
      var z;
      var el;
      var v;
      var j;
      var i;
      var _;
      var result;
      for (; prop >= 64;) {
        t = arr[0];
        r = arr[1];
        e = arr[2];
        s = arr[3];
        x = arr[4];
        y = arr[5];
        z = arr[6];
        el = arr[7];
        /** @type {number} */
        j = 0;
        for (; j < 16; j++) {
          i = b + j * 4;
          /** @type {number} */
          pattern[j] = (a[i] & 255) << 24 | (a[i + 1] & 255) << 16 | (a[i + 2] & 255) << 8 | a[i + 3] & 255;
        }
        /** @type {number} */
        j = 16;
        for (; j < 64; j++) {
          v = pattern[j - 2];
          /** @type {number} */
          _ = (v >>> 17 | v << 32 - 17) ^ (v >>> 19 | v << 32 - 19) ^ v >>> 10;
          v = pattern[j - 15];
          /** @type {number} */
          result = (v >>> 7 | v << 32 - 7) ^ (v >>> 18 | v << 32 - 18) ^ v >>> 3;
          /** @type {number} */
          pattern[j] = (_ + pattern[j - 7] | 0) + (result + pattern[j - 16] | 0);
        }
        /** @type {number} */
        j = 0;
        for (; j < 64; j++) {
          /** @type {number} */
          _ = (((x >>> 6 | x << 32 - 6) ^ (x >>> 11 | x << 32 - 11) ^ (x >>> 25 | x << 32 - 25)) + (x & y ^ ~x & z) | 0) + (el + (c[j] + pattern[j] | 0) | 0) | 0;
          /** @type {number} */
          result = ((t >>> 2 | t << 32 - 2) ^ (t >>> 13 | t << 32 - 13) ^ (t >>> 22 | t << 32 - 22)) + (t & r ^ t & e ^ r & e) | 0;
          el = z;
          z = y;
          y = x;
          /** @type {number} */
          x = s + _ | 0;
          s = e;
          e = r;
          r = t;
          /** @type {number} */
          t = _ + result | 0;
        }
        arr[0] += t;
        arr[1] += r;
        arr[2] += e;
        arr[3] += s;
        arr[4] += x;
        arr[5] += y;
        arr[6] += z;
        arr[7] += el;
        b = b + 64;
        /** @type {number} */
        prop = prop - 64;
      }
      return b;
    }
    /**
     * @param {?} selector
     * @return {?}
     */
    function initialize(selector) {
      var m = (new Element).update(selector);
      var sike = m.digest();
      m.clean();
      return sike;
    }
    /**
     * @param {number} url
     * @param {?} str
     * @return {?}
     */
    function update(url, str) {
      var m = (new Array(url)).update(str);
      var outData = m.digest();
      m.clean();
      return outData;
    }
    /**
     * @param {?} msg
     * @param {!Object} obj
     * @param {?} type
     * @param {?} count
     * @return {undefined}
     */
    function fn(msg, obj, type, count) {
      var contextId = count[0];
      if (contextId === 0) {
        throw new Error("hkdf: cannot expand more");
      }
      obj.reset();
      if (contextId > 1) {
        obj.update(msg);
      }
      if (type) {
        obj.update(type);
      }
      obj.update(count);
      obj.finish(msg);
      count[0]++;
    }
    /**
     * @param {?} json
     * @param {number} key
     * @param {?} template
     * @param {number} length
     * @return {?}
     */
    function exports(json, key, template, length) {
      if (key === void 0) {
        /** @type {!Uint8Array} */
        key = ret;
      }
      if (length === void 0) {
        /** @type {number} */
        length = 32;
      }
      /** @type {!Uint8Array} */
      var p = new Uint8Array([1]);
      var obj = update(key, json);
      var target = new Array(obj);
      /** @type {!Uint8Array} */
      var buffer = new Uint8Array(target.digestLength);
      /** @type {number} */
      var pos = buffer.length;
      /** @type {!Uint8Array} */
      var result = new Uint8Array(length);
      /** @type {number} */
      var i = 0;
      for (; i < length; i++) {
        if (pos === buffer.length) {
          fn(buffer, target, template, p);
          /** @type {number} */
          pos = 0;
        }
        /** @type {number} */
        result[i] = buffer[pos++];
      }
      target.clean();
      buffer.fill(0);
      p.fill(0);
      return result;
    }
    /**
     * @param {?} iter
     * @param {?} password
     * @param {number} salt
     * @param {number} count
     * @return {?}
     */
    function pbkdf2_js(iter, password, salt, count) {
      var ret = new Array(iter);
      var len = ret.digestLength;
      /** @type {!Uint8Array} */
      var h = new Uint8Array(4);
      /** @type {!Uint8Array} */
      var state = new Uint8Array(len);
      /** @type {!Uint8Array} */
      var data = new Uint8Array(len);
      /** @type {!Uint8Array} */
      var result = new Uint8Array(count);
      /** @type {number} */
      var i = 0;
      for (; i * len < count; i++) {
        /** @type {number} */
        var nextReplicaIndex = i + 1;
        /** @type {number} */
        h[0] = nextReplicaIndex >>> 24 & 255;
        /** @type {number} */
        h[1] = nextReplicaIndex >>> 16 & 255;
        /** @type {number} */
        h[2] = nextReplicaIndex >>> 8 & 255;
        /** @type {number} */
        h[3] = nextReplicaIndex >>> 0 & 255;
        ret.reset();
        ret.update(password);
        ret.update(h);
        ret.finish(data);
        /** @type {number} */
        var j = 0;
        for (; j < len; j++) {
          /** @type {number} */
          state[j] = data[j];
        }
        /** @type {number} */
        j = 2;
        for (; j <= salt; j++) {
          ret.reset();
          ret.update(data).finish(data);
          /** @type {number} */
          var i = 0;
          for (; i < len; i++) {
            state[i] ^= data[i];
          }
        }
        /** @type {number} */
        j = 0;
        for (; j < len && i * len + j < count; j++) {
          /** @type {number} */
          result[i * len + j] = state[j];
        }
      }
      /** @type {number} */
      i = 0;
      for (; i < len; i++) {
        /** @type {number} */
        state[i] = data[i] = 0;
      }
      /** @type {number} */
      i = 0;
      for (; i < 4; i++) {
        /** @type {number} */
        h[i] = 0;
      }
      ret.clean();
      return result;
    }
    /** @type {boolean} */
    options.__esModule = true;
    /** @type {number} */
    options.digestLength = 32;
    /** @type {number} */
    options.blockSize = 64;
    /** @type {!Uint32Array} */
    var c = new Uint32Array([1116352408, 1899447441, 3049323471, 3921009573, 961987163, 1508970993, 2453635748, 2870763221, 3624381080, 310598401, 607225278, 1426881987, 1925078388, 2162078206, 2614888103, 3248222580, 3835390401, 4022224774, 264347078, 604807628, 770255983, 1249150122, 1555081692, 1996064986, 2554220882, 2821834349, 2952996808, 3210313671, 3336571891, 3584528711, 113926993, 338241895, 666307205, 773529912, 1294757372, 1396182291, 1695183700, 1986661051, 2177026350, 2456956037, 2730485921, 
    2820302411, 3259730800, 3345764771, 3516065817, 3600352804, 4094571909, 275423344, 430227734, 506948616, 659060556, 883997877, 958139571, 1322822218, 1537002063, 1747873779, 1955562222, 2024104815, 2227730452, 2361852424, 2428436474, 2756734187, 3204031479, 3329325298]);
    var Element = function() {
      /**
       * @return {undefined}
       */
      function init() {
        /** @type {number} */
        this.digestLength = options.digestLength;
        /** @type {number} */
        this.blockSize = options.blockSize;
        /** @type {!Int32Array} */
        this.state = new Int32Array(8);
        /** @type {!Int32Array} */
        this.temp = new Int32Array(64);
        /** @type {!Uint8Array} */
        this.buffer = new Uint8Array(128);
        /** @type {number} */
        this.bufferLength = 0;
        /** @type {number} */
        this.bytesHashed = 0;
        /** @type {boolean} */
        this.finished = false;
        this.reset();
      }
      /**
       * @return {?}
       */
      init.prototype.reset = function() {
        /** @type {number} */
        this.state[0] = 1779033703;
        /** @type {number} */
        this.state[1] = 3144134277;
        /** @type {number} */
        this.state[2] = 1013904242;
        /** @type {number} */
        this.state[3] = 2773480762;
        /** @type {number} */
        this.state[4] = 1359893119;
        /** @type {number} */
        this.state[5] = 2600822924;
        /** @type {number} */
        this.state[6] = 528734635;
        /** @type {number} */
        this.state[7] = 1541459225;
        /** @type {number} */
        this.bufferLength = 0;
        /** @type {number} */
        this.bytesHashed = 0;
        /** @type {boolean} */
        this.finished = false;
        return this;
      };
      /**
       * @return {undefined}
       */
      init.prototype.clean = function() {
        /** @type {number} */
        var i = 0;
        for (; i < this.buffer.length; i++) {
          /** @type {number} */
          this.buffer[i] = 0;
        }
        /** @type {number} */
        i = 0;
        for (; i < this.temp.length; i++) {
          /** @type {number} */
          this.temp[i] = 0;
        }
        this.reset();
      };
      /**
       * @param {?} data
       * @param {number} key
       * @return {?}
       */
      init.prototype.update = function(data, key) {
        if (key === void 0) {
          key = data.length;
        }
        if (this.finished) {
          throw new Error("SHA256: can't update because hash was finished.");
        }
        /** @type {number} */
        var value = 0;
        this.bytesHashed += key;
        if (this.bufferLength > 0) {
          for (; this.bufferLength < 64 && key > 0;) {
            this.buffer[this.bufferLength++] = data[value++];
            key--;
          }
          if (this.bufferLength === 64) {
            callback(this.temp, this.state, this.buffer, 0, 64);
            /** @type {number} */
            this.bufferLength = 0;
          }
        }
        if (key >= 64) {
          value = callback(this.temp, this.state, data, value, key);
          /** @type {number} */
          key = key % 64;
        }
        for (; key > 0;) {
          this.buffer[this.bufferLength++] = data[value++];
          key--;
        }
        return this;
      };
      /**
       * @param {?} text
       * @return {?}
       */
      init.prototype.finish = function(text) {
        if (!this.finished) {
          var clen = this.bytesHashed;
          var pos = this.bufferLength;
          /** @type {number} */
          var r = clen / 536870912 | 0;
          /** @type {number} */
          var fclen = clen << 3;
          /** @type {number} */
          var start = clen % 64 < 56 ? 64 : 128;
          /** @type {number} */
          this.buffer[pos] = 128;
          var i = pos + 1;
          for (; i < start - 8; i++) {
            /** @type {number} */
            this.buffer[i] = 0;
          }
          /** @type {number} */
          this.buffer[start - 8] = r >>> 24 & 255;
          /** @type {number} */
          this.buffer[start - 7] = r >>> 16 & 255;
          /** @type {number} */
          this.buffer[start - 6] = r >>> 8 & 255;
          /** @type {number} */
          this.buffer[start - 5] = r >>> 0 & 255;
          /** @type {number} */
          this.buffer[start - 4] = fclen >>> 24 & 255;
          /** @type {number} */
          this.buffer[start - 3] = fclen >>> 16 & 255;
          /** @type {number} */
          this.buffer[start - 2] = fclen >>> 8 & 255;
          /** @type {number} */
          this.buffer[start - 1] = fclen >>> 0 & 255;
          callback(this.temp, this.state, this.buffer, 0, start);
          /** @type {boolean} */
          this.finished = true;
        }
        /** @type {number} */
        i = 0;
        for (; i < 8; i++) {
          /** @type {number} */
          text[i * 4 + 0] = this.state[i] >>> 24 & 255;
          /** @type {number} */
          text[i * 4 + 1] = this.state[i] >>> 16 & 255;
          /** @type {number} */
          text[i * 4 + 2] = this.state[i] >>> 8 & 255;
          /** @type {number} */
          text[i * 4 + 3] = this.state[i] >>> 0 & 255;
        }
        return this;
      };
      /**
       * @return {?}
       */
      init.prototype.digest = function() {
        /** @type {!Uint8Array} */
        var result = new Uint8Array(this.digestLength);
        this.finish(result);
        return result;
      };
      /**
       * @param {?} data
       * @return {undefined}
       */
      init.prototype._saveState = function(data) {
        /** @type {number} */
        var i = 0;
        for (; i < this.state.length; i++) {
          data[i] = this.state[i];
        }
      };
      /**
       * @param {!NodeList} state
       * @param {number} persistObj
       * @return {undefined}
       */
      init.prototype._restoreState = function(state, persistObj) {
        /** @type {number} */
        var i = 0;
        for (; i < this.state.length; i++) {
          this.state[i] = state[i];
        }
        /** @type {number} */
        this.bytesHashed = persistObj;
        /** @type {boolean} */
        this.finished = false;
        /** @type {number} */
        this.bufferLength = 0;
      };
      return init;
    }();
    options.Hash = Element;
    var Array = function() {
      /**
       * @param {!Array} data
       * @return {undefined}
       */
      function init(data) {
        this.inner = new Element;
        this.outer = new Element;
        this.blockSize = this.inner.blockSize;
        this.digestLength = this.inner.digestLength;
        /** @type {!Uint8Array} */
        var out = new Uint8Array(this.blockSize);
        if (data.length > this.blockSize) {
          (new Element).update(data).finish(out).clean();
        } else {
          /** @type {number} */
          var i = 0;
          for (; i < data.length; i++) {
            out[i] = data[i];
          }
        }
        /** @type {number} */
        i = 0;
        for (; i < out.length; i++) {
          out[i] ^= 54;
        }
        this.inner.update(out);
        /** @type {number} */
        i = 0;
        for (; i < out.length; i++) {
          out[i] ^= 54 ^ 92;
        }
        this.outer.update(out);
        /** @type {!Uint32Array} */
        this.istate = new Uint32Array(8);
        /** @type {!Uint32Array} */
        this.ostate = new Uint32Array(8);
        this.inner._saveState(this.istate);
        this.outer._saveState(this.ostate);
        /** @type {number} */
        i = 0;
        for (; i < out.length; i++) {
          /** @type {number} */
          out[i] = 0;
        }
      }
      /**
       * @return {?}
       */
      init.prototype.reset = function() {
        this.inner._restoreState(this.istate, this.inner.blockSize);
        this.outer._restoreState(this.ostate, this.outer.blockSize);
        return this;
      };
      /**
       * @return {undefined}
       */
      init.prototype.clean = function() {
        /** @type {number} */
        var layer_i = 0;
        for (; layer_i < this.istate.length; layer_i++) {
          /** @type {number} */
          this.ostate[layer_i] = this.istate[layer_i] = 0;
        }
        this.inner.clean();
        this.outer.clean();
      };
      /**
       * @param {?} msg
       * @return {?}
       */
      init.prototype.update = function(msg) {
        this.inner.update(msg);
        return this;
      };
      /**
       * @param {?} msg
       * @return {?}
       */
      init.prototype.finish = function(msg) {
        if (this.outer.finished) {
          this.outer.finish(msg);
        } else {
          this.inner.finish(msg);
          this.outer.update(msg, this.digestLength).finish(msg);
        }
        return this;
      };
      /**
       * @return {?}
       */
      init.prototype.digest = function() {
        /** @type {!Uint8Array} */
        var result = new Uint8Array(this.digestLength);
        this.finish(result);
        return result;
      };
      return init;
    }();
    options.HMAC = Array;
    /** @type {function(?): ?} */
    options.hash = initialize;
    /** @type {function(?): ?} */
    options["default"] = initialize;
    /** @type {function(number, ?): ?} */
    options.hmac = update;
    /** @type {!Uint8Array} */
    var ret = new Uint8Array(options.digestLength);
    /** @type {function(?, number, ?, number): ?} */
    options.hkdf = exports;
    /** @type {function(?, ?, number, number): ?} */
    options.pbkdf2 = pbkdf2_js;
  }(data);
  var hash = data.default;
  var prop;
  for (prop in data) {
    hash[prop] = data[prop];
  }
  if ("object" == typeof module && "object" == typeof module.exports) {
    module.exports = hash;
  } else {
    if ("function" == typeof define && define.amd) {
      define(function() {
        return hash;
      });
    } else {
      exports.sha256 = hash;
    }
  }
}(this);

