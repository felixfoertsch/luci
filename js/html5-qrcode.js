'use strict';
var _slicedToArray = function () {
	/**
	 * @param {?} arr
	 * @param {string} i
	 * @return {?}
	 */
	function sliceIterator(arr, i) {
		/** @type {!Array} */
		var _arr = [];
		/** @type {boolean} */
		var _n = true;
		/** @type {boolean} */
		var _d = false;
		var _e = undefined;
		try {
			var _i = arr[Symbol.iterator]();
			var _s;
			for (; !(_n = (_s = _i.next()).done); _n = true) {
				_arr.push(_s.value);
				if (i && _arr.length === i) {
					break;
				}
			}
		} catch (err) {
			/** @type {boolean} */
			_d = true;
			_e = err;
		} finally {
			try {
				if (!_n && _i["return"]) {
					_i["return"]();
				}
			} finally {
				if (_d) {
					throw _e;
				}
			}
		}
		return _arr;
	}
	return function (arr, i) {
		if (Array.isArray(arr)) {
			return arr;
		} else {
			if (Symbol.iterator in Object(arr)) {
				return sliceIterator(arr, i);
			} else {
				throw new TypeError("Invalid attempt to destructure non-iterable instance");
			}
		}
	};
}();
/**
 * @param {!Object} object
 * @param {string} name
 * @param {?} receiver
 * @return {?}
 */
var _get = function get(object, name, receiver) {
	if (object === null) {
		object = Function.prototype;
	}
	/** @type {(ObjectPropertyDescriptor<?>|undefined)} */
	var desc = Object.getOwnPropertyDescriptor(object, name);
	if (desc === undefined) {
		/** @type {(Object|null)} */
		var parent = Object.getPrototypeOf(object);
		if (parent === null) {
			return undefined;
		} else {
			return get(parent, name, receiver);
		}
	} else {
		if ("value" in desc) {
			return desc.value;
		} else {
			/** @type {(function(): ?|undefined)} */
			var getter = desc.get;
			if (getter === undefined) {
				return undefined;
			}
			return getter.call(receiver);
		}
	}
};
var _createClass2 = function () {
	/**
	 * @param {!Function} target
	 * @param {!NodeList} props
	 * @return {undefined}
	 */
	function defineProperties(target, props) {
		/** @type {number} */
		var i = 0;
		for (; i < props.length; i++) {
			var descriptor = props[i];
			descriptor.enumerable = descriptor.enumerable || false;
			/** @type {boolean} */
			descriptor.configurable = true;
			if ("value" in descriptor) {
				/** @type {boolean} */
				descriptor.writable = true;
			}
			Object.defineProperty(target, descriptor.key, descriptor);
		}
	}
	return function (Constructor, protoProps, staticProps) {
		if (protoProps) {
			defineProperties(Constructor.prototype, protoProps);
		}
		if (staticProps) {
			defineProperties(Constructor, staticProps);
		}
		return Constructor;
	};
}();
/** @type {function(string): ?} */
var _typeof2 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (expected) {
	return typeof expected;
} : function (obj) {
	return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
};
/**
 * @param {!AudioNode} instance
 * @param {!Function} Constructor
 * @return {undefined}
 */
function _classCallCheck2(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}
/**
 * @param {string} self
 * @param {string} call
 * @return {?}
 */
function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}
	return call && (typeof call === "object" || typeof call === "function") ? call : self;
}
/**
 * @param {!Object} obj
 * @param {!Object} superClass
 * @return {undefined}
 */
function _inherits(obj, superClass) {
	if (typeof superClass !== "function" && superClass !== null) {
		throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	}
	/** @type {!Object} */
	obj.prototype = Object.create(superClass && superClass.prototype, {
		constructor: {
			value: obj,
			enumerable: false,
			writable: true,
			configurable: true
		}
	});
	if (superClass) {
		if (Object.setPrototypeOf) {
			Object.setPrototypeOf(obj, superClass);
		} else {
			/** @type {!Object} */
			obj.__proto__ = superClass;
		}
	}
}
!function (data, factory) {
	if ("object" == (typeof exports === "undefined" ? "undefined" : _typeof2(exports)) && "undefined" != typeof module) {
		factory(exports);
	} else {
		if ("function" == typeof define && define.amd) {
			define(["exports"], factory);
		} else {
			factory((data = "undefined" != typeof globalThis ? globalThis : data || self).ZXing = {});
		}
	}
}(undefined, function (exports) {
	/**
	 * @return {?}
	 */
	function replace() {
		if ("undefined" != typeof window) {
			return window.BigInt || null;
		}
		if ("undefined" != typeof global) {
			return global.BigInt || null;
		}
		if ("undefined" != typeof self) {
			return self.BigInt || null;
		}
		throw new Error("Can't search globals for BigInt!");
	}
	/**
	 * @param {number} str
	 * @return {?}
	 */
	function parseInt(str) {
		if (void 0 === truncate && (truncate = replace()), null === truncate) {
			throw new Error("BigInt is not supported!");
		}
		return truncate(str);
	}
	/**
	 * @param {number} name
	 * @param {number} model
	 * @param {number} ctx
	 * @return {?}
	 */
	function callback(name, model, ctx) {
		return new Mix(name, model, ctx);
	}
	/** @type {function(!Object, ?): !Object} */
	var e = Object.setPrototypeOf || {
		__proto__: []
	} instanceof Array && function (object, proto) {
		/** @type {!Object} */
		object.__proto__ = proto;
	} || function (i, obj) {
		var o;
		for (o in obj) {
			if (obj.hasOwnProperty(o)) {
				i[o] = obj[o];
			}
		}
	};
	var t;
	var modal = function (type) {
		/**
		 * @param {?} data
		 * @return {?}
		 */
		function main(data) {
			var obj;
			var proto;
			var check;
			var ctor = this.constructor;
			var e = type.call(this, data) || this;
			return Object.defineProperty(e, "name", {
				value: ctor.name,
				enumerable: false
			}), obj = e, proto = ctor.prototype, (check = Object.setPrototypeOf) ? check(obj, proto) : obj.__proto__ = proto, function (error, type) {
				if (void 0 === type) {
					type = error.constructor;
				}
				/** @type {function((Object|null), (!Function|null)=): undefined} */
				var trace = Error.captureStackTrace;
				if (trace) {
					trace(error, type);
				}
			}(e), e;
		}
		return function (d, data) {
			/**
			 * @return {undefined}
			 */
			function BlobError() {
				/** @type {function(?): ?} */
				this.constructor = d;
			}
			e(d, data);
			d.prototype = null === data ? Object.create(data) : (BlobError.prototype = data.prototype, new BlobError);
		}(main, type), main;
	}(Error);
	var input = function (parent) {
		/**
		 * @param {string} message
		 * @return {?}
		 */
		function PubNubError(message) {
			var _this;
			_classCallCheck2(this, PubNubError);
			_this = _possibleConstructorReturn(this, (PubNubError.__proto__ || Object.getPrototypeOf(PubNubError)).call(this, message));
			_this;
			/** @type {string} */
			_this.message = message;
			return _this;
		}
		_inherits(PubNubError, parent);
		_createClass2(PubNubError, [{
			key: "getKind",
			value: function getKind() {
				return this.constructor.kind;
			}
		}]);
		return PubNubError;
	}(modal);
	/** @type {string} */
	input.kind = "Exception";
	var Renderer = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @return {?}
		 */
		function CacheLink() {
			_classCallCheck2(this, CacheLink);
			return _possibleConstructorReturn(this, (CacheLink.__proto__ || Object.getPrototypeOf(CacheLink)).apply(this, arguments));
		}
		_inherits(CacheLink, _WebInspector$GeneralTreeElement);
		return CacheLink;
	}(input);
	/** @type {string} */
	Renderer.kind = "ArgumentException";
	var Function = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @return {?}
		 */
		function CacheLink() {
			_classCallCheck2(this, CacheLink);
			return _possibleConstructorReturn(this, (CacheLink.__proto__ || Object.getPrototypeOf(CacheLink)).apply(this, arguments));
		}
		_inherits(CacheLink, _WebInspector$GeneralTreeElement);
		return CacheLink;
	}(input);
	/** @type {string} */
	Function.kind = "IllegalArgumentException";
	var Registry = function () {
		/**
		 * @param {!Object} options
		 * @return {undefined}
		 */
		function Conversation(options) {
			_classCallCheck2(this, Conversation);
			if (this.binarizer = options, null === options) {
				throw new Function("Binarizer must be non-null.");
			}
		}
		_createClass2(Conversation, [{
			key: "getWidth",
			value: function getWidth() {
				return this.binarizer.getWidth();
			}
		}, {
			key: "getHeight",
			value: function getHeight() {
				return this.binarizer.getHeight();
			}
		}, {
			key: "getBlackRow",
			value: function getBlackRow(y, row) {
				return this.binarizer.getBlackRow(y, row);
			}
		}, {
			key: "getBlackMatrix",
			value: function readyDom() {
				return null !== this.matrix && void 0 !== this.matrix || (this.matrix = this.binarizer.getBlackMatrix()), this.matrix;
			}
		}, {
			key: "isCropSupported",
			value: function isCropSupported() {
				return this.binarizer.getLuminanceSource().isCropSupported();
			}
		}, {
			key: "crop",
			value: function crop(left, top, width, height) {
				var newSource = this.binarizer.getLuminanceSource().crop(left, top, width, height);
				return new Conversation(this.binarizer.createBinarizer(newSource));
			}
		}, {
			key: "isRotateSupported",
			value: function isRotateSupported() {
				return this.binarizer.getLuminanceSource().isRotateSupported();
			}
		}, {
			key: "rotateCounterClockwise",
			value: function _conversationFactory() {
				var newSource = this.binarizer.getLuminanceSource().rotateCounterClockwise();
				return new Conversation(this.binarizer.createBinarizer(newSource));
			}
		}, {
			key: "rotateCounterClockwise45",
			value: function _conversationFactory() {
				var newSource = this.binarizer.getLuminanceSource().rotateCounterClockwise45();
				return new Conversation(this.binarizer.createBinarizer(newSource));
			}
		}, {
			key: "toString",
			value: function getArgumentString() {
				try {
					return this.getBlackMatrix().toString();
				} catch (t) {
					return "";
				}
			}
		}]);
		return Conversation;
	}();
	var Rectangle = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @return {?}
		 */
		function Literal() {
			_classCallCheck2(this, Literal);
			return _possibleConstructorReturn(this, (Literal.__proto__ || Object.getPrototypeOf(Literal)).apply(this, arguments));
		}
		_inherits(Literal, _WebInspector$GeneralTreeElement);
		_createClass2(Literal, null, [{
			key: "getChecksumInstance",
			value: function hydrateAst() {
				return new Literal;
			}
		}]);
		return Literal;
	}(input);
	/** @type {string} */
	Rectangle.kind = "ChecksumException";
	var cfg = function () {
		/**
		 * @param {string} element
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3(element) {
			_classCallCheck2(this, TempusDominusBootstrap3);
			/** @type {string} */
			this.source = element;
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "getLuminanceSource",
			value: function defaultToWhiteSpace() {
				return this.source;
			}
		}, {
			key: "getWidth",
			value: function get() {
				return this.source.getWidth();
			}
		}, {
			key: "getHeight",
			value: function get() {
				return this.source.getHeight();
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var System = function () {
		/**
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3() {
			_classCallCheck2(this, TempusDominusBootstrap3);
		}
		_createClass2(TempusDominusBootstrap3, null, [{
			key: "arraycopy",
			value: function randomIntSetPairs(numberOfPairs, density, size, intSetSource, numberOfSets) {
				for (; numberOfSets--;) {
					size[intSetSource++] = numberOfPairs[density++];
				}
			}
		}, {
			key: "currentTimeMillis",
			value: function scheduleFn() {
				return Date.now();
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var Eventful = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @return {?}
		 */
		function CacheLink() {
			_classCallCheck2(this, CacheLink);
			return _possibleConstructorReturn(this, (CacheLink.__proto__ || Object.getPrototypeOf(CacheLink)).apply(this, arguments));
		}
		_inherits(CacheLink, _WebInspector$GeneralTreeElement);
		return CacheLink;
	}(input);
	/** @type {string} */
	Eventful.kind = "IndexOutOfBoundsException";
	var Graph = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @param {number} status
		 * @param {string} message
		 * @return {?}
		 */
		function PubNubError(status, message) {
			var _this;
			_classCallCheck2(this, PubNubError);
			_this = _possibleConstructorReturn(this, (PubNubError.__proto__ || Object.getPrototypeOf(PubNubError)).call(this, message));
			_this;
			/** @type {number} */
			_this.index = status;
			/** @type {string} */
			_this.message = message;
			return _this;
		}
		_inherits(PubNubError, _WebInspector$GeneralTreeElement);
		return PubNubError;
	}(Eventful);
	/** @type {string} */
	Graph.kind = "ArrayIndexOutOfBoundsException";
	var b = function () {
		/**
		 * @return {undefined}
		 */
		function exports() {
			_classCallCheck2(this, exports);
		}
		_createClass2(exports, null, [{
			key: "fill",
			value: function traverseChildNodeTree(result, obj) {
				/** @type {number} */
				var i = 0;
				var l = result.length;
				for (; i < l; i++) {
					result[i] = obj;
				}
			}
		}, {
			key: "fillWithin",
			value: function modifierMethodExecute(data, params, method, callback) {
				exports.rangeCheck(data.length, params, method);
				var name = params;
				for (; name < method; name++) {
					data[name] = callback;
				}
			}
		}, {
			key: "rangeCheck",
			value: function callback(h, d, n) {
				if (d > n) {
					throw new Function("fromIndex(" + d + ") > toIndex(" + n + ")");
				}
				if (d < 0) {
					throw new Graph(d);
				}
				if (n > h) {
					throw new Graph(n);
				}
			}
		}, {
			key: "asList",
			value: function SignupInline() {
				/** @type {number} */
				var _len8 = arguments.length;
				/** @type {!Array} */
				var storeNames = Array(_len8);
				/** @type {number} */
				var _key8 = 0;
				for (; _key8 < _len8; _key8++) {
					storeNames[_key8] = arguments[_key8];
				}
				return storeNames;
			}
		}, {
			key: "create",
			value: function render(value, dim, count) {
				return Array.from({
					length: value
				}).map(function (canCreateDiscussions) {
					return Array.from({
						length: dim
					}).fill(count);
				});
			}
		}, {
			key: "createInt32Array",
			value: function render(value, dim, count) {
				return Array.from({
					length: value
				}).map(function (canCreateDiscussions) {
					return Int32Array.from({
						length: dim
					}).fill(count);
				});
			}
		}, {
			key: "equals",
			value: function equals(a, b) {
				if (!a) {
					return false;
				}
				if (!b) {
					return false;
				}
				if (!a.length) {
					return false;
				}
				if (!b.length) {
					return false;
				}
				if (a.length !== b.length) {
					return false;
				}
				/** @type {number} */
				var i = 0;
				var az = a.length;
				for (; i < az; i++) {
					if (a[i] !== b[i]) {
						return false;
					}
				}
				return true;
			}
		}, {
			key: "hashCode",
			value: function add(_args) {
				if (null === _args) {
					return 0;
				}
				/** @type {number} */
				var sum = 1;
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError = false;
				var _iteratorError = undefined;
				try {
					var _iterator3 = _args[Symbol.iterator]();
					var $__6;
					for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var item = $__6.value;
						sum = 31 * sum + item;
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError = true;
					_iteratorError = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError) {
							throw _iteratorError;
						}
					}
				}
				return sum;
			}
		}, {
			key: "fillUint8Array",
			value: function _toggleDeleteReady($itemElement, readyToDelete) {
				/** @type {number} */
				var outerMainProp = 0;
				for (; outerMainProp !== $itemElement.length; outerMainProp++) {
					$itemElement[outerMainProp] = readyToDelete;
				}
			}
		}, {
			key: "copyOf",
			value: function after_evaluate(callback, page) {
				return callback.slice(0, page);
			}
		}, {
			key: "copyOfUint8Array",
			value: function byteConcat(a, b) {
				if (a.length <= b) {
					/** @type {!Uint8Array} */
					var x = new Uint8Array(b);
					return x.set(a), x;
				}
				return a.slice(0, b);
			}
		}, {
			key: "copyOfRange",
			value: function $write_0(buf, off, len) {
				/** @type {number} */
				var n = len - off;
				/** @type {!Int32Array} */
				var result = new Int32Array(n);
				return System.arraycopy(buf, off, result, 0, n), result;
			}
		}, {
			key: "binarySearch",
			value: function status(args, req, write) {
				if (void 0 === write) {
					write = exports.numberComparator;
				}
				/** @type {number} */
				var pos = 0;
				/** @type {number} */
				var start = args.length - 1;
				for (; pos <= start;) {
					/** @type {number} */
					var i = start + pos >> 1;
					var _CallableLogger = write(req, args[i]);
					if (_CallableLogger > 0) {
						/** @type {number} */
						pos = i + 1;
					} else {
						if (!(_CallableLogger < 0)) {
							return i;
						}
						/** @type {number} */
						start = i - 1;
					}
				}
				return -pos - 1;
			}
		}, {
			key: "numberComparator",
			value: function LineSegment2(s, e) {
				return s - e;
			}
		}]);
		return exports;
	}();
	var scope = function () {
		/**
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3() {
			_classCallCheck2(this, TempusDominusBootstrap3);
		}
		_createClass2(TempusDominusBootstrap3, null, [{
			key: "numberOfTrailingZeros",
			value: function setToSystem(prop) {
				var p = void 0;
				if (0 === prop) {
					return 32;
				}
				/** @type {number} */
				var o = 31;
				return 0 !== (p = prop << 16) && (o = o - 16, prop = p), 0 !== (p = prop << 8) && (o = o - 8, prop = p), 0 !== (p = prop << 4) && (o = o - 4, prop = p), 0 !== (p = prop << 2) && (o = o - 2, prop = p), o - (prop << 1 >>> 31);
			}
		}, {
			key: "numberOfLeadingZeros",
			value: function setToSystem(i) {
				if (0 === i) {
					return 32;
				}
				/** @type {number} */
				var n = 1;
				return i >>> 16 == 0 && (n = n + 16, i = i << 16), i >>> 24 == 0 && (n = n + 8, i = i << 8), i >>> 28 == 0 && (n = n + 4, i = i << 4), i >>> 30 == 0 && (n = n + 2, i = i << 2), n = n - (i >>> 31);
			}
		}, {
			key: "toHexString",
			value: function strFromLabelVal(val) {
				return val.toString(16);
			}
		}, {
			key: "toBinaryString",
			value: function _validateInteger(text) {
				return String(parseInt(String(text), 2));
			}
		}, {
			key: "bitCount",
			value: function bitCount(i) {
				return i = (i = (858993459 & (i = i - (i >>> 1 & 1431655765))) + (i >>> 2 & 858993459)) + (i >>> 4) & 252645135, i = i + (i >>> 8), 63 & (i = i + (i >>> 16));
			}
		}, {
			key: "truncDivision",
			value: function Int32$div(a, b) {
				return Math.trunc(a / b);
			}
		}, {
			key: "parseInt",
			value: function (_diveTo) {
				/**
				 * @param {?} _x
				 * @param {?} _x2
				 * @return {?}
				 */
				function onPeer(_x, _x2) {
					return _diveTo.apply(this, arguments);
				}
				/**
				 * @return {string}
				 */
				onPeer.toString = function () {
					return _diveTo.toString();
				};
				return onPeer;
			}(function (id_local, radix) {
				return parseInt(id_local, radix);
			})
		}]);
		return TempusDominusBootstrap3;
	}();
	/** @type {number} */
	scope.MIN_VALUE_32_BITS = -2147483648;
	/** @type {number} */
	scope.MAX_VALUE = Number.MAX_SAFE_INTEGER;
	var BitArray = function () {
		/**
		 * @param {number} size
		 * @param {number} value
		 * @return {undefined}
		 */
		function set(size, value) {
			_classCallCheck2(this, set);
			if (void 0 === size) {
				/** @type {number} */
				this.size = 0;
				/** @type {!Int32Array} */
				this.bits = new Int32Array(1);
			} else {
				/** @type {number} */
				this.size = size;
				this.bits = null == value ? set.makeArray(size) : value;
			}
		}
		_createClass2(set, [{
			key: "getSize",
			value: function fileSize() {
				return this.size;
			}
		}, {
			key: "getSizeInBytes",
			value: function pointAsDate() {
				return Math.floor((this.size + 7) / 8);
			}
		}, {
			key: "ensureCapacity",
			value: function set(value) {
				if (value > 32 * this.bits.length) {
					var result = set.makeArray(value);
					System.arraycopy(this.bits, 0, result, 0, this.bits.length);
					this.bits = result;
				}
			}
		}, {
			key: "get",
			value: function check(t) {
				return 0 != (this.bits[Math.floor(t / 32)] & 1 << (31 & t));
			}
		}, {
			key: "set",
			value: function check(t) {
				this.bits[Math.floor(t / 32)] |= 1 << (31 & t);
			}
		}, {
			key: "flip",
			value: function check(t) {
				this.bits[Math.floor(t / 32)] ^= 1 << (31 & t);
			}
		}, {
			key: "getNextSet",
			value: function buildTree(data) {
				var max = this.size;
				if (data >= max) {
					return max;
				}
				var bits = this.bits;
				/** @type {number} */
				var i = Math.floor(data / 32);
				var current = bits[i];
				/** @type {number} */
				current = current & ~((1 << (31 & data)) - 1);
				var n = bits.length;
				for (; 0 === current;) {
					if (++i === n) {
						return max;
					}
					current = bits[i];
				}
				var number = 32 * i + scope.numberOfTrailingZeros(current);
				return number > max ? max : number;
			}
		}, {
			key: "getNextUnset",
			value: function buildTree(data) {
				var max = this.size;
				if (data >= max) {
					return max;
				}
				var bits = this.bits;
				/** @type {number} */
				var i = Math.floor(data / 32);
				/** @type {number} */
				var word = ~bits[i];
				/** @type {number} */
				word = word & ~((1 << (31 & data)) - 1);
				var n = bits.length;
				for (; 0 === word;) {
					if (++i === n) {
						return max;
					}
					/** @type {number} */
					word = ~bits[i];
				}
				var number = 32 * i + scope.numberOfTrailingZeros(word);
				return number > max ? max : number;
			}
		}, {
			key: "setBulk",
			value: function insert(d, v) {
				this.bits[Math.floor(d / 32)] = v;
			}
		}, {
			key: "setRange",
			value: function set(count, index) {
				if (index < count || count < 0 || index > this.size) {
					throw new Function;
				}
				if (index === count) {
					return;
				}
				index--;
				/** @type {number} */
				var startNo = Math.floor(count / 32);
				/** @type {number} */
				var vSectors = Math.floor(index / 32);
				var bits = this.bits;
				/** @type {number} */
				var i = startNo;
				for (; i <= vSectors; i++) {
					/** @type {number} */
					var mask = (2 << (i < vSectors ? 31 : 31 & index)) - (1 << (i > startNo ? 0 : 31 & count));
					bits[i] |= mask;
				}
			}
		}, {
			key: "clear",
			value: function check() {
				var l = this.bits.length;
				var bits = this.bits;
				/** @type {number} */
				var i = 0;
				for (; i < l; i++) {
					/** @type {number} */
					bits[i] = 0;
				}
			}
		}, {
			key: "isRange",
			value: function set(count, index, value) {
				if (index < count || count < 0 || index > this.size) {
					throw new Function;
				}
				if (index === count) {
					return true;
				}
				index--;
				/** @type {number} */
				var startNo = Math.floor(count / 32);
				/** @type {number} */
				var vSectors = Math.floor(index / 32);
				var bits = this.bits;
				/** @type {number} */
				var i = startNo;
				for (; i <= vSectors; i++) {
					/** @type {number} */
					var mask = (2 << (i < vSectors ? 31 : 31 & index)) - (1 << (i > startNo ? 0 : 31 & count)) & 4294967295;
					if ((bits[i] & mask) !== (value ? mask : 0)) {
						return false;
					}
				}
				return true;
			}
		}, {
			key: "appendBit",
			value: function _init(aInitialOptions) {
				this.ensureCapacity(this.size + 1);
				if (aInitialOptions) {
					this.bits[Math.floor(this.size / 32)] |= 1 << (31 & this.size);
				}
				this.size++;
			}
		}, {
			key: "appendBits",
			value: function init(value, numBits) {
				if (numBits < 0 || numBits > 32) {
					throw new Function("Num bits must be between 0 and 32");
				}
				this.ensureCapacity(this.size + numBits);
				/** @type {number} */
				var numBitsLeft = numBits;
				for (; numBitsLeft > 0; numBitsLeft--) {
					this.appendBit(1 == (value >> numBitsLeft - 1 & 1));
				}
			}
		}, {
			key: "appendBitArray",
			value: function handleUnexpectedData(id) {
				var len = id.size;
				this.ensureCapacity(this.size + len);
				/** @type {number} */
				var i = 0;
				for (; i < len; i++) {
					this.appendBit(id.get(i));
				}
			}
		}, {
			key: "xor",
			value: function set(data) {
				if (this.size !== data.size) {
					throw new Function("Sizes don't match");
				}
				var s = this.bits;
				/** @type {number} */
				var i = 0;
				var l = s.length;
				for (; i < l; i++) {
					s[i] ^= data.bits[i];
				}
			}
		}, {
			key: "toBytes",
			value: function write(buffer, array, i, end) {
				/** @type {number} */
				var start = 0;
				for (; start < end; start++) {
					/** @type {number} */
					var theByte = 0;
					/** @type {number} */
					var _e3 = 0;
					for (; _e3 < 8; _e3++) {
						if (this.get(buffer)) {
							/** @type {number} */
							theByte = theByte | 1 << 7 - _e3;
						}
						buffer++;
					}
					/** @type {number} */
					array[i + start] = theByte;
				}
			}
		}, {
			key: "getBitArray",
			value: function getTarget() {
				return this.bits;
			}
		}, {
			key: "reverse",
			value: function buildTree() {
				/** @type {!Int32Array} */
				var newBits = new Int32Array(this.bits.length);
				/** @type {number} */
				var len = Math.floor((this.size - 1) / 32);
				/** @type {number} */
				var oldBitsLen = len + 1;
				var known_x = this.bits;
				/** @type {number} */
				var i = 0;
				for (; i < oldBitsLen; i++) {
					var x = known_x[i];
					/** @type {number} */
					x = (x = (x = (x = (x = x >> 1 & 1431655765 | (1431655765 & x) << 1) >> 2 & 858993459 | (858993459 & x) << 2) >> 4 & 252645135 | (252645135 & x) << 4) >> 8 & 16711935 | (16711935 & x) << 8) >> 16 & 65535 | (65535 & x) << 16;
					/** @type {number} */
					newBits[len - i] = x;
				}
				if (this.size !== 32 * oldBitsLen) {
					/** @type {number} */
					var leftOffset = 32 * oldBitsLen - this.size;
					/** @type {number} */
					var currentInt = newBits[0] >>> leftOffset;
					/** @type {number} */
					var i2 = 1;
					for (; i2 < oldBitsLen; i2++) {
						/** @type {number} */
						var nextInt = newBits[i2];
						/** @type {number} */
						currentInt = currentInt | nextInt << 32 - leftOffset;
						/** @type {number} */
						newBits[i2 - 1] = currentInt;
						/** @type {number} */
						currentInt = nextInt >>> leftOffset;
					}
					/** @type {number} */
					newBits[oldBitsLen - 1] = currentInt;
				}
				/** @type {!Int32Array} */
				this.bits = newBits;
			}
		}, {
			key: "equals",
			value: function equals(s) {
				if (!(s instanceof set)) {
					return false;
				}
				/** @type {string} */
				var self = s;
				return this.size === self.size && b.equals(this.bits, self.bits);
			}
		}, {
			key: "hashCode",
			value: function animate() {
				return 31 * this.size + b.hashCode(this.bits);
			}
		}, {
			key: "toString",
			value: function renderDebugScene() {
				/** @type {string} */
				var result = "";
				/** @type {number} */
				var i = 0;
				var size = this.size;
				for (; i < size; i++) {
					if (0 == (7 & i)) {
						/** @type {string} */
						result = result + " ";
					}
					/** @type {string} */
					result = result + (this.get(i) ? "X" : ".");
				}
				return result;
			}
		}, {
			key: "clone",
			value: function parseOptions() {
				return new set(this.size, this.bits.slice());
			}
		}], [{
			key: "makeArray",
			value: function deserialize(sPresenterData) {
				return new Int32Array(Math.floor((sPresenterData + 31) / 32));
			}
		}]);
		return set;
	}();
	!function (PreparsedElementType) {
		/** @type {string} */
		PreparsedElementType[PreparsedElementType.OTHER = 0] = "OTHER";
		/** @type {string} */
		PreparsedElementType[PreparsedElementType.PURE_BARCODE = 1] = "PURE_BARCODE";
		/** @type {string} */
		PreparsedElementType[PreparsedElementType.POSSIBLE_FORMATS = 2] = "POSSIBLE_FORMATS";
		/** @type {string} */
		PreparsedElementType[PreparsedElementType.TRY_HARDER = 3] = "TRY_HARDER";
		/** @type {string} */
		PreparsedElementType[PreparsedElementType.CHARACTER_SET = 4] = "CHARACTER_SET";
		/** @type {string} */
		PreparsedElementType[PreparsedElementType.ALLOWED_LENGTHS = 5] = "ALLOWED_LENGTHS";
		/** @type {string} */
		PreparsedElementType[PreparsedElementType.ASSUME_CODE_39_CHECK_DIGIT = 6] = "ASSUME_CODE_39_CHECK_DIGIT";
		/** @type {string} */
		PreparsedElementType[PreparsedElementType.ASSUME_GS1 = 7] = "ASSUME_GS1";
		/** @type {string} */
		PreparsedElementType[PreparsedElementType.RETURN_CODABAR_START_END = 8] = "RETURN_CODABAR_START_END";
		/** @type {string} */
		PreparsedElementType[PreparsedElementType.NEED_RESULT_POINT_CALLBACK = 9] = "NEED_RESULT_POINT_CALLBACK";
		/** @type {string} */
		PreparsedElementType[PreparsedElementType.ALLOWED_EAN_EXTENSIONS = 10] = "ALLOWED_EAN_EXTENSIONS";
	}(t || (t = {}));
	var state;
	/** @type {(undefined|{ALLOWED_EAN_EXTENSIONS: number, ALLOWED_LENGTHS: number, ASSUME_CODE_39_CHECK_DIGIT: number, ASSUME_GS1: number, CHARACTER_SET: number, NEED_RESULT_POINT_CALLBACK: number, OTHER: number, POSSIBLE_FORMATS: number, PURE_BARCODE: number, RETURN_CODABAR_START_END: number, TRY_HARDER: number})} */
	var node = t;
	var Date = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @return {?}
		 */
		function Literal() {
			_classCallCheck2(this, Literal);
			return _possibleConstructorReturn(this, (Literal.__proto__ || Object.getPrototypeOf(Literal)).apply(this, arguments));
		}
		_inherits(Literal, _WebInspector$GeneralTreeElement);
		_createClass2(Literal, null, [{
			key: "getFormatInstance",
			value: function hydrateAst() {
				return new Literal;
			}
		}]);
		return Literal;
	}(input);
	/** @type {string} */
	Date.kind = "FormatException";
	(function (_) {
		/** @type {string} */
		_[_.Cp437 = 0] = "Cp437";
		/** @type {string} */
		_[_.ISO8859_1 = 1] = "ISO8859_1";
		/** @type {string} */
		_[_.ISO8859_2 = 2] = "ISO8859_2";
		/** @type {string} */
		_[_.ISO8859_3 = 3] = "ISO8859_3";
		/** @type {string} */
		_[_.ISO8859_4 = 4] = "ISO8859_4";
		/** @type {string} */
		_[_.ISO8859_5 = 5] = "ISO8859_5";
		/** @type {string} */
		_[_.ISO8859_6 = 6] = "ISO8859_6";
		/** @type {string} */
		_[_.ISO8859_7 = 7] = "ISO8859_7";
		/** @type {string} */
		_[_.ISO8859_8 = 8] = "ISO8859_8";
		/** @type {string} */
		_[_.ISO8859_9 = 9] = "ISO8859_9";
		/** @type {string} */
		_[_.ISO8859_10 = 10] = "ISO8859_10";
		/** @type {string} */
		_[_.ISO8859_11 = 11] = "ISO8859_11";
		/** @type {string} */
		_[_.ISO8859_13 = 12] = "ISO8859_13";
		/** @type {string} */
		_[_.ISO8859_14 = 13] = "ISO8859_14";
		/** @type {string} */
		_[_.ISO8859_15 = 14] = "ISO8859_15";
		/** @type {string} */
		_[_.ISO8859_16 = 15] = "ISO8859_16";
		/** @type {string} */
		_[_.SJIS = 16] = "SJIS";
		/** @type {string} */
		_[_.Cp1250 = 17] = "Cp1250";
		/** @type {string} */
		_[_.Cp1251 = 18] = "Cp1251";
		/** @type {string} */
		_[_.Cp1252 = 19] = "Cp1252";
		/** @type {string} */
		_[_.Cp1256 = 20] = "Cp1256";
		/** @type {string} */
		_[_.UnicodeBigUnmarked = 21] = "UnicodeBigUnmarked";
		/** @type {string} */
		_[_.UTF8 = 22] = "UTF8";
		/** @type {string} */
		_[_.ASCII = 23] = "ASCII";
		/** @type {string} */
		_[_.Big5 = 24] = "Big5";
		/** @type {string} */
		_[_.GB18030 = 25] = "GB18030";
		/** @type {string} */
		_[_.EUC_KR = 26] = "EUC_KR";
	})(state || (state = {}));
	var options = function () {
		/**
		 * @param {undefined} name
		 * @param {string} n
		 * @param {!Object} type
		 * @return {undefined}
		 */
		function Function(name, n, type) {
			_classCallCheck2(this, Function);
			/** @type {number} */
			var _len = arguments.length;
			/** @type {!Array} */
			var fields = Array(_len > 3 ? _len - 3 : 0);
			/** @type {number} */
			var _key = 3;
			for (; _key < _len; _key++) {
				fields[_key - 3] = arguments[_key];
			}
			this.valueIdentifier = name;
			/** @type {!Object} */
			this.name = type;
			this.values = "number" == typeof n ? Int32Array.from([n]) : n;
			/** @type {!Array} */
			this.otherEncodingNames = fields;
			Function.VALUE_IDENTIFIER_TO_ECI.set(name, this);
			Function.NAME_TO_ECI.set(type, this);
			var d = this.values;
			/** @type {number} */
			var i = 0;
			var dateL = d.length;
			for (; i !== dateL; i++) {
				var num = d[i];
				Function.VALUES_TO_ECI.set(num, this);
			}
			/** @type {boolean} */
			var _iteratorNormalCompletion3 = true;
			/** @type {boolean} */
			var _didIteratorError2 = false;
			var _iteratorError2 = undefined;
			try {
				var _iterator3 = fields[Symbol.iterator]();
				var $__6;
				for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var item = $__6.value;
					Function.NAME_TO_ECI.set(item, this);
				}
			} catch (err) {
				/** @type {boolean} */
				_didIteratorError2 = true;
				_iteratorError2 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError2) {
						throw _iteratorError2;
					}
				}
			}
		}
		_createClass2(Function, [{
			key: "getValueIdentifier",
			value: function getValueIdentifier() {
				return this.valueIdentifier;
			}
		}, {
			key: "getName",
			value: function getName() {
				return this.name;
			}
		}, {
			key: "getValue",
			value: function getValue() {
				return this.values[0];
			}
		}, {
			key: "equals",
			value: function rename(callback) {
				if (!(callback instanceof Function)) {
					return false;
				}
				/** @type {(Object|string)} */
				var orig = callback;
				return this.getName() === orig.getName();
			}
		}], [{
			key: "getCharacterSetECIByValue",
			value: function versionValue(status) {
				if (status < 0 || status >= 900) {
					throw new Date("incorect value");
				}
				var header = Function.VALUES_TO_ECI.get(status);
				if (void 0 === header) {
					throw new Date("incorect value");
				}
				return header;
			}
		}, {
			key: "getCharacterSetECIByName",
			value: function constructor(styleSheetId) {
				var header = Function.NAME_TO_ECI.get(styleSheetId);
				if (void 0 === header) {
					throw new Date("incorect value");
				}
				return header;
			}
		}]);
		return Function;
	}();
	/** @type {!Map} */
	options.VALUE_IDENTIFIER_TO_ECI = new Map;
	/** @type {!Map} */
	options.VALUES_TO_ECI = new Map;
	/** @type {!Map} */
	options.NAME_TO_ECI = new Map;
	options.Cp437 = new options(state.Cp437, Int32Array.from([0, 2]), "Cp437");
	options.ISO8859_1 = new options(state.ISO8859_1, Int32Array.from([1, 3]), "ISO-8859-1", "ISO88591", "ISO8859_1");
	options.ISO8859_2 = new options(state.ISO8859_2, 4, "ISO-8859-2", "ISO88592", "ISO8859_2");
	options.ISO8859_3 = new options(state.ISO8859_3, 5, "ISO-8859-3", "ISO88593", "ISO8859_3");
	options.ISO8859_4 = new options(state.ISO8859_4, 6, "ISO-8859-4", "ISO88594", "ISO8859_4");
	options.ISO8859_5 = new options(state.ISO8859_5, 7, "ISO-8859-5", "ISO88595", "ISO8859_5");
	options.ISO8859_6 = new options(state.ISO8859_6, 8, "ISO-8859-6", "ISO88596", "ISO8859_6");
	options.ISO8859_7 = new options(state.ISO8859_7, 9, "ISO-8859-7", "ISO88597", "ISO8859_7");
	options.ISO8859_8 = new options(state.ISO8859_8, 10, "ISO-8859-8", "ISO88598", "ISO8859_8");
	options.ISO8859_9 = new options(state.ISO8859_9, 11, "ISO-8859-9", "ISO88599", "ISO8859_9");
	options.ISO8859_10 = new options(state.ISO8859_10, 12, "ISO-8859-10", "ISO885910", "ISO8859_10");
	options.ISO8859_11 = new options(state.ISO8859_11, 13, "ISO-8859-11", "ISO885911", "ISO8859_11");
	options.ISO8859_13 = new options(state.ISO8859_13, 15, "ISO-8859-13", "ISO885913", "ISO8859_13");
	options.ISO8859_14 = new options(state.ISO8859_14, 16, "ISO-8859-14", "ISO885914", "ISO8859_14");
	options.ISO8859_15 = new options(state.ISO8859_15, 17, "ISO-8859-15", "ISO885915", "ISO8859_15");
	options.ISO8859_16 = new options(state.ISO8859_16, 18, "ISO-8859-16", "ISO885916", "ISO8859_16");
	options.SJIS = new options(state.SJIS, 20, "SJIS", "Shift_JIS");
	options.Cp1250 = new options(state.Cp1250, 21, "Cp1250", "windows-1250");
	options.Cp1251 = new options(state.Cp1251, 22, "Cp1251", "windows-1251");
	options.Cp1252 = new options(state.Cp1252, 23, "Cp1252", "windows-1252");
	options.Cp1256 = new options(state.Cp1256, 24, "Cp1256", "windows-1256");
	options.UnicodeBigUnmarked = new options(state.UnicodeBigUnmarked, 25, "UnicodeBigUnmarked", "UTF-16BE", "UnicodeBig");
	options.UTF8 = new options(state.UTF8, 26, "UTF8", "UTF-8");
	options.ASCII = new options(state.ASCII, Int32Array.from([27, 170]), "ASCII", "US-ASCII");
	options.Big5 = new options(state.Big5, 28, "Big5");
	options.GB18030 = new options(state.GB18030, 29, "GB18030", "GB2312", "EUC_CN", "GBK");
	options.EUC_KR = new options(state.EUC_KR, 30, "EUC_KR", "EUC-KR");
	var buffer = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @return {?}
		 */
		function CacheLink() {
			_classCallCheck2(this, CacheLink);
			return _possibleConstructorReturn(this, (CacheLink.__proto__ || Object.getPrototypeOf(CacheLink)).apply(this, arguments));
		}
		_inherits(CacheLink, _WebInspector$GeneralTreeElement);
		return CacheLink;
	}(input);
	/** @type {string} */
	buffer.kind = "UnsupportedOperationException";
	var parser = function () {
		/**
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3() {
			_classCallCheck2(this, TempusDominusBootstrap3);
		}
		_createClass2(TempusDominusBootstrap3, null, [{
			key: "decode",
			value: function onSuccess(data, address) {
				var fromCharset = this.encodingName(address);
				return this.customDecoder ? this.customDecoder(data, fromCharset) : "undefined" == typeof TextDecoder || this.shouldDecodeOnFallback(fromCharset) ? this.decodeFallback(data, fromCharset) : (new TextDecoder(fromCharset)).decode(data);
			}
		}, {
			key: "shouldDecodeOnFallback",
			value: function unregisterDevice(callback) {
				return !TempusDominusBootstrap3.isBrowser() && "ISO-8859-1" === callback;
			}
		}, {
			key: "encode",
			value: function encode(data, level) {
				var numberOfTiles = this.encodingName(level);
				return this.customEncoder ? this.customEncoder(data, numberOfTiles) : "undefined" == typeof TextEncoder ? this.encodeFallback(data) : (new TextEncoder).encode(data);
			}
		}, {
			key: "isBrowser",
			value: function devModeCheck() {
				return "undefined" != typeof window && "[object Window]" === {}.toString.call(window);
			}
		}, {
			key: "encodingName",
			value: function buildElemKey(index) {
				return "string" == typeof index ? index : index.getName();
			}
		}, {
			key: "encodingCharacterSet",
			value: function isLikeHSLA(t) {
				return t instanceof options ? t : options.getCharacterSetECIByName(t);
			}
		}, {
			key: "decodeFallback",
			value: function parseValue(array, value) {
				var label = this.encodingCharacterSet(value);
				if (TempusDominusBootstrap3.isDecodeFallbackSupported(label)) {
					/** @type {string} */
					var url = "";
					/** @type {number} */
					var i = 0;
					var length = array.length;
					for (; i < length; i++) {
						var filename = array[i].toString(16);
						if (filename.length < 2) {
							/** @type {string} */
							filename = "0" + filename;
						}
						/** @type {string} */
						url = url + ("%" + filename);
					}
					return decodeURIComponent(url);
				}
				if (label.equals(options.UnicodeBigUnmarked)) {
					return String.fromCharCode.apply(null, new Uint16Array(array.buffer));
				}
				throw new buffer("Encoding " + this.encodingName(value) + " not supported by fallback.");
			}
		}, {
			key: "isDecodeFallbackSupported",
			value: function storeOrDirty(key) {
				return key.equals(options.UTF8) || key.equals(options.ISO8859_1) || key.equals(options.ASCII);
			}
		}, {
			key: "encodeFallback",
			value: function stringToUint(s) {
				/** @type {!Array<string>} */
				var hashCharacters = btoa(unescape(encodeURIComponent(s))).split("");
				/** @type {!Array} */
				var sentData = [];
				/** @type {number} */
				var i = 0;
				for (; i < hashCharacters.length; i++) {
					sentData.push(hashCharacters[i].charCodeAt(0));
				}
				return new Uint8Array(sentData);
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var _ = function () {
		/**
		 * @return {undefined}
		 */
		function _() {
			_classCallCheck2(this, _);
		}
		_createClass2(_, null, [{
			key: "castAsNonUtf8Char",
			value: function write(byte) {
				var segment = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
				var parent = segment ? segment.getName() : this.ISO88591;
				return parser.decode(new Uint8Array([byte]), parent);
			}
		}, {
			key: "guessEncoding",
			value: function fn(index, script) {
				if (null != script && void 0 !== script.get(node.CHARACTER_SET)) {
					return script.get(node.CHARACTER_SET).toString();
				}
				var count = index.length;
				/** @type {boolean} */
				var minDate = true;
				/** @type {boolean} */
				var maxDate = true;
				/** @type {boolean} */
				var canBeUTF8 = true;
				/** @type {number} */
				var utf8BytesLeft = 0;
				/** @type {number} */
				var a = 0;
				/** @type {number} */
				var b = 0;
				/** @type {number} */
				var c = 0;
				/** @type {number} */
				var date = 0;
				/** @type {number} */
				var u = 0;
				/** @type {number} */
				var sjisCurKatakanaWordLength = 0;
				/** @type {number} */
				var mid = 0;
				/** @type {number} */
				var sjisMaxKatakanaWordLength = 0;
				/** @type {number} */
				var high = 0;
				/** @type {number} */
				var page = 0;
				/** @type {boolean} */
				var utf8bom = index.length > 3 && 239 === index[0] && 187 === index[1] && 191 === index[2];
				/** @type {number} */
				var i = 0;
				for (; i < count && (minDate || maxDate || canBeUTF8); i++) {
					/** @type {number} */
					var _r15 = 255 & index[i];
					if (canBeUTF8) {
						if (utf8BytesLeft > 0) {
							if (0 == (128 & _r15)) {
								/** @type {boolean} */
								canBeUTF8 = false;
							} else {
								utf8BytesLeft--;
							}
						} else {
							if (0 != (128 & _r15)) {
								if (0 == (64 & _r15)) {
									/** @type {boolean} */
									canBeUTF8 = false;
								} else {
									utf8BytesLeft++;
									if (0 == (32 & _r15)) {
										a++;
									} else {
										utf8BytesLeft++;
										if (0 == (16 & _r15)) {
											b++;
										} else {
											utf8BytesLeft++;
											if (0 == (8 & _r15)) {
												c++;
											} else {
												/** @type {boolean} */
												canBeUTF8 = false;
											}
										}
									}
								}
							}
						}
					}
					if (minDate) {
						if (_r15 > 127 && _r15 < 160) {
							/** @type {boolean} */
							minDate = false;
						} else {
							if (_r15 > 159 && (_r15 < 192 || 215 === _r15 || 247 === _r15)) {
								page++;
							}
						}
					}
					if (maxDate) {
						if (date > 0) {
							if (_r15 < 64 || 127 === _r15 || _r15 > 252) {
								/** @type {boolean} */
								maxDate = false;
							} else {
								date--;
							}
						} else {
							if (128 === _r15 || 160 === _r15 || _r15 > 239) {
								/** @type {boolean} */
								maxDate = false;
							} else {
								if (_r15 > 160 && _r15 < 224) {
									u++;
									/** @type {number} */
									mid = 0;
									if (++sjisCurKatakanaWordLength > sjisMaxKatakanaWordLength) {
										/** @type {number} */
										sjisMaxKatakanaWordLength = sjisCurKatakanaWordLength;
									}
								} else {
									if (_r15 > 127) {
										date++;
										/** @type {number} */
										sjisCurKatakanaWordLength = 0;
										if (++mid > high) {
											/** @type {number} */
											high = mid;
										}
									} else {
										/** @type {number} */
										sjisCurKatakanaWordLength = 0;
										/** @type {number} */
										mid = 0;
									}
								}
							}
						}
					}
				}
				return canBeUTF8 && utf8BytesLeft > 0 && (canBeUTF8 = false), maxDate && date > 0 && (maxDate = false), canBeUTF8 && (utf8bom || a + b + c > 0) ? _.UTF8 : maxDate && (_.ASSUME_SHIFT_JIS || sjisMaxKatakanaWordLength >= 3 || high >= 3) ? _.SHIFT_JIS : minDate && maxDate ? 2 === sjisMaxKatakanaWordLength && 2 === u || 10 * page >= count ? _.SHIFT_JIS : _.ISO88591 : minDate ? _.ISO88591 : maxDate ? _.SHIFT_JIS : canBeUTF8 ? _.UTF8 : _.PLATFORM_DEFAULT_ENCODING;
			}
		}, {
			key: "format",
			value: function format(_key) {
				/** @type {number} */
				var _len3 = arguments.length;
				/** @type {!Array} */
				var args = Array(_len3 > 1 ? _len3 - 1 : 0);
				/** @type {number} */
				var _key3 = 1;
				for (; _key3 < _len3; _key3++) {
					args[_key3 - 1] = arguments[_key3];
				}
				/** @type {number} */
				var index = -1;
				return _key.replace(/%(-)?(0?[0-9]+)?([.][0-9]+)?([#][0-9]+)?([scfpexd%])/g, function (precision, undefined, dir, s, widthStr, type) {
					if ("%%" === precision) {
						return "%";
					}
					if (void 0 === args[++index]) {
						return;
					}
					/** @type {(number|undefined)} */
					precision = s ? parseInt(s.substr(1)) : void 0;
					var val = void 0;
					/** @type {(number|undefined)} */
					var radix = widthStr ? parseInt(widthStr.substr(1)) : void 0;
					switch (type) {
						case "s":
							val = args[index];
							break;
						case "c":
							val = args[index][0];
							break;
						case "f":
							/** @type {string} */
							val = parseFloat(args[index]).toFixed(precision);
							break;
						case "p":
							/** @type {string} */
							val = parseFloat(args[index]).toPrecision(precision);
							break;
						case "e":
							/** @type {string} */
							val = parseFloat(args[index]).toExponential(precision);
							break;
						case "x":
							/** @type {string} */
							val = parseInt(args[index]).toString(radix || 16);
							break;
						case "d":
							/** @type {string} */
							val = parseFloat(parseInt(args[index], radix || 10).toPrecision(precision)).toFixed(0);
					}
					/** @type {string} */
					val = "object" == (typeof val === "undefined" ? "undefined" : _typeof2(val)) ? JSON.stringify(val) : (+val).toString(radix);
					/** @type {number} */
					var n = parseInt(dir);
					/** @type {string} */
					var i = dir && dir[0] + "" == "0" ? "0" : " ";
					for (; val.length < n;) {
						/** @type {string} */
						val = void 0 !== undefined ? val + i : i + val;
					}
					return val;
				});
			}
		}, {
			key: "getBytes",
			value: function encode(src, offset) {
				return parser.encode(src, offset);
			}
		}, {
			key: "getCharCode",
			value: function getByte(str) {
				var i = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
				return str.charCodeAt(i);
			}
		}, {
			key: "getCharAt",
			value: function getCharAt(offset) {
				return String.fromCharCode(offset);
			}
		}]);
		return _;
	}();
	_.SHIFT_JIS = options.SJIS.getName();
	/** @type {string} */
	_.GB2312 = "GB2312";
	_.ISO88591 = options.ISO8859_1.getName();
	/** @type {string} */
	_.EUC_JP = "EUC_JP";
	_.UTF8 = options.UTF8.getName();
	_.PLATFORM_DEFAULT_ENCODING = _.UTF8;
	/** @type {boolean} */
	_.ASSUME_SHIFT_JIS = false;
	var Buffer = function () {
		/**
		 * @return {undefined}
		 */
		function Almanac() {
			var last_value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
			_classCallCheck2(this, Almanac);
			this.value = last_value;
		}
		_createClass2(Almanac, [{
			key: "enableDecoding",
			value: function generateDocs(callback) {
				return this.encoding = callback, this;
			}
		}, {
			key: "append",
			value: function append(c) {
				return "string" == typeof c ? this.value += c.toString() : this.encoding ? this.value += _.castAsNonUtf8Char(c, this.encoding) : this.value += String.fromCharCode(c), this;
			}
		}, {
			key: "appendChars",
			value: function addPlate(node, y, h) {
				/** @type {string} */
				var i = y;
				for (; y < y + h; i++) {
					this.append(node[i]);
				}
				return this;
			}
		}, {
			key: "length",
			value: function length() {
				return this.value.length;
			}
		}, {
			key: "charAt",
			value: function next(i) {
				return this.value.charAt(i);
			}
		}, {
			key: "deleteCharAt",
			value: function keyboardHandlerInput(i) {
				this.value = this.value.substr(0, i) + this.value.substring(i + 1);
			}
		}, {
			key: "setCharAt",
			value: function setCharAt(index, chr) {
				this.value = this.value.substr(0, index) + chr + this.value.substr(index + 1);
			}
		}, {
			key: "substring",
			value: function unGroupSelector(index, match) {
				return this.value.substring(index, match);
			}
		}, {
			key: "setLengthToZero",
			value: function _checkNextWizard() {
				/** @type {string} */
				this.value = "";
			}
		}, {
			key: "toString",
			value: function getUnifiedOperator() {
				return this.value;
			}
		}, {
			key: "insert",
			value: function insert(index, value) {
				this.value = this.value.substr(0, index) + value + this.value.substr(index + value.length);
			}
		}]);
		return Almanac;
	}();
	var Image = function () {
		/**
		 * @param {number} width
		 * @param {number} height
		 * @param {number} rowSize
		 * @param {number} bits
		 * @return {undefined}
		 */
		function Node(width, height, rowSize, bits) {
			_classCallCheck2(this, Node);
			if (this.width = width, this.height = height, this.rowSize = rowSize, this.bits = bits, null == height && (height = width), this.height = height, width < 1 || height < 1) {
				throw new Function("Both dimensions must be greater than 0");
			}
			if (null == rowSize) {
				/** @type {number} */
				rowSize = Math.floor((width + 31) / 32);
			}
			/** @type {number} */
			this.rowSize = rowSize;
			if (null == bits) {
				/** @type {!Int32Array} */
				this.bits = new Int32Array(this.rowSize * this.height);
			}
		}
		_createClass2(Node, [{
			key: "get",
			value: function BitMatrix(width, height) {
				/** @type {number} */
				var i = height * this.rowSize + Math.floor(width / 32);
				return 0 != (this.bits[i] >>> (31 & width) & 1);
			}
		}, {
			key: "set",
			value: function BitMatrix(width, height) {
				/** @type {number} */
				var i = height * this.rowSize + Math.floor(width / 32);
				this.bits[i] |= 1 << (31 & width) & 4294967295;
			}
		}, {
			key: "unset",
			value: function BitMatrix(width, height) {
				/** @type {number} */
				var i = height * this.rowSize + Math.floor(width / 32);
				this.bits[i] &= ~(1 << (31 & width) & 4294967295);
			}
		}, {
			key: "flip",
			value: function BitMatrix(width, height) {
				/** @type {number} */
				var i = height * this.rowSize + Math.floor(width / 32);
				this.bits[i] ^= 1 << (31 & width) & 4294967295;
			}
		}, {
			key: "xor",
			value: function getRect(matrix) {
				if (this.width !== matrix.getWidth() || this.height !== matrix.getHeight() || this.rowSize !== matrix.getRowSize()) {
					throw new Function("input matrix dimensions do not match");
				}
				var v = new BitArray(Math.floor(this.width / 32) + 1);
				var size = this.rowSize;
				var plaintext = this.bits;
				/** @type {number} */
				var row = 0;
				var height = this.height;
				for (; row < height; row++) {
					/** @type {number} */
					var i = row * size;
					var xorSegment = matrix.getRow(row, v).getBitArray();
					/** @type {number} */
					var j = 0;
					for (; j < size; j++) {
						plaintext[i + j] ^= xorSegment[j];
					}
				}
			}
		}, {
			key: "clear",
			value: function check() {
				var bits = this.bits;
				var n = bits.length;
				/** @type {number} */
				var i = 0;
				for (; i < n; i++) {
					/** @type {number} */
					bits[i] = 0;
				}
			}
		}, {
			key: "setRegion",
			value: function set(left, top, width, height) {
				if (top < 0 || left < 0) {
					throw new Function("Left and top must be nonnegative");
				}
				if (height < 1 || width < 1) {
					throw new Function("Height and width must be at least 1");
				}
				var right = left + width;
				var bottom = top + height;
				if (bottom > this.height || right > this.width) {
					throw new Function("The region must fit inside the matrix");
				}
				var resolution = this.rowSize;
				var p_displayOptions = this.bits;
				/** @type {number} */
				var y = top;
				for (; y < bottom; y++) {
					/** @type {number} */
					var offset = y * resolution;
					/** @type {number} */
					var x = left;
					for (; x < right; x++) {
						p_displayOptions[offset + Math.floor(x / 32)] |= 1 << (31 & x) & 4294967295;
					}
				}
			}
		}, {
			key: "getRow",
			value: function parse(height, array) {
				if (null == array || array.getSize() < this.width) {
					array = new BitArray(this.width);
				} else {
					array.clear();
				}
				var length = this.rowSize;
				var tags = this.bits;
				/** @type {number} */
				var index = height * length;
				/** @type {number} */
				var i = 0;
				for (; i < length; i++) {
					array.setBulk(32 * i, tags[index + i]);
				}
				return array;
			}
		}, {
			key: "setRow",
			value: function BitMatrix(height, rowSize) {
				System.arraycopy(rowSize.getBitArray(), 0, this.bits, height * this.rowSize, this.rowSize);
			}
		}, {
			key: "rotate180",
			value: function prepare() {
				var width = this.getWidth();
				var height = this.getHeight();
				var topRow = new BitArray(width);
				var bottomRow = new BitArray(width);
				/** @type {number} */
				var i = 0;
				/** @type {number} */
				var cell_amount = Math.floor((height + 1) / 2);
				for (; i < cell_amount; i++) {
					topRow = this.getRow(i, topRow);
					bottomRow = this.getRow(height - 1 - i, bottomRow);
					topRow.reverse();
					bottomRow.reverse();
					this.setRow(i, bottomRow);
					this.setRow(height - 1 - i, topRow);
				}
			}
		}, {
			key: "getEnclosingRectangle",
			value: function BitMatrix() {
				var wid = this.width;
				var h = this.height;
				var M = this.rowSize;
				var bits = this.bits;
				var len = wid;
				var p = h;
				/** @type {number} */
				var j = -1;
				/** @type {number} */
				var i = -1;
				/** @type {number} */
				var k = 0;
				for (; k < h; k++) {
					/** @type {number} */
					var l = 0;
					for (; l < M; l++) {
						var i = bits[k * M + l];
						if (0 !== i) {
							if (k < p && (p = k), k > i && (i = k), 32 * l < len) {
								/** @type {number} */
								var j = 0;
								for (; 0 == (i << 31 - j & 4294967295);) {
									j++;
								}
								if (32 * l + j < len) {
									/** @type {number} */
									len = 32 * l + j;
								}
							}
							if (32 * l + 31 > j) {
								/** @type {number} */
								var w = 31;
								for (; i >>> w == 0;) {
									w--;
								}
								if (32 * l + w > j) {
									/** @type {number} */
									j = 32 * l + w;
								}
							}
						}
					}
				}
				return j < len || i < p ? null : Int32Array.from([len, p, j - len + 1, i - p + 1]);
			}
		}, {
			key: "getTopLeftOnBit",
			value: function check() {
				var r = this.rowSize;
				var line = this.bits;
				/** @type {number} */
				var l = 0;
				for (; l < line.length && 0 === line[l];) {
					l++;
				}
				if (l === line.length) {
					return null;
				}
				/** @type {number} */
				var n = l / r;
				/** @type {number} */
				var input = l % r * 32;
				var handler = line[l];
				/** @type {number} */
				var padString = 0;
				for (; 0 == (handler << 31 - padString & 4294967295);) {
					padString++;
				}
				return input = input + padString, Int32Array.from([input, n]);
			}
		}, {
			key: "getBottomRightOnBit",
			value: function check() {
				var boardSize = this.rowSize;
				var gridColumns = this.bits;
				/** @type {number} */
				var cellIndex = gridColumns.length - 1;
				for (; cellIndex >= 0 && 0 === gridColumns[cellIndex];) {
					cellIndex--;
				}
				if (cellIndex < 0) {
					return null;
				}
				/** @type {number} */
				var k2 = Math.floor(cellIndex / boardSize);
				/** @type {number} */
				var input = 32 * Math.floor(cellIndex % boardSize);
				var column = gridColumns[cellIndex];
				/** @type {number} */
				var padString = 31;
				for (; column >>> padString == 0;) {
					padString--;
				}
				return input = input + padString, Int32Array.from([input, k2]);
			}
		}, {
			key: "getWidth",
			value: function get_width() {
				return this.width;
			}
		}, {
			key: "getHeight",
			value: function get_height() {
				return this.height;
			}
		}, {
			key: "getRowSize",
			value: function getRowSize() {
				return this.rowSize;
			}
		}, {
			key: "equals",
			value: function equals(object) {
				if (!(object instanceof Node)) {
					return false;
				}
				/** @type {string} */
				var other = object;
				return this.width === other.width && this.height === other.height && this.rowSize === other.rowSize && b.equals(this.bits, other.bits);
			}
		}, {
			key: "hashCode",
			value: function BitMatrix() {
				var width = this.width;
				return width = 31 * (width = 31 * (width = 31 * (width = 31 * width + this.width) + this.height) + this.rowSize) + b.hashCode(this.bits);
			}
		}, {
			key: "toString",
			value: function writeTextArgs() {
				var GET_AUTH_URL_TIMEOUT = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "X ";
				var e = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "  ";
				var artistTrack = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "\n";
				return this.buildToString(GET_AUTH_URL_TIMEOUT, e, artistTrack);
			}
		}, {
			key: "buildToString",
			value: function exports(undefined, value, e) {
				var res = new Buffer;
				/** @type {number} */
				var key = 0;
				var SH = this.height;
				for (; key < SH; key++) {
					/** @type {number} */
					var x = 0;
					var wid = this.width;
					for (; x < wid; x++) {
						res.append(this.get(x, key) ? undefined : value);
					}
					res.append(e);
				}
				return res.toString();
			}
		}, {
			key: "clone",
			value: function PNG() {
				return new Node(this.width, this.height, this.rowSize, this.bits.slice());
			}
		}], [{
			key: "parseFromBooleanArray",
			value: function search(t) {
				var ss = t.length;
				var pos = t[0].length;
				var text = new Node(pos, ss);
				/** @type {number} */
				var s = 0;
				for (; s < ss; s++) {
					var val = t[s];
					/** @type {number} */
					var index = 0;
					for (; index < pos; index++) {
						if (val[index]) {
							text.set(index, s);
						}
					}
				}
				return text;
			}
		}, {
			key: "parseFromString",
			value: function init(line, word, id) {
				if (null === line) {
					throw new Function("stringRepresentation cannot be null");
				}
				/** @type {!Array} */
				var options = new Array(line.length);
				/** @type {number} */
				var threshold = 0;
				/** @type {number} */
				var autoupgrade_hp_threshold = 0;
				/** @type {number} */
				var a = -1;
				/** @type {number} */
				var startAccel = 0;
				/** @type {number} */
				var j = 0;
				for (; j < line.length;) {
					if ("\n" === line.charAt(j) || "\r" === line.charAt(j)) {
						if (threshold > autoupgrade_hp_threshold) {
							if (-1 === a) {
								/** @type {number} */
								a = threshold - autoupgrade_hp_threshold;
							} else {
								if (threshold - autoupgrade_hp_threshold !== a) {
									throw new Function("row lengths do not match");
								}
							}
							/** @type {number} */
							autoupgrade_hp_threshold = threshold;
							startAccel++;
						}
						j++;
					} else {
						if (line.substring(j, j + word.length) === word) {
							j = j + word.length;
							/** @type {boolean} */
							options[threshold] = true;
							threshold++;
						} else {
							if (line.substring(j, j + id.length) !== id) {
								throw new Function("illegal character encountered: " + line.substring(j));
							}
							j = j + id.length;
							/** @type {boolean} */
							options[threshold] = false;
							threshold++;
						}
					}
				}
				if (threshold > autoupgrade_hp_threshold) {
					if (-1 === a) {
						/** @type {number} */
						a = threshold - autoupgrade_hp_threshold;
					} else {
						if (threshold - autoupgrade_hp_threshold !== a) {
							throw new Function("row lengths do not match");
						}
					}
					startAccel++;
				}
				var n = new Node(a, startAccel);
				/** @type {number} */
				var diff = 0;
				for (; diff < threshold; diff++) {
					if (options[diff]) {
						n.set(Math.floor(diff % a), Math.floor(diff / a));
					}
				}
				return n;
			}
		}]);
		return Node;
	}();
	var TypeError = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @return {?}
		 */
		function Literal() {
			_classCallCheck2(this, Literal);
			return _possibleConstructorReturn(this, (Literal.__proto__ || Object.getPrototypeOf(Literal)).apply(this, arguments));
		}
		_inherits(Literal, _WebInspector$GeneralTreeElement);
		_createClass2(Literal, null, [{
			key: "getNotFoundInstance",
			value: function hydrateAst() {
				return new Literal;
			}
		}]);
		return Literal;
	}(input);
	/** @type {string} */
	TypeError.kind = "NotFoundException";
	var Subscription = function (parent) {
		/**
		 * @param {?} data
		 * @return {?}
		 */
		function Sprite(data) {
			var that;
			_classCallCheck2(this, Sprite);
			that = _possibleConstructorReturn(this, (Sprite.__proto__ || Object.getPrototypeOf(Sprite)).call(this, data));
			that;
			that.luminances = Sprite.EMPTY;
			/** @type {!Int32Array} */
			that.buckets = new Int32Array(Sprite.LUMINANCE_BUCKETS);
			return that;
		}
		_inherits(Sprite, parent);
		_createClass2(Sprite, [{
			key: "getBlackRow",
			value: function update(X, row) {
				var source = this.getLuminanceSource();
				var width = source.getWidth();
				if (null == row || row.getSize() < width) {
					row = new BitArray(width);
				} else {
					row.clear();
				}
				this.initArrays(width);
				var Xi = source.getRow(X, this.luminances);
				var localBuckets = this.buckets;
				/** @type {number} */
				var i = 0;
				for (; i < width; i++) {
					localBuckets[(255 & Xi[i]) >> Sprite.LUMINANCE_SHIFT]++;
				}
				var blackPoint = Sprite.estimateBlackPoint(localBuckets);
				if (width < 3) {
					/** @type {number} */
					var j = 0;
					for (; j < width; j++) {
						if ((255 & Xi[j]) < blackPoint) {
							row.set(j);
						}
					}
				} else {
					/** @type {number} */
					var a0 = 255 & Xi[0];
					/** @type {number} */
					var interestingPoint = 255 & Xi[1];
					/** @type {number} */
					var i = 1;
					for (; i < width - 1; i++) {
						/** @type {number} */
						var viewportCenter = 255 & Xi[i + 1];
						if ((4 * interestingPoint - a0 - viewportCenter) / 2 < blackPoint) {
							row.set(i);
						}
						/** @type {number} */
						a0 = interestingPoint;
						/** @type {number} */
						interestingPoint = viewportCenter;
					}
				}
				return row;
			}
		}, {
			key: "getBlackMatrix",
			value: function render() {
				var source = this.getLuminanceSource();
				var width = source.getWidth();
				var height = source.getHeight();
				var expected = new Image(width, height);
				this.initArrays(width);
				var localBuckets = this.buckets;
				/** @type {number} */
				var deviceSizeMultiplier = 1;
				for (; deviceSizeMultiplier < 5; deviceSizeMultiplier++) {
					/** @type {number} */
					var X = Math.floor(height * deviceSizeMultiplier / 5);
					var Xi = source.getRow(X, this.luminances);
					/** @type {number} */
					var extendedCount = Math.floor(4 * width / 5);
					/** @type {number} */
					var i = Math.floor(width / 5);
					for (; i < extendedCount; i++) {
						localBuckets[(255 & Xi[i]) >> Sprite.LUMINANCE_SHIFT]++;
					}
				}
				var blackPoint = Sprite.estimateBlackPoint(localBuckets);
				var p_displayOptions = source.getMatrix();
				/** @type {number} */
				var y = 0;
				for (; y < height; y++) {
					/** @type {number} */
					var offset = y * width;
					/** @type {number} */
					var i = 0;
					for (; i < width; i++) {
						if ((255 & p_displayOptions[offset + i]) < blackPoint) {
							expected.set(i, y);
						}
					}
				}
				return expected;
			}
		}, {
			key: "createBinarizer",
			value: function extractSprite(width) {
				return new Sprite(width);
			}
		}, {
			key: "initArrays",
			value: function BloomFilter(size) {
				if (this.luminances.length < size) {
					/** @type {!Uint8ClampedArray} */
					this.luminances = new Uint8ClampedArray(size);
				}
				var buckets = this.buckets;
				/** @type {number} */
				var x = 0;
				for (; x < Sprite.LUMINANCE_BUCKETS; x++) {
					/** @type {number} */
					buckets[x] = 0;
				}
			}
		}], [{
			key: "estimateBlackPoint",
			value: function calendarComputed(data) {
				var il = data.length;
				/** @type {number} */
				var value = 0;
				/** @type {number} */
				var max = 0;
				/** @type {number} */
				var maxValue = 0;
				/** @type {number} */
				var j = 0;
				for (; j < il; j++) {
					if (data[j] > maxValue) {
						/** @type {number} */
						max = j;
						maxValue = data[j];
					}
					if (data[j] > value) {
						value = data[j];
					}
				}
				/** @type {number} */
				var min = 0;
				/** @type {number} */
				var a = 0;
				/** @type {number} */
				var i = 0;
				for (; i < il; i++) {
					/** @type {number} */
					var s = i - max;
					/** @type {number} */
					var b = data[i] * s * s;
					if (b > a) {
						/** @type {number} */
						min = i;
						/** @type {number} */
						a = b;
					}
				}
				if (max > min) {
					/** @type {number} */
					var v = max;
					/** @type {number} */
					max = min;
					/** @type {number} */
					min = v;
				}
				if (min - max <= il / 16) {
					throw new TypeError;
				}
				/** @type {number} */
				var val = min - 1;
				/** @type {number} */
				var lastChecksumFailCount = -1;
				/** @type {number} */
				var n = min - 1;
				for (; n > max; n--) {
					/** @type {number} */
					var i = n - max;
					/** @type {number} */
					var currentChecksumFailCount = i * i * (min - n) * (value - data[n]);
					if (currentChecksumFailCount > lastChecksumFailCount) {
						/** @type {number} */
						val = n;
						/** @type {number} */
						lastChecksumFailCount = currentChecksumFailCount;
					}
				}
				return val << Sprite.LUMINANCE_SHIFT;
			}
		}]);
		return Sprite;
	}(cfg);
	/** @type {number} */
	Subscription.LUMINANCE_BITS = 5;
	/** @type {number} */
	Subscription.LUMINANCE_SHIFT = 8 - Subscription.LUMINANCE_BITS;
	/** @type {number} */
	Subscription.LUMINANCE_BUCKETS = 1 << Subscription.LUMINANCE_BITS;
	/** @type {!Uint8ClampedArray} */
	Subscription.EMPTY = Uint8ClampedArray.from([0]);
	var Emitter = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @param {?} props
		 * @return {?}
		 */
		function _(props) {
			var _this;
			_classCallCheck2(this, _);
			_this = _possibleConstructorReturn(this, (_.__proto__ || Object.getPrototypeOf(_)).call(this, props));
			_this;
			/** @type {null} */
			_this.matrix = null;
			return _this;
		}
		_inherits(_, _WebInspector$GeneralTreeElement);
		_createClass2(_, [{
			key: "getBlackMatrix",
			value: function update() {
				if (null !== this.matrix) {
					return this.matrix;
				}
				var source = this.getLuminanceSource();
				var width = source.getWidth();
				var height = source.getHeight();
				if (width >= _.MINIMUM_DIMENSION && height >= _.MINIMUM_DIMENSION) {
					var luminances = source.getMatrix();
					/** @type {number} */
					var subWidth = width >> _.BLOCK_SIZE_POWER;
					if (0 != (width & _.BLOCK_SIZE_MASK)) {
						subWidth++;
					}
					/** @type {number} */
					var subHeight = height >> _.BLOCK_SIZE_POWER;
					if (0 != (height & _.BLOCK_SIZE_MASK)) {
						subHeight++;
					}
					var blackPoints = _.calculateBlackPoints(luminances, subWidth, subHeight, width, height);
					var newMatrix = new Image(width, height);
					_.calculateThresholdForBlock(luminances, subWidth, subHeight, width, height, blackPoints, newMatrix);
					this.matrix = newMatrix;
				} else {
					this.matrix = _get(_.prototype.__proto__ || Object.getPrototypeOf(_.prototype), "getBlackMatrix", this).call(this);
				}
				return this.matrix;
			}
		}, {
			key: "createBinarizer",
			value: function createBinarizer(source) {
				return new _(source);
			}
		}], [{
			key: "calculateThresholdForBlock",
			value: function calculateThresholdForBlock(luminances, subWidth, subHeight, width, height, blackPoints, matrix) {
				/** @type {number} */
				var maxYOffset = height - _.BLOCK_SIZE;
				/** @type {number} */
				var maxXOffset = width - _.BLOCK_SIZE;
				/** @type {number} */
				var y = 0;
				for (; y < subHeight; y++) {
					/** @type {number} */
					var yoffset = y << _.BLOCK_SIZE_POWER;
					if (yoffset > maxYOffset) {
						/** @type {number} */
						yoffset = maxYOffset;
					}
					var top = _.cap(y, 2, subHeight - 3);
					/** @type {number} */
					var x = 0;
					for (; x < subWidth; x++) {
						/** @type {number} */
						var xoffset = x << _.BLOCK_SIZE_POWER;
						if (xoffset > maxXOffset) {
							/** @type {number} */
							xoffset = maxXOffset;
						}
						var s1 = _.cap(x, 2, subWidth - 3);
						/** @type {number} */
						var totalModuleSize = 0;
						/** @type {number} */
						var z = -2;
						for (; z <= 2; z++) {
							var bumpPixels = blackPoints[top + z];
							totalModuleSize = totalModuleSize + (bumpPixels[s1 - 2] + bumpPixels[s1 - 1] + bumpPixels[s1] + bumpPixels[s1 + 1] + bumpPixels[s1 + 2]);
						}
						/** @type {number} */
						var average = totalModuleSize / 25;
						_.thresholdBlock(luminances, xoffset, yoffset, average, width, matrix);
					}
				}
			}
		}, {
			key: "cap",
			value: function IntervalTreeNode(left, limitLeft, limitRight) {
				return left < limitLeft ? limitLeft : left > limitRight ? limitRight : left;
			}
		}, {
			key: "thresholdBlock",
			value: function isSelected(d, x, y, color, n, batch) {
				/** @type {number} */
				var yy = 0;
				var offset = y * n + x;
				for (; yy < _.BLOCK_SIZE; yy++, offset = offset + n) {
					/** @type {number} */
					var xx = 0;
					for (; xx < _.BLOCK_SIZE; xx++) {
						if ((255 & d[offset + xx]) <= color) {
							batch.set(x + xx, y + yy);
						}
					}
				}
			}
		}, {
			key: "calculateBlackPoints",
			value: function calculateBlackPoints(luminances, subWidth, subHeight, width, height) {
				/** @type {number} */
				var maxYOffset = height - _.BLOCK_SIZE;
				/** @type {number} */
				var maxXOffset = width - _.BLOCK_SIZE;
				/** @type {!Array} */
				var blackPoints = new Array(subHeight);
				/** @type {number} */
				var y = 0;
				for (; y < subHeight; y++) {
					/** @type {!Int32Array} */
					blackPoints[y] = new Int32Array(subWidth);
					/** @type {number} */
					var yoffset = y << _.BLOCK_SIZE_POWER;
					if (yoffset > maxYOffset) {
						/** @type {number} */
						yoffset = maxYOffset;
					}
					/** @type {number} */
					var x = 0;
					for (; x < subWidth; x++) {
						/** @type {number} */
						var xoffset = x << _.BLOCK_SIZE_POWER;
						if (xoffset > maxXOffset) {
							/** @type {number} */
							xoffset = maxXOffset;
						}
						/** @type {number} */
						var nCatCount = 0;
						/** @type {number} */
						var min = 255;
						/** @type {number} */
						var max = 0;
						/** @type {number} */
						var yy = 0;
						/** @type {number} */
						var offset = yoffset * width + xoffset;
						for (; yy < _.BLOCK_SIZE; yy++, offset = offset + width) {
							/** @type {number} */
							var x = 0;
							for (; x < _.BLOCK_SIZE; x++) {
								/** @type {number} */
								var n = 255 & luminances[offset + x];
								/** @type {number} */
								nCatCount = nCatCount + n;
								if (n < min) {
									/** @type {number} */
									min = n;
								}
								if (n > max) {
									/** @type {number} */
									max = n;
								}
							}
							if (max - min > _.MIN_DYNAMIC_RANGE) {
								yy++;
								offset = offset + width;
								for (; yy < _.BLOCK_SIZE; yy++, offset = offset + width) {
									/** @type {number} */
									var x = 0;
									for (; x < _.BLOCK_SIZE; x++) {
										/** @type {number} */
										nCatCount = nCatCount + (255 & luminances[offset + x]);
									}
								}
							}
						}
						/** @type {number} */
						var w = nCatCount >> 2 * _.BLOCK_SIZE_POWER;
						if (max - min <= _.MIN_DYNAMIC_RANGE && (w = min / 2, y > 0 && x > 0)) {
							/** @type {number} */
							var max = (blackPoints[y - 1][x] + 2 * blackPoints[y][x - 1] + blackPoints[y - 1][x - 1]) / 4;
							if (min < max) {
								/** @type {number} */
								w = max;
							}
						}
						/** @type {number} */
						blackPoints[y][x] = w;
					}
				}
				return blackPoints;
			}
		}]);
		return _;
	}(Subscription);
	/** @type {number} */
	Emitter.BLOCK_SIZE_POWER = 3;
	/** @type {number} */
	Emitter.BLOCK_SIZE = 1 << Emitter.BLOCK_SIZE_POWER;
	/** @type {number} */
	Emitter.BLOCK_SIZE_MASK = Emitter.BLOCK_SIZE - 1;
	/** @type {number} */
	Emitter.MINIMUM_DIMENSION = 5 * Emitter.BLOCK_SIZE;
	/** @type {number} */
	Emitter.MIN_DYNAMIC_RANGE = 24;
	var $this = function () {
		/**
		 * @param {number} options
		 * @param {number} element
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3(options, element) {
			_classCallCheck2(this, TempusDominusBootstrap3);
			/** @type {number} */
			this.width = options;
			/** @type {number} */
			this.height = element;
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "getWidth",
			value: function get_width() {
				return this.width;
			}
		}, {
			key: "getHeight",
			value: function get_height() {
				return this.height;
			}
		}, {
			key: "isCropSupported",
			value: function leadingImageNode() {
				return false;
			}
		}, {
			key: "crop",
			value: function contrast(err, cont, color, dark) {
				throw new buffer("This luminance source does not support cropping.");
			}
		}, {
			key: "isRotateSupported",
			value: function leadingImageNode() {
				return false;
			}
		}, {
			key: "rotateCounterClockwise",
			value: function createLines() {
				throw new buffer("This luminance source does not support rotation by 90 degrees.");
			}
		}, {
			key: "rotateCounterClockwise45",
			value: function createLines() {
				throw new buffer("This luminance source does not support rotation by 45 degrees.");
			}
		}, {
			key: "toString",
			value: function reverse() {
				/** @type {!Uint8ClampedArray} */
				var row = new Uint8ClampedArray(this.width);
				var parentEl = new Buffer;
				/** @type {number} */
				var i = 0;
				for (; i < this.height; i++) {
					var rowData = this.getRow(i, row);
					/** @type {number} */
					var j = 0;
					for (; j < this.width; j++) {
						/** @type {number} */
						var levels = 255 & rowData[j];
						var t = void 0;
						/** @type {string} */
						t = levels < 64 ? "#" : levels < 128 ? "+" : levels < 192 ? "." : " ";
						parentEl.append(t);
					}
					parentEl.append("\n");
				}
				return parentEl.toString();
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var O = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @param {!Object} texture
		 * @return {?}
		 */
		function Sprite(texture) {
			var _this;
			_classCallCheck2(this, Sprite);
			_this = _possibleConstructorReturn(this, (Sprite.__proto__ || Object.getPrototypeOf(Sprite)).call(this, texture.getWidth(), texture.getHeight()));
			_this;
			/** @type {!Object} */
			_this.delegate = texture;
			return _this;
		}
		_inherits(Sprite, _WebInspector$GeneralTreeElement);
		_createClass2(Sprite, [{
			key: "getRow",
			value: function update(y, value) {
				var data = this.delegate.getRow(y, value);
				var w = this.getWidth();
				/** @type {number} */
				var bp = 0;
				for (; bp < w; bp++) {
					/** @type {number} */
					data[bp] = 255 - (255 & data[bp]);
				}
				return data;
			}
		}, {
			key: "getMatrix",
			value: function getTranslate() {
				var active_tags = this.delegate.getMatrix();
				/** @type {number} */
				var length = this.getWidth() * this.getHeight();
				/** @type {!Uint8ClampedArray} */
				var result = new Uint8ClampedArray(length);
				/** @type {number} */
				var ii = 0;
				for (; ii < length; ii++) {
					/** @type {number} */
					result[ii] = 255 - (255 & active_tags[ii]);
				}
				return result;
			}
		}, {
			key: "isCropSupported",
			value: function VerticalScroll() {
				return this.delegate.isCropSupported();
			}
		}, {
			key: "crop",
			value: function crop(left, top, width, height) {
				return new Sprite(this.delegate.crop(left, top, width, height));
			}
		}, {
			key: "isRotateSupported",
			value: function VerticalScroll() {
				return this.delegate.isRotateSupported();
			}
		}, {
			key: "invert",
			value: function invert() {
				return this.delegate;
			}
		}, {
			key: "rotateCounterClockwise",
			value: function VerticalScroll() {
				return new Sprite(this.delegate.rotateCounterClockwise());
			}
		}, {
			key: "rotateCounterClockwise45",
			value: function VerticalScroll() {
				return new Sprite(this.delegate.rotateCounterClockwise45());
			}
		}]);
		return Sprite;
	}($this);
	var Context = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @param {!Object} data
		 * @return {?}
		 */
		function context(data) {
			var _this;
			_classCallCheck2(this, context);
			_this = _possibleConstructorReturn(this, (context.__proto__ || Object.getPrototypeOf(context)).call(this, data.width, data.height));
			_this;
			/** @type {!Object} */
			_this.canvas = data;
			/** @type {null} */
			_this.tempCanvasElement = null;
			_this.buffer = context.makeBufferFromCanvasImageData(data);
			return _this;
		}
		_inherits(context, _WebInspector$GeneralTreeElement);
		_createClass2(context, [{
			key: "getRow",
			value: function test(i, arr) {
				if (i < 0 || i >= this.getHeight()) {
					throw new Function("Requested row is outside the image: " + i);
				}
				var len = this.getWidth();
				/** @type {number} */
				var start = i * len;
				return null === arr ? arr = this.buffer.slice(start, start + len) : (arr.length < len && (arr = new Uint8ClampedArray(len)), arr.set(this.buffer.slice(start, start + len))), arr;
			}
		}, {
			key: "getMatrix",
			value: function unserializeArrayBuffer() {
				return this.buffer;
			}
		}, {
			key: "isCropSupported",
			value: function almost_equals() {
				return true;
			}
		}, {
			key: "crop",
			value: function build(gl, e, p, n) {
				return _get(context.prototype.__proto__ || Object.getPrototypeOf(context.prototype), "crop", this).call(this, gl, e, p, n), this;
			}
		}, {
			key: "isRotateSupported",
			value: function almost_equals() {
				return true;
			}
		}, {
			key: "rotateCounterClockwise",
			value: function renderSVGPointsProjected() {
				return this.rotate(-90), this;
			}
		}, {
			key: "rotateCounterClockwise45",
			value: function renderSVGPointsProjected() {
				return this.rotate(-45), this;
			}
		}, {
			key: "getTempCanvasElement",
			value: function pageViewBeforePrint() {
				if (null === this.tempCanvasElement) {
					var targetCanvas = this.canvas.ownerDocument.createElement("canvas");
					targetCanvas.width = this.canvas.width;
					targetCanvas.height = this.canvas.height;
					this.tempCanvasElement = targetCanvas;
				}
				return this.tempCanvasElement;
			}
		}, {
			key: "rotate",
			value: function rotate(y) {
				var buffer = this.getTempCanvasElement();
				var ctx = buffer.getContext("2d");
				/** @type {number} */
				var d = y * context.DEGREE_TO_RADIANS;
				var width = this.canvas.width;
				var height = this.canvas.height;
				/** @type {number} */
				var size = Math.ceil(Math.abs(Math.cos(d)) * width + Math.abs(Math.sin(d)) * height);
				/** @type {number} */
				var HEIGHT = Math.ceil(Math.abs(Math.sin(d)) * width + Math.abs(Math.cos(d)) * height);
				return buffer.width = size, buffer.height = HEIGHT, ctx.translate(size / 2, HEIGHT / 2), ctx.rotate(d), ctx.drawImage(this.canvas, width / -2, height / -2), this.buffer = context.makeBufferFromCanvasImageData(buffer), this;
			}
		}, {
			key: "invert",
			value: function design() {
				return new O(this);
			}
		}], [{
			key: "makeBufferFromCanvasImageData",
			value: function initializeColorRenderCanvas(image) {
				var lz_image = image.getContext("2d").getImageData(0, 0, image.width, image.height);
				return context.toGrayscaleBuffer(lz_image.data, image.width, image.height);
			}
		}, {
			key: "toGrayscaleBuffer",
			value: function invert(input, width, height) {
				/** @type {!Uint8ClampedArray} */
				var result = new Uint8ClampedArray(width * height);
				/** @type {number} */
				var i = 0;
				/** @type {number} */
				var j = 0;
				var inputLen = input.length;
				for (; i < inputLen; i = i + 4, j++) {
					var digit = void 0;
					if (0 === input[i + 3]) {
						/** @type {number} */
						digit = 255;
					} else {
						/** @type {number} */
						digit = 306 * input[i] + 601 * input[i + 1] + 117 * input[i + 2] + 512 >> 10;
					}
					/** @type {number} */
					result[j] = digit;
				}
				return result;
			}
		}]);
		return context;
	}($this);
	/** @type {number} */
	Context.DEGREE_TO_RADIANS = Math.PI / 180;
	var Form = function () {
		/**
		 * @param {string} deviceId
		 * @param {string} name
		 * @param {string} skillKind
		 * @return {undefined}
		 */
		function init(deviceId, name, skillKind) {
			_classCallCheck2(this, init);
			/** @type {string} */
			this.deviceId = deviceId;
			/** @type {string} */
			this.label = name;
			/** @type {string} */
			this.kind = "videoinput";
			this.groupId = skillKind || void 0;
		}
		_createClass2(init, [{
			key: "toJSON",
			value: function convertMediaStreamTrackSource() {
				return {
					kind: this.kind,
					groupId: this.groupId,
					deviceId: this.deviceId,
					label: this.label
				};
			}
		}]);
		return init;
	}();
	var element;
	var wrap = (globalThis || global || self || window ? (globalThis || global || self || window || void 0).__awaiter : void 0) || function (thisArg, _arguments, P, generator) {
		return new (P || (P = Promise))(function (calculateBonus, path) {
			/**
			 * @param {?} value
			 * @return {undefined}
			 */
			function handlePossibleRedirection(value) {
				try {
					step(generator.next(value));
				} catch (shutdown) {
					path(shutdown);
				}
			}
			/**
			 * @param {?} value
			 * @return {undefined}
			 */
			function test(value) {
				try {
					step(generator.throw(value));
				} catch (shutdown) {
					path(shutdown);
				}
			}
			/**
			 * @param {!Object} target
			 * @return {undefined}
			 */
			function step(target) {
				var x;
				if (target.done) {
					calculateBonus(target.value);
				} else {
					(x = target.value, x instanceof P ? x : new P(function (resolve) {
						resolve(x);
					})).then(handlePossibleRedirection, test);
				}
			}
			step((generator = generator.apply(thisArg, _arguments || [])).next());
		});
	};
	var PacketsPanel = function () {
		/**
		 * @param {!Object} options
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3(options) {
			var e = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;
			var hints = arguments[2];
			_classCallCheck2(this, TempusDominusBootstrap3);
			/** @type {!Object} */
			this.reader = options;
			this.timeBetweenScansMillis = e;
			this._hints = hints;
			/** @type {boolean} */
			this._stopContinuousDecode = false;
			/** @type {boolean} */
			this._stopAsyncDecode = false;
			/** @type {number} */
			this._timeBetweenDecodingAttempts = 0;
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "listVideoInputDevices",
			value: function lmap() {
				return wrap(this, void 0, void 0, regeneratorRuntime.mark(function _callee3() {
					var docs;
					var result;
					var _iteratorNormalCompletion3;
					var _didIteratorError3;
					var _iteratorError2;
					var _iterator3;
					var data;
					var device;
					var kind;
					var node;
					return regeneratorRuntime.wrap(function _callee3$(_context4) {
						for (; 1;) {
							switch (_context4.prev = _context4.next) {
								case 0:
									if (this.hasNavigator) {
										/** @type {number} */
										_context4.next = 2;
										break;
									}
									throw new Error("Can't enumerate devices, navigator is not present.");
								case 2:
									if (this.canEnumerateDevices) {
										/** @type {number} */
										_context4.next = 4;
										break;
									}
									throw new Error("Can't enumerate devices, method not supported.");
								case 4:
									/** @type {number} */
									_context4.next = 6;
									return navigator.mediaDevices.enumerateDevices();
								case 6:
									docs = _context4.sent;
									/** @type {!Array} */
									result = [];
									/** @type {boolean} */
									_iteratorNormalCompletion3 = true;
									/** @type {boolean} */
									_didIteratorError3 = false;
									_iteratorError2 = undefined;
									/** @type {number} */
									_context4.prev = 11;
									_iterator3 = docs[Symbol.iterator]();
								case 13:
									if (_iteratorNormalCompletion3 = (data = _iterator3.next()).done) {
										/** @type {number} */
										_context4.next = 23;
										break;
									}
									device = data.value;
									kind = "video" === device.kind ? "videoinput" : device.kind;
									if (!("videoinput" !== kind)) {
										/** @type {number} */
										_context4.next = 18;
										break;
									}
									return _context4.abrupt("continue", 20);
								case 18:
									node = {
										deviceId: device.deviceId || device.id,
										label: device.label || "Video device " + (result.length + 1),
										kind: kind,
										groupId: device.groupId
									};
									result.push(node);
								case 20:
									/** @type {boolean} */
									_iteratorNormalCompletion3 = true;
									/** @type {number} */
									_context4.next = 13;
									break;
								case 23:
									/** @type {number} */
									_context4.next = 29;
									break;
								case 25:
									/** @type {number} */
									_context4.prev = 25;
									_context4.t0 = _context4["catch"](11);
									/** @type {boolean} */
									_didIteratorError3 = true;
									_iteratorError2 = _context4.t0;
								case 29:
									/** @type {number} */
									_context4.prev = 29;
									/** @type {number} */
									_context4.prev = 30;
									if (!_iteratorNormalCompletion3 && _iterator3.return) {
										_iterator3.return();
									}
								case 32:
									/** @type {number} */
									_context4.prev = 32;
									if (!_didIteratorError3) {
										/** @type {number} */
										_context4.next = 35;
										break;
									}
									throw _iteratorError2;
								case 35:
									return _context4.finish(32);
								case 36:
									return _context4.finish(29);
								case 37:
									return _context4.abrupt("return", result);
								case 38:
								case "end":
									return _context4.stop();
							}
						}
					}, _callee3, this, [[11, 25, 29, 37], [30, , 32, 36]]);
				}));
			}
		}, {
			key: "getVideoInputDevices",
			value: function seq() {
				return wrap(this, void 0, void 0, regeneratorRuntime.mark(function _callee2() {
					return regeneratorRuntime.wrap(function _callee2$(_context4) {
						for (; 1;) {
							switch (_context4.prev = _context4.next) {
								case 0:
									/** @type {number} */
									_context4.next = 2;
									return this.listVideoInputDevices();
								case 2:
									/**
									 * @param {!Object} options
									 * @return {?}
									 */
									_context4.t0 = function (options) {
										return new Form(options.deviceId, options.label);
									};
									return _context4.abrupt("return", _context4.sent.map(_context4.t0));
								case 4:
								case "end":
									return _context4.stop();
							}
						}
					}, _callee2, this);
				}));
			}
		}, {
			key: "findDeviceById",
			value: function lmap(fn) {
				return wrap(this, void 0, void 0, regeneratorRuntime.mark(function _callee3() {
					var files;
					return regeneratorRuntime.wrap(function _callee3$(_context3) {
						for (; 1;) {
							switch (_context3.prev = _context3.next) {
								case 0:
									/** @type {number} */
									_context3.next = 2;
									return this.listVideoInputDevices();
								case 2:
									files = _context3.sent;
									return _context3.abrupt("return", files ? files.find(function (item) {
										return item.deviceId === fn;
									}) : null);
								case 4:
								case "end":
									return _context3.stop();
							}
						}
					}, _callee3, this);
				}));
			}
		}, {
			key: "decodeFromInputVideoDevice",
			value: function lfilter(fn, gen) {
				return wrap(this, void 0, void 0, regeneratorRuntime.mark(function _callee4() {
					return regeneratorRuntime.wrap(function _callee4$(_context4) {
						for (; 1;) {
							switch (_context4.prev = _context4.next) {
								case 0:
									/** @type {number} */
									_context4.next = 2;
									return this.decodeOnceFromVideoDevice(fn, gen);
								case 2:
									return _context4.abrupt("return", _context4.sent);
								case 3:
								case "end":
									return _context4.stop();
							}
						}
					}, _callee4, this);
				}));
			}
		}, {
			key: "decodeOnceFromVideoDevice",
			value: function lfilter(max, fn) {
				return wrap(this, void 0, void 0, regeneratorRuntime.mark(function _callee4() {
					var s;
					var data;
					return regeneratorRuntime.wrap(function _callee4$(_context4) {
						for (; 1;) {
							switch (_context4.prev = _context4.next) {
								case 0:
									s = void 0;
									this.reset();
									data = {
										video: s = max ? {
											deviceId: {
												exact: max
											}
										} : {
											facingMode: "environment"
										}
									};
									/** @type {number} */
									_context4.next = 5;
									return this.decodeOnceFromConstraints(data, fn);
								case 5:
									return _context4.abrupt("return", _context4.sent);
								case 6:
								case "end":
									return _context4.stop();
							}
						}
					}, _callee4, this);
				}));
			}
		}, {
			key: "decodeOnceFromConstraints",
			value: function take(constraints, cb) {
				return wrap(this, void 0, void 0, regeneratorRuntime.mark(function _callee5() {
					var schemaStrs;
					return regeneratorRuntime.wrap(function _callee5$(_context5) {
						for (; 1;) {
							switch (_context5.prev = _context5.next) {
								case 0:
									/** @type {number} */
									_context5.next = 2;
									return navigator.mediaDevices.getUserMedia(constraints);
								case 2:
									schemaStrs = _context5.sent;
									/** @type {number} */
									_context5.next = 5;
									return this.decodeOnceFromStream(schemaStrs, cb);
								case 5:
									return _context5.abrupt("return", _context5.sent);
								case 6:
								case "end":
									return _context5.stop();
							}
						}
					}, _callee5, this);
				}));
			}
		}, {
			key: "decodeOnceFromStream",
			value: function read(characteristicUUID, value) {
				return wrap(this, void 0, void 0, regeneratorRuntime.mark(function _callee7() {
					var insertResult;
					return regeneratorRuntime.wrap(function _callee7$(_context8) {
						for (; 1;) {
							switch (_context8.prev = _context8.next) {
								case 0:
									this.reset();
									/** @type {number} */
									_context8.next = 3;
									return this.attachStreamToVideo(characteristicUUID, value);
								case 3:
									insertResult = _context8.sent;
									/** @type {number} */
									_context8.next = 6;
									return this.decodeOnce(insertResult);
								case 6:
									return _context8.abrupt("return", _context8.sent);
								case 7:
								case "end":
									return _context8.stop();
							}
						}
					}, _callee7, this);
				}));
			}
		}, {
			key: "decodeFromInputVideoDeviceContinuously",
			value: function lfilter(fn, num, gen) {
				return wrap(this, void 0, void 0, regeneratorRuntime.mark(function _callee4() {
					return regeneratorRuntime.wrap(function _callee4$(_context4) {
						for (; 1;) {
							switch (_context4.prev = _context4.next) {
								case 0:
									/** @type {number} */
									_context4.next = 2;
									return this.decodeFromVideoDevice(fn, num, gen);
								case 2:
									return _context4.abrupt("return", _context4.sent);
								case 3:
								case "end":
									return _context4.stop();
							}
						}
					}, _callee4, this);
				}));
			}
		}, {
			key: "decodeFromVideoDevice",
			value: function lfilter(max, fn, gen) {
				return wrap(this, void 0, void 0, regeneratorRuntime.mark(function _callee4() {
					var s;
					var data;
					return regeneratorRuntime.wrap(function _callee4$(_context4) {
						for (; 1;) {
							switch (_context4.prev = _context4.next) {
								case 0:
									s = void 0;
									data = {
										video: s = max ? {
											deviceId: {
												exact: max
											}
										} : {
											facingMode: "environment"
										}
									};
									/** @type {number} */
									_context4.next = 4;
									return this.decodeFromConstraints(data, fn, gen);
								case 4:
									return _context4.abrupt("return", _context4.sent);
								case 5:
								case "end":
									return _context4.stop();
							}
						}
					}, _callee4, this);
				}));
			}
		}, {
			key: "decodeFromConstraints",
			value: function clear(c, $table, name) {
				return wrap(this, void 0, void 0, regeneratorRuntime.mark(function _callee4() {
					var m;
					return regeneratorRuntime.wrap(function _callee4$(_context4) {
						for (; 1;) {
							switch (_context4.prev = _context4.next) {
								case 0:
									/** @type {number} */
									_context4.next = 2;
									return navigator.mediaDevices.getUserMedia(c);
								case 2:
									m = _context4.sent;
									/** @type {number} */
									_context4.next = 5;
									return this.decodeFromStream(m, $table, name);
								case 5:
									return _context4.abrupt("return", _context4.sent);
								case 6:
								case "end":
									return _context4.stop();
							}
						}
					}, _callee4, this);
				}));
			}
		}, {
			key: "decodeFromStream",
			value: function lfilter(body, gen, fn) {
				return wrap(this, void 0, void 0, regeneratorRuntime.mark(function _callee4() {
					var m;
					return regeneratorRuntime.wrap(function _callee4$(_context4) {
						for (; 1;) {
							switch (_context4.prev = _context4.next) {
								case 0:
									this.reset();
									/** @type {number} */
									_context4.next = 3;
									return this.attachStreamToVideo(body, gen);
								case 3:
									m = _context4.sent;
									/** @type {number} */
									_context4.next = 6;
									return this.decodeContinuously(m, fn);
								case 6:
									return _context4.abrupt("return", _context4.sent);
								case 7:
								case "end":
									return _context4.stop();
							}
						}
					}, _callee4, this);
				}));
			}
		}, {
			key: "stopAsyncDecode",
			value: function stopAsyncDecode() {
				/** @type {boolean} */
				this._stopAsyncDecode = true;
			}
		}, {
			key: "stopContinuousDecode",
			value: function stopContinuousDecode() {
				/** @type {boolean} */
				this._stopContinuousDecode = true;
			}
		}, {
			key: "attachStreamToVideo",
			value: function get(connection, callback) {
				return wrap(this, void 0, void 0, regeneratorRuntime.mark(function _callee() {
					var result;
					return regeneratorRuntime.wrap(function _callee$(_context) {
						for (; 1;) {
							switch (_context.prev = _context.next) {
								case 0:
									result = this.prepareVideoElement(callback);
									this.addVideoSource(result, connection);
									this.videoElement = result;
									/** @type {!Object} */
									this.stream = connection;
									/** @type {number} */
									_context.next = 6;
									return this.playVideoOnLoadAsync(result);
								case 6:
									return _context.abrupt("return", result);
								case 7:
								case "end":
									return _context.stop();
							}
						}
					}, _callee, this);
				}));
			}
		}, {
			key: "playVideoOnLoadAsync",
			value: function awaitTransitionEnd(n) {
				var jimple = this;
				return new Promise(function (saveNotifs, canCreateDiscussions) {
					return jimple.playVideoOnLoad(n, function () {
						return saveNotifs();
					});
				});
			}
		}, {
			key: "playVideoOnLoad",
			value: function loadMusic(source, i) {
				var flowParser = this;
				/**
				 * @return {?}
				 */
				this.videoEndedListener = function () {
					return flowParser.stopStreams();
				};
				/**
				 * @return {?}
				 */
				this.videoCanPlayListener = function () {
					return flowParser.tryPlayVideo(source);
				};
				source.addEventListener("ended", this.videoEndedListener);
				source.addEventListener("canplay", this.videoCanPlayListener);
				source.addEventListener("playing", i);
				this.tryPlayVideo(source);
			}
		}, {
			key: "isVideoPlaying",
			value: function reset(data) {
				return data.currentTime > 0 && !data.paused && !data.ended && data.readyState > 2;
			}
		}, {
			key: "tryPlayVideo",
			value: function lmap(_) {
				return wrap(this, void 0, void 0, regeneratorRuntime.mark(function _callee3() {
					return regeneratorRuntime.wrap(function _callee3$(_context3) {
						for (; 1;) {
							switch (_context3.prev = _context3.next) {
								case 0:
									if (!this.isVideoPlaying(_)) {
										/** @type {number} */
										_context3.next = 4;
										break;
									}
									console.warn("Trying to play video that is already playing.");
									/** @type {number} */
									_context3.next = 12;
									break;
								case 4:
									/** @type {number} */
									_context3.prev = 4;
									/** @type {number} */
									_context3.next = 7;
									return _.play();
								case 7:
									/** @type {number} */
									_context3.next = 12;
									break;
								case 9:
									/** @type {number} */
									_context3.prev = 9;
									_context3.t0 = _context3["catch"](4);
									console.warn("It was not possible to play the video.");
								case 12:
								case "end":
									return _context3.stop();
							}
						}
					}, _callee3, this, [[4, 9]]);
				}));
			}
		}, {
			key: "getMediaElement",
			value: function init(stage_elem, e) {
				/** @type {(Element|null)} */
				var r = document.getElementById(stage_elem);
				if (!r) {
					throw new Renderer("element with id '" + stage_elem + "' not found");
				}
				if (r.nodeName.toLowerCase() !== e.toLowerCase()) {
					throw new Renderer("element with id '" + stage_elem + "' must be an " + e + " element");
				}
				return r;
			}
		}, {
			key: "decodeFromImage",
			value: function scanPath(absolutePath, omitFileIocComments) {
				if (!absolutePath && !omitFileIocComments) {
					throw new Renderer("either imageElement with a src set or an url must be provided");
				}
				return omitFileIocComments && !absolutePath ? this.decodeFromImageUrl(omitFileIocComments) : this.decodeFromImageElement(absolutePath);
			}
		}, {
			key: "decodeFromVideo",
			value: function scanPath(absolutePath, omitFileIocComments) {
				if (!absolutePath && !omitFileIocComments) {
					throw new Renderer("Either an element with a src set or an URL must be provided");
				}
				return omitFileIocComments && !absolutePath ? this.decodeFromVideoUrl(omitFileIocComments) : this.decodeFromVideoElement(absolutePath);
			}
		}, {
			key: "decodeFromVideoContinuously",
			value: function TapObservable(source, observerOrOnNext, onError) {
				if (void 0 === source && void 0 === observerOrOnNext) {
					throw new Renderer("Either an element with a src set or an URL must be provided");
				}
				return observerOrOnNext && !source ? this.decodeFromVideoUrlContinuously(observerOrOnNext, onError) : this.decodeFromVideoElementContinuously(source, onError);
			}
		}, {
			key: "decodeFromImageElement",
			value: function run(_args) {
				if (!_args) {
					throw new Renderer("An image element must be provided.");
				}
				this.reset();
				var args = this.prepareImageElement(_args);
				var _args4 = void 0;
				return this.imageElement = args, _args4 = this.isImageLoaded(args) ? this.decodeOnce(args, false, true) : this._decodeOnLoadImage(args);
			}
		}, {
			key: "decodeFromVideoElement",
			value: function isLikeHSLA(t) {
				var year = this._decodeFromVideoElementSetup(t);
				return this._decodeOnLoadVideo(year);
			}
		}, {
			key: "decodeFromVideoElementContinuously",
			value: function $get(mmCoreSplitViewBlock, $state) {
				var artistTrack = this._decodeFromVideoElementSetup(mmCoreSplitViewBlock);
				return this._decodeOnLoadVideoContinuously(artistTrack, $state);
			}
		}, {
			key: "_decodeFromVideoElementSetup",
			value: function init(level) {
				if (!level) {
					throw new Renderer("A video element must be provided.");
				}
				this.reset();
				var adjustedLevel = this.prepareVideoElement(level);
				return this.videoElement = adjustedLevel, adjustedLevel;
			}
		}, {
			key: "decodeFromImageUrl",
			value: function run(source) {
				if (!source) {
					throw new Renderer("An URL must be provided.");
				}
				this.reset();
				var element = this.prepareImageElement();
				this.imageElement = element;
				var result = this._decodeOnLoadImage(element);
				return element.src = source, result;
			}
		}, {
			key: "decodeFromVideoUrl",
			value: function render(value) {
				if (!value) {
					throw new Renderer("An URL must be provided.");
				}
				this.reset();
				var itemData = this.prepareVideoElement();
				var reverseItemData = this.decodeFromVideoElement(itemData);
				return itemData.src = value, reverseItemData;
			}
		}, {
			key: "decodeFromVideoUrlContinuously",
			value: function render(value, start) {
				if (!value) {
					throw new Renderer("An URL must be provided.");
				}
				this.reset();
				var itemData = this.prepareVideoElement();
				var node = this.decodeFromVideoElementContinuously(itemData, start);
				return itemData.src = value, node;
			}
		}, {
			key: "_decodeOnLoadImage",
			value: function fetchScriptInternal(url) {
				var request = this;
				return new Promise(function (mapResolved, PL$58) {
					/**
					 * @return {?}
					 */
					request.imageLoadedListener = function () {
						return request.decodeOnce(url, false, true).then(mapResolved, PL$58);
					};
					url.addEventListener("load", request.imageLoadedListener);
				});
			}
		}, {
			key: "_decodeOnLoadVideo",
			value: function lfilter(fn) {
				return wrap(this, void 0, void 0, regeneratorRuntime.mark(function _callee4() {
					return regeneratorRuntime.wrap(function _callee4$(_context4) {
						for (; 1;) {
							switch (_context4.prev = _context4.next) {
								case 0:
									/** @type {number} */
									_context4.next = 2;
									return this.playVideoOnLoadAsync(fn);
								case 2:
									/** @type {number} */
									_context4.next = 4;
									return this.decodeOnce(fn);
								case 4:
									return _context4.abrupt("return", _context4.sent);
								case 5:
								case "end":
									return _context4.stop();
							}
						}
					}, _callee4, this);
				}));
			}
		}, {
			key: "_decodeOnLoadVideoContinuously",
			value: function lfilter(fn, gen) {
				return wrap(this, void 0, void 0, regeneratorRuntime.mark(function _callee4() {
					return regeneratorRuntime.wrap(function g$(_context6) {
						for (; 1;) {
							switch (_context6.prev = _context6.next) {
								case 0:
									/** @type {number} */
									_context6.next = 2;
									return this.playVideoOnLoadAsync(fn);
								case 2:
									this.decodeContinuously(fn, gen);
								case 3:
								case "end":
									return _context6.stop();
							}
						}
					}, _callee4, this);
				}));
			}
		}, {
			key: "isImageLoaded",
			value: function is_image_loaded(img) {
				return !!img.complete && 0 !== img.naturalWidth;
			}
		}, {
			key: "prepareImageElement",
			value: function createModelImg(image) {
				var images = void 0;
				return void 0 === image && ((images = document.createElement("img")).width = 200, images.height = 200), "string" == typeof image && (images = this.getMediaElement(image, "img")), image instanceof HTMLImageElement && (images = image), images;
			}
		}, {
			key: "prepareVideoElement",
			value: function render(element) {
				var videoElement = void 0;
				return element || "undefined" == typeof document || ((videoElement = document.createElement("video")).width = 200, videoElement.height = 200), "string" == typeof element && (videoElement = this.getMediaElement(element, "video")), element instanceof HTMLVideoElement && (videoElement = element), videoElement.setAttribute("autoplay", "true"), videoElement.setAttribute("muted", "true"), videoElement.setAttribute("playsinline", "true"), videoElement;
			}
		}, {
			key: "decodeOnce",
			value: function f(d) {
				var self = this;
				var result = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
				var isNewStateNearStart = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
				/** @type {boolean} */
				this._stopAsyncDecode = false;
				/**
				 * @param {!Function} cb
				 * @param {!Function} done
				 * @return {?}
				 */
				var music = function test(cb, done) {
					if (self._stopAsyncDecode) {
						return done(new TypeError("Video stream has ended before any code could be detected.")), void (self._stopAsyncDecode = void 0);
					}
					try {
						cb(self.decode(d));
					} catch (x) {
						var isReplayingSong = (x instanceof Rectangle || x instanceof Date) && isNewStateNearStart;
						if (result && x instanceof TypeError || isReplayingSong) {
							return setTimeout(test, self._timeBetweenDecodingAttempts, cb, done);
						}
						done(x);
					}
				};
				return new Promise(function (t, cssModules) {
					return music(t, cssModules);
				});
			}
		}, {
			key: "decodeContinuously",
			value: function update(key, callback) {
				var self = this;
				/** @type {boolean} */
				this._stopContinuousDecode = false;
				/**
				 * @return {undefined}
				 */
				var updateIsBeat = function resolve() {
					if (self._stopContinuousDecode) {
						self._stopContinuousDecode = void 0;
					} else {
						try {
							var ret = self.decode(key);
							callback(ret, null);
							setTimeout(resolve, self.timeBetweenScansMillis);
						} catch (x) {
							callback(null, x);
							/** @type {boolean} */
							var _n17 = x instanceof TypeError;
							if (x instanceof Rectangle || x instanceof Date || _n17) {
								setTimeout(resolve, self._timeBetweenDecodingAttempts);
							}
						}
					}
				};
				updateIsBeat();
			}
		}, {
			key: "decode",
			value: function isLikeHSLA(t) {
				var year = this.createBinaryBitmap(t);
				return this.decodeBitmap(year);
			}
		}, {
			key: "createBinaryBitmap",
			value: function child(callback) {
				var wrongfunc = this.getCaptureCanvasContext(callback);
				this.drawImageOnCanvas(wrongfunc, callback);
				var options = this.getCaptureCanvas(callback);
				var ctx = new Context(options);
				var wallcandle = new Emitter(ctx);
				return new Registry(wallcandle);
			}
		}, {
			key: "getCaptureCanvasContext",
			value: function getCaptureContext(name) {
				if (!this.captureCanvasContext) {
					var e = this.getCaptureCanvas(name).getContext("2d");
					this.captureCanvasContext = e;
				}
				return this.captureCanvasContext;
			}
		}, {
			key: "getCaptureCanvas",
			value: function toggleGroupNameEdit(group) {
				if (!this.captureCanvas) {
					var $slaves = this.createCaptureCanvas(group);
					this.captureCanvas = $slaves;
				}
				return this.captureCanvas;
			}
		}, {
			key: "drawImageOnCanvas",
			value: function resizeCanvasImage(canvas, img) {
				canvas.drawImage(img, 0, 0);
			}
		}, {
			key: "decodeBitmap",
			value: function get(data) {
				return this.reader.decode(data, this._hints);
			}
		}, {
			key: "createCaptureCanvas",
			value: function initCanvas(element) {
				if ("undefined" == typeof document) {
					return this._destroyCaptureCanvas(), null;
				}
				/** @type {!Element} */
				var elem = document.createElement("canvas");
				var width = void 0;
				var height = void 0;
				return void 0 !== element && (element instanceof HTMLVideoElement ? (width = element.videoWidth, height = element.videoHeight) : element instanceof HTMLImageElement && (width = element.naturalWidth || element.width, height = element.naturalHeight || element.height)), elem.style.width = width + "px", elem.style.height = height + "px", elem.width = width, elem.height = height, elem;
			}
		}, {
			key: "stopStreams",
			value: function getMixedVideoStream() {
				if (this.stream) {
					this.stream.getVideoTracks().forEach(function (incoming_item) {
						return incoming_item.stop();
					});
					this.stream = void 0;
				}
				if (false === this._stopAsyncDecode) {
					this.stopAsyncDecode();
				}
				if (false === this._stopContinuousDecode) {
					this.stopContinuousDecode();
				}
			}
		}, {
			key: "reset",
			value: function reset() {
				this.stopStreams();
				this._destroyVideoElement();
				this._destroyImageElement();
				this._destroyCaptureCanvas();
			}
		}, {
			key: "_destroyVideoElement",
			value: function handleVideoEvent() {
				if (this.videoElement) {
					if (void 0 !== this.videoEndedListener) {
						this.videoElement.removeEventListener("ended", this.videoEndedListener);
					}
					if (void 0 !== this.videoPlayingEventListener) {
						this.videoElement.removeEventListener("playing", this.videoPlayingEventListener);
					}
					if (void 0 !== this.videoCanPlayListener) {
						this.videoElement.removeEventListener("loadedmetadata", this.videoCanPlayListener);
					}
					this.cleanVideoSource(this.videoElement);
					this.videoElement = void 0;
				}
			}
		}, {
			key: "_destroyImageElement",
			value: function loadImage() {
				if (this.imageElement) {
					if (void 0 !== this.imageLoadedListener) {
						this.imageElement.removeEventListener("load", this.imageLoadedListener);
					}
					this.imageElement.src = void 0;
					this.imageElement.removeAttribute("src");
					this.imageElement = void 0;
				}
			}
		}, {
			key: "_destroyCaptureCanvas",
			value: function _destroyCaptureCanvas() {
				this.captureCanvasContext = void 0;
				this.captureCanvas = void 0;
			}
		}, {
			key: "addVideoSource",
			value: function setMediaElementSource(element, stream) {
				try {
					/** @type {string} */
					element.srcObject = stream;
				} catch (r) {
					/** @type {string} */
					element.src = URL.createObjectURL(stream);
				}
			}
		}, {
			key: "cleanVideoSource",
			value: function init(video) {
				try {
					/** @type {null} */
					video.srcObject = null;
				} catch (e) {
					/** @type {string} */
					video.src = "";
				}
				this.videoElement.removeAttribute("src");
			}
		}, {
			key: "hasNavigator",
			get: function setStatusListener() {
				return "undefined" != typeof navigator;
			}
		}, {
			key: "isMediaDevicesSuported",
			get: function camera() {
				return this.hasNavigator && !!navigator.mediaDevices;
			}
		}, {
			key: "canEnumerateDevices",
			get: function enumerateDevices() {
				return !(!this.isMediaDevicesSuported || !navigator.mediaDevices.enumerateDevices);
			}
		}, {
			key: "timeBetweenDecodingAttempts",
			get: function get() {
				return this._timeBetweenDecodingAttempts;
			},
			set: function signInListener(val) {
				this._timeBetweenDecodingAttempts = val < 0 ? 0 : val;
			}
		}, {
			key: "hints",
			set: function format_error(name) {
				this._hints = name || null;
			},
			get: function get() {
				return this._hints;
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var result = function () {
		/**
		 * @param {string} value
		 * @param {string} _
		 * @return {undefined}
		 */
		function F(value, _) {
			var numBits = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null == _ ? 0 : 8 * _.length;
			var matched_check = arguments[3];
			var fmt = arguments[4];
			var now = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : System.currentTimeMillis();
			_classCallCheck2(this, F);
			/** @type {string} */
			this.text = value;
			/** @type {string} */
			this.rawBytes = _;
			this.numBits = numBits;
			this.resultPoints = matched_check;
			this.format = fmt;
			this.timestamp = now;
			/** @type {string} */
			this.text = value;
			/** @type {string} */
			this.rawBytes = _;
			this.numBits = null == numBits ? null == _ ? 0 : 8 * _.length : numBits;
			this.resultPoints = matched_check;
			this.format = fmt;
			/** @type {null} */
			this.resultMetadata = null;
			this.timestamp = null == now ? System.currentTimeMillis() : now;
		}
		_createClass2(F, [{
			key: "getText",
			value: function monolingualtext() {
				return this.text;
			}
		}, {
			key: "getRawBytes",
			value: function getRawBytes() {
				return this.rawBytes;
			}
		}, {
			key: "getNumBits",
			value: function getNumBits() {
				return this.numBits;
			}
		}, {
			key: "getResultPoints",
			value: function getResultPoints() {
				return this.resultPoints;
			}
		}, {
			key: "getBarcodeFormat",
			value: function makeApi() {
				return this.format;
			}
		}, {
			key: "getResultMetadata",
			value: function getResultMetadata() {
				return this.resultMetadata;
			}
		}, {
			key: "putMetadata",
			value: function genErrors(doc, klass) {
				if (null === this.resultMetadata) {
					/** @type {!Map} */
					this.resultMetadata = new Map;
				}
				this.resultMetadata.set(doc, klass);
			}
		}, {
			key: "putAllMetadata",
			value: function parseGroupingBy(data) {
				if (null !== data) {
					if (null === this.resultMetadata) {
						/** @type {string} */
						this.resultMetadata = data;
					} else {
						/** @type {!Map} */
						this.resultMetadata = new Map(data);
					}
				}
			}
		}, {
			key: "addResultPoints",
			value: function convert2Dto1D(bytes) {
				var b = this.resultPoints;
				if (null === b) {
					/** @type {!Object} */
					this.resultPoints = bytes;
				} else {
					if (null !== bytes && bytes.length > 0) {
						/** @type {!Array} */
						var result = new Array(b.length + bytes.length);
						System.arraycopy(b, 0, result, 0, b.length);
						System.arraycopy(bytes, 0, result, b.length, bytes.length);
						/** @type {!Array} */
						this.resultPoints = result;
					}
				}
			}
		}, {
			key: "getTimestamp",
			value: function getTaskDate() {
				return this.timestamp;
			}
		}, {
			key: "toString",
			value: function monolingualtext() {
				return this.text;
			}
		}]);
		return F;
	}();
	!function (exports) {
		/** @type {string} */
		exports[exports.AZTEC = 0] = "AZTEC";
		/** @type {string} */
		exports[exports.CODABAR = 1] = "CODABAR";
		/** @type {string} */
		exports[exports.CODE_39 = 2] = "CODE_39";
		/** @type {string} */
		exports[exports.CODE_93 = 3] = "CODE_93";
		/** @type {string} */
		exports[exports.CODE_128 = 4] = "CODE_128";
		/** @type {string} */
		exports[exports.DATA_MATRIX = 5] = "DATA_MATRIX";
		/** @type {string} */
		exports[exports.EAN_8 = 6] = "EAN_8";
		/** @type {string} */
		exports[exports.EAN_13 = 7] = "EAN_13";
		/** @type {string} */
		exports[exports.ITF = 8] = "ITF";
		/** @type {string} */
		exports[exports.MAXICODE = 9] = "MAXICODE";
		/** @type {string} */
		exports[exports.PDF_417 = 10] = "PDF_417";
		/** @type {string} */
		exports[exports.QR_CODE = 11] = "QR_CODE";
		/** @type {string} */
		exports[exports.RSS_14 = 12] = "RSS_14";
		/** @type {string} */
		exports[exports.RSS_EXPANDED = 13] = "RSS_EXPANDED";
		/** @type {string} */
		exports[exports.UPC_A = 14] = "UPC_A";
		/** @type {string} */
		exports[exports.UPC_E = 15] = "UPC_E";
		/** @type {string} */
		exports[exports.UPC_EAN_EXTENSION = 16] = "UPC_EAN_EXTENSION";
	}(element || (element = {}));
	var doc;
	/** @type {(undefined|{AZTEC: number, CODABAR: number, CODE_128: number, CODE_39: number, CODE_93: number, DATA_MATRIX: number, EAN_13: number, EAN_8: number, ITF: number, MAXICODE: number, PDF_417: number, QR_CODE: number, RSS_14: number, RSS_EXPANDED: number, UPC_A: number, UPC_E: number, UPC_EAN_EXTENSION: number})} */
	var change = element;
	!function (PreparsedElementType) {
		/** @type {string} */
		PreparsedElementType[PreparsedElementType.OTHER = 0] = "OTHER";
		/** @type {string} */
		PreparsedElementType[PreparsedElementType.ORIENTATION = 1] = "ORIENTATION";
		/** @type {string} */
		PreparsedElementType[PreparsedElementType.BYTE_SEGMENTS = 2] = "BYTE_SEGMENTS";
		/** @type {string} */
		PreparsedElementType[PreparsedElementType.ERROR_CORRECTION_LEVEL = 3] = "ERROR_CORRECTION_LEVEL";
		/** @type {string} */
		PreparsedElementType[PreparsedElementType.ISSUE_NUMBER = 4] = "ISSUE_NUMBER";
		/** @type {string} */
		PreparsedElementType[PreparsedElementType.SUGGESTED_PRICE = 5] = "SUGGESTED_PRICE";
		/** @type {string} */
		PreparsedElementType[PreparsedElementType.POSSIBLE_COUNTRY = 6] = "POSSIBLE_COUNTRY";
		/** @type {string} */
		PreparsedElementType[PreparsedElementType.UPC_EAN_EXTENSION = 7] = "UPC_EAN_EXTENSION";
		/** @type {string} */
		PreparsedElementType[PreparsedElementType.PDF417_EXTRA_METADATA = 8] = "PDF417_EXTRA_METADATA";
		/** @type {string} */
		PreparsedElementType[PreparsedElementType.STRUCTURED_APPEND_SEQUENCE = 9] = "STRUCTURED_APPEND_SEQUENCE";
		/** @type {string} */
		PreparsedElementType[PreparsedElementType.STRUCTURED_APPEND_PARITY = 10] = "STRUCTURED_APPEND_PARITY";
	}(doc || (doc = {}));
	var Tokens;
	var n;
	var kb;
	var fs;
	var SimpleInputType;
	var TokenTypes;
	/** @type {(undefined|{BYTE_SEGMENTS: number, ERROR_CORRECTION_LEVEL: number, ISSUE_NUMBER: number, ORIENTATION: number, OTHER: number, PDF417_EXTRA_METADATA: number, POSSIBLE_COUNTRY: number, STRUCTURED_APPEND_PARITY: number, STRUCTURED_APPEND_SEQUENCE: number, SUGGESTED_PRICE: number, UPC_EAN_EXTENSION: number})} */
	var value = doc;
	var Response = function () {
		/**
		 * @param {string} value
		 * @param {string} options
		 * @param {?} element
		 * @param {string} aureliaUtils
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3(value, options, element, aureliaUtils) {
			var i = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : -1;
			var s = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : -1;
			_classCallCheck2(this, TempusDominusBootstrap3);
			/** @type {string} */
			this.rawBytes = value;
			/** @type {string} */
			this.text = options;
			this.byteSegments = element;
			/** @type {string} */
			this.ecLevel = aureliaUtils;
			this.structuredAppendSequenceNumber = i;
			this.structuredAppendParity = s;
			/** @type {number} */
			this.numBits = null == value ? 0 : 8 * value.length;
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "getRawBytes",
			value: function getRawBytes() {
				return this.rawBytes;
			}
		}, {
			key: "getNumBits",
			value: function getNumBits() {
				return this.numBits;
			}
		}, {
			key: "setNumBits",
			value: function binary(numBits) {
				/** @type {number} */
				this.numBits = numBits;
			}
		}, {
			key: "getText",
			value: function monolingualtext() {
				return this.text;
			}
		}, {
			key: "getByteSegments",
			value: function getByteSegments() {
				return this.byteSegments;
			}
		}, {
			key: "getECLevel",
			value: function getECLevel() {
				return this.ecLevel;
			}
		}, {
			key: "getErrorsCorrected",
			value: function getErrorsCorrected() {
				return this.errorsCorrected;
			}
		}, {
			key: "setErrorsCorrected",
			value: function prefetchGroupsInfo(canCreateDiscussions) {
				this.errorsCorrected = canCreateDiscussions;
			}
		}, {
			key: "getErasures",
			value: function getErasures() {
				return this.erasures;
			}
		}, {
			key: "setErasures",
			value: function prefetchGroupsInfo(canCreateDiscussions) {
				this.erasures = canCreateDiscussions;
			}
		}, {
			key: "getOther",
			value: function selectPlural() {
				return this.other;
			}
		}, {
			key: "setOther",
			value: function TakeUntilObservable(other) {
				/** @type {!Object} */
				this.other = other;
			}
		}, {
			key: "hasStructuredAppend",
			value: function hasStructuredAppend() {
				return this.structuredAppendParity >= 0 && this.structuredAppendSequenceNumber >= 0;
			}
		}, {
			key: "getStructuredAppendParity",
			value: function getStructuredAppendParity() {
				return this.structuredAppendParity;
			}
		}, {
			key: "getStructuredAppendSequenceNumber",
			value: function getStructuredAppendSequenceNumber() {
				return this.structuredAppendSequenceNumber;
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var GF256 = function () {
		/**
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3() {
			_classCallCheck2(this, TempusDominusBootstrap3);
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "exp",
			value: function openTiledImage(kind) {
				return this.expTable[kind];
			}
		}, {
			key: "log",
			value: function test(a) {
				if (0 === a) {
					throw new Function;
				}
				return this.logTable[a];
			}
		}], [{
			key: "addOrSubtract",
			value: function LineSegment2(s, e) {
				return s ^ e;
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var Node = function () {
		/**
		 * @param {string} parent
		 * @param {!Array} indices
		 * @return {undefined}
		 */
		function init(parent, indices) {
			_classCallCheck2(this, init);
			if (0 === indices.length) {
				throw new Function;
			}
			/** @type {string} */
			this.field = parent;
			var end = indices.length;
			if (end > 1 && 0 === indices[0]) {
				/** @type {number} */
				var start = 1;
				for (; start < end && 0 === indices[start];) {
					start++;
				}
				if (start === end) {
					/** @type {!Int32Array} */
					this.coefficients = Int32Array.from([0]);
				} else {
					/** @type {!Int32Array} */
					this.coefficients = new Int32Array(end - start);
					System.arraycopy(indices, start, this.coefficients, 0, this.coefficients.length);
				}
			} else {
				/** @type {!Array} */
				this.coefficients = indices;
			}
		}
		_createClass2(init, [{
			key: "getCoefficients",
			value: function getCoefficients() {
				return this.coefficients;
			}
		}, {
			key: "getDegree",
			value: function _Class() {
				return this.coefficients.length - 1;
			}
		}, {
			key: "isZero",
			value: function Distortion() {
				return 0 === this.coefficients[0];
			}
		}, {
			key: "getCoefficient",
			value: function _Class(_arg) {
				return this.coefficients[this.coefficients.length - 1 - _arg];
			}
		}, {
			key: "evaluateAt",
			value: function _cross(a) {
				if (0 === a) {
					return this.getCoefficient(0);
				}
				var largerCoefficients = this.coefficients;
				var result = void 0;
				if (1 === a) {
					/** @type {number} */
					result = 0;
					/** @type {number} */
					var i = 0;
					var resl = largerCoefficients.length;
					for (; i !== resl; i++) {
						var unloadedImgElement = largerCoefficients[i];
						result = GF256.addOrSubtract(result, unloadedImgElement);
					}
					return result;
				}
				result = largerCoefficients[0];
				var patchLen = largerCoefficients.length;
				var f = this.field;
				/** @type {number} */
				var i = 1;
				for (; i < patchLen; i++) {
					result = GF256.addOrSubtract(f.multiply(a, result), largerCoefficients[i]);
				}
				return result;
			}
		}, {
			key: "addOrSubtract",
			value: function filter(other) {
				if (!this.field.equals(other.field)) {
					throw new Function("GenericGFPolys do not have same GenericGF field");
				}
				if (this.isZero()) {
					return other;
				}
				if (other.isZero()) {
					return this;
				}
				var smallerCoefficients = this.coefficients;
				var largerCoefficients = other.coefficients;
				if (smallerCoefficients.length > largerCoefficients.length) {
					var temp = smallerCoefficients;
					smallerCoefficients = largerCoefficients;
					largerCoefficients = temp;
				}
				/** @type {!Int32Array} */
				var result = new Int32Array(largerCoefficients.length);
				/** @type {number} */
				var n = largerCoefficients.length - smallerCoefficients.length;
				System.arraycopy(largerCoefficients, 0, result, 0, n);
				/** @type {number} */
				var i = n;
				for (; i < largerCoefficients.length; i++) {
					result[i] = GF256.addOrSubtract(smallerCoefficients[i - n], largerCoefficients[i]);
				}
				return new init(this.field, result);
			}
		}, {
			key: "multiply",
			value: function update(other) {
				if (!this.field.equals(other.field)) {
					throw new Function("GenericGFPolys do not have same GenericGF field");
				}
				if (this.isZero() || other.isZero()) {
					return this.field.getZero();
				}
				var aCoefficients = this.coefficients;
				var aLength = aCoefficients.length;
				var bCoefficients = other.coefficients;
				var bLength = bCoefficients.length;
				/** @type {!Int32Array} */
				var product = new Int32Array(aLength + bLength - 1);
				var e = this.field;
				/** @type {number} */
				var i = 0;
				for (; i < aLength; i++) {
					var aCoeff = aCoefficients[i];
					/** @type {number} */
					var j = 0;
					for (; j < bLength; j++) {
						product[i + j] = GF256.addOrSubtract(product[i + j], e.multiply(aCoeff, bCoefficients[j]));
					}
				}
				return new init(e, product);
			}
		}, {
			key: "multiplyScalar",
			value: function sub(num) {
				if (0 === num) {
					return this.field.getZero();
				}
				if (1 === num) {
					return this;
				}
				var size = this.coefficients.length;
				var f = this.field;
				/** @type {!Int32Array} */
				var negativeCoefficients = new Int32Array(size);
				var c = this.coefficients;
				/** @type {number} */
				var i = 0;
				for (; i < size; i++) {
					negativeCoefficients[i] = f.multiply(c[i], num);
				}
				return new init(f, negativeCoefficients);
			}
		}, {
			key: "multiplyByMonomial",
			value: function init(n, value) {
				if (n < 0) {
					throw new Function;
				}
				if (0 === value) {
					return this.field.getZero();
				}
				var m = this.coefficients;
				var size = m.length;
				/** @type {!Int32Array} */
				var result = new Int32Array(size + n);
				var f = this.field;
				/** @type {number} */
				var i = 0;
				for (; i < size; i++) {
					result[i] = f.multiply(m[i], value);
				}
				return new init(f, result);
			}
		}, {
			key: "divide",
			value: function update(other) {
				if (!this.field.equals(other.field)) {
					throw new Function("GenericGFPolys do not have same GenericGF field");
				}
				if (other.isZero()) {
					throw new Function("Divide by 0");
				}
				var Matrix4 = this.field;
				var quotient = Matrix4.getZero();
				var remainder = this;
				var m = other.getCoefficient(other.getDegree());
				var center = Matrix4.inverse(m);
				for (; remainder.getDegree() >= other.getDegree() && !remainder.isZero();) {
					/** @type {number} */
					var degreeDifference = remainder.getDegree() - other.getDegree();
					var scale = Matrix4.multiply(remainder.getCoefficient(remainder.getDegree()), center);
					var term = other.multiplyByMonomial(degreeDifference, scale);
					var iterationQuotient = Matrix4.buildMonomial(degreeDifference, scale);
					quotient = quotient.addOrSubtract(iterationQuotient);
					remainder = remainder.addOrSubtract(term);
				}
				return [quotient, remainder];
			}
		}, {
			key: "toString",
			value: function toString() {
				/** @type {string} */
				var result = "";
				var degree = this.getDegree();
				for (; degree >= 0; degree--) {
					var coefficient = this.getCoefficient(degree);
					if (0 !== coefficient) {
						if (coefficient < 0 ? (result = result + " - ", coefficient = -coefficient) : result.length > 0 && (result = result + " + "), 0 === degree || 1 !== coefficient) {
							var value = this.field.log(coefficient);
							if (0 === value) {
								/** @type {string} */
								result = result + "1";
							} else {
								if (1 === value) {
									/** @type {string} */
									result = result + "a";
								} else {
									/** @type {string} */
									result = result + "a^";
									/** @type {string} */
									result = result + value;
								}
							}
						}
						if (0 !== degree) {
							if (1 === degree) {
								/** @type {string} */
								result = result + "x";
							} else {
								/** @type {string} */
								result = result + "x^";
								/** @type {string} */
								result = result + degree;
							}
						}
					}
				}
				return result;
			}
		}]);
		return init;
	}();
	var GF256Poly = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @return {?}
		 */
		function CacheLink() {
			_classCallCheck2(this, CacheLink);
			return _possibleConstructorReturn(this, (CacheLink.__proto__ || Object.getPrototypeOf(CacheLink)).apply(this, arguments));
		}
		_inherits(CacheLink, _WebInspector$GeneralTreeElement);
		return CacheLink;
	}(input);
	/** @type {string} */
	GF256Poly.kind = "ArithmeticException";
	var r = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @param {number} value
		 * @param {number} size
		 * @param {?} indices
		 * @return {?}
		 */
		function Model(value, size, indices) {
			var self;
			_classCallCheck2(this, Model);
			self = _possibleConstructorReturn(this, (Model.__proto__ || Object.getPrototypeOf(Model)).call(this));
			self;
			/** @type {number} */
			self.primitive = value;
			/** @type {number} */
			self.size = size;
			self.generatorBase = indices;
			/** @type {!Int32Array} */
			var arr = new Int32Array(size);
			/** @type {number} */
			var result = 1;
			/** @type {number} */
			var j = 0;
			for (; j < size; j++) {
				/** @type {number} */
				arr[j] = result;
				if ((result = result * 2) >= size) {
					/** @type {number} */
					result = result ^ value;
					/** @type {number} */
					result = result & size - 1;
				}
			}
			/** @type {!Int32Array} */
			self.expTable = arr;
			/** @type {!Int32Array} */
			var out = new Int32Array(size);
			/** @type {number} */
			var i = 0;
			for (; i < size - 1; i++) {
				/** @type {number} */
				out[arr[i]] = i;
			}
			/** @type {!Int32Array} */
			self.logTable = out;
			self.zero = new Node(self, Int32Array.from([0]));
			self.one = new Node(self, Int32Array.from([1]));
			return self;
		}
		_inherits(Model, _WebInspector$GeneralTreeElement);
		_createClass2(Model, [{
			key: "getZero",
			value: function getBDDFromFormula() {
				return this.zero;
			}
		}, {
			key: "getOne",
			value: function getBDDFromFormula() {
				return this.one;
			}
		}, {
			key: "buildMonomial",
			value: function init(size, width) {
				if (size < 0) {
					throw new Function;
				}
				if (0 === width) {
					return this.zero;
				}
				/** @type {!Int32Array} */
				var data = new Int32Array(size + 1);
				return data[0] = width, new Node(this, data);
			}
		}, {
			key: "inverse",
			value: function GF256(b) {
				if (0 === b) {
					throw new GF256Poly;
				}
				return this.expTable[this.size - this.logTable[b] - 1];
			}
		}, {
			key: "multiply",
			value: function GF256(a, b) {
				return 0 === a || 0 === b ? 0 : this.expTable[(this.logTable[a] + this.logTable[b]) % (this.size - 1)];
			}
		}, {
			key: "getSize",
			value: function fileSize() {
				return this.size;
			}
		}, {
			key: "getGeneratorBase",
			value: function getGeneratorBase() {
				return this.generatorBase;
			}
		}, {
			key: "toString",
			value: function changeEvent() {
				return "GF(0x" + scope.toHexString(this.primitive) + "," + this.size + ")";
			}
		}, {
			key: "equals",
			value: function prefetchGroupsInfo(canCreateDiscussions) {
				return canCreateDiscussions === this;
			}
		}]);
		return Model;
	}(GF256);
	r.AZTEC_DATA_12 = new r(4201, 4096, 1);
	r.AZTEC_DATA_10 = new r(1033, 1024, 1);
	r.AZTEC_DATA_6 = new r(67, 64, 1);
	r.AZTEC_PARAM = new r(19, 16, 1);
	r.QR_CODE_FIELD_256 = new r(285, 256, 0);
	r.DATA_MATRIX_FIELD_256 = new r(301, 256, 1);
	r.AZTEC_DATA_8 = r.DATA_MATRIX_FIELD_256;
	r.MAXICODE_FIELD_64 = r.AZTEC_DATA_6;
	var Parser = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @return {?}
		 */
		function CacheLink() {
			_classCallCheck2(this, CacheLink);
			return _possibleConstructorReturn(this, (CacheLink.__proto__ || Object.getPrototypeOf(CacheLink)).apply(this, arguments));
		}
		_inherits(CacheLink, _WebInspector$GeneralTreeElement);
		return CacheLink;
	}(input);
	/** @type {string} */
	Parser.kind = "ReedSolomonException";
	var Path = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @return {?}
		 */
		function CacheLink() {
			_classCallCheck2(this, CacheLink);
			return _possibleConstructorReturn(this, (CacheLink.__proto__ || Object.getPrototypeOf(CacheLink)).apply(this, arguments));
		}
		_inherits(CacheLink, _WebInspector$GeneralTreeElement);
		return CacheLink;
	}(input);
	/** @type {string} */
	Path.kind = "IllegalStateException";
	var Uint16Array = function () {
		/**
		 * @param {string} element
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3(element) {
			_classCallCheck2(this, TempusDominusBootstrap3);
			/** @type {string} */
			this.field = element;
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "decode",
			value: function parse(received, numECCodewords) {
				var Math = this.field;
				var poly = new Node(Math, received);
				/** @type {!Int32Array} */
				var S = new Int32Array(numECCodewords);
				/** @type {boolean} */
				var s = true;
				/** @type {number} */
				var mu = 0;
				for (; mu < numECCodewords; mu++) {
					var i = poly.evaluateAt(Math.exp(mu + Math.getGeneratorBase()));
					S[S.length - 1 - mu] = i;
					if (0 !== i) {
						/** @type {boolean} */
						s = false;
					}
				}
				if (s) {
					return;
				}
				var syndrome = new Node(Math, S);
				var sigmaOmega = this.runEuclideanAlgorithm(Math.buildMonomial(numECCodewords, 1), syndrome, numECCodewords);
				var sigma = sigmaOmega[0];
				var omega = sigmaOmega[1];
				var errorLocations = this.findErrorLocations(sigma);
				var errorMagnitudes = this.findErrorMagnitudes(omega, errorLocations);
				/** @type {number} */
				var i = 0;
				for (; i < errorLocations.length; i++) {
					/** @type {number} */
					var name = received.length - 1 - Math.log(errorLocations[i]);
					if (name < 0) {
						throw new Parser("Bad error location");
					}
					received[name] = r.addOrSubtract(received[name], errorMagnitudes[i]);
				}
			}
		}, {
			key: "runEuclideanAlgorithm",
			value: function update(a, b, rev) {
				if (a.getDegree() < b.getDegree()) {
					/** @type {number} */
					var bytes = a;
					/** @type {number} */
					a = b;
					b = bytes;
				}
				var self = this.field;
				/** @type {number} */
				var rLast = a;
				/** @type {number} */
				var r = b;
				var tLast = self.getZero();
				var t = self.getOne();
				for (; r.getDegree() >= (rev / 2 | 0);) {
					var rLastLast = rLast;
					var tLastLast = tLast;
					if (tLast = t, (rLast = r).isZero()) {
						throw new Parser("r_{i-1} was zero");
					}
					r = rLastLast;
					var q = self.getZero();
					var document = rLast.getCoefficient(rLast.getDegree());
					var D = self.inverse(document);
					for (; r.getDegree() >= rLast.getDegree() && !r.isZero();) {
						/** @type {number} */
						var degreeDiff = r.getDegree() - rLast.getDegree();
						var scale = self.multiply(r.getCoefficient(r.getDegree()), D);
						q = q.addOrSubtract(self.buildMonomial(degreeDiff, scale));
						r = r.addOrSubtract(rLast.multiplyByMonomial(degreeDiff, scale));
					}
					if (t = q.multiply(tLast).addOrSubtract(tLastLast), r.getDegree() >= rLast.getDegree()) {
						throw new Path("Division algorithm failed to reduce polynomial?");
					}
				}
				var cov = t.getCoefficient(0);
				if (0 === cov) {
					throw new Parser("sigmaTilde(0) was zero");
				}
				var color = self.inverse(cov);
				return [t.multiplyScalar(color), r.multiplyScalar(color)];
			}
		}, {
			key: "findErrorLocations",
			value: function update(errorLocator) {
				var count = errorLocator.getDegree();
				if (1 === count) {
					return Int32Array.from([errorLocator.getCoefficient(1)]);
				}
				/** @type {!Int32Array} */
				var result = new Int32Array(count);
				/** @type {number} */
				var index = 0;
				var field = this.field;
				/** @type {number} */
				var i = 1;
				for (; i < field.getSize() && index < count; i++) {
					if (0 === errorLocator.evaluateAt(i)) {
						result[index] = field.inverse(i);
						index++;
					}
				}
				if (index !== count) {
					throw new Parser("Error locator degree does not match number of roots");
				}
				return result;
			}
		}, {
			key: "findErrorMagnitudes",
			value: function render(h, m) {
				var len = m.length;
				/** @type {!Int32Array} */
				var result = new Int32Array(len);
				var _ = this.field;
				/** @type {number} */
				var k = 0;
				for (; k < len; k++) {
					var i = _.inverse(m[k]);
					/** @type {number} */
					var denominator = 1;
					/** @type {number} */
					var j = 0;
					for (; j < len; j++) {
						if (k !== j) {
							var regexpImage = _.multiply(m[j], i);
							/** @type {number} */
							var hasVar = 0 == (1 & regexpImage) ? 1 | regexpImage : -2 & regexpImage;
							denominator = _.multiply(denominator, hasVar);
						}
					}
					result[k] = _.multiply(h.evaluateAt(i), _.inverse(denominator));
					if (0 !== _.getGeneratorBase()) {
						result[k] = _.multiply(result[k], i);
					}
				}
				return result;
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	!function (Tokens) {
		/** @type {string} */
		Tokens[Tokens.UPPER = 0] = "UPPER";
		/** @type {string} */
		Tokens[Tokens.LOWER = 1] = "LOWER";
		/** @type {string} */
		Tokens[Tokens.MIXED = 2] = "MIXED";
		/** @type {string} */
		Tokens[Tokens.DIGIT = 3] = "DIGIT";
		/** @type {string} */
		Tokens[Tokens.PUNCT = 4] = "PUNCT";
		/** @type {string} */
		Tokens[Tokens.BINARY = 5] = "BINARY";
	}(Tokens || (Tokens = {}));
	var data = function () {
		/**
		 * @return {undefined}
		 */
		function self() {
			_classCallCheck2(this, self);
		}
		_createClass2(self, [{
			key: "decode",
			value: function _messageReceived(event) {
				this.ddata = event;
				var button2 = event.getBits();
				var button2Component = this.extractBits(button2);
				var code = this.correctBits(button2Component);
				var status = self.convertBoolArrayToByteArray(code);
				var res = self.getEncodedData(code);
				var response = new Response(status, res, null, null);
				return response.setNumBits(code.length), response;
			}
		}, {
			key: "correctBits",
			value: function benchmark(list) {
				var data = void 0;
				var height = void 0;
				if (this.ddata.getNbLayers() <= 2) {
					/** @type {number} */
					height = 6;
					data = r.AZTEC_DATA_6;
				} else {
					if (this.ddata.getNbLayers() <= 8) {
						/** @type {number} */
						height = 8;
						data = r.AZTEC_DATA_8;
					} else {
						if (this.ddata.getNbLayers() <= 22) {
							/** @type {number} */
							height = 10;
							data = r.AZTEC_DATA_10;
						} else {
							/** @type {number} */
							height = 12;
							data = r.AZTEC_DATA_12;
						}
					}
				}
				var d = this.ddata.getNbDatablocks();
				/** @type {number} */
				var n = list.length / height;
				if (n < d) {
					throw new Date;
				}
				/** @type {number} */
				var offset = list.length % height;
				/** @type {!Int32Array} */
				var array = new Int32Array(n);
				/** @type {number} */
				var i = 0;
				for (; i < n; i++, offset = offset + height) {
					array[i] = self.readCode(list, offset, height);
				}
				try {
					(new Uint16Array(data)).decode(array, n - d);
				} catch (interpretdYear) {
					throw new Date(interpretdYear);
				}
				/** @type {number} */
				var undefined = (1 << height) - 1;
				/** @type {number} */
				var h = 0;
				/** @type {number} */
				var m = 0;
				for (; m < d; m++) {
					/** @type {number} */
					var value = array[m];
					if (0 === value || value === undefined) {
						throw new Date;
					}
					if (!(1 !== value && value !== undefined - 1)) {
						h++;
					}
				}
				/** @type {!Array} */
				var t = new Array(d * height - h);
				/** @type {number} */
				var y = 0;
				/** @type {number} */
				var j = 0;
				for (; j < d; j++) {
					/** @type {number} */
					var value = array[j];
					if (1 === value || value === undefined - 1) {
						t.fill(value > 1, y, y + height - 1);
						/** @type {number} */
						y = y + (height - 1);
					} else {
						/** @type {number} */
						var position = height - 1;
						for (; position >= 0; --position) {
							/** @type {boolean} */
							t[y++] = 0 != (value & 1 << position);
						}
					}
				}
				return t;
			}
		}, {
			key: "extractBits",
			value: function hash(object) {
				var n = this.ddata.isCompact();
				var popupLeft = this.ddata.getNbLayers();
				/** @type {number} */
				var x = (n ? 11 : 14) + 4 * popupLeft;
				/** @type {!Int32Array} */
				var data = new Int32Array(x);
				/** @type {!Array} */
				var result = new Array(this.totalBitsInLayer(popupLeft, n));
				if (n) {
					/** @type {number} */
					var i = 0;
					for (; i < data.length; i++) {
						/** @type {number} */
						data[i] = i;
					}
				} else {
					/** @type {number} */
					var i = x + 1 + 2 * scope.truncDivision(scope.truncDivision(x, 2) - 1, 15);
					/** @type {number} */
					var t = x / 2;
					var ic = scope.truncDivision(i, 2);
					/** @type {number} */
					var b = 0;
					for (; b < t; b++) {
						var id = b + scope.truncDivision(b, 15);
						/** @type {number} */
						data[t - b - 1] = ic - id - 1;
						data[t + b] = ic + id + 1;
					}
				}
				/** @type {number} */
				var actorLeft = 0;
				/** @type {number} */
				var ii = 0;
				for (; actorLeft < popupLeft; actorLeft++) {
					/** @type {number} */
					var nbComponents = 4 * (popupLeft - actorLeft) + (n ? 9 : 12);
					/** @type {number} */
					var r = 2 * actorLeft;
					/** @type {number} */
					var y = x - 1 - r;
					/** @type {number} */
					var j = 0;
					for (; j < nbComponents; j++) {
						/** @type {number} */
						var tbm = 2 * j;
						/** @type {number} */
						var i = 0;
						for (; i < 2; i++) {
							result[ii + tbm + i] = object.get(data[r + i], data[r + j]);
							result[ii + 2 * nbComponents + tbm + i] = object.get(data[r + j], data[y - i]);
							result[ii + 4 * nbComponents + tbm + i] = object.get(data[y - i], data[y - j]);
							result[ii + 6 * nbComponents + tbm + i] = object.get(data[y - j], data[r + i]);
						}
					}
					/** @type {number} */
					ii = ii + 8 * nbComponents;
				}
				return result;
			}
		}, {
			key: "totalBitsInLayer",
			value: function handleSlide(isSlidingUp, $cont) {
				return (($cont ? 88 : 112) + 16 * isSlidingUp) * isSlidingUp;
			}
		}], [{
			key: "highLevelDecode",
			value: function $get(mmCoreSplitViewBlock) {
				return this.getEncodedData(mmCoreSplitViewBlock);
			}
		}, {
			key: "getEncodedData",
			value: function randomString(stream) {
				var size = stream.length;
				var exitStatus = Tokens.UPPER;
				var status = Tokens.UPPER;
				/** @type {string} */
				var ret = "";
				/** @type {number} */
				var n = 0;
				for (; n < size;) {
					if (status === Tokens.BINARY) {
						if (size - n < 5) {
							break;
						}
						var length = self.readCode(stream, n, 5);
						if (n = n + 5, 0 === length) {
							if (size - n < 11) {
								break;
							}
							length = self.readCode(stream, n, 11) + 31;
							n = n + 11;
						}
						/** @type {number} */
						var i = 0;
						for (; i < length; i++) {
							if (size - n < 8) {
								n = size;
								break;
							}
							var c = self.readCode(stream, n, 8);
							ret = ret + _.castAsNonUtf8Char(c);
							n = n + 8;
						}
						status = exitStatus;
					} else {
						/** @type {number} */
						var count = status === Tokens.DIGIT ? 4 : 5;
						if (size - n < count) {
							break;
						}
						var value = self.readCode(stream, n, count);
						n = n + count;
						var s = self.getCharacter(status, value);
						if (s.startsWith("CTRL_")) {
							exitStatus = status;
							status = self.getTable(s.charAt(5));
							if ("L" === s.charAt(6)) {
								exitStatus = status;
							}
						} else {
							ret = ret + s;
							status = exitStatus;
						}
					}
				}
				return ret;
			}
		}, {
			key: "getTable",
			value: function createConfig(type) {
				switch (type) {
					case "L":
						return Tokens.LOWER;
					case "P":
						return Tokens.PUNCT;
					case "M":
						return Tokens.MIXED;
					case "D":
						return Tokens.DIGIT;
					case "B":
						return Tokens.BINARY;
					case "U":
					default:
						return Tokens.UPPER;
				}
			}
		}, {
			key: "getCharacter",
			value: function getLetter(value, action) {
				switch (value) {
					case Tokens.UPPER:
						return self.UPPER_TABLE[action];
					case Tokens.LOWER:
						return self.LOWER_TABLE[action];
					case Tokens.MIXED:
						return self.MIXED_TABLE[action];
					case Tokens.PUNCT:
						return self.PUNCT_TABLE[action];
					case Tokens.DIGIT:
						return self.DIGIT_TABLE[action];
					default:
						throw new Path("Bad table");
				}
			}
		}, {
			key: "readCode",
			value: function randomIntSetPairs(size, intSetSource, numberOfSets) {
				/** @type {number} */
				var idx = 0;
				/** @type {string} */
				var sizeIdx = intSetSource;
				for (; sizeIdx < intSetSource + numberOfSets; sizeIdx++) {
					/** @type {number} */
					idx = idx << 1;
					if (size[sizeIdx]) {
						/** @type {number} */
						idx = idx | 1;
					}
				}
				return idx;
			}
		}, {
			key: "readByte",
			value: function assertSameProps(a, b) {
				/** @type {number} */
				var i = a.length - b;
				return i >= 8 ? self.readCode(a, b, 8) : self.readCode(a, b, i) << 8 - i;
			}
		}, {
			key: "convertBoolArrayToByteArray",
			value: function copy(b) {
				/** @type {!Uint8Array} */
				var a = new Uint8Array((b.length + 7) / 8);
				/** @type {number} */
				var i = 0;
				for (; i < a.length; i++) {
					a[i] = self.readByte(b, 8 * i);
				}
				return a;
			}
		}]);
		return self;
	}();
	/** @type {!Array} */
	data.UPPER_TABLE = ["CTRL_PS", " ", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "CTRL_LL", "CTRL_ML", "CTRL_DL", "CTRL_BS"];
	/** @type {!Array} */
	data.LOWER_TABLE = ["CTRL_PS", " ", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "CTRL_US", "CTRL_ML", "CTRL_DL", "CTRL_BS"];
	/** @type {!Array} */
	data.MIXED_TABLE = ["CTRL_PS", " ", "\\1", "\\2", "\\3", "\\4", "\\5", "\\6", "\\7", "\b", "\t", "\n", "\\13", "\f", "\r", "\\33", "\\34", "\\35", "\\36", "\\37", "@", "\\", "^", "_", "`", "|", "~", "\\177", "CTRL_LL", "CTRL_UL", "CTRL_PL", "CTRL_BS"];
	/** @type {!Array} */
	data.PUNCT_TABLE = ["", "\r", "\r\n", ". ", ", ", ": ", "!", '"', "#", "$", "%", "&", "'", "(", ")", "*", "+", ",", "-", ".", "/", ":", ";", "<", "=", ">", "?", "[", "]", "{", "}", "CTRL_UL"];
	/** @type {!Array} */
	data.DIGIT_TABLE = ["CTRL_PS", " ", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ",", ".", "CTRL_UL", "CTRL_US"];
	var p = function () {
		/**
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3() {
			_classCallCheck2(this, TempusDominusBootstrap3);
		}
		_createClass2(TempusDominusBootstrap3, null, [{
			key: "round",
			value: function pulverize(total) {
				return NaN === total ? 0 : total <= Number.MIN_SAFE_INTEGER ? Number.MIN_SAFE_INTEGER : total >= Number.MAX_SAFE_INTEGER ? Number.MAX_SAFE_INTEGER : total + (total < 0 ? -.5 : .5) | 0;
			}
		}, {
			key: "distance",
			value: function _euclideanDistance(x2, y2, x1, y1) {
				/** @type {number} */
				var a1 = x2 - x1;
				/** @type {number} */
				var a2 = y2 - y1;
				return Math.sqrt(a1 * a1 + a2 * a2);
			}
		}, {
			key: "sum",
			value: function getMaxAndMinValues(values) {
				/** @type {number} */
				var newIntervals = 0;
				/** @type {number} */
				var i = 0;
				var il = values.length;
				for (; i !== il; i++) {
					newIntervals = newIntervals + values[i];
				}
				return newIntervals;
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var utils = function () {
		/**
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3() {
			_classCallCheck2(this, TempusDominusBootstrap3);
		}
		_createClass2(TempusDominusBootstrap3, null, [{
			key: "floatToIntBits",
			value: function parseCustomUrl(url) {
				return url;
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	/** @type {number} */
	utils.MAX_VALUE = Number.MAX_SAFE_INTEGER;
	var type = function () {
		/**
		 * @param {number} data
		 * @param {number} length
		 * @return {undefined}
		 */
		function Node(data, length) {
			_classCallCheck2(this, Node);
			/** @type {number} */
			this.x = data;
			/** @type {number} */
			this.y = length;
		}
		_createClass2(Node, [{
			key: "getX",
			value: function xAcc() {
				return this.x;
			}
		}, {
			key: "getY",
			value: function DEFAULT_TEXT() {
				return this.y;
			}
		}, {
			key: "equals",
			value: function intersectLineSegments(b) {
				if (b instanceof Node) {
					/** @type {!Object} */
					var pointB = b;
					return this.x === pointB.x && this.y === pointB.y;
				}
				return false;
			}
		}, {
			key: "hashCode",
			value: function hashCode() {
				return 31 * utils.floatToIntBits(this.x) + utils.floatToIntBits(this.y);
			}
		}, {
			key: "toString",
			value: function node_extents() {
				return "(" + this.x + "," + this.y + ")";
			}
		}], [{
			key: "orderBestPatterns",
			value: function orderBestPatterns(map) {
				var b = this.distance(map[0], map[1]);
				var g = this.distance(map[1], map[2]);
				var r = this.distance(map[0], map[2]);
				var val = void 0;
				var key = void 0;
				var value = void 0;
				if (g >= b && g >= r ? (key = map[0], val = map[1], value = map[2]) : r >= g && r >= b ? (key = map[1], val = map[0], value = map[2]) : (key = map[2], val = map[0], value = map[1]), this.crossProductZ(val, key, value) < 0) {
					var formValue = val;
					val = value;
					value = formValue;
				}
				map[0] = val;
				map[1] = key;
				map[2] = value;
			}
		}, {
			key: "distance",
			value: function checkClosePoint(firstPt, currPt) {
				return p.distance(firstPt.x, firstPt.y, currPt.x, currPt.y);
			}
		}, {
			key: "crossProductZ",
			value: function crossProductZ(pointA, pointB, pointC) {
				var bX = pointB.x;
				var bY = pointB.y;
				return (pointC.x - bX) * (pointA.y - bY) - (pointC.y - bY) * (pointA.x - bX);
			}
		}]);
		return Node;
	}();
	var HTMLTagSpecification = function () {
		/**
		 * @param {number} options
		 * @param {?} points
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3(options, points) {
			_classCallCheck2(this, TempusDominusBootstrap3);
			/** @type {number} */
			this.bits = options;
			this.points = points;
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "getBits",
			value: function getTarget() {
				return this.bits;
			}
		}, {
			key: "getPoints",
			value: function interfaceFunction() {
				return this.points;
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var Router = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @param {?} props
		 * @param {?} context
		 * @param {string} x
		 * @param {?} n
		 * @param {number} domElem
		 * @return {?}
		 */
		function Editor(props, context, x, n, domElem) {
			var _this;
			_classCallCheck2(this, Editor);
			_this = _possibleConstructorReturn(this, (Editor.__proto__ || Object.getPrototypeOf(Editor)).call(this, props, context));
			_this;
			/** @type {string} */
			_this.compact = x;
			_this.nbDatablocks = n;
			/** @type {number} */
			_this.nbLayers = domElem;
			return _this;
		}
		_inherits(Editor, _WebInspector$GeneralTreeElement);
		_createClass2(Editor, [{
			key: "getNbLayers",
			value: function getNbLayers() {
				return this.nbLayers;
			}
		}, {
			key: "getNbDatablocks",
			value: function getNbDatablocks() {
				return this.nbDatablocks;
			}
		}, {
			key: "isCompact",
			value: function isCompact() {
				return this.compact;
			}
		}]);
		return Editor;
	}(HTMLTagSpecification);
	var Element = function () {
		/**
		 * @param {!Object} value
		 * @param {number} item
		 * @param {number} isExploreTab
		 * @param {number} Xmain
		 * @return {undefined}
		 */
		function update(value, item, isExploreTab, Xmain) {
			_classCallCheck2(this, update);
			/** @type {!Object} */
			this.image = value;
			this.height = value.getHeight();
			this.width = value.getWidth();
			if (null == item) {
				item = update.INIT_SIZE;
			}
			if (null == isExploreTab) {
				/** @type {number} */
				isExploreTab = value.getWidth() / 2 | 0;
			}
			if (null == Xmain) {
				/** @type {number} */
				Xmain = value.getHeight() / 2 | 0;
			}
			/** @type {number} */
			var Xylabel = item / 2 | 0;
			if (this.leftInit = isExploreTab - Xylabel, this.rightInit = isExploreTab + Xylabel, this.upInit = Xmain - Xylabel, this.downInit = Xmain + Xylabel, this.upInit < 0 || this.leftInit < 0 || this.downInit >= this.height || this.rightInit >= this.width) {
				throw new TypeError;
			}
		}
		_createClass2(update, [{
			key: "detect",
			value: function layoutNodeImpl() {
				var start = this.leftInit;
				var i = this.rightInit;
				var type = this.upInit;
				var n = this.downInit;
				/** @type {boolean} */
				var shouldAvoid = false;
				/** @type {boolean} */
				var s = true;
				/** @type {boolean} */
				var tile = false;
				/** @type {boolean} */
				var def = false;
				/** @type {boolean} */
				var cmp = false;
				/** @type {boolean} */
				var component = false;
				/** @type {boolean} */
				var celldiag = false;
				var canvasWidth = this.width;
				var numValues = this.height;
				for (; s;) {
					/** @type {boolean} */
					s = false;
					/** @type {boolean} */
					var value = true;
					for (; (value || !def) && i < canvasWidth;) {
						if (value = this.containsBlackPoint(type, n, i, false)) {
							i++;
							/** @type {boolean} */
							s = true;
							/** @type {boolean} */
							def = true;
						} else {
							if (!def) {
								i++;
							}
						}
					}
					if (i >= canvasWidth) {
						/** @type {boolean} */
						shouldAvoid = true;
						break;
					}
					/** @type {boolean} */
					var c = true;
					for (; (c || !cmp) && n < numValues;) {
						if (c = this.containsBlackPoint(start, i, n, true)) {
							n++;
							/** @type {boolean} */
							s = true;
							/** @type {boolean} */
							cmp = true;
						} else {
							if (!cmp) {
								n++;
							}
						}
					}
					if (n >= numValues) {
						/** @type {boolean} */
						shouldAvoid = true;
						break;
					}
					/** @type {boolean} */
					var id = true;
					for (; (id || !component) && start >= 0;) {
						if (id = this.containsBlackPoint(type, n, start, false)) {
							start--;
							/** @type {boolean} */
							s = true;
							/** @type {boolean} */
							component = true;
						} else {
							if (!component) {
								start--;
							}
						}
					}
					if (start < 0) {
						/** @type {boolean} */
						shouldAvoid = true;
						break;
					}
					/** @type {boolean} */
					var args = true;
					for (; (args || !celldiag) && type >= 0;) {
						if (args = this.containsBlackPoint(start, i, type, true)) {
							type--;
							/** @type {boolean} */
							s = true;
							/** @type {boolean} */
							celldiag = true;
						} else {
							if (!celldiag) {
								type--;
							}
						}
					}
					if (type < 0) {
						/** @type {boolean} */
						shouldAvoid = true;
						break;
					}
					if (s) {
						/** @type {boolean} */
						tile = true;
					}
				}
				if (!shouldAvoid && tile) {
					/** @type {number} */
					var len = i - start;
					/** @type {null} */
					var number = null;
					/** @type {number} */
					var k = 1;
					for (; null === number && k < len; k++) {
						number = this.getBlackPointOnSegment(start, n - k, start + k, n);
					}
					if (null == number) {
						throw new TypeError;
					}
					/** @type {null} */
					var targetValue = null;
					/** @type {number} */
					var idx = 1;
					for (; null === targetValue && idx < len; idx++) {
						targetValue = this.getBlackPointOnSegment(start, type + idx, start + idx, type);
					}
					if (null == targetValue) {
						throw new TypeError;
					}
					/** @type {null} */
					var val = null;
					/** @type {number} */
					var z = 1;
					for (; null === val && z < len; z++) {
						val = this.getBlackPointOnSegment(i, type + z, i - z, type);
					}
					if (null == val) {
						throw new TypeError;
					}
					/** @type {null} */
					var head = null;
					/** @type {number} */
					var j = 1;
					for (; null === head && j < len; j++) {
						head = this.getBlackPointOnSegment(i, n - j, i - j, n);
					}
					if (null == head) {
						throw new TypeError;
					}
					return this.centerEdges(head, number, val, targetValue);
				}
				throw new TypeError;
			}
		}, {
			key: "getBlackPointOnSegment",
			value: function get(pos, s, x, y) {
				var zoom = p.round(p.distance(pos, s, x, y));
				/** @type {number} */
				var width = (x - pos) / zoom;
				/** @type {number} */
				var yStride = (y - s) / zoom;
				var image = this.image;
				/** @type {number} */
				var d = 0;
				for (; d < zoom; d++) {
					var val = p.round(pos + d * width);
					var data = p.round(s + d * yStride);
					if (image.get(val, data)) {
						return new type(val, data);
					}
				}
				return null;
			}
		}, {
			key: "centerEdges",
			value: function enable(t, f, platform, event) {
				var position = t.getX();
				var previousMonthLength = t.getY();
				var _sceneWidth = f.getX();
				var startingOffset = f.getY();
				var srcn = platform.getX();
				var skippedChars = platform.getY();
				var posTop = event.getX();
				var _sceneHeight = event.getY();
				var offset = update.CORR;
				return position < this.width / 2 ? [new type(posTop - offset, _sceneHeight + offset), new type(_sceneWidth + offset, startingOffset + offset), new type(srcn - offset, skippedChars - offset), new type(position + offset, previousMonthLength - offset)] : [new type(posTop + offset, _sceneHeight + offset), new type(_sceneWidth + offset, startingOffset - offset), new type(srcn - offset, skippedChars + offset), new type(position - offset, previousMonthLength - offset)];
			}
		}, {
			key: "containsBlackPoint",
			value: function init(level, zoom, a, be) {
				var p = this.image;
				if (be) {
					var i = level;
					for (; i <= zoom; i++) {
						if (p.get(i, a)) {
							return true;
						}
					}
				} else {
					var i = level;
					for (; i <= zoom; i++) {
						if (p.get(a, i)) {
							return true;
						}
					}
				}
				return false;
			}
		}]);
		return update;
	}();
	/** @type {number} */
	Element.INIT_SIZE = 10;
	/** @type {number} */
	Element.CORR = 1;
	var context = function () {
		/**
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3() {
			_classCallCheck2(this, TempusDominusBootstrap3);
		}
		_createClass2(TempusDominusBootstrap3, null, [{
			key: "checkAndNudgePoints",
			value: function getNextElementSnapPoint(obj, data) {
				var max = obj.getWidth();
				var selTimeVal = obj.getHeight();
				/** @type {boolean} */
				var closed = true;
				/** @type {number} */
				var index = 0;
				for (; index < data.length && closed; index = index + 2) {
					/** @type {number} */
					var n = Math.floor(data[index]);
					/** @type {number} */
					var tmpTimeVal = Math.floor(data[index + 1]);
					if (n < -1 || n > max || tmpTimeVal < -1 || tmpTimeVal > selTimeVal) {
						throw new TypeError;
					}
					/** @type {boolean} */
					closed = false;
					if (-1 === n) {
						/** @type {number} */
						data[index] = 0;
						/** @type {boolean} */
						closed = true;
					} else {
						if (n === max) {
							/** @type {number} */
							data[index] = max - 1;
							/** @type {boolean} */
							closed = true;
						}
					}
					if (-1 === tmpTimeVal) {
						/** @type {number} */
						data[index + 1] = 0;
						/** @type {boolean} */
						closed = true;
					} else {
						if (tmpTimeVal === selTimeVal) {
							/** @type {number} */
							data[index + 1] = selTimeVal - 1;
							/** @type {boolean} */
							closed = true;
						}
					}
				}
				/** @type {boolean} */
				closed = true;
				/** @type {number} */
				var i = data.length - 2;
				for (; i >= 0 && closed; i = i - 2) {
					/** @type {number} */
					var n = Math.floor(data[i]);
					/** @type {number} */
					var tmpTimeVal = Math.floor(data[i + 1]);
					if (n < -1 || n > max || tmpTimeVal < -1 || tmpTimeVal > selTimeVal) {
						throw new TypeError;
					}
					/** @type {boolean} */
					closed = false;
					if (-1 === n) {
						/** @type {number} */
						data[i] = 0;
						/** @type {boolean} */
						closed = true;
					} else {
						if (n === max) {
							/** @type {number} */
							data[i] = max - 1;
							/** @type {boolean} */
							closed = true;
						}
					}
					if (-1 === tmpTimeVal) {
						/** @type {number} */
						data[i + 1] = 0;
						/** @type {boolean} */
						closed = true;
					} else {
						if (tmpTimeVal === selTimeVal) {
							/** @type {number} */
							data[i + 1] = selTimeVal - 1;
							/** @type {boolean} */
							closed = true;
						}
					}
				}
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var PerspectiveTransform = function () {
		/**
		 * @param {?} a11
		 * @param {?} a21
		 * @param {boolean} a31
		 * @param {?} a12
		 * @param {?} a22
		 * @param {boolean} a32
		 * @param {?} a13
		 * @param {?} a23
		 * @param {boolean} a33
		 * @return {undefined}
		 */
		function PerspectiveTransform(a11, a21, a31, a12, a22, a32, a13, a23, a33) {
			_classCallCheck2(this, PerspectiveTransform);
			this.a11 = a11;
			this.a21 = a21;
			/** @type {boolean} */
			this.a31 = a31;
			this.a12 = a12;
			this.a22 = a22;
			/** @type {boolean} */
			this.a32 = a32;
			this.a13 = a13;
			this.a23 = a23;
			/** @type {boolean} */
			this.a33 = a33;
		}
		_createClass2(PerspectiveTransform, [{
			key: "transformPoints",
			value: function PerspectiveTransform(positions) {
				var n = positions.length;
				var a11 = this.a11;
				var a12 = this.a12;
				var a13 = this.a13;
				var a21 = this.a21;
				var a22 = this.a22;
				var a23 = this.a23;
				var a31 = this.a31;
				var pad = this.a32;
				var GB = this.a33;
				/** @type {number} */
				var j = 0;
				for (; j < n; j = j + 2) {
					var x = positions[j];
					var y = positions[j + 1];
					var size = a13 * x + a23 * y + GB;
					/** @type {number} */
					positions[j] = (a11 * x + a21 * y + a31) / size;
					/** @type {number} */
					positions[j + 1] = (a12 * x + a22 * y + pad) / size;
				}
			}
		}, {
			key: "transformPointsWithValues",
			value: function PerspectiveTransform(symbol, points) {
				var a11 = this.a11;
				var a12 = this.a12;
				var a13 = this.a13;
				var a21 = this.a21;
				var a22 = this.a22;
				var a23 = this.a23;
				var a31 = this.a31;
				var a32 = this.a32;
				var a33 = this.a33;
				var length = symbol.length;
				/** @type {number} */
				var i = 0;
				for (; i < length; i++) {
					var x = symbol[i];
					var y = points[i];
					var denominator = a13 * x + a23 * y + a33;
					/** @type {number} */
					symbol[i] = (a11 * x + a21 * y + a31) / denominator;
					/** @type {number} */
					points[i] = (a12 * x + a22 * y + a32) / denominator;
				}
			}
		}, {
			key: "buildAdjoint",
			value: function PerspectiveTransform() {
				return new PerspectiveTransform(this.a22 * this.a33 - this.a23 * this.a32, this.a23 * this.a31 - this.a21 * this.a33, this.a21 * this.a32 - this.a22 * this.a31, this.a13 * this.a32 - this.a12 * this.a33, this.a11 * this.a33 - this.a13 * this.a31, this.a12 * this.a31 - this.a11 * this.a32, this.a12 * this.a23 - this.a13 * this.a22, this.a13 * this.a21 - this.a11 * this.a23, this.a11 * this.a22 - this.a12 * this.a21);
			}
		}, {
			key: "times",
			value: function PerspectiveTransform(other) {
				return new PerspectiveTransform(this.a11 * other.a11 + this.a21 * other.a12 + this.a31 * other.a13, this.a11 * other.a21 + this.a21 * other.a22 + this.a31 * other.a23, this.a11 * other.a31 + this.a21 * other.a32 + this.a31 * other.a33, this.a12 * other.a11 + this.a22 * other.a12 + this.a32 * other.a13, this.a12 * other.a21 + this.a22 * other.a22 + this.a32 * other.a23, this.a12 * other.a31 + this.a22 * other.a32 + this.a32 * other.a33, this.a13 * other.a11 + this.a23 * other.a12 + this.a33 *
					other.a13, this.a13 * other.a21 + this.a23 * other.a22 + this.a33 * other.a23, this.a13 * other.a31 + this.a23 * other.a32 + this.a33 * other.a33);
			}
		}], [{
			key: "quadrilateralToQuadrilateral",
			value: function quadrilateralToQuadrilateral(x0, y0, x1, y1, x2, y2, x3, y3, x0p, y0p, x1p, y1p, x2p, y2p, x3p, y3p) {
				var qToS = PerspectiveTransform.quadrilateralToSquare(x0, y0, x1, y1, x2, y2, x3, y3);
				return PerspectiveTransform.squareToQuadrilateral(x0p, y0p, x1p, y1p, x2p, y2p, x3p, y3p).times(qToS);
			}
		}, {
			key: "squareToQuadrilateral",
			value: function squareToQuadrilateral(x0, y0, x1, y1, x2, y2, x3, y3) {
				/** @type {number} */
				var dx3 = x0 - x1 + x2 - x3;
				/** @type {number} */
				var dy3 = y0 - y1 + y2 - y3;
				if (0 === dx3 && 0 === dy3) {
					return new PerspectiveTransform(x1 - x0, x2 - x1, x0, y1 - y0, y2 - y1, y0, 0, 0, 1);
				}
				{
					/** @type {number} */
					var dx1 = x1 - x2;
					/** @type {number} */
					var dx2 = x3 - x2;
					/** @type {number} */
					var dy1 = y1 - y2;
					/** @type {number} */
					var dy2 = y3 - y2;
					/** @type {number} */
					var denominator = dx1 * dy2 - dx2 * dy1;
					/** @type {number} */
					var a13 = (dx3 * dy2 - dx2 * dy3) / denominator;
					/** @type {number} */
					var a23 = (dx1 * dy3 - dx3 * dy1) / denominator;
					return new PerspectiveTransform(x1 - x0 + a13 * x1, x3 - x0 + a23 * x3, x0, y1 - y0 + a13 * y1, y3 - y0 + a23 * y3, y0, a13, a23, 1);
				}
			}
		}, {
			key: "quadrilateralToSquare",
			value: function quadrilateralToSquare(x0, y0, x1, y1, x2, y2, x3, y3) {
				return PerspectiveTransform.squareToQuadrilateral(x0, y0, x1, y1, x2, y2, x3, y3).buildAdjoint();
			}
		}]);
		return PerspectiveTransform;
	}();
	var variablesInScope = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @return {?}
		 */
		function CacheLink() {
			_classCallCheck2(this, CacheLink);
			return _possibleConstructorReturn(this, (CacheLink.__proto__ || Object.getPrototypeOf(CacheLink)).apply(this, arguments));
		}
		_inherits(CacheLink, _WebInspector$GeneralTreeElement);
		_createClass2(CacheLink, [{
			key: "sampleGrid",
			value: function sampleGridx(e, image, dimension, p1ToX, p1ToY, p2ToX, p2ToY, p3ToX, p3ToY, p4ToX, p4ToY, p1FromX, p1FromY, p2FromX, p2FromY, p3FromX, p3FromY, p4FromX, p4FromY) {
				var transform = PerspectiveTransform.quadrilateralToQuadrilateral(p1ToX, p1ToY, p2ToX, p2ToY, p3ToX, p3ToY, p4ToX, p4ToY, p1FromX, p1FromY, p2FromX, p2FromY, p3FromX, p3FromY, p4FromX, p4FromY);
				return this.sampleGridWithTransform(e, image, dimension, transform);
			}
		}, {
			key: "sampleGridWithTransform",
			value: function exports(image, data, size, shape) {
				if (data <= 0 || size <= 0) {
					throw new TypeError;
				}
				var r = new Image(data, size);
				/** @type {!Float32Array} */
				var points = new Float32Array(2 * data);
				/** @type {number} */
				var i = 0;
				for (; i < size; i++) {
					/** @type {number} */
					var len2 = points.length;
					/** @type {number} */
					var pt = i + .5;
					/** @type {number} */
					var j = 0;
					for (; j < len2; j = j + 2) {
						/** @type {number} */
						points[j] = j / 2 + .5;
						/** @type {number} */
						points[j + 1] = pt;
					}
					shape.transformPoints(points);
					context.checkAndNudgePoints(image, points);
					try {
						/** @type {number} */
						var x = 0;
						for (; x < len2; x = x + 2) {
							if (image.get(Math.floor(points[x]), Math.floor(points[x + 1]))) {
								r.set(x / 2, i);
							}
						}
					} catch (t) {
						throw new TypeError;
					}
				}
				return r;
			}
		}]);
		return CacheLink;
	}(context);
	var ServiceManager = function () {
		/**
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3() {
			_classCallCheck2(this, TempusDominusBootstrap3);
		}
		_createClass2(TempusDominusBootstrap3, null, [{
			key: "setGridSampler",
			value: function setGridSampler(newGridSampler) {
				TempusDominusBootstrap3.gridSampler = newGridSampler;
			}
		}, {
			key: "getInstance",
			value: function getInstance() {
				return TempusDominusBootstrap3.gridSampler;
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	ServiceManager.gridSampler = new variablesInScope;
	var Coordinate = function () {
		/**
		 * @param {number} options
		 * @param {number} element
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3(options, element) {
			_classCallCheck2(this, TempusDominusBootstrap3);
			/** @type {number} */
			this.x = options;
			/** @type {number} */
			this.y = element;
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "toResultPoint",
			value: function label_refreshPosition() {
				return new type(this.getX(), this.getY());
			}
		}, {
			key: "getX",
			value: function xAcc() {
				return this.x;
			}
		}, {
			key: "getY",
			value: function DEFAULT_TEXT() {
				return this.y;
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var FindNodeRPC = function () {
		/**
		 * @param {number} url
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3(url) {
			_classCallCheck2(this, TempusDominusBootstrap3);
			/** @type {!Int32Array} */
			this.EXPECTED_CORNER_BITS = new Int32Array([3808, 476, 2107, 1799]);
			/** @type {number} */
			this.image = url;
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "detect",
			value: function detect() {
				return this.detectMirror(false);
			}
		}, {
			key: "detectMirror",
			value: function route(numberOfPosts) {
				var videoTemplate = this.getMatrixCenter();
				var a = this.getBullsEyeCorners(videoTemplate);
				if (numberOfPosts) {
					var curKey = a[0];
					a[0] = a[2];
					a[2] = curKey;
				}
				this.extractParameters(a);
				var bits = this.sampleGrid(this.image, a[this.shift % 4], a[(this.shift + 1) % 4], a[(this.shift + 2) % 4], a[(this.shift + 3) % 4]);
				var resolver = this.getMatrixCornerPoints(a);
				return new Router(bits, resolver, this.compact, this.nbDataBlocks, this.nbLayers);
			}
		}, {
			key: "extractParameters",
			value: function isValid(allowedDuration) {
				if (!(this.isValidPoint(allowedDuration[0]) && this.isValidPoint(allowedDuration[1]) && this.isValidPoint(allowedDuration[2]) && this.isValidPoint(allowedDuration[3]))) {
					throw new TypeError;
				}
				/** @type {number} */
				var e = 2 * this.nbCenterLayers;
				/** @type {!Int32Array} */
				var values = new Int32Array([this.sampleLine(allowedDuration[0], allowedDuration[1], e), this.sampleLine(allowedDuration[1], allowedDuration[2], e), this.sampleLine(allowedDuration[2], allowedDuration[3], e), this.sampleLine(allowedDuration[3], allowedDuration[0], e)]);
				this.shift = this.getRotation(values, e);
				/** @type {number} */
				var value = 0;
				/** @type {number} */
				var _t49 = 0;
				for (; _t49 < 4; _t49++) {
					/** @type {number} */
					var notes_mac = values[(this.shift + _t49) % 4];
					if (this.compact) {
						/** @type {number} */
						value = value << 7;
						/** @type {number} */
						value = value + (notes_mac >> 1 & 127);
					} else {
						/** @type {number} */
						value = value << 10;
						/** @type {number} */
						value = value + ((notes_mac >> 2 & 992) + (notes_mac >> 1 & 31));
					}
				}
				var oldCondition = this.getCorrectedParameterData(value, this.compact);
				if (this.compact) {
					/** @type {number} */
					this.nbLayers = 1 + (oldCondition >> 6);
					/** @type {number} */
					this.nbDataBlocks = 1 + (63 & oldCondition);
				} else {
					/** @type {number} */
					this.nbLayers = 1 + (oldCondition >> 11);
					/** @type {number} */
					this.nbDataBlocks = 1 + (2047 & oldCondition);
				}
			}
		}, {
			key: "getRotation",
			value: function error(value, count) {
				/** @type {number} */
				var XorKey = 0;
				value.forEach(function (val, n, i) {
					/** @type {number} */
					XorKey = (XorKey << 3) + ((val >> count - 2 << 1) + (1 & val));
				});
				/** @type {number} */
				XorKey = ((1 & XorKey) << 11) + (XorKey >> 1);
				/** @type {number} */
				var CurrentElement = 0;
				for (; CurrentElement < 4; CurrentElement++) {
					if (scope.bitCount(XorKey ^ this.EXPECTED_CORNER_BITS[CurrentElement]) <= 2) {
						return CurrentElement;
					}
				}
				throw new TypeError;
			}
		}, {
			key: "getCorrectedParameterData",
			value: function benchmark(mask, fn) {
				var len = void 0;
				var j = void 0;
				if (fn) {
					/** @type {number} */
					len = 7;
					/** @type {number} */
					j = 2;
				} else {
					/** @type {number} */
					len = 10;
					/** @type {number} */
					j = 4;
				}
				/** @type {number} */
				var k = len - j;
				/** @type {!Int32Array} */
				var buffer = new Int32Array(len);
				/** @type {number} */
				var offset = len - 1;
				for (; offset >= 0; --offset) {
					/** @type {number} */
					buffer[offset] = 15 & mask;
					/** @type {number} */
					mask = mask >> 4;
				}
				try {
					(new Uint16Array(r.AZTEC_PARAM)).decode(buffer, k);
				} catch (t) {
					throw new TypeError;
				}
				/** @type {number} */
				var n = 0;
				/** @type {number} */
				var i = 0;
				for (; i < j; i++) {
					/** @type {number} */
					n = (n << 4) + buffer[i];
				}
				return n;
			}
		}, {
			key: "getBullsEyeCorners",
			value: function start(index) {
				/** @type {!Object} */
				var sPlayer = index;
				/** @type {!Object} */
				var greenCircle = index;
				/** @type {!Object} */
				var n = index;
				/** @type {!Object} */
				var $Wireworld_StatePosition = index;
				/** @type {boolean} */
				var s = true;
				/** @type {number} */
				this.nbCenterLayers = 1;
				for (; this.nbCenterLayers < 9; this.nbCenterLayers++) {
					var ch__3378 = this.getFirstDifferent(sPlayer, s, 1, -1);
					var s__3381 = this.getFirstDifferent(greenCircle, s, 1, 1);
					var p = this.getFirstDifferent(n, s, -1, 1);
					var ch__3437 = this.getFirstDifferent($Wireworld_StatePosition, s, -1, -1);
					if (this.nbCenterLayers > 2) {
						/** @type {number} */
						var _r39 = this.distancePoint(ch__3437, ch__3378) * this.nbCenterLayers / (this.distancePoint($Wireworld_StatePosition, sPlayer) * (this.nbCenterLayers + 2));
						if (_r39 < .75 || _r39 > 1.25 || !this.isWhiteOrBlackRectangle(ch__3378, s__3381, p, ch__3437)) {
							break;
						}
					}
					sPlayer = ch__3378;
					greenCircle = s__3381;
					n = p;
					$Wireworld_StatePosition = ch__3437;
					/** @type {boolean} */
					s = !s;
				}
				if (5 !== this.nbCenterLayers && 7 !== this.nbCenterLayers) {
					throw new TypeError;
				}
				/** @type {boolean} */
				this.compact = 5 === this.nbCenterLayers;
				var stat = new type(sPlayer.getX() + .5, sPlayer.getY() - .5);
				var rel = new type(greenCircle.getX() + .5, greenCircle.getY() + .5);
				var pixelBuffer = new type(n.getX() - .5, n.getY() + .5);
				var anim = new type($Wireworld_StatePosition.getX() - .5, $Wireworld_StatePosition.getY() - .5);
				return this.expandSquare([stat, rel, pixelBuffer, anim], 2 * this.nbCenterLayers - 3, 2 * this.nbCenterLayers);
			}
		}, {
			key: "getMatrixCenter",
			value: function update() {
				var StatePosition = void 0;
				var $Wireworld_StatePosition = void 0;
				var stepmarker_end = void 0;
				var n = void 0;
				try {
					var inExistingList = (new Element(this.image)).detect();
					StatePosition = inExistingList[0];
					$Wireworld_StatePosition = inExistingList[1];
					stepmarker_end = inExistingList[2];
					n = inExistingList[3];
				} catch (LIMIT) {
					/** @type {number} */
					var i = this.image.getWidth() / 2;
					/** @type {number} */
					var j = this.image.getHeight() / 2;
					StatePosition = this.getFirstDifferent(new Coordinate(i + 7, j - 7), false, 1, -1).toResultPoint();
					$Wireworld_StatePosition = this.getFirstDifferent(new Coordinate(i + 7, j + 7), false, 1, 1).toResultPoint();
					stepmarker_end = this.getFirstDifferent(new Coordinate(i - 7, j + 7), false, -1, 1).toResultPoint();
					n = this.getFirstDifferent(new Coordinate(i - 7, j - 7), false, -1, -1).toResultPoint();
				}
				var i = p.round((StatePosition.getX() + n.getX() + $Wireworld_StatePosition.getX() + stepmarker_end.getX()) / 4);
				var j = p.round((StatePosition.getY() + n.getY() + $Wireworld_StatePosition.getY() + stepmarker_end.getY()) / 4);
				try {
					var inExistingList = (new Element(this.image, 15, i, j)).detect();
					StatePosition = inExistingList[0];
					$Wireworld_StatePosition = inExistingList[1];
					stepmarker_end = inExistingList[2];
					n = inExistingList[3];
				} catch (o) {
					StatePosition = this.getFirstDifferent(new Coordinate(i + 7, j - 7), false, 1, -1).toResultPoint();
					$Wireworld_StatePosition = this.getFirstDifferent(new Coordinate(i + 7, j + 7), false, 1, 1).toResultPoint();
					stepmarker_end = this.getFirstDifferent(new Coordinate(i - 7, j + 7), false, -1, 1).toResultPoint();
					n = this.getFirstDifferent(new Coordinate(i - 7, j - 7), false, -1, -1).toResultPoint();
				}
				return i = p.round((StatePosition.getX() + n.getX() + $Wireworld_StatePosition.getX() + stepmarker_end.getX()) / 4), j = p.round((StatePosition.getY() + n.getY() + $Wireworld_StatePosition.getY() + stepmarker_end.getY()) / 4), new Coordinate(i, j);
			}
		}, {
			key: "getMatrixCornerPoints",
			value: function getContentWidth(context) {
				return this.expandSquare(context, 2 * this.nbCenterLayers, this.getDimension());
			}
		}, {
			key: "sampleGrid",
			value: function update(formData, val, p, n, acc) {
				var self = ServiceManager.getInstance();
				var width = this.getDimension();
				/** @type {number} */
				var siteId = width / 2 - this.nbCenterLayers;
				var verified = width / 2 + this.nbCenterLayers;
				return self.sampleGrid(formData, width, width, siteId, siteId, verified, siteId, verified, verified, siteId, verified, val.getX(), val.getY(), p.getX(), p.getY(), n.getX(), n.getY(), acc.getX(), acc.getY());
			}
		}, {
			key: "sampleLine",
			value: function update(n, p, m) {
				/** @type {number} */
				var line = 0;
				var z = this.distanceResultPoint(n, p);
				/** @type {number} */
				var x = z / m;
				var y = n.getX();
				var cy = n.getY();
				/** @type {number} */
				var spacing = x * (p.getX() - n.getX()) / z;
				/** @type {number} */
				var sign = x * (p.getY() - n.getY()) / z;
				/** @type {number} */
				var k = 0;
				for (; k < m; k++) {
					if (this.image.get(p.round(y + k * spacing), p.round(cy + k * sign))) {
						/** @type {number} */
						line = line | 1 << m - k - 1;
					}
				}
				return line;
			}
		}, {
			key: "isWhiteOrBlackRectangle",
			value: function update(feature, p, n, c) {
				feature = new Coordinate(feature.getX() - 3, feature.getY() + 3);
				p = new Coordinate(p.getX() - 3, p.getY() - 3);
				n = new Coordinate(n.getX() + 3, n.getY() - 3);
				c = new Coordinate(c.getX() + 3, c.getY() + 3);
				var id = this.getColor(c, feature);
				if (0 === id) {
					return false;
				}
				var str = this.getColor(feature, p);
				return str === id && (str = this.getColor(p, n)) === id && (str = this.getColor(n, c)) === id;
			}
		}, {
			key: "getColor",
			value: function draw(map, layer) {
				var r = this.distancePoint(map, layer);
				/** @type {number} */
				var value = (layer.getX() - map.getX()) / r;
				/** @type {number} */
				var s = (layer.getY() - map.getY()) / r;
				/** @type {number} */
				var r2 = 0;
				var size = map.getX();
				var i = map.getY();
				var l = this.image.get(map.getX(), map.getY());
				/** @type {number} */
				var clientHeight = Math.ceil(r);
				/** @type {number} */
				var targetOffsetHeight = 0;
				for (; targetOffsetHeight < clientHeight; targetOffsetHeight++) {
					size = size + value;
					i = i + s;
					if (this.image.get(p.round(size), p.round(i)) !== l) {
						r2++;
					}
				}
				/** @type {number} */
				var tmp = r2 / r;
				return tmp > .1 && tmp < .9 ? 0 : tmp <= .1 === l ? 1 : -1;
			}
		}, {
			key: "getFirstDifferent",
			value: function update(acc, value, diff, i) {
				var val = acc.getX() + diff;
				var y = acc.getY() + i;
				for (; this.isValid(val, y) && this.image.get(val, y) === value;) {
					val = val + diff;
					y = y + i;
				}
				/** @type {number} */
				val = val - diff;
				/** @type {number} */
				y = y - i;
				for (; this.isValid(val, y) && this.image.get(val, y) === value;) {
					val = val + diff;
				}
				/** @type {number} */
				val = val - diff;
				for (; this.isValid(val, y) && this.image.get(val, y) === value;) {
					y = y + i;
				}
				return new Coordinate(val, y = y - i);
			}
		}, {
			key: "expandSquare",
			value: function KDBush(points, getX, getY) {
				/** @type {number} */
				var i = getY / (2 * getX);
				/** @type {number} */
				var margin = points[0].getX() - points[2].getX();
				/** @type {number} */
				var gridLineHeight = points[0].getY() - points[2].getY();
				/** @type {number} */
				var height = (points[0].getX() + points[2].getX()) / 2;
				/** @type {number} */
				var sequenceLineY = (points[0].getY() + points[2].getY()) / 2;
				var anim = new type(height + i * margin, sequenceLineY + i * gridLineHeight);
				var rel = new type(height - i * margin, sequenceLineY - i * gridLineHeight);
				return margin = points[1].getX() - points[3].getX(), gridLineHeight = points[1].getY() - points[3].getY(), height = (points[1].getX() + points[3].getX()) / 2, sequenceLineY = (points[1].getY() + points[3].getY()) / 2, [anim, new type(height + i * margin, sequenceLineY + i * gridLineHeight), rel, new type(height - i * margin, sequenceLineY - i * gridLineHeight)];
			}
		}, {
			key: "isValid",
			value: function LComboBox(size, color) {
				return size >= 0 && size < this.image.getWidth() && color > 0 && color < this.image.getHeight();
			}
		}, {
			key: "isValidPoint",
			value: function update(feature) {
				var year = p.round(feature.getX());
				var end = p.round(feature.getY());
				return this.isValid(year, end);
			}
		}, {
			key: "distancePoint",
			value: function enable(t, f) {
				return p.distance(t.getX(), t.getY(), f.getX(), f.getY());
			}
		}, {
			key: "distanceResultPoint",
			value: function enable(t, f) {
				return p.distance(t.getX(), t.getY(), f.getX(), f.getY());
			}
		}, {
			key: "getDimension",
			value: function HttpPouch() {
				return this.compact ? 4 * this.nbLayers + 11 : this.nbLayers <= 4 ? 4 * this.nbLayers + 15 : 4 * this.nbLayers + 2 * (scope.truncDivision(this.nbLayers - 4, 8) + 1) + 15;
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var Rule = function () {
		/**
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3() {
			_classCallCheck2(this, TempusDominusBootstrap3);
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "decode",
			value: function map(max_out) {
				var e = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
				/** @type {null} */
				var r = null;
				var rpc = new FindNodeRPC(max_out.getBlackMatrix());
				/** @type {null} */
				var _maskLayer = null;
				/** @type {null} */
				var m = null;
				try {
					var inner = rpc.detectMirror(false);
					_maskLayer = inner.getPoints();
					this.reportFoundResultPoints(e, _maskLayer);
					m = (new data).decode(inner);
				} catch (G__20648) {
					r = G__20648;
				}
				if (null == m) {
					try {
						var inner = rpc.detectMirror(true);
						_maskLayer = inner.getPoints();
						this.reportFoundResultPoints(e, _maskLayer);
						m = (new data).decode(inner);
					} catch (t) {
						if (null != r) {
							throw r;
						}
						throw t;
					}
				}
				var self = new result(m.getText(), m.getRawBytes(), m.getNumBits(), _maskLayer, change.AZTEC, System.currentTimeMillis());
				var unexpandedFeatureDirectoryPaths = m.getByteSegments();
				if (null != unexpandedFeatureDirectoryPaths) {
					self.putMetadata(value.BYTE_SEGMENTS, unexpandedFeatureDirectoryPaths);
				}
				var expectedPackagePath = m.getECLevel();
				return null != expectedPackagePath && self.putMetadata(value.ERROR_CORRECTION_LEVEL, expectedPackagePath), self;
			}
		}, {
			key: "reportFoundResultPoints",
			value: function channelRiver(start, end) {
				if (null != start) {
					var epl = start.get(node.NEED_RESULT_POINT_CALLBACK);
					if (null != epl) {
						end.forEach(function (point, canCreateDiscussions, n) {
							epl.foundPossibleResultPoint(point);
						});
					}
				}
			}
		}, {
			key: "reset",
			value: function reset() {
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var path = function () {
		/**
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3() {
			_classCallCheck2(this, TempusDominusBootstrap3);
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "decode",
			value: function update(ajaxResponse, start) {
				try {
					return this.doDecode(ajaxResponse, start);
				} catch (r) {
					if (start && true === start.get(node.TRY_HARDER) && ajaxResponse.isRotateSupported()) {
						var selectedObj = ajaxResponse.rotateCounterClockwise();
						var result = this.doDecode(selectedObj, start);
						var $iconElement = result.getResultMetadata();
						/** @type {number} */
						var paramText = 270;
						if (null !== $iconElement && true === $iconElement.get(value.ORIENTATION)) {
							/** @type {number} */
							paramText = paramText + $iconElement.get(value.ORIENTATION) % 360;
						}
						result.putMetadata(value.ORIENTATION, paramText);
						var points = result.getResultPoints();
						if (null !== points) {
							var _t57 = selectedObj.getHeight();
							/** @type {number} */
							var i = 0;
							for (; i < points.length; i++) {
								points[i] = new type(_t57 - points[i].getY() - 1, points[i].getX());
							}
						}
						return result;
					}
					throw new TypeError;
				}
			}
		}, {
			key: "reset",
			value: function reset() {
			}
		}, {
			key: "doDecode",
			value: function update(pro, obj) {
				var bytesOrBits = pro.getWidth();
				var width = pro.getHeight();
				var options = new BitArray(bytesOrBits);
				var floating = obj && true === obj.get(node.TRY_HARDER);
				/** @type {number} */
				var PLAY_BUFFER_DELAY = Math.max(1, width >> (floating ? 8 : 5));
				var sizeProperty = void 0;
				sizeProperty = floating ? width : 15;
				/** @type {number} */
				var limLonMin = Math.trunc(width / 2);
				/** @type {number} */
				var _s20 = 0;
				for (; _s20 < sizeProperty; _s20++) {
					/** @type {number} */
					var PAGE_HIDDEN_BUFFER_RATIO = Math.trunc((_s20 + 1) / 2);
					/** @type {number} */
					var x = limLonMin + PLAY_BUFFER_DELAY * (0 == (1 & _s20) ? PAGE_HIDDEN_BUFFER_RATIO : -PAGE_HIDDEN_BUFFER_RATIO);
					if (x < 0 || x >= width) {
						break;
					}
					try {
						options = pro.getBlackRow(x, options);
					} catch (t) {
						continue;
					}
					/** @type {number} */
					var _t58 = 0;
					for (; _t58 < 2; _t58++) {
						if (1 === _t58 && (options.reverse(), obj && true === obj.get(node.NEED_RESULT_POINT_CALLBACK))) {
							(function () {
								/** @type {!Map} */
								var self = new Map;
								obj.forEach(function (timer, id) {
									return self.set(id, timer);
								});
								self.delete(node.NEED_RESULT_POINT_CALLBACK);
								/** @type {!Map} */
								obj = self;
							})();
						}
						try {
							var result = this.decodeRow(x, options, obj);
							if (1 === _t58) {
								result.putMetadata(value.ORIENTATION, 180);
								var points = result.getResultPoints();
								if (null !== points) {
									points[0] = new type(bytesOrBits - points[0].getX() - 1, points[0].getY());
									points[1] = new type(bytesOrBits - points[1].getX() - 1, points[1].getY());
								}
							}
							return result;
						} catch (t) {
						}
					}
				}
				throw new TypeError;
			}
		}], [{
			key: "recordPattern",
			value: function init(layer, action, points) {
				var i = points.length;
				/** @type {number} */
				var ii = 0;
				for (; ii < i; ii++) {
					/** @type {number} */
					points[ii] = 0;
				}
				var padBottom = layer.getSize();
				if (action >= padBottom) {
					throw new TypeError;
				}
				/** @type {boolean} */
				var s = !layer.get(action);
				/** @type {number} */
				var j = 0;
				var propName = action;
				for (; propName < padBottom;) {
					if (layer.get(propName) !== s) {
						points[j]++;
					} else {
						if (++j === i) {
							break;
						}
						/** @type {number} */
						points[j] = 1;
						/** @type {boolean} */
						s = !s;
					}
					propName++;
				}
				if (j !== i && (j !== i - 1 || propName !== padBottom)) {
					throw new TypeError;
				}
			}
		}, {
			key: "recordPatternInReverse",
			value: function renderWaveForm(text, index, output) {
				var variableCount = output.length;
				var languageBlock = text.get(index);
				for (; index > 0 && variableCount >= 0;) {
					if (text.get(--index) !== languageBlock) {
						variableCount--;
						/** @type {boolean} */
						languageBlock = !languageBlock;
					}
				}
				if (variableCount >= 0) {
					throw new TypeError;
				}
				TempusDominusBootstrap3.recordPattern(text, index + 1, output);
			}
		}, {
			key: "patternMatchVariance",
			value: function processValue(s, b, value) {
				var length = s.length;
				/** @type {number} */
				var x = 0;
				/** @type {number} */
				var w = 0;
				/** @type {number} */
				var j = 0;
				for (; j < length; j++) {
					x = x + s[j];
					w = w + b[j];
				}
				if (x < w) {
					return Number.POSITIVE_INFINITY;
				}
				/** @type {number} */
				var r = x / w;
				/** @type {number} */
				value = value * r;
				/** @type {number} */
				var input = 0;
				/** @type {number} */
				var i = 0;
				for (; i < length; i++) {
					var x0 = s[i];
					/** @type {number} */
					var x1 = b[i] * r;
					/** @type {number} */
					var c = x0 > x1 ? x0 - x1 : x1 - x0;
					if (c > value) {
						return Number.POSITIVE_INFINITY;
					}
					/** @type {number} */
					input = input + c;
				}
				return input / x;
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var module = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @return {?}
		 */
		function self() {
			_classCallCheck2(this, self);
			return _possibleConstructorReturn(this, (self.__proto__ || Object.getPrototypeOf(self)).apply(this, arguments));
		}
		_inherits(self, _WebInspector$GeneralTreeElement);
		_createClass2(self, [{
			key: "decodeRow",
			value: function init(name, params, handler) {
				var computeHandler = handler && true === handler.get(node.ASSUME_GS1);
				var args = self.findStartPattern(params);
				var date = args[2];
				/** @type {number} */
				var off = 0;
				/** @type {!Uint8Array} */
				var data = new Uint8Array(20);
				var n = void 0;
				switch (data[off++] = date, date) {
					case self.CODE_START_A:
						n = self.CODE_CODE_A;
						break;
					case self.CODE_START_B:
						n = self.CODE_CODE_B;
						break;
					case self.CODE_START_C:
						n = self.CODE_CODE_C;
						break;
					default:
						throw new Date;
				}
				/** @type {boolean} */
				var targetUrl = false;
				/** @type {boolean} */
				var currentRelations = false;
				/** @type {string} */
				var s = "";
				var start = args[0];
				var index = args[1];
				/** @type {!Int32Array} */
				var x = Int32Array.from([0, 0, 0, 0, 0, 0]);
				/** @type {number} */
				var c1 = 0;
				/** @type {number} */
				var c = 0;
				var b = date;
				/** @type {number} */
				var d = 0;
				/** @type {boolean} */
				var coloring = true;
				/** @type {boolean} */
				var canViewMyFiles = false;
				/** @type {boolean} */
				var canAccessMyFiles = false;
				for (; !targetUrl;) {
					/** @type {boolean} */
					var addedRelations = currentRelations;
					switch (currentRelations = false, c1 = c, c = self.decodeCode(params, x, index), data[off++] = c, c !== self.CODE_STOP && (coloring = true), c !== self.CODE_STOP && (b = b + ++d * c), start = index, index = index + x.reduce(function (buckets, name) {
						return buckets + name;
					}, 0), c) {
						case self.CODE_START_A:
						case self.CODE_START_B:
						case self.CODE_START_C:
							throw new Date;
					}
					switch (n) {
						case self.CODE_CODE_A:
							if (c < 64) {
								/** @type {string} */
								s = s + (canAccessMyFiles === canViewMyFiles ? String.fromCharCode(" ".charCodeAt(0) + c) : String.fromCharCode(" ".charCodeAt(0) + c + 128));
								/** @type {boolean} */
								canAccessMyFiles = false;
							} else {
								if (c < 96) {
									/** @type {string} */
									s = s + (canAccessMyFiles === canViewMyFiles ? String.fromCharCode(c - 64) : String.fromCharCode(c + 64));
									/** @type {boolean} */
									canAccessMyFiles = false;
								} else {
									switch (c !== self.CODE_STOP && (coloring = false), c) {
										case self.CODE_FNC_1:
											if (computeHandler) {
												if (0 === s.length) {
													/** @type {string} */
													s = s + "]C1";
												} else {
													/** @type {string} */
													s = s + String.fromCharCode(29);
												}
											}
											break;
										case self.CODE_FNC_2:
										case self.CODE_FNC_3:
											break;
										case self.CODE_FNC_4_A:
											if (!canViewMyFiles && canAccessMyFiles) {
												/** @type {boolean} */
												canViewMyFiles = true;
												/** @type {boolean} */
												canAccessMyFiles = false;
											} else {
												if (canViewMyFiles && canAccessMyFiles) {
													/** @type {boolean} */
													canViewMyFiles = false;
													/** @type {boolean} */
													canAccessMyFiles = false;
												} else {
													/** @type {boolean} */
													canAccessMyFiles = true;
												}
											}
											break;
										case self.CODE_SHIFT:
											/** @type {boolean} */
											currentRelations = true;
											n = self.CODE_CODE_B;
											break;
										case self.CODE_CODE_B:
											n = self.CODE_CODE_B;
											break;
										case self.CODE_CODE_C:
											n = self.CODE_CODE_C;
											break;
										case self.CODE_STOP:
											/** @type {boolean} */
											targetUrl = true;
									}
								}
							}
							break;
						case self.CODE_CODE_B:
							if (c < 96) {
								/** @type {string} */
								s = s + (canAccessMyFiles === canViewMyFiles ? String.fromCharCode(" ".charCodeAt(0) + c) : String.fromCharCode(" ".charCodeAt(0) + c + 128));
								/** @type {boolean} */
								canAccessMyFiles = false;
							} else {
								switch (c !== self.CODE_STOP && (coloring = false), c) {
									case self.CODE_FNC_1:
										if (computeHandler) {
											if (0 === s.length) {
												/** @type {string} */
												s = s + "]C1";
											} else {
												/** @type {string} */
												s = s + String.fromCharCode(29);
											}
										}
										break;
									case self.CODE_FNC_2:
									case self.CODE_FNC_3:
										break;
									case self.CODE_FNC_4_B:
										if (!canViewMyFiles && canAccessMyFiles) {
											/** @type {boolean} */
											canViewMyFiles = true;
											/** @type {boolean} */
											canAccessMyFiles = false;
										} else {
											if (canViewMyFiles && canAccessMyFiles) {
												/** @type {boolean} */
												canViewMyFiles = false;
												/** @type {boolean} */
												canAccessMyFiles = false;
											} else {
												/** @type {boolean} */
												canAccessMyFiles = true;
											}
										}
										break;
									case self.CODE_SHIFT:
										/** @type {boolean} */
										currentRelations = true;
										n = self.CODE_CODE_A;
										break;
									case self.CODE_CODE_A:
										n = self.CODE_CODE_A;
										break;
									case self.CODE_CODE_C:
										n = self.CODE_CODE_C;
										break;
									case self.CODE_STOP:
										/** @type {boolean} */
										targetUrl = true;
								}
							}
							break;
						case self.CODE_CODE_C:
							if (c < 100) {
								if (c < 10) {
									/** @type {string} */
									s = s + "0";
								}
								/** @type {string} */
								s = s + c;
							} else {
								switch (c !== self.CODE_STOP && (coloring = false), c) {
									case self.CODE_FNC_1:
										if (computeHandler) {
											if (0 === s.length) {
												/** @type {string} */
												s = s + "]C1";
											} else {
												/** @type {string} */
												s = s + String.fromCharCode(29);
											}
										}
										break;
									case self.CODE_CODE_A:
										n = self.CODE_CODE_A;
										break;
									case self.CODE_CODE_B:
										n = self.CODE_CODE_B;
										break;
									case self.CODE_STOP:
										/** @type {boolean} */
										targetUrl = true;
								}
							}
					}
					if (addedRelations) {
						n = n === self.CODE_CODE_A ? self.CODE_CODE_B : self.CODE_CODE_A;
					}
				}
				/** @type {number} */
				var i = index - start;
				if (index = params.getNextUnset(index), !params.isRange(index, Math.min(params.getSize(), index + (index - start) / 2), false)) {
					throw new TypeError;
				}
				if ((b = b - d * c1) % 103 !== c1) {
					throw new Rectangle;
				}
				/** @type {number} */
				var length = s.length;
				if (0 === length) {
					throw new TypeError;
				}
				if (length > 0 && coloring) {
					/** @type {string} */
					s = n === self.CODE_CODE_C ? s.substring(0, length - 2) : s.substring(0, length - 1);
				}
				/** @type {number} */
				var o = (args[1] + args[0]) / 2;
				var val = start + i / 2;
				/** @type {number} */
				var res = data.length;
				/** @type {!Uint8Array} */
				var response = new Uint8Array(res);
				/** @type {number} */
				var key = 0;
				for (; key < res; key++) {
					/** @type {number} */
					response[key] = data[key];
				}
				/** @type {!Array} */
				var B = [new type(o, name), new type(val, name)];
				return new result(s, response, 0, B, change.CODE_128, (new Date).getTime());
			}
		}], [{
			key: "findStartPattern",
			value: function range(array) {
				var oALen = array.getSize();
				var undefined = array.getNextSet(0);
				/** @type {number} */
				var cntr_map = 0;
				/** @type {!Int32Array} */
				var counters = Int32Array.from([0, 0, 0, 0, 0, 0]);
				var i = undefined;
				/** @type {boolean} */
				var CRLF = false;
				var len = undefined;
				for (; len < oALen; len++) {
					if (array.get(len) !== CRLF) {
						counters[cntr_map]++;
					} else {
						if (5 === cntr_map) {
							var clientHeight = self.MAX_AVG_VARIANCE;
							/** @type {number} */
							var first_li = -1;
							var id = self.CODE_START_A;
							for (; id <= self.CODE_START_C; id++) {
								var docHeight = path.patternMatchVariance(counters, self.CODE_PATTERNS[id], self.MAX_INDIVIDUAL_VARIANCE);
								if (docHeight < clientHeight) {
									clientHeight = docHeight;
									first_li = id;
								}
							}
							if (first_li >= 0 && array.isRange(Math.max(0, i - (len - i) / 2), i, false)) {
								return Int32Array.from([i, len, first_li]);
							}
							i = i + (counters[0] + counters[1]);
							/** @type {number} */
							(counters = counters.slice(2, counters.length - 1))[cntr_map - 1] = 0;
							/** @type {number} */
							counters[cntr_map] = 0;
							cntr_map--;
						} else {
							cntr_map++;
						}
						/** @type {number} */
						counters[cntr_map] = 1;
						/** @type {boolean} */
						CRLF = !CRLF;
					}
				}
				throw new TypeError;
			}
		}, {
			key: "decodeCode",
			value: function render(canvas, pie, index) {
				path.recordPattern(canvas, index, pie);
				var clientHeight = self.MAX_AVG_VARIANCE;
				/** @type {number} */
				var input = -1;
				/** @type {number} */
				var x = 0;
				for (; x < self.CODE_PATTERNS.length; x++) {
					var tempX1 = self.CODE_PATTERNS[x];
					var docHeight = this.patternMatchVariance(pie, tempX1, self.MAX_INDIVIDUAL_VARIANCE);
					if (docHeight < clientHeight) {
						clientHeight = docHeight;
						/** @type {number} */
						input = x;
					}
				}
				if (input >= 0) {
					return input;
				}
				throw new TypeError;
			}
		}]);
		return self;
	}(path);
	/** @type {!Array} */
	module.CODE_PATTERNS = [Int32Array.from([2, 1, 2, 2, 2, 2]), Int32Array.from([2, 2, 2, 1, 2, 2]), Int32Array.from([2, 2, 2, 2, 2, 1]), Int32Array.from([1, 2, 1, 2, 2, 3]), Int32Array.from([1, 2, 1, 3, 2, 2]), Int32Array.from([1, 3, 1, 2, 2, 2]), Int32Array.from([1, 2, 2, 2, 1, 3]), Int32Array.from([1, 2, 2, 3, 1, 2]), Int32Array.from([1, 3, 2, 2, 1, 2]), Int32Array.from([2, 2, 1, 2, 1, 3]), Int32Array.from([2, 2, 1, 3, 1, 2]), Int32Array.from([2, 3, 1, 2, 1, 2]), Int32Array.from([1, 1, 2, 2, 3,
		2]), Int32Array.from([1, 2, 2, 1, 3, 2]), Int32Array.from([1, 2, 2, 2, 3, 1]), Int32Array.from([1, 1, 3, 2, 2, 2]), Int32Array.from([1, 2, 3, 1, 2, 2]), Int32Array.from([1, 2, 3, 2, 2, 1]), Int32Array.from([2, 2, 3, 2, 1, 1]), Int32Array.from([2, 2, 1, 1, 3, 2]), Int32Array.from([2, 2, 1, 2, 3, 1]), Int32Array.from([2, 1, 3, 2, 1, 2]), Int32Array.from([2, 2, 3, 1, 1, 2]), Int32Array.from([3, 1, 2, 1, 3, 1]), Int32Array.from([3, 1, 1, 2, 2, 2]), Int32Array.from([3, 2, 1, 1, 2, 2]), Int32Array.from([3,
			2, 1, 2, 2, 1]), Int32Array.from([3, 1, 2, 2, 1, 2]), Int32Array.from([3, 2, 2, 1, 1, 2]), Int32Array.from([3, 2, 2, 2, 1, 1]), Int32Array.from([2, 1, 2, 1, 2, 3]), Int32Array.from([2, 1, 2, 3, 2, 1]), Int32Array.from([2, 3, 2, 1, 2, 1]), Int32Array.from([1, 1, 1, 3, 2, 3]), Int32Array.from([1, 3, 1, 1, 2, 3]), Int32Array.from([1, 3, 1, 3, 2, 1]), Int32Array.from([1, 1, 2, 3, 1, 3]), Int32Array.from([1, 3, 2, 1, 1, 3]), Int32Array.from([1, 3, 2, 3, 1, 1]), Int32Array.from([2, 1, 1, 3, 1, 3]), Int32Array.from([2,
				3, 1, 1, 1, 3]), Int32Array.from([2, 3, 1, 3, 1, 1]), Int32Array.from([1, 1, 2, 1, 3, 3]), Int32Array.from([1, 1, 2, 3, 3, 1]), Int32Array.from([1, 3, 2, 1, 3, 1]), Int32Array.from([1, 1, 3, 1, 2, 3]), Int32Array.from([1, 1, 3, 3, 2, 1]), Int32Array.from([1, 3, 3, 1, 2, 1]), Int32Array.from([3, 1, 3, 1, 2, 1]), Int32Array.from([2, 1, 1, 3, 3, 1]), Int32Array.from([2, 3, 1, 1, 3, 1]), Int32Array.from([2, 1, 3, 1, 1, 3]), Int32Array.from([2, 1, 3, 3, 1, 1]), Int32Array.from([2, 1, 3, 1, 3, 1]), Int32Array.from([3,
					1, 1, 1, 2, 3]), Int32Array.from([3, 1, 1, 3, 2, 1]), Int32Array.from([3, 3, 1, 1, 2, 1]), Int32Array.from([3, 1, 2, 1, 1, 3]), Int32Array.from([3, 1, 2, 3, 1, 1]), Int32Array.from([3, 3, 2, 1, 1, 1]), Int32Array.from([3, 1, 4, 1, 1, 1]), Int32Array.from([2, 2, 1, 4, 1, 1]), Int32Array.from([4, 3, 1, 1, 1, 1]), Int32Array.from([1, 1, 1, 2, 2, 4]), Int32Array.from([1, 1, 1, 4, 2, 2]), Int32Array.from([1, 2, 1, 1, 2, 4]), Int32Array.from([1, 2, 1, 4, 2, 1]), Int32Array.from([1, 4, 1, 1, 2, 2]), Int32Array.from([1,
						4, 1, 2, 2, 1]), Int32Array.from([1, 1, 2, 2, 1, 4]), Int32Array.from([1, 1, 2, 4, 1, 2]), Int32Array.from([1, 2, 2, 1, 1, 4]), Int32Array.from([1, 2, 2, 4, 1, 1]), Int32Array.from([1, 4, 2, 1, 1, 2]), Int32Array.from([1, 4, 2, 2, 1, 1]), Int32Array.from([2, 4, 1, 2, 1, 1]), Int32Array.from([2, 2, 1, 1, 1, 4]), Int32Array.from([4, 1, 3, 1, 1, 1]), Int32Array.from([2, 4, 1, 1, 1, 2]), Int32Array.from([1, 3, 4, 1, 1, 1]), Int32Array.from([1, 1, 1, 2, 4, 2]), Int32Array.from([1, 2, 1, 1, 4, 2]), Int32Array.from([1,
							2, 1, 2, 4, 1]), Int32Array.from([1, 1, 4, 2, 1, 2]), Int32Array.from([1, 2, 4, 1, 1, 2]), Int32Array.from([1, 2, 4, 2, 1, 1]), Int32Array.from([4, 1, 1, 2, 1, 2]), Int32Array.from([4, 2, 1, 1, 1, 2]), Int32Array.from([4, 2, 1, 2, 1, 1]), Int32Array.from([2, 1, 2, 1, 4, 1]), Int32Array.from([2, 1, 4, 1, 2, 1]), Int32Array.from([4, 1, 2, 1, 2, 1]), Int32Array.from([1, 1, 1, 1, 4, 3]), Int32Array.from([1, 1, 1, 3, 4, 1]), Int32Array.from([1, 3, 1, 1, 4, 1]), Int32Array.from([1, 1, 4, 1, 1, 3]), Int32Array.from([1,
								1, 4, 3, 1, 1]), Int32Array.from([4, 1, 1, 1, 1, 3]), Int32Array.from([4, 1, 1, 3, 1, 1]), Int32Array.from([1, 1, 3, 1, 4, 1]), Int32Array.from([1, 1, 4, 1, 3, 1]), Int32Array.from([3, 1, 1, 1, 4, 1]), Int32Array.from([4, 1, 1, 1, 3, 1]), Int32Array.from([2, 1, 1, 4, 1, 2]), Int32Array.from([2, 1, 1, 2, 1, 4]), Int32Array.from([2, 1, 1, 2, 3, 2]), Int32Array.from([2, 3, 3, 1, 1, 1, 2])];
	/** @type {number} */
	module.MAX_AVG_VARIANCE = .25;
	/** @type {number} */
	module.MAX_INDIVIDUAL_VARIANCE = .7;
	/** @type {number} */
	module.CODE_SHIFT = 98;
	/** @type {number} */
	module.CODE_CODE_C = 99;
	/** @type {number} */
	module.CODE_CODE_B = 100;
	/** @type {number} */
	module.CODE_CODE_A = 101;
	/** @type {number} */
	module.CODE_FNC_1 = 102;
	/** @type {number} */
	module.CODE_FNC_2 = 97;
	/** @type {number} */
	module.CODE_FNC_3 = 96;
	/** @type {number} */
	module.CODE_FNC_4_A = 101;
	/** @type {number} */
	module.CODE_FNC_4_B = 100;
	/** @type {number} */
	module.CODE_START_A = 103;
	/** @type {number} */
	module.CODE_START_B = 104;
	/** @type {number} */
	module.CODE_START_C = 105;
	/** @type {number} */
	module.CODE_STOP = 106;
	var Sprite = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @return {?}
		 */
		function App() {
			var edit_device = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
			var _this;
			var variableSrv = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
			_classCallCheck2(this, App);
			_this = _possibleConstructorReturn(this, (App.__proto__ || Object.getPrototypeOf(App)).call(this));
			_this;
			_this.usingCheckDigit = edit_device;
			_this.extendedMode = variableSrv;
			/** @type {string} */
			_this.decodeRowResult = "";
			/** @type {!Int32Array} */
			_this.counters = new Int32Array(9);
			return _this;
		}
		_inherits(App, _WebInspector$GeneralTreeElement);
		_createClass2(App, [{
			key: "decodeRow",
			value: function init(right, array, errorHandler) {
				var data = this.counters;
				data.fill(0);
				/** @type {string} */
				this.decodeRowResult = "";
				var individualMD5 = void 0;
				var width = void 0;
				var match = App.findAsteriskPattern(array, data);
				var value = array.getNextSet(match[1]);
				var y = array.getSize();
				do {
					App.recordPattern(array, value, data);
					var template = App.toNarrowWidePattern(data);
					if (template < 0) {
						throw new TypeError;
					}
					individualMD5 = App.patternToChar(template);
					this.decodeRowResult += individualMD5;
					width = value;
					/** @type {boolean} */
					var _iteratorNormalCompletion4 = true;
					/** @type {boolean} */
					var _didIteratorError4 = false;
					var _iteratorError4 = undefined;
					try {
						var _iterator4 = data[Symbol.iterator]();
						var $__6;
						for (; !(_iteratorNormalCompletion4 = ($__6 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
							var translate = $__6.value;
							value = value + translate;
						}
					} catch (err) {
						/** @type {boolean} */
						_didIteratorError4 = true;
						_iteratorError4 = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion4 && _iterator4.return) {
								_iterator4.return();
							}
						} finally {
							if (_didIteratorError4) {
								throw _iteratorError4;
							}
						}
					}
					value = array.getNextSet(value);
				} while ("*" !== individualMD5);
				/** @type {string} */
				this.decodeRowResult = this.decodeRowResult.substring(0, this.decodeRowResult.length - 1);
				var s = void 0;
				/** @type {number} */
				var x = 0;
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError5 = false;
				var _iteratorError5 = undefined;
				try {
					var _iterator3 = data[Symbol.iterator]();
					var $__6;
					for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var item = $__6.value;
						x = x + item;
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError5 = true;
					_iteratorError5 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError5) {
							throw _iteratorError5;
						}
					}
				}
				if (value !== y && 2 * (value - width - x) < x) {
					throw new TypeError;
				}
				if (this.usingCheckDigit) {
					/** @type {number} */
					var maxLen = this.decodeRowResult.length - 1;
					/** @type {number} */
					var i = 0;
					/** @type {number} */
					var lsd = 0;
					for (; lsd < maxLen; lsd++) {
						i = i + App.ALPHABET_STRING.indexOf(this.decodeRowResult.charAt(lsd));
					}
					if (this.decodeRowResult.charAt(maxLen) !== App.ALPHABET_STRING.charAt(i % 43)) {
						throw new Rectangle;
					}
					/** @type {string} */
					this.decodeRowResult = this.decodeRowResult.substring(0, maxLen);
				}
				if (0 === this.decodeRowResult.length) {
					throw new TypeError;
				}
				s = this.extendedMode ? App.decodeExtended(this.decodeRowResult) : this.decodeRowResult;
				/** @type {number} */
				var o = (match[1] + match[0]) / 2;
				var size = width + x / 2;
				return new result(s, null, 0, [new type(o, right), new type(size, right)], change.CODE_39, (new Date).getTime());
			}
		}], [{
			key: "findAsteriskPattern",
			value: function update(array, e) {
				var eleSize = array.getSize();
				var settings = array.getNextSet(0);
				/** @type {number} */
				var j = 0;
				var s = settings;
				/** @type {boolean} */
				var outer = false;
				var rows = e.length;
				var x = settings;
				for (; x < eleSize; x++) {
					if (array.get(x) !== outer) {
						e[j]++;
					} else {
						if (j === rows - 1) {
							if (this.toNarrowWidePattern(e) === App.ASTERISK_ENCODING && array.isRange(Math.max(0, s - Math.floor((x - s) / 2)), s, false)) {
								return [s, x];
							}
							s = s + (e[0] + e[1]);
							e.copyWithin(0, 2, 2 + j - 1);
							/** @type {number} */
							e[j - 1] = 0;
							/** @type {number} */
							e[j] = 0;
							j--;
						} else {
							j++;
						}
						/** @type {number} */
						e[j] = 1;
						/** @type {boolean} */
						outer = !outer;
					}
				}
				throw new TypeError;
			}
		}, {
			key: "toNarrowWidePattern",
			value: function from(params) {
				var count = void 0;
				var end = params.length;
				/** @type {number} */
				var max = 0;
				do {
					/** @type {number} */
					var min = 2147483647;
					/** @type {boolean} */
					var _iteratorNormalCompletion3 = true;
					/** @type {boolean} */
					var _didIteratorError6 = false;
					var _iteratorError6 = undefined;
					try {
						var _iterator3 = params[Symbol.iterator]();
						var step;
						for (; !(_iteratorNormalCompletion3 = (step = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
							var number = step.value;
							if (number < min && number > max) {
								min = number;
							}
						}
					} catch (err) {
						/** @type {boolean} */
						_didIteratorError6 = true;
						_iteratorError6 = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion3 && _iterator3.return) {
								_iterator3.return();
							}
						} finally {
							if (_didIteratorError6) {
								throw _iteratorError6;
							}
						}
					}
					max = min;
					/** @type {number} */
					count = 0;
					/** @type {number} */
					var total = 0;
					/** @type {number} */
					var to = 0;
					/** @type {number} */
					var j = 0;
					for (; j < end; j++) {
						var value = params[j];
						if (value > max) {
							/** @type {number} */
							to = to | 1 << end - 1 - j;
							count++;
							total = total + value;
						}
					}
					if (3 === count) {
						/** @type {number} */
						var i = 0;
						for (; i < end && count > 0; i++) {
							var limit = params[i];
							if (limit > max && (count--, 2 * limit >= total)) {
								return -1;
							}
						}
						return to;
					}
				} while (count > 3);
				return -1;
			}
		}, {
			key: "patternToChar",
			value: function encode(src) {
				/** @type {number} */
				var i = 0;
				for (; i < App.CHARACTER_ENCODINGS.length; i++) {
					if (App.CHARACTER_ENCODINGS[i] === src) {
						return App.ALPHABET_STRING.charAt(i);
					}
				}
				if (src === App.ASTERISK_ENCODING) {
					return "*";
				}
				throw new TypeError;
			}
		}, {
			key: "decodeExtended",
			value: function parse(value) {
				var sources = value.length;
				/** @type {string} */
				var string = "";
				/** @type {number} */
				var index = 0;
				for (; index < sources; index++) {
					var key = value.charAt(index);
					if ("+" === key || "$" === key || "%" === key || "/" === key) {
						var nextChar = value.charAt(index + 1);
						/** @type {string} */
						var data = "\x00";
						switch (key) {
							case "+":
								if (!(nextChar >= "A" && nextChar <= "Z")) {
									throw new Date;
								}
								/** @type {string} */
								data = String.fromCharCode(nextChar.charCodeAt(0) + 32);
								break;
							case "$":
								if (!(nextChar >= "A" && nextChar <= "Z")) {
									throw new Date;
								}
								/** @type {string} */
								data = String.fromCharCode(nextChar.charCodeAt(0) - 64);
								break;
							case "%":
								if (nextChar >= "A" && nextChar <= "E") {
									/** @type {string} */
									data = String.fromCharCode(nextChar.charCodeAt(0) - 38);
								} else {
									if (nextChar >= "F" && nextChar <= "J") {
										/** @type {string} */
										data = String.fromCharCode(nextChar.charCodeAt(0) - 11);
									} else {
										if (nextChar >= "K" && nextChar <= "O") {
											/** @type {string} */
											data = String.fromCharCode(nextChar.charCodeAt(0) + 16);
										} else {
											if (nextChar >= "P" && nextChar <= "T") {
												/** @type {string} */
												data = String.fromCharCode(nextChar.charCodeAt(0) + 43);
											} else {
												if ("U" === nextChar) {
													/** @type {string} */
													data = "\x00";
												} else {
													if ("V" === nextChar) {
														/** @type {string} */
														data = "@";
													} else {
														if ("W" === nextChar) {
															/** @type {string} */
															data = "`";
														} else {
															if ("X" !== nextChar && "Y" !== nextChar && "Z" !== nextChar) {
																throw new Date;
															}
															/** @type {string} */
															data = "\u007f";
														}
													}
												}
											}
										}
									}
								}
								break;
							case "/":
								if (nextChar >= "A" && nextChar <= "O") {
									/** @type {string} */
									data = String.fromCharCode(nextChar.charCodeAt(0) - 32);
								} else {
									if ("Z" !== nextChar) {
										throw new Date;
									}
									/** @type {string} */
									data = ":";
								}
						}
						/** @type {string} */
						string = string + data;
						index++;
					} else {
						/** @type {string} */
						string = string + key;
					}
				}
				return string;
			}
		}]);
		return App;
	}(path);
	/** @type {string} */
	Sprite.ALPHABET_STRING = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%";
	/** @type {!Array} */
	Sprite.CHARACTER_ENCODINGS = [52, 289, 97, 352, 49, 304, 112, 37, 292, 100, 265, 73, 328, 25, 280, 88, 13, 268, 76, 28, 259, 67, 322, 19, 274, 82, 7, 262, 70, 22, 385, 193, 448, 145, 400, 208, 133, 388, 196, 168, 162, 138, 42];
	/** @type {number} */
	Sprite.ASTERISK_ENCODING = 148;
	var EdgeDrawer = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @return {?}
		 */
		function self() {
			var _this;
			_classCallCheck2(this, self);
			_this = _possibleConstructorReturn(this, (self.__proto__ || Object.getPrototypeOf(self)).apply(this, arguments));
			_this;
			/** @type {number} */
			_this.narrowLineWidth = -1;
			return _this;
		}
		_inherits(self, _WebInspector$GeneralTreeElement);
		_createClass2(self, [{
			key: "decodeRow",
			value: function init(data, level, name) {
				var cfg = this.decodeStart(level);
				var record = this.decodeEnd(level);
				var s = new Buffer;
				self.decodeMiddle(level, cfg[1], record[0], s);
				var config = s.toString();
				/** @type {null} */
				var a = null;
				if (null != name) {
					a = name.get(node.ALLOWED_LENGTHS);
				}
				if (null == a) {
					a = self.DEFAULT_ALLOWED_LENGTHS;
				}
				var len = config.length;
				/** @type {boolean} */
				var nMax = false;
				/** @type {number} */
				var max = 0;
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError7 = false;
				var _iteratorError7 = undefined;
				try {
					var _iterator3 = a[Symbol.iterator]();
					var $__6;
					for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var item = $__6.value;
						if (len === item) {
							/** @type {boolean} */
							nMax = true;
							break;
						}
						if (item > max) {
							max = item;
						}
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError7 = true;
					_iteratorError7 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError7) {
							throw _iteratorError7;
						}
					}
				}
				if (!nMax && len > max && (nMax = true), !nMax) {
					throw new Date;
				}
				/** @type {!Array} */
				var u = [new type(cfg[1], data), new type(record[0], data)];
				return new result(config, null, 0, u, change.ITF, (new Date).getTime());
			}
		}, {
			key: "decodeStart",
			value: function readToken(res) {
				var p1 = self.skipWhiteSpace(res);
				var r = self.findGuardPattern(res, p1, self.START_PATTERN);
				return this.narrowLineWidth = (r[1] - r[0]) / 4, this.validateQuietZone(res, r[0]), r;
			}
		}, {
			key: "validateQuietZone",
			value: function base64Filter(dataObject, dataLength) {
				/** @type {number} */
				var avgRangeEnd = 10 * this.narrowLineWidth;
				avgRangeEnd = avgRangeEnd < dataLength ? avgRangeEnd : dataLength;
				/** @type {number} */
				var propName = dataLength - 1;
				for (; avgRangeEnd > 0 && propName >= 0 && !dataObject.get(propName); propName--) {
					avgRangeEnd--;
				}
				if (0 !== avgRangeEnd) {
					throw new TypeError;
				}
			}
		}, {
			key: "decodeEnd",
			value: function parseName(buffer) {
				buffer.reverse();
				try {
					var p = void 0;
					var version = self.skipWhiteSpace(buffer);
					try {
						p = self.findGuardPattern(buffer, version, self.END_PATTERN_REVERSED[0]);
					} catch (ex) {
						if (ex instanceof TypeError) {
							p = self.findGuardPattern(buffer, version, self.END_PATTERN_REVERSED[1]);
						}
					}
					this.validateQuietZone(buffer, p[0]);
					var v4 = p[0];
					return p[0] = buffer.getSize() - p[1], p[1] = buffer.getSize() - v4, p;
				} finally {
					buffer.reverse();
				}
			}
		}], [{
			key: "decodeMiddle",
			value: function hash(m, d, t, s) {
				/** @type {!Int32Array} */
				var data = new Int32Array(10);
				/** @type {!Int32Array} */
				var cache = new Int32Array(5);
				/** @type {!Int32Array} */
				var output = new Int32Array(5);
				data.fill(0);
				cache.fill(0);
				output.fill(0);
				for (; d < t;) {
					path.recordPattern(m, d, data);
					/** @type {number} */
					var name = 0;
					for (; name < 5; name++) {
						/** @type {number} */
						var prop = 2 * name;
						/** @type {number} */
						cache[name] = data[prop];
						/** @type {number} */
						output[name] = data[prop + 1];
					}
					var keys = self.decodeDigit(cache);
					s.append(keys.toString());
					keys = this.decodeDigit(output);
					s.append(keys.toString());
					data.forEach(function (n_per_item) {
						d = d + n_per_item;
					});
				}
			}
		}, {
			key: "skipWhiteSpace",
			value: function getFileConfigByExtName(array) {
				var preferiteIndex = array.getSize();
				var r = array.getNextSet(0);
				if (r === preferiteIndex) {
					throw new TypeError;
				}
				return r;
			}
		}, {
			key: "findGuardPattern",
			value: function init(shared, callback, pattern) {
				var length = pattern.length;
				/** @type {!Int32Array} */
				var buf = new Int32Array(length);
				var std = shared.getSize();
				/** @type {boolean} */
				var open = false;
				/** @type {number} */
				var i = 0;
				/** @type {!Array} */
				var handler = callback;
				buf.fill(0);
				/** @type {!Array} */
				var o = callback;
				for (; o < std; o++) {
					if (shared.get(o) !== open) {
						buf[i]++;
					} else {
						if (i === length - 1) {
							if (path.patternMatchVariance(buf, pattern, self.MAX_INDIVIDUAL_VARIANCE) < self.MAX_AVG_VARIANCE) {
								return [handler, o];
							}
							handler = handler + (buf[0] + buf[1]);
							System.arraycopy(buf, 2, buf, 0, i - 1);
							/** @type {number} */
							buf[i - 1] = 0;
							/** @type {number} */
							buf[i] = 0;
							i--;
						} else {
							i++;
						}
						/** @type {number} */
						buf[i] = 1;
						/** @type {boolean} */
						open = !open;
					}
				}
				throw new TypeError;
			}
		}, {
			key: "decodeDigit",
			value: function number(v) {
				var result = self.MAX_AVG_VARIANCE;
				/** @type {number} */
				var commonIndex = -1;
				var patchLen = self.PATTERNS.length;
				/** @type {number} */
				var i = 0;
				for (; i < patchLen; i++) {
					var pattern = self.PATTERNS[i];
					var key = path.patternMatchVariance(v, pattern, self.MAX_INDIVIDUAL_VARIANCE);
					if (key < result) {
						result = key;
						/** @type {number} */
						commonIndex = i;
					} else {
						if (key === result) {
							/** @type {number} */
							commonIndex = -1;
						}
					}
				}
				if (commonIndex >= 0) {
					return commonIndex % 10;
				}
				throw new TypeError;
			}
		}]);
		return self;
	}(path);
	/** @type {!Array} */
	EdgeDrawer.PATTERNS = [Int32Array.from([1, 1, 2, 2, 1]), Int32Array.from([2, 1, 1, 1, 2]), Int32Array.from([1, 2, 1, 1, 2]), Int32Array.from([2, 2, 1, 1, 1]), Int32Array.from([1, 1, 2, 1, 2]), Int32Array.from([2, 1, 2, 1, 1]), Int32Array.from([1, 2, 2, 1, 1]), Int32Array.from([1, 1, 1, 2, 2]), Int32Array.from([2, 1, 1, 2, 1]), Int32Array.from([1, 2, 1, 2, 1]), Int32Array.from([1, 1, 3, 3, 1]), Int32Array.from([3, 1, 1, 1, 3]), Int32Array.from([1, 3, 1, 1, 3]), Int32Array.from([3, 3, 1, 1, 1]),
	Int32Array.from([1, 1, 3, 1, 3]), Int32Array.from([3, 1, 3, 1, 1]), Int32Array.from([1, 3, 3, 1, 1]), Int32Array.from([1, 1, 1, 3, 3]), Int32Array.from([3, 1, 1, 3, 1]), Int32Array.from([1, 3, 1, 3, 1])];
	/** @type {number} */
	EdgeDrawer.MAX_AVG_VARIANCE = .38;
	/** @type {number} */
	EdgeDrawer.MAX_INDIVIDUAL_VARIANCE = .5;
	/** @type {!Array} */
	EdgeDrawer.DEFAULT_ALLOWED_LENGTHS = [6, 8, 10, 12, 14];
	/** @type {!Int32Array} */
	EdgeDrawer.START_PATTERN = Int32Array.from([1, 1, 1, 1]);
	/** @type {!Array} */
	EdgeDrawer.END_PATTERN_REVERSED = [Int32Array.from([1, 1, 2]), Int32Array.from([1, 1, 3])];
	var $ = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @return {?}
		 */
		function self() {
			var proto;
			_classCallCheck2(this, self);
			proto = _possibleConstructorReturn(this, (self.__proto__ || Object.getPrototypeOf(self)).apply(this, arguments));
			proto;
			/** @type {string} */
			proto.decodeRowStringBuffer = "";
			return proto;
		}
		_inherits(self, _WebInspector$GeneralTreeElement);
		_createClass2(self, null, [{
			key: "findStartGuardPattern",
			value: function f(params) {
				var c = void 0;
				/** @type {boolean} */
				var a = false;
				/** @type {number} */
				var p = 0;
				/** @type {!Int32Array} */
				var secStart = Int32Array.from([0, 0, 0]);
				for (; !a;) {
					/** @type {!Int32Array} */
					secStart = Int32Array.from([0, 0, 0]);
					var b = (c = self.findGuardPattern(params, p, false, this.START_END_PATTERN, secStart))[0];
					/** @type {number} */
					var i = b - ((p = c[1]) - b);
					if (i >= 0) {
						a = params.isRange(i, b, false);
					}
				}
				return c;
			}
		}, {
			key: "checkChecksum",
			value: function extractPresetLocal(callback) {
				return self.checkStandardUPCEANChecksum(callback);
			}
		}, {
			key: "checkStandardUPCEANChecksum",
			value: function checkForErrorCode(code) {
				var l = code.length;
				if (0 === l) {
					return false;
				}
				/** @type {number} */
				var whiteRating = parseInt(code.charAt(l - 1), 10);
				return self.getStandardUPCEANChecksum(code.substring(0, l - 1)) === whiteRating;
			}
		}, {
			key: "getStandardUPCEANChecksum",
			value: function buildTokenList(chars) {
				var count = chars.length;
				/** @type {number} */
				var ret = 0;
				/** @type {number} */
				var i = count - 1;
				for (; i >= 0; i = i - 2) {
					/** @type {number} */
					var dot = chars.charAt(i).charCodeAt(0) - "0".charCodeAt(0);
					if (dot < 0 || dot > 9) {
						throw new Date;
					}
					/** @type {number} */
					ret = ret + dot;
				}
				/** @type {number} */
				ret = ret * 3;
				/** @type {number} */
				var end = count - 2;
				for (; end >= 0; end = end - 2) {
					/** @type {number} */
					var dot = chars.charAt(end).charCodeAt(0) - "0".charCodeAt(0);
					if (dot < 0 || dot > 9) {
						throw new Date;
					}
					/** @type {number} */
					ret = ret + dot;
				}
				return (1e3 - ret) % 10;
			}
		}, {
			key: "decodeEnd",
			value: function naive_merge_rest(i1, i2) {
				return self.findGuardPattern(i1, i2, false, self.START_END_PATTERN, (new Int32Array(self.START_END_PATTERN.length)).fill(0));
			}
		}, {
			key: "findGuardPatternWithoutCounters",
			value: function naive_merge_rest(c1, c2, i1, i2) {
				return this.findGuardPattern(c1, c2, i1, i2, new Int32Array(i2.length));
			}
		}, {
			key: "findGuardPattern",
			value: function range(array, result, element, pattern, res) {
				var heightInTiles = array.getSize();
				/** @type {number} */
				var i = 0;
				var test = result = element ? array.getNextUnset(result) : array.getNextSet(result);
				var length = pattern.length;
				/** @type {boolean} */
				var outer = element;
				/** @type {string} */
				var r = result;
				for (; r < heightInTiles; r++) {
					if (array.get(r) !== outer) {
						res[i]++;
					} else {
						if (i === length - 1) {
							if (path.patternMatchVariance(res, pattern, self.MAX_INDIVIDUAL_VARIANCE) < self.MAX_AVG_VARIANCE) {
								return Int32Array.from([test, r]);
							}
							test = test + (res[0] + res[1]);
							var suggestionToBeAdded = res.slice(2, res.length - 1);
							/** @type {number} */
							var k = 0;
							for (; k < i - 1; k++) {
								res[k] = suggestionToBeAdded[k];
							}
							/** @type {number} */
							res[i - 1] = 0;
							/** @type {number} */
							res[i] = 0;
							i--;
						} else {
							i++;
						}
						/** @type {number} */
						res[i] = 1;
						/** @type {boolean} */
						outer = !outer;
					}
				}
				throw new TypeError;
			}
		}, {
			key: "decodeDigit",
			value: function Url$parse(parseQueryString, hostDenotesSlash, disableAutoEscapeChars, str) {
				this.recordPattern(parseQueryString, disableAutoEscapeChars, hostDenotesSlash);
				var ev_min = this.MAX_AVG_VARIANCE;
				/** @type {number} */
				var lastTransfer = -1;
				var len = str.length;
				/** @type {number} */
				var pos = 0;
				for (; pos < len; pos++) {
					var ch = str[pos];
					var value = path.patternMatchVariance(hostDenotesSlash, ch, self.MAX_INDIVIDUAL_VARIANCE);
					if (value < ev_min) {
						ev_min = value;
						/** @type {number} */
						lastTransfer = pos;
					}
				}
				if (lastTransfer >= 0) {
					return lastTransfer;
				}
				throw new TypeError;
			}
		}]);
		return self;
	}(path);
	/** @type {number} */
	$.MAX_AVG_VARIANCE = .48;
	/** @type {number} */
	$.MAX_INDIVIDUAL_VARIANCE = .7;
	/** @type {!Int32Array} */
	$.START_END_PATTERN = Int32Array.from([1, 1, 1]);
	/** @type {!Int32Array} */
	$.MIDDLE_PATTERN = Int32Array.from([1, 1, 1, 1, 1]);
	/** @type {!Int32Array} */
	$.END_PATTERN = Int32Array.from([1, 1, 1, 1, 1, 1]);
	/** @type {!Array} */
	$.L_PATTERNS = [Int32Array.from([3, 2, 1, 1]), Int32Array.from([2, 2, 2, 1]), Int32Array.from([2, 1, 2, 2]), Int32Array.from([1, 4, 1, 1]), Int32Array.from([1, 1, 3, 2]), Int32Array.from([1, 2, 3, 1]), Int32Array.from([1, 1, 1, 4]), Int32Array.from([1, 3, 1, 2]), Int32Array.from([1, 2, 1, 3]), Int32Array.from([3, 1, 1, 2])];
	var PluginError = function () {
		/**
		 * @return {undefined}
		 */
		function re() {
			_classCallCheck2(this, re);
			/** @type {!Array} */
			this.CHECK_DIGIT_ENCODINGS = [24, 20, 18, 17, 12, 6, 3, 10, 9, 5];
			/** @type {!Int32Array} */
			this.decodeMiddleCounters = Int32Array.from([0, 0, 0, 0]);
			/** @type {string} */
			this.decodeRowStringBuffer = "";
		}
		_createClass2(re, [{
			key: "decodeRow",
			value: function create(data, format, values) {
				var val = this.decodeRowStringBuffer;
				var text = this.decodeMiddle(format, values, val);
				var s = val.toString();
				var l = re.parseExtensionString(s);
				/** @type {!Array} */
				var defaults = [new type((values[0] + values[1]) / 2, data), new type(text, data)];
				var r = new result(s, null, 0, defaults, change.UPC_EAN_EXTENSION, (new Date).getTime());
				return null != l && r.putAllMetadata(l), r;
			}
		}, {
			key: "decodeMiddle",
			value: function refresh(pro, msg, url) {
				var menuItems = this.decodeMiddleCounters;
				/** @type {number} */
				menuItems[0] = 0;
				/** @type {number} */
				menuItems[1] = 0;
				/** @type {number} */
				menuItems[2] = 0;
				/** @type {number} */
				menuItems[3] = 0;
				var min = pro.getSize();
				var result = msg[1];
				/** @type {number} */
				var permissions = 0;
				/** @type {number} */
				var i = 0;
				for (; i < 5 && result < min; i++) {
					var detector = $.decodeDigit(pro, menuItems, result, $.L_AND_G_PATTERNS);
					/** @type {string} */
					url = url + String.fromCharCode("0".charCodeAt(0) + detector % 10);
					/** @type {boolean} */
					var _iteratorNormalCompletion3 = true;
					/** @type {boolean} */
					var _didIteratorError8 = false;
					var _iteratorError8 = undefined;
					try {
						var _iterator3 = menuItems[Symbol.iterator]();
						var $__6;
						for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
							var item = $__6.value;
							result = result + item;
						}
					} catch (err) {
						/** @type {boolean} */
						_didIteratorError8 = true;
						_iteratorError8 = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion3 && _iterator3.return) {
								_iterator3.return();
							}
						} finally {
							if (_didIteratorError8) {
								throw _iteratorError8;
							}
						}
					}
					if (detector >= 10) {
						/** @type {number} */
						permissions = permissions | 1 << 4 - i;
					}
					if (4 !== i) {
						result = pro.getNextSet(result);
						result = pro.getNextUnset(result);
					}
				}
				if (5 !== url.length) {
					throw new TypeError;
				}
				var groupPermissionsRef = this.determineCheckDigit(permissions);
				if (re.extensionChecksum(url.toString()) !== groupPermissionsRef) {
					throw new TypeError;
				}
				return result;
			}
		}, {
			key: "determineCheckDigit",
			value: function collection_check(key) {
				/** @type {number} */
				var i = 0;
				for (; i < 10; i++) {
					if (key === this.CHECK_DIGIT_ENCODINGS[i]) {
						return i;
					}
				}
				throw new TypeError;
			}
		}], [{
			key: "extensionChecksum",
			value: function buildTokenList(chars) {
				var count = chars.length;
				/** @type {number} */
				var ypos = 0;
				/** @type {number} */
				var i = count - 2;
				for (; i >= 0; i = i - 2) {
					/** @type {number} */
					ypos = ypos + (chars.charAt(i).charCodeAt(0) - "0".charCodeAt(0));
				}
				/** @type {number} */
				ypos = ypos * 3;
				/** @type {number} */
				var end = count - 1;
				for (; end >= 0; end = end - 2) {
					/** @type {number} */
					ypos = ypos + (chars.charAt(end).charCodeAt(0) - "0".charCodeAt(0));
				}
				return (ypos = ypos * 3) % 10;
			}
		}, {
			key: "parseExtensionString",
			value: function isPermutationMap(str1) {
				if (5 !== str1.length) {
					return null;
				}
				var dist = re.parseExtension5String(str1);
				return null == dist ? null : new Map([[value.SUGGESTED_PRICE, dist]]);
			}
		}, {
			key: "parseExtension5String",
			value: function addProcessedTaskTracerData(result) {
				var newMid = void 0;
				switch (result.charAt(0)) {
					case "0":
						/** @type {string} */
						newMid = "\u00a3";
						break;
					case "5":
						/** @type {string} */
						newMid = "$";
						break;
					case "9":
						switch (result) {
							case "90000":
								return null;
							case "99991":
								return "0.00";
							case "99990":
								return "Used";
						}/** @type {string} */
						newMid = "";
						break;
					default:
						/** @type {string} */
						newMid = "";
				}
				/** @type {number} */
				var i = parseInt(result.substring(1));
				/** @type {number} */
				var angle = i % 100;
				return newMid + (i / 100).toString() + "." + (angle < 10 ? "0" + angle : angle.toString());
			}
		}]);
		return re;
	}();
	var LDAPError = function () {
		/**
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3() {
			_classCallCheck2(this, TempusDominusBootstrap3);
			/** @type {!Int32Array} */
			this.decodeMiddleCounters = Int32Array.from([0, 0, 0, 0]);
			/** @type {string} */
			this.decodeRowStringBuffer = "";
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "decodeRow",
			value: function create(data, format, values) {
				var val = this.decodeRowStringBuffer;
				var text = this.decodeMiddle(format, values, val);
				var s = val.toString();
				var l = TempusDominusBootstrap3.parseExtensionString(s);
				/** @type {!Array} */
				var defaults = [new type((values[0] + values[1]) / 2, data), new type(text, data)];
				var r = new result(s, null, 0, defaults, change.UPC_EAN_EXTENSION, (new Date).getTime());
				return null != l && r.putAllMetadata(l), r;
			}
		}, {
			key: "decodeMiddle",
			value: function get(pro, obj, skip) {
				var menuItems = this.decodeMiddleCounters;
				/** @type {number} */
				menuItems[0] = 0;
				/** @type {number} */
				menuItems[1] = 0;
				/** @type {number} */
				menuItems[2] = 0;
				/** @type {number} */
				menuItems[3] = 0;
				var last = pro.getSize();
				var query = obj[1];
				/** @type {number} */
				var o = 0;
				/** @type {number} */
				var toIndex = 0;
				for (; toIndex < 2 && query < last; toIndex++) {
					var fromPlugin = $.decodeDigit(pro, menuItems, query, $.L_AND_G_PATTERNS);
					/** @type {string} */
					skip = skip + String.fromCharCode("0".charCodeAt(0) + fromPlugin % 10);
					/** @type {boolean} */
					var _iteratorNormalCompletion3 = true;
					/** @type {boolean} */
					var _didIteratorError9 = false;
					var _iteratorError9 = undefined;
					try {
						var _iterator3 = menuItems[Symbol.iterator]();
						var $__6;
						for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
							var item = $__6.value;
							query = query + item;
						}
					} catch (err) {
						/** @type {boolean} */
						_didIteratorError9 = true;
						_iteratorError9 = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion3 && _iterator3.return) {
								_iterator3.return();
							}
						} finally {
							if (_didIteratorError9) {
								throw _iteratorError9;
							}
						}
					}
					if (fromPlugin >= 10) {
						/** @type {number} */
						o = o | 1 << 1 - toIndex;
					}
					if (1 !== toIndex) {
						query = pro.getNextSet(query);
						query = pro.getNextUnset(query);
					}
				}
				if (2 !== skip.length) {
					throw new TypeError;
				}
				if (parseInt(skip.toString()) % 4 !== o) {
					throw new TypeError;
				}
				return query;
			}
		}], [{
			key: "parseExtensionString",
			value: function genErrors(klass) {
				return 2 !== klass.length ? null : new Map([[value.ISSUE_NUMBER, parseInt(klass)]]);
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var P = function () {
		/**
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3() {
			_classCallCheck2(this, TempusDominusBootstrap3);
		}
		_createClass2(TempusDominusBootstrap3, null, [{
			key: "decodeRow",
			value: function emitError(message, e, type) {
				var n = $.findGuardPattern(e, type, false, this.EXTENSION_START_PATTERN, (new Int32Array(this.EXTENSION_START_PATTERN.length)).fill(0));
				try {
					return (new PluginError).decodeRow(message, e, n);
				} catch (r) {
					return (new LDAPError).decodeRow(message, e, n);
				}
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	/** @type {!Int32Array} */
	P.EXTENSION_START_PATTERN = Int32Array.from([1, 1, 2]);
	var client = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @return {?}
		 */
		function self() {
			var proto;
			_classCallCheck2(this, self);
			proto = _possibleConstructorReturn(this, (self.__proto__ || Object.getPrototypeOf(self)).call(this));
			proto;
			/** @type {string} */
			proto.decodeRowStringBuffer = "";
			self.L_AND_G_PATTERNS = self.L_PATTERNS.map(function (buffer) {
				return Int32Array.from(buffer);
			});
			/** @type {number} */
			var step = 10;
			for (; step < 20; step++) {
				var buffer = self.L_PATTERNS[step - 10];
				/** @type {!Int32Array} */
				var result = new Int32Array(buffer.length);
				/** @type {number} */
				var i = 0;
				for (; i < buffer.length; i++) {
					result[i] = buffer[buffer.length - i - 1];
				}
				/** @type {!Int32Array} */
				self.L_AND_G_PATTERNS[step] = result;
			}
			return proto;
		}
		_inherits(self, _WebInspector$GeneralTreeElement);
		_createClass2(self, [{
			key: "decodeRow",
			value: function init(args, params, subjectTree) {
				var count = self.findStartGuardPattern(params);
				var DOCUMENT_TIMELINE = null == subjectTree ? null : subjectTree.get(node.NEED_RESULT_POINT_CALLBACK);
				if (null != DOCUMENT_TIMELINE) {
					var anim = new type((count[0] + count[1]) / 2, args);
					DOCUMENT_TIMELINE.foundPossibleResultPoint(anim);
				}
				var cmd = this.decodeMiddle(params, count, this.decodeRowStringBuffer);
				var options = cmd.rowOffset;
				var pid = cmd.resultString;
				if (null != DOCUMENT_TIMELINE) {
					var anim = new type(options, args);
					DOCUMENT_TIMELINE.foundPossibleResultPoint(anim);
				}
				var data = self.decodeEnd(params, options);
				if (null != DOCUMENT_TIMELINE) {
					var anim = new type((data[0] + data[1]) / 2, args);
					DOCUMENT_TIMELINE.foundPossibleResultPoint(anim);
				}
				var x = data[1];
				var file = x + (x - data[0]);
				if (file >= params.getSize() || !params.isRange(x, file, false)) {
					throw new TypeError;
				}
				var s = pid.toString();
				if (s.length < 8) {
					throw new Date;
				}
				if (!self.checkChecksum(s)) {
					throw new Rectangle;
				}
				/** @type {number} */
				var key = (count[1] + count[0]) / 2;
				/** @type {number} */
				var o = (data[1] + data[0]) / 2;
				var builtIns = this.getBarcodeFormat();
				/** @type {!Array} */
				var endRow = [new type(key, args), new type(o, args)];
				var r = new result(s, null, 0, endRow, builtIns, (new Date).getTime());
				/** @type {number} */
				var nb_ft = 0;
				try {
					var actMethod = P.decodeRow(args, params, data[1]);
					r.putMetadata(value.UPC_EAN_EXTENSION, actMethod.getText());
					r.putAllMetadata(actMethod.getResultMetadata());
					r.addResultPoints(actMethod.getResultPoints());
					nb_ft = actMethod.getText().length;
				} catch (t) {
				}
				var btnClasses = null == subjectTree ? null : subjectTree.get(node.ALLOWED_EAN_EXTENSIONS);
				if (null != btnClasses) {
					/** @type {boolean} */
					var _t76 = false;
					var btnIt;
					for (btnIt in btnClasses) {
						if (nb_ft.toString() === btnIt) {
							/** @type {boolean} */
							_t76 = true;
							break;
						}
					}
					if (!_t76) {
						throw new TypeError;
					}
				}
				return builtIns === change.EAN_13 || change.UPC_A, r;
			}
		}], [{
			key: "checkChecksum",
			value: function extractPresetLocal(callback) {
				return self.checkStandardUPCEANChecksum(callback);
			}
		}, {
			key: "checkStandardUPCEANChecksum",
			value: function checkForErrorCode(code) {
				var l = code.length;
				if (0 === l) {
					return false;
				}
				/** @type {number} */
				var whiteRating = parseInt(code.charAt(l - 1), 10);
				return self.getStandardUPCEANChecksum(code.substring(0, l - 1)) === whiteRating;
			}
		}, {
			key: "getStandardUPCEANChecksum",
			value: function buildTokenList(chars) {
				var count = chars.length;
				/** @type {number} */
				var ret = 0;
				/** @type {number} */
				var i = count - 1;
				for (; i >= 0; i = i - 2) {
					/** @type {number} */
					var dot = chars.charAt(i).charCodeAt(0) - "0".charCodeAt(0);
					if (dot < 0 || dot > 9) {
						throw new Date;
					}
					/** @type {number} */
					ret = ret + dot;
				}
				/** @type {number} */
				ret = ret * 3;
				/** @type {number} */
				var end = count - 2;
				for (; end >= 0; end = end - 2) {
					/** @type {number} */
					var dot = chars.charAt(end).charCodeAt(0) - "0".charCodeAt(0);
					if (dot < 0 || dot > 9) {
						throw new Date;
					}
					/** @type {number} */
					ret = ret + dot;
				}
				return (1e3 - ret) % 10;
			}
		}, {
			key: "decodeEnd",
			value: function naive_merge_rest(i1, i2) {
				return self.findGuardPattern(i1, i2, false, self.START_END_PATTERN, (new Int32Array(self.START_END_PATTERN.length)).fill(0));
			}
		}]);
		return self;
	}($);
	var Actions = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @return {?}
		 */
		function TacoTableCell() {
			var _this;
			_classCallCheck2(this, TacoTableCell);
			_this = _possibleConstructorReturn(this, (TacoTableCell.__proto__ || Object.getPrototypeOf(TacoTableCell)).call(this));
			_this;
			/** @type {!Int32Array} */
			_this.decodeMiddleCounters = Int32Array.from([0, 0, 0, 0]);
			return _this;
		}
		_inherits(TacoTableCell, _WebInspector$GeneralTreeElement);
		_createClass2(TacoTableCell, [{
			key: "decodeMiddle",
			value: function collideTilemap(tx, data, index) {
				var set = this.decodeMiddleCounters;
				/** @type {number} */
				set[0] = 0;
				/** @type {number} */
				set[1] = 0;
				/** @type {number} */
				set[2] = 0;
				/** @type {number} */
				set[3] = 0;
				var MAG_MAX_REFERENCES = tx.getSize();
				var i = data[1];
				/** @type {number} */
				var indexOctant = 0;
				/** @type {number} */
				var c = 0;
				for (; c < 6 && i < MAG_MAX_REFERENCES; c++) {
					var val = client.decodeDigit(tx, set, i, client.L_AND_G_PATTERNS);
					/** @type {string} */
					index = index + String.fromCharCode("0".charCodeAt(0) + val % 10);
					/** @type {boolean} */
					var _iteratorNormalCompletion3 = true;
					/** @type {boolean} */
					var _didIteratorError10 = false;
					var _iteratorError10 = undefined;
					try {
						var _iterator3 = set[Symbol.iterator]();
						var $__6;
						for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
							var item = $__6.value;
							i = i + item;
						}
					} catch (err) {
						/** @type {boolean} */
						_didIteratorError10 = true;
						_iteratorError10 = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion3 && _iterator3.return) {
								_iterator3.return();
							}
						} finally {
							if (_didIteratorError10) {
								throw _iteratorError10;
							}
						}
					}
					if (val >= 10) {
						/** @type {number} */
						indexOctant = indexOctant | 1 << 5 - c;
					}
				}
				index = TacoTableCell.determineFirstDigit(index, indexOctant);
				i = client.findGuardPattern(tx, i, true, client.MIDDLE_PATTERN, (new Int32Array(client.MIDDLE_PATTERN.length)).fill(0))[1];
				/** @type {number} */
				var mMagRefIdx = 0;
				for (; mMagRefIdx < 6 && i < MAG_MAX_REFERENCES; mMagRefIdx++) {
					var val = client.decodeDigit(tx, set, i, client.L_PATTERNS);
					/** @type {string} */
					index = index + String.fromCharCode("0".charCodeAt(0) + val);
					/** @type {boolean} */
					var _iteratorNormalCompletion3 = true;
					/** @type {boolean} */
					var _didIteratorError11 = false;
					var _iteratorError11 = undefined;
					try {
						var _iterator3 = set[Symbol.iterator]();
						var $__6;
						for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
							var item = $__6.value;
							i = i + item;
						}
					} catch (err) {
						/** @type {boolean} */
						_didIteratorError11 = true;
						_iteratorError11 = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion3 && _iterator3.return) {
								_iterator3.return();
							}
						} finally {
							if (_didIteratorError11) {
								throw _iteratorError11;
							}
						}
					}
				}
				return {
					rowOffset: i,
					resultString: index
				};
			}
		}, {
			key: "getBarcodeFormat",
			value: function getBarcodeFormat() {
				return change.EAN_13;
			}
		}], [{
			key: "determineFirstDigit",
			value: function columnLabelMaker_(type, s) {
				/** @type {number} */
				var i = 0;
				for (; i < 10; i++) {
					if (s === this.FIRST_DIGIT_ENCODINGS[i]) {
						return type = String.fromCharCode("0".charCodeAt(0) + i) + type;
					}
				}
				throw new TypeError;
			}
		}]);
		return TacoTableCell;
	}(client);
	/** @type {!Array} */
	Actions.FIRST_DIGIT_ENCODINGS = [0, 11, 13, 14, 19, 25, 28, 21, 22, 26];
	var MyBlock = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @return {?}
		 */
		function TacoTableCell() {
			var _this;
			_classCallCheck2(this, TacoTableCell);
			_this = _possibleConstructorReturn(this, (TacoTableCell.__proto__ || Object.getPrototypeOf(TacoTableCell)).call(this));
			_this;
			/** @type {!Int32Array} */
			_this.decodeMiddleCounters = Int32Array.from([0, 0, 0, 0]);
			return _this;
		}
		_inherits(TacoTableCell, _WebInspector$GeneralTreeElement);
		_createClass2(TacoTableCell, [{
			key: "decodeMiddle",
			value: function collideTilemap(tx, index, resultString) {
				var set = this.decodeMiddleCounters;
				/** @type {number} */
				set[0] = 0;
				/** @type {number} */
				set[1] = 0;
				/** @type {number} */
				set[2] = 0;
				/** @type {number} */
				set[3] = 0;
				var MAG_MAX_REFERENCES = tx.getSize();
				var i = index[1];
				/** @type {number} */
				var c = 0;
				for (; c < 4 && i < MAG_MAX_REFERENCES; c++) {
					var val = client.decodeDigit(tx, set, i, client.L_PATTERNS);
					/** @type {string} */
					resultString = resultString + String.fromCharCode("0".charCodeAt(0) + val);
					/** @type {boolean} */
					var _iteratorNormalCompletion3 = true;
					/** @type {boolean} */
					var _didIteratorError12 = false;
					var _iteratorError12 = undefined;
					try {
						var _iterator3 = set[Symbol.iterator]();
						var $__6;
						for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
							var item = $__6.value;
							i = i + item;
						}
					} catch (err) {
						/** @type {boolean} */
						_didIteratorError12 = true;
						_iteratorError12 = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion3 && _iterator3.return) {
								_iterator3.return();
							}
						} finally {
							if (_didIteratorError12) {
								throw _iteratorError12;
							}
						}
					}
				}
				i = client.findGuardPattern(tx, i, true, client.MIDDLE_PATTERN, (new Int32Array(client.MIDDLE_PATTERN.length)).fill(0))[1];
				/** @type {number} */
				var mMagRefIdx = 0;
				for (; mMagRefIdx < 4 && i < MAG_MAX_REFERENCES; mMagRefIdx++) {
					var val = client.decodeDigit(tx, set, i, client.L_PATTERNS);
					/** @type {string} */
					resultString = resultString + String.fromCharCode("0".charCodeAt(0) + val);
					/** @type {boolean} */
					var _iteratorNormalCompletion3 = true;
					/** @type {boolean} */
					var _didIteratorError13 = false;
					var _iteratorError13 = undefined;
					try {
						var _iterator3 = set[Symbol.iterator]();
						var $__4;
						for (; !(_iteratorNormalCompletion3 = ($__4 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
							var item = $__4.value;
							i = i + item;
						}
					} catch (err) {
						/** @type {boolean} */
						_didIteratorError13 = true;
						_iteratorError13 = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion3 && _iterator3.return) {
								_iterator3.return();
							}
						} finally {
							if (_didIteratorError13) {
								throw _iteratorError13;
							}
						}
					}
				}
				return {
					rowOffset: i,
					resultString: resultString
				};
			}
		}, {
			key: "getBarcodeFormat",
			value: function getBarcodeFormat() {
				return change.EAN_8;
			}
		}]);
		return TacoTableCell;
	}(client);
	var Stage = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @return {?}
		 */
		function ArchiveForm() {
			var _this;
			_classCallCheck2(this, ArchiveForm);
			_this = _possibleConstructorReturn(this, (ArchiveForm.__proto__ || Object.getPrototypeOf(ArchiveForm)).apply(this, arguments));
			_this;
			_this.ean13Reader = new Actions;
			return _this;
		}
		_inherits(ArchiveForm, _WebInspector$GeneralTreeElement);
		_createClass2(ArchiveForm, [{
			key: "getBarcodeFormat",
			value: function getBarcodeFormat() {
				return change.UPC_A;
			}
		}, {
			key: "decode",
			value: function decode(input, isBinaryData) {
				return this.maybeReturnResult(this.ean13Reader.decode(input));
			}
		}, {
			key: "decodeRow",
			value: function $get(mmCoreSplitViewBlock, $state, $interpolate) {
				return this.maybeReturnResult(this.ean13Reader.decodeRow(mmCoreSplitViewBlock, $state, $interpolate));
			}
		}, {
			key: "decodeMiddle",
			value: function $get(mmCoreSplitViewBlock, $state, $interpolate) {
				return this.ean13Reader.decodeMiddle(mmCoreSplitViewBlock, $state, $interpolate);
			}
		}, {
			key: "maybeReturnResult",
			value: function create(t) {
				var valueConfig = t.getText();
				if ("0" === valueConfig.charAt(0)) {
					var r = new result(valueConfig.substring(1), null, null, t.getResultPoints(), change.UPC_A);
					return null != t.getResultMetadata() && r.putAllMetadata(t.getResultMetadata()), r;
				}
				throw new TypeError;
			}
		}, {
			key: "reset",
			value: function TimeHandlr() {
				this.ean13Reader.reset();
			}
		}]);
		return ArchiveForm;
	}(client);
	var Register = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @return {?}
		 */
		function self() {
			var result;
			_classCallCheck2(this, self);
			result = _possibleConstructorReturn(this, (self.__proto__ || Object.getPrototypeOf(self)).call(this));
			result;
			/** @type {!Int32Array} */
			result.decodeMiddleCounters = new Int32Array(4);
			return result;
		}
		_inherits(self, _WebInspector$GeneralTreeElement);
		_createClass2(self, [{
			key: "decodeMiddle",
			value: function orchestratorPlugin(schema, parentNode, args) {
				var x = this.decodeMiddleCounters.map(function (canCreateDiscussions) {
					return canCreateDiscussions;
				});
				/** @type {number} */
				x[0] = 0;
				/** @type {number} */
				x[1] = 0;
				/** @type {number} */
				x[2] = 0;
				/** @type {number} */
				x[3] = 0;
				var min = schema.getSize();
				var result = parentNode[1];
				/** @type {number} */
				var array = 0;
				/** @type {number} */
				var i = 0;
				for (; i < 6 && result < min; i++) {
					var keys = self.decodeDigit(schema, x, result, self.L_AND_G_PATTERNS);
					/** @type {string} */
					args = args + String.fromCharCode("0".charCodeAt(0) + keys % 10);
					/** @type {boolean} */
					var _iteratorNormalCompletion3 = true;
					/** @type {boolean} */
					var _didIteratorError14 = false;
					var _iteratorError14 = undefined;
					try {
						var _iterator3 = x[Symbol.iterator]();
						var _step14;
						for (; !(_iteratorNormalCompletion3 = (_step14 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
							var valueString = _step14.value;
							result = result + valueString;
						}
					} catch (err) {
						/** @type {boolean} */
						_didIteratorError14 = true;
						_iteratorError14 = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion3 && _iterator3.return) {
								_iterator3.return();
							}
						} finally {
							if (_didIteratorError14) {
								throw _iteratorError14;
							}
						}
					}
					if (keys >= 10) {
						/** @type {number} */
						array = array | 1 << 5 - i;
					}
				}
				return self.determineNumSysAndCheckDigit(new Buffer(args), array), result;
			}
		}, {
			key: "decodeEnd",
			value: function $get(selector, what) {
				return self.findGuardPatternWithoutCounters(selector, what, true, self.MIDDLE_END_PATTERN);
			}
		}, {
			key: "checkChecksum",
			value: function mutateDataFromClient(data) {
				return client.checkChecksum(self.convertUPCEtoUPCA(data));
			}
		}, {
			key: "getBarcodeFormat",
			value: function getBarcodeFormat() {
				return change.UPC_E;
			}
		}], [{
			key: "determineNumSysAndCheckDigit",
			value: function delAllCreated(t, callback) {
				/** @type {number} */
				var m = 0;
				for (; m <= 1; m++) {
					/** @type {number} */
					var i = 0;
					for (; i < 10; i++) {
						if (callback === this.NUMSYS_AND_CHECK_DIGIT_PATTERNS[m][i]) {
							return t.insert(0, "0" + m), void t.append("0" + i);
						}
					}
				}
				throw TypeError.getNotFoundInstance();
			}
		}, {
			key: "convertUPCEtoUPCA",
			value: function reset(t) {
				var result = t.slice(1, 7).split("").map(function (strUtf8) {
					return strUtf8.charCodeAt(0);
				});
				var r = new Buffer;
				r.append(t.charAt(0));
				var n = result[5];
				switch (n) {
					case 0:
					case 1:
					case 2:
						r.appendChars(result, 0, 2);
						r.append(n);
						r.append("0000");
						r.appendChars(result, 2, 3);
						break;
					case 3:
						r.appendChars(result, 0, 3);
						r.append("00000");
						r.appendChars(result, 3, 2);
						break;
					case 4:
						r.appendChars(result, 0, 4);
						r.append("00000");
						r.append(result[4]);
						break;
					default:
						r.appendChars(result, 0, 5);
						r.append("0000");
						r.append(n);
				}
				return t.length >= 8 && r.append(t.charAt(7)), r.toString();
			}
		}]);
		return self;
	}(client);
	/** @type {!Int32Array} */
	Register.MIDDLE_END_PATTERN = Int32Array.from([1, 1, 1, 1, 1, 1]);
	/** @type {!Array} */
	Register.NUMSYS_AND_CHECK_DIGIT_PATTERNS = [Int32Array.from([56, 52, 50, 49, 44, 38, 35, 42, 41, 37]), Int32Array.from([7, 11, 13, 14, 19, 25, 28, 21, 22, 1])];
	var ObjectType = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @param {!Object} h
		 * @return {?}
		 */
		function Stage(h) {
			_classCallCheck2(this, Stage);
			var _this = _possibleConstructorReturn(this, (Stage.__proto__ || Object.getPrototypeOf(Stage)).call(this));
			var ignoreEmitEvents = null == h ? null : h.get(node.POSSIBLE_FORMATS);
			/** @type {!Array} */
			var r = [];
			if (null != ignoreEmitEvents) {
				if (ignoreEmitEvents.indexOf(change.EAN_13) > -1) {
					r.push(new Actions);
				} else {
					if (ignoreEmitEvents.indexOf(change.UPC_A) > -1) {
						r.push(new Stage);
					}
				}
				if (ignoreEmitEvents.indexOf(change.EAN_8) > -1) {
					r.push(new MyBlock);
				}
				if (ignoreEmitEvents.indexOf(change.UPC_E) > -1) {
					r.push(new Register);
				}
			}
			if (0 === r.length) {
				r.push(new Actions);
				r.push(new MyBlock);
				r.push(new Register);
			}
			/** @type {!Array} */
			_this.readers = r;
			return _this;
		}
		_inherits(Stage, _WebInspector$GeneralTreeElement);
		_createClass2(Stage, [{
			key: "decodeRow",
			value: function create(props, method, from) {
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError15 = false;
				var referenceNameDecl = undefined;
				try {
					var _iterator3 = this.readers[Symbol.iterator]();
					var item;
					for (; !(_iteratorNormalCompletion3 = (item = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var b = item.value;
						try {
							var p = b.decodeRow(props, method, from);
							/** @type {boolean} */
							var reverseIsSingle = p.getBarcodeFormat() === change.EAN_13 && "0" === p.getText().charAt(0);
							var events = null == from ? null : from.get(node.POSSIBLE_FORMATS);
							var reverseValue = null == events || events.includes(change.UPC_A);
							if (reverseIsSingle && reverseValue) {
								var gm = p.getRawBytes();
								var r = new result(p.getText().substring(1), gm, gm.length, p.getResultPoints(), change.UPC_A);
								return r.putAllMetadata(p.getResultMetadata()), r;
							}
							return p;
						} catch (t) {
						}
					}
				} catch (undefined) {
					/** @type {boolean} */
					_didIteratorError15 = true;
					referenceNameDecl = undefined;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError15) {
							throw referenceNameDecl;
						}
					}
				}
				throw new TypeError;
			}
		}, {
			key: "reset",
			value: function _destroyWaypoints() {
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError16 = false;
				var _iteratorError16 = undefined;
				try {
					var _iterator3 = this.readers[Symbol.iterator]();
					var $__6;
					for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var item = $__6.value;
						item.reset();
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError16 = true;
					_iteratorError16 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError16) {
							throw _iteratorError16;
						}
					}
				}
			}
		}]);
		return Stage;
	}(path);
	var hash = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @return {?}
		 */
		function World() {
			var mesh;
			_classCallCheck2(this, World);
			mesh = _possibleConstructorReturn(this, (World.__proto__ || Object.getPrototypeOf(World)).call(this));
			mesh;
			/** @type {!Int32Array} */
			mesh.decodeFinderCounters = new Int32Array(4);
			/** @type {!Int32Array} */
			mesh.dataCharacterCounters = new Int32Array(8);
			/** @type {!Array} */
			mesh.oddRoundingErrors = new Array(4);
			/** @type {!Array} */
			mesh.evenRoundingErrors = new Array(4);
			/** @type {!Array} */
			mesh.oddCounts = new Array(mesh.dataCharacterCounters.length / 2);
			/** @type {!Array} */
			mesh.evenCounts = new Array(mesh.dataCharacterCounters.length / 2);
			return mesh;
		}
		_inherits(World, _WebInspector$GeneralTreeElement);
		_createClass2(World, [{
			key: "getDecodeFinderCounters",
			value: function getDecodeFinderCounters() {
				return this.decodeFinderCounters;
			}
		}, {
			key: "getDataCharacterCounters",
			value: function getDataCharacterCounters() {
				return this.dataCharacterCounters;
			}
		}, {
			key: "getOddRoundingErrors",
			value: function getOddRoundingErrors() {
				return this.oddRoundingErrors;
			}
		}, {
			key: "getEvenRoundingErrors",
			value: function getEvenRoundingErrors() {
				return this.evenRoundingErrors;
			}
		}, {
			key: "getOddCounts",
			value: function getOddCounts() {
				return this.oddCounts;
			}
		}, {
			key: "getEvenCounts",
			value: function getEvenCounts() {
				return this.evenCounts;
			}
		}, {
			key: "parseFinderValue",
			value: function collection_check(key, keys) {
				/** @type {number} */
				var i = 0;
				for (; i < keys.length; i++) {
					if (path.patternMatchVariance(key, keys[i], World.MAX_INDIVIDUAL_VARIANCE) < World.MAX_AVG_VARIANCE) {
						return i;
					}
				}
				throw new TypeError;
			}
		}], [{
			key: "count",
			value: function sub(data) {
				return p.sum(new Int32Array(data));
			}
		}, {
			key: "increment",
			value: function calcReturns(err, prices) {
				/** @type {number} */
				var status = 0;
				var minBuy = prices[0];
				/** @type {number} */
				var i = 1;
				for (; i < err.length; i++) {
					if (prices[i] > minBuy) {
						minBuy = prices[i];
						/** @type {number} */
						status = i;
					}
				}
				err[status]++;
			}
		}, {
			key: "decrement",
			value: function _spawnService(output, input) {
				/** @type {number} */
				var overlapIndex = 0;
				var m = input[0];
				/** @type {number} */
				var j = 1;
				for (; j < output.length; j++) {
					if (input[j] < m) {
						m = input[j];
						/** @type {number} */
						overlapIndex = j;
					}
				}
				output[overlapIndex]--;
			}
		}, {
			key: "isFinderPattern",
			value: function hasScopeDescriptor(fields) {
				var result = fields[0] + fields[1];
				/** @type {number} */
				var r = result / (result + fields[2] + fields[3]);
				if (r >= World.MIN_FINDER_PATTERN_RATIO && r <= World.MAX_FINDER_PATTERN_RATIO) {
					/** @type {number} */
					var min = Number.MAX_SAFE_INTEGER;
					/** @type {number} */
					var max = Number.MIN_SAFE_INTEGER;
					/** @type {boolean} */
					var _iteratorNormalCompletion3 = true;
					/** @type {boolean} */
					var _didIteratorError17 = false;
					var _iteratorError17 = undefined;
					try {
						var _iterator3 = fields[Symbol.iterator]();
						var _step17;
						for (; !(_iteratorNormalCompletion3 = (_step17 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
							var xValue = _step17.value;
							if (xValue > max) {
								max = xValue;
							}
							if (xValue < min) {
								min = xValue;
							}
						}
					} catch (err) {
						/** @type {boolean} */
						_didIteratorError17 = true;
						_iteratorError17 = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion3 && _iterator3.return) {
								_iterator3.return();
							}
						} finally {
							if (_didIteratorError17) {
								throw _iteratorError17;
							}
						}
					}
					return max < 10 * min;
				}
				return false;
			}
		}]);
		return World;
	}(path);
	/** @type {number} */
	hash.MAX_AVG_VARIANCE = .2;
	/** @type {number} */
	hash.MAX_INDIVIDUAL_VARIANCE = .45;
	/** @type {number} */
	hash.MIN_FINDER_PATTERN_RATIO = 9.5 / 12;
	/** @type {number} */
	hash.MAX_FINDER_PATTERN_RATIO = 12.5 / 14;
	var Controller = function () {
		/**
		 * @param {!Object} options
		 * @param {string} element
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3(options, element) {
			_classCallCheck2(this, TempusDominusBootstrap3);
			/** @type {!Object} */
			this.value = options;
			/** @type {string} */
			this.checksumPortion = element;
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "getValue",
			value: function getUnifiedOperator() {
				return this.value;
			}
		}, {
			key: "getChecksumPortion",
			value: function getChecksumPortion() {
				return this.checksumPortion;
			}
		}, {
			key: "toString",
			value: function JSONValueNode() {
				return this.value + "(" + this.checksumPortion + ")";
			}
		}, {
			key: "equals",
			value: function sortByAlpha(b) {
				if (!(b instanceof TempusDominusBootstrap3)) {
					return false;
				}
				/** @type {!Object} */
				var other = b;
				return this.value === other.value && this.checksumPortion === other.checksumPortion;
			}
		}, {
			key: "hashCode",
			value: function JSONValueNode() {
				return this.value ^ this.checksumPortion;
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var Component = function () {
		/**
		 * @param {!Object} value
		 * @param {!Object} commands
		 * @param {?} options
		 * @param {?} context
		 * @param {?} data
		 * @return {undefined}
		 */
		function init(value, commands, options, context, data) {
			_classCallCheck2(this, init);
			/** @type {!Object} */
			this.value = value;
			/** @type {!Object} */
			this.startEnd = commands;
			/** @type {!Object} */
			this.value = value;
			/** @type {!Object} */
			this.startEnd = commands;
			/** @type {!Array} */
			this.resultPoints = new Array;
			this.resultPoints.push(new type(options, data));
			this.resultPoints.push(new type(context, data));
		}
		_createClass2(init, [{
			key: "getValue",
			value: function getUnifiedOperator() {
				return this.value;
			}
		}, {
			key: "getStartEnd",
			value: function getStartEnd() {
				return this.startEnd;
			}
		}, {
			key: "getResultPoints",
			value: function getResultPoints() {
				return this.resultPoints;
			}
		}, {
			key: "equals",
			value: function equals(src) {
				if (!(src instanceof init)) {
					return false;
				}
				/** @type {!Object} */
				var options = src;
				return this.value === options.value;
			}
		}, {
			key: "hashCode",
			value: function getUnifiedOperator() {
				return this.value;
			}
		}]);
		return init;
	}();
	var console = function () {
		/**
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3() {
			_classCallCheck2(this, TempusDominusBootstrap3);
		}
		_createClass2(TempusDominusBootstrap3, null, [{
			key: "getRSSvalue",
			value: function add(o, x, stage) {
				/** @type {number} */
				var n = 0;
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError18 = false;
				var _iteratorError18 = undefined;
				try {
					var _iterator3 = o[Symbol.iterator]();
					var $__1;
					for (; !(_iteratorNormalCompletion3 = ($__1 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var p = $__1.value;
						n = n + p;
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError18 = true;
					_iteratorError18 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError18) {
							throw _iteratorError18;
						}
					}
				}
				/** @type {number} */
				var sum = 0;
				/** @type {number} */
				var len = 0;
				var i = o.length;
				/** @type {number} */
				var j = 0;
				for (; j < i - 1; j++) {
					var t = void 0;
					/** @type {number} */
					t = 1;
					/** @type {number} */
					len = len | 1 << j;
					for (; t < o[j]; t++, len = len & ~(1 << j)) {
						var y = TempusDominusBootstrap3.combins(n - t - 1, i - j - 2);
						if (stage && 0 === len && n - t - (i - j - 1) >= i - j - 1 && (y = y - TempusDominusBootstrap3.combins(n - t - (i - j), i - j - 2)), i - j - 1 > 1) {
							/** @type {number} */
							var step = 0;
							/** @type {number} */
							var v = n - t - (i - j - 2);
							for (; v > x; v--) {
								step = step + TempusDominusBootstrap3.combins(n - t - v - 1, i - j - 3);
							}
							/** @type {number} */
							y = y - step * (i - 1 - j);
						} else {
							if (n - t > x) {
								y--;
							}
						}
						sum = sum + y;
					}
					/** @type {number} */
					n = n - t;
				}
				return sum;
			}
		}, {
			key: "combins",
			value: function compareAlphabetically(a, b) {
				var C = void 0;
				var r = void 0;
				if (a - b > b) {
					/** @type {number} */
					r = b;
					/** @type {number} */
					C = a - b;
				} else {
					/** @type {number} */
					r = a - b;
					/** @type {number} */
					C = b;
				}
				/** @type {number} */
				var relative_threshold = 1;
				/** @type {number} */
				var above_thresh_counter = 1;
				/** @type {number} */
				var relative_gate_factor = a;
				for (; relative_gate_factor > C; relative_gate_factor--) {
					/** @type {number} */
					relative_threshold = relative_threshold * relative_gate_factor;
					if (above_thresh_counter <= r) {
						/** @type {number} */
						relative_threshold = relative_threshold / above_thresh_counter;
						above_thresh_counter++;
					}
				}
				for (; above_thresh_counter <= r;) {
					/** @type {number} */
					relative_threshold = relative_threshold / above_thresh_counter;
					above_thresh_counter++;
				}
				return relative_threshold;
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var bt = function () {
		/**
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3() {
			_classCallCheck2(this, TempusDominusBootstrap3);
		}
		_createClass2(TempusDominusBootstrap3, null, [{
			key: "buildBitArray",
			value: function parse(string) {
				/** @type {number} */
				var idx_last = 2 * string.length - 1;
				if (null == string[string.length - 1].getRightChar()) {
					/** @type {number} */
					idx_last = idx_last - 1;
				}
				var result = new BitArray(12 * idx_last);
				/** @type {number} */
				var value = 0;
				var i = string[0].getRightChar().getValue();
				/** @type {number} */
				var j = 11;
				for (; j >= 0; --j) {
					if (0 != (i & 1 << j)) {
						result.set(value);
					}
					value++;
				}
				/** @type {number} */
				var index = 1;
				for (; index < string.length; ++index) {
					var char = string[index];
					var bitsShiftedOut = char.getLeftChar().getValue();
					/** @type {number} */
					var numBitsShift = 11;
					for (; numBitsShift >= 0; --numBitsShift) {
						if (0 != (bitsShiftedOut & 1 << numBitsShift)) {
							result.set(value);
						}
						value++;
					}
					if (null != char.getRightChar()) {
						var bitsShiftedOut = char.getRightChar().getValue();
						/** @type {number} */
						var numBitsShift = 11;
						for (; numBitsShift >= 0; --numBitsShift) {
							if (0 != (bitsShiftedOut & 1 << numBitsShift)) {
								result.set(value);
							}
							value++;
						}
					}
				}
				return result;
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var Promise = function () {
		/**
		 * @param {boolean} element
		 * @param {!AudioNode} options
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3(element, options) {
			_classCallCheck2(this, TempusDominusBootstrap3);
			if (options) {
				/** @type {null} */
				this.decodedInformation = null;
			} else {
				/** @type {boolean} */
				this.finished = element;
				/** @type {!AudioNode} */
				this.decodedInformation = options;
			}
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "getDecodedInformation",
			value: function getDecodedInformation() {
				return this.decodedInformation;
			}
		}, {
			key: "isFinished",
			value: function _isDealFinished() {
				return this.finished;
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var fetchFailure = function () {
		/**
		 * @param {?} options
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3(options) {
			_classCallCheck2(this, TempusDominusBootstrap3);
			this.newPosition = options;
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "getNewPosition",
			value: function getNewPosition() {
				return this.newPosition;
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var Geo = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @param {?} props
		 * @param {!Object} entries
		 * @return {?}
		 */
		function StarRatingComponent(props, entries) {
			var _this;
			_classCallCheck2(this, StarRatingComponent);
			_this = _possibleConstructorReturn(this, (StarRatingComponent.__proto__ || Object.getPrototypeOf(StarRatingComponent)).call(this, props));
			_this;
			/** @type {!Object} */
			_this.value = entries;
			return _this;
		}
		_inherits(StarRatingComponent, _WebInspector$GeneralTreeElement);
		_createClass2(StarRatingComponent, [{
			key: "getValue",
			value: function getUnifiedOperator() {
				return this.value;
			}
		}, {
			key: "isFNC1",
			value: function JSONValueNode() {
				return this.value === StarRatingComponent.FNC1;
			}
		}]);
		return StarRatingComponent;
	}(fetchFailure);
	/** @type {string} */
	Geo.FNC1 = "$";
	var Data = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @param {?} data
		 * @param {string} options
		 * @param {?} el
		 * @return {?}
		 */
		function Countdown(data, options, el) {
			var _this;
			_classCallCheck2(this, Countdown);
			_this = _possibleConstructorReturn(this, (Countdown.__proto__ || Object.getPrototypeOf(Countdown)).call(this, data));
			_this;
			if (el) {
				/** @type {boolean} */
				_this.remaining = true;
				_this.remainingValue = _this.remainingValue;
			} else {
				/** @type {boolean} */
				_this.remaining = false;
				/** @type {number} */
				_this.remainingValue = 0;
			}
			/** @type {string} */
			_this.newString = options;
			return _this;
		}
		_inherits(Countdown, _WebInspector$GeneralTreeElement);
		_createClass2(Countdown, [{
			key: "getNewString",
			value: function getNewString() {
				return this.newString;
			}
		}, {
			key: "isRemaining",
			value: function remainingtime() {
				return this.remaining;
			}
		}, {
			key: "getRemainingValue",
			value: function getRemainingValue() {
				return this.remainingValue;
			}
		}]);
		return Countdown;
	}(fetchFailure);
	var error = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @param {?} name
		 * @param {number} data
		 * @param {number} server
		 * @return {?}
		 */
		function Obj(name, data, server) {
			var _this;
			_classCallCheck2(this, Obj);
			if (_this = _possibleConstructorReturn(this, (Obj.__proto__ || Object.getPrototypeOf(Obj)).call(this, name)), _this, data < 0 || data > 10 || server < 0 || server > 10) {
				throw new Date;
			}
			/** @type {number} */
			_this.firstDigit = data;
			/** @type {number} */
			_this.secondDigit = server;
			return _possibleConstructorReturn(_this);
		}
		_inherits(Obj, _WebInspector$GeneralTreeElement);
		_createClass2(Obj, [{
			key: "getFirstDigit",
			value: function getFirstDigit() {
				return this.firstDigit;
			}
		}, {
			key: "getSecondDigit",
			value: function getSecondDigit() {
				return this.secondDigit;
			}
		}, {
			key: "getValue",
			value: function PHPNumber() {
				return 10 * this.firstDigit + this.secondDigit;
			}
		}, {
			key: "isFirstDigitFNC1",
			value: function PHPNumber() {
				return this.firstDigit === Obj.FNC1;
			}
		}, {
			key: "isSecondDigitFNC1",
			value: function isSecondDigitFNC1() {
				return this.secondDigit === Obj.FNC1;
			}
		}, {
			key: "isAnyFNC1",
			value: function PHPNumber() {
				return this.firstDigit === Obj.FNC1 || this.secondDigit === Obj.FNC1;
			}
		}]);
		return Obj;
	}(fetchFailure);
	/** @type {number} */
	error.FNC1 = 10;
	var kt = function () {
		/**
		 * @return {undefined}
		 */
		function node() {
			_classCallCheck2(this, node);
		}
		_createClass2(node, null, [{
			key: "parseFieldsInGeneralPurpose",
			value: function getClauses(uuid) {
				if (!uuid) {
					return null;
				}
				if (uuid.length < 2) {
					throw new TypeError;
				}
				var left = uuid.substring(0, 2);
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError19 = false;
				var _iteratorError19 = undefined;
				try {
					var _iterator3 = node.TWO_DIGIT_DATA_LENGTH[Symbol.iterator]();
					var _step3;
					for (; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var name = _step3.value;
						if (name[0] === left) {
							return name[1] === node.VARIABLE_LENGTH ? node.processVariableAI(2, name[2], uuid) : node.processFixedAI(2, name[1], uuid);
						}
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError19 = true;
					_iteratorError19 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError19) {
							throw _iteratorError19;
						}
					}
				}
				if (uuid.length < 3) {
					throw new TypeError;
				}
				var o = uuid.substring(0, 3);
				/** @type {boolean} */
				var _iteratorNormalCompletion4 = true;
				/** @type {boolean} */
				var _didIteratorError20 = false;
				var referenceNameDecl = undefined;
				try {
					var _iterator4 = node.THREE_DIGIT_DATA_LENGTH[Symbol.iterator]();
					var _step3;
					for (; !(_iteratorNormalCompletion4 = (_step3 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
						var name = _step3.value;
						if (name[0] === o) {
							return name[1] === node.VARIABLE_LENGTH ? node.processVariableAI(3, name[2], uuid) : node.processFixedAI(3, name[1], uuid);
						}
					}
				} catch (undefined) {
					/** @type {boolean} */
					_didIteratorError20 = true;
					referenceNameDecl = undefined;
				} finally {
					try {
						if (!_iteratorNormalCompletion4 && _iterator4.return) {
							_iterator4.return();
						}
					} finally {
						if (_didIteratorError20) {
							throw referenceNameDecl;
						}
					}
				}
				/** @type {boolean} */
				var _iteratorNormalCompletion = true;
				/** @type {boolean} */
				var _didIteratorError21 = false;
				var activeCommandI = undefined;
				try {
					var _iterator = node.THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH[Symbol.iterator]();
					var _step3;
					for (; !(_iteratorNormalCompletion = (_step3 = _iterator.next()).done); _iteratorNormalCompletion = true) {
						var name = _step3.value;
						if (name[0] === o) {
							return name[1] === node.VARIABLE_LENGTH ? node.processVariableAI(4, name[2], uuid) : node.processFixedAI(4, name[1], uuid);
						}
					}
				} catch (undefined) {
					/** @type {boolean} */
					_didIteratorError21 = true;
					activeCommandI = undefined;
				} finally {
					try {
						if (!_iteratorNormalCompletion && _iterator.return) {
							_iterator.return();
						}
					} finally {
						if (_didIteratorError21) {
							throw activeCommandI;
						}
					}
				}
				if (uuid.length < 4) {
					throw new TypeError;
				}
				var n = uuid.substring(0, 4);
				/** @type {boolean} */
				var _iteratorNormalCompletion2 = true;
				/** @type {boolean} */
				var _didIteratorError22 = false;
				var _iteratorError17 = undefined;
				try {
					var _iterator2 = node.FOUR_DIGIT_DATA_LENGTH[Symbol.iterator]();
					var _step3;
					for (; !(_iteratorNormalCompletion2 = (_step3 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
						var name = _step3.value;
						if (name[0] === n) {
							return name[1] === node.VARIABLE_LENGTH ? node.processVariableAI(4, name[2], uuid) : node.processFixedAI(4, name[1], uuid);
						}
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError22 = true;
					_iteratorError17 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion2 && _iterator2.return) {
							_iterator2.return();
						}
					} finally {
						if (_didIteratorError22) {
							throw _iteratorError17;
						}
					}
				}
				throw new TypeError;
			}
		}, {
			key: "processFixedAI",
			value: function QueryStringParser$placeNestedValue(l, i, data) {
				if (data.length < l) {
					throw new TypeError;
				}
				var output = data.substring(0, l);
				if (data.length < l + i) {
					throw new TypeError;
				}
				var top = data.substring(l, l + i);
				var iconNode = data.substring(l + i);
				/** @type {string} */
				var y = "(" + output + ")" + top;
				var x = node.parseFieldsInGeneralPurpose(iconNode);
				return null == x ? y : y + x;
			}
		}, {
			key: "processVariableAI",
			value: function ARPPacket(offset, length, buffer) {
				var n = void 0;
				var stretch = buffer.substring(0, offset);
				n = buffer.length < offset + length ? buffer.length : offset + length;
				var v = buffer.substring(offset, n);
				var label = buffer.substring(n);
				/** @type {string} */
				var i = "(" + stretch + ")" + v;
				var l = node.parseFieldsInGeneralPurpose(label);
				return null == l ? i : i + l;
			}
		}]);
		return node;
	}();
	/** @type {!Array} */
	kt.VARIABLE_LENGTH = [];
	/** @type {!Array} */
	kt.TWO_DIGIT_DATA_LENGTH = [["00", 18], ["01", 14], ["02", 14], ["10", kt.VARIABLE_LENGTH, 20], ["11", 6], ["12", 6], ["13", 6], ["15", 6], ["17", 6], ["20", 2], ["21", kt.VARIABLE_LENGTH, 20], ["22", kt.VARIABLE_LENGTH, 29], ["30", kt.VARIABLE_LENGTH, 8], ["37", kt.VARIABLE_LENGTH, 8], ["90", kt.VARIABLE_LENGTH, 30], ["91", kt.VARIABLE_LENGTH, 30], ["92", kt.VARIABLE_LENGTH, 30], ["93", kt.VARIABLE_LENGTH, 30], ["94", kt.VARIABLE_LENGTH, 30], ["95", kt.VARIABLE_LENGTH, 30], ["96", kt.VARIABLE_LENGTH,
		30], ["97", kt.VARIABLE_LENGTH, 3], ["98", kt.VARIABLE_LENGTH, 30], ["99", kt.VARIABLE_LENGTH, 30]];
	/** @type {!Array} */
	kt.THREE_DIGIT_DATA_LENGTH = [["240", kt.VARIABLE_LENGTH, 30], ["241", kt.VARIABLE_LENGTH, 30], ["242", kt.VARIABLE_LENGTH, 6], ["250", kt.VARIABLE_LENGTH, 30], ["251", kt.VARIABLE_LENGTH, 30], ["253", kt.VARIABLE_LENGTH, 17], ["254", kt.VARIABLE_LENGTH, 20], ["400", kt.VARIABLE_LENGTH, 30], ["401", kt.VARIABLE_LENGTH, 30], ["402", 17], ["403", kt.VARIABLE_LENGTH, 30], ["410", 13], ["411", 13], ["412", 13], ["413", 13], ["414", 13], ["420", kt.VARIABLE_LENGTH, 20], ["421", kt.VARIABLE_LENGTH, 15],
	["422", 3], ["423", kt.VARIABLE_LENGTH, 15], ["424", 3], ["425", 3], ["426", 3]];
	/** @type {!Array} */
	kt.THREE_DIGIT_PLUS_DIGIT_DATA_LENGTH = [["310", 6], ["311", 6], ["312", 6], ["313", 6], ["314", 6], ["315", 6], ["316", 6], ["320", 6], ["321", 6], ["322", 6], ["323", 6], ["324", 6], ["325", 6], ["326", 6], ["327", 6], ["328", 6], ["329", 6], ["330", 6], ["331", 6], ["332", 6], ["333", 6], ["334", 6], ["335", 6], ["336", 6], ["340", 6], ["341", 6], ["342", 6], ["343", 6], ["344", 6], ["345", 6], ["346", 6], ["347", 6], ["348", 6], ["349", 6], ["350", 6], ["351", 6], ["352", 6], ["353", 6], ["354",
		6], ["355", 6], ["356", 6], ["357", 6], ["360", 6], ["361", 6], ["362", 6], ["363", 6], ["364", 6], ["365", 6], ["366", 6], ["367", 6], ["368", 6], ["369", 6], ["390", kt.VARIABLE_LENGTH, 15], ["391", kt.VARIABLE_LENGTH, 18], ["392", kt.VARIABLE_LENGTH, 15], ["393", kt.VARIABLE_LENGTH, 18], ["703", kt.VARIABLE_LENGTH, 30]];
	/** @type {!Array} */
	kt.FOUR_DIGIT_DATA_LENGTH = [["7001", 13], ["7002", kt.VARIABLE_LENGTH, 30], ["7003", 10], ["8001", 14], ["8002", kt.VARIABLE_LENGTH, 20], ["8003", kt.VARIABLE_LENGTH, 30], ["8004", kt.VARIABLE_LENGTH, 30], ["8005", 6], ["8006", 18], ["8007", kt.VARIABLE_LENGTH, 30], ["8008", kt.VARIABLE_LENGTH, 12], ["8018", 18], ["8020", kt.VARIABLE_LENGTH, 25], ["8100", 6], ["8101", 10], ["8102", 2], ["8110", kt.VARIABLE_LENGTH, 70], ["8200", kt.VARIABLE_LENGTH, 70]];
	var ObjectOperation = function () {
		/**
		 * @param {!Object} options
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3(options) {
			_classCallCheck2(this, TempusDominusBootstrap3);
			this.buffer = new Buffer;
			/** @type {!Object} */
			this.information = options;
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "decodeAllCodes",
			value: function get_stats_box(stats, type_of_averaging) {
				var fingerprintPrefixLen = type_of_averaging;
				/** @type {null} */
				var qIdx = null;
				for (; ;) {
					var base = this.decodeGeneralPurposeField(fingerprintPrefixLen, qIdx);
					var style = kt.parseFieldsInGeneralPurpose(base.getNewString());
					if (null != style && stats.append(style), qIdx = base.isRemaining() ? "" + base.getRemainingValue() : null, fingerprintPrefixLen === base.getNewPosition()) {
						break;
					}
					fingerprintPrefixLen = base.getNewPosition();
				}
				return stats.toString();
			}
		}, {
			key: "isStillNumeric",
			value: function isAlive(key) {
				if (key + 7 > this.information.getSize()) {
					return key + 4 <= this.information.getSize();
				}
				/** @type {number} */
				var SETTINGS_KEY = key;
				for (; SETTINGS_KEY < key + 3; ++SETTINGS_KEY) {
					if (this.information.get(SETTINGS_KEY)) {
						return true;
					}
				}
				return this.information.get(key + 3);
			}
		}, {
			key: "decodeNumeric",
			value: function score(val) {
				if (val + 7 > this.information.getSize()) {
					var number = this.extractNumericValueFromBitArray(val, 4);
					return new error(this.information.getSize(), 0 === number ? error.FNC1 : number - 1, error.FNC1);
				}
				var kinks = this.extractNumericValueFromBitArray(val, 7);
				return new error(val + 7, (kinks - 8) / 11, (kinks - 8) % 11);
			}
		}, {
			key: "extractNumericValueFromBitArray",
			value: function $get(mmCoreSplitViewBlock, $state) {
				return TempusDominusBootstrap3.extractNumericValueFromBitArray(this.information, mmCoreSplitViewBlock, $state);
			}
		}, {
			key: "decodeGeneralPurposeField",
			value: function init(context, data) {
				this.buffer.setLengthToZero();
				if (null != data) {
					this.buffer.append(data);
				}
				this.current.setPosition(context);
				var r = this.parseBlocks();
				return null != r && r.isRemaining() ? new Data(this.current.getPosition(), this.buffer.toString(), r.getRemainingValue()) : new Data(this.current.getPosition(), this.buffer.toString());
			}
		}, {
			key: "parseBlocks",
			value: function add() {
				var t = void 0;
				var e = void 0;
				do {
					var r = this.current.getPosition();
					if (t = this.current.isAlpha() ? (e = this.parseAlphaBlock()).isFinished() : this.current.isIsoIec646() ? (e = this.parseIsoIec646Block()).isFinished() : (e = this.parseNumericBlock()).isFinished(), !(r !== this.current.getPosition()) && !t) {
						break;
					}
				} while (!t);
				return e.getDecodedInformation();
			}
		}, {
			key: "parseNumericBlock",
			value: function update() {
				for (; this.isStillNumeric(this.current.getPosition());) {
					var base = this.decodeNumeric(this.current.getPosition());
					if (this.current.setPosition(base.getNewPosition()), base.isFirstDigitFNC1()) {
						var rej = void 0;
						return rej = base.isSecondDigitFNC1() ? new Data(this.current.getPosition(), this.buffer.toString()) : new Data(this.current.getPosition(), this.buffer.toString(), base.getSecondDigit()), new Promise(true, rej);
					}
					if (this.buffer.append(base.getFirstDigit()), base.isSecondDigitFNC1()) {
						var data = new Data(this.current.getPosition(), this.buffer.toString());
						return new Promise(true, data);
					}
					this.buffer.append(base.getSecondDigit());
				}
				return this.isNumericToAlphaNumericLatch(this.current.getPosition()) && (this.current.setAlpha(), this.current.incrementPosition(4)), new Promise(false);
			}
		}, {
			key: "parseIsoIec646Block",
			value: function update() {
				for (; this.isStillIsoIec646(this.current.getPosition());) {
					var base = this.decodeIsoIec646(this.current.getPosition());
					if (this.current.setPosition(base.getNewPosition()), base.isFNC1()) {
						var data = new Data(this.current.getPosition(), this.buffer.toString());
						return new Promise(true, data);
					}
					this.buffer.append(base.getValue());
				}
				return this.isAlphaOr646ToNumericLatch(this.current.getPosition()) ? (this.current.incrementPosition(3), this.current.setNumeric()) : this.isAlphaTo646ToAlphaLatch(this.current.getPosition()) && (this.current.getPosition() + 5 < this.information.getSize() ? this.current.incrementPosition(5) : this.current.setPosition(this.information.getSize()), this.current.setAlpha()), new Promise(false);
			}
		}, {
			key: "parseAlphaBlock",
			value: function init() {
				for (; this.isStillAlpha(this.current.getPosition());) {
					var base = this.decodeAlphanumeric(this.current.getPosition());
					if (this.current.setPosition(base.getNewPosition()), base.isFNC1()) {
						var data = new Data(this.current.getPosition(), this.buffer.toString());
						return new Promise(true, data);
					}
					this.buffer.append(base.getValue());
				}
				return this.isAlphaOr646ToNumericLatch(this.current.getPosition()) ? (this.current.incrementPosition(3), this.current.setNumeric()) : this.isAlphaTo646ToAlphaLatch(this.current.getPosition()) && (this.current.getPosition() + 5 < this.information.getSize() ? this.current.incrementPosition(5) : this.current.setPosition(this.information.getSize()), this.current.setIsoIec646()), new Promise(false);
			}
		}, {
			key: "isStillIsoIec646",
			value: function isAlive(f) {
				if (f + 5 > this.information.getSize()) {
					return false;
				}
				var s = this.extractNumericValueFromBitArray(f, 5);
				if (s >= 5 && s < 16) {
					return true;
				}
				if (f + 7 > this.information.getSize()) {
					return false;
				}
				var version = this.extractNumericValueFromBitArray(f, 7);
				if (version >= 64 && version < 116) {
					return true;
				}
				if (f + 8 > this.information.getSize()) {
					return false;
				}
				var offset = this.extractNumericValueFromBitArray(f, 8);
				return offset >= 232 && offset < 253;
			}
		}, {
			key: "decodeIsoIec646",
			value: function injectDirectionProxy(name) {
				var s = this.extractNumericValueFromBitArray(name, 5);
				if (15 === s) {
					return new Geo(name + 5, Geo.FNC1);
				}
				if (s >= 5 && s < 15) {
					return new Geo(name + 5, "0" + (s - 5));
				}
				var dateDelim = void 0;
				var temp = this.extractNumericValueFromBitArray(name, 7);
				if (temp >= 64 && temp < 90) {
					return new Geo(name + 7, "" + (temp + 1));
				}
				if (temp >= 90 && temp < 116) {
					return new Geo(name + 7, "" + (temp + 7));
				}
				switch (this.extractNumericValueFromBitArray(name, 8)) {
					case 232:
						/** @type {string} */
						dateDelim = "!";
						break;
					case 233:
						/** @type {string} */
						dateDelim = '"';
						break;
					case 234:
						/** @type {string} */
						dateDelim = "%";
						break;
					case 235:
						/** @type {string} */
						dateDelim = "&";
						break;
					case 236:
						/** @type {string} */
						dateDelim = "'";
						break;
					case 237:
						/** @type {string} */
						dateDelim = "(";
						break;
					case 238:
						/** @type {string} */
						dateDelim = ")";
						break;
					case 239:
						/** @type {string} */
						dateDelim = "*";
						break;
					case 240:
						/** @type {string} */
						dateDelim = "+";
						break;
					case 241:
						/** @type {string} */
						dateDelim = ",";
						break;
					case 242:
						/** @type {string} */
						dateDelim = "-";
						break;
					case 243:
						/** @type {string} */
						dateDelim = ".";
						break;
					case 244:
						/** @type {string} */
						dateDelim = "/";
						break;
					case 245:
						/** @type {string} */
						dateDelim = ":";
						break;
					case 246:
						/** @type {string} */
						dateDelim = ";";
						break;
					case 247:
						/** @type {string} */
						dateDelim = "<";
						break;
					case 248:
						/** @type {string} */
						dateDelim = "=";
						break;
					case 249:
						/** @type {string} */
						dateDelim = ">";
						break;
					case 250:
						/** @type {string} */
						dateDelim = "?";
						break;
					case 251:
						/** @type {string} */
						dateDelim = "_";
						break;
					case 252:
						/** @type {string} */
						dateDelim = " ";
						break;
					default:
						throw new Date;
				}
				return new Geo(name + 8, dateDelim);
			}
		}, {
			key: "isStillAlpha",
			value: function isAlive(f) {
				if (f + 5 > this.information.getSize()) {
					return false;
				}
				var s = this.extractNumericValueFromBitArray(f, 5);
				if (s >= 5 && s < 16) {
					return true;
				}
				if (f + 6 > this.information.getSize()) {
					return false;
				}
				var version = this.extractNumericValueFromBitArray(f, 6);
				return version >= 16 && version < 63;
			}
		}, {
			key: "decodeAlphanumeric",
			value: function getPath(name) {
				var s = this.extractNumericValueFromBitArray(name, 5);
				if (15 === s) {
					return new Geo(name + 5, Geo.FNC1);
				}
				if (s >= 5 && s < 15) {
					return new Geo(name + 5, "0" + (s - 5));
				}
				var dateDelim = void 0;
				var i = this.extractNumericValueFromBitArray(name, 6);
				if (i >= 32 && i < 58) {
					return new Geo(name + 6, "" + (i + 33));
				}
				switch (i) {
					case 58:
						/** @type {string} */
						dateDelim = "*";
						break;
					case 59:
						/** @type {string} */
						dateDelim = ",";
						break;
					case 60:
						/** @type {string} */
						dateDelim = "-";
						break;
					case 61:
						/** @type {string} */
						dateDelim = ".";
						break;
					case 62:
						/** @type {string} */
						dateDelim = "/";
						break;
					default:
						throw new Path("Decoding invalid alphanumeric value: " + i);
				}
				return new Geo(name + 6, dateDelim);
			}
		}, {
			key: "isAlphaTo646ToAlphaLatch",
			value: function isAlive(key) {
				if (key + 1 > this.information.getSize()) {
					return false;
				}
				/** @type {number} */
				var path = 0;
				for (; path < 5 && path + key < this.information.getSize(); ++path) {
					if (2 === path) {
						if (!this.information.get(key + 2)) {
							return false;
						}
					} else {
						if (this.information.get(key + path)) {
							return false;
						}
					}
				}
				return true;
			}
		}, {
			key: "isAlphaOr646ToNumericLatch",
			value: function isAlive(object) {
				if (object + 3 > this.information.getSize()) {
					return false;
				}
				/** @type {number} */
				var target = object;
				for (; target < object + 3; ++target) {
					if (this.information.get(target)) {
						return false;
					}
				}
				return true;
			}
		}, {
			key: "isNumericToAlphaNumericLatch",
			value: function isAlive(key) {
				if (key + 1 > this.information.getSize()) {
					return false;
				}
				/** @type {number} */
				var path = 0;
				for (; path < 4 && path + key < this.information.getSize(); ++path) {
					if (this.information.get(key + path)) {
						return false;
					}
				}
				return true;
			}
		}], [{
			key: "extractNumericValueFromBitArray",
			value: function setup(schema, name, repeatedDigitLen) {
				/** @type {number} */
				var res = 0;
				/** @type {number} */
				var i = 0;
				for (; i < repeatedDigitLen; ++i) {
					if (schema.get(name + i)) {
						/** @type {number} */
						res = res | 1 << repeatedDigitLen - i - 1;
					}
				}
				return res;
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var level = function () {
		/**
		 * @param {!Object} data
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3(data) {
			_classCallCheck2(this, TempusDominusBootstrap3);
			/** @type {!Object} */
			this.information = data;
			this.generalDecoder = new ObjectOperation(data);
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "getInformation",
			value: function getInformation() {
				return this.information;
			}
		}, {
			key: "getGeneralDecoder",
			value: function getGeneralDecoder() {
				return this.generalDecoder;
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var field = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @param {?} props
		 * @return {?}
		 */
		function RemoveStyleControls(props) {
			_classCallCheck2(this, RemoveStyleControls);
			return _possibleConstructorReturn(this, (RemoveStyleControls.__proto__ || Object.getPrototypeOf(RemoveStyleControls)).call(this, props));
		}
		_inherits(RemoveStyleControls, _WebInspector$GeneralTreeElement);
		_createClass2(RemoveStyleControls, [{
			key: "encodeCompressedGtin",
			value: function handleClick(e, btn) {
				e.append("(01)");
				var artistTrack = e.length();
				e.append("9");
				this.encodeCompressedGtinWithoutAI(e, btn, artistTrack);
			}
		}, {
			key: "encodeCompressedGtinWithoutAI",
			value: function setup(levelManager, options, fieldsConfig) {
				/** @type {number} */
				var curZoom = 0;
				for (; curZoom < 4; ++curZoom) {
					var $afterBags = this.getGeneralDecoder().extractNumericValueFromBitArray(options + 10 * curZoom, 10);
					if ($afterBags / 100 == 0) {
						levelManager.append("0");
					}
					if ($afterBags / 10 == 0) {
						levelManager.append("0");
					}
					levelManager.append($afterBags);
				}
				RemoveStyleControls.appendCheckDigit(levelManager, fieldsConfig);
			}
		}], [{
			key: "appendCheckDigit",
			value: function ss$indexOfAnyString(s, count) {
				/** @type {number} */
				var r = 0;
				/** @type {number} */
				var index = 0;
				for (; index < 13; index++) {
					/** @type {number} */
					var H = s.charAt(index + count).charCodeAt(0) - "0".charCodeAt(0);
					/** @type {number} */
					r = r + (0 == (1 & index) ? 3 * H : H);
				}
				if (10 === (r = 10 - r % 10)) {
					/** @type {number} */
					r = 0;
				}
				s.append(r);
			}
		}]);
		return RemoveStyleControls;
	}(level);
	/** @type {number} */
	field.GTIN_SIZE = 40;
	var m = function (parent) {
		/**
		 * @param {?} props
		 * @return {?}
		 */
		function RemoveStyleControls(props) {
			_classCallCheck2(this, RemoveStyleControls);
			return _possibleConstructorReturn(this, (RemoveStyleControls.__proto__ || Object.getPrototypeOf(RemoveStyleControls)).call(this, props));
		}
		_inherits(RemoveStyleControls, parent);
		_createClass2(RemoveStyleControls, [{
			key: "parseInformation",
			value: function getTextFromArrayBuffer() {
				var e = new Buffer;
				e.append("(01)");
				var lastviewmatrix = e.length();
				var table = this.getGeneralDecoder().extractNumericValueFromBitArray(RemoveStyleControls.HEADER_SIZE, 4);
				return e.append(table), this.encodeCompressedGtinWithoutAI(e, RemoveStyleControls.HEADER_SIZE + 4, lastviewmatrix), this.getGeneralDecoder().decodeAllCodes(e, RemoveStyleControls.HEADER_SIZE + 44);
			}
		}]);
		return RemoveStyleControls;
	}(field);
	/** @type {number} */
	m.HEADER_SIZE = 4;
	var adjustedLevel = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @param {?} props
		 * @return {?}
		 */
		function RemoveStyleControls(props) {
			_classCallCheck2(this, RemoveStyleControls);
			return _possibleConstructorReturn(this, (RemoveStyleControls.__proto__ || Object.getPrototypeOf(RemoveStyleControls)).call(this, props));
		}
		_inherits(RemoveStyleControls, _WebInspector$GeneralTreeElement);
		_createClass2(RemoveStyleControls, [{
			key: "parseInformation",
			value: function camOn() {
				var InventoryBuffer = new Buffer;
				return this.getGeneralDecoder().decodeAllCodes(InventoryBuffer, RemoveStyleControls.HEADER_SIZE);
			}
		}]);
		return RemoveStyleControls;
	}(level);
	/** @type {number} */
	adjustedLevel.HEADER_SIZE = 5;
	var realField = function (parent) {
		/**
		 * @param {?} props
		 * @return {?}
		 */
		function RemoveStyleControls(props) {
			_classCallCheck2(this, RemoveStyleControls);
			return _possibleConstructorReturn(this, (RemoveStyleControls.__proto__ || Object.getPrototypeOf(RemoveStyleControls)).call(this, props));
		}
		_inherits(RemoveStyleControls, parent);
		_createClass2(RemoveStyleControls, [{
			key: "encodeCompressedWeight",
			value: function checkFont(style, font, data) {
				var result = this.getGeneralDecoder().extractNumericValueFromBitArray(font, data);
				this.addWeightCode(style, result);
				var value = this.checkWeight(result);
				/** @type {number} */
				var position = 1e5;
				/** @type {number} */
				var _e73 = 0;
				for (; _e73 < 5; ++_e73) {
					if (value / position == 0) {
						style.append("0");
					}
					/** @type {number} */
					position = position / 10;
				}
				style.append(value);
			}
		}]);
		return RemoveStyleControls;
	}(field);
	var magnifier = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @param {?} props
		 * @return {?}
		 */
		function RemoveStyleControls(props) {
			_classCallCheck2(this, RemoveStyleControls);
			return _possibleConstructorReturn(this, (RemoveStyleControls.__proto__ || Object.getPrototypeOf(RemoveStyleControls)).call(this, props));
		}
		_inherits(RemoveStyleControls, _WebInspector$GeneralTreeElement);
		_createClass2(RemoveStyleControls, [{
			key: "parseInformation",
			value: function loadSync() {
				if (this.getInformation().getSize() != RemoveStyleControls.HEADER_SIZE + realField.GTIN_SIZE + RemoveStyleControls.WEIGHT_SIZE) {
					throw new TypeError;
				}
				var t = new Buffer;
				return this.encodeCompressedGtin(t, RemoveStyleControls.HEADER_SIZE), this.encodeCompressedWeight(t, RemoveStyleControls.HEADER_SIZE + realField.GTIN_SIZE, RemoveStyleControls.WEIGHT_SIZE), t.toString();
			}
		}]);
		return RemoveStyleControls;
	}(realField);
	/** @type {number} */
	magnifier.HEADER_SIZE = 5;
	/** @type {number} */
	magnifier.WEIGHT_SIZE = 15;
	var $magnifier = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @param {?} props
		 * @return {?}
		 */
		function RemoveStyleControls(props) {
			_classCallCheck2(this, RemoveStyleControls);
			return _possibleConstructorReturn(this, (RemoveStyleControls.__proto__ || Object.getPrototypeOf(RemoveStyleControls)).call(this, props));
		}
		_inherits(RemoveStyleControls, _WebInspector$GeneralTreeElement);
		_createClass2(RemoveStyleControls, [{
			key: "addWeightCode",
			value: function _toggleDeleteReady($itemElement, readyToDelete) {
				$itemElement.append("(3103)");
			}
		}, {
			key: "checkWeight",
			value: function parseCustomUrl(url) {
				return url;
			}
		}]);
		return RemoveStyleControls;
	}(magnifier);
	var ReactiveLoop = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @param {?} props
		 * @return {?}
		 */
		function RemoveStyleControls(props) {
			_classCallCheck2(this, RemoveStyleControls);
			return _possibleConstructorReturn(this, (RemoveStyleControls.__proto__ || Object.getPrototypeOf(RemoveStyleControls)).call(this, props));
		}
		_inherits(RemoveStyleControls, _WebInspector$GeneralTreeElement);
		_createClass2(RemoveStyleControls, [{
			key: "addWeightCode",
			value: function _toggleDeleteReady($itemElement, readyToDelete) {
				if (readyToDelete < 1e4) {
					$itemElement.append("(3202)");
				} else {
					$itemElement.append("(3203)");
				}
			}
		}, {
			key: "checkWeight",
			value: function justinImageSize(width) {
				return width < 1e4 ? width : width - 1e4;
			}
		}]);
		return RemoveStyleControls;
	}(magnifier);
	var string = function (parent) {
		/**
		 * @param {?} props
		 * @return {?}
		 */
		function RemoveStyleControls(props) {
			_classCallCheck2(this, RemoveStyleControls);
			return _possibleConstructorReturn(this, (RemoveStyleControls.__proto__ || Object.getPrototypeOf(RemoveStyleControls)).call(this, props));
		}
		_inherits(RemoveStyleControls, parent);
		_createClass2(RemoveStyleControls, [{
			key: "parseInformation",
			value: function getTextFromArrayBuffer() {
				if (this.getInformation().getSize() < RemoveStyleControls.HEADER_SIZE + field.GTIN_SIZE) {
					throw new TypeError;
				}
				var t = new Buffer;
				this.encodeCompressedGtin(t, RemoveStyleControls.HEADER_SIZE);
				var e = this.getGeneralDecoder().extractNumericValueFromBitArray(RemoveStyleControls.HEADER_SIZE + field.GTIN_SIZE, RemoveStyleControls.LAST_DIGIT_SIZE);
				t.append("(392");
				t.append(e);
				t.append(")");
				var r = this.getGeneralDecoder().decodeGeneralPurposeField(RemoveStyleControls.HEADER_SIZE + field.GTIN_SIZE + RemoveStyleControls.LAST_DIGIT_SIZE, null);
				return t.append(r.getNewString()), t.toString();
			}
		}]);
		return RemoveStyleControls;
	}(field);
	/** @type {number} */
	string.HEADER_SIZE = 8;
	/** @type {number} */
	string.LAST_DIGIT_SIZE = 2;
	var Props = function (parent) {
		/**
		 * @param {?} props
		 * @return {?}
		 */
		function RemoveStyleControls(props) {
			_classCallCheck2(this, RemoveStyleControls);
			return _possibleConstructorReturn(this, (RemoveStyleControls.__proto__ || Object.getPrototypeOf(RemoveStyleControls)).call(this, props));
		}
		_inherits(RemoveStyleControls, parent);
		_createClass2(RemoveStyleControls, [{
			key: "parseInformation",
			value: function getTextFromArrayBuffer() {
				if (this.getInformation().getSize() < RemoveStyleControls.HEADER_SIZE + field.GTIN_SIZE) {
					throw new TypeError;
				}
				var t = new Buffer;
				this.encodeCompressedGtin(t, RemoveStyleControls.HEADER_SIZE);
				var e = this.getGeneralDecoder().extractNumericValueFromBitArray(RemoveStyleControls.HEADER_SIZE + field.GTIN_SIZE, RemoveStyleControls.LAST_DIGIT_SIZE);
				t.append("(393");
				t.append(e);
				t.append(")");
				var r = this.getGeneralDecoder().extractNumericValueFromBitArray(RemoveStyleControls.HEADER_SIZE + field.GTIN_SIZE + RemoveStyleControls.LAST_DIGIT_SIZE, RemoveStyleControls.FIRST_THREE_DIGITS_SIZE);
				if (r / 100 == 0) {
					t.append("0");
				}
				if (r / 10 == 0) {
					t.append("0");
				}
				t.append(r);
				var n = this.getGeneralDecoder().decodeGeneralPurposeField(RemoveStyleControls.HEADER_SIZE + field.GTIN_SIZE + RemoveStyleControls.LAST_DIGIT_SIZE + RemoveStyleControls.FIRST_THREE_DIGITS_SIZE, null);
				return t.append(n.getNewString()), t.toString();
			}
		}]);
		return RemoveStyleControls;
	}(field);
	/** @type {number} */
	Props.HEADER_SIZE = 8;
	/** @type {number} */
	Props.LAST_DIGIT_SIZE = 2;
	/** @type {number} */
	Props.FIRST_THREE_DIGITS_SIZE = 10;
	var C = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @param {?} props
		 * @param {?} mode
		 * @param {?} options
		 * @return {?}
		 */
		function TacoTableCell(props, mode, options) {
			var _this;
			_classCallCheck2(this, TacoTableCell);
			_this = _possibleConstructorReturn(this, (TacoTableCell.__proto__ || Object.getPrototypeOf(TacoTableCell)).call(this, props));
			_this;
			_this.dateCode = options;
			_this.firstAIdigits = mode;
			return _this;
		}
		_inherits(TacoTableCell, _WebInspector$GeneralTreeElement);
		_createClass2(TacoTableCell, [{
			key: "parseInformation",
			value: function loadSync() {
				if (this.getInformation().getSize() != TacoTableCell.HEADER_SIZE + TacoTableCell.GTIN_SIZE + TacoTableCell.WEIGHT_SIZE + TacoTableCell.DATE_SIZE) {
					throw new TypeError;
				}
				var t = new Buffer;
				return this.encodeCompressedGtin(t, TacoTableCell.HEADER_SIZE), this.encodeCompressedWeight(t, TacoTableCell.HEADER_SIZE + TacoTableCell.GTIN_SIZE, TacoTableCell.WEIGHT_SIZE), this.encodeCompressedDate(t, TacoTableCell.HEADER_SIZE + TacoTableCell.GTIN_SIZE + TacoTableCell.WEIGHT_SIZE), t.toString();
			}
		}, {
			key: "encodeCompressedDate",
			value: function userToGroup(group, user) {
				var i = this.getGeneralDecoder().extractNumericValueFromBitArray(user, TacoTableCell.DATE_SIZE);
				if (38400 == i) {
					return;
				}
				group.append("(");
				group.append(this.dateCode);
				group.append(")");
				/** @type {number} */
				var number = i % 32;
				/** @type {number} */
				var groupBtn = (i = i / 32) % 12 + 1;
				var start = i = i / 12;
				if (start / 10 == 0) {
					group.append("0");
				}
				group.append(start);
				if (groupBtn / 10 == 0) {
					group.append("0");
				}
				group.append(groupBtn);
				if (number / 10 == 0) {
					group.append("0");
				}
				group.append(number);
			}
		}, {
			key: "addWeightCode",
			value: function _toggleDeleteReady($itemElement, readyToDelete) {
				$itemElement.append("(");
				$itemElement.append(this.firstAIdigits);
				$itemElement.append(readyToDelete / 1e5);
				$itemElement.append(")");
			}
		}, {
			key: "checkWeight",
			value: function prefetchGroupsInfo(canCreateDiscussions) {
				return canCreateDiscussions % 1e5;
			}
		}]);
		return TacoTableCell;
	}(realField);
	/** @type {number} */
	C.HEADER_SIZE = 8;
	/** @type {number} */
	C.WEIGHT_SIZE = 20;
	/** @type {number} */
	C.DATE_SIZE = 16;
	var Page = function () {
		/**
		 * @param {string} options
		 * @param {string} element
		 * @param {number} aureliaUtils
		 * @param {?} renderer
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3(options, element, aureliaUtils, renderer) {
			_classCallCheck2(this, TempusDominusBootstrap3);
			/** @type {string} */
			this.leftchar = options;
			/** @type {string} */
			this.rightchar = element;
			/** @type {number} */
			this.finderpattern = aureliaUtils;
			this.maybeLast = renderer;
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "mayBeLast",
			value: function mayBeLast() {
				return this.maybeLast;
			}
		}, {
			key: "getLeftChar",
			value: function getLeftChar() {
				return this.leftchar;
			}
		}, {
			key: "getRightChar",
			value: function getRightChar() {
				return this.rightchar;
			}
		}, {
			key: "getFinderPattern",
			value: function getFinderPattern() {
				return this.finderpattern;
			}
		}, {
			key: "mustBeLast",
			value: function mustBeLast() {
				return null == this.rightchar;
			}
		}, {
			key: "toString",
			value: function gameStarter() {
				return "[ " + this.leftchar + ", " + this.rightchar + " : " + (null == this.finderpattern ? "null" : this.finderpattern.getValue()) + " ]";
			}
		}, {
			key: "hashCode",
			value: function removeMemberFromDiscussion() {
				return this.leftchar.getValue() ^ this.rightchar.getValue() ^ this.finderpattern.getValue();
			}
		}], [{
			key: "equals",
			value: function handleSlide(isSlidingUp, $cont) {
				return isSlidingUp instanceof TempusDominusBootstrap3 && TempusDominusBootstrap3.equalsOrNull(isSlidingUp.leftchar, $cont.leftchar) && TempusDominusBootstrap3.equalsOrNull(isSlidingUp.rightchar, $cont.rightchar) && TempusDominusBootstrap3.equalsOrNull(isSlidingUp.finderpattern, $cont.finderpattern);
			}
		}, {
			key: "equalsOrNull",
			value: function storeOrDirty(string, value) {
				return null === string ? null === value : TempusDominusBootstrap3.equals(string, value);
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var Cartesian3 = function () {
		/**
		 * @param {!Object} options
		 * @param {number} element
		 * @param {?} aureliaUtils
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3(options, element, aureliaUtils) {
			_classCallCheck2(this, TempusDominusBootstrap3);
			/** @type {!Object} */
			this.pairs = options;
			/** @type {number} */
			this.rowNumber = element;
			this.wasReversed = aureliaUtils;
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "getPairs",
			value: function getPairs() {
				return this.pairs;
			}
		}, {
			key: "getRowNumber",
			value: function getRowNumber() {
				return this.rowNumber;
			}
		}, {
			key: "isReversed",
			value: function isReversed() {
				return this.wasReversed;
			}
		}, {
			key: "isEquivalent",
			value: function $get(mmCoreSplitViewBlock) {
				return this.checkEqualitity(this, mmCoreSplitViewBlock);
			}
		}, {
			key: "toString",
			value: function HashNode() {
				return "{ " + this.pairs + " }";
			}
		}, {
			key: "equals",
			value: function removeFromArray_(array, value) {
				return array instanceof TempusDominusBootstrap3 && this.checkEqualitity(array, value) && array.wasReversed === value.wasReversed;
			}
		}, {
			key: "checkEqualitity",
			value: function updateInstrumentInSong(targets, method) {
				if (!targets || !method) {
					return;
				}
				var r = void 0;
				return targets.forEach(function (canCreateDiscussions, n) {
					method.forEach(function (isSlidingUp) {
						if (canCreateDiscussions.getLeftChar().getValue() === isSlidingUp.getLeftChar().getValue() && canCreateDiscussions.getRightChar().getValue() === isSlidingUp.getRightChar().getValue() && canCreateDiscussions.getFinderPatter().getValue() === isSlidingUp.getFinderPatter().getValue()) {
							/** @type {boolean} */
							r = true;
						}
					});
				}), r;
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var Queue = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @param {(number|string)} value
		 * @return {?}
		 */
		function self(value) {
			var result;
			_classCallCheck2(this, self);
			result = _possibleConstructorReturn(this, (self.__proto__ || Object.getPrototypeOf(self)).apply(this, arguments));
			result;
			/** @type {!Array} */
			result.pairs = new Array(self.MAX_PAIRS);
			/** @type {!Array} */
			result.rows = new Array;
			/** @type {!Array} */
			result.startEnd = [2];
			/** @type {boolean} */
			result.verbose = true === value;
			return result;
		}
		_inherits(self, _WebInspector$GeneralTreeElement);
		_createClass2(self, [{
			key: "decodeRow",
			value: function register(json, name, render_function) {
				/** @type {number} */
				this.pairs.length = 0;
				/** @type {boolean} */
				this.startFromEven = false;
				try {
					return self.constructResult(this.decodeRow2pairs(json, name));
				} catch (conv_reverse_sort) {
					if (this.verbose) {
						console.log(conv_reverse_sort);
					}
				}
				return this.pairs.length = 0, this.startFromEven = true, self.constructResult(this.decodeRow2pairs(json, name));
			}
		}, {
			key: "reset",
			value: function F2Pattern() {
				/** @type {number} */
				this.pairs.length = 0;
				/** @type {number} */
				this.rows.length = 0;
			}
		}, {
			key: "decodeRow2pairs",
			value: function ObjectType(src, className) {
				var image_2 = void 0;
				/** @type {boolean} */
				var n = false;
				for (; !n;) {
					try {
						this.pairs.push(this.retrieveNextPair(className, this.pairs, src));
					} catch (ex) {
						if (ex instanceof TypeError) {
							if (!this.pairs.length) {
								throw new TypeError;
							}
							/** @type {boolean} */
							n = true;
						}
					}
				}
				if (this.checkChecksum()) {
					return this.pairs;
				}
				if (image_2 = !!this.rows.length, this.storeRow(src, false), image_2) {
					var _this = this.checkRowsBoolean(false);
					if (null != _this) {
						return _this;
					}
					if (null != (_this = this.checkRowsBoolean(true))) {
						return _this;
					}
				}
				throw new TypeError;
			}
		}, {
			key: "checkRowsBoolean",
			value: function build(n) {
				if (this.rows.length > 25) {
					return this.rows.length = 0, null;
				}
				/** @type {number} */
				this.pairs.length = 0;
				if (n) {
					this.rows = this.rows.reverse();
				}
				/** @type {null} */
				var r = null;
				try {
					r = this.checkRows(new Array, 0);
				} catch (conv_reverse_sort) {
					if (this.verbose) {
						console.log(conv_reverse_sort);
					}
				}
				return n && (this.rows = this.rows.reverse()), r;
			}
		}, {
			key: "checkRows",
			value: function add(rows, options) {
				/** @type {number} */
				var i = options;
				for (; i < this.rows.length; i++) {
					var rowId = this.rows[i];
					/** @type {number} */
					this.pairs.length = 0;
					/** @type {boolean} */
					var _iteratorNormalCompletion3 = true;
					/** @type {boolean} */
					var _didIteratorError23 = false;
					var _iteratorError17 = undefined;
					try {
						var _iterator3 = rows[Symbol.iterator]();
						var $__6;
						for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
							var item = $__6.value;
							this.pairs.push(item.getPairs());
						}
					} catch (err) {
						/** @type {boolean} */
						_didIteratorError23 = true;
						_iteratorError17 = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion3 && _iterator3.return) {
								_iterator3.return();
							}
						} finally {
							if (_didIteratorError23) {
								throw _iteratorError17;
							}
						}
					}
					if (this.pairs.push(rowId.getPairs()), !self.isValidSequence(this.pairs)) {
						continue;
					}
					if (this.checkChecksum()) {
						return this.pairs;
					}
					/** @type {!Array} */
					var m = new Array(rows);
					m.push(rowId);
					try {
						return this.checkRows(m, i + 1);
					} catch (conv_reverse_sort) {
						if (this.verbose) {
							console.log(conv_reverse_sort);
						}
					}
				}
				throw new TypeError;
			}
		}, {
			key: "storeRow",
			value: function ctor(size, height) {
				/** @type {number} */
				var j = 0;
				/** @type {boolean} */
				var winRef = false;
				/** @type {boolean} */
				var inputWin = false;
				for (; j < this.rows.length;) {
					var tr = this.rows[j];
					if (tr.getRowNumber() > size) {
						inputWin = tr.isEquivalent(this.pairs);
						break;
					}
					winRef = tr.isEquivalent(this.pairs);
					j++;
				}
				if (!(inputWin || winRef || self.isPartialRow(this.pairs, this.rows))) {
					this.rows.push(j, new Cartesian3(this.pairs, size, height));
					this.removePartialRows(this.pairs, this.rows);
				}
			}
		}, {
			key: "removePartialRows",
			value: function update(options, newValues) {
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError24 = false;
				var _iteratorError17 = undefined;
				try {
					var _iterator3 = newValues[Symbol.iterator]();
					var $__6;
					for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var item = $__6.value;
						if (item.getPairs().length !== options.length) {
							/** @type {boolean} */
							var _iteratorNormalCompletion3 = true;
							/** @type {boolean} */
							var _didIteratorError25 = false;
							var _iteratorError17 = undefined;
							try {
								var _iterator3 = item.getPairs()[Symbol.iterator]();
								var $__6;
								for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
									var item = $__6.value;
									/** @type {boolean} */
									var _iteratorNormalCompletion3 = true;
									/** @type {boolean} */
									var _didIteratorError26 = false;
									var _iteratorError17 = undefined;
									try {
										var _iterator3 = options[Symbol.iterator]();
										var _step2;
										for (; !(_iteratorNormalCompletion3 = (_step2 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
											var data = _step2.value;
											if (Page.equals(item, data)) {
												break;
											}
										}
									} catch (err) {
										/** @type {boolean} */
										_didIteratorError26 = true;
										_iteratorError17 = err;
									} finally {
										try {
											if (!_iteratorNormalCompletion3 && _iterator3.return) {
												_iterator3.return();
											}
										} finally {
											if (_didIteratorError26) {
												throw _iteratorError17;
											}
										}
									}
								}
							} catch (err) {
								/** @type {boolean} */
								_didIteratorError25 = true;
								_iteratorError17 = err;
							} finally {
								try {
									if (!_iteratorNormalCompletion3 && _iterator3.return) {
										_iterator3.return();
									}
								} finally {
									if (_didIteratorError25) {
										throw _iteratorError17;
									}
								}
							}
						}
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError24 = true;
					_iteratorError17 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError24) {
							throw _iteratorError17;
						}
					}
				}
			}
		}, {
			key: "getRows",
			value: function getRows() {
				return this.rows;
			}
		}, {
			key: "checkChecksum",
			value: function build() {
				var t = this.pairs.get(0);
				var exportedP1 = t.getLeftChar();
				var r = t.getRightChar();
				if (null == r) {
					return false;
				}
				var angle = r.getChecksumPortion();
				/** @type {number} */
				var event_patch = 2;
				/** @type {number} */
				var i = 1;
				for (; i < this.pairs.size(); ++i) {
					var $sendIcon = this.pairs.get(i);
					angle = angle + $sendIcon.getLeftChar().getChecksumPortion();
					event_patch++;
					var _r55 = $sendIcon.getRightChar();
					if (null != _r55) {
						angle = angle + _r55.getChecksumPortion();
						event_patch++;
					}
				}
				return 211 * (event_patch - 4) + (angle = angle % 211) == exportedP1.getValue();
			}
		}, {
			key: "retrieveNextPair",
			value: function do_sources(args, cb, opts) {
				var options = void 0;
				/** @type {boolean} */
				var a = cb.length % 2 == 0;
				if (this.startFromEven) {
					/** @type {boolean} */
					a = !a;
				}
				/** @type {boolean} */
				var s = true;
				/** @type {number} */
				var childDecision = -1;
				do {
					this.findNextPair(args, cb, childDecision);
					if (null == (options = this.parseFoundFinderPattern(args, opts, a))) {
						childDecision = self.getNextSecondBar(args, this.startEnd[0]);
					} else {
						/** @type {boolean} */
						s = false;
					}
				} while (s);
				var res = void 0;
				var template = this.decodeDataCharacter(args, options, a, true);
				if (!this.isEmptyPair(cb) && cb[cb.length - 1].mustBeLast()) {
					throw new TypeError;
				}
				try {
					res = this.decodeDataCharacter(args, options, a, false);
				} catch (conv_reverse_sort) {
					/** @type {null} */
					res = null;
					if (this.verbose) {
						console.log(conv_reverse_sort);
					}
				}
				return new Page(template, res, options, true);
			}
		}, {
			key: "isEmptyPair",
			value: function instrumentTree(outFile) {
				return 0 === outFile.length;
			}
		}, {
			key: "findNextPair",
			value: function init(store, weight, size) {
				var d = this.getDecodeFinderCounters();
				/** @type {number} */
				d[0] = 0;
				/** @type {number} */
				d[1] = 0;
				/** @type {number} */
				d[2] = 0;
				/** @type {number} */
				d[3] = 0;
				var idx = void 0;
				var length = store.getSize();
				if (size >= 0) {
					/** @type {number} */
					idx = size;
				} else {
					if (this.isEmptyPair(weight)) {
						/** @type {number} */
						idx = 0;
					} else {
						idx = weight[weight.length - 1].getFinderPattern().getStartEnd()[1];
					}
				}
				/** @type {boolean} */
				var ranges = weight.length % 2 != 0;
				if (this.startFromEven) {
					/** @type {boolean} */
					ranges = !ranges;
				}
				/** @type {boolean} */
				var c = false;
				for (; idx < length && (c = !store.get(idx));) {
					idx++;
				}
				/** @type {number} */
				var search_lemma = 0;
				var tmpIdx = idx;
				var i = idx;
				for (; i < length; i++) {
					if (store.get(i) != c) {
						d[search_lemma]++;
					} else {
						if (3 == search_lemma) {
							if (ranges && self.reverseCounters(d), self.isFinderPattern(d)) {
								return this.startEnd[0] = tmpIdx, void (this.startEnd[1] = i);
							}
							if (ranges) {
								self.reverseCounters(d);
							}
							tmpIdx = tmpIdx + (d[0] + d[1]);
							d[0] = d[2];
							d[1] = d[3];
							/** @type {number} */
							d[2] = 0;
							/** @type {number} */
							d[3] = 0;
							search_lemma--;
						} else {
							search_lemma++;
						}
						/** @type {number} */
						d[search_lemma] = 1;
						/** @type {boolean} */
						c = !c;
					}
				}
				throw new TypeError;
			}
		}, {
			key: "parseFoundFinderPattern",
			value: function lookup(name, cb, retry) {
				var h = void 0;
				var step = void 0;
				var configuration = void 0;
				if (retry) {
					/** @type {number} */
					var n = this.startEnd[0] - 1;
					for (; n >= 0 && !name.get(n);) {
						n--;
					}
					n++;
					/** @type {number} */
					h = this.startEnd[0] - n;
					/** @type {number} */
					step = n;
					configuration = this.startEnd[1];
				} else {
					step = this.startEnd[0];
					/** @type {number} */
					h = (configuration = name.getNextUnset(this.startEnd[1] + 1)) - this.startEnd[1];
				}
				var n = void 0;
				var c = this.getDecodeFinderCounters();
				System.arraycopy(c, 0, c, 1, c.length - 1);
				/** @type {number} */
				c[0] = h;
				try {
					n = this.parseFinderValue(c, self.FINDER_PATTERNS);
				} catch (t) {
					return null;
				}
				return new Component(n, [step, configuration], step, configuration, cb);
			}
		}, {
			key: "decodeDataCharacter",
			value: function add(attachKey, url, oData, fn) {
				var data = this.getDataCharacterCounters();
				/** @type {number} */
				var byteIndex = 0;
				for (; byteIndex < data.length; byteIndex++) {
					/** @type {number} */
					data[byteIndex] = 0;
				}
				if (fn) {
					self.recordPatternInReverse(attachKey, url.getStartEnd()[0], data);
				} else {
					self.recordPattern(attachKey, url.getStartEnd()[1], data);
					/** @type {number} */
					var j = 0;
					/** @type {number} */
					var k = data.length - 1;
					for (; j < k; j++, k--) {
						var v = data[j];
						data[j] = data[k];
						data[k] = v;
					}
				}
				/** @type {number} */
				var auth0_time = p.sum(new Int32Array(data)) / 17;
				/** @type {number} */
				var local_time = (url.getStartEnd()[1] - url.getStartEnd()[0]) / 15;
				if (Math.abs(auth0_time - local_time) / local_time > .3) {
					throw new TypeError;
				}
				var m = this.getOddCounts();
				var l = this.getEvenCounts();
				var ruleLengthFromMin = this.getOddRoundingErrors();
				var ruleLengthToMin = this.getEvenRoundingErrors();
				/** @type {number} */
				var index = 0;
				for (; index < data.length; index++) {
					/** @type {number} */
					var tmp = 1 * data[index] / auth0_time;
					/** @type {number} */
					var tmp4 = tmp + .5;
					if (tmp4 < 1) {
						if (tmp < .3) {
							throw new TypeError;
						}
						/** @type {number} */
						tmp4 = 1;
					} else {
						if (tmp4 > 8) {
							if (tmp > 8.7) {
								throw new TypeError;
							}
							/** @type {number} */
							tmp4 = 8;
						}
					}
					/** @type {number} */
					var i = index / 2;
					if (0 == (1 & index)) {
						/** @type {number} */
						m[i] = tmp4;
						/** @type {number} */
						ruleLengthFromMin[i] = tmp - tmp4;
					} else {
						/** @type {number} */
						l[i] = tmp4;
						/** @type {number} */
						ruleLengthToMin[i] = tmp - tmp4;
					}
				}
				this.adjustOddEvenCounts(17);
				/** @type {number} */
				var n = 4 * url.getValue() + (oData ? 0 : 2) + (fn ? 0 : 1) - 1;
				/** @type {number} */
				var w = 0;
				/** @type {number} */
				var sum = 0;
				/** @type {number} */
				var k = m.length - 1;
				for (; k >= 0; k--) {
					if (self.isNotA1left(url, oData, fn)) {
						var x = self.WEIGHTS[n][2 * k];
						/** @type {number} */
						sum = sum + m[k] * x;
					}
					w = w + m[k];
				}
				/** @type {number} */
				var num = 0;
				/** @type {number} */
				var VIEW_COLUMN_NB_TX = l.length - 1;
				for (; VIEW_COLUMN_NB_TX >= 0; VIEW_COLUMN_NB_TX--) {
					if (self.isNotA1left(url, oData, fn)) {
						var EMA26_K = self.WEIGHTS[n][2 * VIEW_COLUMN_NB_TX + 1];
						/** @type {number} */
						num = num + l[VIEW_COLUMN_NB_TX] * EMA26_K;
					}
				}
				/** @type {number} */
				var name = sum + num;
				if (0 != (1 & w) || w > 13 || w < 4) {
					throw new TypeError;
				}
				/** @type {number} */
				var i = (13 - w) / 2;
				var range = self.SYMBOL_WIDEST[i];
				/** @type {number} */
				var start = 9 - range;
				var a = console.getRSSvalue(m, range, true);
				var b = console.getRSSvalue(l, start, false);
				var t = self.EVEN_TOTAL_SUBSET[i];
				var bucket = self.GSUM[i];
				return new Controller(a * t + b + bucket, name);
			}
		}, {
			key: "adjustOddEvenCounts",
			value: function sum(width) {
				var managementcommandsdns = p.sum(new Int32Array(this.getOddCounts()));
				var siteName = p.sum(new Int32Array(this.getEvenCounts()));
				/** @type {boolean} */
				var n = false;
				/** @type {boolean} */
				var fromGroup = false;
				if (managementcommandsdns > 13) {
					/** @type {boolean} */
					fromGroup = true;
				} else {
					if (managementcommandsdns < 4) {
						/** @type {boolean} */
						n = true;
					}
				}
				/** @type {boolean} */
				var isvalid = false;
				/** @type {boolean} */
				var s = false;
				if (siteName > 13) {
					/** @type {boolean} */
					s = true;
				} else {
					if (siteName < 4) {
						/** @type {boolean} */
						isvalid = true;
					}
				}
				/** @type {number} */
				var remaining_space = managementcommandsdns + siteName - width;
				/** @type {boolean} */
				var l = 1 == (1 & managementcommandsdns);
				/** @type {boolean} */
				var h = 0 == (1 & siteName);
				if (1 == remaining_space) {
					if (l) {
						if (h) {
							throw new TypeError;
						}
						/** @type {boolean} */
						fromGroup = true;
					} else {
						if (!h) {
							throw new TypeError;
						}
						/** @type {boolean} */
						s = true;
					}
				} else {
					if (-1 == remaining_space) {
						if (l) {
							if (h) {
								throw new TypeError;
							}
							/** @type {boolean} */
							n = true;
						} else {
							if (!h) {
								throw new TypeError;
							}
							/** @type {boolean} */
							isvalid = true;
						}
					} else {
						if (0 != remaining_space) {
							throw new TypeError;
						}
						if (l) {
							if (!h) {
								throw new TypeError;
							}
							if (managementcommandsdns < siteName) {
								/** @type {boolean} */
								n = true;
								/** @type {boolean} */
								s = true;
							} else {
								/** @type {boolean} */
								fromGroup = true;
								/** @type {boolean} */
								isvalid = true;
							}
						} else {
							if (h) {
								throw new TypeError;
							}
						}
					}
				}
				if (n) {
					if (fromGroup) {
						throw new TypeError;
					}
					self.increment(this.getOddCounts(), this.getOddRoundingErrors());
				}
				if (fromGroup && self.decrement(this.getOddCounts(), this.getOddRoundingErrors()), isvalid) {
					if (s) {
						throw new TypeError;
					}
					self.increment(this.getEvenCounts(), this.getOddRoundingErrors());
				}
				if (s) {
					self.decrement(this.getEvenCounts(), this.getEvenRoundingErrors());
				}
			}
		}], [{
			key: "isValidSequence",
			value: function buildClause(params) {
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError27 = false;
				var _iteratorError17 = undefined;
				try {
					var _iterator3 = self.FINDER_PATTERN_SEQUENCES[Symbol.iterator]();
					var $__6;
					for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var item = $__6.value;
						if (params.length > item.length) {
							continue;
						}
						/** @type {boolean} */
						var r = true;
						/** @type {number} */
						var i = 0;
						for (; i < params.length; i++) {
							if (params[i].getFinderPattern().getValue() != item[i]) {
								/** @type {boolean} */
								r = false;
								break;
							}
						}
						if (r) {
							return true;
						}
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError27 = true;
					_iteratorError17 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError27) {
							throw _iteratorError17;
						}
					}
				}
				return false;
			}
		}, {
			key: "isPartialRow",
			value: function hasScopeDescriptor(fromScopes, toScopes) {
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError28 = false;
				var _iteratorError17 = undefined;
				try {
					var _iterator3 = toScopes[Symbol.iterator]();
					var $__6;
					for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var item = $__6.value;
						/** @type {boolean} */
						var _e85 = true;
						/** @type {boolean} */
						var _iteratorNormalCompletion3 = true;
						/** @type {boolean} */
						var _didIteratorError29 = false;
						var _iteratorError17 = undefined;
						try {
							var _iterator3 = fromScopes[Symbol.iterator]();
							var $__6;
							for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
								var item = $__6.value;
								/** @type {boolean} */
								var _t98 = false;
								/** @type {boolean} */
								var _iteratorNormalCompletion3 = true;
								/** @type {boolean} */
								var _didIteratorError30 = false;
								var _iteratorError17 = undefined;
								try {
									var _iterator3 = item.getPairs()[Symbol.iterator]();
									var _step6;
									for (; !(_iteratorNormalCompletion3 = (_step6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
										var section = _step6.value;
										if (item.equals(section)) {
											/** @type {boolean} */
											_t98 = true;
											break;
										}
									}
								} catch (err) {
									/** @type {boolean} */
									_didIteratorError30 = true;
									_iteratorError17 = err;
								} finally {
									try {
										if (!_iteratorNormalCompletion3 && _iterator3.return) {
											_iterator3.return();
										}
									} finally {
										if (_didIteratorError30) {
											throw _iteratorError17;
										}
									}
								}
								if (!_t98) {
									/** @type {boolean} */
									_e85 = false;
									break;
								}
							}
						} catch (err) {
							/** @type {boolean} */
							_didIteratorError29 = true;
							_iteratorError17 = err;
						} finally {
							try {
								if (!_iteratorNormalCompletion3 && _iterator3.return) {
									_iterator3.return();
								}
							} finally {
								if (_didIteratorError29) {
									throw _iteratorError17;
								}
							}
						}
						if (_e85) {
							return true;
						}
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError28 = true;
					_iteratorError17 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError28) {
							throw _iteratorError17;
						}
					}
				}
				return false;
			}
		}, {
			key: "constructResult",
			value: function create(html) {
				var s = function (a) {
					try {
						if (a.get(1)) {
							return new m(a);
						}
						if (!a.get(2)) {
							return new adjustedLevel(a);
						}
						switch (ObjectOperation.extractNumericValueFromBitArray(a, 1, 4)) {
							case 4:
								return new $magnifier(a);
							case 5:
								return new ReactiveLoop(a);
						}
						switch (ObjectOperation.extractNumericValueFromBitArray(a, 1, 5)) {
							case 12:
								return new string(a);
							case 13:
								return new Props(a);
						}
						switch (ObjectOperation.extractNumericValueFromBitArray(a, 1, 7)) {
							case 56:
								return new C(a, "310", "11");
							case 57:
								return new C(a, "320", "11");
							case 58:
								return new C(a, "310", "13");
							case 59:
								return new C(a, "320", "13");
							case 60:
								return new C(a, "310", "15");
							case 61:
								return new C(a, "320", "15");
							case 62:
								return new C(a, "310", "17");
							case 63:
								return new C(a, "320", "17");
						}
					} catch (conv_reverse_sort) {
						throw console.log(conv_reverse_sort), new Path("unknown decoder: " + a);
					}
				}(bt.buildBitArray(html)).parseInformation();
				var r = html[0].getFinderPattern().getResultPoints();
				var n = html[html.length - 1].getFinderPattern().getResultPoints();
				/** @type {!Array} */
				var i = [r[0], r[1], n[0], n[1]];
				return new result(s, null, null, i, change.RSS_EXPANDED, null);
			}
		}, {
			key: "getNextSecondBar",
			value: function findDataPoint(array, i) {
				var query = void 0;
				return array.get(i) ? (query = array.getNextUnset(i), query = array.getNextSet(query)) : (query = array.getNextSet(i), query = array.getNextUnset(query)), query;
			}
		}, {
			key: "reverseCounters",
			value: function haveGroupOnServer(group) {
				var len = group.length;
				/** @type {number} */
				var i = 0;
				for (; i < len / 2; ++i) {
					var key = group[i];
					group[i] = group[len - i - 1];
					group[len - i - 1] = key;
				}
			}
		}, {
			key: "isNotA1left",
			value: function getLatestMessages(id, options, type) {
				return !(0 == id.getValue() && options && type);
			}
		}]);
		return self;
	}(hash);
	/** @type {!Array} */
	Queue.SYMBOL_WIDEST = [7, 5, 4, 3, 1];
	/** @type {!Array} */
	Queue.EVEN_TOTAL_SUBSET = [4, 20, 52, 104, 204];
	/** @type {!Array} */
	Queue.GSUM = [0, 348, 1388, 2948, 3988];
	/** @type {!Array} */
	Queue.FINDER_PATTERNS = [Int32Array.from([1, 8, 4, 1]), Int32Array.from([3, 6, 4, 1]), Int32Array.from([3, 4, 6, 1]), Int32Array.from([3, 2, 8, 1]), Int32Array.from([2, 6, 5, 1]), Int32Array.from([2, 2, 9, 1])];
	/** @type {!Array} */
	Queue.WEIGHTS = [[1, 3, 9, 27, 81, 32, 96, 77], [20, 60, 180, 118, 143, 7, 21, 63], [189, 145, 13, 39, 117, 140, 209, 205], [193, 157, 49, 147, 19, 57, 171, 91], [62, 186, 136, 197, 169, 85, 44, 132], [185, 133, 188, 142, 4, 12, 36, 108], [113, 128, 173, 97, 80, 29, 87, 50], [150, 28, 84, 41, 123, 158, 52, 156], [46, 138, 203, 187, 139, 206, 196, 166], [76, 17, 51, 153, 37, 111, 122, 155], [43, 129, 176, 106, 107, 110, 119, 146], [16, 48, 144, 10, 30, 90, 59, 177], [109, 116, 137, 200, 178, 112,
		125, 164], [70, 210, 208, 202, 184, 130, 179, 115], [134, 191, 151, 31, 93, 68, 204, 190], [148, 22, 66, 198, 172, 94, 71, 2], [6, 18, 54, 162, 64, 192, 154, 40], [120, 149, 25, 75, 14, 42, 126, 167], [79, 26, 78, 23, 69, 207, 199, 175], [103, 98, 83, 38, 114, 131, 182, 124], [161, 61, 183, 127, 170, 88, 53, 159], [55, 165, 73, 8, 24, 72, 5, 15], [45, 135, 194, 160, 58, 174, 100, 89]];
	/** @type {number} */
	Queue.FINDER_PAT_A = 0;
	/** @type {number} */
	Queue.FINDER_PAT_B = 1;
	/** @type {number} */
	Queue.FINDER_PAT_C = 2;
	/** @type {number} */
	Queue.FINDER_PAT_D = 3;
	/** @type {number} */
	Queue.FINDER_PAT_E = 4;
	/** @type {number} */
	Queue.FINDER_PAT_F = 5;
	/** @type {!Array} */
	Queue.FINDER_PATTERN_SEQUENCES = [[Queue.FINDER_PAT_A, Queue.FINDER_PAT_A], [Queue.FINDER_PAT_A, Queue.FINDER_PAT_B, Queue.FINDER_PAT_B], [Queue.FINDER_PAT_A, Queue.FINDER_PAT_C, Queue.FINDER_PAT_B, Queue.FINDER_PAT_D], [Queue.FINDER_PAT_A, Queue.FINDER_PAT_E, Queue.FINDER_PAT_B, Queue.FINDER_PAT_D, Queue.FINDER_PAT_C], [Queue.FINDER_PAT_A, Queue.FINDER_PAT_E, Queue.FINDER_PAT_B, Queue.FINDER_PAT_D, Queue.FINDER_PAT_D, Queue.FINDER_PAT_F], [Queue.FINDER_PAT_A, Queue.FINDER_PAT_E, Queue.FINDER_PAT_B,
	Queue.FINDER_PAT_D, Queue.FINDER_PAT_E, Queue.FINDER_PAT_F, Queue.FINDER_PAT_F], [Queue.FINDER_PAT_A, Queue.FINDER_PAT_A, Queue.FINDER_PAT_B, Queue.FINDER_PAT_B, Queue.FINDER_PAT_C, Queue.FINDER_PAT_C, Queue.FINDER_PAT_D, Queue.FINDER_PAT_D], [Queue.FINDER_PAT_A, Queue.FINDER_PAT_A, Queue.FINDER_PAT_B, Queue.FINDER_PAT_B, Queue.FINDER_PAT_C, Queue.FINDER_PAT_C, Queue.FINDER_PAT_D, Queue.FINDER_PAT_E, Queue.FINDER_PAT_E], [Queue.FINDER_PAT_A, Queue.FINDER_PAT_A, Queue.FINDER_PAT_B, Queue.FINDER_PAT_B,
	Queue.FINDER_PAT_C, Queue.FINDER_PAT_C, Queue.FINDER_PAT_D, Queue.FINDER_PAT_E, Queue.FINDER_PAT_F, Queue.FINDER_PAT_F], [Queue.FINDER_PAT_A, Queue.FINDER_PAT_A, Queue.FINDER_PAT_B, Queue.FINDER_PAT_B, Queue.FINDER_PAT_C, Queue.FINDER_PAT_D, Queue.FINDER_PAT_D, Queue.FINDER_PAT_E, Queue.FINDER_PAT_E, Queue.FINDER_PAT_F, Queue.FINDER_PAT_F]];
	/** @type {number} */
	Queue.MAX_PAIRS = 11;
	var AppController = function (parent) {
		/**
		 * @param {?} props
		 * @param {?} context
		 * @param {?} r
		 * @return {?}
		 */
		function NavigationTransitioner(props, context, r) {
			var _this;
			_classCallCheck2(this, NavigationTransitioner);
			_this = _possibleConstructorReturn(this, (NavigationTransitioner.__proto__ || Object.getPrototypeOf(NavigationTransitioner)).call(this, props, context));
			_this;
			/** @type {number} */
			_this.count = 0;
			_this.finderPattern = r;
			return _this;
		}
		_inherits(NavigationTransitioner, parent);
		_createClass2(NavigationTransitioner, [{
			key: "getFinderPattern",
			value: function getFinderPattern() {
				return this.finderPattern;
			}
		}, {
			key: "getCount",
			value: function count() {
				return this.count;
			}
		}, {
			key: "incrementCount",
			value: function incrementCount() {
				this.count++;
			}
		}]);
		return NavigationTransitioner;
	}(Controller);
	var tile = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @return {?}
		 */
		function self() {
			var _this;
			_classCallCheck2(this, self);
			_this = _possibleConstructorReturn(this, (self.__proto__ || Object.getPrototypeOf(self)).apply(this, arguments));
			_this;
			/** @type {!Array} */
			_this.possibleLeftPairs = [];
			/** @type {!Array} */
			_this.possibleRightPairs = [];
			return _this;
		}
		_inherits(self, _WebInspector$GeneralTreeElement);
		_createClass2(self, [{
			key: "decodeRow",
			value: function update(height, options, key) {
				var result = this.decodePair(options, false, height, key);
				self.addOrTally(this.possibleLeftPairs, result);
				options.reverse();
				var node = this.decodePair(options, true, height, key);
				self.addOrTally(this.possibleRightPairs, node);
				options.reverse();
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError31 = false;
				var _iteratorError17 = undefined;
				try {
					var _iterator3 = this.possibleLeftPairs[Symbol.iterator]();
					var $__6;
					for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var item = $__6.value;
						if (item.getCount() > 1) {
							/** @type {boolean} */
							var _iteratorNormalCompletion3 = true;
							/** @type {boolean} */
							var _didIteratorError32 = false;
							var _iteratorError17 = undefined;
							try {
								var _iterator3 = this.possibleRightPairs[Symbol.iterator]();
								var $__4;
								for (; !(_iteratorNormalCompletion3 = ($__4 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
									var value = $__4.value;
									if (value.getCount() > 1 && self.checkChecksum(item, value)) {
										return self.constructResult(item, value);
									}
								}
							} catch (err) {
								/** @type {boolean} */
								_didIteratorError32 = true;
								_iteratorError17 = err;
							} finally {
								try {
									if (!_iteratorNormalCompletion3 && _iterator3.return) {
										_iterator3.return();
									}
								} finally {
									if (_didIteratorError32) {
										throw _iteratorError17;
									}
								}
							}
						}
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError31 = true;
					_iteratorError17 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError31) {
							throw _iteratorError17;
						}
					}
				}
				throw new TypeError;
			}
		}, {
			key: "reset",
			value: function writeTextArgs() {
				/** @type {number} */
				this.possibleLeftPairs.length = 0;
				/** @type {number} */
				this.possibleRightPairs.length = 0;
			}
		}, {
			key: "decodePair",
			value: function init(target, name, data, value) {
				try {
					var w = this.findFinderPattern(target, name);
					var r = this.parseFoundFinderPattern(target, data, name, w);
					var outEvents = null == value ? null : value.get(node.NEED_RESULT_POINT_CALLBACK);
					if (null != outEvents) {
						/** @type {number} */
						var key = (w[0] + w[1]) / 2;
						if (name) {
							/** @type {number} */
							key = target.getSize() - 1 - key;
						}
						outEvents.foundPossibleResultPoint(new type(key, data));
					}
					var actual = this.decodeDataCharacter(target, r, true);
					var s = this.decodeDataCharacter(target, r, false);
					return new AppController(1597 * actual.getValue() + s.getValue(), actual.getChecksumPortion() + 4 * s.getChecksumPortion(), r);
				} catch (t) {
					return null;
				}
			}
		}, {
			key: "decodeDataCharacter",
			value: function controller($location, invitationAPI, settings) {
				var s = this.getDataCharacterCounters();
				/** @type {number} */
				var i = 0;
				for (; i < s.length; i++) {
					/** @type {number} */
					s[i] = 0;
				}
				if (settings) {
					path.recordPatternInReverse($location, invitationAPI.getStartEnd()[0], s);
				} else {
					path.recordPattern($location, invitationAPI.getStartEnd()[1] + 1, s);
					/** @type {number} */
					var j = 0;
					/** @type {number} */
					var i = s.length - 1;
					for (; j < i; j++, i--) {
						var u = s[j];
						s[j] = s[i];
						s[i] = u;
					}
				}
				/** @type {number} */
				var pathSettings = settings ? 16 : 15;
				/** @type {number} */
				var totalWeight = p.sum(new Int32Array(s)) / pathSettings;
				var params = this.getOddCounts();
				var e = this.getEvenCounts();
				var ruleLengthFromMin = this.getOddRoundingErrors();
				var ruleLengthToMin = this.getEvenRoundingErrors();
				/** @type {number} */
				var j = 0;
				for (; j < s.length; j++) {
					/** @type {number} */
					var tmp = s[j] / totalWeight;
					/** @type {number} */
					var tmp4 = Math.floor(tmp + .5);
					if (tmp4 < 1) {
						/** @type {number} */
						tmp4 = 1;
					} else {
						if (tmp4 > 8) {
							/** @type {number} */
							tmp4 = 8;
						}
					}
					/** @type {number} */
					var i = Math.floor(j / 2);
					if (0 == (1 & j)) {
						/** @type {number} */
						params[i] = tmp4;
						/** @type {number} */
						ruleLengthFromMin[i] = tmp - tmp4;
					} else {
						/** @type {number} */
						e[i] = tmp4;
						/** @type {number} */
						ruleLengthToMin[i] = tmp - tmp4;
					}
				}
				this.adjustOddEvenCounts(settings, pathSettings);
				/** @type {number} */
				var path = 0;
				/** @type {number} */
				var u = 0;
				/** @type {number} */
				var q = params.length - 1;
				for (; q >= 0; q--) {
					/** @type {number} */
					u = u * 9;
					u = u + params[q];
					path = path + params[q];
				}
				/** @type {number} */
				var sum = 0;
				/** @type {number} */
				var ctrl = 0;
				/** @type {number} */
				var k = e.length - 1;
				for (; k >= 0; k--) {
					/** @type {number} */
					sum = sum * 9;
					sum = sum + e[k];
					ctrl = ctrl + e[k];
				}
				var plusMinusId = u + 3 * sum;
				if (settings) {
					if (0 != (1 & path) || path > 12 || path < 4) {
						throw new TypeError;
					}
					/** @type {number} */
					var i = (12 - path) / 2;
					var n = self.OUTSIDE_ODD_WIDEST[i];
					/** @type {number} */
					var r = 9 - n;
					var newIndex = console.getRSSvalue(params, n, false);
					var y = console.getRSSvalue(e, r, true);
					var height = self.OUTSIDE_EVEN_TOTAL_SUBSET[i];
					var t = self.OUTSIDE_GSUM[i];
					return new Controller(newIndex * height + y + t, plusMinusId);
				}
				{
					if (0 != (1 & ctrl) || ctrl > 10 || ctrl < 4) {
						throw new TypeError;
					}
					/** @type {number} */
					var i = (10 - ctrl) / 2;
					var currentIndex = self.INSIDE_ODD_WIDEST[i];
					/** @type {number} */
					var dist = 9 - currentIndex;
					var newIndex = console.getRSSvalue(params, currentIndex, true);
					var f = console.getRSSvalue(e, dist, false);
					var s = self.INSIDE_ODD_TOTAL_SUBSET[i];
					var _c = self.INSIDE_GSUM[i];
					return new Controller(f * s + newIndex + _c, plusMinusId);
				}
			}
		}, {
			key: "findFinderPattern",
			value: function spawnServiceMiddleware(output, next) {
				var w = this.getDecodeFinderCounters();
				/** @type {number} */
				w[0] = 0;
				/** @type {number} */
				w[1] = 0;
				/** @type {number} */
				w[2] = 0;
				/** @type {number} */
				w[3] = 0;
				var lim = output.getSize();
				/** @type {boolean} */
				var c = false;
				/** @type {number} */
				var cur = 0;
				for (; cur < lim && next !== (c = !output.get(cur));) {
					cur++;
				}
				/** @type {number} */
				var IEADD = 0;
				/** @type {number} */
				var start = cur;
				/** @type {number} */
				var i = cur;
				for (; i < lim; i++) {
					if (output.get(i) !== c) {
						w[IEADD]++;
					} else {
						if (3 === IEADD) {
							if (hash.isFinderPattern(w)) {
								return [start, i];
							}
							start = start + (w[0] + w[1]);
							w[0] = w[2];
							w[1] = w[3];
							/** @type {number} */
							w[2] = 0;
							/** @type {number} */
							w[3] = 0;
							IEADD--;
						} else {
							IEADD++;
						}
						/** @type {number} */
						w[IEADD] = 1;
						/** @type {boolean} */
						c = !c;
					}
				}
				throw new TypeError;
			}
		}, {
			key: "parseFoundFinderPattern",
			value: function parse(p, cb, n, a) {
				var aArray = p.get(a[0]);
				/** @type {number} */
				var b = a[0] - 1;
				for (; b >= 0 && aArray !== p.get(b);) {
					b--;
				}
				b++;
				/** @type {number} */
				var z = a[0] - b;
				var buffer = this.getDecodeFinderCounters();
				/** @type {!Int32Array} */
				var tmp = new Int32Array(buffer.length);
				System.arraycopy(buffer, 0, tmp, 1, buffer.length - 1);
				/** @type {number} */
				tmp[0] = z;
				var node = this.parseFinderValue(tmp, self.FINDER_PATTERNS);
				/** @type {number} */
				var j = b;
				var i = a[1];
				return n && (j = p.getSize() - 1 - j, i = p.getSize() - 1 - i), new Component(node, [b, a[1]], j, i, cb);
			}
		}, {
			key: "adjustOddEvenCounts",
			value: function onerror(event, a) {
				var len = p.sum(new Int32Array(this.getOddCounts()));
				var n = p.sum(new Int32Array(this.getEvenCounts()));
				/** @type {boolean} */
				var i = false;
				/** @type {boolean} */
				var fromGroup = false;
				/** @type {boolean} */
				var isvalid = false;
				/** @type {boolean} */
				var nDistanceFromEnd = false;
				if (event) {
					if (len > 12) {
						/** @type {boolean} */
						fromGroup = true;
					} else {
						if (len < 4) {
							/** @type {boolean} */
							i = true;
						}
					}
					if (n > 12) {
						/** @type {boolean} */
						nDistanceFromEnd = true;
					} else {
						if (n < 4) {
							/** @type {boolean} */
							isvalid = true;
						}
					}
				} else {
					if (len > 11) {
						/** @type {boolean} */
						fromGroup = true;
					} else {
						if (len < 5) {
							/** @type {boolean} */
							i = true;
						}
					}
					if (n > 10) {
						/** @type {boolean} */
						nDistanceFromEnd = true;
					} else {
						if (n < 4) {
							/** @type {boolean} */
							isvalid = true;
						}
					}
				}
				/** @type {number} */
				var c = len + n - a;
				/** @type {boolean} */
				var h = (1 & len) == (event ? 1 : 0);
				/** @type {boolean} */
				var l = 1 == (1 & n);
				if (1 === c) {
					if (h) {
						if (l) {
							throw new TypeError;
						}
						/** @type {boolean} */
						fromGroup = true;
					} else {
						if (!l) {
							throw new TypeError;
						}
						/** @type {boolean} */
						nDistanceFromEnd = true;
					}
				} else {
					if (-1 === c) {
						if (h) {
							if (l) {
								throw new TypeError;
							}
							/** @type {boolean} */
							i = true;
						} else {
							if (!l) {
								throw new TypeError;
							}
							/** @type {boolean} */
							isvalid = true;
						}
					} else {
						if (0 !== c) {
							throw new TypeError;
						}
						if (h) {
							if (!l) {
								throw new TypeError;
							}
							if (len < n) {
								/** @type {boolean} */
								i = true;
								/** @type {boolean} */
								nDistanceFromEnd = true;
							} else {
								/** @type {boolean} */
								fromGroup = true;
								/** @type {boolean} */
								isvalid = true;
							}
						} else {
							if (l) {
								throw new TypeError;
							}
						}
					}
				}
				if (i) {
					if (fromGroup) {
						throw new TypeError;
					}
					hash.increment(this.getOddCounts(), this.getOddRoundingErrors());
				}
				if (fromGroup && hash.decrement(this.getOddCounts(), this.getOddRoundingErrors()), isvalid) {
					if (nDistanceFromEnd) {
						throw new TypeError;
					}
					hash.increment(this.getEvenCounts(), this.getOddRoundingErrors());
				}
				if (nDistanceFromEnd) {
					hash.decrement(this.getEvenCounts(), this.getEvenRoundingErrors());
				}
			}
		}], [{
			key: "addOrTally",
			value: function render(menuItems, options) {
				if (null == options) {
					return;
				}
				/** @type {boolean} */
				var r = false;
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError33 = false;
				var _iteratorError17 = undefined;
				try {
					var _iterator3 = menuItems[Symbol.iterator]();
					var $__6;
					for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var item = $__6.value;
						if (item.getValue() === options.getValue()) {
							item.incrementCount();
							/** @type {boolean} */
							r = true;
							break;
						}
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError33 = true;
					_iteratorError17 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError33) {
							throw _iteratorError17;
						}
					}
				}
				if (!r) {
					menuItems.push(options);
				}
			}
		}, {
			key: "constructResult",
			value: function create(gen, cb) {
				var s_str = 4537077 * gen.getValue() + cb.getValue();
				/** @type {string} */
				var sub = (new String(s_str)).toString();
				var tmp = new Buffer;
				/** @type {number} */
				var _t107 = 13 - sub.length;
				for (; _t107 > 0; _t107--) {
					tmp.append("0");
				}
				tmp.append(sub);
				/** @type {number} */
				var s = 0;
				/** @type {number} */
				var i = 0;
				for (; i < 13; i++) {
					/** @type {number} */
					var pm = tmp.charAt(i).charCodeAt(0) - "0".charCodeAt(0);
					/** @type {number} */
					s = s + (0 == (1 & i) ? 3 * pm : pm);
				}
				if (10 === (s = 10 - s % 10)) {
					/** @type {number} */
					s = 0;
				}
				tmp.append(s.toString());
				var o = gen.getFinderPattern().getResultPoints();
				var a = cb.getFinderPattern().getResultPoints();
				return new result(tmp.toString(), null, 0, [o[0], o[1], a[0], a[1]], change.RSS_14, (new Date).getTime());
			}
		}, {
			key: "checkChecksum",
			value: function removeMemberFromDiscussion(options, successCallback) {
				/** @type {number} */
				var r = (options.getChecksumPortion() + 16 * successCallback.getChecksumPortion()) % 79;
				var preferiteIndex = 9 * options.getFinderPattern().getValue() + successCallback.getFinderPattern().getValue();
				return preferiteIndex > 72 && preferiteIndex--, preferiteIndex > 8 && preferiteIndex--, r === preferiteIndex;
			}
		}]);
		return self;
	}(hash);
	/** @type {!Array} */
	tile.OUTSIDE_EVEN_TOTAL_SUBSET = [1, 10, 34, 70, 126];
	/** @type {!Array} */
	tile.INSIDE_ODD_TOTAL_SUBSET = [4, 20, 48, 81];
	/** @type {!Array} */
	tile.OUTSIDE_GSUM = [0, 161, 961, 2015, 2715];
	/** @type {!Array} */
	tile.INSIDE_GSUM = [0, 336, 1036, 1516];
	/** @type {!Array} */
	tile.OUTSIDE_ODD_WIDEST = [8, 6, 4, 3, 1];
	/** @type {!Array} */
	tile.INSIDE_ODD_WIDEST = [2, 4, 6, 8];
	/** @type {!Array} */
	tile.FINDER_PATTERNS = [Int32Array.from([3, 8, 2, 1]), Int32Array.from([3, 5, 5, 1]), Int32Array.from([3, 3, 7, 1]), Int32Array.from([3, 1, 9, 1]), Int32Array.from([2, 7, 4, 1]), Int32Array.from([2, 5, 6, 1]), Int32Array.from([2, 3, 8, 1]), Int32Array.from([1, 5, 7, 1]), Int32Array.from([1, 3, 9, 1])];
	var Value = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @param {!Node} props
		 * @param {(number|string)} context
		 * @return {?}
		 */
		function LayerConfigurator(props, context) {
			var _this;
			_classCallCheck2(this, LayerConfigurator);
			_this = _possibleConstructorReturn(this, (LayerConfigurator.__proto__ || Object.getPrototypeOf(LayerConfigurator)).call(this));
			_this;
			/** @type {!Array} */
			_this.readers = [];
			/** @type {boolean} */
			_this.verbose = true === context;
			var events = props ? props.get(node.POSSIBLE_FORMATS) : null;
			var newName = props && void 0 !== props.get(node.ASSUME_CODE_39_CHECK_DIGIT);
			if (events) {
				if (events.includes(change.EAN_13) || events.includes(change.UPC_A) || events.includes(change.EAN_8) || events.includes(change.UPC_E)) {
					_this.readers.push(new ObjectType(props));
				}
				if (events.includes(change.CODE_39)) {
					_this.readers.push(new Sprite(newName));
				}
				if (events.includes(change.CODE_128)) {
					_this.readers.push(new module);
				}
				if (events.includes(change.ITF)) {
					_this.readers.push(new EdgeDrawer);
				}
				if (events.includes(change.RSS_14)) {
					_this.readers.push(new tile);
				}
				if (events.includes(change.RSS_EXPANDED)) {
					_this.readers.push(new Queue(_this.verbose));
				}
			}
			if (0 === _this.readers.length) {
				_this.readers.push(new ObjectType(props));
				_this.readers.push(new Sprite);
				_this.readers.push(new ObjectType(props));
				_this.readers.push(new module);
				_this.readers.push(new EdgeDrawer);
				_this.readers.push(new tile);
				_this.readers.push(new Queue(_this.verbose));
			}
			return _this;
		}
		_inherits(LayerConfigurator, _WebInspector$GeneralTreeElement);
		_createClass2(LayerConfigurator, [{
			key: "decodeRow",
			value: function Url$parse(expectedHashCode, str, parseQueryString) {
				/** @type {number} */
				var i = 0;
				for (; i < this.readers.length; i++) {
					try {
						return this.readers[i].decodeRow(expectedHashCode, str, parseQueryString);
					} catch (t) {
					}
				}
				throw new TypeError;
			}
		}, {
			key: "reset",
			value: function enumerate_permissions() {
				this.readers.forEach(function (applyViewModelsSpy) {
					return applyViewModelsSpy.reset();
				});
			}
		}]);
		return LayerConfigurator;
	}(path);
	var I32BinOp = function () {
		/**
		 * @param {?} alignmentPatternCenters
		 * @param {?} ecBlocks1
		 * @param {?} args
		 * @return {undefined}
		 */
		function Version(alignmentPatternCenters, ecBlocks1, args) {
			_classCallCheck2(this, Version);
			this.ecCodewords = alignmentPatternCenters;
			/** @type {!Array} */
			this.ecBlocks = [ecBlocks1];
			if (args) {
				this.ecBlocks.push(args);
			}
		}
		_createClass2(Version, [{
			key: "getECCodewords",
			value: function getECCodewords() {
				return this.ecCodewords;
			}
		}, {
			key: "getECBlocks",
			value: function getECBlocks() {
				return this.ecBlocks;
			}
		}]);
		return Version;
	}();
	var I64CompareFunc = function () {
		/**
		 * @param {number} alignmentPatternCenters
		 * @param {number} ecBlocks1
		 * @return {undefined}
		 */
		function Version(alignmentPatternCenters, ecBlocks1) {
			_classCallCheck2(this, Version);
			/** @type {number} */
			this.count = alignmentPatternCenters;
			/** @type {number} */
			this.dataCodewords = ecBlocks1;
		}
		_createClass2(Version, [{
			key: "getCount",
			value: function count() {
				return this.count;
			}
		}, {
			key: "getDataCodewords",
			value: function getDataCodewords() {
				return this.dataCodewords;
			}
		}]);
		return Version;
	}();
	var Stomp = function () {
		/**
		 * @param {number} versionNumber
		 * @param {?} alignmentPatternCenters
		 * @param {?} ecBlocks3
		 * @param {?} ecBlocks4
		 * @param {?} ecCodewordsPerBlock
		 * @param {!Array} ecBlocks1
		 * @return {undefined}
		 */
		function Version(versionNumber, alignmentPatternCenters, ecBlocks3, ecBlocks4, ecCodewordsPerBlock, ecBlocks1) {
			_classCallCheck2(this, Version);
			/** @type {number} */
			this.versionNumber = versionNumber;
			this.symbolSizeRows = alignmentPatternCenters;
			this.symbolSizeColumns = ecBlocks3;
			this.dataRegionSizeRows = ecBlocks4;
			this.dataRegionSizeColumns = ecCodewordsPerBlock;
			/** @type {!Array} */
			this.ecBlocks = ecBlocks1;
			/** @type {number} */
			var total = 0;
			var a = ecBlocks1.getECCodewords();
			var vmArgSetters = ecBlocks1.getECBlocks();
			/** @type {boolean} */
			var _iteratorNormalCompletion3 = true;
			/** @type {boolean} */
			var _didIteratorError34 = false;
			var _iteratorError17 = undefined;
			try {
				var _iterator3 = vmArgSetters[Symbol.iterator]();
				var $__6;
				for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var item = $__6.value;
					/** @type {number} */
					total = total + item.getCount() * (item.getDataCodewords() + a);
				}
			} catch (err) {
				/** @type {boolean} */
				_didIteratorError34 = true;
				_iteratorError17 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError34) {
						throw _iteratorError17;
					}
				}
			}
			/** @type {number} */
			this.totalCodewords = total;
		}
		_createClass2(Version, [{
			key: "getVersionNumber",
			value: function getVersionNumber() {
				return this.versionNumber;
			}
		}, {
			key: "getSymbolSizeRows",
			value: function getSymbolSizeRows() {
				return this.symbolSizeRows;
			}
		}, {
			key: "getSymbolSizeColumns",
			value: function getSymbolSizeColumns() {
				return this.symbolSizeColumns;
			}
		}, {
			key: "getDataRegionSizeRows",
			value: function getDataRegionSizeRows() {
				return this.dataRegionSizeRows;
			}
		}, {
			key: "getDataRegionSizeColumns",
			value: function getDataRegionSizeColumns() {
				return this.dataRegionSizeColumns;
			}
		}, {
			key: "getTotalCodewords",
			value: function getTotalCodewords() {
				return this.totalCodewords;
			}
		}, {
			key: "getECBlocks",
			value: function getECBlocks() {
				return this.ecBlocks;
			}
		}, {
			key: "toString",
			value: function uaMatch() {
				return "" + this.versionNumber;
			}
		}], [{
			key: "getVersionForDimensions",
			value: function _getMinMaxDates(headOffX, headOffY) {
				if (0 != (1 & headOffX) || 0 != (1 & headOffY)) {
					throw new Date;
				}
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError35 = false;
				var _iteratorError17 = undefined;
				try {
					var _iterator3 = Version.VERSIONS[Symbol.iterator]();
					var _step;
					for (; !(_iteratorNormalCompletion3 = (_step = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var syncData = _step.value;
						if (syncData.symbolSizeRows === headOffX && syncData.symbolSizeColumns === headOffY) {
							return syncData;
						}
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError35 = true;
					_iteratorError17 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError35) {
							throw _iteratorError17;
						}
					}
				}
				throw new Date;
			}
		}, {
			key: "buildVersions",
			value: function translateFunctionCode() {
				return [new Version(1, 10, 10, 8, 8, new I32BinOp(5, new I64CompareFunc(1, 3))), new Version(2, 12, 12, 10, 10, new I32BinOp(7, new I64CompareFunc(1, 5))), new Version(3, 14, 14, 12, 12, new I32BinOp(10, new I64CompareFunc(1, 8))), new Version(4, 16, 16, 14, 14, new I32BinOp(12, new I64CompareFunc(1, 12))), new Version(5, 18, 18, 16, 16, new I32BinOp(14, new I64CompareFunc(1, 18))), new Version(6, 20, 20, 18, 18, new I32BinOp(18, new I64CompareFunc(1, 22))), new Version(7, 22, 22, 20, 20,
					new I32BinOp(20, new I64CompareFunc(1, 30))), new Version(8, 24, 24, 22, 22, new I32BinOp(24, new I64CompareFunc(1, 36))), new Version(9, 26, 26, 24, 24, new I32BinOp(28, new I64CompareFunc(1, 44))), new Version(10, 32, 32, 14, 14, new I32BinOp(36, new I64CompareFunc(1, 62))), new Version(11, 36, 36, 16, 16, new I32BinOp(42, new I64CompareFunc(1, 86))), new Version(12, 40, 40, 18, 18, new I32BinOp(48, new I64CompareFunc(1, 114))), new Version(13, 44, 44, 20, 20, new I32BinOp(56, new I64CompareFunc(1,
						144))), new Version(14, 48, 48, 22, 22, new I32BinOp(68, new I64CompareFunc(1, 174))), new Version(15, 52, 52, 24, 24, new I32BinOp(42, new I64CompareFunc(2, 102))), new Version(16, 64, 64, 14, 14, new I32BinOp(56, new I64CompareFunc(2, 140))), new Version(17, 72, 72, 16, 16, new I32BinOp(36, new I64CompareFunc(4, 92))), new Version(18, 80, 80, 18, 18, new I32BinOp(48, new I64CompareFunc(4, 114))), new Version(19, 88, 88, 20, 20, new I32BinOp(56, new I64CompareFunc(4, 144))), new Version(20,
							96, 96, 22, 22, new I32BinOp(68, new I64CompareFunc(4, 174))), new Version(21, 104, 104, 24, 24, new I32BinOp(56, new I64CompareFunc(6, 136))), new Version(22, 120, 120, 18, 18, new I32BinOp(68, new I64CompareFunc(6, 175))), new Version(23, 132, 132, 20, 20, new I32BinOp(62, new I64CompareFunc(8, 163))), new Version(24, 144, 144, 22, 22, new I32BinOp(62, new I64CompareFunc(8, 156), new I64CompareFunc(2, 155))), new Version(25, 8, 18, 6, 16, new I32BinOp(7, new I64CompareFunc(1, 5))), new Version(26,
								8, 32, 6, 14, new I32BinOp(11, new I64CompareFunc(1, 10))), new Version(27, 12, 26, 10, 24, new I32BinOp(14, new I64CompareFunc(1, 16))), new Version(28, 12, 36, 10, 16, new I32BinOp(18, new I64CompareFunc(1, 22))), new Version(29, 16, 36, 14, 16, new I32BinOp(24, new I64CompareFunc(1, 32))), new Version(30, 16, 48, 14, 22, new I32BinOp(28, new I64CompareFunc(1, 49)))];
			}
		}]);
		return Version;
	}();
	Stomp.VERSIONS = Stomp.buildVersions();
	var Polygone = function () {
		/**
		 * @param {!Object} line
		 * @return {undefined}
		 */
		function exports(line) {
			_classCallCheck2(this, exports);
			var e = line.getHeight();
			if (e < 8 || e > 144 || 0 != (1 & e)) {
				throw new Date;
			}
			this.version = exports.readVersion(line);
			this.mappingBitMatrix = this.extractDataRegion(line);
			this.readMappingMatrix = new Image(this.mappingBitMatrix.getWidth(), this.mappingBitMatrix.getHeight());
		}
		_createClass2(exports, [{
			key: "getVersion",
			value: function version() {
				return this.version;
			}
		}, {
			key: "readCodewords",
			value: function loadData() {
				/** @type {!Int8Array} */
				var data = new Int8Array(this.version.getTotalCodewords());
				/** @type {number} */
				var off = 0;
				/** @type {number} */
				var j = 4;
				/** @type {number} */
				var k = 0;
				var i = this.mappingBitMatrix.getHeight();
				var len = this.mappingBitMatrix.getWidth();
				/** @type {boolean} */
				var o = false;
				/** @type {boolean} */
				var a = false;
				/** @type {boolean} */
				var l = false;
				/** @type {boolean} */
				var h = false;
				do {
					if (j !== i || 0 !== k || o) {
						if (j !== i - 2 || 0 !== k || 0 == (3 & len) || a) {
							if (j !== i + 4 || 2 !== k || 0 != (7 & len) || l) {
								if (j !== i - 2 || 0 !== k || 4 != (7 & len) || h) {
									do {
										if (j < i && k >= 0 && !this.readMappingMatrix.get(k, j)) {
											/** @type {number} */
											data[off++] = 255 & this.readUtah(j, k, i, len);
										}
										/** @type {number} */
										j = j - 2;
										/** @type {number} */
										k = k + 2;
									} while (j >= 0 && k < len);
									j = j + 1;
									/** @type {number} */
									k = k + 3;
									do {
										if (j >= 0 && k < len && !this.readMappingMatrix.get(k, j)) {
											/** @type {number} */
											data[off++] = 255 & this.readUtah(j, k, i, len);
										}
										j = j + 2;
										/** @type {number} */
										k = k - 2;
									} while (j < i && k >= 0);
									j = j + 3;
									/** @type {number} */
									k = k + 1;
								} else {
									/** @type {number} */
									data[off++] = 255 & this.readCorner4(i, len);
									/** @type {number} */
									j = j - 2;
									/** @type {number} */
									k = k + 2;
									/** @type {boolean} */
									h = true;
								}
							} else {
								/** @type {number} */
								data[off++] = 255 & this.readCorner3(i, len);
								/** @type {number} */
								j = j - 2;
								/** @type {number} */
								k = k + 2;
								/** @type {boolean} */
								l = true;
							}
						} else {
							/** @type {number} */
							data[off++] = 255 & this.readCorner2(i, len);
							/** @type {number} */
							j = j - 2;
							/** @type {number} */
							k = k + 2;
							/** @type {boolean} */
							a = true;
						}
					} else {
						/** @type {number} */
						data[off++] = 255 & this.readCorner1(i, len);
						/** @type {number} */
						j = j - 2;
						/** @type {number} */
						k = k + 2;
						/** @type {boolean} */
						o = true;
					}
				} while (j < i || k < len);
				if (off !== this.version.getTotalCodewords()) {
					throw new Date;
				}
				return data;
			}
		}, {
			key: "readModule",
			value: function pushResult(pos, i, n, rn) {
				return pos < 0 && (pos = pos + n, i = i + (4 - (n + 4 & 7))), i < 0 && (i = i + rn, pos = pos + (4 - (rn + 4 & 7))), this.readMappingMatrix.set(i, pos), this.mappingBitMatrix.get(i, pos);
			}
		}, {
			key: "readUtah",
			value: function $get(i, name, $location, $upload) {
				/** @type {number} */
				var value = 0;
				return this.readModule(i - 2, name - 2, $location, $upload) && (value = value | 1), value = value << 1, this.readModule(i - 2, name - 1, $location, $upload) && (value = value | 1), value = value << 1, this.readModule(i - 1, name - 2, $location, $upload) && (value = value | 1), value = value << 1, this.readModule(i - 1, name - 1, $location, $upload) && (value = value | 1), value = value << 1, this.readModule(i - 1, name, $location, $upload) && (value = value | 1), value = value << 1, this.readModule(i,
					name - 2, $location, $upload) && (value = value | 1), value = value << 1, this.readModule(i, name - 1, $location, $upload) && (value = value | 1), value = value << 1, this.readModule(i, name, $location, $upload) && (value = value | 1), value;
			}
		}, {
			key: "readCorner1",
			value: function $get(mmCoreSplitViewBlock, $state) {
				/** @type {number} */
				var r = 0;
				return this.readModule(mmCoreSplitViewBlock - 1, 0, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1, this.readModule(mmCoreSplitViewBlock - 1, 1, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1, this.readModule(mmCoreSplitViewBlock - 1, 2, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1, this.readModule(0, $state - 2, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1, this.readModule(0, $state - 1, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1,
					this.readModule(1, $state - 1, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1, this.readModule(2, $state - 1, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1, this.readModule(3, $state - 1, mmCoreSplitViewBlock, $state) && (r = r | 1), r;
			}
		}, {
			key: "readCorner2",
			value: function $get(mmCoreSplitViewBlock, $state) {
				/** @type {number} */
				var r = 0;
				return this.readModule(mmCoreSplitViewBlock - 3, 0, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1, this.readModule(mmCoreSplitViewBlock - 2, 0, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1, this.readModule(mmCoreSplitViewBlock - 1, 0, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1, this.readModule(0, $state - 4, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1, this.readModule(0, $state - 3, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1,
					this.readModule(0, $state - 2, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1, this.readModule(0, $state - 1, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1, this.readModule(1, $state - 1, mmCoreSplitViewBlock, $state) && (r = r | 1), r;
			}
		}, {
			key: "readCorner3",
			value: function $get(mmCoreSplitViewBlock, $state) {
				/** @type {number} */
				var r = 0;
				return this.readModule(mmCoreSplitViewBlock - 1, 0, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1, this.readModule(mmCoreSplitViewBlock - 1, $state - 1, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1, this.readModule(0, $state - 3, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1, this.readModule(0, $state - 2, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1, this.readModule(0, $state - 1, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1, this.readModule(1,
					$state - 3, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1, this.readModule(1, $state - 2, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1, this.readModule(1, $state - 1, mmCoreSplitViewBlock, $state) && (r = r | 1), r;
			}
		}, {
			key: "readCorner4",
			value: function $get(mmCoreSplitViewBlock, $state) {
				/** @type {number} */
				var r = 0;
				return this.readModule(mmCoreSplitViewBlock - 3, 0, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1, this.readModule(mmCoreSplitViewBlock - 2, 0, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1, this.readModule(mmCoreSplitViewBlock - 1, 0, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1, this.readModule(0, $state - 2, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1, this.readModule(0, $state - 1, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1,
					this.readModule(1, $state - 1, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1, this.readModule(2, $state - 1, mmCoreSplitViewBlock, $state) && (r = r | 1), r = r << 1, this.readModule(3, $state - 1, mmCoreSplitViewBlock, $state) && (r = r | 1), r;
			}
		}, {
			key: "extractDataRegion",
			value: function open(app) {
				var nTruck = this.version.getSymbolSizeRows();
				var z = this.version.getSymbolSizeColumns();
				if (app.getHeight() !== nTruck) {
					throw new Function("Dimension of bitMatrix must match the version size");
				}
				var n = this.version.getDataRegionSizeRows();
				var w = this.version.getDataRegionSizeColumns();
				/** @type {number} */
				var h = nTruck / n | 0;
				/** @type {number} */
				var r = z / w | 0;
				var node = new Image(r * w, h * n);
				/** @type {number} */
				var y = 0;
				for (; y < h; ++y) {
					/** @type {number} */
					var x = y * n;
					/** @type {number} */
					var v = 0;
					for (; v < r; ++v) {
						/** @type {number} */
						var sx = v * w;
						/** @type {number} */
						var i = 0;
						for (; i < n; ++i) {
							/** @type {number} */
							var symlinkd = y * (n + 2) + 1 + i;
							/** @type {number} */
							var uid = x + i;
							/** @type {number} */
							var r = 0;
							for (; r < w; ++r) {
								/** @type {number} */
								var path = v * (w + 2) + 1 + r;
								if (app.get(path, symlinkd)) {
									/** @type {number} */
									var x = sx + r;
									node.set(x, uid);
								}
							}
						}
					}
				}
				return node;
			}
		}], [{
			key: "readVersion",
			value: function scrollFn(event) {
				var url = event.getHeight();
				var headers = event.getWidth();
				return Stomp.getVersionForDimensions(url, headers);
			}
		}]);
		return exports;
	}();
	var liveLibs = function () {
		/**
		 * @param {?} element
		 * @param {!Object} options
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3(element, options) {
			_classCallCheck2(this, TempusDominusBootstrap3);
			this.numDataCodewords = element;
			/** @type {!Object} */
			this.codewords = options;
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "getNumDataCodewords",
			value: function getNumDataCodewords() {
				return this.numDataCodewords;
			}
		}, {
			key: "getCodewords",
			value: function getCodewords() {
				return this.codewords;
			}
		}], [{
			key: "getDataBlocks",
			value: function init(value, version) {
				var ecBlocks = version.getECBlocks();
				/** @type {number} */
				var totalBlocks = 0;
				var vmArgSetters = ecBlocks.getECBlocks();
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError36 = false;
				var _iteratorError17 = undefined;
				try {
					var _iterator3 = vmArgSetters[Symbol.iterator]();
					var $__6;
					for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var item = $__6.value;
						totalBlocks = totalBlocks + item.getCount();
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError36 = true;
					_iteratorError17 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError36) {
							throw _iteratorError17;
						}
					}
				}
				/** @type {!Array} */
				var result = new Array(totalBlocks);
				/** @type {number} */
				var index = 0;
				/** @type {boolean} */
				var _iteratorNormalCompletion4 = true;
				/** @type {boolean} */
				var _didIteratorError37 = false;
				var _iteratorError18 = undefined;
				try {
					var _iterator4 = vmArgSetters[Symbol.iterator]();
					var $__6;
					for (; !(_iteratorNormalCompletion4 = ($__6 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
						var item = $__6.value;
						/** @type {number} */
						var _e99 = 0;
						for (; _e99 < item.getCount(); _e99++) {
							var length = item.getDataCodewords();
							var newLength = ecBlocks.getECCodewords() + length;
							result[index++] = new TempusDominusBootstrap3(length, new Uint8Array(newLength));
						}
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError37 = true;
					_iteratorError18 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion4 && _iterator4.return) {
							_iterator4.return();
						}
					} finally {
						if (_didIteratorError37) {
							throw _iteratorError18;
						}
					}
				}
				/** @type {number} */
				var neededLength = result[0].codewords.length - ecBlocks.getECCodewords();
				/** @type {number} */
				var zerosToAdd = neededLength - 1;
				/** @type {number} */
				var key = 0;
				/** @type {number} */
				var i = 0;
				for (; i < zerosToAdd; i++) {
					/** @type {number} */
					var j = 0;
					for (; j < index; j++) {
						result[j].codewords[i] = value[key++];
					}
				}
				/** @type {boolean} */
				var keys = 24 === version.getVersionNumber();
				/** @type {number} */
				var currentKey = keys ? 8 : index;
				/** @type {number} */
				var j = 0;
				for (; j < currentKey; j++) {
					result[j].codewords[neededLength - 1] = value[key++];
				}
				var numNow = result[0].codewords.length;
				/** @type {number} */
				var start = neededLength;
				for (; start < numNow; start++) {
					/** @type {number} */
					var pos = 0;
					for (; pos < index; pos++) {
						/** @type {number} */
						var j = keys ? (pos + 8) % index : pos;
						/** @type {number} */
						var i = keys && j > 7 ? start - 1 : start;
						result[j].codewords[i] = value[key++];
					}
				}
				if (key !== value.length) {
					throw new Function;
				}
				return result;
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var SyntaxError = function () {
		/**
		 * @param {!Array} options
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3(options) {
			_classCallCheck2(this, TempusDominusBootstrap3);
			/** @type {!Array} */
			this.bytes = options;
			/** @type {number} */
			this.byteOffset = 0;
			/** @type {number} */
			this.bitOffset = 0;
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "getBitOffset",
			value: function getBitOffset() {
				return this.bitOffset;
			}
		}, {
			key: "getByteOffset",
			value: function getByteOffset() {
				return this.byteOffset;
			}
		}, {
			key: "readBits",
			value: function init(numBits) {
				if (numBits < 1 || numBits > 32 || numBits > this.available()) {
					throw new Function("" + numBits);
				}
				/** @type {number} */
				var result = 0;
				var bitPos = this.bitOffset;
				var i = this.byteOffset;
				var bytes = this.bytes;
				if (bitPos > 0) {
					/** @type {number} */
					var bitsLeft = 8 - bitPos;
					var toRead = numBits < bitsLeft ? numBits : bitsLeft;
					/** @type {number} */
					var bitsToNotRead = bitsLeft - toRead;
					/** @type {number} */
					var mask = 255 >> 8 - toRead << bitsToNotRead;
					/** @type {number} */
					result = (bytes[i] & mask) >> bitsToNotRead;
					/** @type {number} */
					numBits = numBits - toRead;
					if (8 === (bitPos = bitPos + toRead)) {
						/** @type {number} */
						bitPos = 0;
						i++;
					}
				}
				if (numBits > 0) {
					for (; numBits >= 8;) {
						/** @type {number} */
						result = result << 8 | 255 & bytes[i];
						i++;
						/** @type {number} */
						numBits = numBits - 8;
					}
					if (numBits > 0) {
						/** @type {number} */
						var bitsToNotRead = 8 - numBits;
						/** @type {number} */
						var mask = 255 >> bitsToNotRead << bitsToNotRead;
						/** @type {number} */
						result = result << numBits | (bytes[i] & mask) >> bitsToNotRead;
						bitPos = bitPos + numBits;
					}
				}
				return this.bitOffset = bitPos, this.byteOffset = i, result;
			}
		}, {
			key: "available",
			value: function getter() {
				return 8 * (this.bytes.length - this.byteOffset) - this.bitOffset;
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	!function (canCreateDiscussions) {
		/** @type {string} */
		canCreateDiscussions[canCreateDiscussions.PAD_ENCODE = 0] = "PAD_ENCODE";
		/** @type {string} */
		canCreateDiscussions[canCreateDiscussions.ASCII_ENCODE = 1] = "ASCII_ENCODE";
		/** @type {string} */
		canCreateDiscussions[canCreateDiscussions.C40_ENCODE = 2] = "C40_ENCODE";
		/** @type {string} */
		canCreateDiscussions[canCreateDiscussions.TEXT_ENCODE = 3] = "TEXT_ENCODE";
		/** @type {string} */
		canCreateDiscussions[canCreateDiscussions.ANSIX12_ENCODE = 4] = "ANSIX12_ENCODE";
		/** @type {string} */
		canCreateDiscussions[canCreateDiscussions.EDIFACT_ENCODE = 5] = "EDIFACT_ENCODE";
		/** @type {string} */
		canCreateDiscussions[canCreateDiscussions.BASE256_ENCODE = 6] = "BASE256_ENCODE";
	}(n || (n = {}));
	var config = function () {
		/**
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3() {
			_classCallCheck2(this, TempusDominusBootstrap3);
		}
		_createClass2(TempusDominusBootstrap3, null, [{
			key: "decode",
			value: function run(data) {
				var error = new SyntaxError(data);
				var r = new Buffer;
				var B = new Buffer;
				/** @type {!Array} */
				var body = new Array;
				var p = n.ASCII_ENCODE;
				do {
					if (p === n.ASCII_ENCODE) {
						p = this.decodeAsciiSegment(error, r, B);
					} else {
						switch (p) {
							case n.C40_ENCODE:
								this.decodeC40Segment(error, r);
								break;
							case n.TEXT_ENCODE:
								this.decodeTextSegment(error, r);
								break;
							case n.ANSIX12_ENCODE:
								this.decodeAnsiX12Segment(error, r);
								break;
							case n.EDIFACT_ENCODE:
								this.decodeEdifactSegment(error, r);
								break;
							case n.BASE256_ENCODE:
								this.decodeBase256Segment(error, r, body);
								break;
							default:
								throw new Date;
						}
						p = n.ASCII_ENCODE;
					}
				} while (p !== n.PAD_ENCODE && error.available() > 0);
				return B.length() > 0 && r.append(B.toString()), new Response(data, r.toString(), 0 === body.length ? null : body, null);
			}
		}, {
			key: "decodeAsciiSegment",
			value: function run(source, dest, ext) {
				/** @type {boolean} */
				var q = false;
				do {
					var length = source.readBits(8);
					if (0 === length) {
						throw new Date;
					}
					if (length <= 128) {
						return q && (length = length + 128), dest.append(String.fromCharCode(length - 1)), n.ASCII_ENCODE;
					}
					if (129 === length) {
						return n.PAD_ENCODE;
					}
					if (length <= 229) {
						/** @type {number} */
						var i = length - 130;
						if (i < 10) {
							dest.append("0");
						}
						dest.append("" + i);
					} else {
						switch (length) {
							case 230:
								return n.C40_ENCODE;
							case 231:
								return n.BASE256_ENCODE;
							case 232:
								dest.append(String.fromCharCode(29));
								break;
							case 233:
							case 234:
								break;
							case 235:
								/** @type {boolean} */
								q = true;
								break;
							case 236:
								dest.append("[)>\u001e05\u001d");
								ext.insert(0, "\u001e\u0004");
								break;
							case 237:
								dest.append("[)>\u001e06\u001d");
								ext.insert(0, "\u001e\u0004");
								break;
							case 238:
								return n.ANSIX12_ENCODE;
							case 239:
								return n.TEXT_ENCODE;
							case 240:
								return n.EDIFACT_ENCODE;
							case 241:
								break;
							default:
								if (254 !== length || 0 !== source.available()) {
									throw new Date;
								}
						}
					}
				} while (source.available() > 0);
				return n.ASCII_ENCODE;
			}
		}, {
			key: "decodeC40Segment",
			value: function run(s, t) {
				/** @type {boolean} */
				var r = false;
				/** @type {!Array} */
				var command = [];
				/** @type {number} */
				var startIndex = 0;
				do {
					if (8 === s.available()) {
						return;
					}
					var artistTrack = s.readBits(8);
					if (254 === artistTrack) {
						return;
					}
					this.parseTwoBytes(artistTrack, s.readBits(8), command);
					/** @type {number} */
					var type = 0;
					for (; type < 3; type++) {
						var index = command[type];
						switch (startIndex) {
							case 0:
								if (index < 3) {
									startIndex = index + 1;
								} else {
									if (!(index < this.C40_BASIC_SET_CHARS.length)) {
										throw new Date;
									}
									{
										var callback = this.C40_BASIC_SET_CHARS[index];
										if (r) {
											t.append(String.fromCharCode(callback.charCodeAt(0) + 128));
											/** @type {boolean} */
											r = false;
										} else {
											t.append(callback);
										}
									}
								}
								break;
							case 1:
								if (r) {
									t.append(String.fromCharCode(index + 128));
									/** @type {boolean} */
									r = false;
								} else {
									t.append(String.fromCharCode(index));
								}
								/** @type {number} */
								startIndex = 0;
								break;
							case 2:
								if (index < this.C40_SHIFT2_SET_CHARS.length) {
									var callback = this.C40_SHIFT2_SET_CHARS[index];
									if (r) {
										t.append(String.fromCharCode(callback.charCodeAt(0) + 128));
										/** @type {boolean} */
										r = false;
									} else {
										t.append(callback);
									}
								} else {
									switch (index) {
										case 27:
											t.append(String.fromCharCode(29));
											break;
										case 30:
											/** @type {boolean} */
											r = true;
											break;
										default:
											throw new Date;
									}
								}
								/** @type {number} */
								startIndex = 0;
								break;
							case 3:
								if (r) {
									t.append(String.fromCharCode(index + 224));
									/** @type {boolean} */
									r = false;
								} else {
									t.append(String.fromCharCode(index + 96));
								}
								/** @type {number} */
								startIndex = 0;
								break;
							default:
								throw new Date;
						}
					}
				} while (s.available() > 0);
			}
		}, {
			key: "decodeTextSegment",
			value: function run(s, t) {
				/** @type {boolean} */
				var r = false;
				/** @type {!Array} */
				var command = [];
				/** @type {number} */
				var startIndex = 0;
				do {
					if (8 === s.available()) {
						return;
					}
					var artistTrack = s.readBits(8);
					if (254 === artistTrack) {
						return;
					}
					this.parseTwoBytes(artistTrack, s.readBits(8), command);
					/** @type {number} */
					var type = 0;
					for (; type < 3; type++) {
						var index = command[type];
						switch (startIndex) {
							case 0:
								if (index < 3) {
									startIndex = index + 1;
								} else {
									if (!(index < this.TEXT_BASIC_SET_CHARS.length)) {
										throw new Date;
									}
									{
										var callback = this.TEXT_BASIC_SET_CHARS[index];
										if (r) {
											t.append(String.fromCharCode(callback.charCodeAt(0) + 128));
											/** @type {boolean} */
											r = false;
										} else {
											t.append(callback);
										}
									}
								}
								break;
							case 1:
								if (r) {
									t.append(String.fromCharCode(index + 128));
									/** @type {boolean} */
									r = false;
								} else {
									t.append(String.fromCharCode(index));
								}
								/** @type {number} */
								startIndex = 0;
								break;
							case 2:
								if (index < this.TEXT_SHIFT2_SET_CHARS.length) {
									var callback = this.TEXT_SHIFT2_SET_CHARS[index];
									if (r) {
										t.append(String.fromCharCode(callback.charCodeAt(0) + 128));
										/** @type {boolean} */
										r = false;
									} else {
										t.append(callback);
									}
								} else {
									switch (index) {
										case 27:
											t.append(String.fromCharCode(29));
											break;
										case 30:
											/** @type {boolean} */
											r = true;
											break;
										default:
											throw new Date;
									}
								}
								/** @type {number} */
								startIndex = 0;
								break;
							case 3:
								if (!(index < this.TEXT_SHIFT3_SET_CHARS.length)) {
									throw new Date;
								}
								{
									var callback = this.TEXT_SHIFT3_SET_CHARS[index];
									if (r) {
										t.append(String.fromCharCode(callback.charCodeAt(0) + 128));
										/** @type {boolean} */
										r = false;
									} else {
										t.append(callback);
									}
									/** @type {number} */
									startIndex = 0;
								}
								break;
							default:
								throw new Date;
						}
					}
				} while (s.available() > 0);
			}
		}, {
			key: "decodeAnsiX12Segment",
			value: function run(config, logger) {
				/** @type {!Array} */
				var structuredData = [];
				do {
					if (8 === config.available()) {
						return;
					}
					var GET_AUTH_URL_TIMEOUT = config.readBits(8);
					if (254 === GET_AUTH_URL_TIMEOUT) {
						return;
					}
					this.parseTwoBytes(GET_AUTH_URL_TIMEOUT, config.readBits(8), structuredData);
					/** @type {number} */
					var newTypeName = 0;
					for (; newTypeName < 3; newTypeName++) {
						var charCodeUpperA = structuredData[newTypeName];
						switch (charCodeUpperA) {
							case 0:
								logger.append("\r");
								break;
							case 1:
								logger.append("*");
								break;
							case 2:
								logger.append(">");
								break;
							case 3:
								logger.append(" ");
								break;
							default:
								if (charCodeUpperA < 14) {
									logger.append(String.fromCharCode(charCodeUpperA + 44));
								} else {
									if (!(charCodeUpperA < 40)) {
										throw new Date;
									}
									logger.append(String.fromCharCode(charCodeUpperA + 51));
								}
						}
					}
				} while (config.available() > 0);
			}
		}, {
			key: "parseTwoBytes",
			value: function addError(offset, text, tag) {
				/** @type {number} */
				var scrollLeft = (offset << 8) + text - 1;
				/** @type {number} */
				var i = Math.floor(scrollLeft / 1600);
				/** @type {number} */
				tag[0] = i;
				/** @type {number} */
				scrollLeft = scrollLeft - 1600 * i;
				/** @type {number} */
				i = Math.floor(scrollLeft / 40);
				/** @type {number} */
				tag[1] = i;
				/** @type {number} */
				tag[2] = scrollLeft - 40 * i;
			}
		}, {
			key: "decodeEdifactSegment",
			value: function run(reader, result) {
				do {
					if (reader.available() <= 16) {
						return;
					}
					/** @type {number} */
					var c = 0;
					for (; c < 4; c++) {
						c = reader.readBits(6);
						if (31 === c) {
							/** @type {number} */
							var node = 8 - reader.getBitOffset();
							return void (8 !== node && reader.readBits(node));
						}
						if (0 == (32 & c)) {
							/** @type {number} */
							c = c | 64;
						}
						result.append(String.fromCharCode(c));
					}
				} while (reader.available() > 0);
			}
		}, {
			key: "decodeBase256Segment",
			value: function build(pro, obj, options) {
				var n = 1 + pro.getByteOffset();
				var ratio = this.unrandomize255State(pro.readBits(8), n++);
				var framesize = void 0;
				if ((framesize = 0 === ratio ? pro.available() / 8 | 0 : ratio < 250 ? ratio : 250 * (ratio - 249) + this.unrandomize255State(pro.readBits(8), n++)) < 0) {
					throw new Date;
				}
				/** @type {!Uint8Array} */
				var data = new Uint8Array(framesize);
				/** @type {number} */
				var i = 0;
				for (; i < framesize; i++) {
					if (pro.available() < 8) {
						throw new Date;
					}
					data[i] = this.unrandomize255State(pro.readBits(8), n++);
				}
				options.push(data);
				try {
					obj.append(parser.decode(data, _.ISO88591));
				} catch (controlFlowAction) {
					throw new Path("Platform does not support required encoding: " + controlFlowAction.message);
				}
			}
		}, {
			key: "unrandomize255State",
			value: function handleSlide(isSlidingUp, $cont) {
				/** @type {number} */
				var r = isSlidingUp - (149 * $cont % 255 + 1);
				return r >= 0 ? r : r + 256;
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	/** @type {!Array} */
	config.C40_BASIC_SET_CHARS = ["*", "*", "*", " ", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
	/** @type {!Array} */
	config.C40_SHIFT2_SET_CHARS = ["!", '"', "#", "$", "%", "&", "'", "(", ")", "*", "+", ",", "-", ".", "/", ":", ";", "<", "=", ">", "?", "@", "[", "\\", "]", "^", "_"];
	/** @type {!Array} */
	config.TEXT_BASIC_SET_CHARS = ["*", "*", "*", " ", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
	/** @type {!Array} */
	config.TEXT_SHIFT2_SET_CHARS = config.C40_SHIFT2_SET_CHARS;
	/** @type {!Array} */
	config.TEXT_SHIFT3_SET_CHARS = ["`", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "{", "|", "}", "~", String.fromCharCode(127)];
	var StringDecoder = function () {
		/**
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3() {
			_classCallCheck2(this, TempusDominusBootstrap3);
			this.rsDecoder = new Uint16Array(r.DATA_MATRIX_FIELD_256);
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "decode",
			value: function parse(waypoints) {
				var polygone = new Polygone(waypoints);
				var version = polygone.getVersion();
				var codewords = polygone.readCodewords();
				var dataBlocks = liveLibs.getDataBlocks(codewords, version);
				/** @type {number} */
				var signature = 0;
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError38 = false;
				var _iteratorError17 = undefined;
				try {
					var _iterator3 = dataBlocks[Symbol.iterator]();
					var $__6;
					for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var item = $__6.value;
						signature = signature + item.getNumDataCodewords();
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError38 = true;
					_iteratorError17 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError38) {
							throw _iteratorError17;
						}
					}
				}
				/** @type {!Uint8Array} */
				var result = new Uint8Array(signature);
				var numCurves = dataBlocks.length;
				/** @type {number} */
				var j = 0;
				for (; j < numCurves; j++) {
					var dataBlock = dataBlocks[j];
					var inlineStyles = dataBlock.getCodewords();
					var keyboardShortcutCount = dataBlock.getNumDataCodewords();
					this.correctErrors(inlineStyles, keyboardShortcutCount);
					/** @type {number} */
					var i = 0;
					for (; i < keyboardShortcutCount; i++) {
						result[i * numCurves + j] = inlineStyles[i];
					}
				}
				return config.decode(result);
			}
		}, {
			key: "correctErrors",
			value: function hash(n, s) {
				/** @type {!Int32Array} */
				var data = new Int32Array(n);
				try {
					this.rsDecoder.decode(data, n.length - s);
				} catch (t) {
					throw new Rectangle;
				}
				/** @type {number} */
				var p = 0;
				for (; p < s; p++) {
					/** @type {number} */
					n[p] = data[p];
				}
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var LanguageDetect = function () {
		/**
		 * @param {string} value
		 * @return {undefined}
		 */
		function self(value) {
			_classCallCheck2(this, self);
			/** @type {string} */
			this.image = value;
			this.rectangleDetector = new Element(this.image);
		}
		_createClass2(self, [{
			key: "detect",
			value: function check() {
				var PERIODS_PREVIOUS_X_DAYS = this.rectangleDetector.detect();
				var match = this.detectSolid1(PERIODS_PREVIOUS_X_DAYS);
				if ((match = this.detectSolid2(match))[3] = this.correctTopRight(match), !match[3]) {
					throw new TypeError;
				}
				var value = (match = this.shiftToModuleCenter(match))[0];
				var type = match[1];
				var marker = match[2];
				var width = match[3];
				var x = this.transitionsBetween(value, width) + 1;
				var y = this.transitionsBetween(marker, width) + 1;
				if (1 == (1 & x)) {
					x = x + 1;
				}
				if (1 == (1 & y)) {
					y = y + 1;
				}
				if (4 * x < 7 * y && 4 * y < 7 * x) {
					/** @type {number} */
					x = y = Math.max(x, y);
				}
				var info = self.sampleGrid(this.image, value, type, marker, width, x, y);
				return new HTMLTagSpecification(info, [value, type, marker, width]);
			}
		}, {
			key: "detectSolid1",
			value: function isLikeHSLA(t) {
				var f = t[0];
				var value = t[1];
				var e = t[3];
				var n = t[2];
				var prefix = this.transitionsBetween(f, value);
				var o = this.transitionsBetween(value, e);
				var v = this.transitionsBetween(e, n);
				var s = this.transitionsBetween(n, f);
				var r = prefix;
				/** @type {!Array} */
				var args = [n, f, value, e];
				return r > o && (r = o, args[0] = f, args[1] = value, args[2] = e, args[3] = n), r > v && (r = v, args[0] = value, args[1] = e, args[2] = n, args[3] = f), r > s && (args[0] = e, args[1] = n, args[2] = f, args[3] = value), args;
			}
		}, {
			key: "detectSolid2",
			value: function onComplete(data) {
				var o = data[0];
				var f = data[1];
				var d = data[2];
				var red = data[3];
				var color = this.transitionsBetween(o, red);
				var a = self.shiftPoint(f, d, 4 * (color + 1));
				var b = self.shiftPoint(d, f, 4 * (color + 1));
				return this.transitionsBetween(a, o) < this.transitionsBetween(b, red) ? (data[0] = o, data[1] = f, data[2] = d, data[3] = red) : (data[0] = f, data[1] = d, data[2] = red, data[3] = o), data;
			}
		}, {
			key: "correctTopRight",
			value: function validate(r) {
				var parent = r[0];
				var offset = r[1];
				var container = r[2];
				var options = r[3];
				var svg = this.transitionsBetween(parent, options);
				var point = this.transitionsBetween(offset, options);
				var left = self.shiftPoint(parent, offset, 4 * (point + 1));
				var right = self.shiftPoint(container, offset, 4 * (svg + 1));
				svg = this.transitionsBetween(left, options);
				point = this.transitionsBetween(right, options);
				var text = new type(options.getX() + (container.getX() - offset.getX()) / (svg + 1), options.getY() + (container.getY() - offset.getY()) / (svg + 1));
				var c = new type(options.getX() + (parent.getX() - offset.getX()) / (point + 1), options.getY() + (parent.getY() - offset.getY()) / (point + 1));
				return this.isValid(text) ? this.isValid(c) ? this.transitionsBetween(left, text) + this.transitionsBetween(right, text) > this.transitionsBetween(left, c) + this.transitionsBetween(right, c) ? text : c : text : this.isValid(c) ? c : null;
			}
		}, {
			key: "shiftToModuleCenter",
			value: function parseLine(arr) {
				var t = arr[0];
				var n = arr[1];
				var e = arr[2];
				var b = arr[3];
				var ret = this.transitionsBetween(t, b) + 1;
				var concurrents = this.transitionsBetween(e, b) + 1;
				var a = self.shiftPoint(t, n, 4 * concurrents);
				var result = self.shiftPoint(e, n, 4 * ret);
				if (1 == (1 & (ret = this.transitionsBetween(a, b) + 1))) {
					ret = ret + 1;
				}
				if (1 == (1 & (concurrents = this.transitionsBetween(result, b) + 1))) {
					concurrents = concurrents + 1;
				}
				var r = void 0;
				var m = void 0;
				/** @type {number} */
				var d = (t.getX() + n.getX() + e.getX() + b.getX()) / 4;
				/** @type {number} */
				var p = (t.getY() + n.getY() + e.getY() + b.getY()) / 4;
				return t = self.moveAway(t, d, p), n = self.moveAway(n, d, p), e = self.moveAway(e, d, p), b = self.moveAway(b, d, p), a = self.shiftPoint(t, n, 4 * concurrents), a = self.shiftPoint(a, b, 4 * ret), r = self.shiftPoint(n, t, 4 * concurrents), r = self.shiftPoint(r, e, 4 * ret), result = self.shiftPoint(e, b, 4 * concurrents), result = self.shiftPoint(result, n, 4 * ret), m = self.shiftPoint(b, e, 4 * concurrents), [a, r, result, m = self.shiftPoint(m, t, 4 * ret)];
			}
		}, {
			key: "isValid",
			value: function draw(event) {
				return event.getX() >= 0 && event.getX() < this.image.getWidth() && event.getY() > 0 && event.getY() < this.image.getHeight();
			}
		}, {
			key: "transitionsBetween",
			value: function draw(pixel, e) {
				/** @type {number} */
				var index = Math.trunc(pixel.getX());
				/** @type {number} */
				var key = Math.trunc(pixel.getY());
				/** @type {number} */
				var value = Math.trunc(e.getX());
				/** @type {number} */
				var result = Math.trunc(e.getY());
				/** @type {boolean} */
				var keys = Math.abs(result - key) > Math.abs(value - index);
				if (keys) {
					/** @type {number} */
					var x = index;
					/** @type {number} */
					index = key;
					/** @type {number} */
					key = x;
					/** @type {number} */
					x = value;
					/** @type {number} */
					value = result;
					/** @type {number} */
					result = x;
				}
				/** @type {number} */
				var a = Math.abs(value - index);
				/** @type {number} */
				var chroma = Math.abs(result - key);
				/** @type {number} */
				var b = -a / 2;
				/** @type {number} */
				var typeSuffix = key < result ? 1 : -1;
				/** @type {number} */
				var deltaKnot = index < value ? 1 : -1;
				/** @type {number} */
				var results = 0;
				var initialLetter = this.image.get(keys ? key : index, keys ? index : key);
				/** @type {number} */
				var v = index;
				/** @type {number} */
				var object = key;
				for (; v !== value; v = v + deltaKnot) {
					var letterCandidate = this.image.get(keys ? object : v, keys ? v : object);
					if (letterCandidate !== initialLetter && (results++, initialLetter = letterCandidate), (b = b + chroma) > 0) {
						if (object === result) {
							break;
						}
						/** @type {number} */
						object = object + typeSuffix;
						/** @type {number} */
						b = b - a;
					}
				}
				return results;
			}
		}], [{
			key: "shiftPoint",
			value: function keyPress(item, event, handler) {
				/** @type {number} */
				var n = (event.getX() - item.getX()) / (handler + 1);
				/** @type {number} */
				var offset = (event.getY() - item.getY()) / (handler + 1);
				return new type(item.getX() + n, item.getY() + offset);
			}
		}, {
			key: "moveAway",
			value: function enable(event, data, f) {
				var val = event.getX();
				var h = event.getY();
				return val < data ? val = val - 1 : val = val + 1, h < f ? h = h - 1 : h = h + 1, new type(val, h);
			}
		}, {
			key: "sampleGrid",
			value: function update(showButtons, e, p, f, options, callback, opt_timeoutSeconds) {
				return ServiceManager.getInstance().sampleGrid(showButtons, callback, opt_timeoutSeconds, .5, .5, callback - .5, .5, callback - .5, opt_timeoutSeconds - .5, .5, opt_timeoutSeconds - .5, e.getX(), e.getY(), options.getX(), options.getY(), f.getX(), f.getY(), p.getX(), p.getY());
			}
		}]);
		return self;
	}();
	var Model = function () {
		/**
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3() {
			_classCallCheck2(this, TempusDominusBootstrap3);
			this.decoder = new StringDecoder;
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "decode",
			value: function test(clientCipher) {
				var jQueryNames = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
				var pro = void 0;
				var endRow = void 0;
				if (null != jQueryNames && jQueryNames.has(node.PURE_BARCODE)) {
					var data = TempusDominusBootstrap3.extractPureBits(clientCipher.getBlackMatrix());
					pro = this.decoder.decode(data);
					endRow = TempusDominusBootstrap3.NO_POINTS;
				} else {
					var stream = (new LanguageDetect(clientCipher.getBlackMatrix())).detect();
					pro = this.decoder.decode(stream.getBits());
					endRow = stream.getPoints();
				}
				var gm = pro.getRawBytes();
				var r = new result(pro.getText(), gm, 8 * gm.length, endRow, change.DATA_MATRIX, System.currentTimeMillis());
				var nextMinID = pro.getByteSegments();
				if (null != nextMinID) {
					r.putMetadata(value.BYTE_SEGMENTS, nextMinID);
				}
				var hookCheckOne = pro.getECLevel();
				return null != hookCheckOne && r.putMetadata(value.ERROR_CORRECTION_LEVEL, hookCheckOne), r;
			}
		}, {
			key: "reset",
			value: function reset() {
			}
		}], [{
			key: "extractPureBits",
			value: function open(m) {
				var data = m.getTopLeftOnBit();
				var n = m.getBottomRightOnBit();
				if (null == data || null == n) {
					throw new TypeError;
				}
				var d = this.moduleSize(data, m);
				var i = data[1];
				var last = n[1];
				var r = data[0];
				/** @type {number} */
				var x = (n[0] - r + 1) / d;
				/** @type {number} */
				var y = (last - i + 1) / d;
				if (x <= 0 || y <= 0) {
					throw new TypeError;
				}
				/** @type {number} */
				var c = d / 2;
				i = i + c;
				r = r + c;
				var result = new Image(x, y);
				/** @type {number} */
				var j = 0;
				for (; j < y; j++) {
					var v2 = i + j * d;
					/** @type {number} */
					var t = 0;
					for (; t < x; t++) {
						if (m.get(r + t * d, v2)) {
							result.set(t, j);
						}
					}
				}
				return result;
			}
		}, {
			key: "moduleSize",
			value: function getTileGrid(index, group) {
				var len = group.getWidth();
				var j = index[0];
				var i = index[1];
				for (; j < len && group.get(j, i);) {
					j++;
				}
				if (j === len) {
					throw new TypeError;
				}
				/** @type {number} */
				var tileGrid = j - index[0];
				if (0 === tileGrid) {
					throw new TypeError;
				}
				return tileGrid;
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	/** @type {!Array} */
	Model.NO_POINTS = [];
	!function (types) {
		/** @type {string} */
		types[types.L = 0] = "L";
		/** @type {string} */
		types[types.M = 1] = "M";
		/** @type {string} */
		types[types.Q = 2] = "Q";
		/** @type {string} */
		types[types.H = 3] = "H";
	}(kb || (kb = {}));
	var ErrorCorrectionLevel = function () {
		/**
		 * @param {!Object} d
		 * @param {?} i
		 * @param {number} r
		 * @return {undefined}
		 */
		function f(d, i, r) {
			_classCallCheck2(this, f);
			/** @type {!Object} */
			this.value = d;
			this.stringValue = i;
			/** @type {number} */
			this.bits = r;
			f.FOR_BITS.set(r, this);
			f.FOR_VALUE.set(d, this);
		}
		_createClass2(f, [{
			key: "getValue",
			value: function getUnifiedOperator() {
				return this.value;
			}
		}, {
			key: "getBits",
			value: function getTarget() {
				return this.bits;
			}
		}, {
			key: "toString",
			value: function getString() {
				return this.stringValue;
			}
		}, {
			key: "equals",
			value: function sortByAlpha(b) {
				if (!(b instanceof f)) {
					return false;
				}
				/** @type {!Object} */
				var other = b;
				return this.value === other.value;
			}
		}], [{
			key: "fromString",
			value: function _getErrorCorrectLevel(ecl) {
				switch (ecl) {
					case "L":
						return f.L;
					case "M":
						return f.M;
					case "Q":
						return f.Q;
					case "H":
						return f.H;
					default:
						throw new Renderer(ecl + "not available");
				}
			}
		}, {
			key: "forBits",
			value: function init(y) {
				if (y < 0 || y >= f.FOR_BITS.size) {
					throw new Function;
				}
				return f.FOR_BITS.get(y);
			}
		}]);
		return f;
	}();
	/** @type {!Map} */
	ErrorCorrectionLevel.FOR_BITS = new Map;
	/** @type {!Map} */
	ErrorCorrectionLevel.FOR_VALUE = new Map;
	ErrorCorrectionLevel.L = new ErrorCorrectionLevel(kb.L, "L", 1);
	ErrorCorrectionLevel.M = new ErrorCorrectionLevel(kb.M, "M", 0);
	ErrorCorrectionLevel.Q = new ErrorCorrectionLevel(kb.Q, "Q", 3);
	ErrorCorrectionLevel.H = new ErrorCorrectionLevel(kb.H, "H", 2);
	var FormatInformation = function () {
		/**
		 * @param {number} formatInfo
		 * @return {undefined}
		 */
		function FormatInformation(formatInfo) {
			_classCallCheck2(this, FormatInformation);
			this.errorCorrectionLevel = ErrorCorrectionLevel.forBits(formatInfo >> 3 & 3);
			/** @type {number} */
			this.dataMask = 7 & formatInfo;
		}
		_createClass2(FormatInformation, [{
			key: "getErrorCorrectionLevel",
			value: function getErrorCorrectionLevel() {
				return this.errorCorrectionLevel;
			}
		}, {
			key: "getDataMask",
			value: function getDataMask() {
				return this.dataMask;
			}
		}, {
			key: "hashCode",
			value: function FormatInformation() {
				return this.errorCorrectionLevel.getBits() << 3 | this.dataMask;
			}
		}, {
			key: "equals",
			value: function FormatInformation(b) {
				if (!(b instanceof FormatInformation)) {
					return false;
				}
				/** @type {!Object} */
				var other = b;
				return this.errorCorrectionLevel === other.errorCorrectionLevel && this.dataMask === other.dataMask;
			}
		}], [{
			key: "numBitsDiffering",
			value: function LineSegment2(s, e) {
				return scope.bitCount(s ^ e);
			}
		}, {
			key: "decodeFormatInformation",
			value: function decodeFormatInformation(maskedFormatInfo, targetInfo) {
				var bitsDifference = FormatInformation.doDecodeFormatInformation(maskedFormatInfo, targetInfo);
				return null !== bitsDifference ? bitsDifference : FormatInformation.doDecodeFormatInformation(maskedFormatInfo ^ FormatInformation.FORMAT_INFO_MASK_QR, targetInfo ^ FormatInformation.FORMAT_INFO_MASK_QR);
			}
		}, {
			key: "doDecodeFormatInformation",
			value: function init(h, f) {
				/** @type {number} */
				var min = Number.MAX_SAFE_INTEGER;
				/** @type {number} */
				var bestFormatInfo = 0;
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError39 = false;
				var _iteratorError17 = undefined;
				try {
					var _iterator3 = FormatInformation.FORMAT_INFO_DECODE_LOOKUP[Symbol.iterator]();
					var $__6;
					for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var item = $__6.value;
						var s = item[0];
						if (s === h || s === f) {
							return new FormatInformation(item[1]);
						}
						var t = FormatInformation.numBitsDiffering(h, s);
						if (t < min) {
							bestFormatInfo = item[1];
							min = t;
						}
						if (h !== f && (t = FormatInformation.numBitsDiffering(f, s)) < min) {
							bestFormatInfo = item[1];
							min = t;
						}
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError39 = true;
					_iteratorError17 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError39) {
							throw _iteratorError17;
						}
					}
				}
				return min <= 3 ? new FormatInformation(bestFormatInfo) : null;
			}
		}]);
		return FormatInformation;
	}();
	/** @type {number} */
	FormatInformation.FORMAT_INFO_MASK_QR = 21522;
	/** @type {!Array} */
	FormatInformation.FORMAT_INFO_DECODE_LOOKUP = [Int32Array.from([21522, 0]), Int32Array.from([20773, 1]), Int32Array.from([24188, 2]), Int32Array.from([23371, 3]), Int32Array.from([17913, 4]), Int32Array.from([16590, 5]), Int32Array.from([20375, 6]), Int32Array.from([19104, 7]), Int32Array.from([30660, 8]), Int32Array.from([29427, 9]), Int32Array.from([32170, 10]), Int32Array.from([30877, 11]), Int32Array.from([26159, 12]), Int32Array.from([25368, 13]), Int32Array.from([27713, 14]), Int32Array.from([26998,
		15]), Int32Array.from([5769, 16]), Int32Array.from([5054, 17]), Int32Array.from([7399, 18]), Int32Array.from([6608, 19]), Int32Array.from([1890, 20]), Int32Array.from([597, 21]), Int32Array.from([3340, 22]), Int32Array.from([2107, 23]), Int32Array.from([13663, 24]), Int32Array.from([12392, 25]), Int32Array.from([16177, 26]), Int32Array.from([14854, 27]), Int32Array.from([9396, 28]), Int32Array.from([8579, 29]), Int32Array.from([11994, 30]), Int32Array.from([11245, 31])];
	var ECBlocks = function () {
		/**
		 * @param {number} ecCodewordsPerBlock
		 * @return {undefined}
		 */
		function Version(ecCodewordsPerBlock) {
			_classCallCheck2(this, Version);
			/** @type {number} */
			var _len5 = arguments.length;
			/** @type {!Array} */
			var listenables = Array(_len5 > 1 ? _len5 - 1 : 0);
			/** @type {number} */
			var _key5 = 1;
			for (; _key5 < _len5; _key5++) {
				listenables[_key5 - 1] = arguments[_key5];
			}
			/** @type {number} */
			this.ecCodewordsPerBlock = ecCodewordsPerBlock;
			/** @type {!Array} */
			this.ecBlocks = listenables;
		}
		_createClass2(Version, [{
			key: "getECCodewordsPerBlock",
			value: function getECCodewordsPerBlock() {
				return this.ecCodewordsPerBlock;
			}
		}, {
			key: "getNumBlocks",
			value: function update() {
				/** @type {number} */
				var resp = 0;
				var vmArgSetters = this.ecBlocks;
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError40 = false;
				var _iteratorError17 = undefined;
				try {
					var _iterator3 = vmArgSetters[Symbol.iterator]();
					var $__6;
					for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var item = $__6.value;
						resp = resp + item.getCount();
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError40 = true;
					_iteratorError17 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError40) {
							throw _iteratorError17;
						}
					}
				}
				return resp;
			}
		}, {
			key: "getTotalECCodewords",
			value: function Version() {
				return this.ecCodewordsPerBlock * this.getNumBlocks();
			}
		}, {
			key: "getECBlocks",
			value: function getECBlocks() {
				return this.ecBlocks;
			}
		}]);
		return Version;
	}();
	var ECB = function () {
		/**
		 * @param {number} alignmentPatternCenters
		 * @param {number} ecBlocks1
		 * @return {undefined}
		 */
		function Version(alignmentPatternCenters, ecBlocks1) {
			_classCallCheck2(this, Version);
			/** @type {number} */
			this.count = alignmentPatternCenters;
			/** @type {number} */
			this.dataCodewords = ecBlocks1;
		}
		_createClass2(Version, [{
			key: "getCount",
			value: function count() {
				return this.count;
			}
		}, {
			key: "getDataCodewords",
			value: function getDataCodewords() {
				return this.dataCodewords;
			}
		}]);
		return Version;
	}();
	var Version = function () {
		/**
		 * @param {number} versionNumber
		 * @param {!Object} alignmentPatternCenters
		 * @return {undefined}
		 */
		function Version(versionNumber, alignmentPatternCenters) {
			_classCallCheck2(this, Version);
			/** @type {number} */
			var _len5 = arguments.length;
			/** @type {!Array} */
			var listenables = Array(_len5 > 2 ? _len5 - 2 : 0);
			/** @type {number} */
			var _key5 = 2;
			for (; _key5 < _len5; _key5++) {
				listenables[_key5 - 2] = arguments[_key5];
			}
			/** @type {number} */
			this.versionNumber = versionNumber;
			/** @type {!Object} */
			this.alignmentPatternCenters = alignmentPatternCenters;
			/** @type {!Array} */
			this.ecBlocks = listenables;
			/** @type {number} */
			var total = 0;
			var i = listenables[0].getECCodewordsPerBlock();
			var vmArgSetters = listenables[0].getECBlocks();
			/** @type {boolean} */
			var _iteratorNormalCompletion3 = true;
			/** @type {boolean} */
			var _didIteratorError41 = false;
			var _iteratorError17 = undefined;
			try {
				var _iterator3 = vmArgSetters[Symbol.iterator]();
				var $__6;
				for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var item = $__6.value;
					/** @type {number} */
					total = total + item.getCount() * (item.getDataCodewords() + i);
				}
			} catch (err) {
				/** @type {boolean} */
				_didIteratorError41 = true;
				_iteratorError17 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError41) {
						throw _iteratorError17;
					}
				}
			}
			/** @type {number} */
			this.totalCodewords = total;
		}
		_createClass2(Version, [{
			key: "getVersionNumber",
			value: function getVersionNumber() {
				return this.versionNumber;
			}
		}, {
			key: "getAlignmentPatternCenters",
			value: function getAlignmentPatternCenters() {
				return this.alignmentPatternCenters;
			}
		}, {
			key: "getTotalCodewords",
			value: function getTotalCodewords() {
				return this.totalCodewords;
			}
		}, {
			key: "getDimensionForVersion",
			value: function currentVersion() {
				return 17 + 4 * this.versionNumber;
			}
		}, {
			key: "getECBlocksForLevel",
			value: function Version(current) {
				return this.ecBlocks[current.getValue()];
			}
		}, {
			key: "buildFunctionPattern",
			value: function Version() {
				var dimension = this.getDimensionForVersion();
				var bitMatrix = new Image(dimension);
				bitMatrix.setRegion(0, 0, 9, 9);
				bitMatrix.setRegion(dimension - 8, 0, 8, 9);
				bitMatrix.setRegion(0, dimension - 8, 9, 8);
				var max = this.alignmentPatternCenters.length;
				/** @type {number} */
				var min = 0;
				for (; min < max; min++) {
					/** @type {number} */
					var i = this.alignmentPatternCenters[min] - 2;
					/** @type {number} */
					var x = 0;
					for (; x < max; x++) {
						if (!(0 === min && (0 === x || x === max - 1) || min === max - 1 && 0 === x)) {
							bitMatrix.setRegion(this.alignmentPatternCenters[x] - 2, i, 5, 5);
						}
					}
				}
				return bitMatrix.setRegion(6, 9, 1, dimension - 17), bitMatrix.setRegion(9, 6, dimension - 17, 1), this.versionNumber > 6 && (bitMatrix.setRegion(dimension - 11, 0, 3, 6), bitMatrix.setRegion(0, dimension - 11, 6, 3)), bitMatrix;
			}
		}, {
			key: "toString",
			value: function uaMatch() {
				return "" + this.versionNumber;
			}
		}], [{
			key: "getProvisionalVersionForDimension",
			value: function handleMessageFromWorker(msg) {
				if (msg % 4 != 1) {
					throw new Date;
				}
				try {
					return this.getVersionForNumber((msg - 17) / 4);
				} catch (t) {
					throw new Date;
				}
			}
		}, {
			key: "getVersionForNumber",
			value: function getVersionForNumber(versionNumber) {
				if (versionNumber < 1 || versionNumber > 40) {
					throw new Function;
				}
				return Version.VERSIONS[versionNumber - 1];
			}
		}, {
			key: "decodeVersionInformation",
			value: function stringToInteger(maskedFormatInfo) {
				/** @type {number} */
				var bestDifference = Number.MAX_SAFE_INTEGER;
				/** @type {number} */
				var bestVersion = 0;
				/** @type {number} */
				var i = 0;
				for (; i < Version.VERSION_DECODE_INFO.length; i++) {
					var targetInfo = Version.VERSION_DECODE_INFO[i];
					if (targetInfo === maskedFormatInfo) {
						return Version.getVersionForNumber(i + 7);
					}
					var bitsDifference = FormatInformation.numBitsDiffering(maskedFormatInfo, targetInfo);
					if (bitsDifference < bestDifference) {
						/** @type {number} */
						bestVersion = i + 7;
						bestDifference = bitsDifference;
					}
				}
				return bestDifference <= 3 ? Version.getVersionForNumber(bestVersion) : null;
			}
		}]);
		return Version;
	}();
	/** @type {!Int32Array} */
	Version.VERSION_DECODE_INFO = Int32Array.from([31892, 34236, 39577, 42195, 48118, 51042, 55367, 58893, 63784, 68472, 70749, 76311, 79154, 84390, 87683, 92361, 96236, 102084, 102881, 110507, 110734, 117786, 119615, 126325, 127568, 133589, 136944, 141498, 145311, 150283, 152622, 158308, 161089, 167017]);
	/** @type {!Array} */
	Version.VERSIONS = [new Version(1, new Int32Array(0), new ECBlocks(7, new ECB(1, 19)), new ECBlocks(10, new ECB(1, 16)), new ECBlocks(13, new ECB(1, 13)), new ECBlocks(17, new ECB(1, 9))), new Version(2, Int32Array.from([6, 18]), new ECBlocks(10, new ECB(1, 34)), new ECBlocks(16, new ECB(1, 28)), new ECBlocks(22, new ECB(1, 22)), new ECBlocks(28, new ECB(1, 16))), new Version(3, Int32Array.from([6, 22]), new ECBlocks(15, new ECB(1, 55)), new ECBlocks(26, new ECB(1, 44)), new ECBlocks(18, new ECB(2,
		17)), new ECBlocks(22, new ECB(2, 13))), new Version(4, Int32Array.from([6, 26]), new ECBlocks(20, new ECB(1, 80)), new ECBlocks(18, new ECB(2, 32)), new ECBlocks(26, new ECB(2, 24)), new ECBlocks(16, new ECB(4, 9))), new Version(5, Int32Array.from([6, 30]), new ECBlocks(26, new ECB(1, 108)), new ECBlocks(24, new ECB(2, 43)), new ECBlocks(18, new ECB(2, 15), new ECB(2, 16)), new ECBlocks(22, new ECB(2, 11), new ECB(2, 12))), new Version(6, Int32Array.from([6, 34]), new ECBlocks(18, new ECB(2, 68)),
			new ECBlocks(16, new ECB(4, 27)), new ECBlocks(24, new ECB(4, 19)), new ECBlocks(28, new ECB(4, 15))), new Version(7, Int32Array.from([6, 22, 38]), new ECBlocks(20, new ECB(2, 78)), new ECBlocks(18, new ECB(4, 31)), new ECBlocks(18, new ECB(2, 14), new ECB(4, 15)), new ECBlocks(26, new ECB(4, 13), new ECB(1, 14))), new Version(8, Int32Array.from([6, 24, 42]), new ECBlocks(24, new ECB(2, 97)), new ECBlocks(22, new ECB(2, 38), new ECB(2, 39)), new ECBlocks(22, new ECB(4, 18), new ECB(2, 19)), new ECBlocks(26,
				new ECB(4, 14), new ECB(2, 15))), new Version(9, Int32Array.from([6, 26, 46]), new ECBlocks(30, new ECB(2, 116)), new ECBlocks(22, new ECB(3, 36), new ECB(2, 37)), new ECBlocks(20, new ECB(4, 16), new ECB(4, 17)), new ECBlocks(24, new ECB(4, 12), new ECB(4, 13))), new Version(10, Int32Array.from([6, 28, 50]), new ECBlocks(18, new ECB(2, 68), new ECB(2, 69)), new ECBlocks(26, new ECB(4, 43), new ECB(1, 44)), new ECBlocks(24, new ECB(6, 19), new ECB(2, 20)), new ECBlocks(28, new ECB(6, 15), new ECB(2,
					16))), new Version(11, Int32Array.from([6, 30, 54]), new ECBlocks(20, new ECB(4, 81)), new ECBlocks(30, new ECB(1, 50), new ECB(4, 51)), new ECBlocks(28, new ECB(4, 22), new ECB(4, 23)), new ECBlocks(24, new ECB(3, 12), new ECB(8, 13))), new Version(12, Int32Array.from([6, 32, 58]), new ECBlocks(24, new ECB(2, 92), new ECB(2, 93)), new ECBlocks(22, new ECB(6, 36), new ECB(2, 37)), new ECBlocks(26, new ECB(4, 20), new ECB(6, 21)), new ECBlocks(28, new ECB(7, 14), new ECB(4, 15))), new Version(13,
						Int32Array.from([6, 34, 62]), new ECBlocks(26, new ECB(4, 107)), new ECBlocks(22, new ECB(8, 37), new ECB(1, 38)), new ECBlocks(24, new ECB(8, 20), new ECB(4, 21)), new ECBlocks(22, new ECB(12, 11), new ECB(4, 12))), new Version(14, Int32Array.from([6, 26, 46, 66]), new ECBlocks(30, new ECB(3, 115), new ECB(1, 116)), new ECBlocks(24, new ECB(4, 40), new ECB(5, 41)), new ECBlocks(20, new ECB(11, 16), new ECB(5, 17)), new ECBlocks(24, new ECB(11, 12), new ECB(5, 13))), new Version(15, Int32Array.from([6,
							26, 48, 70]), new ECBlocks(22, new ECB(5, 87), new ECB(1, 88)), new ECBlocks(24, new ECB(5, 41), new ECB(5, 42)), new ECBlocks(30, new ECB(5, 24), new ECB(7, 25)), new ECBlocks(24, new ECB(11, 12), new ECB(7, 13))), new Version(16, Int32Array.from([6, 26, 50, 74]), new ECBlocks(24, new ECB(5, 98), new ECB(1, 99)), new ECBlocks(28, new ECB(7, 45), new ECB(3, 46)), new ECBlocks(24, new ECB(15, 19), new ECB(2, 20)), new ECBlocks(30, new ECB(3, 15), new ECB(13, 16))), new Version(17, Int32Array.from([6,
								30, 54, 78]), new ECBlocks(28, new ECB(1, 107), new ECB(5, 108)), new ECBlocks(28, new ECB(10, 46), new ECB(1, 47)), new ECBlocks(28, new ECB(1, 22), new ECB(15, 23)), new ECBlocks(28, new ECB(2, 14), new ECB(17, 15))), new Version(18, Int32Array.from([6, 30, 56, 82]), new ECBlocks(30, new ECB(5, 120), new ECB(1, 121)), new ECBlocks(26, new ECB(9, 43), new ECB(4, 44)), new ECBlocks(28, new ECB(17, 22), new ECB(1, 23)), new ECBlocks(28, new ECB(2, 14), new ECB(19, 15))), new Version(19, Int32Array.from([6,
									30, 58, 86]), new ECBlocks(28, new ECB(3, 113), new ECB(4, 114)), new ECBlocks(26, new ECB(3, 44), new ECB(11, 45)), new ECBlocks(26, new ECB(17, 21), new ECB(4, 22)), new ECBlocks(26, new ECB(9, 13), new ECB(16, 14))), new Version(20, Int32Array.from([6, 34, 62, 90]), new ECBlocks(28, new ECB(3, 107), new ECB(5, 108)), new ECBlocks(26, new ECB(3, 41), new ECB(13, 42)), new ECBlocks(30, new ECB(15, 24), new ECB(5, 25)), new ECBlocks(28, new ECB(15, 15), new ECB(10, 16))), new Version(21, Int32Array.from([6,
										28, 50, 72, 94]), new ECBlocks(28, new ECB(4, 116), new ECB(4, 117)), new ECBlocks(26, new ECB(17, 42)), new ECBlocks(28, new ECB(17, 22), new ECB(6, 23)), new ECBlocks(30, new ECB(19, 16), new ECB(6, 17))), new Version(22, Int32Array.from([6, 26, 50, 74, 98]), new ECBlocks(28, new ECB(2, 111), new ECB(7, 112)), new ECBlocks(28, new ECB(17, 46)), new ECBlocks(30, new ECB(7, 24), new ECB(16, 25)), new ECBlocks(24, new ECB(34, 13))), new Version(23, Int32Array.from([6, 30, 54, 78, 102]), new ECBlocks(30,
											new ECB(4, 121), new ECB(5, 122)), new ECBlocks(28, new ECB(4, 47), new ECB(14, 48)), new ECBlocks(30, new ECB(11, 24), new ECB(14, 25)), new ECBlocks(30, new ECB(16, 15), new ECB(14, 16))), new Version(24, Int32Array.from([6, 28, 54, 80, 106]), new ECBlocks(30, new ECB(6, 117), new ECB(4, 118)), new ECBlocks(28, new ECB(6, 45), new ECB(14, 46)), new ECBlocks(30, new ECB(11, 24), new ECB(16, 25)), new ECBlocks(30, new ECB(30, 16), new ECB(2, 17))), new Version(25, Int32Array.from([6, 32, 58, 84,
												110]), new ECBlocks(26, new ECB(8, 106), new ECB(4, 107)), new ECBlocks(28, new ECB(8, 47), new ECB(13, 48)), new ECBlocks(30, new ECB(7, 24), new ECB(22, 25)), new ECBlocks(30, new ECB(22, 15), new ECB(13, 16))), new Version(26, Int32Array.from([6, 30, 58, 86, 114]), new ECBlocks(28, new ECB(10, 114), new ECB(2, 115)), new ECBlocks(28, new ECB(19, 46), new ECB(4, 47)), new ECBlocks(28, new ECB(28, 22), new ECB(6, 23)), new ECBlocks(30, new ECB(33, 16), new ECB(4, 17))), new Version(27, Int32Array.from([6,
													34, 62, 90, 118]), new ECBlocks(30, new ECB(8, 122), new ECB(4, 123)), new ECBlocks(28, new ECB(22, 45), new ECB(3, 46)), new ECBlocks(30, new ECB(8, 23), new ECB(26, 24)), new ECBlocks(30, new ECB(12, 15), new ECB(28, 16))), new Version(28, Int32Array.from([6, 26, 50, 74, 98, 122]), new ECBlocks(30, new ECB(3, 117), new ECB(10, 118)), new ECBlocks(28, new ECB(3, 45), new ECB(23, 46)), new ECBlocks(30, new ECB(4, 24), new ECB(31, 25)), new ECBlocks(30, new ECB(11, 15), new ECB(31, 16))), new Version(29,
														Int32Array.from([6, 30, 54, 78, 102, 126]), new ECBlocks(30, new ECB(7, 116), new ECB(7, 117)), new ECBlocks(28, new ECB(21, 45), new ECB(7, 46)), new ECBlocks(30, new ECB(1, 23), new ECB(37, 24)), new ECBlocks(30, new ECB(19, 15), new ECB(26, 16))), new Version(30, Int32Array.from([6, 26, 52, 78, 104, 130]), new ECBlocks(30, new ECB(5, 115), new ECB(10, 116)), new ECBlocks(28, new ECB(19, 47), new ECB(10, 48)), new ECBlocks(30, new ECB(15, 24), new ECB(25, 25)), new ECBlocks(30, new ECB(23, 15),
															new ECB(25, 16))), new Version(31, Int32Array.from([6, 30, 56, 82, 108, 134]), new ECBlocks(30, new ECB(13, 115), new ECB(3, 116)), new ECBlocks(28, new ECB(2, 46), new ECB(29, 47)), new ECBlocks(30, new ECB(42, 24), new ECB(1, 25)), new ECBlocks(30, new ECB(23, 15), new ECB(28, 16))), new Version(32, Int32Array.from([6, 34, 60, 86, 112, 138]), new ECBlocks(30, new ECB(17, 115)), new ECBlocks(28, new ECB(10, 46), new ECB(23, 47)), new ECBlocks(30, new ECB(10, 24), new ECB(35, 25)), new ECBlocks(30,
																new ECB(19, 15), new ECB(35, 16))), new Version(33, Int32Array.from([6, 30, 58, 86, 114, 142]), new ECBlocks(30, new ECB(17, 115), new ECB(1, 116)), new ECBlocks(28, new ECB(14, 46), new ECB(21, 47)), new ECBlocks(30, new ECB(29, 24), new ECB(19, 25)), new ECBlocks(30, new ECB(11, 15), new ECB(46, 16))), new Version(34, Int32Array.from([6, 34, 62, 90, 118, 146]), new ECBlocks(30, new ECB(13, 115), new ECB(6, 116)), new ECBlocks(28, new ECB(14, 46), new ECB(23, 47)), new ECBlocks(30, new ECB(44,
																	24), new ECB(7, 25)), new ECBlocks(30, new ECB(59, 16), new ECB(1, 17))), new Version(35, Int32Array.from([6, 30, 54, 78, 102, 126, 150]), new ECBlocks(30, new ECB(12, 121), new ECB(7, 122)), new ECBlocks(28, new ECB(12, 47), new ECB(26, 48)), new ECBlocks(30, new ECB(39, 24), new ECB(14, 25)), new ECBlocks(30, new ECB(22, 15), new ECB(41, 16))), new Version(36, Int32Array.from([6, 24, 50, 76, 102, 128, 154]), new ECBlocks(30, new ECB(6, 121), new ECB(14, 122)), new ECBlocks(28, new ECB(6, 47),
																		new ECB(34, 48)), new ECBlocks(30, new ECB(46, 24), new ECB(10, 25)), new ECBlocks(30, new ECB(2, 15), new ECB(64, 16))), new Version(37, Int32Array.from([6, 28, 54, 80, 106, 132, 158]), new ECBlocks(30, new ECB(17, 122), new ECB(4, 123)), new ECBlocks(28, new ECB(29, 46), new ECB(14, 47)), new ECBlocks(30, new ECB(49, 24), new ECB(10, 25)), new ECBlocks(30, new ECB(24, 15), new ECB(46, 16))), new Version(38, Int32Array.from([6, 32, 58, 84, 110, 136, 162]), new ECBlocks(30, new ECB(4, 122), new ECB(18,
																			123)), new ECBlocks(28, new ECB(13, 46), new ECB(32, 47)), new ECBlocks(30, new ECB(48, 24), new ECB(14, 25)), new ECBlocks(30, new ECB(42, 15), new ECB(32, 16))), new Version(39, Int32Array.from([6, 26, 54, 82, 110, 138, 166]), new ECBlocks(30, new ECB(20, 117), new ECB(4, 118)), new ECBlocks(28, new ECB(40, 47), new ECB(7, 48)), new ECBlocks(30, new ECB(43, 24), new ECB(22, 25)), new ECBlocks(30, new ECB(10, 15), new ECB(67, 16))), new Version(40, Int32Array.from([6, 30, 58, 86, 114, 142, 170]),
																				new ECBlocks(30, new ECB(19, 118), new ECB(6, 119)), new ECBlocks(28, new ECB(18, 47), new ECB(31, 48)), new ECBlocks(30, new ECB(34, 24), new ECB(34, 25)), new ECBlocks(30, new ECB(20, 15), new ECB(61, 16)))];
	(function (canCreateDiscussions) {
		/** @type {string} */
		canCreateDiscussions[canCreateDiscussions.DATA_MASK_000 = 0] = "DATA_MASK_000";
		/** @type {string} */
		canCreateDiscussions[canCreateDiscussions.DATA_MASK_001 = 1] = "DATA_MASK_001";
		/** @type {string} */
		canCreateDiscussions[canCreateDiscussions.DATA_MASK_010 = 2] = "DATA_MASK_010";
		/** @type {string} */
		canCreateDiscussions[canCreateDiscussions.DATA_MASK_011 = 3] = "DATA_MASK_011";
		/** @type {string} */
		canCreateDiscussions[canCreateDiscussions.DATA_MASK_100 = 4] = "DATA_MASK_100";
		/** @type {string} */
		canCreateDiscussions[canCreateDiscussions.DATA_MASK_101 = 5] = "DATA_MASK_101";
		/** @type {string} */
		canCreateDiscussions[canCreateDiscussions.DATA_MASK_110 = 6] = "DATA_MASK_110";
		/** @type {string} */
		canCreateDiscussions[canCreateDiscussions.DATA_MASK_111 = 7] = "DATA_MASK_111";
	})(fs || (fs = {}));
	var set = function () {
		/**
		 * @param {!Object} options
		 * @param {?} element
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3(options, element) {
			_classCallCheck2(this, TempusDominusBootstrap3);
			/** @type {!Object} */
			this.value = options;
			this.isMasked = element;
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "unmaskBitMatrix",
			value: function encode(bits, points) {
				/** @type {number} */
				var i = 0;
				for (; i < points; i++) {
					/** @type {number} */
					var j = 0;
					for (; j < points; j++) {
						if (this.isMasked(i, j)) {
							bits.flip(j, i);
						}
					}
				}
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	/** @type {!Map} */
	set.values = new Map([[fs.DATA_MASK_000, new set(fs.DATA_MASK_000, function (buckets, name) {
		return 0 == (buckets + name & 1);
	})], [fs.DATA_MASK_001, new set(fs.DATA_MASK_001, function (canCreateDiscussions, isSlidingUp) {
		return 0 == (1 & canCreateDiscussions);
	})], [fs.DATA_MASK_010, new set(fs.DATA_MASK_010, function (canCreateDiscussions, isSlidingUp) {
		return isSlidingUp % 3 == 0;
	})], [fs.DATA_MASK_011, new set(fs.DATA_MASK_011, function (buckets, name) {
		return (buckets + name) % 3 == 0;
	})], [fs.DATA_MASK_100, new set(fs.DATA_MASK_100, function (parentKeyLeft, resumeTime) {
		return 0 == (Math.floor(parentKeyLeft / 2) + Math.floor(resumeTime / 3) & 1);
	})], [fs.DATA_MASK_101, new set(fs.DATA_MASK_101, function (mmCoreSecondsDay, daysInterval) {
		return mmCoreSecondsDay * daysInterval % 6 == 0;
	})], [fs.DATA_MASK_110, new set(fs.DATA_MASK_110, function (mmCoreSecondsDay, daysInterval) {
		return mmCoreSecondsDay * daysInterval % 6 < 3;
	})], [fs.DATA_MASK_111, new set(fs.DATA_MASK_111, function (mByte, kByte) {
		return 0 == (mByte + kByte + mByte * kByte % 3 & 1);
	})]]);
	var CSV = function () {
		/**
		 * @param {!Object} bitMatrix
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3(bitMatrix) {
			_classCallCheck2(this, TempusDominusBootstrap3);
			var e = bitMatrix.getHeight();
			if (e < 21 || 1 != (3 & e)) {
				throw new Date;
			}
			/** @type {!Object} */
			this.bitMatrix = bitMatrix;
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "readFormatInformation",
			value: function BitMatrixParser() {
				if (null !== this.parsedFormatInfo && void 0 !== this.parsedFormatInfo) {
					return this.parsedFormatInfo;
				}
				/** @type {number} */
				var formatInfoBits = 0;
				/** @type {number} */
				var artistTrack = 0;
				for (; artistTrack < 6; artistTrack++) {
					formatInfoBits = this.copyBit(artistTrack, 8, formatInfoBits);
				}
				formatInfoBits = this.copyBit(7, 8, formatInfoBits);
				formatInfoBits = this.copyBit(8, 8, formatInfoBits);
				formatInfoBits = this.copyBit(8, 7, formatInfoBits);
				/** @type {number} */
				var pop = 5;
				for (; pop >= 0; pop--) {
					formatInfoBits = this.copyBit(8, pop, formatInfoBits);
				}
				var len = this.bitMatrix.getHeight();
				/** @type {number} */
				var versionBits = 0;
				/** @type {number} */
				var n = len - 7;
				/** @type {number} */
				var j = len - 1;
				for (; j >= n; j--) {
					versionBits = this.copyBit(8, j, versionBits);
				}
				/** @type {number} */
				var i = len - 8;
				for (; i < len; i++) {
					versionBits = this.copyBit(i, 8, versionBits);
				}
				if (this.parsedFormatInfo = FormatInformation.decodeFormatInformation(formatInfoBits, versionBits), null !== this.parsedFormatInfo) {
					return this.parsedFormatInfo;
				}
				throw new Date;
			}
		}, {
			key: "readVersion",
			value: function BitMatrixParser() {
				if (null !== this.parsedVersion && void 0 !== this.parsedVersion) {
					return this.parsedVersion;
				}
				var len = this.bitMatrix.getHeight();
				/** @type {number} */
				var versionNum = Math.floor((len - 17) / 4);
				if (versionNum <= 6) {
					return Version.getVersionForNumber(versionNum);
				}
				/** @type {number} */
				var versionBits = 0;
				/** @type {number} */
				var n = len - 11;
				/** @type {number} */
				var j = 5;
				for (; j >= 0; j--) {
					/** @type {number} */
					var i = len - 9;
					for (; i >= n; i--) {
						versionBits = this.copyBit(i, j, versionBits);
					}
				}
				var provisionalVersion = Version.decodeVersionInformation(versionBits);
				if (null !== provisionalVersion && provisionalVersion.getDimensionForVersion() === len) {
					return this.parsedVersion = provisionalVersion, provisionalVersion;
				}
				/** @type {number} */
				versionBits = 0;
				/** @type {number} */
				var i = 5;
				for (; i >= 0; i--) {
					/** @type {number} */
					var j = len - 9;
					for (; j >= n; j--) {
						versionBits = this.copyBit(i, j, versionBits);
					}
				}
				if (null !== (provisionalVersion = Version.decodeVersionInformation(versionBits)) && provisionalVersion.getDimensionForVersion() === len) {
					return this.parsedVersion = provisionalVersion, provisionalVersion;
				}
				throw new Date;
			}
		}, {
			key: "copyBit",
			value: function copyBit(i, j, versionBits) {
				return (this.isMirror ? this.bitMatrix.get(j, i) : this.bitMatrix.get(i, j)) ? versionBits << 1 | 1 : versionBits << 1;
			}
		}, {
			key: "readCodewords",
			value: function exports() {
				var formatInfo = this.readFormatInformation();
				var version = this.readVersion();
				var dataMask = set.values.get(formatInfo.getDataMask());
				var dimension = this.bitMatrix.getHeight();
				dataMask.unmaskBitMatrix(this.bitMatrix, dimension);
				var functionPattern = version.buildFunctionPattern();
				/** @type {boolean} */
				var readingUp = true;
				/** @type {!Uint8Array} */
				var bytes = new Uint8Array(version.getTotalCodewords());
				/** @type {number} */
				var i = 0;
				/** @type {number} */
				var value = 0;
				/** @type {number} */
				var h = 0;
				/** @type {number} */
				var j = dimension - 1;
				for (; j > 0; j = j - 2) {
					if (6 === j) {
						j--;
					}
					/** @type {number} */
					var count = 0;
					for (; count < dimension; count++) {
						/** @type {number} */
						var i = readingUp ? dimension - 1 - count : count;
						/** @type {number} */
						var col = 0;
						for (; col < 2; col++) {
							if (!functionPattern.get(j - col, i)) {
								h++;
								/** @type {number} */
								value = value << 1;
								if (this.bitMatrix.get(j - col, i)) {
									/** @type {number} */
									value = value | 1;
								}
								if (8 === h) {
									/** @type {number} */
									bytes[i++] = value;
									/** @type {number} */
									h = 0;
									/** @type {number} */
									value = 0;
								}
							}
						}
					}
					/** @type {boolean} */
					readingUp = !readingUp;
				}
				if (i !== version.getTotalCodewords()) {
					throw new Date;
				}
				return bytes;
			}
		}, {
			key: "remask",
			value: function BitMatrixParser() {
				if (null === this.parsedFormatInfo) {
					return;
				}
				var dataMask = set.values[this.parsedFormatInfo.getDataMask()];
				var dimension = this.bitMatrix.getHeight();
				dataMask.unmaskBitMatrix(this.bitMatrix, dimension);
			}
		}, {
			key: "setMirror",
			value: function BitMatrixParser(bitMatrix) {
				/** @type {null} */
				this.parsedVersion = null;
				/** @type {null} */
				this.parsedFormatInfo = null;
				/** @type {boolean} */
				this.isMirror = bitMatrix;
			}
		}, {
			key: "mirror",
			value: function move() {
				var tile = this.bitMatrix;
				/** @type {number} */
				var x = 0;
				var blockCountY = tile.getWidth();
				for (; x < blockCountY; x++) {
					/** @type {number} */
					var y = x + 1;
					var blockCountY = tile.getHeight();
					for (; y < blockCountY; y++) {
						if (tile.get(x, y) !== tile.get(y, x)) {
							tile.flip(y, x);
							tile.flip(x, y);
						}
					}
				}
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var DataBlock = function () {
		/**
		 * @param {?} element
		 * @param {!Object} options
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3(element, options) {
			_classCallCheck2(this, TempusDominusBootstrap3);
			this.numDataCodewords = element;
			/** @type {!Object} */
			this.codewords = options;
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "getNumDataCodewords",
			value: function getNumDataCodewords() {
				return this.numDataCodewords;
			}
		}, {
			key: "getCodewords",
			value: function getCodewords() {
				return this.codewords;
			}
		}], [{
			key: "getDataBlocks",
			value: function encode(rawCodewords, version, ecLevel) {
				if (rawCodewords.length !== version.getTotalCodewords()) {
					throw new Function;
				}
				var ecBlocks = version.getECBlocksForLevel(ecLevel);
				/** @type {number} */
				var totalBlocks = 0;
				var vmArgSetters = ecBlocks.getECBlocks();
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError42 = false;
				var _iteratorError17 = undefined;
				try {
					var _iterator3 = vmArgSetters[Symbol.iterator]();
					var $__6;
					for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var item = $__6.value;
						totalBlocks = totalBlocks + item.getCount();
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError42 = true;
					_iteratorError17 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError42) {
							throw _iteratorError17;
						}
					}
				}
				/** @type {!Array} */
				var result = new Array(totalBlocks);
				/** @type {number} */
				var i = 0;
				/** @type {boolean} */
				var _iteratorNormalCompletion4 = true;
				/** @type {boolean} */
				var _didIteratorError43 = false;
				var _iteratorError18 = undefined;
				try {
					var _iterator4 = vmArgSetters[Symbol.iterator]();
					var $__6;
					for (; !(_iteratorNormalCompletion4 = ($__6 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
						var item = $__6.value;
						/** @type {number} */
						var _e118 = 0;
						for (; _e118 < item.getCount(); _e118++) {
							var length = item.getDataCodewords();
							var newLength = ecBlocks.getECCodewordsPerBlock() + length;
							result[i++] = new TempusDominusBootstrap3(length, new Uint8Array(newLength));
						}
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError43 = true;
					_iteratorError18 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion4 && _iterator4.return) {
							_iterator4.return();
						}
					} finally {
						if (_didIteratorError43) {
							throw _iteratorError18;
						}
					}
				}
				var shorterBlocksTotalCodewords = result[0].codewords.length;
				/** @type {number} */
				var b = result.length - 1;
				for (; b >= 0;) {
					if (result[b].codewords.length === shorterBlocksTotalCodewords) {
						break;
					}
					b--;
				}
				b++;
				/** @type {number} */
				var shorterBlocksNumDataCodewords = shorterBlocksTotalCodewords - ecBlocks.getECCodewordsPerBlock();
				/** @type {number} */
				var rawCodewordsOffset = 0;
				/** @type {number} */
				var iOffset = 0;
				for (; iOffset < shorterBlocksNumDataCodewords; iOffset++) {
					/** @type {number} */
					var j = 0;
					for (; j < i; j++) {
						result[j].codewords[iOffset] = rawCodewords[rawCodewordsOffset++];
					}
				}
				/** @type {number} */
				var j = b;
				for (; j < i; j++) {
					result[j].codewords[shorterBlocksNumDataCodewords] = rawCodewords[rawCodewordsOffset++];
				}
				var readersLength = result[0].codewords.length;
				/** @type {number} */
				var r = shorterBlocksNumDataCodewords;
				for (; r < readersLength; r++) {
					/** @type {number} */
					var y = 0;
					for (; y < i; y++) {
						/** @type {number} */
						var i = y < b ? r : r + 1;
						result[y].codewords[i] = rawCodewords[rawCodewordsOffset++];
					}
				}
				return result;
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	!function (ModeEnum) {
		/** @type {string} */
		ModeEnum[ModeEnum.TERMINATOR = 0] = "TERMINATOR";
		/** @type {string} */
		ModeEnum[ModeEnum.NUMERIC = 1] = "NUMERIC";
		/** @type {string} */
		ModeEnum[ModeEnum.ALPHANUMERIC = 2] = "ALPHANUMERIC";
		/** @type {string} */
		ModeEnum[ModeEnum.STRUCTURED_APPEND = 3] = "STRUCTURED_APPEND";
		/** @type {string} */
		ModeEnum[ModeEnum.BYTE = 4] = "BYTE";
		/** @type {string} */
		ModeEnum[ModeEnum.ECI = 5] = "ECI";
		/** @type {string} */
		ModeEnum[ModeEnum.KANJI = 6] = "KANJI";
		/** @type {string} */
		ModeEnum[ModeEnum.FNC1_FIRST_POSITION = 7] = "FNC1_FIRST_POSITION";
		/** @type {string} */
		ModeEnum[ModeEnum.FNC1_SECOND_POSITION = 8] = "FNC1_SECOND_POSITION";
		/** @type {string} */
		ModeEnum[ModeEnum.HANZI = 9] = "HANZI";
	}(SimpleInputType || (SimpleInputType = {}));
	var ModeEnum = function () {
		/**
		 * @param {number} m
		 * @param {?} r
		 * @param {?} p
		 * @param {number} f
		 * @return {undefined}
		 */
		function Map(m, r, p, f) {
			_classCallCheck2(this, Map);
			/** @type {number} */
			this.value = m;
			this.stringValue = r;
			this.characterCountBitsForVersions = p;
			/** @type {number} */
			this.bits = f;
			Map.FOR_BITS.set(f, this);
			Map.FOR_VALUE.set(m, this);
		}
		_createClass2(Map, [{
			key: "getCharacterCountBits",
			value: function firebaseRetrieveToken(config) {
				var e = config.getVersionNumber();
				var offset = void 0;
				return offset = e <= 9 ? 0 : e <= 26 ? 1 : 2, this.characterCountBitsForVersions[offset];
			}
		}, {
			key: "getValue",
			value: function getUnifiedOperator() {
				return this.value;
			}
		}, {
			key: "getBits",
			value: function getTarget() {
				return this.bits;
			}
		}, {
			key: "equals",
			value: function sortByAlpha(b) {
				if (!(b instanceof Map)) {
					return false;
				}
				/** @type {!Object} */
				var other = b;
				return this.value === other.value;
			}
		}, {
			key: "toString",
			value: function getString() {
				return this.stringValue;
			}
		}], [{
			key: "forBits",
			value: function _executePhantomJS(script) {
				var e = Map.FOR_BITS.get(script);
				if (void 0 === e) {
					throw new Function;
				}
				return e;
			}
		}]);
		return Map;
	}();
	/** @type {!Map} */
	ModeEnum.FOR_BITS = new Map;
	/** @type {!Map} */
	ModeEnum.FOR_VALUE = new Map;
	ModeEnum.TERMINATOR = new ModeEnum(SimpleInputType.TERMINATOR, "TERMINATOR", Int32Array.from([0, 0, 0]), 0);
	ModeEnum.NUMERIC = new ModeEnum(SimpleInputType.NUMERIC, "NUMERIC", Int32Array.from([10, 12, 14]), 1);
	ModeEnum.ALPHANUMERIC = new ModeEnum(SimpleInputType.ALPHANUMERIC, "ALPHANUMERIC", Int32Array.from([9, 11, 13]), 2);
	ModeEnum.STRUCTURED_APPEND = new ModeEnum(SimpleInputType.STRUCTURED_APPEND, "STRUCTURED_APPEND", Int32Array.from([0, 0, 0]), 3);
	ModeEnum.BYTE = new ModeEnum(SimpleInputType.BYTE, "BYTE", Int32Array.from([8, 16, 16]), 4);
	ModeEnum.ECI = new ModeEnum(SimpleInputType.ECI, "ECI", Int32Array.from([0, 0, 0]), 7);
	ModeEnum.KANJI = new ModeEnum(SimpleInputType.KANJI, "KANJI", Int32Array.from([8, 10, 12]), 8);
	ModeEnum.FNC1_FIRST_POSITION = new ModeEnum(SimpleInputType.FNC1_FIRST_POSITION, "FNC1_FIRST_POSITION", Int32Array.from([0, 0, 0]), 5);
	ModeEnum.FNC1_SECOND_POSITION = new ModeEnum(SimpleInputType.FNC1_SECOND_POSITION, "FNC1_SECOND_POSITION", Int32Array.from([0, 0, 0]), 9);
	ModeEnum.HANZI = new ModeEnum(SimpleInputType.HANZI, "HANZI", Int32Array.from([8, 10, 12]), 13);
	var component = function () {
		/**
		 * @return {undefined}
		 */
		function _() {
			_classCallCheck2(this, _);
		}
		_createClass2(_, null, [{
			key: "decode",
			value: function put(data, version, timeout, spec) {
				var bits = new SyntaxError(data);
				var result = new Buffer;
				/** @type {!Array} */
				var body = new Array;
				/** @type {number} */
				var rPath = -1;
				/** @type {number} */
				var maxLeafs = -1;
				try {
					var mode = void 0;
					/** @type {null} */
					var currentCharacterSet = null;
					/** @type {boolean} */
					var fc1InEffect = false;
					do {
						if (bits.available() < 4) {
							mode = ModeEnum.TERMINATOR;
						} else {
							var byte = bits.readBits(4);
							mode = ModeEnum.forBits(byte);
						}
						switch (mode) {
							case ModeEnum.TERMINATOR:
								break;
							case ModeEnum.FNC1_FIRST_POSITION:
							case ModeEnum.FNC1_SECOND_POSITION:
								/** @type {boolean} */
								fc1InEffect = true;
								break;
							case ModeEnum.STRUCTURED_APPEND:
								if (bits.available() < 16) {
									throw new Date;
								}
								rPath = bits.readBits(8);
								maxLeafs = bits.readBits(8);
								break;
							case ModeEnum.ECI:
								var value = _.parseECIValue(bits);
								if (null === (currentCharacterSet = options.getCharacterSetECIByValue(value))) {
									throw new Date;
								}
								break;
							case ModeEnum.HANZI:
								var func = bits.readBits(4);
								var countHanzi = bits.readBits(mode.getCharacterCountBits(version));
								if (func === _.GB2312_SUBSET) {
									_.decodeHanziSegment(bits, result, countHanzi);
								}
								break;
							default:
								var count = bits.readBits(mode.getCharacterCountBits(version));
								switch (mode) {
									case ModeEnum.NUMERIC:
										_.decodeNumericSegment(bits, result, count);
										break;
									case ModeEnum.ALPHANUMERIC:
										_.decodeAlphanumericSegment(bits, result, count, fc1InEffect);
										break;
									case ModeEnum.BYTE:
										_.decodeByteSegment(bits, result, count, currentCharacterSet, body, spec);
										break;
									case ModeEnum.KANJI:
										_.decodeKanjiSegment(bits, result, count);
										break;
									default:
										throw new Date;
								}
						}
					} while (mode !== ModeEnum.TERMINATOR);
				} catch (t) {
					throw new Date;
				}
				return new Response(data, result.toString(), 0 === body.length ? null : body, null === timeout ? null : timeout.toString(), rPath, maxLeafs);
			}
		}, {
			key: "decodeHanziSegment",
			value: function decode(bits, stream, length) {
				if (13 * length > bits.available()) {
					throw new Date;
				}
				/** @type {!Uint8Array} */
				var str = new Uint8Array(2 * length);
				/** @type {number} */
				var endPos = 0;
				for (; length > 0;) {
					var _e121 = bits.readBits(13);
					/** @type {number} */
					var ff = _e121 / 96 << 8 & 4294967295 | _e121 % 96;
					/** @type {number} */
					ff = ff + (ff < 959 ? 41377 : 42657);
					/** @type {number} */
					str[endPos] = ff >> 8 & 255;
					/** @type {number} */
					str[endPos + 1] = 255 & ff;
					/** @type {number} */
					endPos = endPos + 2;
					length--;
				}
				try {
					stream.append(parser.decode(str, _.GB2312));
				} catch (interpretdYear) {
					throw new Date(interpretdYear);
				}
			}
		}, {
			key: "decodeKanjiSegment",
			value: function decode(bits, stream, length) {
				if (13 * length > bits.available()) {
					throw new Date;
				}
				/** @type {!Uint8Array} */
				var buffer = new Uint8Array(2 * length);
				/** @type {number} */
				var offset = 0;
				for (; length > 0;) {
					var _e122 = bits.readBits(13);
					/** @type {number} */
					var assembledTwoBytes = _e122 / 192 << 8 & 4294967295 | _e122 % 192;
					/** @type {number} */
					assembledTwoBytes = assembledTwoBytes + (assembledTwoBytes < 7936 ? 33088 : 49472);
					/** @type {number} */
					buffer[offset] = assembledTwoBytes >> 8;
					/** @type {number} */
					buffer[offset + 1] = assembledTwoBytes;
					/** @type {number} */
					offset = offset + 2;
					length--;
				}
				try {
					stream.append(parser.decode(buffer, _.SHIFT_JIS));
				} catch (interpretdYear) {
					throw new Date(interpretdYear);
				}
			}
		}, {
			key: "decodeByteSegment",
			value: function read(s, data, size, object, pointer, callback) {
				if (8 * size > s.available()) {
					throw new Date;
				}
				/** @type {!Uint8Array} */
				var input = new Uint8Array(size);
				/** @type {number} */
				var i = 0;
				for (; i < size; i++) {
					input[i] = s.readBits(8);
				}
				var options = void 0;
				options = null === object ? _.guessEncoding(input, callback) : object.getName();
				try {
					data.append(parser.decode(input, options));
				} catch (interpretdYear) {
					throw new Date(interpretdYear);
				}
				pointer.push(input);
			}
		}, {
			key: "toAlphaNumericChar",
			value: function changeLanguageCode(lang) {
				if (lang >= _.ALPHANUMERIC_CHARS.length) {
					throw new Date;
				}
				return _.ALPHANUMERIC_CHARS[lang];
			}
		}, {
			key: "decodeAlphanumericSegment",
			value: function init(b, map, lat, lng) {
				var time = map.length();
				for (; lat > 1;) {
					if (b.available() < 11) {
						throw new Date;
					}
					var rTime = b.readBits(11);
					map.append(_.toAlphaNumericChar(Math.floor(rTime / 45)));
					map.append(_.toAlphaNumericChar(rTime % 45));
					/** @type {number} */
					lat = lat - 2;
				}
				if (1 === lat) {
					if (b.available() < 6) {
						throw new Date;
					}
					map.append(_.toAlphaNumericChar(b.readBits(6)));
				}
				if (lng) {
					var x = time;
					for (; x < map.length(); x++) {
						if ("%" === map.charAt(x)) {
							if (x < map.length() - 1 && "%" === map.charAt(x + 1)) {
								map.deleteCharAt(x + 1);
							} else {
								map.setCharAt(x, String.fromCharCode(29));
							}
						}
					}
				}
			}
		}, {
			key: "decodeNumericSegment",
			value: function run(reader, result, diff) {
				for (; diff >= 3;) {
					if (reader.available() < 10) {
						throw new Date;
					}
					var rTime = reader.readBits(10);
					if (rTime >= 1e3) {
						throw new Date;
					}
					result.append(_.toAlphaNumericChar(Math.floor(rTime / 100)));
					result.append(_.toAlphaNumericChar(Math.floor(rTime / 10) % 10));
					result.append(_.toAlphaNumericChar(rTime % 10));
					/** @type {number} */
					diff = diff - 3;
				}
				if (2 === diff) {
					if (reader.available() < 7) {
						throw new Date;
					}
					var rTime = reader.readBits(7);
					if (rTime >= 100) {
						throw new Date;
					}
					result.append(_.toAlphaNumericChar(Math.floor(rTime / 10)));
					result.append(_.toAlphaNumericChar(rTime % 10));
				} else {
					if (1 === diff) {
						if (reader.available() < 4) {
							throw new Date;
						}
						var digitBits = reader.readBits(4);
						if (digitBits >= 10) {
							throw new Date;
						}
						result.append(_.toAlphaNumericChar(digitBits));
					}
				}
			}
		}, {
			key: "parseECIValue",
			value: function firebaseRetrieveToken(config) {
				var e = config.readBits(8);
				if (0 == (128 & e)) {
					return 127 & e;
				}
				if (128 == (192 & e)) {
					return (63 & e) << 8 & 4294967295 | config.readBits(8);
				}
				if (192 == (224 & e)) {
					return (31 & e) << 16 & 4294967295 | config.readBits(16);
				}
				throw new Date;
			}
		}]);
		return _;
	}();
	/** @type {string} */
	component.ALPHANUMERIC_CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ $%*+-./:";
	/** @type {number} */
	component.GB2312_SUBSET = 1;
	var ElementCreator = function () {
		/**
		 * @param {?} options
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3(options) {
			_classCallCheck2(this, TempusDominusBootstrap3);
			this.mirrored = options;
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "isMirrored",
			value: function isMirrored() {
				return this.mirrored;
			}
		}, {
			key: "applyMirroredCorrection",
			value: function end(a) {
				if (!this.mirrored || null === a || a.length < 3) {
					return;
				}
				var curKey = a[0];
				a[0] = a[2];
				a[2] = curKey;
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var decoder = function () {
		/**
		 * @return {undefined}
		 */
		function TempusDominusBootstrap3() {
			_classCallCheck2(this, TempusDominusBootstrap3);
			this.rsDecoder = new Uint16Array(r.QR_CODE_FIELD_256);
		}
		_createClass2(TempusDominusBootstrap3, [{
			key: "decodeBooleanArray",
			value: function justinImageSize(image, width) {
				return this.decodeBitMatrix(Image.parseFromBooleanArray(image), width);
			}
		}, {
			key: "decodeBitMatrix",
			value: function exports(data, start) {
				var parser = new CSV(data);
				/** @type {null} */
				var n = null;
				try {
					return this.decodeBitMatrixParser(parser, start);
				} catch (numInternals) {
					n = numInternals;
				}
				try {
					parser.remask();
					parser.setMirror(true);
					parser.readVersion();
					parser.readFormatInformation();
					parser.mirror();
					var node = this.decodeBitMatrixParser(parser, start);
					return node.setOther(new ElementCreator(true)), node;
				} catch (t) {
					if (null !== n) {
						throw n;
					}
					throw t;
				}
			}
		}, {
			key: "decodeBitMatrixParser",
			value: function parse(parser, index) {
				var version = parser.readVersion();
				var options = parser.readFormatInformation().getErrorCorrectionLevel();
				var codewords = parser.readCodewords();
				var result = DataBlock.getDataBlocks(codewords, version, options);
				/** @type {number} */
				var buf1 = 0;
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError44 = false;
				var _iteratorError17 = undefined;
				try {
					var _iterator3 = result[Symbol.iterator]();
					var $__6;
					for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var item = $__6.value;
						buf1 = buf1 + item.getNumDataCodewords();
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError44 = true;
					_iteratorError17 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError44) {
							throw _iteratorError17;
						}
					}
				}
				/** @type {!Uint8Array} */
				var a = new Uint8Array(buf1);
				/** @type {number} */
				var cnt = 0;
				/** @type {boolean} */
				var _iteratorNormalCompletion4 = true;
				/** @type {boolean} */
				var _didIteratorError45 = false;
				var _iteratorError18 = undefined;
				try {
					var _iterator4 = result[Symbol.iterator]();
					var $__6;
					for (; !(_iteratorNormalCompletion4 = ($__6 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
						var dataBlock = $__6.value;
						var a1 = dataBlock.getCodewords();
						var noParts = dataBlock.getNumDataCodewords();
						this.correctErrors(a1, noParts);
						/** @type {number} */
						var p = 0;
						for (; p < noParts; p++) {
							a[cnt++] = a1[p];
						}
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError45 = true;
					_iteratorError18 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion4 && _iterator4.return) {
							_iterator4.return();
						}
					} finally {
						if (_didIteratorError45) {
							throw _iteratorError18;
						}
					}
				}
				return component.decode(a, version, options, index);
			}
		}, {
			key: "correctErrors",
			value: function hash(n, s) {
				/** @type {!Int32Array} */
				var data = new Int32Array(n);
				try {
					this.rsDecoder.decode(data, n.length - s);
				} catch (t) {
					throw new Rectangle;
				}
				/** @type {number} */
				var p = 0;
				for (; p < s; p++) {
					/** @type {number} */
					n[p] = data[p];
				}
			}
		}]);
		return TempusDominusBootstrap3;
	}();
	var FinderPattern = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @param {?} props
		 * @param {?} context
		 * @param {number} fileView
		 * @return {?}
		 */
		function ElementEditor(props, context, fileView) {
			var _this;
			_classCallCheck2(this, ElementEditor);
			_this = _possibleConstructorReturn(this, (ElementEditor.__proto__ || Object.getPrototypeOf(ElementEditor)).call(this, props, context));
			_this;
			/** @type {number} */
			_this.estimatedModuleSize = fileView;
			return _this;
		}
		_inherits(ElementEditor, _WebInspector$GeneralTreeElement);
		_createClass2(ElementEditor, [{
			key: "aboutEquals",
			value: function draw(i, y, x) {
				if (Math.abs(y - this.getY()) <= i && Math.abs(x - this.getX()) <= i) {
					/** @type {number} */
					var moduleSizeDiff = Math.abs(i - this.estimatedModuleSize);
					return moduleSizeDiff <= 1 || moduleSizeDiff <= this.estimatedModuleSize;
				}
				return false;
			}
		}, {
			key: "combineEstimate",
			value: function start(height, width, useAnimation) {
				/** @type {number} */
				var header = (this.getX() + width) / 2;
				/** @type {number} */
				var i = (this.getY() + height) / 2;
				/** @type {number} */
				var div = (this.estimatedModuleSize + useAnimation) / 2;
				return new ElementEditor(header, i, div);
			}
		}]);
		return ElementEditor;
	}(type);
	var AlignmentPatternFinder = function () {
		/**
		 * @param {string} image
		 * @param {number} startX
		 * @param {number} startY
		 * @param {number} width
		 * @param {number} height
		 * @param {number} moduleSize
		 * @param {string} resultPointCallback
		 * @return {undefined}
		 */
		function AlignmentPatternFinder(image, startX, startY, width, height, moduleSize, resultPointCallback) {
			_classCallCheck2(this, AlignmentPatternFinder);
			/** @type {string} */
			this.image = image;
			/** @type {number} */
			this.startX = startX;
			/** @type {number} */
			this.startY = startY;
			/** @type {number} */
			this.width = width;
			/** @type {number} */
			this.height = height;
			/** @type {number} */
			this.moduleSize = moduleSize;
			/** @type {string} */
			this.resultPointCallback = resultPointCallback;
			/** @type {!Array} */
			this.possibleCenters = [];
			/** @type {!Int32Array} */
			this.crossCheckStateCount = new Int32Array(3);
		}
		_createClass2(AlignmentPatternFinder, [{
			key: "find",
			value: function draw() {
				var startX = this.startX;
				var clientHeight = this.height;
				var maxJ = startX + this.width;
				var middleI = this.startY + clientHeight / 2;
				/** @type {!Int32Array} */
				var stateCount = new Int32Array(3);
				var image = this.image;
				/** @type {number} */
				var targetOffsetHeight = 0;
				for (; targetOffsetHeight < clientHeight; targetOffsetHeight++) {
					var i = middleI + (0 == (1 & targetOffsetHeight) ? Math.floor((targetOffsetHeight + 1) / 2) : -Math.floor((targetOffsetHeight + 1) / 2));
					/** @type {number} */
					stateCount[0] = 0;
					/** @type {number} */
					stateCount[1] = 0;
					/** @type {number} */
					stateCount[2] = 0;
					var j = startX;
					for (; j < maxJ && !image.get(j, i);) {
						j++;
					}
					/** @type {number} */
					var currentState = 0;
					for (; j < maxJ;) {
						if (image.get(j, i)) {
							if (1 === currentState) {
								stateCount[1]++;
							} else {
								if (2 === currentState) {
									if (this.foundPatternCross(stateCount)) {
										var confirmed = this.handlePossibleCenter(stateCount, i, j);
										if (null !== confirmed) {
											return confirmed;
										}
									}
									/** @type {number} */
									stateCount[0] = stateCount[2];
									/** @type {number} */
									stateCount[1] = 1;
									/** @type {number} */
									stateCount[2] = 0;
									/** @type {number} */
									currentState = 1;
								} else {
									stateCount[++currentState]++;
								}
							}
						} else {
							if (1 === currentState) {
								currentState++;
							}
							stateCount[currentState]++;
						}
						j++;
					}
					if (this.foundPatternCross(stateCount)) {
						var confirmed = this.handlePossibleCenter(stateCount, i, maxJ);
						if (null !== confirmed) {
							return confirmed;
						}
					}
				}
				if (0 !== this.possibleCenters.length) {
					return this.possibleCenters[0];
				}
				throw new TypeError;
			}
		}, {
			key: "foundPatternCross",
			value: function almost_equals(stateCount) {
				var moduleSize = this.moduleSize;
				/** @type {number} */
				var maxVariance = moduleSize / 2;
				/** @type {number} */
				var i = 0;
				for (; i < 3; i++) {
					if (Math.abs(moduleSize - stateCount[i]) >= maxVariance) {
						return false;
					}
				}
				return true;
			}
		}, {
			key: "crossCheckVertical",
			value: function update(startI, centerJ, maxCount, originalStateCountTotal) {
				var image = this.image;
				var maxI = image.getHeight();
				var stateCount = this.crossCheckStateCount;
				/** @type {number} */
				stateCount[0] = 0;
				/** @type {number} */
				stateCount[1] = 0;
				/** @type {number} */
				stateCount[2] = 0;
				/** @type {number} */
				var i = startI;
				for (; i >= 0 && image.get(centerJ, i) && stateCount[1] <= maxCount;) {
					stateCount[1]++;
					i--;
				}
				if (i < 0 || stateCount[1] > maxCount) {
					return NaN;
				}
				for (; i >= 0 && !image.get(centerJ, i) && stateCount[0] <= maxCount;) {
					stateCount[0]++;
					i--;
				}
				if (stateCount[0] > maxCount) {
					return NaN;
				}
				i = startI + 1;
				for (; i < maxI && image.get(centerJ, i) && stateCount[1] <= maxCount;) {
					stateCount[1]++;
					i++;
				}
				if (i === maxI || stateCount[1] > maxCount) {
					return NaN;
				}
				for (; i < maxI && !image.get(centerJ, i) && stateCount[2] <= maxCount;) {
					stateCount[2]++;
					i++;
				}
				if (stateCount[2] > maxCount) {
					return NaN;
				}
				var stateCountTotal = stateCount[0] + stateCount[1] + stateCount[2];
				return 5 * Math.abs(stateCountTotal - originalStateCountTotal) >= 2 * originalStateCountTotal ? NaN : this.foundPatternCross(stateCount) ? AlignmentPatternFinder.centerFromEnd(stateCount, i) : NaN;
			}
		}, {
			key: "handlePossibleCenter",
			value: function isIndexMatchable(value, i, j) {
				var stateCountTotal = value[0] + value[1] + value[2];
				var centerJ = AlignmentPatternFinder.centerFromEnd(value, j);
				var centerI = this.crossCheckVertical(i, centerJ, 2 * value[1], stateCountTotal);
				if (!isNaN(centerI)) {
					/** @type {number} */
					var estimatedModuleSize = (value[0] + value[1] + value[2]) / 3;
					/** @type {boolean} */
					var _iteratorNormalCompletion3 = true;
					/** @type {boolean} */
					var _didIteratorError46 = false;
					var _iteratorError17 = undefined;
					try {
						var _iterator3 = this.possibleCenters[Symbol.iterator]();
						var result;
						for (; !(_iteratorNormalCompletion3 = (result = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
							var center = result.value;
							if (center.aboutEquals(estimatedModuleSize, centerI, centerJ)) {
								return center.combineEstimate(centerI, centerJ, estimatedModuleSize);
							}
						}
					} catch (err) {
						/** @type {boolean} */
						_didIteratorError46 = true;
						_iteratorError17 = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion3 && _iterator3.return) {
								_iterator3.return();
							}
						} finally {
							if (_didIteratorError46) {
								throw _iteratorError17;
							}
						}
					}
					var point = new FinderPattern(centerJ, centerI, estimatedModuleSize);
					this.possibleCenters.push(point);
					if (null !== this.resultPointCallback && void 0 !== this.resultPointCallback) {
						this.resultPointCallback.foundPossibleResultPoint(point);
					}
				}
				return null;
			}
		}], [{
			key: "centerFromEnd",
			value: function handleSlide(isSlidingUp, $cont) {
				return $cont - isSlidingUp[2] - isSlidingUp[1] / 2;
			}
		}]);
		return AlignmentPatternFinder;
	}();
	var AlignmentPattern = function (_WebInspector$GeneralTreeElement) {
		/**
		 * @param {?} file
		 * @param {?} name
		 * @param {number} n
		 * @param {number} p
		 * @return {?}
		 */
		function Graph(file, name, n, p) {
			var t;
			_classCallCheck2(this, Graph);
			t = _possibleConstructorReturn(this, (Graph.__proto__ || Object.getPrototypeOf(Graph)).call(this, file, name));
			t;
			/** @type {number} */
			t.estimatedModuleSize = n;
			/** @type {number} */
			t.count = p;
			if (void 0 === p) {
				/** @type {number} */
				t.count = 1;
			}
			return t;
		}
		_inherits(Graph, _WebInspector$GeneralTreeElement);
		_createClass2(Graph, [{
			key: "getEstimatedModuleSize",
			value: function getEstimatedModuleSize() {
				return this.estimatedModuleSize;
			}
		}, {
			key: "getCount",
			value: function count() {
				return this.count;
			}
		}, {
			key: "aboutEquals",
			value: function draw(i, y, x) {
				if (Math.abs(y - this.getY()) <= i && Math.abs(x - this.getX()) <= i) {
					/** @type {number} */
					var moduleSizeDiff = Math.abs(i - this.estimatedModuleSize);
					return moduleSizeDiff <= 1 || moduleSizeDiff <= this.estimatedModuleSize;
				}
				return false;
			}
		}, {
			key: "combineEstimate",
			value: function AlignmentPattern(posX, posY, estimatedModuleSize) {
				var count = this.count + 1;
				/** @type {number} */
				var target = (this.count * this.getX() + posY) / count;
				/** @type {number} */
				var width = (this.count * this.getY() + posX) / count;
				/** @type {number} */
				var options = (this.count * this.estimatedModuleSize + estimatedModuleSize) / count;
				return new Graph(target, width, options, count);
			}
		}]);
		return Graph;
	}(type);
	var FinderPatternInfo = function () {
		/**
		 * @param {!Array} patternCenters
		 * @return {undefined}
		 */
		function Frame(patternCenters) {
			_classCallCheck2(this, Frame);
			this.bottomLeft = patternCenters[0];
			this.topLeft = patternCenters[1];
			this.topRight = patternCenters[2];
		}
		_createClass2(Frame, [{
			key: "getBottomLeft",
			value: function getBottomLeft() {
				return this.bottomLeft;
			}
		}, {
			key: "getTopLeft",
			value: function getTopLeft() {
				return this.topLeft;
			}
		}, {
			key: "getTopRight",
			value: function getTopRight() {
				return this.topRight;
			}
		}]);
		return Frame;
	}();
	var Prepare = function () {
		/**
		 * @param {string} url
		 * @param {string} s
		 * @return {undefined}
		 */
		function _(url, s) {
			_classCallCheck2(this, _);
			/** @type {string} */
			this.image = url;
			/** @type {string} */
			this.resultPointCallback = s;
			/** @type {!Array} */
			this.possibleCenters = [];
			/** @type {!Int32Array} */
			this.crossCheckStateCount = new Int32Array(5);
			/** @type {string} */
			this.resultPointCallback = s;
		}
		_createClass2(_, [{
			key: "getImage",
			value: function getImage() {
				return this.image;
			}
		}, {
			key: "getPossibleCenters",
			value: function getPossibleCenters() {
				return this.possibleCenters;
			}
		}, {
			key: "find",
			value: function hash(context) {
				/** @type {boolean} */
				var tryHarder = null != context && void 0 !== context.get(node.TRY_HARDER);
				/** @type {boolean} */
				var artistTrack = null != context && void 0 !== context.get(node.PURE_BARCODE);
				var image = this.image;
				var maxI = image.getHeight();
				var maxJ = image.getWidth();
				/** @type {number} */
				var iSkip = Math.floor(3 * maxI / (4 * _.MAX_MODULES));
				if (iSkip < _.MIN_SKIP || tryHarder) {
					iSkip = _.MIN_SKIP;
				}
				/** @type {boolean} */
				var done = false;
				/** @type {!Int32Array} */
				var stateCount = new Int32Array(5);
				/** @type {number} */
				var i = iSkip - 1;
				for (; i < maxI && !done; i = i + iSkip) {
					/** @type {number} */
					stateCount[0] = 0;
					/** @type {number} */
					stateCount[1] = 0;
					/** @type {number} */
					stateCount[2] = 0;
					/** @type {number} */
					stateCount[3] = 0;
					/** @type {number} */
					stateCount[4] = 0;
					/** @type {number} */
					var currentState = 0;
					/** @type {number} */
					var j = 0;
					for (; j < maxJ; j++) {
						if (image.get(j, i)) {
							if (1 == (1 & currentState)) {
								currentState++;
							}
							stateCount[currentState]++;
						} else {
							if (0 == (1 & currentState)) {
								if (4 === currentState) {
									if (_.foundPatternCross(stateCount)) {
										if (true !== this.handlePossibleCenter(stateCount, i, j, artistTrack)) {
											/** @type {number} */
											stateCount[0] = stateCount[2];
											/** @type {number} */
											stateCount[1] = stateCount[3];
											/** @type {number} */
											stateCount[2] = stateCount[4];
											/** @type {number} */
											stateCount[3] = 1;
											/** @type {number} */
											stateCount[4] = 0;
											/** @type {number} */
											currentState = 3;
											continue;
										}
										if (iSkip = 2, true === this.hasSkipped) {
											done = this.haveMultiplyConfirmedCenters();
										} else {
											var rowSkip = this.findRowSkip();
											if (rowSkip > stateCount[2]) {
												i = i + (rowSkip - stateCount[2] - iSkip);
												/** @type {number} */
												j = maxJ - 1;
											}
										}
										/** @type {number} */
										currentState = 0;
										/** @type {number} */
										stateCount[0] = 0;
										/** @type {number} */
										stateCount[1] = 0;
										/** @type {number} */
										stateCount[2] = 0;
										/** @type {number} */
										stateCount[3] = 0;
										/** @type {number} */
										stateCount[4] = 0;
									} else {
										/** @type {number} */
										stateCount[0] = stateCount[2];
										/** @type {number} */
										stateCount[1] = stateCount[3];
										/** @type {number} */
										stateCount[2] = stateCount[4];
										/** @type {number} */
										stateCount[3] = 1;
										/** @type {number} */
										stateCount[4] = 0;
										/** @type {number} */
										currentState = 3;
									}
								} else {
									stateCount[++currentState]++;
								}
							} else {
								stateCount[currentState]++;
							}
						}
					}
					if (_.foundPatternCross(stateCount)) {
						if (true === this.handlePossibleCenter(stateCount, i, maxJ, artistTrack)) {
							/** @type {number} */
							iSkip = stateCount[0];
							if (this.hasSkipped) {
								done = this.haveMultiplyConfirmedCenters();
							}
						}
					}
				}
				var patternInfo = this.selectBestPatterns();
				return type.orderBestPatterns(patternInfo), new FinderPatternInfo(patternInfo);
			}
		}, {
			key: "getCrossCheckStateCount",
			value: function AlignmentPatternFinder() {
				var stateCount = this.crossCheckStateCount;
				return stateCount[0] = 0, stateCount[1] = 0, stateCount[2] = 0, stateCount[3] = 0, stateCount[4] = 0, stateCount;
			}
		}, {
			key: "crossCheckDiagonal",
			value: function update(index, i, maxCount, originalStateCountTotal) {
				var stateCount = this.getCrossCheckStateCount();
				/** @type {number} */
				var offset = 0;
				var text = this.image;
				for (; index >= offset && i >= offset && text.get(i - offset, index - offset);) {
					stateCount[2]++;
					offset++;
				}
				if (index < offset || i < offset) {
					return false;
				}
				for (; index >= offset && i >= offset && !text.get(i - offset, index - offset) && stateCount[1] <= maxCount;) {
					stateCount[1]++;
					offset++;
				}
				if (index < offset || i < offset || stateCount[1] > maxCount) {
					return false;
				}
				for (; index >= offset && i >= offset && text.get(i - offset, index - offset) && stateCount[0] <= maxCount;) {
					stateCount[0]++;
					offset++;
				}
				if (stateCount[0] > maxCount) {
					return false;
				}
				var length = text.getHeight();
				var len = text.getWidth();
				/** @type {number} */
				offset = 1;
				for (; index + offset < length && i + offset < len && text.get(i + offset, index + offset);) {
					stateCount[2]++;
					offset++;
				}
				if (index + offset >= length || i + offset >= len) {
					return false;
				}
				for (; index + offset < length && i + offset < len && !text.get(i + offset, index + offset) && stateCount[3] < maxCount;) {
					stateCount[3]++;
					offset++;
				}
				if (index + offset >= length || i + offset >= len || stateCount[3] >= maxCount) {
					return false;
				}
				for (; index + offset < length && i + offset < len && text.get(i + offset, index + offset) && stateCount[4] < maxCount;) {
					stateCount[4]++;
					offset++;
				}
				if (stateCount[4] >= maxCount) {
					return false;
				}
				var stateCountTotal = stateCount[0] + stateCount[1] + stateCount[2] + stateCount[3] + stateCount[4];
				return Math.abs(stateCountTotal - originalStateCountTotal) < 2 * originalStateCountTotal && _.foundPatternCross(stateCount);
			}
		}, {
			key: "crossCheckVertical",
			value: function update(startI, centerJ, maxCount, originalStateCountTotal) {
				var image = this.image;
				var maxI = image.getHeight();
				var stateCount = this.getCrossCheckStateCount();
				/** @type {number} */
				var i = startI;
				for (; i >= 0 && image.get(centerJ, i);) {
					stateCount[2]++;
					i--;
				}
				if (i < 0) {
					return NaN;
				}
				for (; i >= 0 && !image.get(centerJ, i) && stateCount[1] <= maxCount;) {
					stateCount[1]++;
					i--;
				}
				if (i < 0 || stateCount[1] > maxCount) {
					return NaN;
				}
				for (; i >= 0 && image.get(centerJ, i) && stateCount[0] <= maxCount;) {
					stateCount[0]++;
					i--;
				}
				if (stateCount[0] > maxCount) {
					return NaN;
				}
				i = startI + 1;
				for (; i < maxI && image.get(centerJ, i);) {
					stateCount[2]++;
					i++;
				}
				if (i === maxI) {
					return NaN;
				}
				for (; i < maxI && !image.get(centerJ, i) && stateCount[3] < maxCount;) {
					stateCount[3]++;
					i++;
				}
				if (i === maxI || stateCount[3] >= maxCount) {
					return NaN;
				}
				for (; i < maxI && image.get(centerJ, i) && stateCount[4] < maxCount;) {
					stateCount[4]++;
					i++;
				}
				if (stateCount[4] >= maxCount) {
					return NaN;
				}
				var stateCountTotal = stateCount[0] + stateCount[1] + stateCount[2] + stateCount[3] + stateCount[4];
				return 5 * Math.abs(stateCountTotal - originalStateCountTotal) >= 2 * originalStateCountTotal ? NaN : _.foundPatternCross(stateCount) ? _.centerFromEnd(stateCount, i) : NaN;
			}
		}, {
			key: "crossCheckHorizontal",
			value: function update(startJ, centerI, maxCount, originalStateCountTotal) {
				var image = this.image;
				var maxJ = image.getWidth();
				var stateCount = this.getCrossCheckStateCount();
				/** @type {number} */
				var j = startJ;
				for (; j >= 0 && image.get(j, centerI);) {
					stateCount[2]++;
					j--;
				}
				if (j < 0) {
					return NaN;
				}
				for (; j >= 0 && !image.get(j, centerI) && stateCount[1] <= maxCount;) {
					stateCount[1]++;
					j--;
				}
				if (j < 0 || stateCount[1] > maxCount) {
					return NaN;
				}
				for (; j >= 0 && image.get(j, centerI) && stateCount[0] <= maxCount;) {
					stateCount[0]++;
					j--;
				}
				if (stateCount[0] > maxCount) {
					return NaN;
				}
				j = startJ + 1;
				for (; j < maxJ && image.get(j, centerI);) {
					stateCount[2]++;
					j++;
				}
				if (j === maxJ) {
					return NaN;
				}
				for (; j < maxJ && !image.get(j, centerI) && stateCount[3] < maxCount;) {
					stateCount[3]++;
					j++;
				}
				if (j === maxJ || stateCount[3] >= maxCount) {
					return NaN;
				}
				for (; j < maxJ && image.get(j, centerI) && stateCount[4] < maxCount;) {
					stateCount[4]++;
					j++;
				}
				if (stateCount[4] >= maxCount) {
					return NaN;
				}
				var stateCountTotal = stateCount[0] + stateCount[1] + stateCount[2] + stateCount[3] + stateCount[4];
				return 5 * Math.abs(stateCountTotal - originalStateCountTotal) >= originalStateCountTotal ? NaN : _.foundPatternCross(stateCount) ? _.centerFromEnd(stateCount, j) : NaN;
			}
		}, {
			key: "handlePossibleCenter",
			value: function tree_equals(stateCount, i, j, a) {
				var stateCountTotal = stateCount[0] + stateCount[1] + stateCount[2] + stateCount[3] + stateCount[4];
				var centerJ = _.centerFromEnd(stateCount, j);
				var centerI = this.crossCheckVertical(i, Math.floor(centerJ), stateCount[2], stateCountTotal);
				if (!isNaN(centerI) && (centerJ = this.crossCheckHorizontal(Math.floor(centerJ), Math.floor(centerI), stateCount[2], stateCountTotal), !isNaN(centerJ) && (!a || this.crossCheckDiagonal(Math.floor(centerI), Math.floor(centerJ), stateCount[2], stateCountTotal)))) {
					/** @type {number} */
					var estimatedModuleSize = stateCountTotal / 7;
					/** @type {boolean} */
					var _e131 = false;
					var shape = this.possibleCenters;
					/** @type {number} */
					var index = 0;
					var n = shape.length;
					for (; index < n; index++) {
						var center = shape[index];
						if (center.aboutEquals(estimatedModuleSize, centerI, centerJ)) {
							shape[index] = center.combineEstimate(centerI, centerJ, estimatedModuleSize);
							/** @type {boolean} */
							_e131 = true;
							break;
						}
					}
					if (!_e131) {
						var point = new AlignmentPattern(centerJ, centerI, estimatedModuleSize);
						shape.push(point);
						if (null !== this.resultPointCallback && void 0 !== this.resultPointCallback) {
							this.resultPointCallback.foundPossibleResultPoint(point);
						}
					}
					return true;
				}
				return false;
			}
		}, {
			key: "findRowSkip",
			value: function update() {
				if (this.possibleCenters.length <= 1) {
					return 0;
				}
				/** @type {null} */
				var acc = null;
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError47 = false;
				var _iteratorError17 = undefined;
				try {
					var _iterator3 = this.possibleCenters[Symbol.iterator]();
					var $__6;
					for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var item = $__6.value;
						if (item.getCount() >= _.CENTER_QUORUM) {
							if (null != acc) {
								return this.hasSkipped = true, Math.floor((Math.abs(acc.getX() - item.getX()) - Math.abs(acc.getY() - item.getY())) / 2);
							}
							acc = item;
						}
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError47 = true;
					_iteratorError17 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError47) {
							throw _iteratorError17;
						}
					}
				}
				return 0;
			}
		}, {
			key: "haveMultiplyConfirmedCenters",
			value: function update() {
				/** @type {number} */
				var t = 0;
				/** @type {number} */
				var totalModuleSize = 0;
				var startSize = this.possibleCenters.length;
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError48 = false;
				var _iteratorError17 = undefined;
				try {
					var _iterator3 = this.possibleCenters[Symbol.iterator]();
					var _step2;
					for (; !(_iteratorNormalCompletion3 = (_step2 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var pattern = _step2.value;
						if (pattern.getCount() >= _.CENTER_QUORUM) {
							t++;
							totalModuleSize = totalModuleSize + pattern.getEstimatedModuleSize();
						}
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError48 = true;
					_iteratorError17 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError48) {
							throw _iteratorError17;
						}
					}
				}
				if (t < 3) {
					return false;
				}
				/** @type {number} */
				var average = totalModuleSize / startSize;
				/** @type {number} */
				var totalDeviation = 0;
				/** @type {boolean} */
				var _iteratorNormalCompletion4 = true;
				/** @type {boolean} */
				var _didIteratorError49 = false;
				var _iteratorError18 = undefined;
				try {
					var _iterator4 = this.possibleCenters[Symbol.iterator]();
					var _step2;
					for (; !(_iteratorNormalCompletion4 = (_step2 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
						var pattern = _step2.value;
						/** @type {number} */
						totalDeviation = totalDeviation + Math.abs(pattern.getEstimatedModuleSize() - average);
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError49 = true;
					_iteratorError18 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion4 && _iterator4.return) {
							_iterator4.return();
						}
					} finally {
						if (_didIteratorError49) {
							throw _iteratorError18;
						}
					}
				}
				return totalDeviation <= .05 * totalModuleSize;
			}
		}, {
			key: "selectBestPatterns",
			value: function initialize() {
				var startSize = this.possibleCenters.length;
				if (startSize < 3) {
					throw new TypeError;
				}
				var fields = this.possibleCenters;
				var average = void 0;
				if (startSize > 3) {
					/** @type {number} */
					var totalModuleSize = 0;
					/** @type {number} */
					var square = 0;
					/** @type {boolean} */
					var _iteratorNormalCompletion3 = true;
					/** @type {boolean} */
					var _didIteratorError50 = false;
					var _iteratorError17 = undefined;
					try {
						var _iterator3 = this.possibleCenters[Symbol.iterator]();
						var _step2;
						for (; !(_iteratorNormalCompletion3 = (_step2 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
							var pattern = _step2.value;
							var centerValue = pattern.getEstimatedModuleSize();
							totalModuleSize = totalModuleSize + centerValue;
							/** @type {number} */
							square = square + centerValue * centerValue;
						}
					} catch (err) {
						/** @type {boolean} */
						_didIteratorError50 = true;
						_iteratorError17 = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion3 && _iterator3.return) {
								_iterator3.return();
							}
						} finally {
							if (_didIteratorError50) {
								throw _iteratorError17;
							}
						}
					}
					/** @type {number} */
					average = totalModuleSize / startSize;
					/** @type {number} */
					var start = Math.sqrt(square / startSize - average * average);
					fields.sort(function (possibleCenter, center1) {
						/** @type {number} */
						var progressOld = Math.abs(center1.getEstimatedModuleSize() - average);
						/** @type {number} */
						var progressNew = Math.abs(possibleCenter.getEstimatedModuleSize() - average);
						return progressOld < progressNew ? -1 : progressOld > progressNew ? 1 : 0;
					});
					/** @type {number} */
					var childStartView2 = Math.max(.2 * average, start);
					/** @type {number} */
					var i = 0;
					for (; i < fields.length && fields.length > 3; i++) {
						var c_field = fields[i];
						if (Math.abs(c_field.getEstimatedModuleSize() - average) > childStartView2) {
							fields.splice(i, 1);
							i--;
						}
					}
				}
				if (fields.length > 3) {
					/** @type {number} */
					var sum = 0;
					/** @type {boolean} */
					var _iteratorNormalCompletion3 = true;
					/** @type {boolean} */
					var _didIteratorError51 = false;
					var _iteratorError17 = undefined;
					try {
						var _iterator3 = fields[Symbol.iterator]();
						var _step2;
						for (; !(_iteratorNormalCompletion3 = (_step2 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
							var pattern = _step2.value;
							sum = sum + pattern.getEstimatedModuleSize();
						}
					} catch (err) {
						/** @type {boolean} */
						_didIteratorError51 = true;
						_iteratorError17 = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion3 && _iterator3.return) {
								_iterator3.return();
							}
						} finally {
							if (_didIteratorError51) {
								throw _iteratorError17;
							}
						}
					}
					/** @type {number} */
					average = sum / fields.length;
					fields.sort(function (center1, center) {
						if (center.getCount() === center1.getCount()) {
							/** @type {number} */
							var progressOld = Math.abs(center.getEstimatedModuleSize() - average);
							/** @type {number} */
							var progressNew = Math.abs(center1.getEstimatedModuleSize() - average);
							return progressOld < progressNew ? 1 : progressOld > progressNew ? -1 : 0;
						}
						return center.getCount() - center1.getCount();
					});
					fields.splice(3);
				}
				return [fields[0], fields[1], fields[2]];
			}
		}], [{
			key: "centerFromEnd",
			value: function create(p, h) {
				return h - p[4] - p[3] - p[2] / 2;
			}
		}, {
			key: "foundPatternCross",
			value: function almost_equals(childMargins) {
				/** @type {number} */
				var indexes = 0;
				/** @type {number} */
				var indexLookupKey = 0;
				for (; indexLookupKey < 5; indexLookupKey++) {
					var currentIndex = childMargins[indexLookupKey];
					if (0 === currentIndex) {
						return false;
					}
					indexes = indexes + currentIndex;
				}
				if (indexes < 7) {
					return false;
				}
				/** @type {number} */
				var height = indexes / 7;
				/** @type {number} */
				var middle = height / 2;
				return Math.abs(height - childMargins[0]) < middle && Math.abs(height - childMargins[1]) < middle && Math.abs(3 * height - childMargins[2]) < 3 * middle && Math.abs(height - childMargins[3]) < middle && Math.abs(height - childMargins[4]) < middle;
			}
		}]);
		return _;
	}();
	/** @type {number} */
	Prepare.CENTER_QUORUM = 2;
	/** @type {number} */
	Prepare.MIN_SKIP = 3;
	/** @type {number} */
	Prepare.MAX_MODULES = 57;
	var AttachmentViewModel = function () {
		/**
		 * @param {string} url
		 * @return {undefined}
		 */
		function Dialog(url) {
			_classCallCheck2(this, Dialog);
			/** @type {string} */
			this.image = url;
		}
		_createClass2(Dialog, [{
			key: "getImage",
			value: function getImage() {
				return this.image;
			}
		}, {
			key: "getResultPointCallback",
			value: function getResultPointCallback() {
				return this.resultPointCallback;
			}
		}, {
			key: "detect",
			value: function prepare(arr) {
				this.resultPointCallback = null == arr ? null : arr.get(node.NEED_RESULT_POINT_CALLBACK);
				var info = (new Prepare(this.image, this.resultPointCallback)).find(arr);
				return this.processFinderPatternInfo(info);
			}
		}, {
			key: "processFinderPatternInfo",
			value: function update(bounds) {
				var topLeft = bounds.getTopLeft();
				var topRight = bounds.getTopRight();
				var bottomLeft = bounds.getBottomLeft();
				var moduleSize = this.calculateModuleSize(topLeft, topRight, bottomLeft);
				if (moduleSize < 1) {
					throw new TypeError("No pattern found in proccess finder.");
				}
				var dimension = Dialog.computeDimension(topLeft, topRight, bottomLeft, moduleSize);
				var provisionalVersion = Version.getProvisionalVersionForDimension(dimension);
				/** @type {number} */
				var a = provisionalVersion.getDimensionForVersion() - 7;
				/** @type {null} */
				var alignmentPattern = null;
				if (provisionalVersion.getAlignmentPatternCenters().length > 0) {
					var blueWins = topRight.getX() - topLeft.getX() + bottomLeft.getX();
					var redWins = topRight.getY() - topLeft.getY() + bottomLeft.getY();
					/** @type {number} */
					var KFactor = 1 - 3 / a;
					/** @type {number} */
					var estAlignmentX = Math.floor(topLeft.getX() + KFactor * (blueWins - topLeft.getX()));
					/** @type {number} */
					var estAlignmentY = Math.floor(topLeft.getY() + KFactor * (redWins - topLeft.getY()));
					/** @type {number} */
					var i = 4;
					for (; i <= 16; i = i << 1) {
						try {
							alignmentPattern = this.findAlignmentInRegion(moduleSize, estAlignmentX, estAlignmentY, i);
							break;
						} catch (ex) {
							if (!(ex instanceof TypeError)) {
								throw ex;
							}
						}
					}
				}
				var transform = Dialog.createTransform(topLeft, topRight, bottomLeft, alignmentPattern, dimension);
				var bits = Dialog.sampleGrid(this.image, transform, dimension);
				var u = void 0;
				return new HTMLTagSpecification(bits, u = null === alignmentPattern ? [bottomLeft, topLeft, topRight] : [bottomLeft, topLeft, topRight, alignmentPattern]);
			}
		}, {
			key: "calculateModuleSize",
			value: function calculateModuleSize(topLeft, topRight, bottomLeft) {
				return (this.calculateModuleSizeOneWay(topLeft, topRight) + this.calculateModuleSizeOneWay(topLeft, bottomLeft)) / 2;
			}
		}, {
			key: "calculateModuleSizeOneWay",
			value: function start(e, f) {
				var moduleSizeEst1 = this.sizeOfBlackWhiteBlackRunBothWays(Math.floor(e.getX()), Math.floor(e.getY()), Math.floor(f.getX()), Math.floor(f.getY()));
				var moduleSizeEst2 = this.sizeOfBlackWhiteBlackRunBothWays(Math.floor(f.getX()), Math.floor(f.getY()), Math.floor(e.getX()), Math.floor(e.getY()));
				return isNaN(moduleSizeEst1) ? moduleSizeEst2 / 7 : isNaN(moduleSizeEst2) ? moduleSizeEst1 / 7 : (moduleSizeEst1 + moduleSizeEst2) / 14;
			}
		}, {
			key: "sizeOfBlackWhiteBlackRunBothWays",
			value: function resize(fromX, fromY, toX, toY) {
				var result = this.sizeOfBlackWhiteBlackRun(fromX, fromY, toX, toY);
				/** @type {number} */
				var scale = 1;
				/** @type {number} */
				var otherToX = fromX - (toX - fromX);
				if (otherToX < 0) {
					/** @type {number} */
					scale = fromX / (fromX - otherToX);
					/** @type {number} */
					otherToX = 0;
				} else {
					if (otherToX >= this.image.getWidth()) {
						/** @type {number} */
						scale = (this.image.getWidth() - 1 - fromX) / (otherToX - fromX);
						/** @type {number} */
						otherToX = this.image.getWidth() - 1;
					}
				}
				/** @type {number} */
				var otherToY = Math.floor(fromY - (toY - fromY) * scale);
				return scale = 1, otherToY < 0 ? (scale = fromY / (fromY - otherToY), otherToY = 0) : otherToY >= this.image.getHeight() && (scale = (this.image.getHeight() - 1 - fromY) / (otherToY - fromY), otherToY = this.image.getHeight() - 1), otherToX = Math.floor(fromX + (otherToX - fromX) * scale), (result = result + this.sizeOfBlackWhiteBlackRun(fromX, fromY, otherToX, otherToY)) - 1;
			}
		}, {
			key: "sizeOfBlackWhiteBlackRun",
			value: function get(x, y, n, r) {
				/** @type {boolean} */
				var reverse = Math.abs(r - y) > Math.abs(n - x);
				if (reverse) {
					/** @type {number} */
					var tmp = x;
					/** @type {number} */
					x = y;
					y = tmp;
					/** @type {number} */
					tmp = n;
					/** @type {number} */
					n = r;
					r = tmp;
				}
				/** @type {number} */
				var hMargin = Math.abs(n - x);
				/** @type {number} */
				var destX = Math.abs(r - y);
				/** @type {number} */
				var w = -hMargin / 2;
				/** @type {number} */
				var i = x < n ? 1 : -1;
				/** @type {number} */
				var MarkersSymbolizer = y < r ? 1 : -1;
				/** @type {number} */
				var b = 0;
				var s = n + i;
				/** @type {number} */
				var pos = x;
				/** @type {number} */
				var key = y;
				for (; pos !== s; pos = pos + i) {
					var i = reverse ? key : pos;
					var start = reverse ? pos : key;
					if (1 === b === this.image.get(i, start)) {
						if (2 === b) {
							return p.distance(pos, key, x, y);
						}
						b++;
					}
					if ((w = w + destX) > 0) {
						if (key === r) {
							break;
						}
						key = key + MarkersSymbolizer;
						/** @type {number} */
						w = w - hMargin;
					}
				}
				return 2 === b ? p.distance(n + i, r, x, y) : NaN;
			}
		}, {
			key: "findAlignmentInRegion",
			value: function playstatebaseDrawVignettes(overallEstModuleSize, start, index, allowanceFactor) {
				/** @type {number} */
				var length = Math.floor(allowanceFactor * overallEstModuleSize);
				/** @type {number} */
				var alignmentAreaLeftX = Math.max(0, start - length);
				/** @type {number} */
				var alignmentAreaRightX = Math.min(this.image.getWidth() - 1, start + length);
				if (alignmentAreaRightX - alignmentAreaLeftX < 3 * overallEstModuleSize) {
					throw new TypeError("Alignment top exceeds estimated module size.");
				}
				/** @type {number} */
				var alignmentAreaTopY = Math.max(0, index - length);
				/** @type {number} */
				var alignmentAreaBottomY = Math.min(this.image.getHeight() - 1, index + length);
				if (alignmentAreaBottomY - alignmentAreaTopY < 3 * overallEstModuleSize) {
					throw new TypeError("Alignment bottom exceeds estimated module size.");
				}
				return (new AlignmentPatternFinder(this.image, alignmentAreaLeftX, alignmentAreaTopY, alignmentAreaRightX - alignmentAreaLeftX, alignmentAreaBottomY - alignmentAreaTopY, overallEstModuleSize, this.resultPointCallback)).find();
			}
		}], [{
			key: "createTransform",
			value: function start(face, v, p, n, then) {
				/** @type {number} */
				var dimMinusThree = then - 3.5;
				var bottomRightX = void 0;
				var bottomRightY = void 0;
				var sourceBottomRightX = void 0;
				var sourceBottomRightY = void 0;
				return null !== n ? (bottomRightX = n.getX(), bottomRightY = n.getY(), sourceBottomRightY = sourceBottomRightX = dimMinusThree - 3) : (bottomRightX = v.getX() - face.getX() + p.getX(), bottomRightY = v.getY() - face.getY() + p.getY(), sourceBottomRightX = dimMinusThree, sourceBottomRightY = dimMinusThree), PerspectiveTransform.quadrilateralToQuadrilateral(3.5, 3.5, dimMinusThree, 3.5, sourceBottomRightX, sourceBottomRightY, 3.5, dimMinusThree, face.getX(), face.getY(), v.getX(), v.getY(),
					bottomRightX, bottomRightY, p.getX(), p.getY());
			}
		}, {
			key: "sampleGrid",
			value: function DashboardService($compile, $http, $timeout) {
				return ServiceManager.getInstance().sampleGridWithTransform($compile, $timeout, $timeout, $http);
			}
		}, {
			key: "computeDimension",
			value: function dummy(str, e, n, y) {
				var i = p.round(type.distance(str, e) / y);
				var GROUPSIZE = p.round(type.distance(str, n) / y);
				/** @type {number} */
				var .num_const = Math.floor((i + GROUPSIZE) / 2) + 7;
			switch(3 & .num_const) {
          case 0:
            .num_const++;
	break;
          case 2:
            .num_const--;
	break;
          case 3:
	throw new TypeError("Dimensions could be not found.");
}
        return .num_const;
      }
    }]);
return Dialog;
  }();
var Message = function () {
	/**
	 * @return {undefined}
	 */
	function TempusDominusBootstrap3() {
		_classCallCheck2(this, TempusDominusBootstrap3);
		this.decoder = new decoder;
	}
	_createClass2(TempusDominusBootstrap3, [{
		key: "getDecoder",
		value: function getDecoder() {
			return this.decoder;
		}
	}, {
		key: "decode",
		value: function create(src, type) {
			var content = void 0;
			var outletMap = void 0;
			if (null != type && void 0 !== type.get(node.PURE_BARCODE)) {
				var data = TempusDominusBootstrap3.extractPureBits(src.getBlackMatrix());
				content = this.decoder.decodeBitMatrix(data, type);
				outletMap = TempusDominusBootstrap3.NO_POINTS;
			} else {
				var ac = (new AttachmentViewModel(src.getBlackMatrix())).detect(type);
				content = this.decoder.decodeBitMatrix(ac.getBits(), type);
				outletMap = ac.getPoints();
			}
			if (content.getOther() instanceof ElementCreator) {
				content.getOther().applyMirroredCorrection(outletMap);
			}
			var r = new result(content.getText(), content.getRawBytes(), void 0, outletMap, change.QR_CODE, void 0);
			var nextMinID = content.getByteSegments();
			if (null !== nextMinID) {
				r.putMetadata(value.BYTE_SEGMENTS, nextMinID);
			}
			var hookCheckOne = content.getECLevel();
			return null !== hookCheckOne && r.putMetadata(value.ERROR_CORRECTION_LEVEL, hookCheckOne), content.hasStructuredAppend() && (r.putMetadata(value.STRUCTURED_APPEND_SEQUENCE, content.getStructuredAppendSequenceNumber()), r.putMetadata(value.STRUCTURED_APPEND_PARITY, content.getStructuredAppendParity())), r;
		}
	}, {
		key: "reset",
		value: function reset() {
		}
	}], [{
		key: "extractPureBits",
		value: function open(name) {
			var x = name.getTopLeftOnBit();
			var args = name.getBottomRightOnBit();
			if (null === x || null === args) {
				throw new TypeError;
			}
			var step = this.moduleSize(x, name);
			var shift = x[1];
			var zIndex = args[1];
			var b = x[0];
			var t = args[0];
			if (b >= t || shift >= zIndex) {
				throw new TypeError;
			}
			if (zIndex - shift != t - b && (t = b + (zIndex - shift)) >= name.getWidth()) {
				throw new TypeError;
			}
			/** @type {number} */
			var size = Math.round((t - b + 1) / step);
			/** @type {number} */
			var key = Math.round((zIndex - shift + 1) / step);
			if (size <= 0 || key <= 0) {
				throw new TypeError;
			}
			if (key !== size) {
				throw new TypeError;
			}
			/** @type {number} */
			var c = Math.floor(step / 2);
			shift = shift + c;
			/** @type {number} */
			var d = (b = b + c) + Math.floor((size - 1) * step) - t;
			if (d > 0) {
				if (d > c) {
					throw new TypeError;
				}
				/** @type {number} */
				b = b - d;
			}
			/** @type {number} */
			var loc = shift + Math.floor((key - 1) * step) - zIndex;
			if (loc > 0) {
				if (loc > c) {
					throw new TypeError;
				}
				/** @type {number} */
				shift = shift - loc;
			}
			var data = new Image(size, key);
			/** @type {number} */
			var val = 0;
			for (; val < key; val++) {
				var loc = shift + Math.floor(val * step);
				/** @type {number} */
				var i = 0;
				for (; i < size; i++) {
					if (name.get(b + Math.floor(i * step), loc)) {
						data.set(i, val);
					}
				}
			}
			return data;
		}
	}, {
		key: "moduleSize",
		value: function _downHandler(e, t) {
			var m = t.getHeight();
			var n = t.getWidth();
			var i = e[0];
			var j = e[1];
			/** @type {boolean} */
			var val = true;
			/** @type {number} */
			var a = 0;
			for (; i < n && j < m;) {
				if (val !== t.get(i, j)) {
					if (5 == ++a) {
						break;
					}
					/** @type {boolean} */
					val = !val;
				}
				i++;
				j++;
			}
			if (i === n || j === m) {
				throw new TypeError;
			}
			return (i - e[0]) / 7;
		}
	}]);
	return TempusDominusBootstrap3;
}();
/** @type {!Array} */
Message.NO_POINTS = new Array;
var that = function () {
	/**
	 * @return {undefined}
	 */
	function self() {
		_classCallCheck2(this, self);
	}
	_createClass2(self, [{
		key: "PDF417Common",
		value: function PDF417Common() {
		}
	}], [{
		key: "getBitCountSum",
		value: function nodeLength(node) {
			return p.sum(node);
		}
	}, {
		key: "toIntArray",
		value: function setExtendAttribute(rules) {
			if (null == rules || !rules.length) {
				return self.EMPTY_INT_ARRAY;
			}
			/** @type {!Int32Array} */
			var out = new Int32Array(rules.length);
			/** @type {number} */
			var j = 0;
			/** @type {boolean} */
			var _iteratorNormalCompletion3 = true;
			/** @type {boolean} */
			var _didIteratorError52 = false;
			var _iteratorError17 = undefined;
			try {
				var _iterator3 = rules[Symbol.iterator]();
				var $__6;
				for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var item = $__6.value;
					out[j++] = item;
				}
			} catch (err) {
				/** @type {boolean} */
				_didIteratorError52 = true;
				_iteratorError17 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError52) {
						throw _iteratorError17;
					}
				}
			}
			return out;
		}
	}, {
		key: "getCodeword",
		value: function numericTicks(a) {
			var arglevel = b.binarySearch(self.SYMBOL_TABLE, 262143 & a);
			return arglevel < 0 ? -1 : (self.CODEWORD_TABLE[arglevel] - 1) % self.NUMBER_OF_CODEWORDS;
		}
	}]);
	return self;
}();
/** @type {number} */
that.NUMBER_OF_CODEWORDS = 929;
/** @type {number} */
that.MAX_CODEWORDS_IN_BARCODE = that.NUMBER_OF_CODEWORDS - 1;
/** @type {number} */
that.MIN_ROWS_IN_BARCODE = 3;
/** @type {number} */
that.MAX_ROWS_IN_BARCODE = 90;
/** @type {number} */
that.MODULES_IN_CODEWORD = 17;
/** @type {number} */
that.MODULES_IN_STOP_PATTERN = 18;
/** @type {number} */
that.BARS_IN_MODULE = 8;
/** @type {!Int32Array} */
that.EMPTY_INT_ARRAY = new Int32Array([]);
/** @type {!Int32Array} */
that.SYMBOL_TABLE = Int32Array.from([66142, 66170, 66206, 66236, 66290, 66292, 66350, 66382, 66396, 66454, 66470, 66476, 66594, 66600, 66614, 66626, 66628, 66632, 66640, 66654, 66662, 66668, 66682, 66690, 66718, 66720, 66748, 66758, 66776, 66798, 66802, 66804, 66820, 66824, 66832, 66846, 66848, 66876, 66880, 66936, 66950, 66956, 66968, 66992, 67006, 67022, 67036, 67042, 67044, 67048, 67062, 67118, 67150, 67164, 67214, 67228, 67256, 67294, 67322, 67350, 67366, 67372, 67398, 67404, 67416, 67438,
	67474, 67476, 67490, 67492, 67496, 67510, 67618, 67624, 67650, 67656, 67664, 67678, 67686, 67692, 67706, 67714, 67716, 67728, 67742, 67744, 67772, 67782, 67788, 67800, 67822, 67826, 67828, 67842, 67848, 67870, 67872, 67900, 67904, 67960, 67974, 67992, 68016, 68030, 68046, 68060, 68066, 68068, 68072, 68086, 68104, 68112, 68126, 68128, 68156, 68160, 68216, 68336, 68358, 68364, 68376, 68400, 68414, 68448, 68476, 68494, 68508, 68536, 68546, 68548, 68552, 68560, 68574, 68582, 68588, 68654, 68686, 68700,
	68706, 68708, 68712, 68726, 68750, 68764, 68792, 68802, 68804, 68808, 68816, 68830, 68838, 68844, 68858, 68878, 68892, 68920, 68976, 68990, 68994, 68996, 69e3, 69008, 69022, 69024, 69052, 69062, 69068, 69080, 69102, 69106, 69108, 69142, 69158, 69164, 69190, 69208, 69230, 69254, 69260, 69272, 69296, 69310, 69326, 69340, 69386, 69394, 69396, 69410, 69416, 69430, 69442, 69444, 69448, 69456, 69470, 69478, 69484, 69554, 69556, 69666, 69672, 69698, 69704, 69712, 69726, 69754, 69762, 69764, 69776, 69790,
	69792, 69820, 69830, 69836, 69848, 69870, 69874, 69876, 69890, 69918, 69920, 69948, 69952, 70008, 70022, 70040, 70064, 70078, 70094, 70108, 70114, 70116, 70120, 70134, 70152, 70174, 70176, 70264, 70384, 70412, 70448, 70462, 70496, 70524, 70542, 70556, 70584, 70594, 70600, 70608, 70622, 70630, 70636, 70664, 70672, 70686, 70688, 70716, 70720, 70776, 70896, 71136, 71180, 71192, 71216, 71230, 71264, 71292, 71360, 71416, 71452, 71480, 71536, 71550, 71554, 71556, 71560, 71568, 71582, 71584, 71612, 71622,
	71628, 71640, 71662, 71726, 71732, 71758, 71772, 71778, 71780, 71784, 71798, 71822, 71836, 71864, 71874, 71880, 71888, 71902, 71910, 71916, 71930, 71950, 71964, 71992, 72048, 72062, 72066, 72068, 72080, 72094, 72096, 72124, 72134, 72140, 72152, 72174, 72178, 72180, 72206, 72220, 72248, 72304, 72318, 72416, 72444, 72456, 72464, 72478, 72480, 72508, 72512, 72568, 72588, 72600, 72624, 72638, 72654, 72668, 72674, 72676, 72680, 72694, 72726, 72742, 72748, 72774, 72780, 72792, 72814, 72838, 72856, 72880,
	72894, 72910, 72924, 72930, 72932, 72936, 72950, 72966, 72972, 72984, 73008, 73022, 73056, 73084, 73102, 73116, 73144, 73156, 73160, 73168, 73182, 73190, 73196, 73210, 73226, 73234, 73236, 73250, 73252, 73256, 73270, 73282, 73284, 73296, 73310, 73318, 73324, 73346, 73348, 73352, 73360, 73374, 73376, 73404, 73414, 73420, 73432, 73454, 73498, 73518, 73522, 73524, 73550, 73564, 73570, 73572, 73576, 73590, 73800, 73822, 73858, 73860, 73872, 73886, 73888, 73916, 73944, 73970, 73972, 73992, 74014, 74016,
	74044, 74048, 74104, 74118, 74136, 74160, 74174, 74210, 74212, 74216, 74230, 74244, 74256, 74270, 74272, 74360, 74480, 74502, 74508, 74544, 74558, 74592, 74620, 74638, 74652, 74680, 74690, 74696, 74704, 74726, 74732, 74782, 74784, 74812, 74992, 75232, 75288, 75326, 75360, 75388, 75456, 75512, 75576, 75632, 75646, 75650, 75652, 75664, 75678, 75680, 75708, 75718, 75724, 75736, 75758, 75808, 75836, 75840, 75896, 76016, 76256, 76736, 76824, 76848, 76862, 76896, 76924, 76992, 77048, 77296, 77340, 77368,
	77424, 77438, 77536, 77564, 77572, 77576, 77584, 77600, 77628, 77632, 77688, 77702, 77708, 77720, 77744, 77758, 77774, 77788, 77870, 77902, 77916, 77922, 77928, 77966, 77980, 78008, 78018, 78024, 78032, 78046, 78060, 78074, 78094, 78136, 78192, 78206, 78210, 78212, 78224, 78238, 78240, 78268, 78278, 78284, 78296, 78322, 78324, 78350, 78364, 78448, 78462, 78560, 78588, 78600, 78622, 78624, 78652, 78656, 78712, 78726, 78744, 78768, 78782, 78798, 78812, 78818, 78820, 78824, 78838, 78862, 78876, 78904,
	78960, 78974, 79072, 79100, 79296, 79352, 79368, 79376, 79390, 79392, 79420, 79424, 79480, 79600, 79628, 79640, 79664, 79678, 79712, 79740, 79772, 79800, 79810, 79812, 79816, 79824, 79838, 79846, 79852, 79894, 79910, 79916, 79942, 79948, 79960, 79982, 79988, 80006, 80024, 80048, 80062, 80078, 80092, 80098, 80100, 80104, 80134, 80140, 80176, 80190, 80224, 80252, 80270, 80284, 80312, 80328, 80336, 80350, 80358, 80364, 80378, 80390, 80396, 80408, 80432, 80446, 80480, 80508, 80576, 80632, 80654, 80668,
	80696, 80752, 80766, 80776, 80784, 80798, 80800, 80828, 80844, 80856, 80878, 80882, 80884, 80914, 80916, 80930, 80932, 80936, 80950, 80962, 80968, 80976, 80990, 80998, 81004, 81026, 81028, 81040, 81054, 81056, 81084, 81094, 81100, 81112, 81134, 81154, 81156, 81160, 81168, 81182, 81184, 81212, 81216, 81272, 81286, 81292, 81304, 81328, 81342, 81358, 81372, 81380, 81384, 81398, 81434, 81454, 81458, 81460, 81486, 81500, 81506, 81508, 81512, 81526, 81550, 81564, 81592, 81602, 81604, 81608, 81616, 81630,
	81638, 81644, 81702, 81708, 81722, 81734, 81740, 81752, 81774, 81778, 81780, 82050, 82078, 82080, 82108, 82180, 82184, 82192, 82206, 82208, 82236, 82240, 82296, 82316, 82328, 82352, 82366, 82402, 82404, 82408, 82440, 82448, 82462, 82464, 82492, 82496, 82552, 82672, 82694, 82700, 82712, 82736, 82750, 82784, 82812, 82830, 82882, 82884, 82888, 82896, 82918, 82924, 82952, 82960, 82974, 82976, 83004, 83008, 83064, 83184, 83424, 83468, 83480, 83504, 83518, 83552, 83580, 83648, 83704, 83740, 83768, 83824,
	83838, 83842, 83844, 83848, 83856, 83872, 83900, 83910, 83916, 83928, 83950, 83984, 84E3, 84028, 84032, 84088, 84208, 84448, 84928, 85040, 85054, 85088, 85116, 85184, 85240, 85488, 85560, 85616, 85630, 85728, 85756, 85764, 85768, 85776, 85790, 85792, 85820, 85824, 85880, 85894, 85900, 85912, 85936, 85966, 85980, 86048, 86080, 86136, 86256, 86496, 86976, 88160, 88188, 88256, 88312, 88560, 89056, 89200, 89214, 89312, 89340, 89536, 89592, 89608, 89616, 89632, 89664, 89720, 89840, 89868, 89880, 89904,
	89952, 89980, 89998, 90012, 90040, 90190, 90204, 90254, 90268, 90296, 90306, 90308, 90312, 90334, 90382, 90396, 90424, 90480, 90494, 90500, 90504, 90512, 90526, 90528, 90556, 90566, 90572, 90584, 90610, 90612, 90638, 90652, 90680, 90736, 90750, 90848, 90876, 90884, 90888, 90896, 90910, 90912, 90940, 90944, 91E3, 91014, 91020, 91032, 91056, 91070, 91086, 91100, 91106, 91108, 91112, 91126, 91150, 91164, 91192, 91248, 91262, 91360, 91388, 91584, 91640, 91664, 91678, 91680, 91708, 91712, 91768, 91888,
	91928, 91952, 91966, 92E3, 92028, 92046, 92060, 92088, 92098, 92100, 92104, 92112, 92126, 92134, 92140, 92188, 92216, 92272, 92384, 92412, 92608, 92664, 93168, 93200, 93214, 93216, 93244, 93248, 93304, 93424, 93664, 93720, 93744, 93758, 93792, 93820, 93888, 93944, 93980, 94008, 94064, 94078, 94084, 94088, 94096, 94110, 94112, 94140, 94150, 94156, 94168, 94246, 94252, 94278, 94284, 94296, 94318, 94342, 94348, 94360, 94384, 94398, 94414, 94428, 94440, 94470, 94476, 94488, 94512, 94526, 94560, 94588,
	94606, 94620, 94648, 94658, 94660, 94664, 94672, 94686, 94694, 94700, 94714, 94726, 94732, 94744, 94768, 94782, 94816, 94844, 94912, 94968, 94990, 95004, 95032, 95088, 95102, 95112, 95120, 95134, 95136, 95164, 95180, 95192, 95214, 95218, 95220, 95244, 95256, 95280, 95294, 95328, 95356, 95424, 95480, 95728, 95758, 95772, 95800, 95856, 95870, 95968, 95996, 96008, 96016, 96030, 96032, 96060, 96064, 96120, 96152, 96176, 96190, 96220, 96226, 96228, 96232, 96290, 96292, 96296, 96310, 96322, 96324, 96328,
	96336, 96350, 96358, 96364, 96386, 96388, 96392, 96400, 96414, 96416, 96444, 96454, 96460, 96472, 96494, 96498, 96500, 96514, 96516, 96520, 96528, 96542, 96544, 96572, 96576, 96632, 96646, 96652, 96664, 96688, 96702, 96718, 96732, 96738, 96740, 96744, 96758, 96772, 96776, 96784, 96798, 96800, 96828, 96832, 96888, 97008, 97030, 97036, 97048, 97072, 97086, 97120, 97148, 97166, 97180, 97208, 97220, 97224, 97232, 97246, 97254, 97260, 97326, 97330, 97332, 97358, 97372, 97378, 97380, 97384, 97398, 97422,
	97436, 97464, 97474, 97476, 97480, 97488, 97502, 97510, 97516, 97550, 97564, 97592, 97648, 97666, 97668, 97672, 97680, 97694, 97696, 97724, 97734, 97740, 97752, 97774, 97830, 97836, 97850, 97862, 97868, 97880, 97902, 97906, 97908, 97926, 97932, 97944, 97968, 97998, 98012, 98018, 98020, 98024, 98038, 98618, 98674, 98676, 98838, 98854, 98874, 98892, 98904, 98926, 98930, 98932, 98968, 99006, 99042, 99044, 99048, 99062, 99166, 99194, 99246, 99286, 99350, 99366, 99372, 99386, 99398, 99416, 99438, 99442,
	99444, 99462, 99504, 99518, 99534, 99548, 99554, 99556, 99560, 99574, 99590, 99596, 99608, 99632, 99646, 99680, 99708, 99726, 99740, 99768, 99778, 99780, 99784, 99792, 99806, 99814, 99820, 99834, 99858, 99860, 99874, 99880, 99894, 99906, 99920, 99934, 99962, 99970, 99972, 99976, 99984, 99998, 1E5, 100028, 100038, 100044, 100056, 100078, 100082, 100084, 100142, 100174, 100188, 100246, 100262, 100268, 100306, 100308, 100390, 100396, 100410, 100422, 100428, 100440, 100462, 100466, 100468, 100486,
	100504, 100528, 100542, 100558, 100572, 100578, 100580, 100584, 100598, 100620, 100656, 100670, 100704, 100732, 100750, 100792, 100802, 100808, 100816, 100830, 100838, 100844, 100858, 100888, 100912, 100926, 100960, 100988, 101056, 101112, 101148, 101176, 101232, 101246, 101250, 101252, 101256, 101264, 101278, 101280, 101308, 101318, 101324, 101336, 101358, 101362, 101364, 101410, 101412, 101416, 101430, 101442, 101448, 101456, 101470, 101478, 101498, 101506, 101508, 101520, 101534, 101536, 101564,
	101580, 101618, 101620, 101636, 101640, 101648, 101662, 101664, 101692, 101696, 101752, 101766, 101784, 101838, 101858, 101860, 101864, 101934, 101938, 101940, 101966, 101980, 101986, 101988, 101992, 102030, 102044, 102072, 102082, 102084, 102088, 102096, 102138, 102166, 102182, 102188, 102214, 102220, 102232, 102254, 102282, 102290, 102292, 102306, 102308, 102312, 102326, 102444, 102458, 102470, 102476, 102488, 102514, 102516, 102534, 102552, 102576, 102590, 102606, 102620, 102626, 102632, 102646,
	102662, 102668, 102704, 102718, 102752, 102780, 102798, 102812, 102840, 102850, 102856, 102864, 102878, 102886, 102892, 102906, 102936, 102974, 103008, 103036, 103104, 103160, 103224, 103280, 103294, 103298, 103300, 103312, 103326, 103328, 103356, 103366, 103372, 103384, 103406, 103410, 103412, 103472, 103486, 103520, 103548, 103616, 103672, 103920, 103992, 104048, 104062, 104160, 104188, 104194, 104196, 104200, 104208, 104224, 104252, 104256, 104312, 104326, 104332, 104344, 104368, 104382, 104398,
	104412, 104418, 104420, 104424, 104482, 104484, 104514, 104520, 104528, 104542, 104550, 104570, 104578, 104580, 104592, 104606, 104608, 104636, 104652, 104690, 104692, 104706, 104712, 104734, 104736, 104764, 104768, 104824, 104838, 104856, 104910, 104930, 104932, 104936, 104968, 104976, 104990, 104992, 105020, 105024, 105080, 105200, 105240, 105278, 105312, 105372, 105410, 105412, 105416, 105424, 105446, 105518, 105524, 105550, 105564, 105570, 105572, 105576, 105614, 105628, 105656, 105666, 105672,
	105680, 105702, 105722, 105742, 105756, 105784, 105840, 105854, 105858, 105860, 105864, 105872, 105888, 105932, 105970, 105972, 106006, 106022, 106028, 106054, 106060, 106072, 106100, 106118, 106124, 106136, 106160, 106174, 106190, 106210, 106212, 106216, 106250, 106258, 106260, 106274, 106276, 106280, 106306, 106308, 106312, 106320, 106334, 106348, 106394, 106414, 106418, 106420, 106566, 106572, 106610, 106612, 106630, 106636, 106648, 106672, 106686, 106722, 106724, 106728, 106742, 106758, 106764,
	106776, 106800, 106814, 106848, 106876, 106894, 106908, 106936, 106946, 106948, 106952, 106960, 106974, 106982, 106988, 107032, 107056, 107070, 107104, 107132, 107200, 107256, 107292, 107320, 107376, 107390, 107394, 107396, 107400, 107408, 107422, 107424, 107452, 107462, 107468, 107480, 107502, 107506, 107508, 107544, 107568, 107582, 107616, 107644, 107712, 107768, 108016, 108060, 108088, 108144, 108158, 108256, 108284, 108290, 108292, 108296, 108304, 108318, 108320, 108348, 108352, 108408, 108422,
	108428, 108440, 108464, 108478, 108494, 108508, 108514, 108516, 108520, 108592, 108640, 108668, 108736, 108792, 109040, 109536, 109680, 109694, 109792, 109820, 110016, 110072, 110084, 110088, 110096, 110112, 110140, 110144, 110200, 110320, 110342, 110348, 110360, 110384, 110398, 110432, 110460, 110478, 110492, 110520, 110532, 110536, 110544, 110558, 110658, 110686, 110714, 110722, 110724, 110728, 110736, 110750, 110752, 110780, 110796, 110834, 110836, 110850, 110852, 110856, 110864, 110878, 110880,
	110908, 110912, 110968, 110982, 111E3, 111054, 111074, 111076, 111080, 111108, 111112, 111120, 111134, 111136, 111164, 111168, 111224, 111344, 111372, 111422, 111456, 111516, 111554, 111556, 111560, 111568, 111590, 111632, 111646, 111648, 111676, 111680, 111736, 111856, 112096, 112152, 112224, 112252, 112320, 112440, 112514, 112516, 112520, 112528, 112542, 112544, 112588, 112686, 112718, 112732, 112782, 112796, 112824, 112834, 112836, 112840, 112848, 112870, 112890, 112910, 112924, 112952, 113008,
	113022, 113026, 113028, 113032, 113040, 113054, 113056, 113100, 113138, 113140, 113166, 113180, 113208, 113264, 113278, 113376, 113404, 113416, 113424, 113440, 113468, 113472, 113560, 113614, 113634, 113636, 113640, 113686, 113702, 113708, 113734, 113740, 113752, 113778, 113780, 113798, 113804, 113816, 113840, 113854, 113870, 113890, 113892, 113896, 113926, 113932, 113944, 113968, 113982, 114016, 114044, 114076, 114114, 114116, 114120, 114128, 114150, 114170, 114194, 114196, 114210, 114212, 114216,
	114242, 114244, 114248, 114256, 114270, 114278, 114306, 114308, 114312, 114320, 114334, 114336, 114364, 114380, 114420, 114458, 114478, 114482, 114484, 114510, 114524, 114530, 114532, 114536, 114842, 114866, 114868, 114970, 114994, 114996, 115042, 115044, 115048, 115062, 115130, 115226, 115250, 115252, 115278, 115292, 115298, 115300, 115304, 115318, 115342, 115394, 115396, 115400, 115408, 115422, 115430, 115436, 115450, 115478, 115494, 115514, 115526, 115532, 115570, 115572, 115738, 115758, 115762,
	115764, 115790, 115804, 115810, 115812, 115816, 115830, 115854, 115868, 115896, 115906, 115912, 115920, 115934, 115942, 115948, 115962, 115996, 116024, 116080, 116094, 116098, 116100, 116104, 116112, 116126, 116128, 116156, 116166, 116172, 116184, 116206, 116210, 116212, 116246, 116262, 116268, 116282, 116294, 116300, 116312, 116334, 116338, 116340, 116358, 116364, 116376, 116400, 116414, 116430, 116444, 116450, 116452, 116456, 116498, 116500, 116514, 116520, 116534, 116546, 116548, 116552, 116560,
	116574, 116582, 116588, 116602, 116654, 116694, 116714, 116762, 116782, 116786, 116788, 116814, 116828, 116834, 116836, 116840, 116854, 116878, 116892, 116920, 116930, 116936, 116944, 116958, 116966, 116972, 116986, 117006, 117048, 117104, 117118, 117122, 117124, 117136, 117150, 117152, 117180, 117190, 117196, 117208, 117230, 117234, 117236, 117304, 117360, 117374, 117472, 117500, 117506, 117508, 117512, 117520, 117536, 117564, 117568, 117624, 117638, 117644, 117656, 117680, 117694, 117710, 117724,
	117730, 117732, 117736, 117750, 117782, 117798, 117804, 117818, 117830, 117848, 117874, 117876, 117894, 117936, 117950, 117966, 117986, 117988, 117992, 118022, 118028, 118040, 118064, 118078, 118112, 118140, 118172, 118210, 118212, 118216, 118224, 118238, 118246, 118266, 118306, 118312, 118338, 118352, 118366, 118374, 118394, 118402, 118404, 118408, 118416, 118430, 118432, 118460, 118476, 118514, 118516, 118574, 118578, 118580, 118606, 118620, 118626, 118628, 118632, 118678, 118694, 118700, 118730,
	118738, 118740, 118830, 118834, 118836, 118862, 118876, 118882, 118884, 118888, 118902, 118926, 118940, 118968, 118978, 118980, 118984, 118992, 119006, 119014, 119020, 119034, 119068, 119096, 119152, 119166, 119170, 119172, 119176, 119184, 119198, 119200, 119228, 119238, 119244, 119256, 119278, 119282, 119284, 119324, 119352, 119408, 119422, 119520, 119548, 119554, 119556, 119560, 119568, 119582, 119584, 119612, 119616, 119672, 119686, 119692, 119704, 119728, 119742, 119758, 119772, 119778, 119780,
	119784, 119798, 119920, 119934, 120032, 120060, 120256, 120312, 120324, 120328, 120336, 120352, 120384, 120440, 120560, 120582, 120588, 120600, 120624, 120638, 120672, 120700, 120718, 120732, 120760, 120770, 120772, 120776, 120784, 120798, 120806, 120812, 120870, 120876, 120890, 120902, 120908, 120920, 120946, 120948, 120966, 120972, 120984, 121008, 121022, 121038, 121058, 121060, 121064, 121078, 121100, 121112, 121136, 121150, 121184, 121212, 121244, 121282, 121284, 121288, 121296, 121318, 121338,
	121356, 121368, 121392, 121406, 121440, 121468, 121536, 121592, 121656, 121730, 121732, 121736, 121744, 121758, 121760, 121804, 121842, 121844, 121890, 121922, 121924, 121928, 121936, 121950, 121958, 121978, 121986, 121988, 121992, 122E3, 122014, 122016, 122044, 122060, 122098, 122100, 122116, 122120, 122128, 122142, 122144, 122172, 122176, 122232, 122246, 122264, 122318, 122338, 122340, 122344, 122414, 122418, 122420, 122446, 122460, 122466, 122468, 122472, 122510, 122524, 122552, 122562, 122564,
	122568, 122576, 122598, 122618, 122646, 122662, 122668, 122694, 122700, 122712, 122738, 122740, 122762, 122770, 122772, 122786, 122788, 122792, 123018, 123026, 123028, 123042, 123044, 123048, 123062, 123098, 123146, 123154, 123156, 123170, 123172, 123176, 123190, 123202, 123204, 123208, 123216, 123238, 123244, 123258, 123290, 123314, 123316, 123402, 123410, 123412, 123426, 123428, 123432, 123446, 123458, 123464, 123472, 123486, 123494, 123500, 123514, 123522, 123524, 123528, 123536, 123552, 123580,
	123590, 123596, 123608, 123630, 123634, 123636, 123674, 123698, 123700, 123740, 123746, 123748, 123752, 123834, 123914, 123922, 123924, 123938, 123944, 123958, 123970, 123976, 123984, 123998, 124006, 124012, 124026, 124034, 124036, 124048, 124062, 124064, 124092, 124102, 124108, 124120, 124142, 124146, 124148, 124162, 124164, 124168, 124176, 124190, 124192, 124220, 124224, 124280, 124294, 124300, 124312, 124336, 124350, 124366, 124380, 124386, 124388, 124392, 124406, 124442, 124462, 124466, 124468,
	124494, 124508, 124514, 124520, 124558, 124572, 124600, 124610, 124612, 124616, 124624, 124646, 124666, 124694, 124710, 124716, 124730, 124742, 124748, 124760, 124786, 124788, 124818, 124820, 124834, 124836, 124840, 124854, 124946, 124948, 124962, 124964, 124968, 124982, 124994, 124996, 125E3, 125008, 125022, 125030, 125036, 125050, 125058, 125060, 125064, 125072, 125086, 125088, 125116, 125126, 125132, 125144, 125166, 125170, 125172, 125186, 125188, 125192, 125200, 125216, 125244, 125248, 125304,
	125318, 125324, 125336, 125360, 125374, 125390, 125404, 125410, 125412, 125416, 125430, 125444, 125448, 125456, 125472, 125504, 125560, 125680, 125702, 125708, 125720, 125744, 125758, 125792, 125820, 125838, 125852, 125880, 125890, 125892, 125896, 125904, 125918, 125926, 125932, 125978, 125998, 126002, 126004, 126030, 126044, 126050, 126052, 126056, 126094, 126108, 126136, 126146, 126148, 126152, 126160, 126182, 126202, 126222, 126236, 126264, 126320, 126334, 126338, 126340, 126344, 126352, 126366,
	126368, 126412, 126450, 126452, 126486, 126502, 126508, 126522, 126534, 126540, 126552, 126574, 126578, 126580, 126598, 126604, 126616, 126640, 126654, 126670, 126684, 126690, 126692, 126696, 126738, 126754, 126756, 126760, 126774, 126786, 126788, 126792, 126800, 126814, 126822, 126828, 126842, 126894, 126898, 126900, 126934, 127126, 127142, 127148, 127162, 127178, 127186, 127188, 127254, 127270, 127276, 127290, 127302, 127308, 127320, 127342, 127346, 127348, 127370, 127378, 127380, 127394, 127396,
	127400, 127450, 127510, 127526, 127532, 127546, 127558, 127576, 127598, 127602, 127604, 127622, 127628, 127640, 127664, 127678, 127694, 127708, 127714, 127716, 127720, 127734, 127754, 127762, 127764, 127778, 127784, 127810, 127812, 127816, 127824, 127838, 127846, 127866, 127898, 127918, 127922, 127924, 128022, 128038, 128044, 128058, 128070, 128076, 128088, 128110, 128114, 128116, 128134, 128140, 128152, 128176, 128190, 128206, 128220, 128226, 128228, 128232, 128246, 128262, 128268, 128280, 128304,
	128318, 128352, 128380, 128398, 128412, 128440, 128450, 128452, 128456, 128464, 128478, 128486, 128492, 128506, 128522, 128530, 128532, 128546, 128548, 128552, 128566, 128578, 128580, 128584, 128592, 128606, 128614, 128634, 128642, 128644, 128648, 128656, 128670, 128672, 128700, 128716, 128754, 128756, 128794, 128814, 128818, 128820, 128846, 128860, 128866, 128868, 128872, 128886, 128918, 128934, 128940, 128954, 128978, 128980, 129178, 129198, 129202, 129204, 129238, 129258, 129306, 129326, 129330,
	129332, 129358, 129372, 129378, 129380, 129384, 129398, 129430, 129446, 129452, 129466, 129482, 129490, 129492, 129562, 129582, 129586, 129588, 129614, 129628, 129634, 129636, 129640, 129654, 129678, 129692, 129720, 129730, 129732, 129736, 129744, 129758, 129766, 129772, 129814, 129830, 129836, 129850, 129862, 129868, 129880, 129902, 129906, 129908, 129930, 129938, 129940, 129954, 129956, 129960, 129974, 130010]);
/** @type {!Int32Array} */
that.CODEWORD_TABLE = Int32Array.from([2627, 1819, 2622, 2621, 1813, 1812, 2729, 2724, 2723, 2779, 2774, 2773, 902, 896, 908, 868, 865, 861, 859, 2511, 873, 871, 1780, 835, 2493, 825, 2491, 842, 837, 844, 1764, 1762, 811, 810, 809, 2483, 807, 2482, 806, 2480, 815, 814, 813, 812, 2484, 817, 816, 1745, 1744, 1742, 1746, 2655, 2637, 2635, 2626, 2625, 2623, 2628, 1820, 2752, 2739, 2737, 2728, 2727, 2725, 2730, 2785, 2783, 2778, 2777, 2775, 2780, 787, 781, 747, 739, 736, 2413, 754, 752, 1719, 692, 689,
	681, 2371, 678, 2369, 700, 697, 694, 703, 1688, 1686, 642, 638, 2343, 631, 2341, 627, 2338, 651, 646, 643, 2345, 654, 652, 1652, 1650, 1647, 1654, 601, 599, 2322, 596, 2321, 594, 2319, 2317, 611, 610, 608, 606, 2324, 603, 2323, 615, 614, 612, 1617, 1616, 1614, 1612, 616, 1619, 1618, 2575, 2538, 2536, 905, 901, 898, 909, 2509, 2507, 2504, 870, 867, 864, 860, 2512, 875, 872, 1781, 2490, 2489, 2487, 2485, 1748, 836, 834, 832, 830, 2494, 827, 2492, 843, 841, 839, 845, 1765, 1763, 2701, 2676, 2674,
	2653, 2648, 2656, 2634, 2633, 2631, 2629, 1821, 2638, 2636, 2770, 2763, 2761, 2750, 2745, 2753, 2736, 2735, 2733, 2731, 1848, 2740, 2738, 2786, 2784, 591, 588, 576, 569, 566, 2296, 1590, 537, 534, 526, 2276, 522, 2274, 545, 542, 539, 548, 1572, 1570, 481, 2245, 466, 2242, 462, 2239, 492, 485, 482, 2249, 496, 494, 1534, 1531, 1528, 1538, 413, 2196, 406, 2191, 2188, 425, 419, 2202, 415, 2199, 432, 430, 427, 1472, 1467, 1464, 433, 1476, 1474, 368, 367, 2160, 365, 2159, 362, 2157, 2155, 2152, 378,
	377, 375, 2166, 372, 2165, 369, 2162, 383, 381, 379, 2168, 1419, 1418, 1416, 1414, 385, 1411, 384, 1423, 1422, 1420, 1424, 2461, 802, 2441, 2439, 790, 786, 783, 794, 2409, 2406, 2403, 750, 742, 738, 2414, 756, 753, 1720, 2367, 2365, 2362, 2359, 1663, 693, 691, 684, 2373, 680, 2370, 702, 699, 696, 704, 1690, 1687, 2337, 2336, 2334, 2332, 1624, 2329, 1622, 640, 637, 2344, 634, 2342, 630, 2340, 650, 648, 645, 2346, 655, 653, 1653, 1651, 1649, 1655, 2612, 2597, 2595, 2571, 2568, 2565, 2576, 2534, 2529,
	2526, 1787, 2540, 2537, 907, 904, 900, 910, 2503, 2502, 2500, 2498, 1768, 2495, 1767, 2510, 2508, 2506, 869, 866, 863, 2513, 876, 874, 1782, 2720, 2713, 2711, 2697, 2694, 2691, 2702, 2672, 2670, 2664, 1828, 2678, 2675, 2647, 2646, 2644, 2642, 1823, 2639, 1822, 2654, 2652, 2650, 2657, 2771, 1855, 2765, 2762, 1850, 1849, 2751, 2749, 2747, 2754, 353, 2148, 344, 342, 336, 2142, 332, 2140, 345, 1375, 1373, 306, 2130, 299, 2128, 295, 2125, 319, 314, 311, 2132, 1354, 1352, 1349, 1356, 262, 257, 2101,
	253, 2096, 2093, 274, 273, 267, 2107, 263, 2104, 280, 278, 275, 1316, 1311, 1308, 1320, 1318, 2052, 202, 2050, 2044, 2040, 219, 2063, 212, 2060, 208, 2055, 224, 221, 2066, 1260, 1258, 1252, 231, 1248, 229, 1266, 1264, 1261, 1268, 155, 1998, 153, 1996, 1994, 1991, 1988, 165, 164, 2007, 162, 2006, 159, 2003, 2E3, 172, 171, 169, 2012, 166, 2010, 1186, 1184, 1182, 1179, 175, 1176, 173, 1192, 1191, 1189, 1187, 176, 1194, 1193, 2313, 2307, 2305, 592, 589, 2294, 2292, 2289, 578, 572, 568, 2297, 580, 1591,
	2272, 2267, 2264, 1547, 538, 536, 529, 2278, 525, 2275, 547, 544, 541, 1574, 1571, 2237, 2235, 2229, 1493, 2225, 1489, 478, 2247, 470, 2244, 465, 2241, 493, 488, 484, 2250, 498, 495, 1536, 1533, 1530, 1539, 2187, 2186, 2184, 2182, 1432, 2179, 1430, 2176, 1427, 414, 412, 2197, 409, 2195, 405, 2193, 2190, 426, 424, 421, 2203, 418, 2201, 431, 429, 1473, 1471, 1469, 1466, 434, 1477, 1475, 2478, 2472, 2470, 2459, 2457, 2454, 2462, 803, 2437, 2432, 2429, 1726, 2443, 2440, 792, 789, 785, 2401, 2399, 2393,
	1702, 2389, 1699, 2411, 2408, 2405, 745, 741, 2415, 758, 755, 1721, 2358, 2357, 2355, 2353, 1661, 2350, 1660, 2347, 1657, 2368, 2366, 2364, 2361, 1666, 690, 687, 2374, 683, 2372, 701, 698, 705, 1691, 1689, 2619, 2617, 2610, 2608, 2605, 2613, 2593, 2588, 2585, 1803, 2599, 2596, 2563, 2561, 2555, 1797, 2551, 1795, 2573, 2570, 2567, 2577, 2525, 2524, 2522, 2520, 1786, 2517, 1785, 2514, 1783, 2535, 2533, 2531, 2528, 1788, 2541, 2539, 906, 903, 911, 2721, 1844, 2715, 2712, 1838, 1836, 2699, 2696, 2693,
	2703, 1827, 1826, 1824, 2673, 2671, 2669, 2666, 1829, 2679, 2677, 1858, 1857, 2772, 1854, 1853, 1851, 1856, 2766, 2764, 143, 1987, 139, 1986, 135, 133, 131, 1984, 128, 1983, 125, 1981, 138, 137, 136, 1985, 1133, 1132, 1130, 112, 110, 1974, 107, 1973, 104, 1971, 1969, 122, 121, 119, 117, 1977, 114, 1976, 124, 1115, 1114, 1112, 1110, 1117, 1116, 84, 83, 1953, 81, 1952, 78, 1950, 1948, 1945, 94, 93, 91, 1959, 88, 1958, 85, 1955, 99, 97, 95, 1961, 1086, 1085, 1083, 1081, 1078, 100, 1090, 1089, 1087,
	1091, 49, 47, 1917, 44, 1915, 1913, 1910, 1907, 59, 1926, 56, 1925, 53, 1922, 1919, 66, 64, 1931, 61, 1929, 1042, 1040, 1038, 71, 1035, 70, 1032, 68, 1048, 1047, 1045, 1043, 1050, 1049, 12, 10, 1869, 1867, 1864, 1861, 21, 1880, 19, 1877, 1874, 1871, 28, 1888, 25, 1886, 22, 1883, 982, 980, 977, 974, 32, 30, 991, 989, 987, 984, 34, 995, 994, 992, 2151, 2150, 2147, 2146, 2144, 356, 355, 354, 2149, 2139, 2138, 2136, 2134, 1359, 343, 341, 338, 2143, 335, 2141, 348, 347, 346, 1376, 1374, 2124, 2123,
	2121, 2119, 1326, 2116, 1324, 310, 308, 305, 2131, 302, 2129, 298, 2127, 320, 318, 316, 313, 2133, 322, 321, 1355, 1353, 1351, 1357, 2092, 2091, 2089, 2087, 1276, 2084, 1274, 2081, 1271, 259, 2102, 256, 2100, 252, 2098, 2095, 272, 269, 2108, 266, 2106, 281, 279, 277, 1317, 1315, 1313, 1310, 282, 1321, 1319, 2039, 2037, 2035, 2032, 1203, 2029, 1200, 1197, 207, 2053, 205, 2051, 201, 2049, 2046, 2043, 220, 218, 2064, 215, 2062, 211, 2059, 228, 226, 223, 2069, 1259, 1257, 1254, 232, 1251, 230, 1267,
	1265, 1263, 2316, 2315, 2312, 2311, 2309, 2314, 2304, 2303, 2301, 2299, 1593, 2308, 2306, 590, 2288, 2287, 2285, 2283, 1578, 2280, 1577, 2295, 2293, 2291, 579, 577, 574, 571, 2298, 582, 581, 1592, 2263, 2262, 2260, 2258, 1545, 2255, 1544, 2252, 1541, 2273, 2271, 2269, 2266, 1550, 535, 532, 2279, 528, 2277, 546, 543, 549, 1575, 1573, 2224, 2222, 2220, 1486, 2217, 1485, 2214, 1482, 1479, 2238, 2236, 2234, 2231, 1496, 2228, 1492, 480, 477, 2248, 473, 2246, 469, 2243, 490, 487, 2251, 497, 1537, 1535,
	1532, 2477, 2476, 2474, 2479, 2469, 2468, 2466, 2464, 1730, 2473, 2471, 2453, 2452, 2450, 2448, 1729, 2445, 1728, 2460, 2458, 2456, 2463, 805, 804, 2428, 2427, 2425, 2423, 1725, 2420, 1724, 2417, 1722, 2438, 2436, 2434, 2431, 1727, 2444, 2442, 793, 791, 788, 795, 2388, 2386, 2384, 1697, 2381, 1696, 2378, 1694, 1692, 2402, 2400, 2398, 2395, 1703, 2392, 1701, 2412, 2410, 2407, 751, 748, 744, 2416, 759, 757, 1807, 2620, 2618, 1806, 1805, 2611, 2609, 2607, 2614, 1802, 1801, 1799, 2594, 2592, 2590,
	2587, 1804, 2600, 2598, 1794, 1793, 1791, 1789, 2564, 2562, 2560, 2557, 1798, 2554, 1796, 2574, 2572, 2569, 2578, 1847, 1846, 2722, 1843, 1842, 1840, 1845, 2716, 2714, 1835, 1834, 1832, 1830, 1839, 1837, 2700, 2698, 2695, 2704, 1817, 1811, 1810, 897, 862, 1777, 829, 826, 838, 1760, 1758, 808, 2481, 1741, 1740, 1738, 1743, 2624, 1818, 2726, 2776, 782, 740, 737, 1715, 686, 679, 695, 1682, 1680, 639, 628, 2339, 647, 644, 1645, 1643, 1640, 1648, 602, 600, 597, 595, 2320, 593, 2318, 609, 607, 604, 1611,
	1610, 1608, 1606, 613, 1615, 1613, 2328, 926, 924, 892, 886, 899, 857, 850, 2505, 1778, 824, 823, 821, 819, 2488, 818, 2486, 833, 831, 828, 840, 1761, 1759, 2649, 2632, 2630, 2746, 2734, 2732, 2782, 2781, 570, 567, 1587, 531, 527, 523, 540, 1566, 1564, 476, 467, 463, 2240, 486, 483, 1524, 1521, 1518, 1529, 411, 403, 2192, 399, 2189, 423, 416, 1462, 1457, 1454, 428, 1468, 1465, 2210, 366, 363, 2158, 360, 2156, 357, 2153, 376, 373, 370, 2163, 1410, 1409, 1407, 1405, 382, 1402, 380, 1417, 1415, 1412,
	1421, 2175, 2174, 777, 774, 771, 784, 732, 725, 722, 2404, 743, 1716, 676, 674, 668, 2363, 665, 2360, 685, 1684, 1681, 626, 624, 622, 2335, 620, 2333, 617, 2330, 641, 635, 649, 1646, 1644, 1642, 2566, 928, 925, 2530, 2527, 894, 891, 888, 2501, 2499, 2496, 858, 856, 854, 851, 1779, 2692, 2668, 2665, 2645, 2643, 2640, 2651, 2768, 2759, 2757, 2744, 2743, 2741, 2748, 352, 1382, 340, 337, 333, 1371, 1369, 307, 300, 296, 2126, 315, 312, 1347, 1342, 1350, 261, 258, 250, 2097, 246, 2094, 271, 268, 264,
	1306, 1301, 1298, 276, 1312, 1309, 2115, 203, 2048, 195, 2045, 191, 2041, 213, 209, 2056, 1246, 1244, 1238, 225, 1234, 222, 1256, 1253, 1249, 1262, 2080, 2079, 154, 1997, 150, 1995, 147, 1992, 1989, 163, 160, 2004, 156, 2001, 1175, 1174, 1172, 1170, 1167, 170, 1164, 167, 1185, 1183, 1180, 1177, 174, 1190, 1188, 2025, 2024, 2022, 587, 586, 564, 559, 556, 2290, 573, 1588, 520, 518, 512, 2268, 508, 2265, 530, 1568, 1565, 461, 457, 2233, 450, 2230, 446, 2226, 479, 471, 489, 1526, 1523, 1520, 397, 395,
	2185, 392, 2183, 389, 2180, 2177, 410, 2194, 402, 422, 1463, 1461, 1459, 1456, 1470, 2455, 799, 2433, 2430, 779, 776, 773, 2397, 2394, 2390, 734, 728, 724, 746, 1717, 2356, 2354, 2351, 2348, 1658, 677, 675, 673, 670, 667, 688, 1685, 1683, 2606, 2589, 2586, 2559, 2556, 2552, 927, 2523, 2521, 2518, 2515, 1784, 2532, 895, 893, 890, 2718, 2709, 2707, 2689, 2687, 2684, 2663, 2662, 2660, 2658, 1825, 2667, 2769, 1852, 2760, 2758, 142, 141, 1139, 1138, 134, 132, 129, 126, 1982, 1129, 1128, 1126, 1131,
	113, 111, 108, 105, 1972, 101, 1970, 120, 118, 115, 1109, 1108, 1106, 1104, 123, 1113, 1111, 82, 79, 1951, 75, 1949, 72, 1946, 92, 89, 86, 1956, 1077, 1076, 1074, 1072, 98, 1069, 96, 1084, 1082, 1079, 1088, 1968, 1967, 48, 45, 1916, 42, 1914, 39, 1911, 1908, 60, 57, 54, 1923, 50, 1920, 1031, 1030, 1028, 1026, 67, 1023, 65, 1020, 62, 1041, 1039, 1036, 1033, 69, 1046, 1044, 1944, 1943, 1941, 11, 9, 1868, 7, 1865, 1862, 1859, 20, 1878, 16, 1875, 13, 1872, 970, 968, 966, 963, 29, 960, 26, 23, 983,
	981, 978, 975, 33, 971, 31, 990, 988, 985, 1906, 1904, 1902, 993, 351, 2145, 1383, 331, 330, 328, 326, 2137, 323, 2135, 339, 1372, 1370, 294, 293, 291, 289, 2122, 286, 2120, 283, 2117, 309, 303, 317, 1348, 1346, 1344, 245, 244, 242, 2090, 239, 2088, 236, 2085, 2082, 260, 2099, 249, 270, 1307, 1305, 1303, 1300, 1314, 189, 2038, 186, 2036, 183, 2033, 2030, 2026, 206, 198, 2047, 194, 216, 1247, 1245, 1243, 1240, 227, 1237, 1255, 2310, 2302, 2300, 2286, 2284, 2281, 565, 563, 561, 558, 575, 1589, 2261,
	2259, 2256, 2253, 1542, 521, 519, 517, 514, 2270, 511, 533, 1569, 1567, 2223, 2221, 2218, 2215, 1483, 2211, 1480, 459, 456, 453, 2232, 449, 474, 491, 1527, 1525, 1522, 2475, 2467, 2465, 2451, 2449, 2446, 801, 800, 2426, 2424, 2421, 2418, 1723, 2435, 780, 778, 775, 2387, 2385, 2382, 2379, 1695, 2375, 1693, 2396, 735, 733, 730, 727, 749, 1718, 2616, 2615, 2604, 2603, 2601, 2584, 2583, 2581, 2579, 1800, 2591, 2550, 2549, 2547, 2545, 1792, 2542, 1790, 2558, 929, 2719, 1841, 2710, 2708, 1833, 1831,
	2690, 2688, 2686, 1815, 1809, 1808, 1774, 1756, 1754, 1737, 1736, 1734, 1739, 1816, 1711, 1676, 1674, 633, 629, 1638, 1636, 1633, 1641, 598, 1605, 1604, 1602, 1600, 605, 1609, 1607, 2327, 887, 853, 1775, 822, 820, 1757, 1755, 1584, 524, 1560, 1558, 468, 464, 1514, 1511, 1508, 1519, 408, 404, 400, 1452, 1447, 1444, 417, 1458, 1455, 2208, 364, 361, 358, 2154, 1401, 1400, 1398, 1396, 374, 1393, 371, 1408, 1406, 1403, 1413, 2173, 2172, 772, 726, 723, 1712, 672, 669, 666, 682, 1678, 1675, 625, 623,
	621, 618, 2331, 636, 632, 1639, 1637, 1635, 920, 918, 884, 880, 889, 849, 848, 847, 846, 2497, 855, 852, 1776, 2641, 2742, 2787, 1380, 334, 1367, 1365, 301, 297, 1340, 1338, 1335, 1343, 255, 251, 247, 1296, 1291, 1288, 265, 1302, 1299, 2113, 204, 196, 192, 2042, 1232, 1230, 1224, 214, 1220, 210, 1242, 1239, 1235, 1250, 2077, 2075, 151, 148, 1993, 144, 1990, 1163, 1162, 1160, 1158, 1155, 161, 1152, 157, 1173, 1171, 1168, 1165, 168, 1181, 1178, 2021, 2020, 2018, 2023, 585, 560, 557, 1585, 516, 509,
	1562, 1559, 458, 447, 2227, 472, 1516, 1513, 1510, 398, 396, 393, 390, 2181, 386, 2178, 407, 1453, 1451, 1449, 1446, 420, 1460, 2209, 769, 764, 720, 712, 2391, 729, 1713, 664, 663, 661, 659, 2352, 656, 2349, 671, 1679, 1677, 2553, 922, 919, 2519, 2516, 885, 883, 881, 2685, 2661, 2659, 2767, 2756, 2755, 140, 1137, 1136, 130, 127, 1125, 1124, 1122, 1127, 109, 106, 102, 1103, 1102, 1100, 1098, 116, 1107, 1105, 1980, 80, 76, 73, 1947, 1068, 1067, 1065, 1063, 90, 1060, 87, 1075, 1073, 1070, 1080, 1966,
	1965, 46, 43, 40, 1912, 36, 1909, 1019, 1018, 1016, 1014, 58, 1011, 55, 1008, 51, 1029, 1027, 1024, 1021, 63, 1037, 1034, 1940, 1939, 1937, 1942, 8, 1866, 4, 1863, 1, 1860, 956, 954, 952, 949, 946, 17, 14, 969, 967, 964, 961, 27, 957, 24, 979, 976, 972, 1901, 1900, 1898, 1896, 986, 1905, 1903, 350, 349, 1381, 329, 327, 324, 1368, 1366, 292, 290, 287, 284, 2118, 304, 1341, 1339, 1337, 1345, 243, 240, 237, 2086, 233, 2083, 254, 1297, 1295, 1293, 1290, 1304, 2114, 190, 187, 184, 2034, 180, 2031, 177,
	2027, 199, 1233, 1231, 1229, 1226, 217, 1223, 1241, 2078, 2076, 584, 555, 554, 552, 550, 2282, 562, 1586, 507, 506, 504, 502, 2257, 499, 2254, 515, 1563, 1561, 445, 443, 441, 2219, 438, 2216, 435, 2212, 460, 454, 475, 1517, 1515, 1512, 2447, 798, 797, 2422, 2419, 770, 768, 766, 2383, 2380, 2376, 721, 719, 717, 714, 731, 1714, 2602, 2582, 2580, 2548, 2546, 2543, 923, 921, 2717, 2706, 2705, 2683, 2682, 2680, 1771, 1752, 1750, 1733, 1732, 1731, 1735, 1814, 1707, 1670, 1668, 1631, 1629, 1626, 1634,
	1599, 1598, 1596, 1594, 1603, 1601, 2326, 1772, 1753, 1751, 1581, 1554, 1552, 1504, 1501, 1498, 1509, 1442, 1437, 1434, 401, 1448, 1445, 2206, 1392, 1391, 1389, 1387, 1384, 359, 1399, 1397, 1394, 1404, 2171, 2170, 1708, 1672, 1669, 619, 1632, 1630, 1628, 1773, 1378, 1363, 1361, 1333, 1328, 1336, 1286, 1281, 1278, 248, 1292, 1289, 2111, 1218, 1216, 1210, 197, 1206, 193, 1228, 1225, 1221, 1236, 2073, 2071, 1151, 1150, 1148, 1146, 152, 1143, 149, 1140, 145, 1161, 1159, 1156, 1153, 158, 1169, 1166,
	2017, 2016, 2014, 2019, 1582, 510, 1556, 1553, 452, 448, 1506, 1500, 394, 391, 387, 1443, 1441, 1439, 1436, 1450, 2207, 765, 716, 713, 1709, 662, 660, 657, 1673, 1671, 916, 914, 879, 878, 877, 882, 1135, 1134, 1121, 1120, 1118, 1123, 1097, 1096, 1094, 1092, 103, 1101, 1099, 1979, 1059, 1058, 1056, 1054, 77, 1051, 74, 1066, 1064, 1061, 1071, 1964, 1963, 1007, 1006, 1004, 1002, 999, 41, 996, 37, 1017, 1015, 1012, 1009, 52, 1025, 1022, 1936, 1935, 1933, 1938, 942, 940, 938, 935, 932, 5, 2, 955, 953,
	950, 947, 18, 943, 15, 965, 962, 958, 1895, 1894, 1892, 1890, 973, 1899, 1897, 1379, 325, 1364, 1362, 288, 285, 1334, 1332, 1330, 241, 238, 234, 1287, 1285, 1283, 1280, 1294, 2112, 188, 185, 181, 178, 2028, 1219, 1217, 1215, 1212, 200, 1209, 1227, 2074, 2072, 583, 553, 551, 1583, 505, 503, 500, 513, 1557, 1555, 444, 442, 439, 436, 2213, 455, 451, 1507, 1505, 1502, 796, 763, 762, 760, 767, 711, 710, 708, 706, 2377, 718, 715, 1710, 2544, 917, 915, 2681, 1627, 1597, 1595, 2325, 1769, 1749, 1747, 1499,
	1438, 1435, 2204, 1390, 1388, 1385, 1395, 2169, 2167, 1704, 1665, 1662, 1625, 1623, 1620, 1770, 1329, 1282, 1279, 2109, 1214, 1207, 1222, 2068, 2065, 1149, 1147, 1144, 1141, 146, 1157, 1154, 2013, 2011, 2008, 2015, 1579, 1549, 1546, 1495, 1487, 1433, 1431, 1428, 1425, 388, 1440, 2205, 1705, 658, 1667, 1664, 1119, 1095, 1093, 1978, 1057, 1055, 1052, 1062, 1962, 1960, 1005, 1003, 1E3, 997, 38, 1013, 1010, 1932, 1930, 1927, 1934, 941, 939, 936, 933, 6, 930, 3, 951, 948, 944, 1889, 1887, 1884, 1881,
	959, 1893, 1891, 35, 1377, 1360, 1358, 1327, 1325, 1322, 1331, 1277, 1275, 1272, 1269, 235, 1284, 2110, 1205, 1204, 1201, 1198, 182, 1195, 179, 1213, 2070, 2067, 1580, 501, 1551, 1548, 440, 437, 1497, 1494, 1490, 1503, 761, 709, 707, 1706, 913, 912, 2198, 1386, 2164, 2161, 1621, 1766, 2103, 1208, 2058, 2054, 1145, 1142, 2005, 2002, 1999, 2009, 1488, 1429, 1426, 2200, 1698, 1659, 1656, 1975, 1053, 1957, 1954, 1001, 998, 1924, 1921, 1918, 1928, 937, 934, 931, 1879, 1876, 1873, 1870, 945, 1885, 1882,
	1323, 1273, 1270, 2105, 1202, 1199, 1196, 1211, 2061, 2057, 1576, 1543, 1540, 1484, 1481, 1478, 1491, 1700]);
var Store = function () {
	/**
	 * @param {number} options
	 * @param {?} points
	 * @return {undefined}
	 */
	function TempusDominusBootstrap3(options, points) {
		_classCallCheck2(this, TempusDominusBootstrap3);
		/** @type {number} */
		this.bits = options;
		this.points = points;
	}
	_createClass2(TempusDominusBootstrap3, [{
		key: "getBits",
		value: function getTarget() {
			return this.bits;
		}
	}, {
		key: "getPoints",
		value: function interfaceFunction() {
			return this.points;
		}
	}]);
	return TempusDominusBootstrap3;
}();
var queue = function () {
	/**
	 * @return {undefined}
	 */
	function self() {
		_classCallCheck2(this, self);
	}
	_createClass2(self, null, [{
		key: "detectMultiple",
		value: function refresh(pro, next, data) {
			var options = pro.getBlackMatrix();
			var opts = self.detect(data, options);
			return opts.length || ((options = options.clone()).rotate180(), opts = self.detect(data, options)), new Store(options, opts);
		}
	}, {
		key: "detect",
		value: function update(e, options) {
			/** @type {!Array} */
			var parent = new Array;
			/** @type {number} */
			var i = 0;
			/** @type {number} */
			var startY = 0;
			/** @type {boolean} */
			var y2Tooltip = false;
			for (; i < options.getHeight();) {
				var path = self.findVertices(options, i, startY);
				if (null != path[0] || null != path[3]) {
					if (y2Tooltip = true, parent.push(path), !e) {
						break;
					}
					if (null != path[2]) {
						/** @type {number} */
						startY = Math.trunc(path[2].getX());
						/** @type {number} */
						i = Math.trunc(path[2].getY());
					} else {
						/** @type {number} */
						startY = Math.trunc(path[4].getX());
						/** @type {number} */
						i = Math.trunc(path[4].getY());
					}
				} else {
					if (!y2Tooltip) {
						break;
					}
					/** @type {boolean} */
					y2Tooltip = false;
					/** @type {number} */
					startY = 0;
					/** @type {boolean} */
					var _iteratorNormalCompletion3 = true;
					/** @type {boolean} */
					var _didIteratorError53 = false;
					var _iteratorError17 = undefined;
					try {
						var _iterator3 = parent[Symbol.iterator]();
						var $__6;
						for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
							var item = $__6.value;
							if (null != item[1]) {
								/** @type {number} */
								i = Math.trunc(Math.max(i, item[1].getY()));
							}
							if (null != item[3]) {
								/** @type {number} */
								i = Math.max(i, Math.trunc(item[3].getY()));
							}
						}
					} catch (err) {
						/** @type {boolean} */
						_didIteratorError53 = true;
						_iteratorError17 = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion3 && _iterator3.return) {
								_iterator3.return();
							}
						} finally {
							if (_didIteratorError53) {
								throw _iteratorError17;
							}
						}
					}
					i = i + self.ROW_STEP;
				}
			}
			return parent;
		}
	}, {
		key: "findVertices",
		value: function update(matrix, duration, error) {
			var height = matrix.getHeight();
			var width = matrix.getWidth();
			/** @type {!Array} */
			var points = new Array(8);
			return self.copyToResult(points, self.findRowsWithPattern(matrix, height, width, duration, error, self.START_PATTERN), self.INDEXES_START_PATTERN), null != points[4] && (error = Math.trunc(points[4].getX()), duration = Math.trunc(points[4].getY())), self.copyToResult(points, self.findRowsWithPattern(matrix, height, width, duration, error, self.STOP_PATTERN), self.INDEXES_STOP_PATTERN), points;
		}
	}, {
		key: "copyToResult",
		value: function copyToResult(result, tmpResult, destinationIndexes) {
			/** @type {number} */
			var i = 0;
			for (; i < destinationIndexes.length; i++) {
				result[destinationIndexes[i]] = tmpResult[i];
			}
		}
	}, {
		key: "findRowsWithPattern",
		value: function init(matrix, index, count, i, size, hash) {
			/** @type {!Array} */
			var values = new Array(4);
			/** @type {boolean} */
			var a = false;
			/** @type {!Int32Array} */
			var x = new Int32Array(hash.length);
			for (; i < index; i = i + self.ROW_STEP) {
				var result = self.findGuardPattern(matrix, size, i, count, false, hash, x);
				if (null != result) {
					for (; i > 0;) {
						var v = self.findGuardPattern(matrix, size, --i, count, false, hash, x);
						if (null == v) {
							i++;
							break;
						}
						result = v;
					}
					values[0] = new type(result[0], i);
					values[1] = new type(result[1], i);
					/** @type {boolean} */
					a = true;
					break;
				}
			}
			var value = i + 1;
			if (a) {
				/** @type {number} */
				var msSinceEnd = 0;
				/** @type {!Int32Array} */
				var p = Int32Array.from([Math.trunc(values[0].getX()), Math.trunc(values[1].getX())]);
				for (; value < index; value++) {
					var v = self.findGuardPattern(matrix, p[0], value, count, false, hash, x);
					if (null != v && Math.abs(p[0] - v[0]) < self.MAX_PATTERN_DRIFT && Math.abs(p[1] - v[1]) < self.MAX_PATTERN_DRIFT) {
						p = v;
						/** @type {number} */
						msSinceEnd = 0;
					} else {
						if (msSinceEnd > self.SKIPPED_ROW_COUNT_MAX) {
							break;
						}
						msSinceEnd++;
					}
				}
				/** @type {number} */
				value = value - (msSinceEnd + 1);
				values[2] = new type(p[0], value);
				values[3] = new type(p[1], value);
			}
			return value - i < self.BARCODE_MIN_HEIGHT && b.fill(values, null), values;
		}
	}, {
		key: "findGuardPattern",
		value: function deserialize(obj, i, cb, value, element, o, c) {
			b.fillWithin(c, 0, c.length, 0);
			/** @type {number} */
			var value = i;
			/** @type {number} */
			var interval = 0;
			for (; obj.get(value, cb) && value > 0 && interval++ < self.MAX_PIXEL_DRIFT;) {
				value--;
			}
			var x = value;
			/** @type {number} */
			var p = 0;
			var pages = o.length;
			/** @type {boolean} */
			var outer = element;
			for (; x < value; x++) {
				if (obj.get(x, cb) !== outer) {
					c[p]++;
				} else {
					if (p === pages - 1) {
						if (self.patternMatchVariance(c, o, self.MAX_INDIVIDUAL_VARIANCE) < self.MAX_AVG_VARIANCE) {
							return new Int32Array([value, x]);
						}
						value = value + (c[0] + c[1]);
						System.arraycopy(c, 2, c, 0, p - 1);
						/** @type {number} */
						c[p - 1] = 0;
						/** @type {number} */
						c[p] = 0;
						p--;
					} else {
						p++;
					}
					/** @type {number} */
					c[p] = 1;
					/** @type {boolean} */
					outer = !outer;
				}
			}
			return p === pages - 1 && self.patternMatchVariance(c, o, self.MAX_INDIVIDUAL_VARIANCE) < self.MAX_AVG_VARIANCE ? new Int32Array([value, x - 1]) : null;
		}
	}, {
		key: "patternMatchVariance",
		value: function drawWithColorToOpaque(h, m, w) {
			var len = h.length;
			/** @type {number} */
			var i = 0;
			/** @type {number} */
			var s = 0;
			/** @type {number} */
			var j = 0;
			for (; j < len; j++) {
				i = i + h[j];
				s = s + m[j];
			}
			if (i < s) {
				return 1 / 0;
			}
			/** @type {number} */
			var v = i / s;
			/** @type {number} */
			w = w * v;
			/** @type {number} */
			var integer = 0;
			/** @type {number} */
			var p = 0;
			for (; p < len; p++) {
				var a = h[p];
				/** @type {number} */
				var b = m[p] * v;
				/** @type {number} */
				var xInterval = a > b ? a - b : b - a;
				if (xInterval > w) {
					return 1 / 0;
				}
				/** @type {number} */
				integer = integer + xInterval;
			}
			return integer / i;
		}
	}]);
	return self;
}();
/** @type {!Int32Array} */
queue.INDEXES_START_PATTERN = Int32Array.from([0, 4, 1, 5]);
/** @type {!Int32Array} */
queue.INDEXES_STOP_PATTERN = Int32Array.from([6, 2, 7, 3]);
/** @type {number} */
queue.MAX_AVG_VARIANCE = .42;
/** @type {number} */
queue.MAX_INDIVIDUAL_VARIANCE = .8;
/** @type {!Int32Array} */
queue.START_PATTERN = Int32Array.from([8, 1, 1, 1, 1, 1, 1, 3]);
/** @type {!Int32Array} */
queue.STOP_PATTERN = Int32Array.from([7, 1, 1, 3, 1, 1, 1, 2, 1]);
/** @type {number} */
queue.MAX_PIXEL_DRIFT = 3;
/** @type {number} */
queue.MAX_PATTERN_DRIFT = 5;
/** @type {number} */
queue.SKIPPED_ROW_COUNT_MAX = 25;
/** @type {number} */
queue.ROW_STEP = 5;
/** @type {number} */
queue.BARCODE_MIN_HEIGHT = 10;
var Toolbar = function () {
	/**
	 * @param {string} parent
	 * @param {!Array} d
	 * @return {undefined}
	 */
	function Promise(parent, d) {
		_classCallCheck2(this, Promise);
		if (0 === d.length) {
			throw new Function;
		}
		/** @type {string} */
		this.field = parent;
		var end = d.length;
		if (end > 1 && 0 === d[0]) {
			/** @type {number} */
			var start = 1;
			for (; start < end && 0 === d[start];) {
				start++;
			}
			if (start === end) {
				/** @type {!Int32Array} */
				this.coefficients = new Int32Array([0]);
			} else {
				/** @type {!Int32Array} */
				this.coefficients = new Int32Array(end - start);
				System.arraycopy(d, start, this.coefficients, 0, this.coefficients.length);
			}
		} else {
			/** @type {!Array} */
			this.coefficients = d;
		}
	}
	_createClass2(Promise, [{
		key: "getCoefficients",
		value: function getCoefficients() {
			return this.coefficients;
		}
	}, {
		key: "getDegree",
		value: function _Class() {
			return this.coefficients.length - 1;
		}
	}, {
		key: "isZero",
		value: function Distortion() {
			return 0 === this.coefficients[0];
		}
	}, {
		key: "getCoefficient",
		value: function _Class(_arg) {
			return this.coefficients[this.coefficients.length - 1 - _arg];
		}
	}, {
		key: "evaluateAt",
		value: function invokeListeners(a) {
			if (0 === a) {
				return this.getCoefficient(0);
			}
			if (1 === a) {
				/** @type {number} */
				var i = 0;
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError54 = false;
				var _iteratorError17 = undefined;
				try {
					var _iterator3 = this.coefficients[Symbol.iterator]();
					var $__6;
					for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var item = $__6.value;
						i = this.field.add(i, item);
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError54 = true;
					_iteratorError17 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError54) {
							throw _iteratorError17;
						}
					}
				}
				return i;
			}
			var result2 = this.coefficients[0];
			var size = this.coefficients.length;
			/** @type {number} */
			var i = 1;
			for (; i < size; i++) {
				result2 = this.field.add(this.field.multiply(a, result2), this.coefficients[i]);
			}
			return result2;
		}
	}, {
		key: "add",
		value: function add(other) {
			if (!this.field.equals(other.field)) {
				throw new Function("ModulusPolys do not have same ModulusGF field");
			}
			if (this.isZero()) {
				return other;
			}
			if (other.isZero()) {
				return this;
			}
			var smallerCoefficients = this.coefficients;
			var largerCoefficients = other.coefficients;
			if (smallerCoefficients.length > largerCoefficients.length) {
				var temp = smallerCoefficients;
				smallerCoefficients = largerCoefficients;
				largerCoefficients = temp;
			}
			/** @type {!Int32Array} */
			var result = new Int32Array(largerCoefficients.length);
			/** @type {number} */
			var n = largerCoefficients.length - smallerCoefficients.length;
			System.arraycopy(largerCoefficients, 0, result, 0, n);
			/** @type {number} */
			var i = n;
			for (; i < largerCoefficients.length; i++) {
				result[i] = this.field.add(smallerCoefficients[i - n], largerCoefficients[i]);
			}
			return new Promise(this.field, result);
		}
	}, {
		key: "subtract",
		value: function update(p) {
			if (!this.field.equals(p.field)) {
				throw new Function("ModulusPolys do not have same ModulusGF field");
			}
			return p.isZero() ? this : this.add(p.negative());
		}
	}, {
		key: "multiply",
		value: function Circle(x) {
			return x instanceof Promise ? this.multiplyOther(x) : this.multiplyScalar(x);
		}
	}, {
		key: "multiplyOther",
		value: function update(other) {
			if (!this.field.equals(other.field)) {
				throw new Function("ModulusPolys do not have same ModulusGF field");
			}
			if (this.isZero() || other.isZero()) {
				return new Promise(this.field, new Int32Array([0]));
			}
			var aCoefficients = this.coefficients;
			var aLength = aCoefficients.length;
			var bCoefficients = other.coefficients;
			var bLength = bCoefficients.length;
			/** @type {!Int32Array} */
			var product = new Int32Array(aLength + bLength - 1);
			/** @type {number} */
			var i = 0;
			for (; i < aLength; i++) {
				var aCoeff = aCoefficients[i];
				/** @type {number} */
				var j = 0;
				for (; j < bLength; j++) {
					product[i + j] = this.field.add(product[i + j], this.field.multiply(aCoeff, bCoefficients[j]));
				}
			}
			return new Promise(this.field, product);
		}
	}, {
		key: "negative",
		value: function GF256Poly() {
			var size = this.coefficients.length;
			/** @type {!Int32Array} */
			var values = new Int32Array(size);
			/** @type {number} */
			var i = 0;
			for (; i < size; i++) {
				values[i] = this.field.subtract(0, this.coefficients[i]);
			}
			return new Promise(this.field, values);
		}
	}, {
		key: "multiplyScalar",
		value: function step(coefficient) {
			if (0 === coefficient) {
				return new Promise(this.field, new Int32Array([0]));
			}
			if (1 === coefficient) {
				return this;
			}
			var size = this.coefficients.length;
			/** @type {!Int32Array} */
			var product = new Int32Array(size);
			/** @type {number} */
			var i = 0;
			for (; i < size; i++) {
				product[i] = this.field.multiply(this.coefficients[i], coefficient);
			}
			return new Promise(this.field, product);
		}
	}, {
		key: "multiplyByMonomial",
		value: function init(degree, coefficient) {
			if (degree < 0) {
				throw new Function;
			}
			if (0 === coefficient) {
				return new Promise(this.field, new Int32Array([0]));
			}
			var size = this.coefficients.length;
			/** @type {!Int32Array} */
			var product = new Int32Array(size + degree);
			/** @type {number} */
			var i = 0;
			for (; i < size; i++) {
				product[i] = this.field.multiply(this.coefficients[i], coefficient);
			}
			return new Promise(this.field, product);
		}
	}, {
		key: "toString",
		value: function _unescapeHex() {
			var result = new Buffer;
			var degree = this.getDegree();
			for (; degree >= 0; degree--) {
				var coefficient = this.getCoefficient(degree);
				if (0 !== coefficient) {
					if (coefficient < 0) {
						result.append(" - ");
						/** @type {number} */
						coefficient = -coefficient;
					} else {
						if (result.length() > 0) {
							result.append(" + ");
						}
					}
					if (!(0 !== degree && 1 === coefficient)) {
						result.append(coefficient);
					}
					if (0 !== degree) {
						if (1 === degree) {
							result.append("x");
						} else {
							result.append("x^");
							result.append(degree);
						}
					}
				}
			}
			return result.toString();
		}
	}]);
	return Promise;
}();
var Ang = function () {
	/**
	 * @return {undefined}
	 */
	function TempusDominusBootstrap3() {
		_classCallCheck2(this, TempusDominusBootstrap3);
	}
	_createClass2(TempusDominusBootstrap3, [{
		key: "add",
		value: function verify(exercise, userMod) {
			return (exercise + userMod) % this.modulus;
		}
	}, {
		key: "subtract",
		value: function verify(t, time) {
			return (this.modulus + t - time) % this.modulus;
		}
	}, {
		key: "exp",
		value: function openTiledImage(kind) {
			return this.expTable[kind];
		}
	}, {
		key: "log",
		value: function test(a) {
			if (0 === a) {
				throw new Function;
			}
			return this.logTable[a];
		}
	}, {
		key: "inverse",
		value: function GF256(b) {
			if (0 === b) {
				throw new GF256Poly;
			}
			return this.expTable[this.modulus - this.logTable[b] - 1];
		}
	}, {
		key: "multiply",
		value: function verify(a, b) {
			return 0 === a || 0 === b ? 0 : this.expTable[(this.logTable[a] + this.logTable[b]) % (this.modulus - 1)];
		}
	}, {
		key: "getSize",
		value: function getSize() {
			return this.modulus;
		}
	}, {
		key: "equals",
		value: function prefetchGroupsInfo(canCreateDiscussions) {
			return canCreateDiscussions === this;
		}
	}]);
	return TempusDominusBootstrap3;
}();
var Color = function (_WebInspector$GeneralTreeElement) {
	/**
	 * @param {number} n
	 * @param {number} target
	 * @return {?}
	 */
	function EventHandler(n, target) {
		var self;
		_classCallCheck2(this, EventHandler);
		self = _possibleConstructorReturn(this, (EventHandler.__proto__ || Object.getPrototypeOf(EventHandler)).call(this));
		self;
		/** @type {number} */
		self.modulus = n;
		/** @type {!Int32Array} */
		self.expTable = new Int32Array(n);
		/** @type {!Int32Array} */
		self.logTable = new Int32Array(n);
		/** @type {number} */
		var a = 1;
		/** @type {number} */
		var i = 0;
		for (; i < n; i++) {
			/** @type {number} */
			self.expTable[i] = a;
			/** @type {number} */
			a = a * target % n;
		}
		/** @type {number} */
		var j = 0;
		for (; j < n - 1; j++) {
			/** @type {number} */
			self.logTable[self.expTable[j]] = j;
		}
		self.zero = new Toolbar(self, new Int32Array([0]));
		self.one = new Toolbar(self, new Int32Array([1]));
		return self;
	}
	_inherits(EventHandler, _WebInspector$GeneralTreeElement);
	_createClass2(EventHandler, [{
		key: "getZero",
		value: function getBDDFromFormula() {
			return this.zero;
		}
	}, {
		key: "getOne",
		value: function getBDDFromFormula() {
			return this.one;
		}
	}, {
		key: "buildMonomial",
		value: function init(size, name) {
			if (size < 0) {
				throw new Function;
			}
			if (0 === name) {
				return this.zero;
			}
			/** @type {!Int32Array} */
			var params = new Int32Array(size + 1);
			return params[0] = name, new Toolbar(this, params);
		}
	}]);
	return EventHandler;
}(Ang);
Color.PDF417_GF = new Color(that.NUMBER_OF_CODEWORDS, 3);
var EventEmitter = function () {
	/**
	 * @return {undefined}
	 */
	function TempusDominusBootstrap3() {
		_classCallCheck2(this, TempusDominusBootstrap3);
		this.field = Color.PDF417_GF;
	}
	_createClass2(TempusDominusBootstrap3, [{
		key: "decode",
		value: function init(received, numECCodewords, _args) {
			var poly = new Toolbar(this.field, received);
			/** @type {!Int32Array} */
			var S = new Int32Array(numECCodewords);
			/** @type {boolean} */
			var s = false;
			/** @type {number} */
			var i = numECCodewords;
			for (; i > 0; i--) {
				var t = poly.evaluateAt(this.field.exp(i));
				S[numECCodewords - i] = t;
				if (0 !== t) {
					/** @type {boolean} */
					s = true;
				}
			}
			if (!s) {
				return 0;
			}
			var result = this.field.getOne();
			if (null != _args) {
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError55 = false;
				var _iteratorError17 = undefined;
				try {
					var _iterator3 = _args[Symbol.iterator]();
					var $__6;
					for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var erasure = $__6.value;
						var scope = this.field.exp(received.length - 1 - erasure);
						var term = new Toolbar(this.field, new Int32Array([this.field.subtract(0, scope), 1]));
						result = result.multiply(term);
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError55 = true;
					_iteratorError17 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError55) {
							throw _iteratorError17;
						}
					}
				}
			}
			var syndrome = new Toolbar(this.field, S);
			var sigmaOmega = this.runEuclideanAlgorithm(this.field.buildMonomial(numECCodewords, 1), syndrome, numECCodewords);
			var sigma = sigmaOmega[0];
			var omega = sigmaOmega[1];
			var errorLocations = this.findErrorLocations(sigma);
			var errorMagnitudes = this.findErrorMagnitudes(omega, sigma, errorLocations);
			/** @type {number} */
			var j = 0;
			for (; j < errorLocations.length; j++) {
				/** @type {number} */
				var i = received.length - 1 - this.field.log(errorLocations[j]);
				if (i < 0) {
					throw Rectangle.getChecksumInstance();
				}
				received[i] = this.field.subtract(received[i], errorMagnitudes[j]);
			}
			return errorLocations.length;
		}
	}, {
		key: "runEuclideanAlgorithm",
		value: function update(a, b, status) {
			if (a.getDegree() < b.getDegree()) {
				/** @type {number} */
				var bytes = a;
				/** @type {number} */
				a = b;
				b = bytes;
			}
			/** @type {number} */
			var rLast = a;
			/** @type {number} */
			var r = b;
			var tLast = this.field.getZero();
			var t = this.field.getOne();
			for (; r.getDegree() >= Math.round(status / 2);) {
				var rLastLast = rLast;
				var tLastLast = tLast;
				if (tLast = t, (rLast = r).isZero()) {
					throw Rectangle.getChecksumInstance();
				}
				r = rLastLast;
				var q = this.field.getZero();
				var denominatorLeadingTerm = rLast.getCoefficient(rLast.getDegree());
				var inverseDenominatorLeadingTerm = this.field.inverse(denominatorLeadingTerm);
				for (; r.getDegree() >= rLast.getDegree() && !r.isZero();) {
					/** @type {number} */
					var degreeDiff = r.getDegree() - rLast.getDegree();
					var scale = this.field.multiply(r.getCoefficient(r.getDegree()), inverseDenominatorLeadingTerm);
					q = q.add(this.field.buildMonomial(degreeDiff, scale));
					r = r.subtract(rLast.multiplyByMonomial(degreeDiff, scale));
				}
				t = q.multiply(tLast).subtract(tLastLast).negative();
			}
			var i = t.getCoefficient(0);
			if (0 === i) {
				throw Rectangle.getChecksumInstance();
			}
			var p = this.field.inverse(i);
			return [t.multiply(p), r.multiply(p)];
		}
	}, {
		key: "findErrorLocations",
		value: function render(errorLocator) {
			var n = errorLocator.getDegree();
			/** @type {!Int32Array} */
			var result = new Int32Array(n);
			/** @type {number} */
			var j = 0;
			/** @type {number} */
			var i = 1;
			for (; i < this.field.getSize() && j < n; i++) {
				if (0 === errorLocator.evaluateAt(i)) {
					result[j] = this.field.inverse(i);
					j++;
				}
			}
			if (j !== n) {
				throw Rectangle.getChecksumInstance();
			}
			return result;
		}
	}, {
		key: "findErrorMagnitudes",
		value: function render(errorEvaluator, errorLocator, errorLocations) {
			var errorLocatorDegree = errorLocator.getDegree();
			/** @type {!Int32Array} */
			var formalDerivativeCoefficients = new Int32Array(errorLocatorDegree);
			/** @type {number} */
			var i = 1;
			for (; i <= errorLocatorDegree; i++) {
				formalDerivativeCoefficients[errorLocatorDegree - i] = this.field.multiply(i, errorLocator.getCoefficient(i));
			}
			var formalDerivative = new Toolbar(this.field, formalDerivativeCoefficients);
			var s = errorLocations.length;
			/** @type {!Int32Array} */
			var result = new Int32Array(s);
			/** @type {number} */
			var j = 0;
			for (; j < s; j++) {
				var xiInverse = this.field.inverse(errorLocations[j]);
				var value = this.field.subtract(0, errorEvaluator.evaluateAt(xiInverse));
				var i = this.field.inverse(formalDerivative.evaluateAt(xiInverse));
				result[j] = this.field.multiply(value, i);
			}
			return result;
		}
	}]);
	return TempusDominusBootstrap3;
}();
var Polygon = function () {
	/**
	 * @param {!AudioNode} img
	 * @param {?} callback
	 * @param {?} name
	 * @param {?} preload
	 * @param {?} onload
	 * @return {undefined}
	 */
	function Bitmap(img, callback, name, preload, onload) {
		_classCallCheck2(this, Bitmap);
		if (img instanceof Bitmap) {
			this.constructor_2(img);
		} else {
			this.constructor_1(img, callback, name, preload, onload);
		}
	}
	_createClass2(Bitmap, [{
		key: "constructor_1",
		value: function render(value, p, item, object, entity) {
			/** @type {boolean} */
			var reverseIsSingle = null == p || null == item;
			/** @type {boolean} */
			var reverseValue = null == object || null == entity;
			if (reverseIsSingle && reverseValue) {
				throw new TypeError;
			}
			if (reverseIsSingle) {
				p = new type(0, object.getY());
				item = new type(0, entity.getY());
			} else {
				if (reverseValue) {
					object = new type(value.getWidth() - 1, p.getY());
					entity = new type(value.getWidth() - 1, item.getY());
				}
			}
			/** @type {!Object} */
			this.image = value;
			/** @type {!Object} */
			this.topLeft = p;
			/** @type {!Object} */
			this.bottomLeft = item;
			/** @type {!Object} */
			this.topRight = object;
			/** @type {!Object} */
			this.bottomRight = entity;
			/** @type {number} */
			this.minX = Math.trunc(Math.min(p.getX(), item.getX()));
			/** @type {number} */
			this.maxX = Math.trunc(Math.max(object.getX(), entity.getX()));
			/** @type {number} */
			this.minY = Math.trunc(Math.min(p.getY(), object.getY()));
			/** @type {number} */
			this.maxY = Math.trunc(Math.max(item.getY(), entity.getY()));
		}
	}, {
		key: "constructor_2",
		value: function NonZeroSumValueNodeDrawer(rect) {
			this.image = rect.image;
			this.topLeft = rect.getTopLeft();
			this.bottomLeft = rect.getBottomLeft();
			this.topRight = rect.getTopRight();
			this.bottomRight = rect.getBottomRight();
			this.minX = rect.getMinX();
			this.maxX = rect.getMaxX();
			this.minY = rect.getMinY();
			this.maxY = rect.getMaxY();
		}
	}, {
		key: "addMissingRows",
		value: function render(now, dt, hero) {
			var layerHeight = this.topLeft;
			var callback = this.bottomLeft;
			var longestAnim = this.topRight;
			var animation = this.bottomRight;
			if (now > 0) {
				var StatePosition = hero ? this.topLeft : this.topRight;
				/** @type {number} */
				var args = Math.trunc(StatePosition.getY() - now);
				if (args < 0) {
					/** @type {number} */
					args = 0;
				}
				var anim = new type(StatePosition.getX(), args);
				if (hero) {
					layerHeight = anim;
				} else {
					longestAnim = anim;
				}
			}
			if (dt > 0) {
				var StatePosition = hero ? this.bottomLeft : this.bottomRight;
				/** @type {number} */
				var args = Math.trunc(StatePosition.getY() + dt);
				if (args >= this.image.getHeight()) {
					/** @type {number} */
					args = this.image.getHeight() - 1;
				}
				var anim = new type(StatePosition.getX(), args);
				if (hero) {
					callback = anim;
				} else {
					animation = anim;
				}
			}
			return new Bitmap(this.image, layerHeight, callback, longestAnim, animation);
		}
	}, {
		key: "getMinX",
		value: function getMinX() {
			return this.minX;
		}
	}, {
		key: "getMaxX",
		value: function rightBoxOffset() {
			return this.maxX;
		}
	}, {
		key: "getMinY",
		value: function getMinY() {
			return this.minY;
		}
	}, {
		key: "getMaxY",
		value: function getMaxY() {
			return this.maxY;
		}
	}, {
		key: "getTopLeft",
		value: function getTopLeft() {
			return this.topLeft;
		}
	}, {
		key: "getTopRight",
		value: function getTopRight() {
			return this.topRight;
		}
	}, {
		key: "getBottomLeft",
		value: function getBottomLeft() {
			return this.bottomLeft;
		}
	}, {
		key: "getBottomRight",
		value: function getBottomRight() {
			return this.bottomRight;
		}
	}], [{
		key: "merge",
		value: function Frame(data, model) {
			return null == data ? model : null == model ? data : new Bitmap(data.image, data.topLeft, data.bottomLeft, model.topRight, model.bottomRight);
		}
	}]);
	return Bitmap;
}();
var WidgetsInTemplateMixin = function () {
	/**
	 * @param {?} columnCount
	 * @param {(Object|number)} options
	 * @param {!Object} option
	 * @param {?} element
	 * @return {undefined}
	 */
	function TempusDominusBootstrap3(columnCount, options, option, element) {
		_classCallCheck2(this, TempusDominusBootstrap3);
		this.columnCount = columnCount;
		this.errorCorrectionLevel = element;
		/** @type {(Object|number)} */
		this.rowCountUpperPart = options;
		/** @type {!Object} */
		this.rowCountLowerPart = option;
		this.rowCount = options + option;
	}
	_createClass2(TempusDominusBootstrap3, [{
		key: "getColumnCount",
		value: function getColumnCount() {
			return this.columnCount;
		}
	}, {
		key: "getErrorCorrectionLevel",
		value: function getErrorCorrectionLevel() {
			return this.errorCorrectionLevel;
		}
	}, {
		key: "getRowCount",
		value: function processResponse() {
			return this.rowCount;
		}
	}, {
		key: "getRowCountUpperPart",
		value: function getRowCountUpperPart() {
			return this.rowCountUpperPart;
		}
	}, {
		key: "getRowCountLowerPart",
		value: function getRowCountLowerPart() {
			return this.rowCountLowerPart;
		}
	}]);
	return TempusDominusBootstrap3;
}();
var Deserializer = function () {
	/**
	 * @return {undefined}
	 */
	function Request() {
		_classCallCheck2(this, Request);
		/** @type {string} */
		this.buffer = "";
	}
	_createClass2(Request, [{
		key: "format",
		value: function send(data) {
			/** @type {number} */
			var length = arguments.length;
			/** @type {!Array} */
			var args = Array(length > 1 ? length - 1 : 0);
			/** @type {number} */
			var i = 1;
			for (; i < length; i++) {
				args[i - 1] = arguments[i];
			}
			this.buffer += Request.form(data, args);
		}
	}, {
		key: "toString",
		value: function unserializeArrayBuffer() {
			return this.buffer;
		}
	}], [{
		key: "form",
		value: function format(err, values) {
			/** @type {number} */
			var j = -1;
			return err.replace(/%(-)?(0?[0-9]+)?([.][0-9]+)?([#][0-9]+)?([scfpexd%])/g, function (precision, undefined, dir, s, widthStr, type) {
				if ("%%" === precision) {
					return "%";
				}
				if (void 0 === values[++j]) {
					return;
				}
				/** @type {(number|undefined)} */
				precision = s ? parseInt(s.substr(1)) : void 0;
				var val = void 0;
				/** @type {(number|undefined)} */
				var radix = widthStr ? parseInt(widthStr.substr(1)) : void 0;
				switch (type) {
					case "s":
						val = values[j];
						break;
					case "c":
						val = values[j][0];
						break;
					case "f":
						/** @type {string} */
						val = parseFloat(values[j]).toFixed(precision);
						break;
					case "p":
						/** @type {string} */
						val = parseFloat(values[j]).toPrecision(precision);
						break;
					case "e":
						/** @type {string} */
						val = parseFloat(values[j]).toExponential(precision);
						break;
					case "x":
						/** @type {string} */
						val = parseInt(values[j]).toString(radix || 16);
						break;
					case "d":
						/** @type {string} */
						val = parseFloat(parseInt(values[j], radix || 10).toPrecision(precision)).toFixed(0);
				}
				/** @type {string} */
				val = "object" == (typeof val === "undefined" ? "undefined" : _typeof2(val)) ? JSON.stringify(val) : (+val).toString(radix);
				/** @type {number} */
				var n = parseInt(dir);
				/** @type {string} */
				var i = dir && dir[0] + "" == "0" ? "0" : " ";
				for (; val.length < n;) {
					/** @type {string} */
					val = void 0 !== undefined ? val + i : i + val;
				}
				return val;
			});
		}
	}]);
	return Request;
}();
var Tag = function () {
	/**
	 * @param {?} polygon
	 * @return {undefined}
	 */
	function exports(polygon) {
		_classCallCheck2(this, exports);
		this.boundingBox = new Polygon(polygon);
		/** @type {!Array} */
		this.codewords = new Array(polygon.getMaxY() - polygon.getMinY() + 1);
	}
	_createClass2(exports, [{
		key: "getCodewordNearby",
		value: function getCodewordNearby(imageRow) {
			var codeword = this.getCodeword(imageRow);
			if (null != codeword) {
				return codeword;
			}
			/** @type {number} */
			var i = 1;
			for (; i < exports.MAX_NEARBY_DISTANCE; i++) {
				/** @type {number} */
				var nearImageRow = this.imageRowToCodewordIndex(imageRow) - i;
				if (nearImageRow >= 0 && null != (codeword = this.codewords[nearImageRow])) {
					return codeword;
				}
				if ((nearImageRow = this.imageRowToCodewordIndex(imageRow) + i) < this.codewords.length && null != (codeword = this.codewords[nearImageRow])) {
					return codeword;
				}
			}
			return null;
		}
	}, {
		key: "imageRowToCodewordIndex",
		value: function _renderUI(parentNode) {
			return parentNode - this.boundingBox.getMinY();
		}
	}, {
		key: "setCodeword",
		value: function setCodeword(imageRow, codeword) {
			this.codewords[this.imageRowToCodewordIndex(imageRow)] = codeword;
		}
	}, {
		key: "getCodeword",
		value: function getCodeword(imageRow) {
			return this.codewords[this.imageRowToCodewordIndex(imageRow)];
		}
	}, {
		key: "getBoundingBox",
		value: function getBoundingBox() {
			return this.boundingBox;
		}
	}, {
		key: "getCodewords",
		value: function getCodewords() {
			return this.codewords;
		}
	}, {
		key: "toString",
		value: function value() {
			var util = new Deserializer;
			/** @type {number} */
			var count = 0;
			/** @type {boolean} */
			var _iteratorNormalCompletion3 = true;
			/** @type {boolean} */
			var _didIteratorError56 = false;
			var _iteratorError17 = undefined;
			try {
				var _iterator3 = this.codewords[Symbol.iterator]();
				var $__6;
				for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var item = $__6.value;
					if (null != item) {
						util.format("%3d: %3d|%3d%n", count++, item.getRowNumber(), item.getValue());
					} else {
						util.format("%3d:    |   %n", count++);
					}
				}
			} catch (err) {
				/** @type {boolean} */
				_didIteratorError56 = true;
				_iteratorError17 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError56) {
						throw _iteratorError17;
					}
				}
			}
			return util.toString();
		}
	}]);
	return exports;
}();
/** @type {number} */
Tag.MAX_NEARBY_DISTANCE = 5;
var Point = function () {
	/**
	 * @return {undefined}
	 */
	function TempusDominusBootstrap3() {
		_classCallCheck2(this, TempusDominusBootstrap3);
		/** @type {!Map} */
		this.values = new Map;
	}
	_createClass2(TempusDominusBootstrap3, [{
		key: "setValue",
		value: function setValue(val) {
			/** @type {number} */
			val = Math.trunc(val);
			var value = this.values.get(val);
			if (null == value) {
				/** @type {number} */
				value = 0;
			}
			value++;
			this.values.set(val, value);
		}
	}, {
		key: "getValue",
		value: function render() {
			/** @type {number} */
			var t = -1;
			/** @type {!Array} */
			var a = new Array;
			/** @type {boolean} */
			var _iteratorNormalCompletion3 = true;
			/** @type {boolean} */
			var _didIteratorError57 = false;
			var _iteratorError17 = undefined;
			try {
				/**
				 * @return {undefined}
				 */
				var pre_function = function _loop() {
					var _qualifiedName$split62 = _slicedToArray(_step17.value, 2);
					var wasSet = _qualifiedName$split62[0];
					var findings = _qualifiedName$split62[1];
					var self = {
						getKey: function set() {
							return wasSet;
						},
						getValue: function set() {
							return findings;
						}
					};
					if (self.getValue() > t) {
						t = self.getValue();
						(a = []).push(self.getKey());
					} else {
						if (self.getValue() === t) {
							a.push(self.getKey());
						}
					}
				};
				var _iterator3 = this.values.entries()[Symbol.iterator]();
				var _step17;
				for (; !(_iteratorNormalCompletion3 = (_step17 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					pre_function();
				}
			} catch (err) {
				/** @type {boolean} */
				_didIteratorError57 = true;
				_iteratorError17 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError57) {
						throw _iteratorError17;
					}
				}
			}
			return that.toIntArray(a);
		}
	}, {
		key: "getConfidence",
		value: function toggleGroupNameEdit(group) {
			return this.values.get(group);
		}
	}]);
	return TempusDominusBootstrap3;
}();
var Validator = function (_PlanOutOpUnary3) {
	/**
	 * @param {?} options
	 * @param {string} key
	 * @return {?}
	 */
	function Requester(options, key) {
		var _this;
		_classCallCheck2(this, Requester);
		_this = _possibleConstructorReturn(this, (Requester.__proto__ || Object.getPrototypeOf(Requester)).call(this, options));
		_this;
		/** @type {string} */
		_this._isLeft = key;
		return _this;
	}
	_inherits(Requester, _PlanOutOpUnary3);
	_createClass2(Requester, [{
		key: "setRowNumbers",
		value: function _destroyWaypoints() {
			/** @type {boolean} */
			var _iteratorNormalCompletion3 = true;
			/** @type {boolean} */
			var _didIteratorError58 = false;
			var _iteratorError17 = undefined;
			try {
				var _iterator3 = this.getCodewords()[Symbol.iterator]();
				var $__6;
				for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var item = $__6.value;
					if (null != item) {
						item.setRowNumberAsRowIndicatorColumn();
					}
				}
			} catch (err) {
				/** @type {boolean} */
				_didIteratorError58 = true;
				_iteratorError17 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError58) {
						throw _iteratorError17;
					}
				}
			}
		}
	}, {
		key: "adjustCompleteIndicatorColumnRowNumbers",
		value: function update(table) {
			var codewords = this.getCodewords();
			this.setRowNumbers();
			this.removeIncorrectCodewords(codewords, table);
			var bounds = this.getBoundingBox();
			var StatePosition = this._isLeft ? bounds.getTopLeft() : bounds.getTopRight();
			var $Wireworld_StatePosition = this._isLeft ? bounds.getBottomLeft() : bounds.getBottomRight();
			var firstRow = this.imageRowToCodewordIndex(Math.trunc(StatePosition.getY()));
			var lastRow = this.imageRowToCodewordIndex(Math.trunc($Wireworld_StatePosition.getY()));
			/** @type {number} */
			var duedate = -1;
			/** @type {number} */
			var maxOffset = 1;
			/** @type {number} */
			var off = 0;
			var codewordRow = firstRow;
			for (; codewordRow < lastRow; codewordRow++) {
				if (null == codewords[codewordRow]) {
					continue;
				}
				var codeword = codewords[codewordRow];
				/** @type {number} */
				var timeSubmittedDiff = codeword.getRowNumber() - duedate;
				if (0 === timeSubmittedDiff) {
					off++;
				} else {
					if (1 === timeSubmittedDiff) {
						/** @type {number} */
						maxOffset = Math.max(maxOffset, off);
						/** @type {number} */
						off = 1;
						duedate = codeword.getRowNumber();
					} else {
						if (timeSubmittedDiff < 0 || codeword.getRowNumber() >= table.getRowCount() || timeSubmittedDiff > codewordRow) {
							/** @type {null} */
							codewords[codewordRow] = null;
						} else {
							var checkedRows = void 0;
							/** @type {boolean} */
							var closePreviousCodewordFound = (checkedRows = maxOffset > 2 ? (maxOffset - 2) * timeSubmittedDiff : timeSubmittedDiff) >= codewordRow;
							/** @type {number} */
							var i = 1;
							for (; i <= checkedRows && !closePreviousCodewordFound; i++) {
								/** @type {boolean} */
								closePreviousCodewordFound = null != codewords[codewordRow - i];
							}
							if (closePreviousCodewordFound) {
								/** @type {null} */
								codewords[codewordRow] = null;
							} else {
								duedate = codeword.getRowNumber();
								/** @type {number} */
								off = 1;
							}
						}
					}
				}
			}
		}
	}, {
		key: "getRowHeights",
		value: function select() {
			var barcodeMetadata = this.getBarcodeMetadata();
			if (null == barcodeMetadata) {
				return null;
			}
			this.adjustIncompleteIndicatorColumnRowNumbers(barcodeMetadata);
			/** @type {!Int32Array} */
			var result = new Int32Array(barcodeMetadata.getRowCount());
			/** @type {boolean} */
			var _iteratorNormalCompletion3 = true;
			/** @type {boolean} */
			var _didIteratorError59 = false;
			var _iteratorError17 = undefined;
			try {
				var _iterator3 = this.getCodewords()[Symbol.iterator]();
				var item;
				for (; !(_iteratorNormalCompletion3 = (item = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var ds = item.value;
					if (null != ds) {
						var j = ds.getRowNumber();
						if (j >= result.length) {
							continue;
						}
						result[j]++;
					}
				}
			} catch (err) {
				/** @type {boolean} */
				_didIteratorError59 = true;
				_iteratorError17 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError59) {
						throw _iteratorError17;
					}
				}
			}
			return result;
		}
	}, {
		key: "adjustIncompleteIndicatorColumnRowNumbers",
		value: function updateLevel(haveDrawn) {
			var bounds = this.getBoundingBox();
			var StatePosition = this._isLeft ? bounds.getTopLeft() : bounds.getTopRight();
			var $Wireworld_StatePosition = this._isLeft ? bounds.getBottomLeft() : bounds.getBottomRight();
			var firstRow = this.imageRowToCodewordIndex(Math.trunc(StatePosition.getY()));
			var lastRow = this.imageRowToCodewordIndex(Math.trunc($Wireworld_StatePosition.getY()));
			var options = this.getCodewords();
			/** @type {number} */
			var duedate = -1;
			var i = firstRow;
			for (; i < lastRow; i++) {
				if (null == options[i]) {
					continue;
				}
				var cw = options[i];
				cw.setRowNumberAsRowIndicatorColumn();
				/** @type {number} */
				var timeSubmittedDiff = cw.getRowNumber() - duedate;
				if (!(0 === timeSubmittedDiff)) {
					if (1 === timeSubmittedDiff) {
						duedate = cw.getRowNumber();
					} else {
						if (cw.getRowNumber() >= haveDrawn.getRowCount()) {
							/** @type {null} */
							options[i] = null;
						} else {
							duedate = cw.getRowNumber();
						}
					}
				}
			}
		}
	}, {
		key: "getBarcodeMetadata",
		value: function render() {
			var object = this.getCodewords();
			var barcodeColumnCount = new Point;
			var ecPropertyDesc = new Point;
			var barcodeRowCountLowerPart = new Point;
			var barcodeECLevel = new Point;
			/** @type {boolean} */
			var _iteratorNormalCompletion3 = true;
			/** @type {boolean} */
			var _didIteratorError60 = false;
			var _iteratorError17 = undefined;
			try {
				var _iterator3 = object[Symbol.iterator]();
				var $__6;
				for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var item = $__6.value;
					if (null == item) {
						continue;
					}
					item.setRowNumberAsRowIndicatorColumn();
					/** @type {number} */
					var rowIndicatorValue = item.getValue() % 30;
					var _o39 = item.getRowNumber();
					switch (this._isLeft || (_o39 = _o39 + 2), _o39 % 3) {
						case 0:
							ecPropertyDesc.setValue(3 * rowIndicatorValue + 1);
							break;
						case 1:
							barcodeECLevel.setValue(rowIndicatorValue / 3);
							barcodeRowCountLowerPart.setValue(rowIndicatorValue % 3);
							break;
						case 2:
							barcodeColumnCount.setValue(rowIndicatorValue + 1);
					}
				}
			} catch (err) {
				/** @type {boolean} */
				_didIteratorError60 = true;
				_iteratorError17 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError60) {
						throw _iteratorError17;
					}
				}
			}
			if (0 === barcodeColumnCount.getValue().length || 0 === ecPropertyDesc.getValue().length || 0 === barcodeRowCountLowerPart.getValue().length || 0 === barcodeECLevel.getValue().length || barcodeColumnCount.getValue()[0] < 1 || ecPropertyDesc.getValue()[0] + barcodeRowCountLowerPart.getValue()[0] < that.MIN_ROWS_IN_BARCODE || ecPropertyDesc.getValue()[0] + barcodeRowCountLowerPart.getValue()[0] > that.MAX_ROWS_IN_BARCODE) {
				return null;
			}
			var result = new WidgetsInTemplateMixin(barcodeColumnCount.getValue()[0], ecPropertyDesc.getValue()[0], barcodeRowCountLowerPart.getValue()[0], barcodeECLevel.getValue()[0]);
			return this.removeIncorrectCodewords(object, result), result;
		}
	}, {
		key: "removeIncorrectCodewords",
		value: function flattenTable(data, rows) {
			/** @type {number} */
			var i = 0;
			for (; i < data.length; i++) {
				var ds = data[i];
				if (null == data[i]) {
					continue;
				}
				/** @type {number} */
				var time = ds.getValue() % 30;
				var _s49 = ds.getRowNumber();
				if (_s49 > rows.getRowCount()) {
					/** @type {null} */
					data[i] = null;
				} else {
					switch (this._isLeft || (_s49 = _s49 + 2), _s49 % 3) {
						case 0:
							if (3 * time + 1 !== rows.getRowCountUpperPart()) {
								/** @type {null} */
								data[i] = null;
							}
							break;
						case 1:
							if (!(Math.trunc(time / 3) === rows.getErrorCorrectionLevel() && time % 3 === rows.getRowCountLowerPart())) {
								/** @type {null} */
								data[i] = null;
							}
							break;
						case 2:
							if (time + 1 !== rows.getColumnCount()) {
								/** @type {null} */
								data[i] = null;
							}
					}
				}
			}
		}
	}, {
		key: "isLeft",
		value: function isLeft() {
			return this._isLeft;
		}
	}, {
		key: "toString",
		value: function toString() {
			return "IsLeft: " + this._isLeft + "\n" + _get(Requester.prototype.__proto__ || Object.getPrototypeOf(Requester.prototype), "toString", this).call(this);
		}
	}]);
	return Requester;
}(Tag);
var runtimeConstructor = function () {
	/**
	 * @param {number} cursor
	 * @param {!Rect} timeout
	 * @return {undefined}
	 */
	function render(cursor, timeout) {
		_classCallCheck2(this, render);
		/** @type {number} */
		this.ADJUST_ROW_NUMBER_SKIP = 2;
		/** @type {number} */
		this.barcodeMetadata = cursor;
		this.barcodeColumnCount = cursor.getColumnCount();
		/** @type {!Rect} */
		this.boundingBox = timeout;
		/** @type {!Array} */
		this.detectionResultColumns = new Array(this.barcodeColumnCount + 2);
	}
	_createClass2(render, [{
		key: "getDetectionResultColumns",
		value: function getDetectionResultColumns() {
			this.adjustIndicatorColumnRowNumbers(this.detectionResultColumns[0]);
			this.adjustIndicatorColumnRowNumbers(this.detectionResultColumns[this.barcodeColumnCount + 1]);
			var previousUnadjustedCount = void 0;
			var unadjustedCodewordCount = that.MAX_CODEWORDS_IN_BARCODE;
			do {
				previousUnadjustedCount = unadjustedCodewordCount;
				unadjustedCodewordCount = this.adjustRowNumbersAndGetCount();
			} while (unadjustedCodewordCount > 0 && unadjustedCodewordCount < previousUnadjustedCount);
			return this.detectionResultColumns;
		}
	}, {
		key: "adjustIndicatorColumnRowNumbers",
		value: function prefetchGroupsInfo(canCreateDiscussions) {
			if (null != canCreateDiscussions) {
				canCreateDiscussions.adjustCompleteIndicatorColumnRowNumbers(this.barcodeMetadata);
			}
		}
	}, {
		key: "adjustRowNumbersAndGetCount",
		value: function tryNextPath() {
			var 0 = this.adjustRowNumbersByRow();
			if (0 === 0) {
				return 0;
			}
			/** @type {number} */
			var barcodeColumn = 1;
			for (; barcodeColumn < this.barcodeColumnCount + 1; barcodeColumn++) {
				var codewords = this.detectionResultColumns[barcodeColumn].getCodewords();
				/** @type {number} */
				var codewordsRow = 0;
				for (; codewordsRow < codewords.length; codewordsRow++) {
					if (null != codewords[codewordsRow]) {
						if (!codewords[codewordsRow].hasValidRowNumber()) {
							this.adjustRowNumbers(barcodeColumn, codewordsRow, codewords);
						}
					}
				}
			}
			return 0;
		}
	}, {
		key: "adjustRowNumbersByRow",
		value: function adjustRowNumbersByRow() {
			return this.adjustRowNumbersFromBothRI(), this.adjustRowNumbersFromLRI() + this.adjustRowNumbersFromRRI();
		}
	}, {
		key: "adjustRowNumbersFromBothRI",
		value: function writeTextArgs() {
			if (null == this.detectionResultColumns[0] || null == this.detectionResultColumns[this.barcodeColumnCount + 1]) {
				return;
			}
			var crossfilterable_layers = this.detectionResultColumns[0].getCodewords();
			var e = this.detectionResultColumns[this.barcodeColumnCount + 1].getCodewords();
			/** @type {number} */
			var layer_i = 0;
			for (; layer_i < crossfilterable_layers.length; layer_i++) {
				if (null != crossfilterable_layers[layer_i] && null != e[layer_i] && crossfilterable_layers[layer_i].getRowNumber() === e[layer_i].getRowNumber()) {
					/** @type {number} */
					var indexLookupKey = 1;
					for (; indexLookupKey <= this.barcodeColumnCount; indexLookupKey++) {
						var layer = this.detectionResultColumns[indexLookupKey].getCodewords()[layer_i];
						if (null != layer) {
							layer.setRowNumber(crossfilterable_layers[layer_i].getRowNumber());
							if (!layer.hasValidRowNumber()) {
								/** @type {null} */
								this.detectionResultColumns[indexLookupKey].getCodewords()[layer_i] = null;
							}
						}
					}
				}
			}
		}
	}, {
		key: "adjustRowNumbersFromRRI",
		value: function tryNextPath() {
			if (null == this.detectionResultColumns[this.barcodeColumnCount + 1]) {
				return 0;
			}
			/** @type {number} */
			var 0 = 0;
			var keywordResults = this.detectionResultColumns[this.barcodeColumnCount + 1].getCodewords();
			/** @type {number} */
			var i = 0;
			for (; i < keywordResults.length; i++) {
				if (null == keywordResults[i]) {
					continue;
				}
				var rowIndicatorRowNumber = keywordResults[i].getRowNumber();
				/** @type {number} */
				var invalidRowCounts = 0;
				var barcodeColumn = this.barcodeColumnCount + 1;
				for (; barcodeColumn > 0 && invalidRowCounts < this.ADJUST_ROW_NUMBER_SKIP; barcodeColumn--) {
					var uv = this.detectionResultColumns[barcodeColumn].getCodewords()[i];
					if (null != uv) {
						invalidRowCounts = render.adjustRowNumberIfValid(rowIndicatorRowNumber, invalidRowCounts, uv);
						if (!uv.hasValidRowNumber()) {
							0++;
						}
					}
				}
			}
			return 0;
		}
	}, {
		key: "adjustRowNumbersFromLRI",
		value: function tryNextPath() {
			if (null == this.detectionResultColumns[0]) {
				return 0;
			}
			/** @type {number} */
			var 0 = 0;
			var LRIcodewords = this.detectionResultColumns[0].getCodewords();
			/** @type {number} */
			var codewordsRow = 0;
			for (; codewordsRow < LRIcodewords.length; codewordsRow++) {
				if (null == LRIcodewords[codewordsRow]) {
					continue;
				}
				var rowIndicatorRowNumber = LRIcodewords[codewordsRow].getRowNumber();
				/** @type {number} */
				var invalidRowCounts = 0;
				/** @type {number} */
				var barcodeColumn = 1;
				for (; barcodeColumn < this.barcodeColumnCount + 1 && invalidRowCounts < this.ADJUST_ROW_NUMBER_SKIP; barcodeColumn++) {
					var codeword = this.detectionResultColumns[barcodeColumn].getCodewords()[codewordsRow];
					if (null != codeword) {
						invalidRowCounts = render.adjustRowNumberIfValid(rowIndicatorRowNumber, invalidRowCounts, codeword);
						if (!codeword.hasValidRowNumber()) {
							0++;
						}
					}
				}
			}
			return 0;
		}
	}, {
		key: "adjustRowNumbers",
		value: function setExtendAttribute(current_notebook, k, e) {
			var color = e[k];
			var a = this.detectionResultColumns[current_notebook - 1].getCodewords();
			var r = a;
			if (null != this.detectionResultColumns[current_notebook + 1]) {
				r = this.detectionResultColumns[current_notebook + 1].getCodewords();
			}
			/** @type {!Array} */
			var result = new Array(14);
			result[2] = a[k];
			result[3] = r[k];
			if (k > 0) {
				result[0] = e[k - 1];
				result[4] = a[k - 1];
				result[5] = r[k - 1];
			}
			if (k > 1) {
				result[8] = e[k - 2];
				result[10] = a[k - 2];
				result[11] = r[k - 2];
			}
			if (k < e.length - 1) {
				result[1] = e[k + 1];
				result[6] = a[k + 1];
				result[7] = r[k + 1];
			}
			if (k < e.length - 2) {
				result[9] = e[k + 2];
				result[12] = a[k + 2];
				result[13] = r[k + 2];
			}
			/** @type {boolean} */
			var _iteratorNormalCompletion3 = true;
			/** @type {boolean} */
			var _didIteratorError61 = false;
			var _iteratorError17 = undefined;
			try {
				var _iterator3 = result[Symbol.iterator]();
				var $__6;
				for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var item = $__6.value;
					if (render.adjustRowNumber(color, item)) {
						return;
					}
				}
			} catch (err) {
				/** @type {boolean} */
				_didIteratorError61 = true;
				_iteratorError17 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError61) {
						throw _iteratorError17;
					}
				}
			}
		}
	}, {
		key: "getBarcodeColumnCount",
		value: function getBarcodeColumnCount() {
			return this.barcodeColumnCount;
		}
	}, {
		key: "getBarcodeRowCount",
		value: function getBarcodeRowCount() {
			return this.barcodeMetadata.getRowCount();
		}
	}, {
		key: "getBarcodeECLevel",
		value: function getBarcodeECLevel() {
			return this.barcodeMetadata.getErrorCorrectionLevel();
		}
	}, {
		key: "setBoundingBox",
		value: function setPosition(boundingBox) {
			/** @type {!Rect} */
			this.boundingBox = boundingBox;
		}
	}, {
		key: "getBoundingBox",
		value: function getBoundingBox() {
			return this.boundingBox;
		}
	}, {
		key: "setDetectionResultColumn",
		value: function handleKeyboard(hashId, keyCode) {
			this.detectionResultColumns[hashId] = keyCode;
		}
	}, {
		key: "getDetectionResultColumn",
		value: function hasBall(ballNumber) {
			return this.detectionResultColumns[ballNumber];
		}
	}, {
		key: "toString",
		value: function value() {
			var dataBlock = this.detectionResultColumns[0];
			if (null == dataBlock) {
				dataBlock = this.detectionResultColumns[this.barcodeColumnCount + 1];
			}
			var stream = new Deserializer;
			/** @type {number} */
			var i = 0;
			for (; i < dataBlock.getCodewords().length; i++) {
				stream.format("CW %3d:", i);
				/** @type {number} */
				var indexLookupKey = 0;
				for (; indexLookupKey < this.barcodeColumnCount + 2; indexLookupKey++) {
					if (null == this.detectionResultColumns[indexLookupKey]) {
						stream.format("    |   ");
						continue;
					}
					var ds = this.detectionResultColumns[indexLookupKey].getCodewords()[i];
					if (null != ds) {
						stream.format(" %3d|%3d", ds.getRowNumber(), ds.getValue());
					} else {
						stream.format("    |   ");
					}
				}
				stream.format("%n");
			}
			return stream.toString();
		}
	}], [{
		key: "adjustRowNumberIfValid",
		value: function compensateForAnchor(value, axis, o) {
			return null == o ? axis : (o.hasValidRowNumber() || (o.isValidRowNumber(value) ? (o.setRowNumber(value), axis = 0) : ++axis), axis);
		}
	}, {
		key: "adjustRowNumber",
		value: function remove(storage, config) {
			return null != config && !(!config.hasValidRowNumber() || config.getBucket() !== storage.getBucket()) && (storage.setRowNumber(config.getRowNumber()), true);
		}
	}]);
	return render;
}();
var NotificationEvent = function () {
	/**
	 * @param {?} e
	 * @param {?} n
	 * @param {?} num
	 * @param {?} offset
	 * @return {undefined}
	 */
	function render(e, n, num, offset) {
		_classCallCheck2(this, render);
		this.rowNumber = render.BARCODE_ROW_UNKNOWN;
		/** @type {number} */
		this.startX = Math.trunc(e);
		/** @type {number} */
		this.endX = Math.trunc(n);
		/** @type {number} */
		this.bucket = Math.trunc(num);
		/** @type {number} */
		this.value = Math.trunc(offset);
	}
	_createClass2(render, [{
		key: "hasValidRowNumber",
		value: function hasValidRowNumber() {
			return this.isValidRowNumber(this.rowNumber);
		}
	}, {
		key: "isValidRowNumber",
		value: function saveList(callback) {
			return callback !== render.BARCODE_ROW_UNKNOWN && this.bucket === callback % 3 * 3;
		}
	}, {
		key: "setRowNumberAsRowIndicatorColumn",
		value: function loadList() {
			/** @type {number} */
			this.rowNumber = Math.trunc(3 * Math.trunc(this.value / 30) + Math.trunc(this.bucket / 3));
		}
	}, {
		key: "getWidth",
		value: function change() {
			return this.endX - this.startX;
		}
	}, {
		key: "getStartX",
		value: function getStartX() {
			return this.startX;
		}
	}, {
		key: "getEndX",
		value: function getEndX() {
			return this.endX;
		}
	}, {
		key: "getBucket",
		value: function getBucket() {
			return this.bucket;
		}
	}, {
		key: "getValue",
		value: function getUnifiedOperator() {
			return this.value;
		}
	}, {
		key: "getRowNumber",
		value: function getRowNumber() {
			return this.rowNumber;
		}
	}, {
		key: "setRowNumber",
		value: function render(lagOffset) {
			/** @type {number} */
			this.rowNumber = lagOffset;
		}
	}, {
		key: "toString",
		value: function handleClick() {
			return this.rowNumber + "|" + this.value;
		}
	}]);
	return render;
}();
/** @type {number} */
NotificationEvent.BARCODE_ROW_UNKNOWN = -1;
var attr = function () {
	/**
	 * @return {undefined}
	 */
	function jsonbidsResponse() {
		_classCallCheck2(this, jsonbidsResponse);
	}
	_createClass2(jsonbidsResponse, null, [{
		key: "initialize",
		value: function f() {
			/** @type {number} */
			var j = 0;
			for (; j < that.SYMBOL_TABLE.length; j++) {
				var mask = that.SYMBOL_TABLE[j];
				/** @type {number} */
				var index = 1 & mask;
				/** @type {number} */
				var y = 0;
				for (; y < that.BARS_IN_MODULE; y++) {
					/** @type {number} */
					var i = 0;
					for (; (1 & mask) === index;) {
						/** @type {number} */
						i = i + 1;
						/** @type {number} */
						mask = mask >> 1;
					}
					/** @type {number} */
					index = 1 & mask;
					if (!jsonbidsResponse.RATIOS_TABLE[j]) {
						/** @type {!Array} */
						jsonbidsResponse.RATIOS_TABLE[j] = new Array(that.BARS_IN_MODULE);
					}
					jsonbidsResponse.RATIOS_TABLE[j][that.BARS_IN_MODULE - y - 1] = Math.fround(i / that.MODULES_IN_CODEWORD);
				}
			}
			/** @type {boolean} */
			this.bSymbolTableReady = true;
		}
	}, {
		key: "getDecodedValue",
		value: function getDecodedValue(moduleBitCount) {
			var e = jsonbidsResponse.getDecodedCodewordValue(jsonbidsResponse.sampleBitCounts(moduleBitCount));
			return -1 !== e ? e : jsonbidsResponse.getClosestDecodedValue(moduleBitCount);
		}
	}, {
		key: "sampleBitCounts",
		value: function add(results) {
			var bucket = p.sum(results);
			/** @type {!Int32Array} */
			var map = new Int32Array(that.BARS_IN_MODULE);
			/** @type {number} */
			var elem = 0;
			/** @type {number} */
			var sum = 0;
			/** @type {number} */
			var i = 0;
			for (; i < that.MODULES_IN_CODEWORD; i++) {
				/** @type {number} */
				var DAYS_IN_WEEK = bucket / (2 * that.MODULES_IN_CODEWORD) + i * bucket / that.MODULES_IN_CODEWORD;
				if (sum + results[elem] <= DAYS_IN_WEEK) {
					sum = sum + results[elem];
					elem++;
				}
				map[elem]++;
			}
			return map;
		}
	}, {
		key: "getDecodedCodewordValue",
		value: function getDecodedCodewordValue(moduleBitCount) {
			var decodedValue = jsonbidsResponse.getBitValue(moduleBitCount);
			return -1 === that.getCodeword(decodedValue) ? -1 : decodedValue;
		}
	}, {
		key: "getBitValue",
		value: function fn(t) {
			/** @type {number} */
			var valueOfIsNaN = 0;
			/** @type {number} */
			var i = 0;
			for (; i < t.length; i++) {
				/** @type {number} */
				var tp = 0;
				for (; tp < t[i]; tp++) {
					/** @type {number} */
					valueOfIsNaN = valueOfIsNaN << 1 | (i % 2 == 0 ? 1 : 0);
				}
			}
			return Math.trunc(valueOfIsNaN);
		}
	}, {
		key: "getClosestDecodedValue",
		value: function add(data) {
			var step = p.sum(data);
			/** @type {!Array} */
			var ports = new Array(that.BARS_IN_MODULE);
			if (step > 1) {
				/** @type {number} */
				var i = 0;
				for (; i < ports.length; i++) {
					ports[i] = Math.fround(data[i] / step);
				}
			}
			var minExp = utils.MAX_VALUE;
			/** @type {number} */
			var i0 = -1;
			if (!this.bSymbolTableReady) {
				jsonbidsResponse.initialize();
			}
			/** @type {number} */
			var j = 0;
			for (; j < jsonbidsResponse.RATIOS_TABLE.length; j++) {
				/** @type {number} */
				var exp = 0;
				var aw = jsonbidsResponse.RATIOS_TABLE[j];
				/** @type {number} */
				var i = 0;
				for (; i < that.BARS_IN_MODULE; i++) {
					var x = Math.fround(aw[i] - ports[i]);
					if ((exp = exp + Math.fround(x * x)) >= minExp) {
						break;
					}
				}
				if (exp < minExp) {
					minExp = exp;
					i0 = that.SYMBOL_TABLE[j];
				}
			}
			return i0;
		}
	}]);
	return jsonbidsResponse;
}();
/** @type {boolean} */
attr.bSymbolTableReady = false;
/** @type {!Array<?>} */
attr.RATIOS_TABLE = (new Array(that.SYMBOL_TABLE.length)).map(function (canCreateDiscussions) {
	return new Array(that.BARS_IN_MODULE);
});
var ArrayType = function () {
	/**
	 * @return {undefined}
	 */
	function TempusDominusBootstrap3() {
		_classCallCheck2(this, TempusDominusBootstrap3);
		/** @type {number} */
		this.segmentCount = -1;
		/** @type {number} */
		this.fileSize = -1;
		/** @type {number} */
		this.timestamp = -1;
		/** @type {number} */
		this.checksum = -1;
	}
	_createClass2(TempusDominusBootstrap3, [{
		key: "getSegmentIndex",
		value: function getSegmentIndex() {
			return this.segmentIndex;
		}
	}, {
		key: "setSegmentIndex",
		value: function prefetchGroupsInfo(canCreateDiscussions) {
			this.segmentIndex = canCreateDiscussions;
		}
	}, {
		key: "getFileId",
		value: function getFileId() {
			return this.fileId;
		}
	}, {
		key: "setFileId",
		value: function GridStore(id) {
			/** @type {!Object} */
			this.fileId = id;
		}
	}, {
		key: "getOptionalData",
		value: function getOptionalData() {
			return this.optionalData;
		}
	}, {
		key: "setOptionalData",
		value: function prefetchGroupsInfo(canCreateDiscussions) {
			this.optionalData = canCreateDiscussions;
		}
	}, {
		key: "isLastSegment",
		value: function isLastSegment() {
			return this.lastSegment;
		}
	}, {
		key: "setLastSegment",
		value: function Stats(atim_msec) {
			this.lastSegment = atim_msec;
		}
	}, {
		key: "getSegmentCount",
		value: function getSegmentCount() {
			return this.segmentCount;
		}
	}, {
		key: "setSegmentCount",
		value: function playlistResponse(bitrate) {
			/** @type {!Array} */
			this.segmentCount = bitrate;
		}
	}, {
		key: "getSender",
		value: function GetMyself() {
			return this.sender || null;
		}
	}, {
		key: "setSender",
		value: function Messenger(sender) {
			/** @type {!Object} */
			this.sender = sender;
		}
	}, {
		key: "getAddressee",
		value: function getAddressee() {
			return this.addressee || null;
		}
	}, {
		key: "setAddressee",
		value: function prefetchGroupsInfo(canCreateDiscussions) {
			/** @type {string} */
			this.addressee = canCreateDiscussions;
		}
	}, {
		key: "getFileName",
		value: function getScriptName() {
			return this.fileName;
		}
	}, {
		key: "setFileName",
		value: function ProtoModalCtrl(fileName) {
			/** @type {!Blob} */
			this.fileName = fileName;
		}
	}, {
		key: "getFileSize",
		value: function size() {
			return this.fileSize;
		}
	}, {
		key: "setFileSize",
		value: function FileUpload(evaporate) {
			/** @type {!Array} */
			this.fileSize = evaporate;
		}
	}, {
		key: "getChecksum",
		value: function getChecksum() {
			return this.checksum;
		}
	}, {
		key: "setChecksum",
		value: function UTF8(checksum) {
			/** @type {!Array} */
			this.checksum = checksum;
		}
	}, {
		key: "getTimestamp",
		value: function getTaskDate() {
			return this.timestamp;
		}
	}, {
		key: "setTimestamp",
		value: function tweenProps(timestamp) {
			/** @type {string} */
			this.timestamp = timestamp;
		}
	}]);
	return TempusDominusBootstrap3;
}();
var Ke = function () {
	/**
	 * @return {undefined}
	 */
	function TempusDominusBootstrap3() {
		_classCallCheck2(this, TempusDominusBootstrap3);
	}
	_createClass2(TempusDominusBootstrap3, null, [{
		key: "parseLong",
		value: function integer(length, radix) {
			return parseInt(length, radix);
		}
	}]);
	return TempusDominusBootstrap3;
}();
var copynpaster = function (_WebInspector$GeneralTreeElement) {
	/**
	 * @return {?}
	 */
	function CacheLink() {
		_classCallCheck2(this, CacheLink);
		return _possibleConstructorReturn(this, (CacheLink.__proto__ || Object.getPrototypeOf(CacheLink)).apply(this, arguments));
	}
	_inherits(CacheLink, _WebInspector$GeneralTreeElement);
	return CacheLink;
}(input);
/** @type {string} */
copynpaster.kind = "NullPointerException";
var alwaysDownload = function () {
	/**
	 * @return {undefined}
	 */
	function TempusDominusBootstrap3() {
		_classCallCheck2(this, TempusDominusBootstrap3);
	}
	_createClass2(TempusDominusBootstrap3, [{
		key: "writeBytes",
		value: function clickWithWebdriver(selector) {
			this.writeBytesOffset(selector, 0, selector.length);
		}
	}, {
		key: "writeBytesOffset",
		value: function writeWidget(s, index, count) {
			if (null == s) {
				throw new copynpaster;
			}
			if (index < 0 || index > s.length || count < 0 || index + count > s.length || index + count < 0) {
				throw new Eventful;
			}
			if (0 !== count) {
				/** @type {number} */
				var i = 0;
				for (; i < count; i++) {
					this.write(s[index + i]);
				}
			}
		}
	}, {
		key: "flush",
		value: function flush() {
		}
	}, {
		key: "close",
		value: function close() {
		}
	}]);
	return TempusDominusBootstrap3;
}();
var uploader = function (_WebInspector$GeneralTreeElement) {
	/**
	 * @return {?}
	 */
	function CacheLink() {
		_classCallCheck2(this, CacheLink);
		return _possibleConstructorReturn(this, (CacheLink.__proto__ || Object.getPrototypeOf(CacheLink)).apply(this, arguments));
	}
	_inherits(CacheLink, _WebInspector$GeneralTreeElement);
	return CacheLink;
}(input);
var Uint8Array = function (_PlanOutOpUnary3) {
	/**
	 * @return {?}
	 */
	function LeanKitNotifier() {
		var _this;
		var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 32;
		_classCallCheck2(this, LeanKitNotifier);
		if (_this = _possibleConstructorReturn(this, (LeanKitNotifier.__proto__ || Object.getPrototypeOf(LeanKitNotifier)).call(this)), _this, _this.count = 0, data < 0) {
			throw new Function("Negative initial size: " + data);
		}
		/** @type {!Uint8Array} */
		_this.buf = new Uint8Array(data);
		return _possibleConstructorReturn(_this);
	}
	_inherits(LeanKitNotifier, _PlanOutOpUnary3);
	_createClass2(LeanKitNotifier, [{
		key: "ensureCapacity",
		value: function get(size) {
			if (size - this.buf.length > 0) {
				this.grow(size);
			}
		}
	}, {
		key: "grow",
		value: function send(length) {
			/** @type {number} */
			var offset = this.buf.length << 1;
			if (offset - length < 0 && (offset = length), offset < 0) {
				if (length < 0) {
					throw new uploader;
				}
				offset = scope.MAX_VALUE;
			}
			this.buf = b.copyOfUint8Array(this.buf, offset);
		}
	}, {
		key: "write",
		value: function $write(b) {
			this.ensureCapacity(this.count + 1);
			/** @type {boolean} */
			this.buf[this.count] = b;
			this.count += 1;
		}
	}, {
		key: "writeBytesOffset",
		value: function $write_0(b, off, len) {
			if (off < 0 || off > b.length || len < 0 || off + len - b.length > 0) {
				throw new Eventful;
			}
			this.ensureCapacity(this.count + len);
			System.arraycopy(b, off, this.buf, this.count, len);
			this.count += len;
		}
	}, {
		key: "writeTo",
		value: function GlobalComponentsContext(out) {
			out.writeBytesOffset(this.buf, 0, this.count);
		}
	}, {
		key: "reset",
		value: function subset_QMARK_() {
			/** @type {number} */
			this.count = 0;
		}
	}, {
		key: "toByteArray",
		value: function toByteArray() {
			return b.copyOfUint8Array(this.buf, this.count);
		}
	}, {
		key: "size",
		value: function count() {
			return this.count;
		}
	}, {
		key: "toString",
		value: function transform_delete_update(a) {
			return a ? "string" == typeof a ? this.toString_string(a) : this.toString_number(a) : this.toString_void();
		}
	}, {
		key: "toString_void",
		value: function read() {
			return (new String(this.buf)).toString();
		}
	}, {
		key: "toString_string",
		value: function read(options) {
			return (new String(this.buf)).toString();
		}
	}, {
		key: "toString_number",
		value: function read(options) {
			return (new String(this.buf)).toString();
		}
	}, {
		key: "close",
		value: function close() {
		}
	}]);
	return LeanKitNotifier;
}(alwaysDownload);
var truncate = void 0;
!function (ETextureFormat) {
	/** @type {string} */
	ETextureFormat[ETextureFormat.ALPHA = 0] = "ALPHA";
	/** @type {string} */
	ETextureFormat[ETextureFormat.LOWER = 1] = "LOWER";
	/** @type {string} */
	ETextureFormat[ETextureFormat.MIXED = 2] = "MIXED";
	/** @type {string} */
	ETextureFormat[ETextureFormat.PUNCT = 3] = "PUNCT";
	/** @type {string} */
	ETextureFormat[ETextureFormat.ALPHA_SHIFT = 4] = "ALPHA_SHIFT";
	/** @type {string} */
	ETextureFormat[ETextureFormat.PUNCT_SHIFT = 5] = "PUNCT_SHIFT";
}(TokenTypes || (TokenTypes = {}));
var i18n = function () {
	/**
	 * @return {undefined}
	 */
	function self() {
		_classCallCheck2(this, self);
	}
	_createClass2(self, null, [{
		key: "decode",
		value: function decode(codewords, erasures) {
			var message = new Buffer("");
			var correspondingDatumField = options.ISO8859_1;
			message.enableDecoding(correspondingDatumField);
			/** @type {number} */
			var codeIndex = 1;
			var code = codewords[codeIndex++];
			var value = new ArrayType;
			for (; codeIndex < codewords[0];) {
				switch (code) {
					case self.TEXT_COMPACTION_MODE_LATCH:
						codeIndex = self.textCompaction(codewords, codeIndex, message);
						break;
					case self.BYTE_COMPACTION_MODE_LATCH:
					case self.BYTE_COMPACTION_MODE_LATCH_6:
						codeIndex = self.byteCompaction(code, codewords, correspondingDatumField, codeIndex, message);
						break;
					case self.MODE_SHIFT_TO_BYTE_COMPACTION_MODE:
						message.append(codewords[codeIndex++]);
						break;
					case self.NUMERIC_COMPACTION_MODE_LATCH:
						codeIndex = self.numericCompaction(codewords, codeIndex, message);
						break;
					case self.ECI_CHARSET:
						options.getCharacterSetECIByValue(codewords[codeIndex++]);
						break;
					case self.ECI_GENERAL_PURPOSE:
						codeIndex = codeIndex + 2;
						break;
					case self.ECI_USER_DEFINED:
						codeIndex++;
						break;
					case self.BEGIN_MACRO_PDF417_CONTROL_BLOCK:
						codeIndex = self.decodeMacroBlock(codewords, codeIndex, value);
						break;
					case self.BEGIN_MACRO_PDF417_OPTIONAL_FIELD:
					case self.MACRO_PDF417_TERMINATOR:
						throw new Date;
					default:
						codeIndex--;
						codeIndex = self.textCompaction(codewords, codeIndex, message);
				}
				if (!(codeIndex < codewords.length)) {
					throw Date.getFormatInstance();
				}
				code = codewords[codeIndex++];
			}
			if (0 === message.length()) {
				throw Date.getFormatInstance();
			}
			var result = new Response(null, message.toString(), null, erasures);
			return result.setOther(value), result;
		}
	}, {
		key: "decodeMacroBlock",
		value: function getObject(obj, i, str) {
			if (i + self.NUMBER_OF_SEQUENCE_CODEWORDS > obj[0]) {
				throw Date.getFormatInstance();
			}
			/** @type {!Int32Array} */
			var data = new Int32Array(self.NUMBER_OF_SEQUENCE_CODEWORDS);
			/** @type {number} */
			var j = 0;
			for (; j < self.NUMBER_OF_SEQUENCE_CODEWORDS; j++, i++) {
				data[j] = obj[i];
			}
			str.setSegmentIndex(scope.parseInt(self.decodeBase900toBase10(data, self.NUMBER_OF_SEQUENCE_CODEWORDS)));
			var value = new Buffer;
			i = self.textCompaction(obj, i, value);
			str.setFileId(value.toString());
			/** @type {number} */
			var start = -1;
			if (obj[i] === self.BEGIN_MACRO_PDF417_OPTIONAL_FIELD) {
				start = i + 1;
			}
			for (; i < obj[0];) {
				switch (obj[i]) {
					case self.BEGIN_MACRO_PDF417_OPTIONAL_FIELD:
						switch (obj[++i]) {
							case self.MACRO_PDF417_OPTIONAL_FIELD_FILE_NAME:
								var res = new Buffer;
								i = self.textCompaction(obj, i + 1, res);
								str.setFileName(res.toString());
								break;
							case self.MACRO_PDF417_OPTIONAL_FIELD_SENDER:
								var value = new Buffer;
								i = self.textCompaction(obj, i + 1, value);
								str.setSender(value.toString());
								break;
							case self.MACRO_PDF417_OPTIONAL_FIELD_ADDRESSEE:
								var c = new Buffer;
								i = self.textCompaction(obj, i + 1, c);
								str.setAddressee(c.toString());
								break;
							case self.MACRO_PDF417_OPTIONAL_FIELD_SEGMENT_COUNT:
								var data = new Buffer;
								i = self.numericCompaction(obj, i + 1, data);
								str.setSegmentCount(scope.parseInt(data.toString()));
								break;
							case self.MACRO_PDF417_OPTIONAL_FIELD_TIME_STAMP:
								var len = new Buffer;
								i = self.numericCompaction(obj, i + 1, len);
								str.setTimestamp(Ke.parseLong(len.toString()));
								break;
							case self.MACRO_PDF417_OPTIONAL_FIELD_CHECKSUM:
								var end = new Buffer;
								i = self.numericCompaction(obj, i + 1, end);
								str.setChecksum(scope.parseInt(end.toString()));
								break;
							case self.MACRO_PDF417_OPTIONAL_FIELD_FILE_SIZE:
								var name = new Buffer;
								i = self.numericCompaction(obj, i + 1, name);
								str.setFileSize(Ke.parseLong(name.toString()));
								break;
							default:
								throw Date.getFormatInstance();
						}break;
					case self.MACRO_PDF417_TERMINATOR:
						i++;
						str.setLastSegment(true);
						break;
					default:
						throw Date.getFormatInstance();
				}
			}
			if (-1 !== start) {
				/** @type {number} */
				var length = i - start;
				if (str.isLastSegment()) {
					length--;
				}
				str.setOptionalData(b.copyOfRange(obj, start, start + length));
			}
			return i;
		}
	}, {
		key: "textCompaction",
		value: function getDecimalReplacement(vector, result, count) {
			/** @type {!Int32Array} */
			var buffer = new Int32Array(2 * (vector[0] - result));
			/** @type {!Int32Array} */
			var array = new Int32Array(2 * (vector[0] - result));
			/** @type {number} */
			var i = 0;
			/** @type {boolean} */
			var cursorLeftSet = false;
			for (; result < vector[0] && !cursorLeftSet;) {
				var x = vector[result++];
				if (x < self.TEXT_COMPACTION_MODE_LATCH) {
					/** @type {number} */
					buffer[i] = x / 30;
					/** @type {number} */
					buffer[i + 1] = x % 30;
					/** @type {number} */
					i = i + 2;
				} else {
					switch (x) {
						case self.TEXT_COMPACTION_MODE_LATCH:
							buffer[i++] = self.TEXT_COMPACTION_MODE_LATCH;
							break;
						case self.BYTE_COMPACTION_MODE_LATCH:
						case self.BYTE_COMPACTION_MODE_LATCH_6:
						case self.NUMERIC_COMPACTION_MODE_LATCH:
						case self.BEGIN_MACRO_PDF417_CONTROL_BLOCK:
						case self.BEGIN_MACRO_PDF417_OPTIONAL_FIELD:
						case self.MACRO_PDF417_TERMINATOR:
							result--;
							/** @type {boolean} */
							cursorLeftSet = true;
							break;
						case self.MODE_SHIFT_TO_BYTE_COMPACTION_MODE:
							buffer[i] = self.MODE_SHIFT_TO_BYTE_COMPACTION_MODE;
							x = vector[result++];
							array[i] = x;
							i++;
					}
				}
			}
			return self.decodeTextCompaction(buffer, array, i, count), result;
		}
	}, {
		key: "decodeTextCompaction",
		value: function update(next, a, indent, c) {
			var type = TokenTypes.ALPHA;
			var currentUserContainer = TokenTypes.ALPHA;
			/** @type {number} */
			var i = 0;
			for (; i < indent;) {
				var j = next[i];
				/** @type {string} */
				var mask = "";
				switch (type) {
					case TokenTypes.ALPHA:
						if (j < 26) {
							/** @type {string} */
							mask = String.fromCharCode(65 + j);
						} else {
							switch (j) {
								case 26:
									/** @type {string} */
									mask = " ";
									break;
								case self.LL:
									type = TokenTypes.LOWER;
									break;
								case self.ML:
									type = TokenTypes.MIXED;
									break;
								case self.PS:
									currentUserContainer = type;
									type = TokenTypes.PUNCT_SHIFT;
									break;
								case self.MODE_SHIFT_TO_BYTE_COMPACTION_MODE:
									c.append(a[i]);
									break;
								case self.TEXT_COMPACTION_MODE_LATCH:
									type = TokenTypes.ALPHA;
							}
						}
						break;
					case TokenTypes.LOWER:
						if (j < 26) {
							/** @type {string} */
							mask = String.fromCharCode(97 + j);
						} else {
							switch (j) {
								case 26:
									/** @type {string} */
									mask = " ";
									break;
								case self.AS:
									currentUserContainer = type;
									type = TokenTypes.ALPHA_SHIFT;
									break;
								case self.ML:
									type = TokenTypes.MIXED;
									break;
								case self.PS:
									currentUserContainer = type;
									type = TokenTypes.PUNCT_SHIFT;
									break;
								case self.MODE_SHIFT_TO_BYTE_COMPACTION_MODE:
									c.append(a[i]);
									break;
								case self.TEXT_COMPACTION_MODE_LATCH:
									type = TokenTypes.ALPHA;
							}
						}
						break;
					case TokenTypes.MIXED:
						if (j < self.PL) {
							mask = self.MIXED_CHARS[j];
						} else {
							switch (j) {
								case self.PL:
									type = TokenTypes.PUNCT;
									break;
								case 26:
									/** @type {string} */
									mask = " ";
									break;
								case self.LL:
									type = TokenTypes.LOWER;
									break;
								case self.AL:
									type = TokenTypes.ALPHA;
									break;
								case self.PS:
									currentUserContainer = type;
									type = TokenTypes.PUNCT_SHIFT;
									break;
								case self.MODE_SHIFT_TO_BYTE_COMPACTION_MODE:
									c.append(a[i]);
									break;
								case self.TEXT_COMPACTION_MODE_LATCH:
									type = TokenTypes.ALPHA;
							}
						}
						break;
					case TokenTypes.PUNCT:
						if (j < self.PAL) {
							mask = self.PUNCT_CHARS[j];
						} else {
							switch (j) {
								case self.PAL:
									type = TokenTypes.ALPHA;
									break;
								case self.MODE_SHIFT_TO_BYTE_COMPACTION_MODE:
									c.append(a[i]);
									break;
								case self.TEXT_COMPACTION_MODE_LATCH:
									type = TokenTypes.ALPHA;
							}
						}
						break;
					case TokenTypes.ALPHA_SHIFT:
						if (type = currentUserContainer, j < 26) {
							/** @type {string} */
							mask = String.fromCharCode(65 + j);
						} else {
							switch (j) {
								case 26:
									/** @type {string} */
									mask = " ";
									break;
								case self.TEXT_COMPACTION_MODE_LATCH:
									type = TokenTypes.ALPHA;
							}
						}
						break;
					case TokenTypes.PUNCT_SHIFT:
						if (type = currentUserContainer, j < self.PAL) {
							mask = self.PUNCT_CHARS[j];
						} else {
							switch (j) {
								case self.PAL:
									type = TokenTypes.ALPHA;
									break;
								case self.MODE_SHIFT_TO_BYTE_COMPACTION_MODE:
									c.append(a[i]);
									break;
								case self.TEXT_COMPACTION_MODE_LATCH:
									type = TokenTypes.ALPHA;
							}
						}
				}
				if ("" !== mask) {
					c.append(mask);
				}
				i++;
			}
		}
	}, {
		key: "byteCompaction",
		value: function process(text, obj, i, value, element) {
			var out = new Uint8Array;
			/** @type {number} */
			var resultLen = 0;
			/** @type {number} */
			var id = 0;
			/** @type {boolean} */
			var tick = false;
			switch (text) {
				case self.BYTE_COMPACTION_MODE_LATCH:
					/** @type {!Int32Array} */
					var result = new Int32Array(6);
					var index = obj[value++];
					for (; value < obj[0] && !tick;) {
						switch (result[resultLen++] = index, id = 900 * id + index, index = obj[value++]) {
							case self.TEXT_COMPACTION_MODE_LATCH:
							case self.BYTE_COMPACTION_MODE_LATCH:
							case self.NUMERIC_COMPACTION_MODE_LATCH:
							case self.BYTE_COMPACTION_MODE_LATCH_6:
							case self.BEGIN_MACRO_PDF417_CONTROL_BLOCK:
							case self.BEGIN_MACRO_PDF417_OPTIONAL_FIELD:
							case self.MACRO_PDF417_TERMINATOR:
								value--;
								/** @type {boolean} */
								tick = true;
								break;
							default:
								if (resultLen % 5 == 0 && resultLen > 0) {
									/** @type {number} */
									var _t170 = 0;
									for (; _t170 < 6; ++_t170) {
										out.write(Number(parseInt(id) >> parseInt(8 * (5 - _t170))));
									}
									/** @type {number} */
									id = 0;
									/** @type {number} */
									resultLen = 0;
								}
						}
					}
					if (value === obj[0] && index < self.TEXT_COMPACTION_MODE_LATCH) {
						result[resultLen++] = index;
					}
					/** @type {number} */
					var i = 0;
					for (; i < resultLen; i++) {
						out.write(result[i]);
					}
					break;
				case self.BYTE_COMPACTION_MODE_LATCH_6:
					for (; value < obj[0] && !tick;) {
						var index = obj[value++];
						if (index < self.TEXT_COMPACTION_MODE_LATCH) {
							resultLen++;
							id = 900 * id + index;
						} else {
							switch (index) {
								case self.TEXT_COMPACTION_MODE_LATCH:
								case self.BYTE_COMPACTION_MODE_LATCH:
								case self.NUMERIC_COMPACTION_MODE_LATCH:
								case self.BYTE_COMPACTION_MODE_LATCH_6:
								case self.BEGIN_MACRO_PDF417_CONTROL_BLOCK:
								case self.BEGIN_MACRO_PDF417_OPTIONAL_FIELD:
								case self.MACRO_PDF417_TERMINATOR:
									value--;
									/** @type {boolean} */
									tick = true;
							}
						}
						if (resultLen % 5 == 0 && resultLen > 0) {
							/** @type {number} */
							var _t173 = 0;
							for (; _t173 < 6; ++_t173) {
								out.write(Number(parseInt(id) >> parseInt(8 * (5 - _t173))));
							}
							/** @type {number} */
							id = 0;
							/** @type {number} */
							resultLen = 0;
						}
					}
			}
			return element.append(parser.decode(out.toByteArray(), i)), value;
		}
	}, {
		key: "numericCompaction",
		value: function hash(table, code, data) {
			/** @type {number} */
			var i = 0;
			/** @type {boolean} */
			var _inArray = false;
			/** @type {!Int32Array} */
			var r = new Int32Array(self.MAX_NUMERIC_CODEWORDS);
			for (; code < table[0] && !_inArray;) {
				var row = table[code++];
				if (code === table[0] && (_inArray = true), row < self.TEXT_COMPACTION_MODE_LATCH) {
					r[i] = row;
					i++;
				} else {
					switch (row) {
						case self.TEXT_COMPACTION_MODE_LATCH:
						case self.BYTE_COMPACTION_MODE_LATCH:
						case self.BYTE_COMPACTION_MODE_LATCH_6:
						case self.BEGIN_MACRO_PDF417_CONTROL_BLOCK:
						case self.BEGIN_MACRO_PDF417_OPTIONAL_FIELD:
						case self.MACRO_PDF417_TERMINATOR:
							code--;
							/** @type {boolean} */
							_inArray = true;
					}
				}
				if ((i % self.MAX_NUMERIC_CODEWORDS == 0 || row === self.NUMERIC_COMPACTION_MODE_LATCH || _inArray) && i > 0) {
					data.append(self.decodeBase900toBase10(r, i));
					/** @type {number} */
					i = 0;
				}
			}
			return code;
		}
	}, {
		key: "decodeBase900toBase10",
		value: function cb(stats, v) {
			var word = parseInt(0);
			/** @type {number} */
			var l = 0;
			for (; l < v; l++) {
				word = word + self.EXP900[v - l - 1] * parseInt(stats[l]);
			}
			var valueConfig = word.toString();
			if ("1" !== valueConfig.charAt(0)) {
				throw new Date;
			}
			return valueConfig.substring(1);
		}
	}]);
	return self;
}();
/** @type {number} */
i18n.TEXT_COMPACTION_MODE_LATCH = 900;
/** @type {number} */
i18n.BYTE_COMPACTION_MODE_LATCH = 901;
/** @type {number} */
i18n.NUMERIC_COMPACTION_MODE_LATCH = 902;
/** @type {number} */
i18n.BYTE_COMPACTION_MODE_LATCH_6 = 924;
/** @type {number} */
i18n.ECI_USER_DEFINED = 925;
/** @type {number} */
i18n.ECI_GENERAL_PURPOSE = 926;
/** @type {number} */
i18n.ECI_CHARSET = 927;
/** @type {number} */
i18n.BEGIN_MACRO_PDF417_CONTROL_BLOCK = 928;
/** @type {number} */
i18n.BEGIN_MACRO_PDF417_OPTIONAL_FIELD = 923;
/** @type {number} */
i18n.MACRO_PDF417_TERMINATOR = 922;
/** @type {number} */
i18n.MODE_SHIFT_TO_BYTE_COMPACTION_MODE = 913;
/** @type {number} */
i18n.MAX_NUMERIC_CODEWORDS = 15;
/** @type {number} */
i18n.MACRO_PDF417_OPTIONAL_FIELD_FILE_NAME = 0;
/** @type {number} */
i18n.MACRO_PDF417_OPTIONAL_FIELD_SEGMENT_COUNT = 1;
/** @type {number} */
i18n.MACRO_PDF417_OPTIONAL_FIELD_TIME_STAMP = 2;
/** @type {number} */
i18n.MACRO_PDF417_OPTIONAL_FIELD_SENDER = 3;
/** @type {number} */
i18n.MACRO_PDF417_OPTIONAL_FIELD_ADDRESSEE = 4;
/** @type {number} */
i18n.MACRO_PDF417_OPTIONAL_FIELD_FILE_SIZE = 5;
/** @type {number} */
i18n.MACRO_PDF417_OPTIONAL_FIELD_CHECKSUM = 6;
/** @type {number} */
i18n.PL = 25;
/** @type {number} */
i18n.LL = 27;
/** @type {number} */
i18n.AS = 27;
/** @type {number} */
i18n.ML = 28;
/** @type {number} */
i18n.AL = 28;
/** @type {number} */
i18n.PS = 29;
/** @type {number} */
i18n.PAL = 29;
/** @type {string} */
i18n.PUNCT_CHARS = ";<>@[\\]_`~!\r\t,:\n-.$/\"|*()?{}'";
/** @type {string} */
i18n.MIXED_CHARS = "0123456789&\r\t,:#-.$/+%*=^";
i18n.EXP900 = replace() ? function () {
	/** @type {!Array} */
	var a = [];
	a[0] = parseInt(1);
	var y = parseInt(900);
	a[1] = y;
	/** @type {number} */
	var i = 2;
	for (; i < 16; i++) {
		/** @type {number} */
		a[i] = a[i - 1] * y;
	}
	return a;
}() : [];
/** @type {number} */
i18n.NUMBER_OF_SEQUENCE_CODEWORDS = 2;
var ctx = function () {
	/**
	 * @return {undefined}
	 */
	function exports() {
		_classCallCheck2(this, exports);
	}
	_createClass2(exports, null, [{
		key: "decode",
		value: function render(image, options, data, d, color, minCodewordWidth, maxCodewordWidth) {
			var detectionResult = void 0;
			var polygon = new Polygon(image, options, data, d, color);
			/** @type {null} */
			var leftRowIndicatorColumn = null;
			/** @type {null} */
			var rightRowIndicatorColumn = null;
			/** @type {boolean} */
			var currSect = true;
			for (; ; currSect = false) {
				if (null != options && (leftRowIndicatorColumn = exports.getRowIndicatorColumn(image, polygon, options, true, minCodewordWidth, maxCodewordWidth)), null != d && (rightRowIndicatorColumn = exports.getRowIndicatorColumn(image, polygon, d, false, minCodewordWidth, maxCodewordWidth)), null == (detectionResult = exports.merge(leftRowIndicatorColumn, rightRowIndicatorColumn))) {
					throw TypeError.getNotFoundInstance();
				}
				var decompressor = detectionResult.getBoundingBox();
				if (!currSect || null == decompressor || !(decompressor.getMinY() < polygon.getMinY() || decompressor.getMaxY() > polygon.getMaxY())) {
					break;
				}
				polygon = decompressor;
			}
			detectionResult.setBoundingBox(polygon);
			var maxBarcodeColumn = detectionResult.getBarcodeColumnCount() + 1;
			detectionResult.setDetectionResultColumn(0, leftRowIndicatorColumn);
			detectionResult.setDetectionResultColumn(maxBarcodeColumn, rightRowIndicatorColumn);
			/** @type {boolean} */
			var leftToRight = null != leftRowIndicatorColumn;
			/** @type {number} */
			var barcodeColumnCount = 1;
			for (; barcodeColumnCount <= maxBarcodeColumn; barcodeColumnCount++) {
				var detectionResultColumn = void 0;
				/** @type {number} */
				var barcodeColumn = leftToRight ? barcodeColumnCount : maxBarcodeColumn - barcodeColumnCount;
				if (void 0 !== detectionResult.getDetectionResultColumn(barcodeColumn)) {
					continue;
				}
				detectionResultColumn = 0 === barcodeColumn || barcodeColumn === maxBarcodeColumn ? new Validator(polygon, 0 === barcodeColumn) : new Tag(polygon);
				detectionResult.setDetectionResultColumn(barcodeColumn, detectionResultColumn);
				/** @type {number} */
				var startColumn = -1;
				/** @type {number} */
				var previousStartColumn = startColumn;
				var imageRow = polygon.getMinY();
				for (; imageRow <= polygon.getMaxY(); imageRow++) {
					if ((startColumn = exports.getStartColumn(detectionResult, barcodeColumn, imageRow, leftToRight)) < 0 || startColumn > polygon.getMaxX()) {
						if (-1 === previousStartColumn) {
							continue;
						}
						startColumn = previousStartColumn;
					}
					var codeword = exports.detectCodeword(image, polygon.getMinX(), polygon.getMaxX(), leftToRight, startColumn, imageRow, minCodewordWidth, maxCodewordWidth);
					if (null != codeword) {
						detectionResultColumn.setCodeword(imageRow, codeword);
						previousStartColumn = startColumn;
						/** @type {number} */
						minCodewordWidth = Math.min(minCodewordWidth, codeword.getWidth());
						/** @type {number} */
						maxCodewordWidth = Math.max(maxCodewordWidth, codeword.getWidth());
					}
				}
			}
			return exports.createDecoderResult(detectionResult);
		}
	}, {
		key: "merge",
		value: function merge(leftRowIndicatorColumn, rightRowIndicatorColumn) {
			if (null == leftRowIndicatorColumn && null == rightRowIndicatorColumn) {
				return null;
			}
			var barcodeMetadata = exports.getBarcodeMetadata(leftRowIndicatorColumn, rightRowIndicatorColumn);
			if (null == barcodeMetadata) {
				return null;
			}
			var blobqueueConstructor = Polygon.merge(exports.adjustBoundingBox(leftRowIndicatorColumn), exports.adjustBoundingBox(rightRowIndicatorColumn));
			return new runtimeConstructor(barcodeMetadata, blobqueueConstructor);
		}
	}, {
		key: "adjustBoundingBox",
		value: function init(obj) {
			if (null == obj) {
				return null;
			}
			var json = obj.getRowHeights();
			if (null == json) {
				return null;
			}
			var maxRowHeight = exports.getMax(json);
			/** @type {number} */
			var missingStartRows = 0;
			/** @type {boolean} */
			var _iteratorNormalCompletion3 = true;
			/** @type {boolean} */
			var _didIteratorError62 = false;
			var _iteratorError17 = undefined;
			try {
				var _iterator3 = json[Symbol.iterator]();
				var t;
				for (; !(_iteratorNormalCompletion3 = (t = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var rowHeight = t.value;
					if (missingStartRows = missingStartRows + (maxRowHeight - rowHeight), rowHeight > 0) {
						break;
					}
				}
			} catch (err) {
				/** @type {boolean} */
				_didIteratorError62 = true;
				_iteratorError17 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError62) {
						throw _iteratorError17;
					}
				}
			}
			var payloads = obj.getCodewords();
			/** @type {number} */
			var signedTransactionsCounter = 0;
			for (; missingStartRows > 0 && null == payloads[signedTransactionsCounter]; signedTransactionsCounter++) {
				missingStartRows--;
			}
			/** @type {number} */
			var missingEndRows = 0;
			/** @type {number} */
			var row = json.length - 1;
			for (; row >= 0 && (missingEndRows = missingEndRows + (maxRowHeight - json[row]), !(json[row] > 0)); row--) {
			}
			/** @type {number} */
			var i = payloads.length - 1;
			for (; missingEndRows > 0 && null == payloads[i]; i--) {
				missingEndRows--;
			}
			return obj.getBoundingBox().addMissingRows(missingStartRows, missingEndRows, obj.isLeft());
		}
	}, {
		key: "getMax",
		value: function add(_args) {
			/** @type {number} */
			var index = -1;
			/** @type {boolean} */
			var _iteratorNormalCompletion3 = true;
			/** @type {boolean} */
			var _didIteratorError63 = false;
			var _iteratorError17 = undefined;
			try {
				var _iterator3 = _args[Symbol.iterator]();
				var $__6;
				for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var item = $__6.value;
					/** @type {number} */
					index = Math.max(index, item);
				}
			} catch (err) {
				/** @type {boolean} */
				_didIteratorError63 = true;
				_iteratorError17 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError63) {
						throw _iteratorError17;
					}
				}
			}
			return index;
		}
	}, {
		key: "getBarcodeMetadata",
		value: function layoutDelegate(tilesInvalidated, leftRowIndicatorColumn) {
			var _self = void 0;
			var grid = void 0;
			return null == tilesInvalidated || null == (_self = tilesInvalidated.getBarcodeMetadata()) ? null == leftRowIndicatorColumn ? null : leftRowIndicatorColumn.getBarcodeMetadata() : null == leftRowIndicatorColumn || null == (grid = leftRowIndicatorColumn.getBarcodeMetadata()) ? _self : _self.getColumnCount() !== grid.getColumnCount() && _self.getErrorCorrectionLevel() !== grid.getErrorCorrectionLevel() && _self.getRowCount() !== grid.getRowCount() ? null : _self;
		}
	}, {
		key: "getRowIndicatorColumn",
		value: function getRowIndicatorColumn(image, boundingBox, feature, leftToRight, minCodewordWidth, maxCodewordWidth) {
			var rowIndicatorColumn = new Validator(boundingBox, leftToRight);
			/** @type {number} */
			var _a24 = 0;
			for (; _a24 < 2; _a24++) {
				/** @type {number} */
				var increment = 0 === _a24 ? 1 : -1;
				/** @type {number} */
				var startColumn = Math.trunc(Math.trunc(feature.getX()));
				/** @type {number} */
				var imageRow = Math.trunc(Math.trunc(feature.getY()));
				for (; imageRow <= boundingBox.getMaxY() && imageRow >= boundingBox.getMinY(); imageRow = imageRow + increment) {
					var codeword = exports.detectCodeword(image, 0, image.getWidth(), leftToRight, startColumn, imageRow, minCodewordWidth, maxCodewordWidth);
					if (null != codeword) {
						rowIndicatorColumn.setCodeword(imageRow, codeword);
						startColumn = leftToRight ? codeword.getStartX() : codeword.getEndX();
					}
				}
			}
			return rowIndicatorColumn;
		}
	}, {
		key: "adjustCodewordCount",
		value: function getArrayArgs(delimiter, writeNewLine) {
			var frameRange = writeNewLine[0][1];
			var object = frameRange.getValue();
			/** @type {number} */
			var value = delimiter.getBarcodeColumnCount() * delimiter.getBarcodeRowCount() - exports.getNumberOfECCodeWords(delimiter.getBarcodeECLevel());
			if (0 === object.length) {
				if (value < 1 || value > that.MAX_CODEWORDS_IN_BARCODE) {
					throw TypeError.getNotFoundInstance();
				}
				frameRange.setValue(value);
			} else {
				if (object[0] !== value) {
					frameRange.setValue(value);
				}
			}
		}
	}, {
		key: "createDecoderResult",
		value: function initializeGrid(detectionResult) {
			var barcodeMatrix = exports.createBarcodeMatrix(detectionResult);
			exports.adjustCodewordCount(detectionResult, barcodeMatrix);
			/** @type {!Array} */
			var path = new Array;
			/** @type {!Int32Array} */
			var result = new Int32Array(detectionResult.getBarcodeRowCount() * detectionResult.getBarcodeColumnCount());
			/** @type {!Array} */
			var self = [];
			/** @type {!Array} */
			var str = new Array;
			/** @type {number} */
			var mrI = 0;
			for (; mrI < detectionResult.getBarcodeRowCount(); mrI++) {
				/** @type {number} */
				var mcI = 0;
				for (; mcI < detectionResult.getBarcodeColumnCount(); mcI++) {
					var vs = barcodeMatrix[mrI][mcI + 1].getValue();
					/** @type {number} */
					var index = mrI * detectionResult.getBarcodeColumnCount() + mcI;
					if (0 === vs.length) {
						path.push(index);
					} else {
						if (1 === vs.length) {
							result[index] = vs[0];
						} else {
							str.push(index);
							self.push(vs);
						}
					}
				}
			}
			/** @type {!Array} */
			var options = new Array(self.length);
			/** @type {number} */
			var i = 0;
			for (; i < options.length; i++) {
				options[i] = self[i];
			}
			return exports.createDecoderResultFromAmbiguousValues(detectionResult.getBarcodeECLevel(), result, that.toIntArray(path), that.toIntArray(str), options);
		}
	}, {
		key: "createDecoderResultFromAmbiguousValues",
		value: function createDecoderResultFromAmbiguousValues(ecLevel, codewords, erasureArray, ambiguousIndexes, ambiguousIndexValues) {
			/** @type {!Int32Array} */
			var ambiguousIndexCount = new Int32Array(ambiguousIndexes.length);
			/** @type {number} */
			var o = 100;
			for (; o-- > 0;) {
				/** @type {number} */
				var i = 0;
				for (; i < ambiguousIndexCount.length; i++) {
					codewords[ambiguousIndexes[i]] = ambiguousIndexValues[i][ambiguousIndexCount[i]];
				}
				try {
					return exports.decodeCodewords(codewords, ecLevel, erasureArray);
				} catch (viewOrExtent) {
					if (!(viewOrExtent instanceof Rectangle)) {
						throw viewOrExtent;
					}
				}
				if (0 === ambiguousIndexCount.length) {
					throw Rectangle.getChecksumInstance();
				}
				/** @type {number} */
				var methodIndex = 0;
				for (; methodIndex < ambiguousIndexCount.length; methodIndex++) {
					if (ambiguousIndexCount[methodIndex] < ambiguousIndexValues[methodIndex].length - 1) {
						ambiguousIndexCount[methodIndex]++;
						break;
					}
					if (ambiguousIndexCount[methodIndex] = 0, methodIndex === ambiguousIndexCount.length - 1) {
						throw Rectangle.getChecksumInstance();
					}
				}
			}
			throw Rectangle.getChecksumInstance();
		}
	}, {
		key: "createBarcodeMatrix",
		value: function createAssembly(detectionResult) {
			/** @type {!Array<?>} */
			var elements = Array.from({
				length: detectionResult.getBarcodeRowCount()
			}, function () {
				return new Array(detectionResult.getBarcodeColumnCount() + 2);
			});
			/** @type {number} */
			var i = 0;
			for (; i < elements.length; i++) {
				/** @type {number} */
				var index = 0;
				for (; index < elements[i].length; index++) {
					elements[i][index] = new Point;
				}
			}
			/** @type {number} */
			var id = 0;
			/** @type {boolean} */
			var _iteratorNormalCompletion3 = true;
			/** @type {boolean} */
			var _didIteratorError64 = false;
			var _iteratorError17 = undefined;
			try {
				var _iterator3 = detectionResult.getDetectionResultColumns()[Symbol.iterator]();
				var $__6;
				for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var item = $__6.value;
					if (null != item) {
						/** @type {boolean} */
						var _iteratorNormalCompletion3 = true;
						/** @type {boolean} */
						var _didIteratorError65 = false;
						var _iteratorError17 = undefined;
						try {
							var _iterator3 = item.getCodewords()[Symbol.iterator]();
							var $__6;
							for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
								var item = $__6.value;
								if (null != item) {
									var i = item.getRowNumber();
									if (i >= 0) {
										if (i >= elements.length) {
											continue;
										}
										elements[i][id].setValue(item.getValue());
									}
								}
							}
						} catch (err) {
							/** @type {boolean} */
							_didIteratorError65 = true;
							_iteratorError17 = err;
						} finally {
							try {
								if (!_iteratorNormalCompletion3 && _iterator3.return) {
									_iterator3.return();
								}
							} finally {
								if (_didIteratorError65) {
									throw _iteratorError17;
								}
							}
						}
					}
					id++;
				}
			} catch (err) {
				/** @type {boolean} */
				_didIteratorError64 = true;
				_iteratorError17 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError64) {
						throw _iteratorError17;
					}
				}
			}
			return elements;
		}
	}, {
		key: "isValidBarcodeColumn",
		value: function isValidBarcodeColumn(detectionResult, barcodeColumn) {
			return barcodeColumn >= 0 && barcodeColumn <= detectionResult.getBarcodeColumnCount() + 1;
		}
	}, {
		key: "getStartColumn",
		value: function done(event, number, imageRow, minCodewordWidth) {
			/** @type {number} */
			var abs = minCodewordWidth ? 1 : -1;
			/** @type {null} */
			var ev = null;
			if (exports.isValidBarcodeColumn(event, number - abs) && (ev = event.getDetectionResultColumn(number - abs).getCodeword(imageRow)), null != ev) {
				return minCodewordWidth ? ev.getEndX() : ev.getStartX();
			}
			if (null != (ev = event.getDetectionResultColumn(number).getCodewordNearby(imageRow))) {
				return minCodewordWidth ? ev.getStartX() : ev.getEndX();
			}
			if (exports.isValidBarcodeColumn(event, number - abs) && (ev = event.getDetectionResultColumn(number - abs).getCodewordNearby(imageRow)), null != ev) {
				return minCodewordWidth ? ev.getEndX() : ev.getStartX();
			}
			/** @type {number} */
			var mult = 0;
			for (; exports.isValidBarcodeColumn(event, number - abs);) {
				/** @type {number} */
				number = number - abs;
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError66 = false;
				var _iteratorError17 = undefined;
				try {
					var _iterator3 = event.getDetectionResultColumn(number).getCodewords()[Symbol.iterator]();
					var $__6;
					for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var item = $__6.value;
						if (null != item) {
							return (minCodewordWidth ? item.getEndX() : item.getStartX()) + abs * mult * (item.getEndX() - item.getStartX());
						}
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError66 = true;
					_iteratorError17 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError66) {
							throw _iteratorError17;
						}
					}
				}
				mult++;
			}
			return minCodewordWidth ? event.getBoundingBox().getMinX() : event.getBoundingBox().getMaxX();
		}
	}, {
		key: "detectCodeword",
		value: function detectCodeword(image, minColumn, maxColumn, leftToRight, startColumn, imageRow, minCodewordWidth, maxCodewordWidth) {
			startColumn = exports.adjustCodewordStartColumn(image, minColumn, maxColumn, leftToRight, startColumn, imageRow);
			var endColumn = void 0;
			var moduleBitCount = exports.getModuleBitCount(image, minColumn, maxColumn, leftToRight, startColumn, imageRow);
			if (null == moduleBitCount) {
				return null;
			}
			var codewordBitCount = p.sum(moduleBitCount);
			if (leftToRight) {
				endColumn = startColumn + codewordBitCount;
			} else {
				/** @type {number} */
				var i = 0;
				for (; i < moduleBitCount.length / 2; i++) {
					var tmpCount = moduleBitCount[i];
					moduleBitCount[i] = moduleBitCount[moduleBitCount.length - 1 - i];
					moduleBitCount[moduleBitCount.length - 1 - i] = tmpCount;
				}
				/** @type {number} */
				startColumn = (endColumn = startColumn) - codewordBitCount;
			}
			if (!exports.checkCodewordSkew(codewordBitCount, minCodewordWidth, maxCodewordWidth)) {
				return null;
			}
			var decodedValue = attr.getDecodedValue(moduleBitCount);
			var category = that.getCodeword(decodedValue);
			return -1 === category ? null : new NotificationEvent(startColumn, endColumn, exports.getCodewordBucketNumber(decodedValue), category);
		}
	}, {
		key: "getModuleBitCount",
		value: function move($el, e, end, rtl, start, data) {
			var a = start;
			/** @type {!Int32Array} */
			var buffer = new Int32Array(8);
			/** @type {number} */
			var i = 0;
			/** @type {number} */
			var b = rtl ? 1 : -1;
			/** @type {boolean} */
			var matches = rtl;
			for (; (rtl ? a < end : a >= e) && i < buffer.length;) {
				if ($el.get(a, data) === matches) {
					buffer[i]++;
					a = a + b;
				} else {
					i++;
					/** @type {boolean} */
					matches = !matches;
				}
			}
			return i === buffer.length || a === (rtl ? end : e) && i === buffer.length - 1 ? buffer : null;
		}
	}, {
		key: "getNumberOfECCodeWords",
		value: function prefetchGroupsInfo(canCreateDiscussions) {
			return 2 << canCreateDiscussions;
		}
	}, {
		key: "adjustCodewordStartColumn",
		value: function show_json_structure(debug, l, range, reverse, x, obj) {
			var i = x;
			/** @type {number} */
			var step = reverse ? -1 : 1;
			/** @type {number} */
			var _l22 = 0;
			for (; _l22 < 2; _l22++) {
				for (; (reverse ? i >= l : i < range) && reverse === debug.get(i, obj);) {
					if (Math.abs(x - i) > exports.CODEWORD_SKEW_SIZE) {
						return x;
					}
					i = i + step;
				}
				/** @type {number} */
				step = -step;
				/** @type {boolean} */
				reverse = !reverse;
			}
			return i;
		}
	}, {
		key: "checkCodewordSkew",
		value: function checkCodewordSkew(codewordSize, minCodewordWidth, maxCodewordWidth) {
			return minCodewordWidth - exports.CODEWORD_SKEW_SIZE <= codewordSize && codewordSize <= maxCodewordWidth + exports.CODEWORD_SKEW_SIZE;
		}
	}, {
		key: "decodeCodewords",
		value: function eventMessageHandler(date, e, that) {
			if (0 === date.length) {
				throw Date.getFormatInstance();
			}
			/** @type {number} */
			var registry = 1 << e + 1;
			var val = exports.correctErrors(date, that, registry);
			exports.verifyCodewordCount(date, registry);
			var copy = i18n.decode(date, "" + e);
			return copy.setErrorsCorrected(val), copy.setErasures(that.length), copy;
		}
	}, {
		key: "correctErrors",
		value: function decode(context, payload, secret) {
			if (null != payload && payload.length > secret / 2 + exports.MAX_ERRORS || secret < 0 || secret > exports.MAX_EC_CODEWORDS) {
				throw Rectangle.getChecksumInstance();
			}
			return exports.errorCorrection.decode(context, secret, payload);
		}
	}, {
		key: "verifyCodewordCount",
		value: function decode(codewords, numECCodewords) {
			if (codewords.length < 4) {
				throw Date.getFormatInstance();
			}
			var numberOfCodewords = codewords[0];
			if (numberOfCodewords > codewords.length) {
				throw Date.getFormatInstance();
			}
			if (0 === numberOfCodewords) {
				if (!(numECCodewords < codewords.length)) {
					throw Date.getFormatInstance();
				}
				/** @type {number} */
				codewords[0] = codewords.length - numECCodewords;
			}
		}
	}, {
		key: "getBitCountForCodeword",
		value: function naive_merge_rest(mask) {
			/** @type {!Int32Array} */
			var res = new Int32Array(8);
			/** @type {number} */
			var low = 0;
			/** @type {number} */
			var LAT = res.length - 1;
			for (; !((1 & mask) !== low && (low = 1 & mask, --LAT < 0));) {
				res[LAT]++;
				/** @type {number} */
				mask = mask >> 1;
			}
			return res;
		}
	}, {
		key: "getCodewordBucketNumber",
		value: function glyphy_float_to_two_nimbles(v) {
			return v instanceof Int32Array ? this.getCodewordBucketNumber_Int32Array(v) : this.getCodewordBucketNumber_number(v);
		}
	}, {
		key: "getCodewordBucketNumber_number",
		value: function loadAndSortStyle(tssFile) {
			return exports.getCodewordBucketNumber(exports.getBitCountForCodeword(tssFile));
		}
	}, {
		key: "getCodewordBucketNumber_Int32Array",
		value: function getContentTypeForFormat(f) {
			return (f[0] - f[2] + f[4] - f[6] + 9) % 9;
		}
	}, {
		key: "toString",
		value: function value(values) {
			var stream = new Deserializer;
			/** @type {number} */
			var i = 0;
			for (; i < values.length; i++) {
				stream.format("Row %2d: ", i);
				/** @type {number} */
				var j = 0;
				for (; j < values[i].length; j++) {
					var s = values[i][j];
					if (0 === s.getValue().length) {
						stream.format("        ", null);
					} else {
						stream.format("%4d(%2d)", s.getValue()[0], s.getConfidence(s.getValue()[0]));
					}
				}
				stream.format("%n");
			}
			return stream.toString();
		}
	}]);
	return exports;
}();
/** @type {number} */
ctx.CODEWORD_SKEW_SIZE = 2;
/** @type {number} */
ctx.MAX_ERRORS = 3;
/** @type {number} */
ctx.MAX_EC_CODEWORDS = 512;
ctx.errorCorrection = new EventEmitter;
var Event = function () {
	/**
	 * @return {undefined}
	 */
	function self() {
		_classCallCheck2(this, self);
	}
	_createClass2(self, [{
		key: "decode",
		value: function decode(context) {
			var size = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
			var coord2 = self.decode(context, size, false);
			if (null == coord2 || 0 === coord2.length || null == coord2[0]) {
				throw TypeError.getNotFoundInstance();
			}
			return coord2[0];
		}
	}, {
		key: "decodeMultiple",
		value: function tryDecode(data) {
			var iterator = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
			try {
				return self.decode(data, iterator, true);
			} catch (value) {
				if (value instanceof Date || value instanceof Rectangle) {
					throw TypeError.getNotFoundInstance();
				}
				throw value;
			}
		}
	}, {
		key: "reset",
		value: function reset() {
		}
	}], [{
		key: "decode",
		value: function create(url, data, type) {
			/** @type {!Array} */
			var n = new Array;
			var entity = queue.detectMultiple(url, data, type);
			/** @type {boolean} */
			var _iteratorNormalCompletion3 = true;
			/** @type {boolean} */
			var _didIteratorError67 = false;
			var _iteratorError17 = undefined;
			try {
				var _iterator3 = entity.getPoints()[Symbol.iterator]();
				var item;
				for (; !(_iteratorNormalCompletion3 = (item = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var points = item.value;
					var pro = ctx.decode(entity.getBits(), points[4], points[5], points[6], points[7], self.getMinCodewordWidth(points), self.getMaxCodewordWidth(points));
					var r = new result(pro.getText(), pro.getRawBytes(), void 0, points, change.PDF_417);
					r.putMetadata(value.ERROR_CORRECTION_LEVEL, pro.getECLevel());
					var nextMinID = pro.getOther();
					if (null != nextMinID) {
						r.putMetadata(value.PDF417_EXTRA_METADATA, nextMinID);
					}
					n.push(r);
				}
			} catch (err) {
				/** @type {boolean} */
				_didIteratorError67 = true;
				_iteratorError17 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError67) {
						throw _iteratorError17;
					}
				}
			}
			return n.map(function (conditionalValues) {
				return conditionalValues;
			});
		}
	}, {
		key: "getMaxWidth",
		value: function verify(mmCoreNotDownloaded, mmCoreOutdated) {
			return null == mmCoreNotDownloaded || null == mmCoreOutdated ? 0 : Math.trunc(Math.abs(mmCoreNotDownloaded.getX() - mmCoreOutdated.getX()));
		}
	}, {
		key: "getMinWidth",
		value: function assertNear(mmCoreNotDownloaded, mmCoreOutdated) {
			return null == mmCoreNotDownloaded || null == mmCoreOutdated ? scope.MAX_VALUE : Math.trunc(Math.abs(mmCoreNotDownloaded.getX() - mmCoreOutdated.getX()));
		}
	}, {
		key: "getMaxCodewordWidth",
		value: function render(p) {
			return Math.floor(Math.max(Math.max(self.getMaxWidth(p[0], p[4]), self.getMaxWidth(p[6], p[2]) * that.MODULES_IN_CODEWORD / that.MODULES_IN_STOP_PATTERN), Math.max(self.getMaxWidth(p[1], p[5]), self.getMaxWidth(p[7], p[3]) * that.MODULES_IN_CODEWORD / that.MODULES_IN_STOP_PATTERN)));
		}
	}, {
		key: "getMinCodewordWidth",
		value: function _getBackoffDelay(p) {
			return Math.floor(Math.min(Math.min(self.getMinWidth(p[0], p[4]), self.getMinWidth(p[6], p[2]) * that.MODULES_IN_CODEWORD / that.MODULES_IN_STOP_PATTERN), Math.min(self.getMinWidth(p[1], p[5]), self.getMinWidth(p[7], p[3]) * that.MODULES_IN_CODEWORD / that.MODULES_IN_STOP_PATTERN)));
		}
	}]);
	return self;
}();
var f = function (_WebInspector$GeneralTreeElement) {
	/**
	 * @return {?}
	 */
	function CacheLink() {
		_classCallCheck2(this, CacheLink);
		return _possibleConstructorReturn(this, (CacheLink.__proto__ || Object.getPrototypeOf(CacheLink)).apply(this, arguments));
	}
	_inherits(CacheLink, _WebInspector$GeneralTreeElement);
	return CacheLink;
}(input);
/** @type {string} */
f.kind = "ReaderException";
var SearchFiles = function () {
	/**
	 * @param {(number|string)} options
	 * @return {undefined}
	 */
	function TempusDominusBootstrap3(options) {
		_classCallCheck2(this, TempusDominusBootstrap3);
		/** @type {boolean} */
		this.verbose = true === options;
	}
	_createClass2(TempusDominusBootstrap3, [{
		key: "decode",
		value: function MoveSpies(e, tab) {
			return this.setHints(tab), this.decodeInternal(e);
		}
	}, {
		key: "decodeWithState",
		value: function forget(name) {
			return null !== this.readers && void 0 !== this.readers || this.setHints(null), this.decodeInternal(name);
		}
	}, {
		key: "setHints",
		value: function init(type) {
			/** @type {!Object} */
			this.hints = type;
			/** @type {boolean} */
			var $nxtCollapsed = null != type && void 0 !== type.get(node.TRY_HARDER);
			var _ = null == type ? null : type.get(node.POSSIBLE_FORMATS);
			/** @type {!Array} */
			var result = new Array;
			if (null != _) {
				var $nxtCollasible = _.some(function (i) {
					return i === change.UPC_A || i === change.UPC_E || i === change.EAN_13 || i === change.EAN_8 || i === change.CODABAR || i === change.CODE_39 || i === change.CODE_93 || i === change.CODE_128 || i === change.ITF || i === change.RSS_14 || i === change.RSS_EXPANDED;
				});
				if ($nxtCollasible && !$nxtCollapsed) {
					result.push(new Value(type, this.verbose));
				}
				if (_.includes(change.QR_CODE)) {
					result.push(new Message);
				}
				if (_.includes(change.DATA_MATRIX)) {
					result.push(new Model);
				}
				if (_.includes(change.AZTEC)) {
					result.push(new Rule);
				}
				if (_.includes(change.PDF_417)) {
					result.push(new Event);
				}
				if ($nxtCollasible && $nxtCollapsed) {
					result.push(new Value(type, this.verbose));
				}
			}
			if (0 === result.length) {
				if (!$nxtCollapsed) {
					result.push(new Value(type, this.verbose));
				}
				result.push(new Message);
				result.push(new Model);
				result.push(new Rule);
				result.push(new Event);
				if ($nxtCollapsed) {
					result.push(new Value(type, this.verbose));
				}
			}
			/** @type {!Array} */
			this.readers = result;
		}
	}, {
		key: "reset",
		value: function _destroyWaypoints() {
			if (null !== this.readers) {
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError68 = false;
				var _iteratorError17 = undefined;
				try {
					var _iterator3 = this.readers[Symbol.iterator]();
					var $__6;
					for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var item = $__6.value;
						item.reset();
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError68 = true;
					_iteratorError17 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError68) {
							throw _iteratorError17;
						}
					}
				}
			}
		}
	}, {
		key: "decodeInternal",
		value: function render(pAST) {
			if (null === this.readers) {
				throw new f("No readers where selected, nothing can be read.");
			}
			/** @type {boolean} */
			var _iteratorNormalCompletion3 = true;
			/** @type {boolean} */
			var _didIteratorError69 = false;
			var _iteratorError17 = undefined;
			try {
				var _iterator3 = this.readers[Symbol.iterator]();
				var $__6;
				for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var JSON = $__6.value;
					try {
						return JSON.decode(pAST, this.hints);
					} catch (g) {
						if (g instanceof f) {
							continue;
						}
					}
				}
			} catch (err) {
				/** @type {boolean} */
				_didIteratorError69 = true;
				_iteratorError17 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError69) {
						throw _iteratorError17;
					}
				}
			}
			throw new TypeError("No MultiFormat Readers were able to detect the code.");
		}
	}]);
	return TempusDominusBootstrap3;
}();
var first;
!function (styles) {
	/** @type {string} */
	styles[styles.ERROR_CORRECTION = 0] = "ERROR_CORRECTION";
	/** @type {string} */
	styles[styles.CHARACTER_SET = 1] = "CHARACTER_SET";
	/** @type {string} */
	styles[styles.DATA_MATRIX_SHAPE = 2] = "DATA_MATRIX_SHAPE";
	/** @type {string} */
	styles[styles.MIN_SIZE = 3] = "MIN_SIZE";
	/** @type {string} */
	styles[styles.MAX_SIZE = 4] = "MAX_SIZE";
	/** @type {string} */
	styles[styles.MARGIN = 5] = "MARGIN";
	/** @type {string} */
	styles[styles.PDF417_COMPACT = 6] = "PDF417_COMPACT";
	/** @type {string} */
	styles[styles.PDF417_COMPACTION = 7] = "PDF417_COMPACTION";
	/** @type {string} */
	styles[styles.PDF417_DIMENSIONS = 8] = "PDF417_DIMENSIONS";
	/** @type {string} */
	styles[styles.AZTEC_LAYERS = 9] = "AZTEC_LAYERS";
	/** @type {string} */
	styles[styles.QR_VERSION = 10] = "QR_VERSION";
}(first || (first = {}));
/** @type {(undefined|{AZTEC_LAYERS: number, CHARACTER_SET: number, DATA_MATRIX_SHAPE: number, ERROR_CORRECTION: number, MARGIN: number, MAX_SIZE: number, MIN_SIZE: number, PDF417_COMPACT: number, PDF417_COMPACTION: number, PDF417_DIMENSIONS: number, QR_VERSION: number})} */
var point = first;
var Test = function () {
	/**
	 * @param {string} element
	 * @return {undefined}
	 */
	function TempusDominusBootstrap3(element) {
		_classCallCheck2(this, TempusDominusBootstrap3);
		/** @type {string} */
		this.field = element;
		/** @type {!Array} */
		this.cachedGenerators = [];
		this.cachedGenerators.push(new Node(element, Int32Array.from([1])));
	}
	_createClass2(TempusDominusBootstrap3, [{
		key: "buildGenerator",
		value: function _treeInsert(index) {
			var attributes = this.cachedGenerators;
			if (index >= attributes.length) {
				var node = attributes[attributes.length - 1];
				var b = this.field;
				var length = attributes.length;
				for (; length <= index; length++) {
					var list = node.multiply(new Node(b, Int32Array.from([1, b.exp(length - 1 + b.getGeneratorBase())])));
					attributes.push(list);
					node = list;
				}
			}
			return attributes[index];
		}
	}, {
		key: "encode",
		value: function init(buffer, i) {
			if (0 === i) {
				throw new Function("No error correction bytes");
			}
			/** @type {number} */
			var index = buffer.length - i;
			if (index <= 0) {
				throw new Function("No data bytes provided");
			}
			var operand1 = this.buildGenerator(i);
			/** @type {!Int32Array} */
			var result = new Int32Array(index);
			System.arraycopy(buffer, 0, result, 0, index);
			var tmp = new Node(this.field, result);
			var b = (tmp = tmp.multiplyByMonomial(i, 1)).divide(operand1)[1].getCoefficients();
			/** @type {number} */
			var start = i - b.length;
			/** @type {number} */
			var len = 0;
			for (; len < start; len++) {
				/** @type {number} */
				buffer[index + len] = 0;
			}
			System.arraycopy(b, 0, buffer, index + start, b.length);
		}
	}]);
	return TempusDominusBootstrap3;
}();
var MaskUtil = function () {
	/**
	 * @return {undefined}
	 */
	function _() {
		_classCallCheck2(this, _);
	}
	_createClass2(_, null, [{
		key: "applyMaskPenaltyRule1",
		value: function applyMaskPenaltyRule1(matrix) {
			return _.applyMaskPenaltyRule1Internal(matrix, true) + _.applyMaskPenaltyRule1Internal(matrix, false);
		}
	}, {
		key: "applyMaskPenaltyRule2",
		value: function draw(frame) {
			/** @type {number} */
			var penalty = 0;
			var p = frame.getArray();
			var n = frame.getWidth();
			var seqLen = frame.getHeight();
			/** @type {number} */
			var j = 0;
			for (; j < seqLen - 1; j++) {
				var a = p[j];
				/** @type {number} */
				var i = 0;
				for (; i < n - 1; i++) {
					var x = a[i];
					if (x === a[i + 1] && x === p[j + 1][i] && x === p[j + 1][i + 1]) {
						penalty++;
					}
				}
			}
			return _.N2 * penalty;
		}
	}, {
		key: "applyMaskPenaltyRule3",
		value: function draw(frame) {
			/** @type {number} */
			var points = 0;
			var element = frame.getArray();
			var lineCount = frame.getWidth();
			var nOfMails = frame.getHeight();
			/** @type {number} */
			var i = 0;
			for (; i < nOfMails; i++) {
				/** @type {number} */
				var index = 0;
				for (; index < lineCount; index++) {
					var f = element[i];
					if (index + 6 < lineCount && 1 === f[index] && 0 === f[index + 1] && 1 === f[index + 2] && 1 === f[index + 3] && 1 === f[index + 4] && 0 === f[index + 5] && 1 === f[index + 6] && (_.isWhiteHorizontal(f, index - 4, index) || _.isWhiteHorizontal(f, index + 7, index + 11))) {
						points++;
					}
					if (i + 6 < nOfMails && 1 === element[i][index] && 0 === element[i + 1][index] && 1 === element[i + 2][index] && 1 === element[i + 3][index] && 1 === element[i + 4][index] && 0 === element[i + 5][index] && 1 === element[i + 6][index] && (_.isWhiteVertical(element, index, i - 4, i) || _.isWhiteVertical(element, index, i + 7, i + 11))) {
						points++;
					}
				}
			}
			return points * _.N3;
		}
	}, {
		key: "isWhiteHorizontal",
		value: function initialize(pad, r, x) {
			/** @type {number} */
			r = Math.max(r, 0);
			/** @type {number} */
			x = Math.min(x, pad.length);
			/** @type {number} */
			var i = r;
			for (; i < x; i++) {
				if (1 === pad[i]) {
					return false;
				}
			}
			return true;
		}
	}, {
		key: "isWhiteVertical",
		value: function pointNearEllipse(coords, type, radius, length) {
			/** @type {number} */
			radius = Math.max(radius, 0);
			/** @type {number} */
			length = Math.min(length, coords.length);
			/** @type {number} */
			var a = radius;
			for (; a < length; a++) {
				if (1 === coords[a][type]) {
					return false;
				}
			}
			return true;
		}
	}, {
		key: "applyMaskPenaltyRule4",
		value: function draw(frame) {
			/** @type {number} */
			var tileHScale = 0;
			var nextIdLookup = frame.getArray();
			var rown = frame.getWidth();
			var i = frame.getHeight();
			/** @type {number} */
			var indexLookupKey = 0;
			for (; indexLookupKey < i; indexLookupKey++) {
				var currentIndex = nextIdLookup[indexLookupKey];
				/** @type {number} */
				var j = 0;
				for (; j < rown; j++) {
					if (1 === currentIndex[j]) {
						tileHScale++;
					}
				}
			}
			/** @type {number} */
			var height = frame.getHeight() * frame.getWidth();
			return Math.floor(10 * Math.abs(2 * tileHScale - height) / height) * _.N4;
		}
	}, {
		key: "getDataMaskBit",
		value: function init(text, y, x) {
			var yInBlock = void 0;
			var xmy = void 0;
			switch (text) {
				case 0:
					/** @type {number} */
					yInBlock = x + y & 1;
					break;
				case 1:
					/** @type {number} */
					yInBlock = 1 & x;
					break;
				case 2:
					/** @type {number} */
					yInBlock = y % 3;
					break;
				case 3:
					/** @type {number} */
					yInBlock = (x + y) % 3;
					break;
				case 4:
					/** @type {number} */
					yInBlock = Math.floor(x / 2) + Math.floor(y / 3) & 1;
					break;
				case 5:
					/** @type {number} */
					yInBlock = (1 & (xmy = x * y)) + xmy % 3;
					break;
				case 6:
					/** @type {number} */
					yInBlock = (1 & (xmy = x * y)) + xmy % 3 & 1;
					break;
				case 7:
					/** @type {number} */
					yInBlock = (xmy = x * y) % 3 + (x + y & 1) & 1;
					break;
				default:
					throw new Function("Invalid mask pattern: " + text);
			}
			return 0 === yInBlock;
		}
	}, {
		key: "applyMaskPenaltyRule1Internal",
		value: function draw(matrix, transform) {
			/** @type {number} */
			var selectBox = 0;
			var offsetX = transform ? matrix.getHeight() : matrix.getWidth();
			var max = transform ? matrix.getWidth() : matrix.getHeight();
			var XXt = matrix.getArray();
			/** @type {number} */
			var i = 0;
			for (; i < offsetX; i++) {
				/** @type {number} */
				var numSameBitCells = 0;
				/** @type {number} */
				var interestingPoint = -1;
				/** @type {number} */
				var j = 0;
				for (; j < max; j++) {
					var viewportCenter = transform ? XXt[i][j] : XXt[j][i];
					if (viewportCenter === interestingPoint) {
						numSameBitCells++;
					} else {
						if (numSameBitCells >= 5) {
							selectBox = selectBox + (_.N1 + (numSameBitCells - 5));
						}
						/** @type {number} */
						numSameBitCells = 1;
						interestingPoint = viewportCenter;
					}
				}
				if (numSameBitCells >= 5) {
					selectBox = selectBox + (_.N1 + (numSameBitCells - 5));
				}
			}
			return selectBox;
		}
	}]);
	return _;
}();
/** @type {number} */
MaskUtil.N1 = 3;
/** @type {number} */
MaskUtil.N2 = 3;
/** @type {number} */
MaskUtil.N3 = 40;
/** @type {number} */
MaskUtil.N4 = 10;
var ByteMatrix = function () {
	/**
	 * @param {number} width
	 * @param {number} height
	 * @return {undefined}
	 */
	function updateSize(width, height) {
		_classCallCheck2(this, updateSize);
		/** @type {number} */
		this.width = width;
		/** @type {number} */
		this.height = height;
		/** @type {!Array} */
		var newNodes = new Array(height);
		/** @type {number} */
		var i = 0;
		for (; i !== height; i++) {
			/** @type {!Uint8Array} */
			newNodes[i] = new Uint8Array(width);
		}
		/** @type {!Array} */
		this.bytes = newNodes;
	}
	_createClass2(updateSize, [{
		key: "getHeight",
		value: function get_height() {
			return this.height;
		}
	}, {
		key: "getWidth",
		value: function get_width() {
			return this.width;
		}
	}, {
		key: "get",
		value: function getObjects(s, p) {
			return this.bytes[p][s];
		}
	}, {
		key: "getArray",
		value: function inputFilter() {
			return this.bytes;
		}
	}, {
		key: "setNumber",
		value: function setter(m, n, fn) {
			this.bytes[n][m] = fn;
		}
	}, {
		key: "setBoolean",
		value: function $fz(m, n, froot) {
			/** @type {number} */
			this.bytes[n][m] = froot ? 1 : 0;
		}
	}, {
		key: "clear",
		value: function _destroyWaypoints(encoding) {
			/** @type {boolean} */
			var _iteratorNormalCompletion3 = true;
			/** @type {boolean} */
			var _didIteratorError70 = false;
			var _iteratorError17 = undefined;
			try {
				var _iterator3 = this.bytes[Symbol.iterator]();
				var $__6;
				for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var item = $__6.value;
					b.fill(item, encoding);
				}
			} catch (err) {
				/** @type {boolean} */
				_didIteratorError70 = true;
				_iteratorError17 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError70) {
						throw _iteratorError17;
					}
				}
			}
		}
	}, {
		key: "equals",
		value: function handlePalette(boardManager) {
			if (!(boardManager instanceof updateSize)) {
				return false;
			}
			/** @type {string} */
			var pixelRect = boardManager;
			if (this.width !== pixelRect.width) {
				return false;
			}
			if (this.height !== pixelRect.height) {
				return false;
			}
			/** @type {number} */
			var test_word = 0;
			var SH = this.height;
			for (; test_word < SH; ++test_word) {
				var word = this.bytes[test_word];
				var previousWord = pixelRect.bytes[test_word];
				/** @type {number} */
				var i = 0;
				var sW = this.width;
				for (; i < sW; ++i) {
					if (word[i] !== previousWord[i]) {
						return false;
					}
				}
			}
			return true;
		}
	}, {
		key: "toString",
		value: function toString() {
			var t = new Buffer;
			/** @type {number} */
			var j = 0;
			var ch = this.height;
			for (; j < ch; ++j) {
				var word = this.bytes[j];
				/** @type {number} */
				var i = 0;
				var sW = this.width;
				for (; i < sW; ++i) {
					switch (word[i]) {
						case 0:
							t.append(" 0");
							break;
						case 1:
							t.append(" 1");
							break;
						default:
							t.append("  ");
					}
				}
				t.append("\n");
			}
			return t.toString();
		}
	}]);
	return updateSize;
}();
var QRCode = function () {
	/**
	 * @return {undefined}
	 */
	function TempusDominusBootstrap3() {
		_classCallCheck2(this, TempusDominusBootstrap3);
		/** @type {number} */
		this.maskPattern = -1;
	}
	_createClass2(TempusDominusBootstrap3, [{
		key: "getMode",
		value: function Engine_mode() {
			return this.mode;
		}
	}, {
		key: "getECLevel",
		value: function getECLevel() {
			return this.ecLevel;
		}
	}, {
		key: "getVersion",
		value: function version() {
			return this.version;
		}
	}, {
		key: "getMaskPattern",
		value: function getMaskPattern() {
			return this.maskPattern;
		}
	}, {
		key: "getMatrix",
		value: function update() {
			return this.matrix;
		}
	}, {
		key: "toString",
		value: function toString() {
			var out = new Buffer;
			return out.append("<<\n"), out.append(" mode: "), out.append(this.mode ? this.mode.toString() : "null"), out.append("\n ecLevel: "), out.append(this.ecLevel ? this.ecLevel.toString() : "null"), out.append("\n version: "), out.append(this.version ? this.version.toString() : "null"), out.append("\n maskPattern: "), out.append(this.maskPattern.toString()), this.matrix ? (out.append("\n matrix:\n"), out.append(this.matrix.toString())) : out.append("\n matrix: null\n"), out.append(">>\n"), out.toString();
		}
	}, {
		key: "setMode",
		value: function execute(vcs) {
			/** @type {string} */
			this.mode = vcs;
		}
	}, {
		key: "setECLevel",
		value: function setECLevel(value) {
			/** @type {string} */
			this.ecLevel = value;
		}
	}, {
		key: "setVersion",
		value: function UpdatedPackage(version) {
			/** @type {string} */
			this.version = version;
		}
	}, {
		key: "setMaskPattern",
		value: function setMaskPattern(value) {
			/** @type {number} */
			this.maskPattern = value;
		}
	}, {
		key: "setMatrix",
		value: function LConvolutionFilter(matrix) {
			/** @type {!Object} */
			this.matrix = matrix;
		}
	}], [{
		key: "isValidMaskPattern",
		value: function makeImpl(maskPattern) {
			return maskPattern >= 0 && maskPattern < TempusDominusBootstrap3.NUM_MASK_PATTERNS;
		}
	}]);
	return TempusDominusBootstrap3;
}();
/** @type {number} */
QRCode.NUM_MASK_PATTERNS = 8;
var RegExp = function (_WebInspector$GeneralTreeElement) {
	/**
	 * @return {?}
	 */
	function CacheLink() {
		_classCallCheck2(this, CacheLink);
		return _possibleConstructorReturn(this, (CacheLink.__proto__ || Object.getPrototypeOf(CacheLink)).apply(this, arguments));
	}
	_inherits(CacheLink, _WebInspector$GeneralTreeElement);
	return CacheLink;
}(input);
/** @type {string} */
RegExp.kind = "WriterException";
var MatrixUtil = function () {
	/**
	 * @return {undefined}
	 */
	function _() {
		_classCallCheck2(this, _);
	}
	_createClass2(_, null, [{
		key: "clearMatrix",
		value: function clickWithWebdriver(selector) {
			selector.clear(255);
		}
	}, {
		key: "buildMatrix",
		value: function buildMatrix(dataBits, ecLevel, version, maskPattern, matrix) {
			_.clearMatrix(matrix);
			_.embedBasicPatterns(version, matrix);
			_.embedTypeInfo(ecLevel, maskPattern, matrix);
			_.maybeEmbedVersionInfo(version, matrix);
			_.embedDataBits(dataBits, maskPattern, matrix);
		}
	}, {
		key: "embedBasicPatterns",
		value: function embedBasicPatterns(version, matrix) {
			_.embedPositionDetectionPatternsAndSeparators(matrix);
			_.embedDarkDotAtLeftBottomCorner(matrix);
			_.maybeEmbedPositionAdjustmentPatterns(version, matrix);
			_.embedTimingPatterns(matrix);
		}
	}, {
		key: "embedTypeInfo",
		value: function parse(ecLevel, version, res) {
			var typeInfoBits = new BitArray;
			_.makeTypeInfoBits(ecLevel, version, typeInfoBits);
			/** @type {number} */
			var i = 0;
			var cell_amount = typeInfoBits.getSize();
			for (; i < cell_amount; ++i) {
				var sizeRes = typeInfoBits.get(typeInfoBits.getSize() - 1 - i);
				var bs = _.TYPE_INFO_COORDINATES[i];
				var value = bs[0];
				var b = bs[1];
				if (res.setBoolean(value, b, sizeRes), i < 8) {
					/** @type {number} */
					var value = res.getWidth() - i - 1;
					/** @type {number} */
					var b = 8;
					res.setBoolean(value, b, sizeRes);
				} else {
					/** @type {number} */
					var value = 8;
					/** @type {number} */
					var b = res.getHeight() - 7 + (i - 8);
					res.setBoolean(value, b, sizeRes);
				}
			}
		}
	}, {
		key: "maybeEmbedVersionInfo",
		value: function parse(version, res) {
			if (version.getVersionNumber() < 7) {
				return;
			}
			var versionInfoBits = new BitArray;
			_.makeVersionInfoBits(version, versionInfoBits);
			/** @type {number} */
			var bitIndex = 17;
			/** @type {number} */
			var value = 0;
			for (; value < 6; ++value) {
				/** @type {number} */
				var appId = 0;
				for (; appId < 3; ++appId) {
					var sizeRes = versionInfoBits.get(bitIndex);
					bitIndex--;
					res.setBoolean(value, res.getHeight() - 11 + appId, sizeRes);
					res.setBoolean(res.getHeight() - 11 + appId, value, sizeRes);
				}
			}
		}
	}, {
		key: "embedDataBits",
		value: function menuitem_reset_icon_position(icon, maskPattern, matrix) {
			/** @type {number} */
			var state = 0;
			/** @type {number} */
			var bt = -1;
			/** @type {number} */
			var vpw = matrix.getWidth() - 1;
			/** @type {number} */
			var y = matrix.getHeight() - 1;
			for (; vpw > 0;) {
				if (6 === vpw) {
					/** @type {number} */
					vpw = vpw - 1;
				}
				for (; y >= 0 && y < matrix.getHeight();) {
					/** @type {number} */
					var cvpw = 0;
					for (; cvpw < 2; ++cvpw) {
						/** @type {number} */
						var x = vpw - cvpw;
						if (!_.isEmpty(matrix.get(x, y))) {
							continue;
						}
						var up = void 0;
						if (state < icon.getSize()) {
							up = icon.get(state);
							++state;
						} else {
							/** @type {boolean} */
							up = false;
						}
						if (255 !== maskPattern && MaskUtil.getDataMaskBit(maskPattern, x, y)) {
							/** @type {boolean} */
							up = !up;
						}
						matrix.setBoolean(x, y, up);
					}
					/** @type {number} */
					y = y + bt;
				}
				/** @type {number} */
				y = y + (bt = -bt);
				/** @type {number} */
				vpw = vpw - 2;
			}
			if (state !== icon.getSize()) {
				throw new RegExp("Not all bits consumed: " + state + "/" + icon.getSize());
			}
		}
	}, {
		key: "findMSBSet",
		value: function computeUnsignedVIntSize(value) {
			return 32 - scope.numberOfLeadingZeros(value);
		}
	}, {
		key: "calculateBCHCode",
		value: function initialize(value, poly) {
			if (0 === poly) {
				throw new Function("0 polynomial");
			}
			var msbSetInPoly = _.findMSBSet(poly);
			/** @type {number} */
			value = value << msbSetInPoly - 1;
			for (; _.findMSBSet(value) >= msbSetInPoly;) {
				/** @type {number} */
				value = value ^ poly << _.findMSBSet(value) - msbSetInPoly;
			}
			return value;
		}
	}, {
		key: "makeTypeInfoBits",
		value: function makeTypeInfoBits(ecLevel, maskPattern, bits) {
			if (!QRCode.isValidMaskPattern(maskPattern)) {
				throw new RegExp("Invalid mask pattern");
			}
			/** @type {number} */
			var typeInfo = ecLevel.getBits() << 3 | maskPattern;
			bits.appendBits(typeInfo, 5);
			var bchCode = _.calculateBCHCode(typeInfo, _.TYPE_INFO_POLY);
			bits.appendBits(bchCode, 10);
			var maskBits = new BitArray;
			if (maskBits.appendBits(_.TYPE_INFO_MASK_PATTERN, 15), bits.xor(maskBits), 15 !== bits.getSize()) {
				throw new RegExp("should not happen but we got: " + bits.getSize());
			}
		}
	}, {
		key: "makeVersionInfoBits",
		value: function appendLengthInfo(version, bits) {
			bits.appendBits(version.getVersionNumber(), 6);
			var bchCode = _.calculateBCHCode(version.getVersionNumber(), _.VERSION_INFO_POLY);
			if (bits.appendBits(bchCode, 12), 18 !== bits.getSize()) {
				throw new RegExp("should not happen but we got: " + bits.getSize());
			}
		}
	}, {
		key: "isEmpty",
		value: function isEmpty(excludeBr) {
			return 255 === excludeBr;
		}
	}, {
		key: "embedTimingPatterns",
		value: function createBinaries(res) {
			/** @type {number} */
			var i = 8;
			for (; i < res.getWidth() - 8; ++i) {
				/** @type {number} */
				var sizeRes = (i + 1) % 2;
				if (_.isEmpty(res.get(i, 6))) {
					res.setNumber(i, 6, sizeRes);
				}
				if (_.isEmpty(res.get(6, i))) {
					res.setNumber(6, i, sizeRes);
				}
			}
		}
	}, {
		key: "embedDarkDotAtLeftBottomCorner",
		value: function Birthdays(res) {
			if (0 === res.get(8, res.getHeight() - 8)) {
				throw new RegExp;
			}
			res.setNumber(8, res.getHeight() - 8, 1);
		}
	}, {
		key: "embedHorizontalSeparationPattern",
		value: function constructor(index, keys, self) {
			/** @type {number} */
			var i = 0;
			for (; i < 8; ++i) {
				if (!_.isEmpty(self.get(index + i, keys))) {
					throw new RegExp;
				}
				self.setNumber(index + i, keys, 0);
			}
		}
	}, {
		key: "embedVerticalSeparationPattern",
		value: function addToUsers(value, type, res) {
			/** @type {number} */
			var idx = 0;
			for (; idx < 7; ++idx) {
				if (!_.isEmpty(res.get(value, type + idx))) {
					throw new RegExp;
				}
				res.setNumber(value, type + idx, 0);
			}
		}
	}, {
		key: "embedPositionAdjustmentPattern",
		value: function embedHorizontalSeparationPattern(xStart, yStart, matrix) {
			/** @type {number} */
			var y = 0;
			for (; y < 5; ++y) {
				var v = _.POSITION_ADJUSTMENT_PATTERN[y];
				/** @type {number} */
				var x = 0;
				for (; x < 5; ++x) {
					matrix.setNumber(xStart + x, yStart + y, v[x]);
				}
			}
		}
	}, {
		key: "embedPositionDetectionPattern",
		value: function embedHorizontalSeparationPattern(xStart, yStart, matrix) {
			/** @type {number} */
			var y = 0;
			for (; y < 7; ++y) {
				var v = _.POSITION_DETECTION_PATTERN[y];
				/** @type {number} */
				var x = 0;
				for (; x < 7; ++x) {
					matrix.setNumber(xStart + x, yStart + y, v[x]);
				}
			}
		}
	}, {
		key: "embedPositionDetectionPatternsAndSeparators",
		value: function getNextElementSnapPoint(matrix) {
			var pdpWidth = _.POSITION_DETECTION_PATTERN[0].length;
			_.embedPositionDetectionPattern(0, 0, matrix);
			_.embedPositionDetectionPattern(matrix.getWidth() - pdpWidth, 0, matrix);
			_.embedPositionDetectionPattern(0, matrix.getWidth() - pdpWidth, matrix);
			_.embedHorizontalSeparationPattern(0, 7, matrix);
			_.embedHorizontalSeparationPattern(matrix.getWidth() - 8, 7, matrix);
			_.embedHorizontalSeparationPattern(0, matrix.getWidth() - 8, matrix);
			_.embedVerticalSeparationPattern(7, 0, matrix);
			_.embedVerticalSeparationPattern(matrix.getHeight() - 7 - 1, 0, matrix);
			_.embedVerticalSeparationPattern(7, matrix.getHeight() - 7, matrix);
		}
	}, {
		key: "maybeEmbedPositionAdjustmentPatterns",
		value: function rangeValid(version, matrix) {
			if (version.getVersionNumber() < 2) {
				return;
			}
			/** @type {number} */
			var index = version.getVersionNumber() - 1;
			var s = _.POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE[index];
			/** @type {number} */
			var i = 0;
			var l = s.length;
			for (; i !== l; i++) {
				var y = s[i];
				if (y >= 0) {
					/** @type {number} */
					var i = 0;
					for (; i !== l; i++) {
						var x = s[i];
						if (x >= 0 && _.isEmpty(matrix.get(x, y))) {
							_.embedPositionAdjustmentPattern(x - 2, y - 2, matrix);
						}
					}
				}
			}
		}
	}]);
	return _;
}();
/** @type {!Array<?>} */
MatrixUtil.POSITION_DETECTION_PATTERN = Array.from([Int32Array.from([1, 1, 1, 1, 1, 1, 1]), Int32Array.from([1, 0, 0, 0, 0, 0, 1]), Int32Array.from([1, 0, 1, 1, 1, 0, 1]), Int32Array.from([1, 0, 1, 1, 1, 0, 1]), Int32Array.from([1, 0, 1, 1, 1, 0, 1]), Int32Array.from([1, 0, 0, 0, 0, 0, 1]), Int32Array.from([1, 1, 1, 1, 1, 1, 1])]);
/** @type {!Array<?>} */
MatrixUtil.POSITION_ADJUSTMENT_PATTERN = Array.from([Int32Array.from([1, 1, 1, 1, 1]), Int32Array.from([1, 0, 0, 0, 1]), Int32Array.from([1, 0, 1, 0, 1]), Int32Array.from([1, 0, 0, 0, 1]), Int32Array.from([1, 1, 1, 1, 1])]);
/** @type {!Array<?>} */
MatrixUtil.POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE = Array.from([Int32Array.from([-1, -1, -1, -1, -1, -1, -1]), Int32Array.from([6, 18, -1, -1, -1, -1, -1]), Int32Array.from([6, 22, -1, -1, -1, -1, -1]), Int32Array.from([6, 26, -1, -1, -1, -1, -1]), Int32Array.from([6, 30, -1, -1, -1, -1, -1]), Int32Array.from([6, 34, -1, -1, -1, -1, -1]), Int32Array.from([6, 22, 38, -1, -1, -1, -1]), Int32Array.from([6, 24, 42, -1, -1, -1, -1]), Int32Array.from([6, 26, 46, -1, -1, -1, -1]), Int32Array.from([6,
	28, 50, -1, -1, -1, -1]), Int32Array.from([6, 30, 54, -1, -1, -1, -1]), Int32Array.from([6, 32, 58, -1, -1, -1, -1]), Int32Array.from([6, 34, 62, -1, -1, -1, -1]), Int32Array.from([6, 26, 46, 66, -1, -1, -1]), Int32Array.from([6, 26, 48, 70, -1, -1, -1]), Int32Array.from([6, 26, 50, 74, -1, -1, -1]), Int32Array.from([6, 30, 54, 78, -1, -1, -1]), Int32Array.from([6, 30, 56, 82, -1, -1, -1]), Int32Array.from([6, 30, 58, 86, -1, -1, -1]), Int32Array.from([6, 34, 62, 90, -1, -1, -1]), Int32Array.from([6,
		28, 50, 72, 94, -1, -1]), Int32Array.from([6, 26, 50, 74, 98, -1, -1]), Int32Array.from([6, 30, 54, 78, 102, -1, -1]), Int32Array.from([6, 28, 54, 80, 106, -1, -1]), Int32Array.from([6, 32, 58, 84, 110, -1, -1]), Int32Array.from([6, 30, 58, 86, 114, -1, -1]), Int32Array.from([6, 34, 62, 90, 118, -1, -1]), Int32Array.from([6, 26, 50, 74, 98, 122, -1]), Int32Array.from([6, 30, 54, 78, 102, 126, -1]), Int32Array.from([6, 26, 52, 78, 104, 130, -1]), Int32Array.from([6, 30, 56, 82, 108, 134, -1]), Int32Array.from([6,
			34, 60, 86, 112, 138, -1]), Int32Array.from([6, 30, 58, 86, 114, 142, -1]), Int32Array.from([6, 34, 62, 90, 118, 146, -1]), Int32Array.from([6, 30, 54, 78, 102, 126, 150]), Int32Array.from([6, 24, 50, 76, 102, 128, 154]), Int32Array.from([6, 28, 54, 80, 106, 132, 158]), Int32Array.from([6, 32, 58, 84, 110, 136, 162]), Int32Array.from([6, 26, 54, 82, 110, 138, 166]), Int32Array.from([6, 30, 58, 86, 114, 142, 170])]);
/** @type {!Array<?>} */
MatrixUtil.TYPE_INFO_COORDINATES = Array.from([Int32Array.from([8, 0]), Int32Array.from([8, 1]), Int32Array.from([8, 2]), Int32Array.from([8, 3]), Int32Array.from([8, 4]), Int32Array.from([8, 5]), Int32Array.from([8, 7]), Int32Array.from([8, 8]), Int32Array.from([7, 8]), Int32Array.from([5, 8]), Int32Array.from([4, 8]), Int32Array.from([3, 8]), Int32Array.from([2, 8]), Int32Array.from([1, 8]), Int32Array.from([0, 8])]);
/** @type {number} */
MatrixUtil.VERSION_INFO_POLY = 7973;
/** @type {number} */
MatrixUtil.TYPE_INFO_POLY = 1335;
/** @type {number} */
MatrixUtil.TYPE_INFO_MASK_PATTERN = 21522;
var BlockPair = function () {
	/**
	 * @param {?} data
	 * @param {?} options
	 * @return {undefined}
	 */
	function TempusDominusBootstrap3(data, options) {
		_classCallCheck2(this, TempusDominusBootstrap3);
		this.dataBytes = data;
		this.errorCorrectionBytes = options;
	}
	_createClass2(TempusDominusBootstrap3, [{
		key: "getDataBytes",
		value: function getDataBytes() {
			return this.dataBytes;
		}
	}, {
		key: "getErrorCorrectionBytes",
		value: function getErrorCorrectionBytes() {
			return this.errorCorrectionBytes;
		}
	}]);
	return TempusDominusBootstrap3;
}();
var $scope = function () {
	/**
	 * @return {undefined}
	 */
	function _() {
		_classCallCheck2(this, _);
	}
	_createClass2(_, null, [{
		key: "calculateMaskPenalty",
		value: function calculateMaskPenalty(matrix) {
			return MaskUtil.applyMaskPenaltyRule1(matrix) + MaskUtil.applyMaskPenaltyRule2(matrix) + MaskUtil.applyMaskPenaltyRule3(matrix) + MaskUtil.applyMaskPenaltyRule4(matrix);
		}
	}, {
		key: "encode",
		value: function encode(args, ecLevel) {
			var opt_hints = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
			var element = _.DEFAULT_BYTE_MODE_ENCODING;
			/** @type {boolean} */
			var parentIsInline = null !== opt_hints && void 0 !== opt_hints.get(point.CHARACTER_SET);
			if (parentIsInline) {
				element = opt_hints.get(point.CHARACTER_SET).toString();
			}
			var v = this.chooseMode(args, element);
			var headerBits = new BitArray;
			if (v === ModeEnum.BYTE && (parentIsInline || _.DEFAULT_BYTE_MODE_ENCODING !== element)) {
				var eventOption = options.getCharacterSetECIByName(element);
				if (void 0 !== eventOption) {
					this.appendECI(eventOption, headerBits);
				}
			}
			this.appendModeInfo(v, headerBits);
			var dataBits = new BitArray;
			var version = void 0;
			if (this.appendBytes(args, v, dataBits, element), null !== opt_hints && void 0 !== opt_hints.get(point.QR_VERSION)) {
				/** @type {number} */
				var provisionalVersion = Number.parseInt(opt_hints.get(point.QR_VERSION).toString(), 10);
				version = Version.getVersionForNumber(provisionalVersion);
				var ver = this.calculateBitsNeeded(v, headerBits, dataBits, version);
				if (!this.willFit(ver, version, ecLevel)) {
					throw new RegExp("Data too big for requested version");
				}
			} else {
				version = this.recommendVersion(ecLevel, v, headerBits, dataBits);
			}
			var headerAndDataBits = new BitArray;
			headerAndDataBits.appendBitArray(headerBits);
			var artistTrack = v === ModeEnum.BYTE ? dataBits.getSizeInBytes() : args.length;
			this.appendLengthInfo(artistTrack, version, v, headerAndDataBits);
			headerAndDataBits.appendBitArray(dataBits);
			var ecBlocks = version.getECBlocksForLevel(ecLevel);
			/** @type {number} */
			var numDataBytes = version.getTotalCodewords() - ecBlocks.getTotalECCodewords();
			this.terminateBits(numDataBytes, headerAndDataBits);
			var finalBits = this.interleaveWithECBytes(headerAndDataBits, version.getTotalCodewords(), numDataBytes, ecBlocks.getNumBlocks());
			var qrCode = new QRCode;
			qrCode.setECLevel(ecLevel);
			qrCode.setMode(v);
			qrCode.setVersion(version);
			var dimension = version.getDimensionForVersion();
			var matrix = new ByteMatrix(dimension, dimension);
			var maskPattern = this.chooseMaskPattern(finalBits, ecLevel, version, matrix);
			return qrCode.setMaskPattern(maskPattern), MatrixUtil.buildMatrix(finalBits, ecLevel, version, maskPattern, matrix), qrCode.setMatrix(matrix), qrCode;
		}
	}, {
		key: "recommendVersion",
		value: function recommendVersion(opts, name, version, pkg) {
			var src = this.calculateBitsNeeded(name, version, pkg, Version.getVersionForNumber(1));
			var index = this.chooseVersion(src, opts);
			var result = this.calculateBitsNeeded(name, version, pkg, index);
			return this.chooseVersion(result, opts);
		}
	}, {
		key: "calculateBitsNeeded",
		value: function positionDrawings(mode, inputDrawing, outputDrawing, version) {
			return inputDrawing.getSize() + mode.getCharacterCountBits(version) + outputDrawing.getSize();
		}
	}, {
		key: "getAlphanumericCode",
		value: function getAlphanumericCode(code) {
			return code < _.ALPHANUMERIC_TABLE.length ? _.ALPHANUMERIC_TABLE[code] : -1;
		}
	}, {
		key: "chooseMode",
		value: function read(input) {
			var lib$rsvp$$internal$$PENDING = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
			if (options.SJIS.getName() === lib$rsvp$$internal$$PENDING && this.isOnlyDoubleByteKanji(input)) {
				return ModeEnum.KANJI;
			}
			/** @type {boolean} */
			var rawDataIsList = false;
			/** @type {boolean} */
			var rawDataIsArray = false;
			/** @type {number} */
			var i = 0;
			var inputLen = input.length;
			for (; i < inputLen; ++i) {
				var chr = input.charAt(i);
				if (_.isDigit(chr)) {
					/** @type {boolean} */
					rawDataIsList = true;
				} else {
					if (-1 === this.getAlphanumericCode(chr.charCodeAt(0))) {
						return ModeEnum.BYTE;
					}
					/** @type {boolean} */
					rawDataIsArray = true;
				}
			}
			return rawDataIsArray ? ModeEnum.ALPHANUMERIC : rawDataIsList ? ModeEnum.NUMERIC : ModeEnum.BYTE;
		}
	}, {
		key: "isOnlyDoubleByteKanji",
		value: function $$validate(chunk) {
			var m = void 0;
			try {
				m = parser.encode(chunk, options.SJIS);
			} catch (t) {
				return false;
			}
			var s = m.length;
			if (s % 2 != 0) {
				return false;
			}
			/** @type {number} */
			var j = 0;
			for (; j < s; j = j + 2) {
				/** @type {number} */
				var _r125 = 255 & m[j];
				if ((_r125 < 129 || _r125 > 159) && (_r125 < 224 || _r125 > 235)) {
					return false;
				}
			}
			return true;
		}
	}, {
		key: "chooseMaskPattern",
		value: function chooseMaskPattern(bits, ecLevel, version, matrix) {
			/** @type {number} */
			var best_part_step_diff = Number.MAX_SAFE_INTEGER;
			/** @type {number} */
			var bestMaskPattern = -1;
			/** @type {number} */
			var maskPattern = 0;
			for (; maskPattern < QRCode.NUM_MASK_PATTERNS; maskPattern++) {
				MatrixUtil.buildMatrix(bits, ecLevel, version, maskPattern, matrix);
				var tmp_step_diff = this.calculateMaskPenalty(matrix);
				if (tmp_step_diff < best_part_step_diff) {
					best_part_step_diff = tmp_step_diff;
					/** @type {number} */
					bestMaskPattern = maskPattern;
				}
			}
			return bestMaskPattern;
		}
	}, {
		key: "chooseVersion",
		value: function locusOfChapter(src, e) {
			/** @type {number} */
			var provisionalVersion = 1;
			for (; provisionalVersion <= 40; provisionalVersion++) {
				var recordByTime = Version.getVersionForNumber(provisionalVersion);
				if (_.willFit(src, recordByTime, e)) {
					return recordByTime;
				}
			}
			throw new RegExp("Data too big");
		}
	}, {
		key: "willFit",
		value: function chooseMaskPattern(matrix, version, ecLevel) {
			return version.getTotalCodewords() - version.getECBlocksForLevel(ecLevel).getTotalECCodewords() >= (matrix + 7) / 8;
		}
	}, {
		key: "terminateBits",
		value: function terminateBits(numDataBytes, bits) {
			/** @type {number} */
			var item = 8 * numDataBytes;
			if (bits.getSize() > item) {
				throw new RegExp("data bits cannot fit in the QR Code" + bits.getSize() + " > " + item);
			}
			/** @type {number} */
			var sourceListIndex = 0;
			for (; sourceListIndex < 4 && bits.getSize() < item; ++sourceListIndex) {
				bits.appendBit(false);
			}
			/** @type {number} */
			var n = 7 & bits.getSize();
			if (n > 0) {
				/** @type {number} */
				var toFulfill = n;
				for (; toFulfill < 8; toFulfill++) {
					bits.appendBit(false);
				}
			}
			/** @type {number} */
			var numPaddingBytes = numDataBytes - bits.getSizeInBytes();
			/** @type {number} */
			var i = 0;
			for (; i < numPaddingBytes; ++i) {
				bits.appendBits(0 == (1 & i) ? 236 : 17, 8);
			}
			if (bits.getSize() !== item) {
				throw new RegExp("Bits size does not equal capacity");
			}
		}
	}, {
		key: "getNumDataBytesAndNumECBytesForBlockID",
		value: function init(count, index, len, start, data, array) {
			if (start >= len) {
				throw new RegExp("Block ID too large");
			}
			/** @type {number} */
			var j = count % len;
			/** @type {number} */
			var i = len - j;
			/** @type {number} */
			var w = Math.floor(count / len);
			/** @type {number} */
			var ax = w + 1;
			/** @type {number} */
			var width = Math.floor(index / len);
			/** @type {number} */
			var f = width + 1;
			/** @type {number} */
			var x = w - width;
			/** @type {number} */
			var q = ax - f;
			if (x !== q) {
				throw new RegExp("EC bytes mismatch");
			}
			if (len !== i + j) {
				throw new RegExp("RS blocks mismatch");
			}
			if (count !== (width + x) * i + (f + q) * j) {
				throw new RegExp("Total bytes mismatch");
			}
			if (start < i) {
				/** @type {number} */
				data[0] = width;
				/** @type {number} */
				array[0] = x;
			} else {
				/** @type {number} */
				data[0] = f;
				/** @type {number} */
				array[0] = q;
			}
		}
	}, {
		key: "interleaveWithECBytes",
		value: function encode(bits, numTotalBytes, numDataBytes, numRSBlocks) {
			if (bits.getSizeInBytes() !== numDataBytes) {
				throw new RegExp("Number of bits and data bytes does not match");
			}
			/** @type {number} */
			var dataBytesOffset = 0;
			/** @type {number} */
			var maxNumDataBytes = 0;
			/** @type {number} */
			var blue = 0;
			/** @type {!Array} */
			var blocks = new Array;
			/** @type {number} */
			var i = 0;
			for (; i < numRSBlocks; ++i) {
				/** @type {!Int32Array} */
				var numDataBytesInBlock = new Int32Array(1);
				/** @type {!Int32Array} */
				var numEcBytesInBlock = new Int32Array(1);
				_.getNumDataBytesAndNumECBytesForBlockID(numTotalBytes, numDataBytes, numRSBlocks, i, numDataBytesInBlock, numEcBytesInBlock);
				/** @type {number} */
				var size = numDataBytesInBlock[0];
				/** @type {!Uint8Array} */
				var dataBytes = new Uint8Array(size);
				bits.toBytes(8 * dataBytesOffset, dataBytes, 0, size);
				var ecBytes = _.generateECBytes(dataBytes, numEcBytesInBlock[0]);
				blocks.push(new BlockPair(dataBytes, ecBytes));
				/** @type {number} */
				maxNumDataBytes = Math.max(maxNumDataBytes, size);
				/** @type {number} */
				blue = Math.max(blue, ecBytes.length);
				/** @type {number} */
				dataBytesOffset = dataBytesOffset + numDataBytesInBlock[0];
			}
			if (numDataBytes !== dataBytesOffset) {
				throw new RegExp("Data bytes does not match offset");
			}
			var result = new BitArray;
			/** @type {number} */
			var ii = 0;
			for (; ii < maxNumDataBytes; ++ii) {
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError71 = false;
				var _iteratorError17 = undefined;
				try {
					var _iterator3 = blocks[Symbol.iterator]();
					var _step2;
					for (; !(_iteratorNormalCompletion3 = (_step2 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var block = _step2.value;
						var datas = block.getDataBytes();
						if (ii < datas.length) {
							result.appendBits(datas[ii], 8);
						}
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError71 = true;
					_iteratorError17 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError71) {
							throw _iteratorError17;
						}
					}
				}
			}
			/** @type {number} */
			var b = 0;
			for (; b < blue; ++b) {
				/** @type {boolean} */
				var _iteratorNormalCompletion3 = true;
				/** @type {boolean} */
				var _didIteratorError72 = false;
				var _iteratorError17 = undefined;
				try {
					var _iterator3 = blocks[Symbol.iterator]();
					var _step2;
					for (; !(_iteratorNormalCompletion3 = (_step2 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
						var block = _step2.value;
						var candidates = block.getErrorCorrectionBytes();
						if (b < candidates.length) {
							result.appendBits(candidates[b], 8);
						}
					}
				} catch (err) {
					/** @type {boolean} */
					_didIteratorError72 = true;
					_iteratorError17 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion3 && _iterator3.return) {
							_iterator3.return();
						}
					} finally {
						if (_didIteratorError72) {
							throw _iteratorError17;
						}
					}
				}
			}
			if (numTotalBytes !== result.getSizeInBytes()) {
				throw new RegExp("Interleaving error: " + numTotalBytes + " and " + result.getSizeInBytes() + " differ.");
			}
			return result;
		}
	}, {
		key: "generateECBytes",
		value: function parse(chunks, n) {
			var len = chunks.length;
			/** @type {!Int32Array} */
			var src = new Int32Array(len + n);
			/** @type {number} */
			var j = 0;
			for (; j < len; j++) {
				/** @type {number} */
				src[j] = 255 & chunks[j];
			}
			(new Test(r.QR_CODE_FIELD_256)).encode(src, n);
			/** @type {!Uint8Array} */
			var array = new Uint8Array(n);
			/** @type {number} */
			var i = 0;
			for (; i < n; i++) {
				/** @type {number} */
				array[i] = src[len + i];
			}
			return array;
		}
	}, {
		key: "appendModeInfo",
		value: function repeat(stream, value) {
			value.appendBits(stream.getBits(), 4);
		}
	}, {
		key: "appendLengthInfo",
		value: function appendLengthInfo(numLetters, version, mode, bits) {
			var numBits = mode.getCharacterCountBits(version);
			if (numLetters >= 1 << numBits) {
				throw new RegExp(numLetters + " is bigger than " + ((1 << numBits) - 1));
			}
			bits.appendBits(numLetters, numBits);
		}
	}, {
		key: "appendBytes",
		value: function getCapacity(content, version, bits, name) {
			switch (version) {
				case ModeEnum.NUMERIC:
					_.appendNumericBytes(content, bits);
					break;
				case ModeEnum.ALPHANUMERIC:
					_.appendAlphanumericBytes(content, bits);
					break;
				case ModeEnum.BYTE:
					_.append8BitBytes(content, bits, name);
					break;
				case ModeEnum.KANJI:
					_.appendKanjiBytes(content, bits);
					break;
				default:
					throw new RegExp("Invalid mode: " + version);
			}
		}
	}, {
		key: "getDigit",
		value: function findWebScrobblerEvent(needle) {
			return needle.charCodeAt(0) - 48;
		}
	}, {
		key: "isDigit",
		value: function generate(code) {
			var e = _.getDigit(code);
			return e >= 0 && e <= 9;
		}
	}, {
		key: "appendNumericBytes",
		value: function generate(value, bits) {
			var count = value.length;
			/** @type {number} */
			var i = 0;
			for (; i < count;) {
				var bchCode = _.getDigit(value.charAt(i));
				if (i + 2 < count) {
					var touch2Y = _.getDigit(value.charAt(i + 1));
					var code2 = _.getDigit(value.charAt(i + 2));
					bits.appendBits(100 * bchCode + 10 * touch2Y + code2, 10);
					/** @type {number} */
					i = i + 3;
				} else {
					if (i + 1 < count) {
						var code2 = _.getDigit(value.charAt(i + 1));
						bits.appendBits(10 * bchCode + code2, 7);
						/** @type {number} */
						i = i + 2;
					} else {
						bits.appendBits(bchCode, 4);
						i++;
					}
				}
			}
		}
	}, {
		key: "appendAlphanumericBytes",
		value: function isAccented(value, bits) {
			var count = value.length;
			/** @type {number} */
			var i = 0;
			for (; i < count;) {
				var bchCode = _.getAlphanumericCode(value.charCodeAt(i));
				if (-1 === bchCode) {
					throw new RegExp;
				}
				if (i + 1 < count) {
					var code2 = _.getAlphanumericCode(value.charCodeAt(i + 1));
					if (-1 === code2) {
						throw new RegExp;
					}
					bits.appendBits(45 * bchCode + code2, 11);
					/** @type {number} */
					i = i + 2;
				} else {
					bits.appendBits(bchCode, 6);
					i++;
				}
			}
		}
	}, {
		key: "append8BitBytes",
		value: function arrnest(code, result, name) {
			var res = void 0;
			try {
				res = parser.encode(code, name);
			} catch (expressionRegexSource) {
				throw new RegExp(expressionRegexSource);
			}
			/** @type {number} */
			var i = 0;
			var resl = res.length;
			for (; i !== resl; i++) {
				var b = res[i];
				result.appendBits(b, 8);
			}
		}
	}, {
		key: "appendKanjiBytes",
		value: function chaptersForComponent(data, bits) {
			var json = void 0;
			try {
				json = parser.encode(data, options.SJIS);
			} catch (expressionRegexSource) {
				throw new RegExp(expressionRegexSource);
			}
			var jsonLength = json.length;
			/** @type {number} */
			var i = 0;
			for (; i < jsonLength; i = i + 2) {
				/** @type {number} */
				var scrollBottom = (255 & json[i]) << 8 & 4294967295 | 255 & json[i + 1];
				/** @type {number} */
				var spaceAvailableBelow = -1;
				if (scrollBottom >= 33088 && scrollBottom <= 40956 ? spaceAvailableBelow = scrollBottom - 33088 : scrollBottom >= 57408 && scrollBottom <= 60351 && (spaceAvailableBelow = scrollBottom - 49472), -1 === spaceAvailableBelow) {
					throw new RegExp("Invalid byte sequence");
				}
				/** @type {number} */
				var bchCode = 192 * (spaceAvailableBelow >> 8) + (255 & spaceAvailableBelow);
				bits.appendBits(bchCode, 13);
			}
		}
	}, {
		key: "appendECI",
		value: function setTarget(t, v) {
			v.appendBits(ModeEnum.ECI.getBits(), 4);
			v.appendBits(t.getValue(), 8);
		}
	}]);
	return _;
}();
/** @type {!Int32Array} */
$scope.ALPHANUMERIC_TABLE = Int32Array.from([-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 36, -1, -1, -1, 37, 38, -1, -1, -1, -1, 39, 40, -1, 41, 42, 43, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 44, -1, -1, -1, -1, -1, -1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, -1, -1, -1, -1, -1]);
$scope.DEFAULT_BYTE_MODE_ENCODING = options.UTF8.getName();
var renderer = function () {
	/**
	 * @return {undefined}
	 */
	function renderer() {
		_classCallCheck2(this, renderer);
	}
	_createClass2(renderer, [{
		key: "write",
		value: function init(context, size, value) {
			var p = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
			if (0 === context.length) {
				throw new Function("Found empty contents");
			}
			if (size < 0 || value < 0) {
				throw new Function("Requested dimensions are too small: " + size + "x" + value);
			}
			var q = ErrorCorrectionLevel.L;
			var scrollTop = renderer.QUIET_ZONE_SIZE;
			if (null !== p) {
				if (void 0 !== p.get(point.ERROR_CORRECTION)) {
					q = ErrorCorrectionLevel.fromString(p.get(point.ERROR_CORRECTION).toString());
				}
				if (void 0 !== p.get(point.MARGIN)) {
					/** @type {number} */
					scrollTop = Number.parseInt(p.get(point.MARGIN).toString(), 10);
				}
			}
			var s = $scope.encode(context, q, p);
			return this.renderResult(s, size, value, scrollTop);
		}
	}, {
		key: "writeToDom",
		value: function onChildOutput(data, destination, type, args) {
			var context = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : null;
			if ("string" == typeof data) {
				/** @type {(Element|null)} */
				data = document.querySelector(data);
			}
			var output = this.write(destination, type, args, context);
			if (data) {
				data.appendChild(output);
			}
		}
	}, {
		key: "renderResult",
		value: function scale(o, max, n, p) {
			var el = o.getMatrix();
			if (null === el) {
				throw new Path;
			}
			var s = el.getWidth();
			var a = el.getHeight();
			var x = s + 2 * p;
			var d = a + 2 * p;
			/** @type {number} */
			var y = Math.max(max, x);
			/** @type {number} */
			var c = Math.max(n, d);
			/** @type {number} */
			var r = Math.min(Math.floor(y / x), Math.floor(c / d));
			/** @type {number} */
			var functionValuesX = Math.floor((y - s * r) / 2);
			/** @type {number} */
			var python_django = Math.floor((c - a * r) / 2);
			var ret = this.createSVGElement(y, c);
			/** @type {number} */
			var ch = 0;
			/** @type {number} */
			var name = python_django;
			for (; ch < a; ch++, name = name + r) {
				/** @type {number} */
				var j = 0;
				/** @type {number} */
				var x = functionValuesX;
				for (; j < s; j++, x = x + r) {
					if (1 === el.get(j, ch)) {
						var value = this.createSvgRectElement(x, name, r, r);
						ret.appendChild(value);
					}
				}
			}
			return ret;
		}
	}, {
		key: "createSVGElement",
		value: function createSvgImageElement(width, height) {
			/** @type {!Element} */
			var m = document.createElementNS(renderer.SVG_NS, "svg");
			return m.setAttributeNS(null, "height", width.toString()), m.setAttributeNS(null, "width", height.toString()), m;
		}
	}, {
		key: "createSvgRectElement",
		value: function initialize(rx, ry, offset, col) {
			/** @type {!Element} */
			var selRect = document.createElementNS(renderer.SVG_NS, "rect");
			return selRect.setAttributeNS(null, "x", rx.toString()), selRect.setAttributeNS(null, "y", ry.toString()), selRect.setAttributeNS(null, "height", offset.toString()), selRect.setAttributeNS(null, "width", col.toString()), selRect.setAttributeNS(null, "fill", "#000000"), selRect;
		}
	}]);
	return renderer;
}();
/** @type {number} */
renderer.QUIET_ZONE_SIZE = 4;
/** @type {string} */
renderer.SVG_NS = "http://www.w3.org/2000/svg";
var CollectionView = function () {
	/**
	 * @return {undefined}
	 */
	function me() {
		_classCallCheck2(this, me);
	}
	_createClass2(me, [{
		key: "encode",
		value: function init(s, value, m, n, p) {
			if (0 === s.length) {
				throw new Function("Found empty contents");
			}
			if (value !== change.QR_CODE) {
				throw new Function("Can only encode QR_CODE, but got " + value);
			}
			if (m < 0 || n < 0) {
				throw new Function("Requested dimensions are too small: " + m + "x" + n);
			}
			var q = ErrorCorrectionLevel.L;
			var oldChecked = me.QUIET_ZONE_SIZE;
			if (null !== p) {
				if (void 0 !== p.get(point.ERROR_CORRECTION)) {
					q = ErrorCorrectionLevel.fromString(p.get(point.ERROR_CORRECTION).toString());
				}
				if (void 0 !== p.get(point.MARGIN)) {
					/** @type {number} */
					oldChecked = Number.parseInt(p.get(point.MARGIN).toString(), 10);
				}
			}
			var result = $scope.encode(s, q, p);
			return me.renderResult(result, m, n, oldChecked);
		}
	}], [{
		key: "renderResult",
		value: function scale(actor, x, p, s) {
			var el = actor.getMatrix();
			if (null === el) {
				throw new Path;
			}
			var i = el.getWidth();
			var a = el.getHeight();
			var height = i + 2 * s;
			var d = a + 2 * s;
			/** @type {number} */
			var width = Math.max(x, height);
			/** @type {number} */
			var c = Math.max(p, d);
			/** @type {number} */
			var w = Math.min(Math.floor(width / height), Math.floor(c / d));
			/** @type {number} */
			var functionValuesX = Math.floor((width - i * w) / 2);
			/** @type {number} */
			var undefined = Math.floor((c - a * w) / 2);
			var canvas = new Image(width, c);
			/** @type {number} */
			var ch = 0;
			/** @type {number} */
			var line = undefined;
			for (; ch < a; ch++, line = line + w) {
				/** @type {number} */
				var j = 0;
				/** @type {number} */
				var x = functionValuesX;
				for (; j < i; j++, x = x + w) {
					if (1 === el.get(j, ch)) {
						canvas.setRegion(x, line, w, w);
					}
				}
			}
			return canvas;
		}
	}]);
	return me;
}();
/** @type {number} */
CollectionView.QUIET_ZONE_SIZE = 4;
var tip = function (_WebInspector$GeneralTreeElement) {
	/**
	 * @param {!Object} event
	 * @param {number} selector
	 * @param {!Object} length
	 * @param {number} text
	 * @param {number} index
	 * @param {number} data
	 * @param {number} count
	 * @param {?} timeStamp
	 * @return {?}
	 */
	function render(event, selector, length, text, index, data, count, timeStamp) {
		var _this;
		_classCallCheck2(this, render);
		if (_this = _possibleConstructorReturn(this, (render.__proto__ || Object.getPrototypeOf(render)).call(this, data, count)), _this, _this.yuvData = event, _this.dataWidth = selector, _this.dataHeight = length, _this.left = text, _this.top = index, text + data > selector || index + count > length) {
			throw new Function("Crop rectangle does not fit within image data.");
		}
		if (timeStamp) {
			_this.reverseHorizontal(data, count);
		}
		return _possibleConstructorReturn(_this);
	}
	_inherits(render, _WebInspector$GeneralTreeElement);
	_createClass2(render, [{
		key: "getRow",
		value: function render(value, result) {
			if (value < 0 || value >= this.getHeight()) {
				throw new Function("Requested row is outside the image: " + value);
			}
			var len = this.getWidth();
			if (null == result || result.length < len) {
				/** @type {!Uint8ClampedArray} */
				result = new Uint8ClampedArray(len);
			}
			var end = (value + this.top) * this.dataWidth + this.left;
			return System.arraycopy(this.yuvData, end, result, 0, len), result;
		}
	}, {
		key: "getMatrix",
		value: function render() {
			var w = this.getWidth();
			var h = this.getHeight();
			if (w === this.dataWidth && h === this.dataHeight) {
				return this.yuvData;
			}
			/** @type {number} */
			var len = w * h;
			/** @type {!Uint8ClampedArray} */
			var result = new Uint8ClampedArray(len);
			var off = this.top * this.dataWidth + this.left;
			if (w === this.dataWidth) {
				return System.arraycopy(this.yuvData, off, result, 0, len), result;
			}
			/** @type {number} */
			var i = 0;
			for (; i < h; i++) {
				/** @type {number} */
				var start = i * w;
				System.arraycopy(this.yuvData, off, result, start, w);
				off = off + this.dataWidth;
			}
			return result;
		}
	}, {
		key: "isCropSupported",
		value: function almost_equals() {
			return true;
		}
	}, {
		key: "crop",
		value: function render(dx, dy, angle, update) {
			return new render(this.yuvData, this.dataWidth, this.dataHeight, this.left + dx, this.top + dy, angle, update, false);
		}
	}, {
		key: "renderThumbnail",
		value: function render() {
			/** @type {number} */
			var width = this.getWidth() / render.THUMBNAIL_SCALE_FACTOR;
			/** @type {number} */
			var height = this.getHeight() / render.THUMBNAIL_SCALE_FACTOR;
			/** @type {!Int32Array} */
			var a = new Int32Array(width * height);
			var data2 = this.yuvData;
			var x2 = this.top * this.dataWidth + this.left;
			/** @type {number} */
			var y = 0;
			for (; y < height; y++) {
				/** @type {number} */
				var offset = y * width;
				/** @type {number} */
				var length = 0;
				for (; length < width; length++) {
					/** @type {number} */
					var _t212 = 255 & data2[x2 + length * render.THUMBNAIL_SCALE_FACTOR];
					/** @type {number} */
					a[offset + length] = 4278190080 | 65793 * _t212;
				}
				x2 = x2 + this.dataWidth * render.THUMBNAIL_SCALE_FACTOR;
			}
			return a;
		}
	}, {
		key: "getThumbnailWidth",
		value: function drawLedgerLine() {
			return this.getWidth() / render.THUMBNAIL_SCALE_FACTOR;
		}
	}, {
		key: "getThumbnailHeight",
		value: function setPageNumberFontSize() {
			return this.getHeight() / render.THUMBNAIL_SCALE_FACTOR;
		}
	}, {
		key: "reverseHorizontal",
		value: function render(width, indent) {
			var rules = this.yuvData;
			/** @type {number} */
			var i = 0;
			var x = this.top * this.dataWidth + this.left;
			for (; i < indent; i++, x = x + this.dataWidth) {
				var nx = x + width / 2;
				var j = x;
				/** @type {number} */
				var i = x + width - 1;
				for (; j < nx; j++, i--) {
					var swap = rules[j];
					rules[j] = rules[i];
					rules[i] = swap;
				}
			}
		}
	}, {
		key: "invert",
		value: function design() {
			return new O(this);
		}
	}]);
	return render;
}($this);
/** @type {number} */
tip.THUMBNAIL_SCALE_FACTOR = 2;
var o = function (_WebInspector$GeneralTreeElement) {
	/**
	 * @param {number} data
	 * @param {number} value
	 * @param {!Object} i
	 * @param {?} next
	 * @param {!Object} x
	 * @param {number} y
	 * @param {number} row
	 * @return {?}
	 */
	function Cell(data, value, i, next, x, y, row) {
		var _this;
		_classCallCheck2(this, Cell);
		if (_this = _possibleConstructorReturn(this, (Cell.__proto__ || Object.getPrototypeOf(Cell)).call(this, value, i)), _this, _this.dataWidth = next, _this.dataHeight = x, _this.left = y, _this.top = row, 4 === data.BYTES_PER_ELEMENT) {
			/** @type {number} */
			var b = value * i;
			/** @type {!Uint8ClampedArray} */
			var result = new Uint8ClampedArray(b);
			/** @type {number} */
			var a = 0;
			for (; a < b; a++) {
				var map = data[a];
				/** @type {number} */
				var colorName = map >> 16 & 255;
				/** @type {number} */
				var sitesowners = map >> 7 & 510;
				/** @type {number} */
				var siteName = 255 & map;
				/** @type {number} */
				result[a] = (colorName + sitesowners + siteName) / 4 & 255;
			}
			/** @type {!Uint8ClampedArray} */
			_this.luminances = result;
		} else {
			/** @type {number} */
			_this.luminances = data;
		}
		if (void 0 === next && (_this.dataWidth = value), void 0 === x && (_this.dataHeight = i), void 0 === y && (_this.left = 0), void 0 === row && (_this.top = 0), _this.left + value > _this.dataWidth || _this.top + i > _this.dataHeight) {
			throw new Function("Crop rectangle does not fit within image data.");
		}
		return _possibleConstructorReturn(_this);
	}
	_inherits(Cell, _WebInspector$GeneralTreeElement);
	_createClass2(Cell, [{
		key: "getRow",
		value: function render(value, result) {
			if (value < 0 || value >= this.getHeight()) {
				throw new Function("Requested row is outside the image: " + value);
			}
			var len = this.getWidth();
			if (null == result || result.length < len) {
				/** @type {!Uint8ClampedArray} */
				result = new Uint8ClampedArray(len);
			}
			var end = (value + this.top) * this.dataWidth + this.left;
			return System.arraycopy(this.luminances, end, result, 0, len), result;
		}
	}, {
		key: "getMatrix",
		value: function render() {
			var w = this.getWidth();
			var h = this.getHeight();
			if (w === this.dataWidth && h === this.dataHeight) {
				return this.luminances;
			}
			/** @type {number} */
			var len = w * h;
			/** @type {!Uint8ClampedArray} */
			var result = new Uint8ClampedArray(len);
			var off = this.top * this.dataWidth + this.left;
			if (w === this.dataWidth) {
				return System.arraycopy(this.luminances, off, result, 0, len), result;
			}
			/** @type {number} */
			var i = 0;
			for (; i < h; i++) {
				/** @type {number} */
				var start = i * w;
				System.arraycopy(this.luminances, off, result, start, w);
				off = off + this.dataWidth;
			}
			return result;
		}
	}, {
		key: "isCropSupported",
		value: function almost_equals() {
			return true;
		}
	}, {
		key: "crop",
		value: function render(dx, dy, r, options) {
			return new Cell(this.luminances, r, options, this.dataWidth, this.dataHeight, this.left + dx, this.top + dy);
		}
	}, {
		key: "invert",
		value: function design() {
			return new O(this);
		}
	}]);
	return Cell;
}($this);
var Class = function (_WebInspector$GeneralTreeElement) {
	/**
	 * @return {?}
	 */
	function CacheLink() {
		_classCallCheck2(this, CacheLink);
		return _possibleConstructorReturn(this, (CacheLink.__proto__ || Object.getPrototypeOf(CacheLink)).apply(this, arguments));
	}
	_inherits(CacheLink, _WebInspector$GeneralTreeElement);
	_createClass2(CacheLink, null, [{
		key: "forName",
		value: function forName(name) {
			return this.getCharacterSetECIByName(name);
		}
	}]);
	return CacheLink;
}(options);
/**
 * @return {undefined}
 */
var encoding = function TempusDominusBootstrap3() {
	_classCallCheck2(this, TempusDominusBootstrap3);
};
encoding.ISO_8859_1 = options.ISO8859_1;
var SettingsSlot = function () {
	/**
	 * @return {undefined}
	 */
	function TempusDominusBootstrap3() {
		_classCallCheck2(this, TempusDominusBootstrap3);
	}
	_createClass2(TempusDominusBootstrap3, [{
		key: "isCompact",
		value: function isCompact() {
			return this.compact;
		}
	}, {
		key: "setCompact",
		value: function generate(callback) {
			/** @type {string} */
			this.compact = callback;
		}
	}, {
		key: "getSize",
		value: function fileSize() {
			return this.size;
		}
	}, {
		key: "setSize",
		value: function NodeConstructor(size) {
			/** @type {number} */
			this.size = size;
		}
	}, {
		key: "getLayers",
		value: function findCompLayers() {
			return this.layers;
		}
	}, {
		key: "setLayers",
		value: function completeLayers(layers) {
			/** @type {string} */
			this.layers = layers;
		}
	}, {
		key: "getCodeWords",
		value: function getCodeWords() {
			return this.codeWords;
		}
	}, {
		key: "setCodeWords",
		value: function prefetchGroupsInfo(canCreateDiscussions) {
			this.codeWords = canCreateDiscussions;
		}
	}, {
		key: "getMatrix",
		value: function update() {
			return this.matrix;
		}
	}, {
		key: "setMatrix",
		value: function LConvolutionFilter(matrix) {
			/** @type {!Object} */
			this.matrix = matrix;
		}
	}]);
	return TempusDominusBootstrap3;
}();
var QuickBase = function () {
	/**
	 * @return {undefined}
	 */
	function TempusDominusBootstrap3() {
		_classCallCheck2(this, TempusDominusBootstrap3);
	}
	_createClass2(TempusDominusBootstrap3, null, [{
		key: "singletonList",
		value: function prefetchGroupsInfo(canCreateDiscussions) {
			return [canCreateDiscussions];
		}
	}, {
		key: "min",
		value: function TestDialogController($scope, dialog) {
			return $scope.sort(dialog)[0];
		}
	}]);
	return TempusDominusBootstrap3;
}();
var Rr = function () {
	/**
	 * @param {string} element
	 * @return {undefined}
	 */
	function TempusDominusBootstrap3(element) {
		_classCallCheck2(this, TempusDominusBootstrap3);
		/** @type {string} */
		this.previous = element;
	}
	_createClass2(TempusDominusBootstrap3, [{
		key: "getPrevious",
		value: function lockQuest() {
			return this.previous;
		}
	}]);
	return TempusDominusBootstrap3;
}();
var Mix = function (_WebInspector$GeneralTreeElement) {
	/**
	 * @param {?} props
	 * @param {!Object} mode
	 * @param {number} width
	 * @return {?}
	 */
	function StarRatingComponent(props, mode, width) {
		var _this;
		_classCallCheck2(this, StarRatingComponent);
		_this = _possibleConstructorReturn(this, (StarRatingComponent.__proto__ || Object.getPrototypeOf(StarRatingComponent)).call(this, props));
		_this;
		/** @type {!Object} */
		_this.value = mode;
		/** @type {number} */
		_this.bitCount = width;
		return _this;
	}
	_inherits(StarRatingComponent, _WebInspector$GeneralTreeElement);
	_createClass2(StarRatingComponent, [{
		key: "appendTo",
		value: function RunHaploViewer(result, result_instance_id1) {
			result.appendBits(this.value, this.bitCount);
		}
	}, {
		key: "add",
		value: function handleSlide(isSlidingUp, $cont) {
			return new StarRatingComponent(this, isSlidingUp, $cont);
		}
	}, {
		key: "addBinaryShift",
		value: function handleSlide(isSlidingUp, $cont) {
			return console.warn("addBinaryShift on SimpleToken, this simply returns a copy of this token"), new StarRatingComponent(this, isSlidingUp, $cont);
		}
	}, {
		key: "toString",
		value: function onmessage() {
			/** @type {number} */
			var tp = this.value & (1 << this.bitCount) - 1;
			return tp = tp | 1 << this.bitCount, "<" + scope.toBinaryString(tp | 1 << this.bitCount).substring(1) + ">";
		}
	}]);
	return StarRatingComponent;
}(Rr);
var Sub = function (parent) {
	/**
	 * @param {?} props
	 * @param {string} mode
	 * @param {number} options
	 * @return {?}
	 */
	function TacoTableCell(props, mode, options) {
		var _this;
		_classCallCheck2(this, TacoTableCell);
		_this = _possibleConstructorReturn(this, (TacoTableCell.__proto__ || Object.getPrototypeOf(TacoTableCell)).call(this, props, 0, 0));
		_this;
		/** @type {string} */
		_this.binaryShiftStart = mode;
		/** @type {number} */
		_this.binaryShiftByteCount = options;
		return _this;
	}
	_inherits(TacoTableCell, parent);
	_createClass2(TacoTableCell, [{
		key: "appendTo",
		value: function _normalizeSingle(output, hash) {
			/** @type {number} */
			var identifier = 0;
			for (; identifier < this.binaryShiftByteCount; identifier++) {
				if (0 === identifier || 31 === identifier && this.binaryShiftByteCount <= 62) {
					output.appendBits(31, 5);
					if (this.binaryShiftByteCount > 62) {
						output.appendBits(this.binaryShiftByteCount - 31, 16);
					} else {
						if (0 === identifier) {
							output.appendBits(Math.min(this.binaryShiftByteCount, 31), 5);
						} else {
							output.appendBits(this.binaryShiftByteCount - 31, 5);
						}
					}
				}
				output.appendBits(hash[this.binaryShiftStart + identifier], 8);
			}
		}
	}, {
		key: "addBinaryShift",
		value: function handleSlide(isSlidingUp, $cont) {
			return new TacoTableCell(this, isSlidingUp, $cont);
		}
	}, {
		key: "toString",
		value: function toString() {
			return "<" + this.binaryShiftStart + "::" + (this.binaryShiftStart + this.binaryShiftByteCount - 1) + ">";
		}
	}]);
	return TacoTableCell;
}(Mix);
/** @type {!Array} */
var settings = ["UPPER", "LOWER", "DIGIT", "MIXED", "PUNCT"];
/** @type {number} */
var id = 0;
/** @type {number} */
var category = 1;
/** @type {number} */
var name = 2;
/** @type {number} */
var arrayKey = 3;
/** @type {number} */
var index = 4;
var Completed = new Mix(null, 0, 0);
/** @type {!Array} */
var profiles = [Int32Array.from([0, 327708, 327710, 327709, 656318]), Int32Array.from([590318, 0, 327710, 327709, 656318]), Int32Array.from([262158, 590300, 0, 590301, 932798]), Int32Array.from([327709, 327708, 656318, 0, 327710]), Int32Array.from([327711, 656380, 656382, 656381, 0])];
var rules = function (result) {
	/** @type {boolean} */
	var _iteratorNormalCompletion3 = true;
	/** @type {boolean} */
	var _didIteratorError73 = false;
	var _iteratorError17 = undefined;
	try {
		var _iterator3 = result[Symbol.iterator]();
		var $__6;
		for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
			var item = $__6.value;
			b.fill(item, -1);
		}
	} catch (err) {
		/** @type {boolean} */
		_didIteratorError73 = true;
		_iteratorError17 = err;
	} finally {
		try {
			if (!_iteratorNormalCompletion3 && _iterator3.return) {
				_iterator3.return();
			}
		} finally {
			if (_didIteratorError73) {
				throw _iteratorError17;
			}
		}
	}
	return result[id][index] = 0, result[category][index] = 0, result[category][id] = 28, result[arrayKey][index] = 0, result[name][index] = 0, result[name][id] = 15, result;
}(b.createInt32Array(6, 6));
var TaskFilter = function () {
	/**
	 * @param {string} value
	 * @param {string} m
	 * @param {number} mi
	 * @param {number} s
	 * @return {undefined}
	 */
	function Date(value, m, mi, s) {
		_classCallCheck2(this, Date);
		/** @type {string} */
		this.token = value;
		/** @type {string} */
		this.mode = m;
		/** @type {number} */
		this.binaryShiftByteCount = mi;
		/** @type {number} */
		this.bitCount = s;
	}
	_createClass2(Date, [{
		key: "getMode",
		value: function Engine_mode() {
			return this.mode;
		}
	}, {
		key: "getToken",
		value: function tokenHasCustomStyling() {
			return this.token;
		}
	}, {
		key: "getBinaryShiftByteCount",
		value: function getBinaryShiftByteCount() {
			return this.binaryShiftByteCount;
		}
	}, {
		key: "getBitCount",
		value: function getBitCount() {
			return this.bitCount;
		}
	}, {
		key: "latchAndAppend",
		value: function render(index, object) {
			var td = this.bitCount;
			var res = this.token;
			if (index !== this.mode) {
				var sd = profiles[this.mode][index];
				res = callback(res, 65535 & sd, sd >> 16);
				td = td + (sd >> 16);
			}
			/** @type {number} */
			var i = index === name ? 4 : 5;
			return res = callback(res, object, i), new Date(res, index, 0, td + i);
		}
	}, {
		key: "shiftAndAppend",
		value: function render(type, i) {
			var res = this.token;
			/** @type {number} */
			var output = this.mode === name ? 4 : 5;
			return res = callback(res, rules[this.mode][type], output), res = callback(res, i, 5), new Date(res, this.mode, 0, this.bitCount + output + 5);
		}
	}, {
		key: "addBinaryShiftChar",
		value: function render(startIndex) {
			var res = this.token;
			var key = this.mode;
			var pos = this.bitCount;
			if (this.mode === index || this.mode === name) {
				var header = profiles[key][id];
				res = callback(res, 65535 & header, header >> 16);
				pos = pos + (header >> 16);
				/** @type {number} */
				key = id;
			}
			/** @type {number} */
			var i = 0 === this.binaryShiftByteCount || 31 === this.binaryShiftByteCount ? 18 : 62 === this.binaryShiftByteCount ? 9 : 8;
			var data = new Date(res, key, this.binaryShiftByteCount + 1, pos + i);
			return 2078 === data.binaryShiftByteCount && (data = data.endBinaryShift(startIndex + 1)), data;
		}
	}, {
		key: "endBinaryShift",
		value: function render(lagOffset) {
			if (0 === this.binaryShiftByteCount) {
				return this;
			}
			var lastToken = this.token;
			return lastToken = function (currentItemToken, channel, events) {
				return new Sub(currentItemToken, channel, events);
			}(lastToken, lagOffset - this.binaryShiftByteCount, this.binaryShiftByteCount), new Date(lastToken, this.mode, 0, this.bitCount);
		}
	}, {
		key: "isBetterThanOrEqualTo",
		value: function render(s) {
			var year = this.bitCount + (profiles[this.mode][s.mode] >> 16);
			return this.binaryShiftByteCount < s.binaryShiftByteCount ? year = year + (Date.calculateBinaryShiftCost(s) - Date.calculateBinaryShiftCost(this)) : this.binaryShiftByteCount > s.binaryShiftByteCount && s.binaryShiftByteCount > 0 && (year = year + 10), year <= s.bitCount;
		}
	}, {
		key: "toBitArray",
		value: function parse(domain) {
			/** @type {!Array} */
			var arrayElement = [];
			var input = this.endBinaryShift(domain.length).token;
			for (; null !== input; input = input.getPrevious()) {
				arrayElement.unshift(input);
			}
			var result = new BitArray;
			/** @type {boolean} */
			var _iteratorNormalCompletion3 = true;
			/** @type {boolean} */
			var _didIteratorError74 = false;
			var _iteratorError17 = undefined;
			try {
				var _iterator3 = arrayElement[Symbol.iterator]();
				var info;
				for (; !(_iteratorNormalCompletion3 = (info = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var connection = info.value;
					connection.appendTo(result, domain);
				}
			} catch (err) {
				/** @type {boolean} */
				_didIteratorError74 = true;
				_iteratorError17 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError74) {
						throw _iteratorError17;
					}
				}
			}
			return result;
		}
	}, {
		key: "toString",
		value: function getEventDay() {
			return _.format("%s bits=%d bytes=%d", settings[this.mode], this.bitCount, this.binaryShiftByteCount);
		}
	}], [{
		key: "calculateBinaryShiftCost",
		value: function prefetchGroupsInfo(canCreateDiscussions) {
			return canCreateDiscussions.binaryShiftByteCount > 62 ? 21 : canCreateDiscussions.binaryShiftByteCount > 31 ? 20 : canCreateDiscussions.binaryShiftByteCount > 0 ? 10 : 0;
		}
	}]);
	return Date;
}();
TaskFilter.INITIAL_STATE = new TaskFilter(Completed, id, 0, 0);
var HuffTab = function (result) {
	var prop = _.getCharCode(" ");
	var cfgLang = _.getCharCode(".");
	var limitName = _.getCharCode(",");
	/** @type {number} */
	result[id][prop] = 1;
	var timeParts = _.getCharCode("Z");
	var convertedFormat = _.getCharCode("A");
	var ourName = convertedFormat;
	for (; ourName <= timeParts; ourName++) {
		/** @type {number} */
		result[id][ourName] = ourName - convertedFormat + 2;
	}
	/** @type {number} */
	result[category][prop] = 1;
	var p = _.getCharCode("z");
	var f = _.getCharCode("a");
	var k = f;
	for (; k <= p; k++) {
		/** @type {number} */
		result[category][k] = k - f + 2;
	}
	/** @type {number} */
	result[name][prop] = 1;
	var num_seg = _.getCharCode("9");
	var n0 = _.getCharCode("0");
	var n = n0;
	for (; n <= num_seg; n++) {
		/** @type {number} */
		result[name][n] = n - n0 + 2;
	}
	/** @type {number} */
	result[name][limitName] = 12;
	/** @type {number} */
	result[name][cfgLang] = 13;
	/** @type {!Array} */
	var NETWORK_KEYS = ["\x00", " ", "\u0001", "\u0002", "\u0003", "\u0004", "\u0005", "\u0006", "\u0007", "\b", "\t", "\n", "\v", "\f", "\r", "\u001b", "\u001c", "\u001d", "\u001e", "\u001f", "@", "\\", "^", "_", "`", "|", "~", "\u007f"];
	/** @type {number} */
	var j = 0;
	for (; j < NETWORK_KEYS.length; j++) {
		/** @type {number} */
		result[arrayKey][_.getCharCode(NETWORK_KEYS[j])] = j;
	}
	/** @type {!Array} */
	var prefixes = ["\x00", "\r", "\x00", "\x00", "\x00", "\x00", "!", "'", "#", "$", "%", "&", "'", "(", ")", "*", "+", ",", "-", ".", "/", ":", ";", "<", "=", ">", "?", "[", "]", "{", "}"];
	/** @type {number} */
	var i = 0;
	for (; i < prefixes.length; i++) {
		if (_.getCharCode(prefixes[i]) > 0) {
			/** @type {number} */
			result[index][_.getCharCode(prefixes[i])] = i;
		}
	}
	return result;
}(b.createInt32Array(5, 256));
var Matrix = function () {
	/**
	 * @param {string} options
	 * @return {undefined}
	 */
	function Util(options) {
		_classCallCheck2(this, Util);
		/** @type {string} */
		this.text = options;
	}
	_createClass2(Util, [{
		key: "encode",
		value: function handleKeyPress() {
			var selTimeVal = _.getCharCode(" ");
			var curTimeVal = _.getCharCode("\n");
			var val = QuickBase.singletonList(TaskFilter.INITIAL_STATE);
			/** @type {number} */
			var i = 0;
			for (; i < this.text.length; i++) {
				var max = void 0;
				var tmpTimeVal = i + 1 < this.text.length ? this.text[i + 1] : 0;
				switch (this.text[i]) {
					case _.getCharCode("\r"):
						/** @type {number} */
						max = tmpTimeVal === curTimeVal ? 2 : 0;
						break;
					case _.getCharCode("."):
						/** @type {number} */
						max = tmpTimeVal === selTimeVal ? 3 : 0;
						break;
					case _.getCharCode(","):
						/** @type {number} */
						max = tmpTimeVal === selTimeVal ? 4 : 0;
						break;
					case _.getCharCode(":"):
						/** @type {number} */
						max = tmpTimeVal === selTimeVal ? 5 : 0;
						break;
					default:
						/** @type {number} */
						max = 0;
				}
				if (max > 0) {
					val = Util.updateStateListForPair(val, i, max);
					i++;
				} else {
					val = this.updateStateListForChar(val, i);
				}
			}
			return QuickBase.min(val, function (canCreateDiscussions, isSlidingUp) {
				return canCreateDiscussions.getBitCount() - isSlidingUp.getBitCount();
			}).toBitArray(this.text);
		}
	}, {
		key: "updateStateListForChar",
		value: function _destroyWaypoints(waypoints, highLighted) {
			/** @type {!Array} */
			var currentTimeStr = [];
			/** @type {boolean} */
			var _iteratorNormalCompletion3 = true;
			/** @type {boolean} */
			var _didIteratorError75 = false;
			var _iteratorError17 = undefined;
			try {
				var _iterator3 = waypoints[Symbol.iterator]();
				var $__6;
				for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var item = $__6.value;
					this.updateStateForChar(item, highLighted, currentTimeStr);
				}
			} catch (err) {
				/** @type {boolean} */
				_didIteratorError75 = true;
				_iteratorError17 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError75) {
						throw _iteratorError17;
					}
				}
			}
			return Util.simplifyStates(currentTimeStr);
		}
	}, {
		key: "updateStateForChar",
		value: function focus(controller, index, obj) {
			/** @type {number} */
			var j = 255 & this.text[index];
			/** @type {boolean} */
			var notDeletedPrevEl = HuffTab[controller.getMode()][j] > 0;
			/** @type {null} */
			var idx = null;
			/** @type {number} */
			var i = 0;
			for (; i <= index; i++) {
				var body = HuffTab[i][j];
				if (body > 0) {
					if (null == idx && (idx = controller.endBinaryShift(index)), !notDeletedPrevEl || i === controller.getMode() || i === name) {
						var prop = idx.latchAndAppend(i, body);
						obj.push(prop);
					}
					if (!notDeletedPrevEl && rules[controller.getMode()][i] >= 0) {
						var prop = idx.shiftAndAppend(i, body);
						obj.push(prop);
					}
				}
			}
			if (controller.getBinaryShiftByteCount() > 0 || 0 === HuffTab[controller.getMode()][j]) {
				var strKey = controller.addBinaryShiftChar(index);
				obj.push(strKey);
			}
		}
	}], [{
		key: "updateStateListForPair",
		value: function _destroyWaypoints(waypoints, highLighted, withArrowHead) {
			/** @type {!Array} */
			var n = [];
			/** @type {boolean} */
			var _iteratorNormalCompletion3 = true;
			/** @type {boolean} */
			var _didIteratorError76 = false;
			var _iteratorError17 = undefined;
			try {
				var _iterator3 = waypoints[Symbol.iterator]();
				var $__6;
				for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var item = $__6.value;
					this.updateStateForPair(item, highLighted, withArrowHead, n);
				}
			} catch (err) {
				/** @type {boolean} */
				_didIteratorError76 = true;
				_iteratorError17 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError76) {
						throw _iteratorError17;
					}
				}
			}
			return this.simplifyStates(n);
		}
	}, {
		key: "updateStateForPair",
		value: function filter(context, node, value, results) {
			var ctx = context.endBinaryShift(node);
			if (results.push(ctx.latchAndAppend(index, value)), context.getMode() !== index && results.push(ctx.shiftAndAppend(index, value)), 3 === value || 4 === value) {
				var varg = ctx.latchAndAppend(name, 16 - value).latchAndAppend(name, 1);
				results.push(varg);
			}
			if (context.getBinaryShiftByteCount() > 0) {
				var varg = context.addBinaryShiftChar(node).addBinaryShiftChar(node + 1);
				results.push(varg);
			}
		}
	}, {
		key: "simplifyStates",
		value: function render(menuItems) {
			/** @type {!Array} */
			var set = [];
			/** @type {boolean} */
			var _iteratorNormalCompletion3 = true;
			/** @type {boolean} */
			var _didIteratorError77 = false;
			var _iteratorError17 = undefined;
			try {
				var _iterator3 = menuItems[Symbol.iterator]();
				var $__6;
				for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var item = $__6.value;
					/** @type {boolean} */
					var _t218 = true;
					/** @type {boolean} */
					var _iteratorNormalCompletion3 = true;
					/** @type {boolean} */
					var _didIteratorError78 = false;
					var _iteratorError17 = undefined;
					try {
						/**
						 * @return {?}
						 */
						var tbody = function _loop() {
							var o = _step2.value;
							if (o.isBetterThanOrEqualTo(item)) {
								/** @type {boolean} */
								_t218 = false;
								return "break";
							}
							if (item.isBetterThanOrEqualTo(o)) {
								set = set.filter(function (n) {
									return n !== o;
								});
							}
						};
						var _iterator3 = set[Symbol.iterator]();
						var _step2;
						for (; !(_iteratorNormalCompletion3 = (_step2 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
							var body = tbody();
							if (body === "break") {
								break;
							}
						}
					} catch (err) {
						/** @type {boolean} */
						_didIteratorError78 = true;
						_iteratorError17 = err;
					} finally {
						try {
							if (!_iteratorNormalCompletion3 && _iterator3.return) {
								_iterator3.return();
							}
						} finally {
							if (_didIteratorError78) {
								throw _iteratorError17;
							}
						}
					}
					if (_t218) {
						set.push(item);
					}
				}
			} catch (err) {
				/** @type {boolean} */
				_didIteratorError77 = true;
				_iteratorError17 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError77) {
						throw _iteratorError17;
					}
				}
			}
			return set;
		}
	}]);
	return Util;
}();
var response = function () {
	/**
	 * @return {undefined}
	 */
	function self() {
		_classCallCheck2(this, self);
	}
	_createClass2(self, null, [{
		key: "encodeBytes",
		value: function invalidateControl(reason) {
			return self.encode(reason, self.DEFAULT_EC_PERCENT, self.DEFAULT_AZTEC_LAYERS);
		}
	}, {
		key: "encode",
		value: function init(rows, done, value) {
			var d = void 0;
			var i = void 0;
			var x = void 0;
			var y = void 0;
			var node = void 0;
			var option = (new Matrix(rows)).encode();
			var p = scope.truncDivision(option.getSize() * done, 100) + 11;
			var u = option.getSize() + p;
			if (value !== self.DEFAULT_AZTEC_LAYERS) {
				if (d = value < 0, (i = Math.abs(value)) > (d ? self.MAX_NB_BITS_COMPACT : self.MAX_NB_BITS)) {
					throw new Function(_.format("Illegal value %s for layers", value));
				}
				/** @type {number} */
				var rlt = (x = self.totalBitsInLayer(i, d)) - x % (y = self.WORD_SIZE[i]);
				if ((node = self.stuffBits(option, y)).getSize() + p > rlt) {
					throw new Function("Data to large for user specified layer");
				}
				if (d && node.getSize() > 64 * y) {
					throw new Function("Data to large for user specified layer");
				}
			} else {
				/** @type {number} */
				y = 0;
				/** @type {null} */
				node = null;
				/** @type {number} */
				var len = 0;
				for (; ; len++) {
					if (len > self.MAX_NB_BITS) {
						throw new Function("Data too large for an Aztec code");
					}
					if (i = (d = len <= 3) ? len + 1 : len, u > (x = self.totalBitsInLayer(i, d))) {
						continue;
					}
					if (!(null != node && y === self.WORD_SIZE[i])) {
						y = self.WORD_SIZE[i];
						node = self.stuffBits(option, y);
					}
					/** @type {number} */
					var cox = x - x % y;
					if (!(d && node.getSize() > 64 * y) && node.getSize() + p <= cox) {
						break;
					}
				}
			}
			var v = void 0;
			var sub = self.generateCheckWords(node, x, y);
			/** @type {number} */
			var X = node.getSize() / y;
			var result = self.generateModeMessage(d, i, X);
			/** @type {number} */
			var n = (d ? 11 : 14) + 4 * i;
			/** @type {!Int32Array} */
			var data = new Int32Array(n);
			if (d) {
				/** @type {number} */
				v = n;
				/** @type {number} */
				var i = 0;
				for (; i < data.length; i++) {
					/** @type {number} */
					data[i] = i;
				}
			} else {
				/** @type {number} */
				v = n + 1 + 2 * scope.truncDivision(scope.truncDivision(n, 2) - 1, 15);
				var i = scope.truncDivision(n, 2);
				var Ymain = scope.truncDivision(v, 2);
				/** @type {number} */
				var x = 0;
				for (; x < i; x++) {
					var Yxlabel = x + scope.truncDivision(x, 15);
					/** @type {number} */
					data[i - x - 1] = Ymain - Yxlabel - 1;
					data[i + x] = Ymain + Yxlabel + 1;
				}
			}
			var t = new Image(v);
			/** @type {number} */
			var k = 0;
			/** @type {number} */
			var AnonId_ = 0;
			for (; k < i; k++) {
				/** @type {number} */
				var _ccHeight = 4 * (i - k) + (d ? 9 : 12);
				/** @type {number} */
				var j = 0;
				for (; j < _ccHeight; j++) {
					/** @type {number} */
					var id = 2 * j;
					/** @type {number} */
					var i = 0;
					for (; i < 2; i++) {
						if (sub.get(AnonId_ + id + i)) {
							t.set(data[2 * k + i], data[2 * k + j]);
						}
						if (sub.get(AnonId_ + 2 * _ccHeight + id + i)) {
							t.set(data[2 * k + j], data[n - 1 - 2 * k - i]);
						}
						if (sub.get(AnonId_ + 4 * _ccHeight + id + i)) {
							t.set(data[n - 1 - 2 * k - i], data[n - 1 - 2 * k - j]);
						}
						if (sub.get(AnonId_ + 6 * _ccHeight + id + i)) {
							t.set(data[n - 1 - 2 * k - j], data[2 * k + i]);
						}
					}
				}
				/** @type {number} */
				AnonId_ = AnonId_ + 8 * _ccHeight;
			}
			if (self.drawModeMessage(t, d, v, result), d) {
				self.drawBullsEye(t, scope.truncDivision(v, 2), 5);
			} else {
				self.drawBullsEye(t, scope.truncDivision(v, 2), 7);
				/** @type {number} */
				var keylen = 0;
				/** @type {number} */
				var offset = 0;
				for (; keylen < scope.truncDivision(n, 2) - 1; keylen = keylen + 15, offset = offset + 16) {
					/** @type {number} */
					var d = 1 & scope.truncDivision(v, 2);
					for (; d < v; d = d + 2) {
						t.set(scope.truncDivision(v, 2) - offset, d);
						t.set(scope.truncDivision(v, 2) + offset, d);
						t.set(d, scope.truncDivision(v, 2) - offset);
						t.set(d, scope.truncDivision(v, 2) + offset);
					}
				}
			}
			var _this = new SettingsSlot;
			return _this.setCompact(d), _this.setSize(v), _this.setLayers(i), _this.setCodeWords(X), _this.setMatrix(t), _this;
		}
	}, {
		key: "drawBullsEye",
		value: function windowToSpaceWrapper(ctx, x, i) {
			/** @type {number} */
			var offset = 0;
			for (; offset < i; offset = offset + 2) {
				/** @type {number} */
				var i = x - offset;
				for (; i <= x + offset; i++) {
					ctx.set(i, x - offset);
					ctx.set(i, x + offset);
					ctx.set(x - offset, i);
					ctx.set(x + offset, i);
				}
			}
			ctx.set(x - i, x - i);
			ctx.set(x - i + 1, x - i);
			ctx.set(x - i, x - i + 1);
			ctx.set(x + i, x - i);
			ctx.set(x + i, x - i + 1);
			ctx.set(x + i, x + i - 1);
		}
	}, {
		key: "generateModeMessage",
		value: function prepare(provider, index, size) {
			var result = new BitArray;
			return provider ? (result.appendBits(index - 1, 2), result.appendBits(size - 1, 6), result = self.generateCheckWords(result, 28, 4)) : (result.appendBits(index - 1, 5), result.appendBits(size - 1, 11), result = self.generateCheckWords(result, 40, 4)), result;
		}
	}, {
		key: "drawModeMessage",
		value: function addRemoveClassesPostDigest(ctx, i, element, matrix) {
			var x = scope.truncDivision(element, 2);
			if (i) {
				/** @type {number} */
				var i = 0;
				for (; i < 7; i++) {
					/** @type {number} */
					var r = x - 3 + i;
					if (matrix.get(i)) {
						ctx.set(r, x - 5);
					}
					if (matrix.get(i + 7)) {
						ctx.set(x + 5, r);
					}
					if (matrix.get(20 - i)) {
						ctx.set(r, x + 5);
					}
					if (matrix.get(27 - i)) {
						ctx.set(x - 5, r);
					}
				}
			} else {
				/** @type {number} */
				var i = 0;
				for (; i < 10; i++) {
					var r = x - 5 + i + scope.truncDivision(i, 5);
					if (matrix.get(i)) {
						ctx.set(r, x - 7);
					}
					if (matrix.get(i + 10)) {
						ctx.set(x + 7, r);
					}
					if (matrix.get(29 - i)) {
						ctx.set(r, x + 7);
					}
					if (matrix.get(39 - i)) {
						ctx.set(x - 7, r);
					}
				}
			}
		}
	}, {
		key: "generateCheckWords",
		value: function render(context, n, value) {
			/** @type {number} */
			var kMatchMinLen = context.getSize() / value;
			var data = new Test(self.getGF(value));
			var len = scope.truncDivision(n, value);
			var s = self.bitsToWords(context, value, len);
			data.encode(s, len - kMatchMinLen);
			/** @type {number} */
			var i = n % value;
			var result = new BitArray;
			result.appendBits(0, i);
			/** @type {boolean} */
			var _iteratorNormalCompletion3 = true;
			/** @type {boolean} */
			var _didIteratorError79 = false;
			var _iteratorError17 = undefined;
			try {
				var _iterator3 = Array.from(s)[Symbol.iterator]();
				var $__6;
				for (; !(_iteratorNormalCompletion3 = ($__6 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var item = $__6.value;
					result.appendBits(item, value);
				}
			} catch (err) {
				/** @type {boolean} */
				_didIteratorError79 = true;
				_iteratorError17 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError79) {
						throw _iteratorError17;
					}
				}
			}
			return result;
		}
	}, {
		key: "bitsToWords",
		value: function hash(file, i, n) {
			var k = void 0;
			var columns = void 0;
			/** @type {!Int32Array} */
			var result = new Int32Array(n);
			/** @type {number} */
			k = 0;
			/** @type {number} */
			columns = file.getSize() / i;
			for (; k < columns; k++) {
				/** @type {number} */
				var rule = 0;
				/** @type {number} */
				var offset = 0;
				for (; offset < i; offset++) {
					/** @type {number} */
					rule = rule | (file.get(k * i + offset) ? 1 << i - offset - 1 : 0);
				}
				/** @type {number} */
				result[k] = rule;
			}
			return result;
		}
	}, {
		key: "getGF",
		value: function unserializeArgs(args) {
			switch (args) {
				case 4:
					return r.AZTEC_PARAM;
				case 6:
					return r.AZTEC_DATA_6;
				case 8:
					return r.AZTEC_DATA_8;
				case 10:
					return r.AZTEC_DATA_10;
				case 12:
					return r.AZTEC_DATA_12;
				default:
					throw new Function("Unsupported word size " + args);
			}
		}
	}, {
		key: "stuffBits",
		value: function encode(view, width) {
			var result = new BitArray;
			var outLength = view.getSize();
			/** @type {number} */
			var bitmask = (1 << width) - 2;
			/** @type {number} */
			var i = 0;
			for (; i < outLength; i = i + width) {
				/** @type {number} */
				var value = 0;
				/** @type {number} */
				var offset = 0;
				for (; offset < width; offset++) {
					if (i + offset >= outLength || view.get(i + offset)) {
						/** @type {number} */
						value = value | 1 << width - 1 - offset;
					}
				}
				if ((value & bitmask) === bitmask) {
					result.appendBits(value & bitmask, width);
					i--;
				} else {
					if (0 == (value & bitmask)) {
						result.appendBits(1 | value, width);
						i--;
					} else {
						result.appendBits(value, width);
					}
				}
			}
			return result;
		}
	}, {
		key: "totalBitsInLayer",
		value: function handleSlide(isSlidingUp, $cont) {
			return (($cont ? 88 : 112) + 16 * isSlidingUp) * isSlidingUp;
		}
	}]);
	return self;
}();
/** @type {number} */
response.DEFAULT_EC_PERCENT = 33;
/** @type {number} */
response.DEFAULT_AZTEC_LAYERS = 0;
/** @type {number} */
response.MAX_NB_BITS = 32;
/** @type {number} */
response.MAX_NB_BITS_COMPACT = 4;
/** @type {!Int32Array} */
response.WORD_SIZE = Int32Array.from([4, 6, 6, 8, 8, 8, 8, 8, 8, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12]);
var CreateSBTreeClass = function () {
	/**
	 * @return {undefined}
	 */
	function self() {
		_classCallCheck2(this, self);
	}
	_createClass2(self, [{
		key: "encode",
		value: function $get(mmCoreSplitViewBlock, $state, mmCoreEventSessionExpired, mmCoreUserDeleted) {
			return this.encodeWithHints(mmCoreSplitViewBlock, $state, mmCoreEventSessionExpired, mmCoreUserDeleted, null);
		}
	}, {
		key: "encodeWithHints",
		value: function connect(callback, instance, type, opData, context) {
			var name = encoding.ISO_8859_1;
			var exposed_ports = response.DEFAULT_EC_PERCENT;
			var respObj = response.DEFAULT_AZTEC_LAYERS;
			return null != context && (context.has(point.CHARACTER_SET) && (name = Class.forName(context.get(point.CHARACTER_SET).toString())), context.has(point.ERROR_CORRECTION) && (exposed_ports = scope.parseInt(context.get(point.ERROR_CORRECTION).toString())), context.has(point.AZTEC_LAYERS) && (respObj = scope.parseInt(context.get(point.AZTEC_LAYERS).toString()))), self.encodeLayers(callback, instance, type, opData, name, exposed_ports, respObj);
		}
	}], [{
		key: "encodeLayers",
		value: function init(message, value, lvl, parent, i, options, args) {
			if (value !== change.AZTEC) {
				throw new Function("Can only encode AZTEC, but got " + value);
			}
			var data = response.encode(_.getBytes(message, i), options, args);
			return self.renderResult(data, lvl, parent);
		}
	}, {
		key: "renderResult",
		value: function init(o, u, r) {
			var selectedGroup = o.getMatrix();
			if (null == selectedGroup) {
				throw new Path;
			}
			var m = selectedGroup.getWidth();
			var n = selectedGroup.getHeight();
			/** @type {number} */
			var value = Math.max(u, m);
			/** @type {number} */
			var a = Math.max(r, n);
			/** @type {number} */
			var x = Math.min(value / m, a / n);
			/** @type {number} */
			var pathArgsIndex = (value - m * x) / 2;
			/** @type {number} */
			var firstDisplayed = (a - n * x) / 2;
			var result = new Image(value, a);
			/** @type {number} */
			var j = 0;
			/** @type {number} */
			var i = firstDisplayed;
			for (; j < n; j++, i = i + x) {
				/** @type {number} */
				var n = 0;
				/** @type {number} */
				var j = pathArgsIndex;
				for (; n < m; n++, j = j + x) {
					if (selectedGroup.get(n, j)) {
						result.setRegion(j, i, x, x);
					}
				}
			}
			return result;
		}
	}]);
	return self;
}();
exports.ArgumentException = Renderer;
exports.ArithmeticException = GF256Poly;
exports.AztecCode = SettingsSlot;
exports.AztecCodeReader = Rule;
exports.AztecCodeWriter = CreateSBTreeClass;
exports.AztecDecoder = data;
exports.AztecDetector = FindNodeRPC;
exports.AztecDetectorResult = Router;
exports.AztecEncoder = response;
exports.AztecHighLevelEncoder = Matrix;
exports.AztecPoint = Coordinate;
/** @type {(undefined|{AZTEC: number, CODABAR: number, CODE_128: number, CODE_39: number, CODE_93: number, DATA_MATRIX: number, EAN_13: number, EAN_8: number, ITF: number, MAXICODE: number, PDF_417: number, QR_CODE: number, RSS_14: number, RSS_EXPANDED: number, UPC_A: number, UPC_E: number, UPC_EAN_EXTENSION: number})} */
exports.BarcodeFormat = change;
exports.Binarizer = cfg;
exports.BinaryBitmap = Registry;
exports.BitArray = BitArray;
exports.BitMatrix = Image;
exports.BitSource = SyntaxError;
exports.BrowserAztecCodeReader = function (_WebInspector$GeneralTreeElement) {
	/**
	 * @return {?}
	 */
	function _class() {
		var server = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 500;
		_classCallCheck2(this, _class);
		return _possibleConstructorReturn(this, (_class.__proto__ || Object.getPrototypeOf(_class)).call(this, new Rule, server));
	}
	_inherits(_class, _WebInspector$GeneralTreeElement);
	return _class;
}(PacketsPanel);
exports.BrowserBarcodeReader = function (_WebInspector$GeneralTreeElement) {
	/**
	 * @return {?}
	 */
	function ListItem() {
		var server = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 500;
		var value = arguments[1];
		_classCallCheck2(this, ListItem);
		return _possibleConstructorReturn(this, (ListItem.__proto__ || Object.getPrototypeOf(ListItem)).call(this, new Value(value), server, value));
	}
	_inherits(ListItem, _WebInspector$GeneralTreeElement);
	return ListItem;
}(PacketsPanel);
exports.BrowserCodeReader = PacketsPanel;
exports.BrowserDatamatrixCodeReader = function (_WebInspector$GeneralTreeElement) {
	/**
	 * @return {?}
	 */
	function PubNub() {
		var server = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 500;
		_classCallCheck2(this, PubNub);
		return _possibleConstructorReturn(this, (PubNub.__proto__ || Object.getPrototypeOf(PubNub)).call(this, new Model, server));
	}
	_inherits(PubNub, _WebInspector$GeneralTreeElement);
	return PubNub;
}(PacketsPanel);
exports.BrowserMultiFormatReader = function (_WebInspector$GeneralTreeElement) {
	/**
	 * @return {?}
	 */
	function LeanKitNotifier() {
		var base = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
		var _this;
		var server = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;
		_classCallCheck2(this, LeanKitNotifier);
		var _extend = new SearchFiles;
		_extend.setHints(base);
		_this = _possibleConstructorReturn(this, (LeanKitNotifier.__proto__ || Object.getPrototypeOf(LeanKitNotifier)).call(this, _extend, server));
		_this;
		return _this;
	}
	_inherits(LeanKitNotifier, _WebInspector$GeneralTreeElement);
	_createClass2(LeanKitNotifier, [{
		key: "decodeBitmap",
		value: function extractPresetLocal(callback) {
			return this.reader.decodeWithState(callback);
		}
	}]);
	return LeanKitNotifier;
}(PacketsPanel);
exports.BrowserPDF417Reader = function (_WebInspector$GeneralTreeElement) {
	/**
	 * @return {?}
	 */
	function PubNub() {
		var server = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 500;
		_classCallCheck2(this, PubNub);
		return _possibleConstructorReturn(this, (PubNub.__proto__ || Object.getPrototypeOf(PubNub)).call(this, new Event, server));
	}
	_inherits(PubNub, _WebInspector$GeneralTreeElement);
	return PubNub;
}(PacketsPanel);
exports.BrowserQRCodeReader = function (_WebInspector$GeneralTreeElement) {
	/**
	 * @return {?}
	 */
	function PubNub() {
		var server = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 500;
		_classCallCheck2(this, PubNub);
		return _possibleConstructorReturn(this, (PubNub.__proto__ || Object.getPrototypeOf(PubNub)).call(this, new Message, server));
	}
	_inherits(PubNub, _WebInspector$GeneralTreeElement);
	return PubNub;
}(PacketsPanel);
exports.BrowserQRCodeSvgWriter = renderer;
exports.CharacterSetECI = options;
exports.ChecksumException = Rectangle;
exports.Code128Reader = module;
exports.Code39Reader = Sprite;
exports.DataMatrixDecodedBitStreamParser = config;
exports.DataMatrixReader = Model;
/** @type {(undefined|{ALLOWED_EAN_EXTENSIONS: number, ALLOWED_LENGTHS: number, ASSUME_CODE_39_CHECK_DIGIT: number, ASSUME_GS1: number, CHARACTER_SET: number, NEED_RESULT_POINT_CALLBACK: number, OTHER: number, POSSIBLE_FORMATS: number, PURE_BARCODE: number, RETURN_CODABAR_START_END: number, TRY_HARDER: number})} */
exports.DecodeHintType = node;
exports.DecoderResult = Response;
exports.DefaultGridSampler = variablesInScope;
exports.DetectorResult = HTMLTagSpecification;
exports.EAN13Reader = Actions;
/** @type {(undefined|{AZTEC_LAYERS: number, CHARACTER_SET: number, DATA_MATRIX_SHAPE: number, ERROR_CORRECTION: number, MARGIN: number, MAX_SIZE: number, MIN_SIZE: number, PDF417_COMPACT: number, PDF417_COMPACTION: number, PDF417_DIMENSIONS: number, QR_VERSION: number})} */
exports.EncodeHintType = point;
exports.Exception = input;
exports.FormatException = Date;
exports.GenericGF = r;
exports.GenericGFPoly = Node;
exports.GlobalHistogramBinarizer = Subscription;
exports.GridSampler = context;
exports.GridSamplerInstance = ServiceManager;
exports.HTMLCanvasElementLuminanceSource = Context;
exports.HybridBinarizer = Emitter;
exports.ITFReader = EdgeDrawer;
exports.IllegalArgumentException = Function;
exports.IllegalStateException = Path;
exports.InvertedLuminanceSource = O;
exports.LuminanceSource = $this;
exports.MathUtils = p;
exports.MultiFormatOneDReader = Value;
exports.MultiFormatReader = SearchFiles;
exports.MultiFormatWriter = function () {
	/**
	 * @return {undefined}
	 */
	function TempusDominusBootstrap3() {
		_classCallCheck2(this, TempusDominusBootstrap3);
	}
	_createClass2(TempusDominusBootstrap3, [{
		key: "encode",
		value: function test(length, name, version, route, msg) {
			var self = void 0;
			switch (name) {
				case change.QR_CODE:
					self = new CollectionView;
					break;
				default:
					throw new Function("No encoder available for format " + name);
			}
			return self.encode(length, name, version, route, msg);
		}
	}]);
	return TempusDominusBootstrap3;
}();
exports.NotFoundException = TypeError;
exports.OneDReader = path;
exports.PDF417DecodedBitStreamParser = i18n;
exports.PDF417DecoderErrorCorrection = EventEmitter;
exports.PDF417Reader = Event;
exports.PDF417ResultMetadata = ArrayType;
exports.PerspectiveTransform = PerspectiveTransform;
exports.PlanarYUVLuminanceSource = tip;
exports.QRCodeByteMatrix = ByteMatrix;
exports.QRCodeDataMask = set;
exports.QRCodeDecodedBitStreamParser = component;
exports.QRCodeDecoderErrorCorrectionLevel = ErrorCorrectionLevel;
exports.QRCodeDecoderFormatInformation = FormatInformation;
exports.QRCodeEncoder = $scope;
exports.QRCodeEncoderQRCode = QRCode;
exports.QRCodeMaskUtil = MaskUtil;
exports.QRCodeMatrixUtil = MatrixUtil;
exports.QRCodeMode = ModeEnum;
exports.QRCodeReader = Message;
exports.QRCodeVersion = Version;
exports.QRCodeWriter = CollectionView;
exports.RGBLuminanceSource = o;
exports.RSS14Reader = tile;
exports.RSSExpandedReader = Queue;
exports.ReaderException = f;
exports.ReedSolomonDecoder = Uint16Array;
exports.ReedSolomonEncoder = Test;
exports.ReedSolomonException = Parser;
exports.Result = result;
/** @type {(undefined|{BYTE_SEGMENTS: number, ERROR_CORRECTION_LEVEL: number, ISSUE_NUMBER: number, ORIENTATION: number, OTHER: number, PDF417_EXTRA_METADATA: number, POSSIBLE_COUNTRY: number, STRUCTURED_APPEND_PARITY: number, STRUCTURED_APPEND_SEQUENCE: number, SUGGESTED_PRICE: number, UPC_EAN_EXTENSION: number})} */
exports.ResultMetadataType = value;
exports.ResultPoint = type;
exports.StringUtils = _;
exports.UnsupportedOperationException = buffer;
exports.VideoInputDevice = Form;
exports.WhiteRectangleDetector = Element;
exports.WriterException = RegExp;
exports.ZXingArrays = b;
exports.ZXingCharset = Class;
exports.ZXingInteger = scope;
/** @type {function(): undefined} */
exports.ZXingStandardCharsets = encoding;
exports.ZXingStringBuilder = Buffer;
exports.ZXingStringEncoding = parser;
exports.ZXingSystem = System;
Object.defineProperty(exports, "__esModule", {
	value: true
});
});
"use strict";
/**
 * @param {string} obj
 * @return {?}
 */
function _typeof(obj) {
	"@babel/helpers - typeof";
	return _typeof = "function" == typeof Symbol && "symbol" == _typeof2(Symbol.iterator) ? function (obj) {
		return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
	} : function (obj) {
		return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
	}, _typeof(obj);
}
/**
 * @param {!AudioNode} instance
 * @param {!Function} Constructor
 * @return {undefined}
 */
function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}
/**
 * @param {!Function} obj
 * @param {string} props
 * @return {undefined}
 */
function _defineProperties(obj, props) {
	var descriptor;
	/** @type {number} */
	var i = 0;
	for (; i < props.length; i++) {
		descriptor = props[i];
		descriptor.enumerable = descriptor.enumerable || false;
		/** @type {boolean} */
		descriptor.configurable = true;
		if ("value" in descriptor) {
			/** @type {boolean} */
			descriptor.writable = true;
		}
		Object.defineProperty(obj, descriptor.key, descriptor);
	}
}
/**
 * @param {!Function} Constructor
 * @param {!Function} protoProps
 * @param {!Function} staticProps
 * @return {?}
 */
function _createClass(Constructor, protoProps, staticProps) {
	return protoProps && _defineProperties(Constructor.prototype, protoProps), staticProps && _defineProperties(Constructor, staticProps), Constructor;
}
/**
 * @param {!Object} obj
 * @param {string} key
 * @param {?} value
 * @return {?}
 */
function _defineProperty(obj, key, value) {
	return key in obj ? Object.defineProperty(obj, key, {
		value: value,
		enumerable: true,
		configurable: true,
		writable: true
	}) : obj[key] = value, obj;
}
var Html5Qrcode = function () {
	/**
	 * @param {?} md
	 * @param {(number|string)} type
	 * @return {undefined}
	 */
	function self(md, type) {
		if (_classCallCheck(this, self), !ZXing) {
			throw "Use html5qrcode.min.js without edit, ZXing not found.";
		}
		/** @type {!Map} */
		var content = new Map;
		/** @type {!Array} */
		var newLimit = [ZXing.BarcodeFormat.QR_CODE, ZXing.BarcodeFormat.AZTEC, ZXing.BarcodeFormat.CODABAR, ZXing.BarcodeFormat.CODE_39, ZXing.BarcodeFormat.CODE_93, ZXing.BarcodeFormat.CODE_128, ZXing.BarcodeFormat.DATA_MATRIX, ZXing.BarcodeFormat.MAXICODE, ZXing.BarcodeFormat.ITF, ZXing.BarcodeFormat.EAN_13, ZXing.BarcodeFormat.EAN_8, ZXing.BarcodeFormat.PDF_417, ZXing.BarcodeFormat.RSS_14, ZXing.BarcodeFormat.RSS_EXPANDED, ZXing.BarcodeFormat.UPC_A, ZXing.BarcodeFormat.UPC_E, ZXing.BarcodeFormat.UPC_EAN_EXTENSION];
		content.set(ZXing.DecodeHintType.POSSIBLE_FORMATS, newLimit);
		this.qrcode = new ZXing.MultiFormatReader(type);
		this.qrcode.setHints(content);
		this._elementId = md;
		/** @type {null} */
		this._foreverScanTimeout = null;
		/** @type {null} */
		this._localMediaStream = null;
		/** @type {boolean} */
		this._shouldScan = true;
		this._url = window.URL || window.webkitURL || window.mozURL || window.msURL;
		this._userMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
		/** @type {boolean} */
		this._isScanning = false;
		/** @type {boolean} */
		self.VERBOSE = true === type;
	}
	return _createClass(self, [{
		key: "start",
		value: function init(query, options, callback, fn) {
			var view = this;
			if (!query) {
				throw "cameraIdOrConfig is required";
			}
			if (!callback || "function" != typeof callback) {
				throw "qrCodeSuccessCallback is required and should be a function.";
			}
			if (!fn) {
				/** @type {function(this:Console, ...*): undefined} */
				fn = console.log;
			}
			this._clearElement();
			var that = this;
			var data = options ? options : {};
			data.fps = data.fps ? data.fps : self.SCAN_DEFAULT_FPS;
			/** @type {boolean} */
			var Timeout = false;
			if (data.videoConstraints) {
				if (this._isMediaStreamConstraintsValid(data.videoConstraints)) {
					/** @type {boolean} */
					Timeout = true;
				} else {
					self._logError("'videoConstraints' is not valid 'MediaStreamConstraints, it will be ignored.'", true);
				}
			}
			/** @type {boolean} */
			var err = Timeout;
			/** @type {boolean} */
			var in_greenhouse = null != data.qrbox;
			/** @type {(Element|null)} */
			var element = document.getElementById(this._elementId);
			var i = element.clientWidth ? element.clientWidth : self.DEFAULT_WIDTH;
			if (element.style.position = "relative", this._shouldScan = true, this._element = element, in_greenhouse) {
				var length = data.qrbox;
				if (length < self.MIN_QR_BOX_SIZE) {
					throw "minimum size of 'config.qrbox' is" + " ".concat(self.MIN_QR_BOX_SIZE, "px.");
				}
				if (length > i) {
					throw "'config.qrbox' should not be greater than the width of the HTML element.";
				}
			}
			/**
			 * @param {number} min
			 * @param {number} max
			 * @return {undefined}
			 */
			var log = function waitUntilVideoElementIsRendered(min, max) {
				var date = data.qrbox;
				if (date > max) {
					console.warn("[Html5Qrcode] config.qrboxsize is greater than video height. Shading will be ignored");
				}
				/** @type {boolean} */
				var column = in_greenhouse && date <= max;
				var container = column ? view._getShadedRegionBounds(min, max, date) : {
					x: 0,
					y: 0,
					width: min,
					height: max
				};
				var i = view._createCanvasElement(container.width, container.height);
				var ctx = i.getContext("2d");
				ctx.canvas.width = container.width;
				ctx.canvas.height = container.height;
				element.append(i);
				if (column) {
					view._possiblyInsertShadingElement(element, min, max, date);
				}
				that._qrRegion = container;
				that._context = ctx;
				that._canvasElement = i;
			};
			/**
			 * @return {?}
			 */
			var iterate = function decode() {
				try {
					var gray = new ZXing.HTMLCanvasElementLuminanceSource(that._canvasElement);
					var armored_key = new ZXing.BinaryBitmap(new ZXing.HybridBinarizer(gray));
					var actionConfig = that.qrcode.decode(armored_key);
					return callback(actionConfig.text), view._possiblyUpdateShaders(true), true;
				} catch (i) {
					return view._possiblyUpdateShaders(false), fn("QR code parse error, error = ".concat(i)), false;
				}
			};
			/**
			 * @return {undefined}
			 */
			var assert = function onResize() {
				if (that._shouldScan) {
					if (that._localMediaStream) {
						var video = that._videoElement;
						/** @type {number} */
						var widthPlusPadding = video.videoWidth / video.clientWidth;
						/** @type {number} */
						var heightPlusPadding = video.videoHeight / video.clientHeight;
						/** @type {number} */
						var draw_width = that._qrRegion.width * widthPlusPadding;
						/** @type {number} */
						var draw_height = that._qrRegion.height * heightPlusPadding;
						/** @type {number} */
						var sunMeterX = that._qrRegion.x * widthPlusPadding;
						/** @type {number} */
						var yJar = that._qrRegion.y * heightPlusPadding;
						that._context.drawImage(that._videoElement, sunMeterX, yJar, draw_width, draw_height, 0, 0, that._qrRegion.width, that._qrRegion.height);
						if (!(iterate() || true === data.disableFlip)) {
							view._context.translate(view._context.canvas.width, 0);
							view._context.scale(-1, 1);
							iterate();
						}
					}
					/** @type {number} */
					that._foreverScanTimeout = setTimeout(onResize, self._getTimeoutFps(data.fps));
				}
			};
			/**
			 * @param {string} stream
			 * @return {?}
			 */
			var r = function main(stream) {
				return new Promise(function (done, handler) {
					/**
					 * @return {undefined}
					 */
					var repairDb = function reset() {
						var element = view._createVideoElement(i);
						that._element.append(element);
						element.onabort = handler;
						element.onerror = handler;
						/**
						 * @return {undefined}
						 */
						element.onplaying = function () {
							var value = element.clientWidth;
							var size = element.clientHeight;
							log(value, size);
							assert();
							done();
						};
						/** @type {string} */
						element.srcObject = stream;
						element.play();
						that._videoElement = element;
					};
					if (that._localMediaStream = stream, err || !data.aspectRatio) {
						repairDb();
					} else {
						var fields = {
							aspectRatio: data.aspectRatio
						};
						var validatable = stream.getVideoTracks()[0];
						validatable.applyConstraints(fields).then(function () {
							return repairDb();
						})["catch"](function (contextReference) {
							console.log("[Warning] [Html5Qrcode] Constriants could not be satisfied, ignoring constraints", contextReference);
							repairDb();
						});
					}
				});
			};
			return new Promise(function (saveNotifs, expect) {
				if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
					var args = err ? data.videoConstraints : that._createVideoConstraints(query);
					navigator.mediaDevices.getUserMedia({
						audio: false,
						video: args
					}).then(function (t) {
						r(t).then(function () {
							/** @type {boolean} */
							that._isScanning = true;
							saveNotifs();
						})["catch"](expect);
					})["catch"](function (ts) {
						expect("Error getting userMedia, error = ".concat(ts));
					});
				} else {
					if (navigator.getUserMedia) {
						if ("string" != typeof query) {
							throw "The device doesn't support navigator.mediaDevices, only supported cameraIdOrConfig in this case is deviceId parameter (string).";
						}
						navigator.getUserMedia({
							video: {
								optional: [{
									sourceId: query
								}]
							}
						}, function (t) {
							r(t).then(function () {
								/** @type {boolean} */
								that._isScanning = true;
								saveNotifs();
							})["catch"](expect);
						}, function (ts) {
							expect("Error getting userMedia, error = ".concat(ts));
						});
					} else {
						expect("Web camera streaming not supported by the browser.");
					}
				}
			});
		}
	}, {
		key: "stop",
		value: function update() {
			/** @type {boolean} */
			this._shouldScan = false;
			clearTimeout(this._foreverScanTimeout);
			var _this = this;
			return new Promise(function (constrain) {
				var versionNumber = _this._localMediaStream.getVideoTracks().length;
				/** @type {number} */
				var siteVersion = 0;
				/**
				 * @return {undefined}
				 */
				var prepareWrite = function show() {
					for (; _this._element.getElementsByClassName(self.SHADED_REGION_CLASSNAME).length;) {
						var _childToRemove = _this._element.getElementsByClassName(self.SHADED_REGION_CLASSNAME)[0];
						_this._element.removeChild(_childToRemove);
					}
				};
				/**
				 * @return {undefined}
				 */
				var gotoNewOfflinePage = function finalize() {
					/** @type {null} */
					_this._localMediaStream = null;
					_this._element.removeChild(_this._videoElement);
					_this._element.removeChild(_this._canvasElement);
					prepareWrite();
					/** @type {boolean} */
					_this._isScanning = false;
					if (_this._qrRegion) {
						/** @type {null} */
						_this._qrRegion = null;
					}
					if (_this._context) {
						/** @type {null} */
						_this._context = null;
					}
					constrain(true);
				};
				_this._localMediaStream.getVideoTracks().forEach(function (incoming_item) {
					incoming_item.stop();
					++siteVersion;
					if (siteVersion >= versionNumber) {
						gotoNewOfflinePage();
					}
				});
			});
		}
	}, {
		key: "scanFile",
		value: function loadImage(file, error) {
			var that = this;
			if (!file || !(file instanceof File)) {
				throw "imageFile argument is mandatory and should be instance of File. Use 'event.target.files[0]'";
			}
			if (error = void 0 === error || error, that._isScanning) {
				throw "Close ongoing scan before scanning a file.";
			}
			/**
			 * @param {number} n
			 * @param {number} r
			 * @param {number} size
			 * @param {number} d
			 * @return {?}
			 */
			var transform = function callback(n, r, size, d) {
				if (n <= size && r <= d) {
					/** @type {number} */
					var audioOffsetX = (size - n) / 2;
					/** @type {number} */
					var languageOffsetY = (d - r) / 2;
					return {
						x: audioOffsetX,
						y: languageOffsetY,
						width: n,
						height: r
					};
				}
				/** @type {number} */
				var x = n;
				/** @type {number} */
				var i = r;
				return n > size && (r = size / n * r, n = size), r > d && (n = d / r * n, r = d), self._log("Image downsampled from " + "".concat(x, "X").concat(i) + " to ".concat(n, "X").concat(r, ".")), callback(n, r, size, d);
			};
			return new Promise(function (expect, failure) {
				that._possiblyCloseLastScanImageFile();
				that._clearElement();
				/** @type {!AudioNode} */
				that._lastScanImageFile = file;
				/** @type {!Image} */
				var img = new Image;
				/**
				 * @return {undefined}
				 */
				img.onload = function () {
					/** @type {function(...?): number} */
					var min = Math.max;
					/** @type {number} */
					var x = img.width;
					/** @type {number} */
					var y = img.height;
					/** @type {(Element|null)} */
					var element = document.getElementById(that._elementId);
					var options = element.clientWidth ? element.clientWidth : self.DEFAULT_WIDTH;
					/** @type {number} */
					var a = min(element.clientHeight ? element.clientHeight : y, self.FILE_SCAN_MIN_HEIGHT);
					var pos = transform(x, y, options, a);
					if (error) {
						var b = that._createCanvasElement(options, a, "qr-canvas-visible");
						/** @type {string} */
						b.style.display = "inline-block";
						element.appendChild(b);
						var context = b.getContext("2d");
						context.canvas.width = options;
						/** @type {number} */
						context.canvas.height = a;
						context.drawImage(img, 0, 0, x, y, pos.x, pos.y, pos.width, pos.height);
					}
					var node = that._createCanvasElement(pos.width, pos.height);
					element.appendChild(node);
					var context = node.getContext("2d");
					context.canvas.width = pos.width;
					context.canvas.height = pos.height;
					context.drawImage(img, 0, 0, x, y, 0, 0, pos.width, pos.height);
					try {
						var beforeAfterEvent = new ZXing.HTMLCanvasElementLuminanceSource(node);
						var id = new ZXing.BinaryBitmap(new ZXing.HybridBinarizer(beforeAfterEvent));
						var todo = that.qrcode.decode(id);
						expect(todo.text);
					} catch (error) {
						failure("QR code parse error, error = ".concat(error));
					}
				};
				img.onerror = failure;
				img.onabort = failure;
				img.onstalled = failure;
				img.onsuspend = failure;
				/** @type {string} */
				img.src = URL.createObjectURL(file);
			});
		}
	}, {
		key: "clear",
		value: function value() {
			this._clearElement();
		}
	}, {
		key: "getRunningTrackCapabilities",
		value: function _addStream() {
			if (null == this._localMediaStream) {
				throw "Scanning is not in running state, call this API only when QR code scanning using camera is in running state.";
			}
			if (0 == this._localMediaStream.getVideoTracks().length) {
				throw "No video tracks found";
			}
			var NativeAudio = this._localMediaStream.getVideoTracks()[0];
			return NativeAudio.getCapabilities();
		}
	}, {
		key: "applyVideoConstraints",
		value: function init(fields) {
			var e = this;
			if (!fields) {
				throw "videoConstaints is required argument.";
			} else {
				if (!this._isMediaStreamConstraintsValid(fields)) {
					throw "invalid videoConstaints passed, check logs for more details";
				}
			}
			if (null == this._localMediaStream) {
				throw "Scanning is not in running state, call this API only when QR code scanning using camera is in running state.";
			}
			if (0 == this._localMediaStream.getVideoTracks().length) {
				throw "No video tracks found";
			}
			return new Promise(function (saveNotifs, view) {
				if ("aspectRatio" in fields) {
					return void view("Chaning 'aspectRatio' in run-time is not yet supported.");
				}
				var validatable = e._localMediaStream.getVideoTracks()[0];
				validatable.applyConstraints(fields).then(function (notifications) {
					saveNotifs(notifications);
				})["catch"](function (obj) {
					view(obj);
				});
			});
		}
	}, {
		key: "_clearElement",
		value: function checkForSlideComment() {
			if (this._isScanning) {
				throw "Cannot clear while scan is ongoing, close it first.";
			}
			/** @type {(Element|null)} */
			var contentElement = document.getElementById(this._elementId);
			/** @type {string} */
			contentElement.innerHTML = "";
		}
	}, {
		key: "_createCanvasElement",
		value: function capture(level, x, name) {
			/** @type {!Element} */
			var tmp = document.createElement("canvas");
			return tmp.style.width = "".concat(level, "px"), tmp.style.height = "".concat(x, "px"), tmp.style.display = "none", tmp.id = null == name ? "qr-canvas" : name, tmp;
		}
	}, {
		key: "_createVideoElement",
		value: function reset(x) {
			/** @type {!Element} */
			var video = document.createElement("video");
			return video.style.width = "".concat(x, "px"), video.muted = true, video.playsInline = true, video;
		}
	}, {
		key: "_getShadedRegionBounds",
		value: function anotherConstructor$0(c, a, b) {
			if (b > c || b > a) {
				throw "'config.qrbox' should not be greater than the width and height of the HTML element.";
			}
			return {
				x: (c - b) / 2,
				y: (a - b) / 2,
				width: b,
				height: b
			};
		}
	}, {
		key: "_possiblyInsertShadingElement",
		value: function init(cell, evt, time, startTime) {
			if (!(1 > evt - startTime || 1 > time - startTime)) {
				/** @type {!Element} */
				var div = document.createElement("div");
				if (div.style.position = "absolute", div.style.borderLeft = "".concat((evt - startTime) / 2, "px solid #0000007a"), div.style.borderRight = "".concat((evt - startTime) / 2, "px solid #0000007a"), div.style.borderTop = "".concat((time - startTime) / 2, "px solid #0000007a"), div.style.borderBottom = "".concat((time - startTime) / 2, "px solid #0000007a"), div.style.boxSizing = "border-box", div.style.top = "0px", div.style.bottom = "0px", div.style.left = "0px", div.style.right = "0px", div.id =
					"".concat(self.SHADED_REGION_CLASSNAME), 11 > evt - startTime || 11 > time - startTime) {
					/** @type {boolean} */
					this.hasBorderShaders = false;
				} else {
					this._insertShaderBorders(div, 40, 5, -5, 0, true);
					this._insertShaderBorders(div, 40, 5, -5, 0, false);
					this._insertShaderBorders(div, 40, 5, startTime + 5, 0, true);
					this._insertShaderBorders(div, 40, 5, startTime + 5, 0, false);
					this._insertShaderBorders(div, 5, 45, -5, -5, true);
					this._insertShaderBorders(div, 5, 45, startTime + 5 - 40, -5, true);
					this._insertShaderBorders(div, 5, 45, -5, -5, false);
					this._insertShaderBorders(div, 5, 45, startTime + 5 - 40, -5, false);
					/** @type {boolean} */
					this.hasBorderShaders = true;
				}
				cell.append(div);
			}
		}
	}, {
		key: "_insertShaderBorders",
		value: function initialize(h, text, pattern, x, key, cb_wrap) {
			/** @type {!Element} */
			var b = document.createElement("div");
			/** @type {string} */
			b.style.position = "absolute";
			b.style.backgroundColor = self.BORDER_SHADER_DEFAULT_COLOR;
			/** @type {string} */
			b.style.width = "".concat(text, "px");
			/** @type {string} */
			b.style.height = "".concat(pattern, "px");
			/** @type {string} */
			b.style.top = "".concat(x, "px");
			if (cb_wrap) {
				/** @type {string} */
				b.style.left = "".concat(key, "px");
			} else {
				/** @type {string} */
				b.style.right = "".concat(key, "px");
			}
			if (!this.borderShaders) {
				/** @type {!Array} */
				this.borderShaders = [];
			}
			this.borderShaders.push(b);
			h.appendChild(b);
		}
	}, {
		key: "_possiblyUpdateShaders",
		value: function paint(err) {
			if (!(this.qrMatch === err)) {
				if (this.hasBorderShaders && this.borderShaders && this.borderShaders.length) {
					this.borderShaders.forEach(function (c) {
						c.style.backgroundColor = err ? self.BORDER_SHADER_MATCH_COLOR : self.BORDER_SHADER_DEFAULT_COLOR;
					});
				}
				this.qrMatch = err;
			}
		}
	}, {
		key: "_possiblyCloseLastScanImageFile",
		value: function saveAudio() {
			if (this._lastScanImageFile) {
				URL.revokeObjectURL(this._lastScanImageFile);
				/** @type {null} */
				this._lastScanImageFile = null;
			}
		}
	}, {
		key: "_createVideoConstraints",
		value: function render(obj) {
			if ("string" == typeof obj) {
				return {
					deviceId: {
						exact: obj
					}
				};
			}
			if ("object" == _typeof(obj)) {
				var option = {
					user: true,
					environment: true
				};
				/**
				 * @param {?} a
				 * @return {?}
				 */
				var c = function c(a) {
					if (a in option) {
						return true;
					}
					throw "config has invalid 'facingMode' value = " + "'".concat(a, "'");
				};
				/** @type {!Array<string>} */
				var d = Object.keys(obj);
				if (1 != d.length) {
					throw "'cameraIdOrConfig' object should have exactly 1 key," + " if passed as an object, found ".concat(d.length, " keys");
				}
				/** @type {string} */
				var i = Object.keys(obj)[0];
				if ("facingMode" != i && "deviceId" != i) {
					throw "Only '".concat("facingMode", "' and '").concat("deviceId", "' ") + " are supported for 'cameraIdOrConfig'";
				}
				if ("facingMode" == i) {
					var data = obj[i];
					if ("string" == typeof data) {
						if (c(data)) {
							return {
								facingMode: data
							};
						}
					} else {
						if ("object" != _typeof(data)) {
							var type = _typeof(data);
							throw "Invalid type of 'facingMode' = ".concat(type);
						} else {
							if (!("exact" in data)) {
								throw "'facingMode' should be string or object with" + " ".concat("exact", " as key.");
							} else {
								if (c(data.exact)) {
									return {
										facingMode: {
											exact: data.exact
										}
									};
								}
							}
						}
					}
				} else {
					var data = obj[i];
					if ("string" == typeof data) {
						return {
							deviceId: data
						};
					}
					if ("object" == _typeof(data)) {
						if ("exact" in data) {
							return {
								deviceId: {
									exact: data.exact
								}
							};
						}
						throw "'deviceId' should be string or object with" + " ".concat("exact", " as key.");
					} else {
						var type = _typeof(data);
						throw "Invalid type of 'deviceId' = ".concat(type);
					}
				}
			} else {
				var type = _typeof(obj);
				throw "Invalid type of 'cameraIdOrConfig' = ".concat(type);
			}
		}
	}, {
		key: "_isMediaStreamConstraintsValid",
		value: function start(data) {
			if (!data) {
				return self._logError("Empty videoConstraints", true), false;
			}
			if ("object" !== _typeof(data)) {
				var type = _typeof(data);
				return self._logError("videoConstraints should be of type object, the " + "object passed is of type ".concat(type, "."), true), false;
			}
			var c;
			/** @type {!Array} */
			var visitedImports = ["autoGainControl", "channelCount", "echoCancellation", "latency", "noiseSuppression", "sampleRate", "sampleSize", "volume"];
			/** @type {!Set} */
			var knownFileExtensions = new Set(visitedImports);
			/** @type {!Array<string>} */
			var inData = Object.keys(data);
			/** @type {number} */
			var i = 0;
			for (; i < inData.length; i++) {
				if (c = inData[i], knownFileExtensions.has(c)) {
					return self._logError("".concat(c, " is not supported videoConstaints."), true), false;
				}
			}
			return true;
		}
	}], [{
		key: "getCameras",
		value: function getLocalStream() {
			var $widget = this;
			return new Promise(function (clone, dispatch) {
				if (navigator.mediaDevices && navigator.mediaDevices.enumerateDevices && navigator.mediaDevices.getUserMedia) {
					$widget._log("navigator.mediaDevices used");
					navigator.mediaDevices.getUserMedia({
						audio: false,
						video: true
					}).then(function (mediaStream) {
						/**
						 * @return {?}
						 */
						mediaStream.oninactive = function () {
							return $widget._log("All streams closed");
						};
						/**
						 * @param {!MediaStream} stream
						 * @return {undefined}
						 */
						var killStream = function onStreamSuccessCallback(stream) {
							var track;
							var _ref = stream.getVideoTracks();
							/** @type {number} */
							var _i = 0;
							for (; _i < _ref.length; _i++) {
								track = _ref[_i];
								/** @type {boolean} */
								track.enabled = false;
								track.stop();
								stream.removeTrack(track);
							}
						};
						navigator.mediaDevices.enumerateDevices().then(function (targets) {
							var node;
							/** @type {!Array} */
							var options = [];
							/** @type {number} */
							var i = 0;
							for (; i < targets.length; i++) {
								/** @type {!MediaDeviceInfo} */
								node = targets[i];
								if ("videoinput" == node.kind) {
									options.push({
										id: node.deviceId,
										label: node.label
									});
								}
							}
							$widget._log("".concat(options.length, " results found"));
							killStream(mediaStream);
							clone(options);
						})["catch"](function (options) {
							dispatch("".concat(options.name, " : ").concat(options.message));
						});
					})["catch"](function (options) {
						dispatch("".concat(options.name, " : ").concat(options.message));
					});
				} else {
					if (MediaStreamTrack && MediaStreamTrack.getSources) {
						$widget._log("MediaStreamTrack.getSources used");
						/**
						 * @param {!NodeList} data
						 * @return {undefined}
						 */
						var enableMedia = function updateStreamFn(data) {
							var track;
							/** @type {!Array} */
							var options = [];
							/** @type {number} */
							var i = 0;
							for (; i !== data.length; ++i) {
								track = data[i];
								if ("video" === track.kind) {
									options.push({
										id: track.id,
										label: track.label
									});
								}
							}
							$widget._log("".concat(options.length, " results found"));
							clone(options);
						};
						MediaStreamTrack.getSources(enableMedia);
					} else {
						$widget._log("unable to query supported devices.");
						dispatch("unable to query supported devices.");
					}
				}
			});
		}
	}, {
		key: "_getTimeoutFps",
		value: function makeColorMaterial(a) {
			return 1e3 / a;
		}
	}, {
		key: "_log",
		value: function _check(data) {
			if (self.VERBOSE) {
				console.log(data);
			}
		}
	}, {
		key: "_logError",
		value: function error(message, object) {
			if (self.VERBOSE || true === object) {
				console.error(message);
			}
		}
	}]), self;
}();
_defineProperty(Html5Qrcode, "DEFAULT_WIDTH", 300), _defineProperty(Html5Qrcode, "DEFAULT_WIDTH_OFFSET", 2), _defineProperty(Html5Qrcode, "FILE_SCAN_MIN_HEIGHT", 300), _defineProperty(Html5Qrcode, "SCAN_DEFAULT_FPS", 2), _defineProperty(Html5Qrcode, "MIN_QR_BOX_SIZE", 50), _defineProperty(Html5Qrcode, "SHADED_LEFT", 1), _defineProperty(Html5Qrcode, "SHADED_RIGHT", 2), _defineProperty(Html5Qrcode, "SHADED_TOP", 3), _defineProperty(Html5Qrcode, "SHADED_BOTTOM", 4), _defineProperty(Html5Qrcode, "SHADED_REGION_CLASSNAME",
	"qr-shaded-region"), _defineProperty(Html5Qrcode, "VERBOSE", false), _defineProperty(Html5Qrcode, "BORDER_SHADER_DEFAULT_COLOR", "#ffffff"), _defineProperty(Html5Qrcode, "BORDER_SHADER_MATCH_COLOR", "rgb(90, 193, 56)");
"use strict";
/**
 * @param {!AudioNode} instance
 * @param {!Function} Constructor
 * @return {undefined}
 */
function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}
/**
 * @param {!Function} obj
 * @param {string} props
 * @return {undefined}
 */
function _defineProperties(obj, props) {
	var descriptor;
	/** @type {number} */
	var i = 0;
	for (; i < props.length; i++) {
		descriptor = props[i];
		descriptor.enumerable = descriptor.enumerable || false;
		/** @type {boolean} */
		descriptor.configurable = true;
		if ("value" in descriptor) {
			/** @type {boolean} */
			descriptor.writable = true;
		}
		Object.defineProperty(obj, descriptor.key, descriptor);
	}
}
/**
 * @param {!Function} Constructor
 * @param {!Function} protoProps
 * @param {!Function} staticProps
 * @return {?}
 */
function _createClass(Constructor, protoProps, staticProps) {
	return protoProps && _defineProperties(Constructor.prototype, protoProps), staticProps && _defineProperties(Constructor, staticProps), Constructor;
}
/**
 * @param {!Object} obj
 * @param {string} key
 * @param {?} value
 * @return {?}
 */
function _defineProperty(obj, key, value) {
	return key in obj ? Object.defineProperty(obj, key, {
		value: value,
		enumerable: true,
		configurable: true,
		writable: true
	}) : obj[key] = value, obj;
}
var Html5QrcodeScanner = function () {
	/**
	 * @param {string} id
	 * @param {!Object} config
	 * @param {(number|string)} steps
	 * @return {undefined}
	 */
	function Player(id, config, steps) {
		if (_classCallCheck(this, Player), this.elementId = id, this.config = config, this.verbose = true === steps, !document.getElementById(id)) {
			throw "HTML Element with id=".concat(id, " not found");
		}
		this.currentScanType = Player.SCAN_TYPE_CAMERA;
		/** @type {boolean} */
		this.sectionSwapAllowed = true;
		this.section = void 0;
		this.html5Qrcode = void 0;
		this.qrCodeSuccessCallback = void 0;
		this.qrCodeErrorCallback = void 0;
	}
	return _createClass(Player, [{
		key: "render",
		value: function createPopperElement(id, settings) {
			var args = this;
			this.lastMatchFound = void 0;
			/**
			 * @param {number} c
			 * @return {undefined}
			 */
			this.qrCodeSuccessCallback = function (c) {
				if (args.__setStatus("MATCH", Player.STATUS_SUCCESS), id) {
					id(c);
				} else {
					if (args.lastMatchFound == c) {
						return;
					}
					/** @type {number} */
					args.lastMatchFound = c;
					args.__setHeaderMessage("Last Match: ".concat(c), Player.STATUS_SUCCESS);
				}
			};
			/**
			 * @param {?} current
			 * @return {undefined}
			 */
			this.qrCodeErrorCallback = function (current) {
				args.__setStatus("Scanning");
				if (settings) {
					settings(current);
				}
			};
			/** @type {(Element|null)} */
			var element = document.getElementById(this.elementId);
			/** @type {string} */
			element.innerHTML = "";
			this.__createBasicLayout(element);
			this.html5Qrcode = new Html5Qrcode(this.__getScanRegionId(), this.verbose);
		}
	}, {
		key: "clear",
		value: function wrapper() {
			var spec = this;
			var core = this;
			/**
			 * @return {undefined}
			 */
			var gotoNewOfflinePage = function clear() {
				/** @type {(Element|null)} */
				var block = document.getElementById(spec.elementId);
				if (block) {
					/** @type {string} */
					block.innerHTML = "";
					spec.__resetBasicLayout(block);
				}
			};
			if (this.html5Qrcode) {
				return new Promise(function (negater, userJQuery) {
					if (core.html5Qrcode._isScanning) {
						core.html5Qrcode.stop().then(function () {
							core.html5Qrcode.clear();
							gotoNewOfflinePage();
							negater();
						})["catch"](function (selector) {
							if (core.verbose) {
								console.error("Unable to stop qrcode scanner", selector);
							}
							userJQuery(selector);
						});
					}
				});
			}
		}
	}, {
		key: "__createBasicLayout",
		value: function drawPicker(component) {
			/** @type {string} */
			component.style.position = "relative";
			/** @type {string} */
			component.style.padding = "0px";
			/** @type {string} */
			component.style.border = "1px solid silver";
			this.__createHeader(component);
			/** @type {!Element} */
			var container = document.createElement("div");
			var ksMessageId = this.__getScanRegionId();
			container.id = ksMessageId;
			/** @type {string} */
			container.style.width = "100%";
			/** @type {string} */
			container.style.minHeight = "100px";
			/** @type {string} */
			container.style.textAlign = "center";
			component.appendChild(container);
			this.__insertCameraScanImageToScanRegion();
			/** @type {!Element} */
			var d = document.createElement("div");
			var klassId = this.__getDashboardId();
			d.id = klassId;
			/** @type {string} */
			d.style.width = "100%";
			component.appendChild(d);
			this.__setupInitialDashboard(d);
		}
	}, {
		key: "__resetBasicLayout",
		value: function _findLineHeight(annotateDIV) {
			/** @type {string} */
			annotateDIV.style.border = "none";
		}
	}, {
		key: "__setupInitialDashboard",
		value: function value(saveEvenIfSeemsUnchanged) {
			this.__createSection(saveEvenIfSeemsUnchanged);
			this.__createSectionControlPanel();
			this.__createSectionSwap();
		}
	}, {
		key: "__createHeader",
		value: function init($generic) {
			/** @type {!Element} */
			var div = document.createElement("div");
			/** @type {string} */
			div.style.textAlign = "left";
			/** @type {string} */
			div.style.margin = "0px";
			/** @type {string} */
			div.style.padding = "5px";
			/** @type {string} */
			div.style.fontSize = "20px";
			/** @type {string} */
			div.style.borderBottom = "1px solid rgba(192, 192, 192, 0.18)";
			$generic.appendChild(div);
			/** @type {!Element} */
			var fileSizeSpan = document.createElement("span");
			/** @type {string} */
			fileSizeSpan.innerHTML = "QR Code Scanner";
			div.appendChild(fileSizeSpan);
			/** @type {!Element} */
			var container = document.createElement("span");
			container.id = this.__getStatusSpanId();
			/** @type {string} */
			container.style.float = "right";
			/** @type {string} */
			container.style.padding = "5px 7px";
			/** @type {string} */
			container.style.fontSize = "14px";
			/** @type {string} */
			container.style.background = "#dedede6b";
			/** @type {string} */
			container.style.border = "1px solid #00000000";
			/** @type {string} */
			container.style.color = "rgb(17, 17, 17)";
			div.appendChild(container);
			this.__setStatus("IDLE");
			/** @type {!Element} */
			var el = document.createElement("div");
			el.id = this.__getHeaderMessageContainerId();
			/** @type {string} */
			el.style.display = "none";
			/** @type {string} */
			el.style.fontSize = "14px";
			/** @type {string} */
			el.style.padding = "2px 10px";
			/** @type {string} */
			el.style.marginTop = "4px";
			/** @type {string} */
			el.style.borderTop = "1px solid #f6f6f6";
			div.appendChild(el);
		}
	}, {
		key: "__createSection",
		value: function init(nav) {
			/** @type {!Element} */
			var box1 = document.createElement("div");
			box1.id = this.__getDashboardSectionId();
			/** @type {string} */
			box1.style.width = "100%";
			/** @type {string} */
			box1.style.padding = "10px";
			/** @type {string} */
			box1.style.textAlign = "left";
			nav.appendChild(box1);
		}
	}, {
		key: "__createSectionControlPanel",
		value: function main() {
			var self = this;
			/** @type {(Element|null)} */
			var obj_title = document.getElementById(this.__getDashboardSectionId());
			/** @type {!Element} */
			var d = document.createElement("div");
			obj_title.appendChild(d);
			/** @type {!Element} */
			var div = document.createElement("div");
			div.id = this.__getDashboardSectionCameraScanRegionId();
			/** @type {string} */
			div.style.display = this.currentScanType == Player.SCAN_TYPE_CAMERA ? "block" : "none";
			d.appendChild(div);
			/** @type {!Element} */
			var controls = document.createElement("div");
			/** @type {string} */
			controls.style.textAlign = "center";
			/** @type {!Element} */
			var btnPause = document.createElement("button");
			/** @type {string} */
			btnPause.innerHTML = "Request Camera Permissions";
			btnPause.addEventListener("click", function () {
				/** @type {boolean} */
				btnPause.disabled = true;
				self.__setStatus("PERMISSION");
				self.__setHeaderMessage("Requesting camera permissions...");
				Html5Qrcode.getCameras().then(function (c) {
					self.__setStatus("IDLE");
					self.__resetHeaderMessage();
					if (c && 0 != c.length) {
						div.removeChild(controls);
						self.__renderCameraSelection(c);
					} else {
						self.__setStatus("No Cameras", Player.STATUS_WARNING);
					}
				})["catch"](function (dappId) {
					/** @type {boolean} */
					btnPause.disabled = false;
					self.__setStatus("IDLE");
					self.__setHeaderMessage(dappId, Player.STATUS_WARNING);
				});
			});
			controls.appendChild(btnPause);
			div.appendChild(controls);
			/** @type {!Element} */
			var span = document.createElement("div");
			span.id = this.__getDashboardSectionFileScanRegionId();
			/** @type {string} */
			span.style.textAlign = "center";
			/** @type {string} */
			span.style.display = this.currentScanType == Player.SCAN_TYPE_CAMERA ? "none" : "block";
			d.appendChild(span);
			/** @type {!Element} */
			var input = document.createElement("input");
			input.id = this.__getFileScanInputId();
			/** @type {string} */
			input.accept = "image/*";
			/** @type {string} */
			input.type = "file";
			/** @type {string} */
			input.style.width = "200px";
			/** @type {boolean} */
			input.disabled = this.currentScanType == Player.SCAN_TYPE_CAMERA;
			/** @type {!Element} */
			var downClick = document.createElement("span");
			/** @type {string} */
			downClick.innerHTML = "&nbsp; Select Image";
			span.appendChild(input);
			span.appendChild(downClick);
			input.addEventListener("change", function (event) {
				if (self.currentScanType === Player.SCAN_TYPE_FILE && 0 != event.target.files.length) {
					var file = event.target.files[0];
					self.html5Qrcode.scanFile(file, true).then(function (a) {
						self.__resetHeaderMessage();
						self.qrCodeSuccessCallback(a);
					})["catch"](function (newContent) {
						self.__setStatus("ERROR", Player.STATUS_WARNING);
						self.__setHeaderMessage(newContent, Player.STATUS_WARNING);
						self.qrCodeErrorCallback(newContent);
					});
				}
			});
		}
	}, {
		key: "__renderCameraSelection",
		value: function initialize(a) {
			var self = this;
			/** @type {(Element|null)} */
			var domElement = document.getElementById(this.__getDashboardSectionCameraScanRegionId());
			/** @type {string} */
			domElement.style.textAlign = "center";
			/** @type {!Element} */
			var div = document.createElement("span");
			/** @type {string} */
			div.innerHTML = "Select Camera (".concat(a.length, ") &nbsp;");
			/** @type {string} */
			div.style.marginRight = "10px";
			/** @type {!Element} */
			var c = document.createElement("select");
			c.id = this.__getCameraSelectionId();
			/** @type {number} */
			var j = 0;
			for (; j < a.length; j++) {
				var x = a[j];
				var i = x.id;
				var next = null == x.label ? i : x.label;
				/** @type {!Element} */
				var item = document.createElement("option");
				item.value = i;
				item.innerHTML = next;
				c.appendChild(item);
			}
			div.appendChild(c);
			domElement.appendChild(div);
			/** @type {!Element} */
			var controls = document.createElement("span");
			/** @type {!Element} */
			var btnPlay = document.createElement("button");
			/** @type {string} */
			btnPlay.innerHTML = "Start Scanning";
			controls.appendChild(btnPlay);
			/** @type {!Element} */
			var btnPause = document.createElement("button");
			/** @type {string} */
			btnPause.innerHTML = "Stop Scanning";
			/** @type {string} */
			btnPause.style.display = "none";
			/** @type {boolean} */
			btnPause.disabled = true;
			controls.appendChild(btnPause);
			domElement.appendChild(controls);
			btnPlay.addEventListener("click", function () {
				/** @type {boolean} */
				c.disabled = true;
				/** @type {boolean} */
				btnPlay.disabled = true;
				self._showHideScanTypeSwapLink(false);
				var failure = self.config ? self.config : {
					fps: 10,
					qrbox: 250
				};
				var d = c.value;
				self.html5Qrcode.start(d, failure, self.qrCodeSuccessCallback, self.qrCodeErrorCallback).then(function () {
					/** @type {boolean} */
					btnPause.disabled = false;
					/** @type {string} */
					btnPause.style.display = "inline-block";
					/** @type {string} */
					btnPlay.style.display = "none";
					self.__setStatus("Scanning");
				})["catch"](function (dappId) {
					self._showHideScanTypeSwapLink(true);
					/** @type {boolean} */
					c.disabled = false;
					/** @type {boolean} */
					btnPlay.disabled = false;
					self.__setStatus("IDLE");
					self.__setHeaderMessage(dappId, Player.STATUS_WARNING);
				});
			});
			btnPause.addEventListener("click", function () {
				/** @type {boolean} */
				btnPause.disabled = true;
				self.html5Qrcode.stop().then(function () {
					self._showHideScanTypeSwapLink(true);
					/** @type {boolean} */
					c.disabled = false;
					/** @type {boolean} */
					btnPlay.disabled = false;
					/** @type {string} */
					btnPause.style.display = "none";
					/** @type {string} */
					btnPlay.style.display = "inline-block";
					self.__setStatus("IDLE");
					self.__insertCameraScanImageToScanRegion();
				})["catch"](function (dappId) {
					/** @type {boolean} */
					btnPause.disabled = false;
					self.__setStatus("ERROR", Player.STATUS_WARNING);
					self.__setHeaderMessage(dappId, Player.STATUS_WARNING);
				});
			});
		}
	}, {
		key: "__createSectionSwap",
		value: function init() {
			var state = this;
			/** @type {string} */
			var str = "Scan an Image File";
			/** @type {string} */
			var msg = "Scan using camera directly";
			/** @type {(Element|null)} */
			var cur_child = document.getElementById(this.__getDashboardSectionId());
			/** @type {!Element} */
			var f = document.createElement("div");
			/** @type {string} */
			f.style.textAlign = "center";
			/** @type {!Element} */
			var div = document.createElement("a");
			/** @type {string} */
			div.style.textDecoration = "underline";
			div.id = this.__getDashboardSectionSwapLinkId();
			/** @type {string} */
			div.innerHTML = this.currentScanType == Player.SCAN_TYPE_CAMERA ? str : msg;
			/** @type {string} */
			div.href = "#scan-using-file";
			div.addEventListener("click", function () {
				return state.sectionSwapAllowed ? void (state.__setStatus("IDLE"), state.__resetHeaderMessage(), state.__getFileScanInput().value = "", state.sectionSwapAllowed = false, state.currentScanType == Player.SCAN_TYPE_CAMERA ? (state.__clearScanRegion(), state.__getFileScanInput().disabled = false, state.__getCameraScanRegion().style.display = "none", state.__getFileScanRegion().style.display = "block", div.innerHTML = msg, state.currentScanType = Player.SCAN_TYPE_FILE, state.__insertFileScanImageToScanRegion()) :
					(state.__clearScanRegion(), state.__getFileScanInput().disabled = true, state.__getCameraScanRegion().style.display = "block", state.__getFileScanRegion().style.display = "none", div.innerHTML = str, state.currentScanType = Player.SCAN_TYPE_CAMERA, state.__insertCameraScanImageToScanRegion()), state.sectionSwapAllowed = true) : void (state.verbose && console.error("Section swap called when not allowed"));
			});
			f.appendChild(div);
			cur_child.appendChild(f);
		}
	}, {
		key: "__setStatus",
		value: function callback(value, result) {
			if (!result) {
				result = Player.STATUS_DEFAULT;
			}
			/** @type {(Element|null)} */
			var p = document.getElementById(this.__getStatusSpanId());
			switch (p.innerHTML = value, result) {
				case Player.STATUS_SUCCESS:
					/** @type {string} */
					p.style.background = "#6aaf5042";
					/** @type {string} */
					p.style.color = "#477735";
					break;
				case Player.STATUS_WARNING:
					/** @type {string} */
					p.style.background = "#cb243124";
					/** @type {string} */
					p.style.color = "#cb2431";
					break;
				case Player.STATUS_DEFAULT:
				default:
					/** @type {string} */
					p.style.background = "#eef";
					/** @type {string} */
					p.style.color = "rgb(17, 17, 17)";
			}
		}
	}, {
		key: "__resetHeaderMessage",
		value: function acceptTheCall() {
			/** @type {(Element|null)} */
			var boxChild = document.getElementById(this.__getHeaderMessageContainerId());
			/** @type {string} */
			boxChild.style.display = "none";
		}
	}, {
		key: "__setHeaderMessage",
		value: function callback(html, md) {
			if (!md) {
				md = Player.STATUS_DEFAULT;
			}
			/** @type {(Element|null)} */
			var marker = document.getElementById(this.__getHeaderMessageContainerId());
			switch (marker.innerHTML = html, marker.style.display = "block", md) {
				case Player.STATUS_SUCCESS:
					/** @type {string} */
					marker.style.background = "#6aaf5042";
					/** @type {string} */
					marker.style.color = "#477735";
					break;
				case Player.STATUS_WARNING:
					/** @type {string} */
					marker.style.background = "#cb243124";
					/** @type {string} */
					marker.style.color = "#cb2431";
					break;
				case Player.STATUS_DEFAULT:
				default:
					/** @type {string} */
					marker.style.background = "#00000000";
					/** @type {string} */
					marker.style.color = "rgb(17, 17, 17)";
			}
		}
	}, {
		key: "_showHideScanTypeSwapLink",
		value: function activateBackend(value) {
			if (true !== value) {
				/** @type {boolean} */
				value = false;
			}
			/** @type {boolean} */
			this.sectionSwapAllowed = value;
			/** @type {string} */
			this.__getDashboardSectionSwapLink().style.display = value ? "inline-block" : "none";
		}
	}, {
		key: "__insertCameraScanImageToScanRegion",
		value: function updatePreviewCanvas() {
			var wedgeData = this;
			/** @type {(Element|null)} */
			var inStash = document.getElementById(this.__getScanRegionId());
			return this.cameraScanImage ? (inStash.innerHTML = "<br>", void inStash.appendChild(this.cameraScanImage)) : void (this.cameraScanImage = new Image, this.cameraScanImage.onload = function () {
				/** @type {string} */
				inStash.innerHTML = "<br>";
				inStash.appendChild(wedgeData.cameraScanImage);
			}, this.cameraScanImage.width = 64, this.cameraScanImage.style.opacity = .3, this.cameraScanImage.src = Player.ASSET_CAMERA_SCAN);
		}
	}, {
		key: "__insertFileScanImageToScanRegion",
		value: function updatePreviewCanvas() {
			var wedgeData = this;
			/** @type {(Element|null)} */
			var inStash = document.getElementById(this.__getScanRegionId());
			return this.fileScanImage ? (inStash.innerHTML = "<br>", void inStash.appendChild(this.fileScanImage)) : void (this.fileScanImage = new Image, this.fileScanImage.onload = function () {
				/** @type {string} */
				inStash.innerHTML = "<br>";
				inStash.appendChild(wedgeData.fileScanImage);
			}, this.fileScanImage.width = 64, this.fileScanImage.style.opacity = .3, this.fileScanImage.src = Player.ASSET_FILE_SCAN);
		}
	}, {
		key: "__clearScanRegion",
		value: function checkForSlideComment() {
			/** @type {(Element|null)} */
			var onPlanet = document.getElementById(this.__getScanRegionId());
			/** @type {string} */
			onPlanet.innerHTML = "";
		}
	}, {
		key: "__getDashboardSectionId",
		value: function safeConcat() {
			return "".concat(this.elementId, "__dashboard_section");
		}
	}, {
		key: "__getDashboardSectionCameraScanRegionId",
		value: function safeConcat() {
			return "".concat(this.elementId, "__dashboard_section_csr");
		}
	}, {
		key: "__getDashboardSectionFileScanRegionId",
		value: function safeConcat() {
			return "".concat(this.elementId, "__dashboard_section_fsr");
		}
	}, {
		key: "__getDashboardSectionSwapLinkId",
		value: function safeConcat() {
			return "".concat(this.elementId, "__dashboard_section_swaplink");
		}
	}, {
		key: "__getScanRegionId",
		value: function safeConcat() {
			return "".concat(this.elementId, "__scan_region");
		}
	}, {
		key: "__getDashboardId",
		value: function safeConcat() {
			return "".concat(this.elementId, "__dashboard");
		}
	}, {
		key: "__getFileScanInputId",
		value: function safeConcat() {
			return "".concat(this.elementId, "__filescan_input");
		}
	}, {
		key: "__getStatusSpanId",
		value: function safeConcat() {
			return "".concat(this.elementId, "__status_span");
		}
	}, {
		key: "__getHeaderMessageContainerId",
		value: function safeConcat() {
			return "".concat(this.elementId, "__header_message");
		}
	}, {
		key: "__getCameraSelectionId",
		value: function safeConcat() {
			return "".concat(this.elementId, "__camera_selection");
		}
	}, {
		key: "__getCameraScanRegion",
		value: function el8id() {
			return document.getElementById(this.__getDashboardSectionCameraScanRegionId());
		}
	}, {
		key: "__getFileScanRegion",
		value: function el8id() {
			return document.getElementById(this.__getDashboardSectionFileScanRegionId());
		}
	}, {
		key: "__getFileScanInput",
		value: function el8id() {
			return document.getElementById(this.__getFileScanInputId());
		}
	}, {
		key: "__getDashboardSectionSwapLink",
		value: function el8id() {
			return document.getElementById(this.__getDashboardSectionSwapLinkId());
		}
	}]), Player;
}();
_defineProperty(Html5QrcodeScanner, "SCAN_TYPE_CAMERA", "SCAN_TYPE_CAMERA"), _defineProperty(Html5QrcodeScanner, "SCAN_TYPE_FILE", "SCAN_TYPE_FILE"), _defineProperty(Html5QrcodeScanner, "STATUS_SUCCESS", "STATUS_SUCCESS"), _defineProperty(Html5QrcodeScanner, "STATUS_WARNING", "STATUS_WARNING"), _defineProperty(Html5QrcodeScanner, "STATUS_DEFAULT", "STATUS_DEFAULT"), _defineProperty(Html5QrcodeScanner, "ASSET_FILE_SCAN", "https://raw.githubusercontent.com/mebjas/html5-qrcode/master/assets/file-scan.gif"),
	_defineProperty(Html5QrcodeScanner, "ASSET_CAMERA_SCAN", "https://raw.githubusercontent.com/mebjas/html5-qrcode/master/assets/camera-scan.gif");

